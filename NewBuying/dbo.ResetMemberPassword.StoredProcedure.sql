USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ResetMemberPassword]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ResetMemberPassword]
  @UserID               int,			--  操作员ID
  @MemberID				int,            --  会员ID  
  @RandomPWD			int,			--  是否随机密码。1：yes。 0：No  
  @OldPassword			varchar(512),   --  会员旧密码    
  @NewPassword			varchar(512) output   --  会员新密码  
AS
/****************************************************************************
**  Name : ResetMemberPassword  重置会员密码
**  Version: 1.0.0.2
**  Description : 重置会员密码。 @RandomPWD = 1时， 不校验旧密码，产生随机密码后返回参数。 @RandomPWD=0时，校验旧密码
**  Parameter :
**
 declare @NewPassword			varchar(512)
 set @NewPassword = '123'
  exec ResetMemberPassword  1, 1, 1, '278807', @NewPassword output
  print @NewPassword
  select * from member
**  Created by: Gavin @2012-05-21
**  Modify by: Gavin @2013-12-09 （ver 1.0.0.1）按照passwordrule的设置，设置密码的长度。
**  Modify by: Gavin @2014-06-23 （ver 1.0.0.2）根据输入的@OldPassword长度,如果长度>=32,就使用输入的passward，不做MD5处理。
**
****************************************************************************/
begin
  declare @MemberPassword varchar(512), @PWDValidDays int, @PWDValidDaysUnit int, @ResetPWDDays int, @NewExpiryDate datetime
  declare @PWDMinLength  int, @RandNum decimal(20, 0)
  declare @OldPasswordLen int
  
  set @OldPassword = RTrim(ISNULL(@OldPassword, ''))
  set @OldPasswordLen = LEN(@OldPassword)
  
  if (isnull(@OldPassword, '') <> '') and (@OldPasswordLen < 32) 
    set @OldPassword = dbo.EncryptMD5(@OldPassword)  
  
  select top 1 @PWDMinLength = PWDMinLength from passwordrule	where MemberPWDRule = 1
  set @PWDMinLength = isnull(@PWDMinLength, 6)
  if @PWDMinLength = 0 
    set @PWDMinLength = 6
      
  select Top 1 @PWDValidDays = PWDValidDays, @PWDValidDaysUnit = PWDValidDaysUnit, @ResetPWDDays = ResetPWDDays from PasswordRule where MemberPWDRule = 1
  select @MemberPassword = MemberPassword from member where memberID = @MemberID  
  if @@ROWCOUNT = 0 
    return -1
    
     set @NewExpiryDate = convert(varchar(10), getdate(), 120)     
     if @PWDValidDaysUnit = 0
       set @NewExpiryDate = '2100-01-01'
     if @PWDValidDaysUnit = 1
       set @NewExpiryDate = DATEADD(yy, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 2
       set @NewExpiryDate = DATEADD(mm, @PWDValidDays, @NewExpiryDate)   
     if @PWDValidDaysUnit = 3
       set @NewExpiryDate = DATEADD(ww, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 4
       set @NewExpiryDate = DATEADD(dd, @PWDValidDays, @NewExpiryDate)  
         
  if @RandomPWD = 0 
  begin      
    if isnull(@MemberPassword,'') = isnull(@OldPassword, '')
    begin    
      if isnull(@NewPassword, '') <> ''
      begin
        if @OldPasswordLen < 32
          set @NewPassword = dbo.EncryptMD5(@NewPassword)
      end
      
      Update Member set MemberPassword = @NewPassword,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
           UpdatedBy = @UserID, UpdatedOn = Getdate()
        where memberID = @MemberID              
    end else  
      return -3      
  end else
  begin
    select @RandNum = round(rand() * 100000000000000000, 0)
    set @NewPassword = right('00000000000000000' + convert(varchar(20), @RandNum), @PWDMinLength)    
    if isnull(@NewPassword, '') <> ''
       set @NewPassword = dbo.EncryptMD5(@NewPassword)    
    Update Member set MemberPassword = @NewPassword,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
           UpdatedBy = @UserID, UpdatedOn = Getdate()
        where memberID = @MemberID       
  end  
  return 0
end

GO
