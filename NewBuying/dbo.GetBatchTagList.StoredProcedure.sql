USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBatchTagList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/09 13:19:18
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetBatchTagList]
  
    @PageNumber		        		    int,
	@RowsperPage			        	int,
    @transactionnumbr                   [varchar](100),
    @tagtypecode                        varchar(64),    
    @createddatefrom                    [datetime],
    @createddateto                      [datetime],
    @createdby                          [varchar](50),
    @orderby                            [varchar](50),
    @sortdirection                      [varchar](10)
    
  
AS
SET NOCOUNT ON;


BEGIN
    
    DECLARE @userid [int]
    SET @userid = (SELECT [u].[UserID] FROM [dbo].[Accounts_Users] u
                        WHERE [u].[UserName] = @createdby)
                        
    DECLARE @FirstRec int, @LastRec int
	
    SELECT @FirstRec = (@PageNumber - 1) * @RowsperPage
	SELECT @LastRec = (@PageNumber * @RowsperPage + 1);
    
    IF @sortdirection = 'DESC'
        BEGIN
        
            With TempResult AS
                    			(
                                    Select ROW_NUMBER() OVER(ORDER BY 
                                                                    CASE @orderby
                                                                        WHEN 'TransactionNumber' THEN [b].[BatchCouponCode]
                                                                        WHEN 'TagTypeCode' THEN [c].[CouponTypeCode]
                                                                        WHEN 'CreatedOn' THEN CAST([b].[CreatedOn] AS CHAR)
                                                                        WHEN 'CreatedBy' THEN [a].[UserName] 
                                                                        ELSE [b].[BatchCouponCode]
                                                                     END DESC) AS RowNum,
                                        [b].[BatchCouponCode] AS TransactionNumber,
                                        [c].[CouponTypeCode] AS TagTypeCode,
                                        [b].[CreatedOn] AS CreatedOn,
                                        [a].[UserName] AS CreatedBy,
                                        (SELECT COUNT (1) FROM [dbo].[BatchCoupon] b
                                                INNER JOIN [dbo].[CouponType] c
                                                    ON b.[CouponTypeID] = [c].[CouponTypeID]
                                                INNER JOIN [dbo].[Accounts_Users] a ON [a].[UserID] = [b].[CreatedBy]
                                            WHERE [c].[CouponTypeCode] LIKE '%' + @tagtypecode + '%'
                                            AND [b].[BatchCouponCode]  LIKE '%' + @transactionnumbr + '%'
                                            AND b.[CreatedBy] = @userid
                                            AND CAST([b].[CreatedOn] AS DATE) BETWEEN @createddatefrom AND @createddateto) AS [Count]
                        	        FROM [dbo].[BatchCoupon] b
                                        INNER JOIN [dbo].[CouponType] c
                                            ON b.[CouponTypeID] = [c].[CouponTypeID]
                                        INNER JOIN [dbo].[Accounts_Users] a ON [a].[UserID] = [b].[CreatedBy]
                                    WHERE [c].[CouponTypeCode] LIKE '%' + @tagtypecode + '%'
                                    AND [b].[BatchCouponCode]  LIKE '%' + @transactionnumbr + '%'
                                    AND b.[CreatedBy] = @userid
                                    AND CAST([b].[CreatedOn] AS DATE) BETWEEN @createddatefrom AND @createddateto
                                )        
                                
                    SELECT top (@LastRec-1) *
                			FROM TempResult
                			WHERE RowNum > @FirstRec 
                			AND RowNum < @LastRec          
        END    
   
    ELSE
        BEGIN
        
            With TempResult AS
                    			(
                                    Select ROW_NUMBER() OVER(ORDER BY 
                                                                    CASE @orderby
                                                                        WHEN 'TransactionNumber' THEN [b].[BatchCouponCode]
                                                                        WHEN 'TagTypeCode' THEN [c].[CouponTypeCode]
                                                                        WHEN 'CreatedOn' THEN CAST([b].[CreatedOn] AS CHAR)
                                                                        WHEN 'CreatedBy' THEN [a].[UserName] 
                                                                        ELSE [b].[BatchCouponCode]
                                                                     END ASC) AS RowNum,
                                        [b].[BatchCouponCode] AS TransactionNumber,
                                        [c].[CouponTypeCode] AS TagTypeCode,
                                        [b].[CreatedOn] AS CreatedOn,
                                        [a].[UserName] AS CreatedBy,
                                        (SELECT COUNT (1) FROM [dbo].[BatchCoupon] b
                                                INNER JOIN [dbo].[CouponType] c
                                                    ON b.[CouponTypeID] = [c].[CouponTypeID]
                                                INNER JOIN [dbo].[Accounts_Users] a ON [a].[UserID] = [b].[CreatedBy]
                                            WHERE [c].[CouponTypeCode] LIKE '%' + @tagtypecode + '%'
                                            AND [b].[BatchCouponCode]  LIKE '%' + @transactionnumbr + '%'
                                            AND b.[CreatedBy] = @userid
                                            AND CAST([b].[CreatedOn] AS DATE) BETWEEN @createddatefrom AND @createddateto) AS [Count]
                        	        FROM [dbo].[BatchCoupon] b
                                        INNER JOIN [dbo].[CouponType] c
                                            ON b.[CouponTypeID] = [c].[CouponTypeID]
                                        INNER JOIN [dbo].[Accounts_Users] a ON [a].[UserID] = [b].[CreatedBy]
                                    WHERE [c].[CouponTypeCode] LIKE '%' + @tagtypecode + '%'
                                    AND [b].[BatchCouponCode]  LIKE '%' + @transactionnumbr + '%'
                                    AND b.[CreatedBy] = @userid
                                    AND CAST([b].[CreatedOn] AS DATE) BETWEEN @createddatefrom AND @createddateto
                                )        
                                
                    SELECT top (@LastRec-1) *
                			FROM TempResult
                			WHERE RowNum > @FirstRec 
                			AND RowNum < @LastRec          
        END    

END




GO
