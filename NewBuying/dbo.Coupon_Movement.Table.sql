USE [NewBuying]
GO
/****** Object:  Table [dbo].[Coupon_Movement]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupon_Movement](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[StoreID] [int] NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[OprID] [int] NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[CouponNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[RefKeyID] [int] NULL,
	[RefReceiveKeyID] [int] NULL,
	[RefTxnNo] [varchar](512) NULL,
	[OpenBal] [money] NULL,
	[Amount] [money] NULL,
	[CloseBal] [money] NULL,
	[OrgExpiryDate] [datetime] NULL,
	[NewExpiryDate] [datetime] NULL,
	[OrgStatus] [int] NULL,
	[NewStatus] [int] NULL,
	[BusDate] [datetime] NULL,
	[Txndate] [datetime] NULL,
	[TenderID] [int] NULL,
	[Additional] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[Remark] [varchar](512) NULL,
	[SecurityCode] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [varchar](512) NULL,
 CONSTRAINT [PK_CardPoint_Movement] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Insert_Coupon_Movement]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Coupon_Movement] ON [dbo].[Coupon_Movement]
after INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Coupon_Movement
* Version: 1.0.0.30
* Description : 
select * from coupon_movement
* Create By Gavin @2012-03-27
* Modify By Gavin @2012-07-19 (ver 1.0.0.1)
* Modify By Gavin @2012-07-31 (ver 1.0.0.2) 增加调用DoCouponForfeit。
* Modify By Gavin @2012-08-10 (ver 1.0.0.3) OprID=33 , 需要更新cardnumber
* Modify By Gavin @2012-08-17 (ver 1.0.0.4) 扩展Coupon绑定外部UID的操作（OprID=39），根据填入movement的面值，有效期，状态，来更新coupon
* Modify By Gavin @2012-09-07 (ver 1.0.0.5) forfeit时,根据closebal,更新couponamount.取消其中更改自身的 newstatus 和 newexpirydate。 改为在insert coupon_movement时填入。 同时 after insert 改成for insert
* Modify By Gavin @2012-09-11 (ver 1.0.0.6) 取消在此触发器中调用DoCouponForfeit。因为在coupon_movement触发器中调用，会导致更新coupon的couponamount错误
* Modify By Gavin @2012-09-24 (ver 1.0.0.7) 处理新增的Coupon面额调整功能。（OprID=44）
* Modify By Gavin @2012-10-11 (ver 1.0.0.8) 回滚到 dormant状态时（status=0），需要把pickupflag设置为0
* Modify By Gavin @2012-11-13 (ver 1.0.0.9) issue操作时（OprID=40），把storeid 填入coupon的 locatestoreid。
* Modify By Gavin @2012-11-15 (ver 1.0.0.10) 校验操作后的coupon金额, 如果小于0则raiseerror。 （使用此版本后，所有插入coupon_movement操作，都需要添加 try catch，以捕捉可能的出错）
* Modify By Gavin @2012-12-12 (ver 1.0.0.11) 激活coupon 时（绑定卡），发出消息。
* Modify By Gavin @2012-12-31 (ver 1.0.0.12) 游标加上Local， 加快执行速度
* Modify By Gavin @2013-03-08 (ver 1.0.0.13) 增加OprID=56的处理( POS调用,PostVoid)
* Modify By Gavin @2013-08-20 (ver 1.0.0.14) 取消消息发送功能,此功能由其他服务完成.
* Modify By Gavin @2013-09-09 (ver 1.0.0.15) 赠送Coupon时（OprID=27），也填写CouponActiveDate
* Modify By Gavin @2013-09-10 (ver 1.0.0.16) 第一次填写Coupon的cardnumber时,同时填写InitCardNumber,此后这个字段不再更改.
* Modify By Gavin @2013-09-11 (ver 1.0.0.17) 修正奖励coupon时,没有填写cardnumber的问题
* Modify By Gavin @2014-07-03 (ver 1.0.0.18) 增加StockStatus 的处理
* Modify By Gavin @2014-07-11 (ver 1.0.0.19) 增加CouponRedeemDate （新增字段）的处理
* Modify By Gavin @2014-07-17 (ver 1.0.0.20) Coupon在Active时,同时扣除店铺中的Coupon_onhand表的In Location状态的数量
* Modify By Gavin @2014-08-11 (ver 1.0.0.21) 增加考虑CouponType的 UnlimitedUsage, 如果为1,则此类COupon redeem时不改变状态.
* Modify By Gavin @2014-08-22 (ver 1.0.0.22) 转赠后的Coupon要变成未读 (IsRead=False)
* Modify By Gavin @2014-08-27 (ver 1.0.0.23) 增加消息发送, 增加 转赠失败归还的的处理（OprID=45）
* Modify By Gavin @2014-08-29 (ver 1.0.0.24) 取消消息发送。（未知原因：在触发器调用发送消息的过程没有效果。（过程已经调用，但是没有产生记录，单独调用没有问题））
* Modify By Gavin @2015-05-27 (ver 1.0.0.25) Coupon_Onhand的 Update 可能导致 deadlock。 因此加上 lock
* Modify By Darwin @2017-09-28 (ver 1.0.0.26) OprID = 57
* Modify By Darwin @2017-09-28 (ver 1.0.0.27) Set coupon StockStatus to exchanged if OprID = 57 and NewStatus = 2
* Modify By Darwin @2017-10-24 (ver 1.0.0.28) Set coupon.Exported = 0 if  @OprID IN (33, 35, 38, 41, 57)
* Modify By Darwin @2017-10-25 (ver 1.0.0.29) if @OprID = 58, Set Coupon.StockStatus = 7 / Returned
* Modify By Darwin @2017-10-27 (ver 1.0.0.30) if @OprID = 38, Set Coupon.Status = 4 / Expired / Lost
*/
/*==============================================================*/
begin
  Declare @OprID int
  select @OprID=OprID from INSERTED
  IF @OprID<>0
  BEGIN
        
  declare @Movementkeyid int, @CardNumber nvarchar(512), @CardTypeID int,@CouponNumber nvarchar(512), @CouponTypeID int,        
          @RefTxnNo varchar(512), @Remark varchar(512), @RefReceiveKeyID int, @CreatedBy varchar(512), 
          @OpenBal money, @Amount money, @CloseBal money, @BusDate datetime, @StoreID int, @TxnDate datetime
  declare @CouponValidityDuration int, @CouponValidityUnit int, @MemberID int, @ActiveResetExpiryDate int
  declare @NewCouponExpiryDate datetime, @NewCouponStatus int, @NewCouponAmount money, @NewCouponActiveDate datetime, @NewCouponIssueDate datetime,
		  @NewStoreID int, @NewRedeemStoreID int, @NewCardNumber varchar(512), @NewExpiryDate datetime, @CouponExpiryDate datetime, 
		  @CouponStatus int, @ServerCode varchar(512), @RegisterCode varchar(512), @ApprovalCode varchar(6), @NewStatus int
  declare @pickupflag int, @LocateStoreID int, @InitCardNumber varchar(64)	
  declare @NewStockStatus int, @StockStatus int, @CouponRedeemDate datetime, @UnlimitedUsage int, @IsRead int
  DECLARE @OnhandQty int
  DECLARE @CouponAdjustNumber [varchar](64)

  DECLARE CUR_Coupon_Movement CURSOR fast_forward local FOR
  select keyid, CardNumber,CouponNumber, CouponTypeID, OprID, RefTxnNo, Remark, CreatedBy, RefReceiveKeyID, OpenBal, Amount, 
		CloseBal,BusDate, TxnDate, StoreID, NewExpiryDate, ServerCode, RegisterCode, ApprovalCode, NewStatus FROM INSERTED
  OPEN CUR_Coupon_Movement
  FETCH FROM CUR_Coupon_Movement INTO @Movementkeyid, @CardNumber,@CouponNumber, @CouponTypeID, @OprID, @RefTxnNo,  @Remark,  @CreatedBy, @RefReceiveKeyID, @OpenBal, @Amount, 
        @CloseBal, @BusDate, @TxnDate, @StoreID, @NewExpiryDate, @ServerCode, @RegisterCode, @ApprovalCode, @NewStatus
  WHILE @@FETCH_STATUS=0
  begin
    -- ver 1.0.0.25 
    select @OnhandQty = OnhandQty from Coupon_Onhand with(ROWLOCK,UPDLock)
        where CouponTypeID = @CouponTypeID and StoreID = @StoreID and CouponStockStatus = 6 and CouponStatus = 0 
             
    select @NewCouponStatus = Status, @NewCouponExpiryDate = CouponExpiryDate, @NewCardNumber = CardNumber, @NewCouponAmount = CouponAmount,
		@NewCouponActiveDate = CouponActiveDate, @NewCouponIssueDate = CouponIssueDate, @NewStoreID = StoreID, @NewRedeemStoreID = RedeemStoreID,
		@pickupflag = Pickupflag, @LocateStoreID = LocateStoreID, @InitCardNumber = InitCardNumber, @StockStatus = StockStatus
		, @IsRead = IsRead
	  from Coupon where CouponNumber = @CouponNumber
    select @CouponValidityUnit = CouponValidityUnit, @CouponValidityDuration = CouponValidityDuration, @ActiveResetExpiryDate = ActiveResetExpiryDate 
        , @UnlimitedUsage = isnull(UnlimitedUsage, 0)
      from CouponType where CouponTypeID = @CouponTypeID
    set @CouponExpiryDate = @NewCouponExpiryDate  
    set @CouponStatus = @NewCouponStatus
    
    -- 更改有效期
    set @NewCouponExpiryDate = @NewExpiryDate
    
    -- 设置coupon状态			   
    set @NewCouponStatus = @NewStatus
      
    -- 更新金额
    if @OprID in (31, 32, 33, 34, 39, 53, 54, 55, 44, 56)
      set @NewCouponAmount = @CloseBal
    
    -- 店铺更新
    if @OprID in (33, 53)
      set @NewStoreID = @StoreID     
	if @OprID in (34, 54)
      set @NewRedeemStoreID = @StoreID      

    -- 杂项更新 
    set @NewStockStatus = @StockStatus 
     
    if @OprID in (33, 53)
      set @CouponRedeemDate = GETDATE() 
             
    if @OprID = 36   -- 转赠coupon. （转出）
      set @NewCardNumber = ''
    if @OprID IN (37, 45)   -- 转赠coupon. （转入）
    begin
      set @NewCardNumber = @CardNumber
      set @IsRead = 0
    END  
    IF @OprID = 38
        BEGIN
            SET @NewCouponStatus = 4
        END 
    if @OprID = 40   -- 发行
    begin
      set @NewCouponIssueDate = @Busdate
      set @LocateStoreID = @StoreID
      set @NewStockStatus = 6--5
    end  
    if @OprID = 41   -- 回滚
    begin
      if @StockStatus = 5
        set @NewStockStatus = 2 
    end
    
    if @OprID in (27,33,53)   -- 激活
    begin
      set @NewCouponActiveDate = @Busdate  
    end    
    if @OprID in (27, 31, 32, 33, 53)  -- 绑定
    begin
      set @NewCouponIssueDate = @Busdate
      set @NewCouponActiveDate = @Busdate 
      set @NewCardNumber = @CardNumber
    end
	if @OprID = 56 and @NewCouponStatus = 1   --(selling Coupon 的 void)
	begin
	  set @NewCardNumber = ''	    	    
	  set @LocateStoreID = NULL
	  set @NewCouponActiveDate = null   
	END    
    if @OprID = 57-- AND @NewStatus = 0
    BEGIN
    
        SET @CouponAdjustNumber = (SELECT DISTINCT [h].[CouponAdjustNumber] FROM Ord_CouponAdjust_H h
                                     INNER JOIN [dbo].[Ord_CouponAdjust_D] d
                                        ON [d].[CouponAdjustNumber] = [h].[CouponAdjustNumber]
                                    WHERE [d].[CouponNumber] = @CouponNumber
                                    AND [d].[ExchangeStatus] IS NOT NULL)
        IF @NewStatus = 0
            BEGIN 
                SET @NewCouponStatus = 0
				SET @NewStockStatus = 8
            END
		ELSE
			BEGIN
				SET @NewStockStatus = 8
			END
        
        
        SELECT  @NewCouponActiveDate =  [c].[CouponActiveDate], 
                @NewCouponExpiryDate = [c].[CouponExpiryDate]                
        FROM [dbo].[Coupon] c
             INNER JOIN Ord_CouponAdjust_D d
                ON [d].[CouponNumber] = [c].[CouponNumber]
             INNER JOIN [dbo].[Ord_CouponAdjust_H] h
                ON [h].[CouponAdjustNumber] = [d].[CouponAdjustNumber]
             WHERE [h].[CouponAdjustNumber] = @CouponAdjustNumber
             AND [d].[ExchangeStatus] = 'O'
    END 
    
    IF @OprID = 58
        BEGIN
            SET @NewStockStatus = 7
        END
        
-- ver 1.0.0.10 @2012-11-15 暂不加此更改
--    if @NewCouponAmount < 0 
--      Raiserror('Error Coupon Amount Balance.', 16, 1)
-- end      
    if @NewCouponStatus = 0 
      set @pickupflag = 0
    -- ver 1.0.0.16  
    if isnull(@InitCardNumber, '') = '' and isnull(@NewCardNumber, '') <> ''
      set @InitCardNumber = @NewCardNumber
      
    -- (ver 1.0.0.20)  Coupon_Onhand 表中CouponStatus 只有0  
    if @NewCouponStatus = 2 and @CouponStatus < 2 and @StockStatus = 6
    begin
      update Coupon_Onhand set OnhandQty = OnhandQty - 1 
        where CouponTypeID = @CouponTypeID and StoreID = @StoreID and CouponStockStatus = 6 and CouponStatus = 0 
    end 
     
    -- ver 1.0.0.21 
    if @UnlimitedUsage = 1
    begin
      if @NewCouponStatus = 3
        set @NewCouponStatus = 2
    end
       
    if @NewCouponExpiryDate is null
	  set @NewCouponExpiryDate = '2030-01-01'

    IF @OprID IN (33, 35, 38, 41, 57)
        BEGIN
             update Coupon set Status = @NewCouponStatus, CouponExpiryDate = @NewCouponExpiryDate, UpdatedOn = Getdate(),
             CardNumber = @NewCardNumber, CouponAmount=@NewCouponAmount, StoreID=@NewStoreID, RedeemStoreID = @NewRedeemStoreID, 
             CouponActiveDate = @NewCouponActiveDate, CouponIssueDate = @NewCouponIssueDate, Pickupflag = @pickupflag,
             LocateStoreID = @LocateStoreID, InitCardNumber = @InitCardNumber, StockStatus = @NewStockStatus
             , CouponRedeemDate = @CouponRedeemDate, IsRead = @IsRead, [Exported] =  0
            where CouponNumber = @CouponNumber
        END
    ELSE
        BEGIN
             update Coupon set Status = @NewCouponStatus, CouponExpiryDate = @NewCouponExpiryDate, UpdatedOn = Getdate(),
             CardNumber = @NewCardNumber, CouponAmount=@NewCouponAmount, StoreID=@NewStoreID, RedeemStoreID = @NewRedeemStoreID, 
             CouponActiveDate = @NewCouponActiveDate, CouponIssueDate = @NewCouponIssueDate, Pickupflag = @pickupflag,
             LocateStoreID = @LocateStoreID, InitCardNumber = @InitCardNumber, StockStatus = @NewStockStatus
             , CouponRedeemDate = @CouponRedeemDate, IsRead = @IsRead
            where CouponNumber = @CouponNumber        
        END 
        

   
--    update Coupon_Movement set OrgExpiryDate = @CouponExpiryDate, NewExpiryDate = @NewCouponExpiryDate, OrgStatus = @CouponStatus, NewStatus = @NewCouponStatus where KeyID = @Movementkeyid
/*
    -- 产生消息记录 
    declare @binchar varbinary(max), @MessageBody nvarchar(max), @MessageTitle nvarchar(512)           
    if @OprID in (31, 32, 33, 53) and isnull(@CardNumber, '') <> ''
    begin 
      set @MessageTitle = 'Active Coupon'
      set @MessageBody = 'Active Coupon: ' + @CouponNumber + char(13) + 'CardNumber:' + @CardNumber + char(13)
               + 'TRAN ID:' + @RefTxnNo
      select @MemberID = MemberID from Card where CardNumber = @CardNumber
      set @binchar = cast(@MessageBody as varbinary(max)) 
      exec SendMemberNoticeMessage 1, @MemberID, 1, 1, 0, @MessageTitle, @binchar  
    end                 
*/   
   -- ver 1.0.0.11  发送消息
 /*
   select @CardGradeID = C.CardGradeID, @CardTypeID = C.CardTypeID, @MemberID = MemberID, @CardBrandID = T.BrandID
    from Card C
    left join CardType T on C.CardTypeID = T.CardTypeID
    where C.CardNumber = @NewCardNumber     
   exec GenBusinessMessage @OprID, @MemberID, @NewCardNumber,  @CouponNumber, @CardTypeID, @CardGradeID, @CardBrandID,  @CouponTypeID
 */ 
           
    FETCH FROM CUR_Coupon_Movement INTO @Movementkeyid, @CardNumber,@CouponNumber, @CouponTypeID, @OprID, @RefTxnNo,  @Remark,  @CreatedBy, @RefReceiveKeyID, @OpenBal, @Amount, 
        @CloseBal, @BusDate, @TxnDate, @StoreID, @NewExpiryDate, @ServerCode, @RegisterCode, @ApprovalCode, @NewStatus
  end
  CLOSE CUR_Coupon_Movement 
  DEALLOCATE CUR_Coupon_Movement  
  
  END   
end








GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺主键。receivetxn数据写入时，需要根据storecode+brandcode转换' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型ID：
101：会员获取Coupon（购买，消费赠送...）
102：会员获得转赠。
103：会员转赠他人。
104：过期作废。
105：会员使用coupon。
106：void操作。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'CouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有关联的movement记录的KeyID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'RefKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果有ReceiveTxn记录触发，则记录ReceiveTxn的KeyID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'RefReceiveKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果有ReceiveTxn记录触发，则记录ReceiveTxn的TxnNo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'RefTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作之前的金额值。（现金卷时有效。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'OpenBal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作的金额值。（现金卷时有效。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作之后的金额值。（现金卷时有效。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'CloseBal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作前的有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'OrgExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改后Coupon的有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'NewExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此操作前Coupon状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'OrgStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此操作后Coupon状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'NewStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'Txndate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息。从其他银行卡转账时，记录银行卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用银行卡支付后返回的号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'签名字段。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement', @level2type=N'COLUMN',@level2name=N'SecurityCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵操作流水记录表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Movement'
GO
