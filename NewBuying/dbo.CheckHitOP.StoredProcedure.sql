USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckHitOP]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[CheckHitOP]
  @HitOP        int,
  @HitValue     int,
  @InputValue   money,
  @ReturnValue  int output,
  @HitCount     int output,
  @Limit        int=1  
AS
begin
  declare @Result int
  set @ReturnValue = 0
  set @Result = -1
  set @HitCount = 0
  set @Limit = ISNULL(@Limit, 1)
  if @Limit = 0 
    set @Limit = 1
  if @Limit = -1
    set @Limit = 999999
    
  if @HitOP = 1 and @InputValue >= @HitValue
  begin    
    set @HitCount = @InputValue / @HitValue 
    if @HitCount > @Limit
      set @HitCount = @Limit
    set @ReturnValue = @InputValue - @HitValue * @HitCount    
    set @Result = 0 
  end
  else if @HitOP = 2 and @InputValue <> @HitValue  
  begin
    set @HitCount = 1
    set @Result = 0 
  end  
  else if @HitOP = 3 and @InputValue <= @HitValue  
  begin
    set @HitCount = 1
    set @Result = 0 
  end  
  else if @HitOP = 4 and @InputValue >= @HitValue  
  begin
    set @HitCount = 1
    set @Result = 0
  end  
  else if @HitOP = 5 and @InputValue < @HitValue
  begin
    set @HitCount = 1  
    set @Result = 0
  end  
  else if @HitOP = 6 and @InputValue > @HitValue  
  begin
    set @HitCount = 1
    set @Result = 0
  end  
           
  return @Result      
end

GO
