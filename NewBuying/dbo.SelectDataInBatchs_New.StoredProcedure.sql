USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SelectDataInBatchs_New]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectDataInBatchs_New]
  @QueryStr			nvarchar(4000),    -- 脤戙逄曆瞰: select * from card where cardnumber = 'XXXXX'
  @KeyField			nvarchar(60),	   -- 脤戙杅擂摩腔翋瑩趼僇ㄛ 甡擂翋瑩懂隅暮翹毓峓﹝
  @PageCurrent		int=1,			   -- 菴撓珜暮翹ㄛ蘇1ㄛ苤衾脹衾0奀珩峈1
  @PageSize			int=0,			   -- 藩珜暮翹杅ㄛ 峈0奀ㄛ祥煦珜ㄛ蘇0
  @PageCount		int=1 output,	   -- 殿隙軞珜杅﹝
  @RecordCount		int=0 output,	   -- 殿隙軞暮翹杅﹝
  @OrderCondition	nvarchar(100)='',	   -- 暮翹摩齬唗沭璃,彆峈諾ㄛ寀偌桽KeyField齬唗
  @QueryCondition	nvarchar(2000)=''	   -- 蚚誧赻隅砱脤戙沭璃﹝
AS
/****************************************************************************
**  Name : SelectDataInBatchs_New
**  Version: 1.0.0.0
**  Description : 煦蠶脤戙杅擂摩﹝SQL Server 2012 枑鼎腔OFFSET源楊, 樓辦湮杅擂踱脤戙奀腔厒僅
**  sample:
      declare @m int
      exec SelectDataInBatchs_New 'select * from product', 'prodcode', 2, 50, @m output
      print @m
	  select * from product order by prodcode
	  OFFSET 50 ROWS
	  FETCH NEXT 50 ROWS ONLY
	  sp_helptext SelectDataInBatchs_New
	 01126387 01136379
**  Created by: Gavin @2016-07-25
**
****************************************************************************/
BEGIN
  declare @DataRecordCount int, @x int, @y int, @SQLStr nvarchar(4000), @RemainderCount int
  set @Pagecount = 0
  set @OrderCondition = isnull(@OrderCondition, '')
  set @QueryCondition = isnull(@QueryCondition, '')
  
  if isnull(@QueryStr, '') = ''
    return -1
  if @QueryCondition <> ''
  begin
    set @SQLStr = N'select @count = count(*) from (select * from (' + @QueryStr + ') A ' + ' where (' + @QueryCondition + ')) B'                 
    exec sp_executesql @SQLStr, N'@count bigint output',@DataRecordCount output
    select @RecordCount = @DataRecordCount      
  end else
  begin  
    set @SQLStr = N'select @count = count(*) from (' + @QueryStr + ') A'             
    exec sp_executesql @SQLStr, N'@count bigint output',@DataRecordCount output
    select @RecordCount = @DataRecordCount      
  end  
  
  if @PageSize > 0
  begin             
	select @Pagecount = @DataRecordCount / @PageSize
	select @RemainderCount = @DataRecordCount % @PageSize 
	if @RemainderCount > 0
	  set @Pagecount = @Pagecount + 1
	if @PageCurrent <= 0
      set @PageCurrent = 1 

    set @x = @PageCurrent * @PageSize
	if isnull(@x,0) < 0 set @x = 0
	set @y = (@PageCurrent - 1) * @PageSize

	if @PageCurrent >= @PageCount 
	begin
	  set @x = @RecordCount
	  set @y = (@PageCount - 1) * @PageSize
	end

	if isnull(@y,0) < 0 set @y = 0

    if isnull(@OrderCondition, '') = ''
      set @OrderCondition = ' order by ' + @KeyField
   
    if @QueryCondition <> ''
	begin
      set @SQLStr = N'select * from  (' + @QueryStr + ') A where (' + @QueryCondition + ') '
      if @OrderCondition = ''
        set @OrderCondition = ' order by ' + @KeyField
      set @SQLStr= @SQLStr + ' ' + @OrderCondition  
	  set @SQLStr = @SQLStr + ' OFFSET ' + CAST(@y as varchar) + ' ROWS '
	    + ' FETCH NEXT ' + CAST(@PageSize as varchar) + ' ROWS ONLY '
	end
	else
	begin
	  set @SQLStr = N'select * from  (' + @QueryStr + ') A '
      if @OrderCondition = ''
        set @OrderCondition = ' order by ' + @KeyField
      set @SQLStr= @SQLStr + ' ' + @OrderCondition  
	  set @SQLStr = @SQLStr + ' OFFSET ' + CAST(@y as varchar) + ' ROWS '
	    + ' FETCH NEXT ' + CAST(@PageSize as varchar) + ' ROWS ONLY '
	end

    exec sp_executesql @SQLStr
  end else
  begin
    set @SQLStr = @QueryStr
	  if @QueryCondition <> ''
	    set @SQLStr= 'select * from (select * from (' + @QueryStr + ') A ' + ' where (' + @QueryCondition + ')) B'
  	else
	    set @SQLStr= 'select * from (' + @QueryStr + ') A ' 
	      
    if @OrderCondition = ''
      set @OrderCondition = ' order by ' + @KeyField
    set @SQLStr= @SQLStr + ' ' + @OrderCondition  
            
    exec sp_executesql @SQLStr
  end  
  return 0      
END

GO
