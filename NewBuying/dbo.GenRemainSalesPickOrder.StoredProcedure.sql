USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenRemainSalesPickOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[GenRemainSalesPickOrder]
  @UserID				         INT,	            -- 操作员ID
  @SalesPickOrderNumber          VARCHAR(64)        -- 单号码
AS
/****************************************************************************
**  Name : GenRemainSalesPickOrder 
**  Version: 1.0.0.1
**  Description : 拣货单批核后, 剩余未拣货物品,自动产生新的拣货单
        (这里不判断Pick单的状态,只根据number来取单,根据actualqty来产生新单. 这个SP应该在 trigger中调用, 防止误产生多的pick单)
exec SignSalesShipOrder 1, 'SSO000000000001'
sp_helptext GenRemainSalesPickOrder
**  Created by: Gavin @2016-06-30  
**  Modified by Gavin @2017-03-16 （1.0.0.1) 根据要求,默认ActualQty 填上和 OrderQty相同的数量,而不是0
****************************************************************************/
BEGIN
  DECLARE  @NewNumber VARCHAR(64)

  IF EXISTS(SELECT * FROM Ord_SalesPickOrder_D WHERE SalesPickOrderNumber = @SalesPickOrderNumber AND ISNULL(OrderQty,0) > ISNULL(ActualQty,0))
  BEGIN
	EXEC GetRefNoString 'SPUO', @NewNumber OUTPUT
	INSERT INTO Ord_SalesPickOrder_D(SalesPickOrderNumber,ProdCode,OrderQty,ActualQty,StockTypeCode,Remark)
	SELECT @NewNumber, ProdCode,ISNULL(OrderQty,0) - ISNULL(ActualQty,0),ISNULL(OrderQty,0) - ISNULL(ActualQty,0),StockTypeCode,Remark FROM Ord_SalesPickOrder_D 
	WHERE SalesPickOrderNumber = @SalesPickOrderNumber AND ISNULL(OrderQty,0) > ISNULL(ActualQty,0)

	IF @@ROWCOUNT <> 0
	BEGIN
		INSERT INTO Ord_SalesPickOrder_H(SalesPickOrderNumber,OrderType,MemberID,CardNumber,ReferenceNo,PickupLocation,PickupStaff,PickupDate,
			DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,CreatedBusDate,ApproveBusDate,
			ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		SELECT @NewNumber,OrderType,MemberID,CardNumber,ReferenceNo,PickupLocation,PickupStaff,PickupDate,
			DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,CreatedBusDate,ApproveBusDate,
			'','P',null,@UserID,GETDATE(),@UserID,GETDATE(),@UserID 
		FROM Ord_SalesPickOrder_H WHERE SalesPickOrderNumber = @SalesPickOrderNumber
	END
  END
  RETURN 0
END

GO
