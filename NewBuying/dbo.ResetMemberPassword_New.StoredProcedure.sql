USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ResetMemberPassword_New]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ResetMemberPassword_New]
  @UserID               int,			--  操作员ID
  @MemberID             int,            -- 会员ID
  @MemberRegisterMobile    nvarchar(512),  --  会员注册ID （Member表MemberRegisterMobile） 
  @MemberMobilePhone       nvarchar(512),  --  会员手机号  （默认Member表的MemberMobilePhone，可根据实际情况改）
  @MemberEmail             nvarchar(512),  --  会员注册的邮箱 （默认Member表的MemberEmail，可根据实际情况改）
  @VerifyType              int,            --  验证方式。 0：DB产生新密码。 1：DB产生VerifyCode。（verifycode 有时效性，需要用户在时间内完成reset PWD）
  @VerifyValue             varchar(512),   -- 当@VerifyType=0，则填入旧PWD， 当@VerifyType=1，填入Member的VerifyCode（有时效性） 
  @RandomPWD			   int,			--  是否随机密码。1：yes。 0：No   @VerifyType = 0 时有效   
  @NewPassword             varchar(512) output --  会员新密码。@RandomPWD
AS
/****************************************************************************
**  Name : ResetMemberPassword_New  重置会员密码
**  Version: 1.0.0.5
**  Description : 重置会员密码。 @RandomPWD = 1时， 不校验旧密码，产生随机密码后返回参数。 @RandomPWD=0时，校验旧密码
**  Parameter :
**
 declare @NewPassword   varchar(512)  , @a int
 set @NewPassword = ''  
  exec @a = ResetMemberPassword_New  1, 0, 'kingg64@hotmail.com', '', '', 1, '564709', 0, @NewPassword output  
  print @a
  print @NewPassword  
**  Created by: Gavin @2014-07-09
**  Modify by: Gavin @2014-07-25 (ver 1.0.0.3) 重置密码后，密码错误计数需要清0
**  Modify by: Gavin @2014-09-15 (ver 1.0.0.4) 验证通过重置密码后,需要清空这个code
**  Modify by: Gavin @2015-02-25 (ver 1.0.0.5) 有效时间由原来的10分钟， 改为 12 个小时。精度还是分钟
**
****************************************************************************/
begin
  declare @MemberPassword varchar(512), @PWDValidDays int, @PWDValidDaysUnit int, @ResetPWDDays int, @NewExpiryDate datetime
  declare @PWDMinLength  int, @RandNum decimal(20, 0)
  declare @VerifyValueLen int, @ResetPWDVerCode varchar(64), @ResetPWDVerCodeTime datetime

  declare @VerCodeValid int  -- 校验码有效时间,单位分钟
  --set @VerCodeValid = 10     -- hardcode 有效时间 10 分钟
  set @VerCodeValid = 12 * 60     -- hardcode 有效时间 12 小时
  
  set @MemberID = ISNULL(@MemberID, 0)
  set @MemberRegisterMobile = isnull(@MemberRegisterMobile, '')
  set @MemberMobilePhone = isnull(@MemberMobilePhone, '')
  set @MemberEmail = isnull(@MemberEmail, '')
  set @VerifyType = isnull(@VerifyType, 0)    
  set @VerifyValue = RTrim(ISNULL(@VerifyValue, ''))
  set @VerifyValueLen = LEN(@VerifyValue)

  select @MemberID = memberID, @MemberPassword = MemberPassword, @ResetPWDVerCode = isnull(ResetPWDVerCode,''), 
      @ResetPWDVerCodeTime = isnull(ResetPWDVerCodeTime,'')
    from member 
    where (MemberRegisterMobile = @MemberRegisterMobile and @MemberRegisterMobile <> '') 
       or (MemberMobilePhone = @MemberMobilePhone and @MemberMobilePhone <> '') 
       or (MemberEmail = @MemberEmail and @MemberEmail <> '') 
       or (MemberID = @MemberID and @MemberID <> 0)
  if @@ROWCOUNT = 0 
    return -1 
    
  if @VerifyType = 0
  begin
    if (isnull(@VerifyValue, '') <> '') and (@VerifyValueLen < 32) 
      set @VerifyValue = dbo.EncryptMD5(@VerifyValue) 
    select top 1 @PWDMinLength = PWDMinLength from passwordrule	where MemberPWDRule = 1
    set @PWDMinLength = isnull(@PWDMinLength, 6)
    if @PWDMinLength = 0 
      set @PWDMinLength = 6    
    select Top 1 @PWDValidDays = PWDValidDays, @PWDValidDaysUnit = PWDValidDaysUnit, @ResetPWDDays = ResetPWDDays from PasswordRule where MemberPWDRule = 1
    select @MemberPassword = MemberPassword from member where memberID = @MemberID  
    if @@ROWCOUNT = 0 
      return -1  
      
     set @NewExpiryDate = convert(varchar(10), getdate(), 120)     
     if @PWDValidDaysUnit = 0
       set @NewExpiryDate = '2100-01-01'
     if @PWDValidDaysUnit = 1
       set @NewExpiryDate = DATEADD(yy, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 2
       set @NewExpiryDate = DATEADD(mm, @PWDValidDays, @NewExpiryDate)   
     if @PWDValidDaysUnit = 3
       set @NewExpiryDate = DATEADD(ww, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 4
       set @NewExpiryDate = DATEADD(dd, @PWDValidDays, @NewExpiryDate)  
       
     if @RandomPWD = 0 
     begin      
       if isnull(@MemberPassword,'') = isnull(@VerifyValue, '')
       begin    
         if isnull(@NewPassword, '') <> ''
         begin
           if Len(@NewPassword) < 32
             set @NewPassword = dbo.EncryptMD5(@NewPassword)
         end
        
         Update Member set MemberPassword = @NewPassword,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
              UpdatedBy = @UserID, UpdatedOn = Getdate(), PWDFailCount = 0
           where memberID = @MemberID              
       end else  
         return -3      
     end else
     begin
       select @RandNum = round(rand() * 100000000000000000, 0)
       set @NewPassword = right('00000000000000000' + convert(varchar(20), @RandNum), @PWDMinLength)    
       if isnull(@NewPassword, '') <> ''
          set @NewPassword = dbo.EncryptMD5(@NewPassword)    
       Update Member set MemberPassword = @NewPassword,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
              UpdatedBy = @UserID, UpdatedOn = Getdate(), PWDFailCount = 0,
              ResetPWDVerCode='', ResetPWDVerCodeTime=0
           where memberID = @MemberID       
     end                     
  end
  else
  begin
    if datediff(mi, @ResetPWDVerCodeTime, getdate()) <= @VerCodeValid    -- 校验码在有效期中
    begin
      if @ResetPWDVerCode = @VerifyValue   -- 校验码正确
      begin
         if isnull(@NewPassword, '') <> ''
         begin
           if Len(@NewPassword) < 32
             set @NewPassword = dbo.EncryptMD5(@NewPassword)
         end
        
         Update Member set MemberPassword = @NewPassword,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
              UpdatedBy = @UserID, UpdatedOn = Getdate(), PWDFailCount = 0,
              ResetPWDVerCode='', ResetPWDVerCodeTime=0
           where memberID = @MemberID          
      end else      
        return -201  -- 校验码不正确
    end else
      return -202  --  校验码已经过期
  end
  
  return 0
end

GO
