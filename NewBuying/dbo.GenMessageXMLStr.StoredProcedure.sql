USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenMessageXMLStr]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GenMessageXMLStr]
  @ID                   varchar(1023),    -- 识别ID, 例如:LostPassword
  @InputStr				varchar(max),    -- 输入内容。
  @ReturnXMLstr         varchar(max) output    -- 返回的XML字符串。
AS
/****************************************************************************
**  Name : GenMessageXMLStr
**  Version: 1.0.0.0
**  Description : 根据输入的字符串,产生符合要求的XML格式字符串
**
@InputStr format:
  MPASSWORD=1234,MEMBERID=1,MSGACCOUNT=32424,MSGACCOUNTTYPE=1|MPASSWORD=1234,MEMBERID=1,MSGACCOUNT=32424,MSGACCOUNTTYPE=2
@ReturnXMLstr format (固定结构: Root -> 功能ID -> Account -> 下面具体的内容):
<Root>
	<LostPassword>
	    <Account>
		    <MPASSWORD>1234</MPASSWORD>
		    <MEMBERID>1234</MEMBERID>
		    <MSGACCOUNT>1234</MSGACCOUNT>
		    <MSGACCOUNTTYPE>1234</MSGACCOUNTTYPE>
  	    </Account>
	</LostPassword>
</Root>
declare @ReturnXMLstr  varchar(max)
exec GenMessageXMLStr 'LostPassword', 'MPASSWORD=1234,MEMBERID=1,MSGACCOUNT=32424,MSGACCOUNTTYPE=1|MPASSWORD=1234,MEMBERID=1,MSGACCOUNT=32424,MSGACCOUNTTYPE=2', @ReturnXMLstr output
print @ReturnXMLstr
**  Created by:  Gavin @2013-08-19
**  Modify by Gavin @2012-(ver 1.0.0.1)
****************************************************************************/
begin
  declare @XMLStr varchar(max), @TempStr varchar(max), @POS int, @ConditionTempStr varchar(max),
          @ConditionStr varchar(1023), @Parameter varchar(1023), @Value varchar(1023) 
  set @XMLStr = '<Root>' + '<' + @ID + '>'

  set @TempStr = RTrim(LTrim(@InputStr))
  while len(@TempStr) > 0
  begin
    set @XMLStr = @XMLStr + '<Account>'
    set @POS = CharIndex('|', @TempStr)
    if @POS > 0 
    begin
      set @ConditionTempStr = substring(@TempStr, 1, @POS - 1)
	  set @TempStr = substring(@TempStr, @POS + 1, len(@TempStr) - @POS)
    end  else
    begin
      set @ConditionTempStr = @TempStr
	  set @TempStr = ''
    end
    
    set @ConditionTempStr = RTrim(LTrim(@ConditionTempStr))
    while len(@ConditionTempStr) > 0
    begin
      set @POS = CharIndex(',', @ConditionTempStr)
      if @POS > 0 
      begin
        set @ConditionStr = substring(@ConditionTempStr, 1, @POS - 1)
	    set @ConditionTempStr = substring(@ConditionTempStr, @POS + 1, len(@ConditionTempStr) - @POS)
      end  else
      begin
        set @ConditionStr = @ConditionTempStr
	    set @ConditionTempStr = ''
      end
          
      if len(@ConditionStr) > 0
      begin
        set @POS = CharIndex('=', @ConditionStr)
        set @Parameter = substring(@ConditionStr, 1, @POS - 1)
	    set @Value = substring(@ConditionStr, @POS + 1, len(@ConditionStr) - @POS)
	    set @XMLStr = @XMLStr + '<' + @Parameter + '>' + @Value + '</' + @Parameter + '>'
      end
    end  
    set @XMLStr = @XMLStr + '</Account>'
  end 
    
  set @XMLStr = @XMLStr + '</' + @ID + '>' + '</Root>'
  set @ReturnXMLstr = @XMLStr
  return 0
end

GO
