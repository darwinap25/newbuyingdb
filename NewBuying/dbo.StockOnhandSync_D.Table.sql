USE [NewBuying]
GO
/****** Object:  Table [dbo].[StockOnhandSync_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockOnhandSync_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[HeadKeyID] [int] NOT NULL,
	[SyncType] [int] NOT NULL,
	[StoreTypeID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[CouponTypeID] [int] NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[Status] [int] NULL,
	[StockStatus] [int] NULL,
	[ActualQty] [int] NULL,
	[OnhandQty] [int] NULL,
	[AdjQty] [int] NULL,
 CONSTRAINT [PK_STOCKONHANDSYNC_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[StockOnhandSync_D] ADD  DEFAULT ((1)) FOR [SyncType]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'HeadKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调整类型。1：卡。 2：Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'SyncType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型。（即店铺类型）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存所属店铺（总部）ID。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SyncType=2， 填写CouponTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SyncType=1， 填写此' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SyncType=1， 填写此' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon或者Card的status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon或者Card的stockStatus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'StockStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际统计的 Coupon或者Card表数据' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'ActualQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Onhand表中的数据' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'OnhandQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调整的数据。 ActualQty- OnhandQty （更新时set OnhandQty = OnhandQty + AdjQty）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D', @level2type=N'COLUMN',@level2name=N'AdjQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存onhand同步记录表。（子表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_D'
GO
