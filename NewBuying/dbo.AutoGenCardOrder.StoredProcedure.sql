USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoGenCardOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[AutoGenCardOrder]
  @UserID               int,                     -- 操作用户ID
  @ReplenishCode        varchar(1000) = '',      -- 自动补货规则表Code, 可能会给多个，用 ,  隔开
  @ReferenceNo          varchar(64) = '',        -- 相关单号。（如果有的话）
  @BusDate              datetime = null,         -- 交易日
  @TxnDate              datetime = null,         -- 当前交易时间
  @IsSchedule           int=0                    -- 是否 日程安排 自动执行. 0:不是. 1: 是的
AS
/****************************************************************************
**  Name : AutoGenCardOrder
**  Version: 1.0.0.5
**  Description : 自动产生产生Card的订单（包括给供应商和总部的）.
  （从规则表中InventoryReplenishRule_H读取设置。） 
**
**  Parameter :	 select * from card_movement
  declare @a int  
  exec @a = AutoGenCardOrder 1, '01'
  print @a  
delete from ord_ordertosupplier_H
delete from ord_ordertosupplier_D 
delete from ord_ordertosupplier_card_H
delete from ord_ordertosupplier_card_D   
  select * from card_onhand
exec AutoGenOrder_New @UserID=1,@CouponReplenishCode='ddddd,ccccc,bbbbb,',@ReferenceNo=NULL,@BusDate=NULL,@TxnDate=NULL

**  Created by: Gavin @2014-09-28
**  Modify by: Gavin @2014-11-17 (ver 1.0.0.1) 从group by 子句中去掉 brandid,Priority
**  Modified by: Gavin @2014-11-21 (ver 1.0.0.2) 写入orderdetail时, 数量判断条件 从 onhand<Min 改为onhand<=Min
**  Modified by: Gavin @2014-12-18 (ver 1.0.0.3) @OrderRoundUpQty 变量值需要根据coupontypeid 来读取
**  Modified by: Gavin @2015-04-07 (ver 1.0.0.4) 增加PurchaseType = 1 条件
**  Modified by: Gavin @2015-05-06 (ver 1.0.0.5) 订单表中的数据太大,会导致执行时间过长, 采用预先读入临时表 的方式解决
**
****************************************************************************/
begin
  declare @SupplierID int, @NewPRNumber varchar(64), @OrderTargetID int, @OrderStoreTypeID int, @OrderStoreID int
  declare @StoreAddress varchar(512), @StoreContact varchar(512), @StoreContactPhone varchar(512),
          @StoreEmail varchar(512)
  declare @SourceAddress varchar(512), @SourceContact varchar(512), @SourceContactPhone varchar(512),
          @SourceEmail varchar(512)
  declare @BrandID int, @CouponTypeID int, @OrderQty int, @CompanyID int 
  declare @CardTypeID int, @CardGradeID int, @MediaType int
  declare @OnhandQty int, @MinStockQty int, @RecordCount int, @PackageQty int
  declare @ApprovalCode varchar(64), @OrderRoundUpQty int
  declare @StoreBrandID int
 
  -- ver 1.0.0.5
  declare @supplierorder table(CardTypeID int, CardGradeID int, SupplierQty int, StoreID int)
  declare @StoreOrderPick table(CardTypeID int, CardGradeID int, StoreOrderPickQty int, StoreID int)
  declare @StoreOrder table(CardTypeID int, CardGradeID int, StoreOrderQty int, StoreID int)
  declare @StoreDelivery table(CardTypeID int, CardGradeID int, StoreDeliveryQty int, StoreID int)
  
  insert into @supplierorder
  select D.CardTypeID, D.CardGradeID, sum(D.OrderQty) as SupplierQty, H.StoreID from Ord_OrderToSupplier_Card_D D left join Ord_OrderToSupplier_Card_H H on D.OrderSupplierNumber = H.OrderSupplierNumber where approvestatus = 'P' group by D.CardTypeID, D.CardGradeID, H.StoreID
  insert into @StoreOrderPick
  select D.CardTypeID, D.CardGradeID, sum(D.OrderQty) as StoreOrderPickQty, H.StoreID from Ord_CardPicking_D D left join Ord_CardPicking_H H on D.CardPickingNumber = H.CardPickingNumber where H.approvestatus in ('R','P') group by D.CardTypeID, D.CardGradeID, H.StoreID
  insert into @StoreOrder
  select D.CardTypeID, D.CardGradeID, sum(D.CardQty) as StoreOrderQty, H.StoreID from Ord_CardOrderForm_D D left join Ord_CardOrderForm_H H on D.CardOrderFormNumber = H.CardOrderFormNumber where H.approvestatus in ('A') group by D.CardTypeID, D.CardGradeID, H.StoreID
  insert into @StoreDelivery
  select D.CardTypeID, D.CardGradeID, sum(D.OrderQty) as StoreDeliveryQty, H.StoreID from ord_CardDelivery_D D left join ord_CardDelivery_H H on D.CardDeliveryNumber = H.CardDeliveryNumber where H.approvestatus in ('P') group by D.CardTypeID, D.CardGradeID, H.StoreID
  -----------   
   
  set @RecordCount = 0 
  select Top 1 @CompanyID = CompanyID from Company Order by IsDefault desc
  set @ReplenishCode = isnull(@ReplenishCode, '')          
  if isnull(@BusDate, 0) = 0
  Begin
    select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
    set @BusDate=isnull(@BusDate,getdate())
  end
  if isnull(@TxnDate, 0) = 0
    set @TxnDate=getdate()
 
									   
  DECLARE CUR_GenOrderToSupplier CURSOR fast_forward FOR
    select D.OrderTargetID, H.StoreTypeID, D.StoreID, max(S.StoreFullDetail),  max(S.Contact),   
       max(S.ContactPhone), max(S.Email)
     from InventoryReplenishRule_D D 
      left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
      left join (select CardTypeID, CardGradeID, StoreID, SUM(OnhandQty) as OnhandQty from Card_Onhand where CardStockStatus in (1,2,6) group by CardTypeID, CardGradeID, StoreID) O on H.CardTypeID = O.CardTypeID and H.CardGradeID = O.CardGradeID and D.StoreID = O.StoreID
      left join Store S on D.StoreID = S.StoreID
      left join CardType T on H.CardTypeID = T.CardTypeID  
      left join @supplierorder U on H.CardTypeID = U.CardTypeID and H.CardGradeID = U.CardGradeID and D.StoreID = U.StoreID
      left join @StoreOrderPick I on H.CardTypeID = I.CardTypeID and H.CardGradeID = I.CardGradeID and D.StoreID = I.StoreID 
      left join @StoreOrder J on H.CardTypeID = J.CardTypeID and H.CardGradeID = J.CardGradeID and D.StoreID = J.StoreID   
      left join @StoreDelivery K on H.CardTypeID = K.CardTypeID and H.CardGradeID = K.CardGradeID  and D.StoreID = K.StoreID 
     where isnull(O.OnhandQty,0)+ (case H.StoreTypeID when 1 then isnull(U.SupplierQty,0) else isnull(I.StoreOrderPickQty,0) + isnull(J.StoreOrderQty,0)+ isnull(K.StoreDeliveryQty,0) end) <= isnull(D.MinStockQty,0) 
       and (@ReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @ReplenishCode) > 0)
       and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1
       and ( (@IsSchedule = 0) or (
             ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
         and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
         and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		    )        
       )
      and H.MediaType = 2 and H.PurchaseType = 1
    group by D.OrderTargetID, H.StoreTypeID, D.StoreID -- , H.BrandID, D.Priority
    --order by D.Priority
  OPEN CUR_GenOrderToSupplier
  FETCH FROM CUR_GenOrderToSupplier INTO @OrderTargetID, @OrderStoreTypeID, @OrderStoreID, @StoreAddress, 
        @StoreContact, @StoreContactPhone, @StoreEmail
  WHILE @@FETCH_STATUS=0
  BEGIN
    set @RecordCount = @RecordCount + 1 
    if @OrderStoreTypeID = 2    -- 店铺向总部的订单.    
    begin
      select @StoreBrandID = BrandID from Store where StoreID = @OrderStoreID     
      exec GenApprovalCode @ApprovalCode output 
      exec GetRefNoString 'PRCA', @NewPRNumber output
      insert into Ord_CardOrderForm_H
          (CardOrderFormNumber, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod, SendAddress, FromAddress, 
          FromContactName, FromContactNumber, FromEmail, FromMobile, StoreContactName, StoreContactPhone,
          StoreContactEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, OrderType, ApprovalCode, ApproveOn)
      select @NewPRNumber, @StoreBrandID, @OrderTargetID, @OrderStoreID, 2, 0, 5, @StoreAddress, StoreFullDetail,
          Contact, ContactPhone, Email, '', @StoreContact, @StoreContactPhone,
          @StoreEmail, '', 'A', 'Auto Create order to HQ', 
          @BusDate, getdate(), @UserID, getdate(), @UserID, 1, @ApprovalCode, GETDATE()
       from Store where StoreID = @OrderTargetID        
      insert into Ord_CardOrderForm_D (CardOrderFormNumber, CardTypeID, CardGradeID, CardQty) 
      select @NewPRNumber, A.CardTypeID, A.CardGradeID,
        case (A.RunningStockQty-A.OnhandQty) % A.OrderRoundUpQty when 0 then (A.RunningStockQty-A.OnhandQty) else ((A.RunningStockQty-A.OnhandQty) / A.OrderRoundUpQty + 1) * A.OrderRoundUpQty end as OrderQty
      from 
      ( select H.CardTypeID, H.CardGradeID, (max(isnull(O.OnhandQty,0)) + max(isnull(I.StoreOrderPickQty,0)) + max(isnull(J.StoreOrderQty,0)) + MAX(isnull(K.StoreDeliveryQty,0))) as OnhandQty, max(isnull(D.MinStockQty,0)) as MinStockQty, 
              max(isnull(D.RunningStockQty,0)) as RunningStockQty, max(isnull(D.OrderRoundUpQty,0)) as OrderRoundUpQty --, D.Priority 
           from InventoryReplenishRule_D D 
           left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
           left join (select CardTypeID, CardGradeID, StoreID, SUM(OnhandQty) as OnhandQty from Card_Onhand where CardStockStatus in (1,2,6) group by CardTypeID, CardGradeID, StoreID) O on H.CardTypeID = O.CardTypeID and H.CardGradeID = O.CardGradeID and D.StoreID = O.StoreID
           left join @StoreOrderPick I on H.CardTypeID = I.CardTypeID and H.CardGradeID = I.CardGradeID and D.StoreID = I.StoreID 
           left join @StoreOrder J on H.CardTypeID = J.CardTypeID and H.CardGradeID = J.CardGradeID and D.StoreID = J.StoreID 
           left join @StoreDelivery K on H.CardTypeID = K.CardTypeID and H.CardGradeID = K.CardGradeID  and D.StoreID = K.StoreID                  
          where D.OrderTargetID = @OrderTargetID and D.StoreID = @OrderStoreID  
            and (@ReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @ReplenishCode) > 0)
            and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1            
            and ( (@IsSchedule = 0) or (
                   ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
               and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
               and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		           )        
              )   
            and H.MediaType = 2 and H.PurchaseType = 1             
         group by H.CardTypeID, H.CardGradeID --, D.Priority 
      ) A
      where OnhandQty <= MinStockQty
      --order by A.Priority 
      
      -- 不在触发器中调用,直接手动调用
      exec ChangeCardStockStatus 3, @NewPRNumber, 1
                     
    end 
    
    if @OrderStoreTypeID = 1   -- 总部向供应商的订单.
    begin         
      exec GetRefNoString 'PRCA', @NewPRNumber output
      insert into Ord_OrderToSupplier_Card_H
          (OrderSupplierNumber, SupplierID, OrderSupplierDesc, ReferenceNo, SendMethod, SendAddress,SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreID, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark, IsProvideNumber, OrderType,  
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CompanyID, PackageQty)
      select @NewPRNumber, @OrderTargetID, 'Auto Create order to supplier', @ReferenceNo, 5, @StoreAddress, Supplieraddress,
		  Contact, ContactPhone, ContactEmail, ContactMobile, @OrderStoreID, @StoreContact, @StoreContactPhone, 
		  @StoreEmail, '', 'P', '', 0, 1,
		  @BusDate, getdate(), @UserID, getdate(), @UserID, @CompanyID, 0
        from Supplier where SupplierID = @OrderTargetID     

      -- 产生供应商订单的明细记录。其中没有具体的Number，需要批核后才会产生。批核后会删除此记录，扩展为一个Number一条记录。
      insert into Ord_OrderToSupplier_Card_D (OrderSupplierNumber, CardTypeID, CardGradeID, OrderQty, FirstCardNumber, 
            EndCardNumber, BatchCardCode, PackageQty, OrderRoundUpQty)
      select B.OrderSupplierNumber, B.CardTypeID, B.CardGradeID, B.OrderQty, '', '', '', B.OrderQty / B.OrderRoundUpQty, B.OrderRoundUpQty
      from
      (
        select @NewPRNumber as OrderSupplierNumber, A.CardTypeID, A.CardGradeID, A.OrderRoundUpQty, 
          case (A.RunningStockQty-A.OnhandQty) % A.OrderRoundUpQty when 0 then (A.RunningStockQty-A.OnhandQty) else ((A.RunningStockQty-A.OnhandQty) / A.OrderRoundUpQty + 1) * A.OrderRoundUpQty end as OrderQty        
        from 
        ( select H.CardTypeID, H.CardGradeID, (max(isnull(O.OnhandQty,0)) + max(isnull(I.SupplierQty,0))) as OnhandQty, max(isnull(D.MinStockQty,0)) as MinStockQty, 
                max(isnull(D.RunningStockQty,0)) as RunningStockQty, max(isnull(D.OrderRoundUpQty,0)) as OrderRoundUpQty --, D.Priority 
             from InventoryReplenishRule_D D 
             left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
             left join (select CardTypeID, CardGradeID, StoreID, SUM(OnhandQty) as OnhandQty from Card_Onhand where CardStockStatus in (1,2,6) group by CardTypeID, CardGradeID, StoreID) O on H.CardTypeID = O.CardTypeID and H.CardGradeID = O.CardGradeID and D.StoreID = O.StoreID
             left join @supplierorder I on H.CardTypeID = I.CardTypeID and H.CardGradeID = I.CardGradeID and D.StoreID = I.StoreID 
            where D.OrderTargetID = @OrderTargetID and D.StoreID = @OrderStoreID  
              and (@ReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @ReplenishCode) > 0)
              and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1            
              and ( (@IsSchedule = 0) or (
                     ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
                 and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
                 and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		             )        
                )  
              and H.MediaType = 2 and H.PurchaseType = 1              
           group by H.CardTypeID, H.CardGradeID --, D.Priority 
        ) A 
        where OnhandQty <= MinStockQty       
      ) B 
      --order by B.Priority                    
    end  
    
    FETCH FROM CUR_GenOrderToSupplier INTO @OrderTargetID, @OrderStoreTypeID, @OrderStoreID, @StoreAddress, 
          @StoreContact, @StoreContactPhone, @StoreEmail
  END
  CLOSE CUR_GenOrderToSupplier 
  DEALLOCATE CUR_GenOrderToSupplier    
 
  return 0
end

GO
