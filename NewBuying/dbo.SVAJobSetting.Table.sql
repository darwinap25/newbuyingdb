USE [NewBuying]
GO
/****** Object:  Table [dbo].[SVAJobSetting]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SVAJobSetting](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[SPName] [varchar](64) NOT NULL,
	[Status] [int] NULL,
	[IntervalValue] [int] NULL,
	[IntervalUnit] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartTime] [datetime] NULL,
	[LastTime] [datetime] NULL,
 CONSTRAINT [PK_SVAJOBSETTING] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SVAJobSetting] ADD  DEFAULT ((0)) FOR [Status]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'需要执行的存储过程名字。（只能调用无参数的过程）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'SPName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：无效。 1：有效。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间间隔 数值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'IntervalValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间间隔单位。 1：分钟。2：小时。3：天' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'IntervalUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过程有效开始日期。不带时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过程有效结束日期。不带时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过程开始执行时间（只关注时间，日期忽略）。 当IntervalUnit=3 时有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'StartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后一次执行时间。有时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting', @level2type=N'COLUMN',@level2name=N'LastTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SVA DB job 设置表。
配合总调用过程：SVAJOB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVAJobSetting'
GO
