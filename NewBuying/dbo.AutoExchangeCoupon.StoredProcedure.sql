USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoExchangeCoupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AutoExchangeCoupon]  
  @InputCardNumber varchar(64)=''          
AS  
/****************************************************************************  
**  Name : AutoExchangeCoupon  
**  Version: 1.0.0.4  
**  Description : 自动把Member拥有的Coupon(印花类型)转换成其他Coupon （必须是在EarnCouponRule中设置的，用Coupon换Coupon的那种）  
  
    exec AutoExchangeCoupon '000100047'  
    
**   
**  Created by:  Gavin @2014-08-25  
**  Modify by: Gavin @2014-09-11 (ver 1.0.0.1) 换一个coupon发一次消息.    
**  Modify by: Gavin @2014-11-17 (ver 1.0.0.2) 增加参数@CardNumber， 指定卡的兑换  
**  Modify by: Gavin @2014-12-17 (ver 1.0.0.3) 使用GetCouponNumber获取CouponNumber, enddate的判增加 -1 
**  Modify by: Gavin @2015-01-12 (ver 1.0.0.4) 除了输入指定Card, 增加读取SalesQueue表中的CardNumber作为条件. 
                                               优化查询Coupon的速度。（原先的做法适合所有Card执行兑换。现在改为适合指定Card或者小批量Card的操作）
****************************************************************************/  
begin  
  declare @CouponTypeID int, @ExchangeCouponTypeID int, @ExChangeCouponCount int, @CardTypeIDLimit int, @CardGradeIDLimit int,   
          @CardTypeBrandIDLimit int, @MemberBirthdayLimit int, @MemberSexLimit int, @MemberAgeMinLimit int, @MemberAgeMaxLimit int  
  declare @CardNumber varchar(64), @CouponQty int, @BusDate datetime, @TxnDate datetime  
  declare @TempCount int, @i int, @ExchangeCouponNumber varchar(64), @ExchangeCouponAmount money, @ExchangeCouponExpiryDate datetime  
  declare @ApprovalCode varchar(6), @ReturnCouponNumber varchar(64), @CouponStatus int, @NewCouponStatus int,  
         @CouponExpiryDate datetime, @NewCouponExpiryDate datetime, @CouponAmount money  
  declare @MsgCardTypeID int, @MsgCardGradeID int, @MemberID int ,@MsgCardBrandID int  
  declare @CardTypeID int, @CardGradeID int, @BrandID int, @MemberSex int, @MemberDateOfBirth datetime
  
  set @InputCardNumber = isnull(@InputCardNumber, '')  
  if isnull(@BusDate, 0) = 0  
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc  
  if isnull(@TxnDate, 0) = 0   
    set @TxnDate = getdate()  
        
  DECLARE CUR_AutoExchangeCoupon CURSOR fast_forward FOR  
    select CouponTypeID, ExchangeCouponTypeID, ExChangeCouponCount, isnull(CardTypeIDLimit,0), isnull(CardGradeIDLimit,0),   
       isnull(CardTypeBrandIDLimit,0),isnull(MemberBirthdayLimit,0), isnull(MemberSexLimit,0), isnull(MemberAgeMinLimit,0),   
       isnull(MemberAgeMaxLimit,0)  
     from EarnCouponRule   
    where ExchangeType = 4 and StartDate <= Getdate() and EndDate >= (getdate() - 1) and status = 1  
    order by ExchangeRank  
  OPEN CUR_AutoExchangeCoupon  
  FETCH FROM CUR_AutoExchangeCoupon INTO @CouponTypeID, @ExchangeCouponTypeID, @ExChangeCouponCount, @CardTypeIDLimit,   
       @CardGradeIDLimit, @CardTypeBrandIDLimit, @MemberBirthdayLimit, @MemberSexLimit, @MemberAgeMinLimit, @MemberAgeMaxLimit  
  WHILE @@FETCH_STATUS=0  
  BEGIN        
    DECLARE CUR_AutoExchangeCoupon_Card CURSOR fast_forward FOR
      select C.CardNumber, C.CouponQty, A.CardTypeID, A.CardGradeID, T.BrandID, M.MemberSex, M.MemberDateOfBirth, M.MemberID 
       from (
        select CardNumber, Count(*) as CouponQty   
          from Coupon where CouponTypeID = @ExchangeCouponTypeID and Status = 2
            and ((@InputCardNumber = '' and CardNumber in (select CardNumber from SalesQueue where TypeID= 2)) 
                 or CardNumber = @InputCardNumber)
         group by CardNumber) C
        left join Card A on A.CardNumber = C.CardNumber
        left join CardType T on A.CardTypeID = T.CardTypeID 
        left join Member M on A.MemberID = M.MemberID
/*            
      select CardNumber, CouponQty from (   
        select C.CardNumber, Count(*) as CouponQty   
          from  Coupon C left join Card A on C.CardNumber = A.CardNumber   
           left join Member M on A.MemberID = M.MemberID  
           left join CardType T on A.CardTypeID = T.CardTypeID  
          where C.CouponTypeID = @ExchangeCouponTypeID   
            and (A.CardTypeID = @CardTypeIDLimit or @CardTypeIDLimit = 0)  
            and (A.CardGradeID = @CardGradeIDLimit or @CardGradeIDLimit = 0)  
            and (T.BrandID = @CardTypeBrandIDLimit or @CardTypeBrandIDLimit = 0)  
            and (M.MemberSex = @MemberSexLimit or @MemberSexLimit = 0)  
            and (convert(varchar(10), M.MemberDateOfBirth, 120) = convert(varchar(10), GETDATE(), 120) or @MemberBirthdayLimit = 0)  
            and (DATEDIFF(yy, M.MemberDateOfBirth, GETDATE())+1 >= @MemberAgeMinLimit or @MemberAgeMinLimit = 0)  
            and (DATEDIFF(yy, M.MemberDateOfBirth, GETDATE())+1 <= @MemberAgeMaxLimit or @MemberAgeMaxLimit = 0)  
            and C.Status = 2  
            and (@InputCardNumber = '' or C.CardNumber = @InputCardNumber)   -- ver 1.0.0.2  
         group by C.CardNumber   
      ) A   
      where  CouponQty >= @ExChangeCouponCount 
*/        
    OPEN CUR_AutoExchangeCoupon_Card  
    FETCH FROM CUR_AutoExchangeCoupon_Card INTO @CardNumber, @CouponQty, @CardTypeID, @CardGradeID, @BrandID, 
      @MemberSex, @MemberDateOfBirth, @MemberID  
    WHILE @@FETCH_STATUS=0  
    BEGIN 
      IF (@CouponQty >= @ExChangeCouponCount) 
        and (@CardTypeID = @CardTypeIDLimit or @CardTypeIDLimit = 0) 
        and (@CardGradeID = @CardGradeIDLimit or @CardGradeIDLimit = 0)  
        and (@BrandID = @CardTypeBrandIDLimit or @CardTypeBrandIDLimit = 0)  
        and (@MemberSex = @MemberSexLimit or @MemberSexLimit = 0)  
        and (convert(varchar(10), @MemberDateOfBirth, 120) = convert(varchar(10), GETDATE(), 120) or @MemberBirthdayLimit = 0)  
        and (DATEDIFF(yy, @MemberDateOfBirth, GETDATE())+1 >= @MemberAgeMinLimit or @MemberAgeMinLimit = 0)  
        and (DATEDIFF(yy, @MemberDateOfBirth, GETDATE())+1 <= @MemberAgeMaxLimit or @MemberAgeMaxLimit = 0)        
      BEGIN                  
        set @TempCount = @CouponQty  
        while @TempCount >= @ExChangeCouponCount  
        begin   
--          select top 1 @ReturnCouponNumber = CouponNumber, @CouponAmount = isnull(CouponAmount,0),  
--              @CouponStatus = status, @CouponExpiryDate = CouponExpiryDate   
--            from coupon where CouponTypeID = @CouponTypeID and Status = 1  
          exec GetCouponNumber @CouponTypeID, 1, @ReturnCouponNumber output  
          select @CouponAmount = isnull(CouponAmount,0), @CouponStatus = status, @CouponExpiryDate = CouponExpiryDate  
            from Coupon where CouponNumber = @ReturnCouponNumber  
              
          if isnull(@ReturnCouponNumber, '') <> ''  
          begin                   
            set @i = 0         
            DECLARE CUR_EarnCoupon_Auto CURSOR fast_forward local FOR    
              select CouponNumber, CouponAmount, CouponExpiryDate from coupon   
                where CouponTypeID = @ExchangeCouponTypeID and CardNumber = @CardNumber and status = 2  
              order by CouponNumber   
            OPEN CUR_EarnCoupon_Auto    
            FETCH FROM CUR_EarnCoupon_Auto INTO @ExchangeCouponNumber, @ExchangeCouponAmount, @ExchangeCouponExpiryDate  
            WHILE @@FETCH_STATUS=0    
            BEGIN  
              if @i >= @ExchangeCouponCount   
                break  
              else   
              begin            
                insert into Coupon_Movement  
                   (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal,   
                    BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,  
                    OrgExpirydate, OrgStatus, NewStatus)      
                values  
                   (34, @CardNumber, @ExchangeCouponNumber, @ExchangeCouponTypeID, '', 0, '', @ExchangeCouponAmount, -@ExchangeCouponAmount, 0,  
                   @BusDate, @TxnDate, '','', 1, @ExchangeCouponExpiryDate, '', null, null, null,  
                   @ExchangeCouponExpiryDate, 2, 3)    
                set @i = @i + 1  
              end           
              FETCH FROM CUR_EarnCoupon_Auto INTO @ExchangeCouponNumber, @ExchangeCouponAmount, @ExchangeCouponExpiryDate  
            END  
            CLOSE CUR_EarnCoupon_Auto  
            DEALLOCATE CUR_EarnCoupon_Auto   
                  
           exec GenApprovalCode @ApprovalCode output  
           exec CalcCouponNewStatus @ReturnCouponNumber, @CouponTypeID, 33, @CouponStatus, @NewCouponStatus output  
           exec CalcCouponNewExpiryDate @CouponTypeID, 33, @CouponExpiryDate, @NewCouponExpiryDate output       
            insert into Coupon_Movement  
              (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal,   
               BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,  
               OrgExpirydate, OrgStatus, NewStatus)      
            values  
              (33, @CardNumber, @ReturnCouponNumber, @CouponTypeID, '', 0, '', @CouponAmount, 0, @CouponAmount,  
               @BusDate, @TxnDate, '','', 1, @NewCouponExpiryDate, @ApprovalCode, null, null, null,  
               @CouponExpiryDate, @CouponStatus, @NewCouponStatus)  
   
            exec GenBusinessMessage 510, @MemberID, @CardNumber,  @ReturnCouponNumber, @CardTypeID, @CardGradeID, @BrandID, @CouponTypeID, ''  
                     
            set @TempCount = @TempCount - @ExChangeCouponCount  
          end   
          else  
            set @TempCount = 0   
        end  
      END           
      FETCH FROM CUR_AutoExchangeCoupon_Card INTO @CardNumber, @CouponQty, @CardTypeID, @CardGradeID, @BrandID, 
        @MemberSex, @MemberDateOfBirth, @MemberID  
    END  
    CLOSE CUR_AutoExchangeCoupon_Card   
    DEALLOCATE CUR_AutoExchangeCoupon_Card       
  
    FETCH FROM CUR_AutoExchangeCoupon INTO @CouponTypeID, @ExchangeCouponTypeID, @ExChangeCouponCount, @CardTypeIDLimit,   
       @CardGradeIDLimit, @CardTypeBrandIDLimit, @MemberBirthdayLimit, @MemberSexLimit, @MemberAgeMinLimit, @MemberAgeMaxLimit  
  END  
  CLOSE CUR_AutoExchangeCoupon   
  DEALLOCATE CUR_AutoExchangeCoupon        
end  

GO
