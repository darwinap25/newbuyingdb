USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[WriteSVACard]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[WriteSVACard]
  @UserID int,   
  @UID varchar(20),
  @CardNumber nvarchar(30),
  @CardNumber_Encrypt nvarchar(30),  
  @InitialPWD int = 0,
  @ReturnCardPWD varchar(40) output  
AS
/****************************************************************************
**  Name : WriteSVACard
**  Version: 1.0.0.0
**  Description :批量制作SVA Card(SVA manager 调用)，把SVA卡号写入实体卡中
**
**  Parameter :
  declare @ReturnCardPWD varchar(40), @a int
  set @ReturnCardPWD = ''
  exec @a = WriteSVACard '1', '05818AA9E02585', '000200001', 'uiewry34wiqw872346924', 1, @ReturnCardPWD output
  print @a
  print @ReturnCardPWD
  select top 10 * from card
  select top 10 * from Coupon
  select * from vencardIDMap  
**
**  Created by: Gavin @2012-04-12
****************************************************************************/
begin
  declare @LaserID varchar(20)
  exec UIDConvertToID @UID, @LaserID output
    
  if exists(select * from VenCardIDMap where VendorCardNumber = @UID and ValidateFlag = 1)
    return -30
  if not exists(select * from VenCardIDMap where VendorCardNumber = @UID and CardNumber = @CardNumber)
  begin    
    insert into VenCardIDMap (VendorCardNumber, CardNumber, CardNumberEncrypt, ValidateFlag, LaserID, CreatedBy, UpdatedBy)
    values(@UID, @CardNumber, @CardNumber_Encrypt, 1, @LaserID, @UserID, @UserID)
  end else
  begin
    Update VenCardIDMap set ValidateFlag = 1 where VendorCardNumber = @UID and CardNumber = @CardNumber
  end
  if @InitialPWD = 1
  begin
    if @ReturnCardPWD = ''
      set @ReturnCardPWD = '123456' 
    update card set status = 5, CardPassword = dbo.EncryptMD5(@ReturnCardPWD) where cardnumber = @CardNumber
  end else  
    update card set status = 5 where cardnumber = @CardNumber
  return 0       
end

GO
