USE [NewBuying]
GO
/****** Object:  Table [dbo].[ReceiveMessageHistory]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceiveMessageHistory](
	[ReceiveMessageID] [int] NOT NULL,
	[MemberID] [int] NULL,
	[SendMessageID] [int] NULL,
 CONSTRAINT [PK_RECEIVEMESSAGEHISTORY] PRIMARY KEY CLUSTERED 
(
	[ReceiveMessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
