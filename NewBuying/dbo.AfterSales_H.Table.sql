USE [NewBuying]
GO
/****** Object:  Table [dbo].[AfterSales_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AfterSales_H](
	[TxnNo] [varchar](64) NOT NULL,
	[TxnType] [int] NOT NULL DEFAULT ((0)),
	[RefTransNum] [varchar](64) NULL,
	[NewTransNum] [varchar](64) NULL,
	[StoreCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[BusDate] [datetime] NULL,
	[TxnDate] [datetime] NULL DEFAULT (getdate()),
	[CashierID] [int] NULL,
	[SalesManID] [int] NULL,
	[TotalAmount] [decimal](12, 4) NOT NULL,
	[Status] [int] NOT NULL,
	[Reason] [nvarchar](512) NULL,
	[InvalidateFlag] [int] NULL,
	[MemberSalesFlag] [int] NULL DEFAULT ((0)),
	[MemberID] [varchar](64) NULL,
	[CardNumber] [varchar](64) NULL,
	[DeliveryFlag] [int] NULL DEFAULT ((0)),
	[DeliveryCountry] [varchar](64) NULL,
	[DeliveryProvince] [varchar](64) NULL,
	[DeliveryCity] [varchar](64) NULL,
	[DeliveryDistrict] [varchar](64) NULL,
	[DeliveryAddressDetail] [nvarchar](512) NULL,
	[DeliveryFullAddress] [nvarchar](512) NULL,
	[DeliveryNumber] [varchar](64) NULL,
	[LogisticsProviderID] [int] NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryBy] [int] NULL,
	[Contact] [nvarchar](512) NULL,
	[ContactPhone] [nvarchar](512) NULL,
	[PickupType] [int] NULL DEFAULT ((1)),
	[PickupStoreCode] [varchar](64) NULL,
	[Remark] [nvarchar](512) NULL,
	[SettlementDate] [datetime] NULL,
	[SettlementStaffID] [int] NULL,
	[PaySettleDate] [datetime] NULL,
	[CompleteDate] [datetime] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_AFTERSALES_H] PRIMARY KEY CLUSTERED 
(
	[TxnNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_AfterSales_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_AfterSales_H] ON [dbo].[AfterSales_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_AfterSales_H 
* Version: 1.0.0.1
* Description : (for bauhaus) 要求退货换货，批核后在sales表中插入相应记录。
select * from AfterSales_H

** Create By Gavin @2016-10-13
** Modify By Gavin @2016-10-24 (ver 1.0.0.1) 使用linkserver，在审核时，同步数据到 buying DB。（Linkserver需要hardcode）
*/
/*==============================================================*/
BEGIN  
  DECLARE @TxnNo varchar(64), @Status int, @TxnType int,@CreatedBy int, @OldStatus int
  DECLARE @RefTransNum varchar(64)
  
  DECLARE CUR_AfterSales_H CURSOR fast_forward FOR
    SELECT TxnNo,RefTransNum,TxnType,Status,CreatedBy FROM INSERTED
  OPEN CUR_AfterSales_H
  FETCH FROM CUR_AfterSales_H INTO @TxnNo, @RefTransNum, @TxnType, @Status, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldStatus = Status from Deleted where TxnNo = @TxnNo
    if (isnull(@OldStatus, 0) = 0) and @Status = 1 and Update(Status)
    begin
	  set @TxnNo = isnull(@TxnNo, '')
	  set @RefTransNum = isnull(@RefTransNum, '')
	  if (@TxnNo <> '') and (@RefTransNum <> '') 
	  begin
	    exec GenSalesTxnByAfterSales @TxnNo, @RefTransNum, @CreatedBy 
      end
    end		      
    FETCH FROM CUR_AfterSales_H INTO @TxnNo, @RefTransNum, @TxnType, @Status, @CreatedBy
  END
  CLOSE CUR_AfterSales_H 
  DEALLOCATE CUR_AfterSales_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'TxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型.
1：维修
2：退货
3：换货
4：作废原单。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'TxnType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'RefTransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新交易单号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'NewTransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS机编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期时间。（system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收银员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'CashierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'SalesManID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总销售金额. 只是作为参考' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'TotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易状态。
0： 初建状态。
1： 已经批核
2： 用户已经发出快递
3： 退货已收到。（快递签收）
4： 换新货品已发出
5： 售后单流程完成。 （退货已收到，换货已发出签收，货款已退还）
6：拒绝。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'售后单原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'Reason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单无效标志。订单无效时，状态为完成。 默认0
0： 订单有效
1： 已经被Void。
2： 已经被refund。
3： 已经被exchange' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'InvalidateFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否会员销售。默认0.
0：不是会员销售。
1： 是会员销售' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'MemberSalesFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员号码。MemberSalesFlag = 1时需要填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡号码。 MemberSalesFlag = 1时需要填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货标志。0： 自提，不送货。 1：送货' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在国家（存放Country表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryCountry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在省 （存放Province表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryProvince'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在城市 （存放City表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryCity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址所在区县 （存放District表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryDistrict'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货详细地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryAddressDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货完整地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryFullAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货单号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流供应商ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'LogisticsProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货物送达日期   （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'DeliveryBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'退货方式。默认1
1：用叫快递送。
2：店铺上门取货。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'PickupType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快递送货到达的店铺编号。 （PickupType=1 时有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'PickupStoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收到货的日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'SettlementDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货最后确认人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'SettlementStaffID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'退还支付最后完成日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'PaySettleDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单最终完成日期 （business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'CompleteDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'售后交易表。主表. 用于记录用户的 退货，换货记录。
@2017-03-15， 增加字段： NewTransNum。 如果原单是已经完成的。则记录新产生的退货单或者换货单。 如果原单是未完成的，则记录新产生的销售单。
     TxnType 增加 4：作废' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_H'
GO
