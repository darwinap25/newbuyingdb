USE [NewBuying]
GO
/****** Object:  Table [dbo].[StoreGroup]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StoreGroup](
	[StoreGroupID] [int] IDENTITY(1,1) NOT NULL,
	[StoreGroupCode] [varchar](64) NULL,
	[StoreGroupName1] [nvarchar](512) NULL,
	[StoreGroupName2] [nvarchar](512) NULL,
	[StoreGroupName3] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_StoreGroup] PRIMARY KEY CLUSTERED 
(
	[StoreGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组ID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreGroup', @level2type=N'COLUMN',@level2name=N'StoreGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreGroup', @level2type=N'COLUMN',@level2name=N'StoreGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreGroup', @level2type=N'COLUMN',@level2name=N'StoreGroupName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreGroup', @level2type=N'COLUMN',@level2name=N'StoreGroupName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreGroup', @level2type=N'COLUMN',@level2name=N'StoreGroupName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreGroup'
GO
