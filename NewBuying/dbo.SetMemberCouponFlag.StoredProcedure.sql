USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetMemberCouponFlag]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[SetMemberCouponFlag]
  @CardNumber           varchar(64),       -- 卡号
  @CouponNumber         varchar(2000),       -- Coupon号码
  @FlagType             int,               -- 更新类型. 1: 更新IsRead标志。 2：更新IsFavorite标志
  @Value                int                -- 要更新的值。  0： No。 1： yes
AS
/****************************************************************************
**  Name : SetMemberCouponFlag  
**  Version: 1.0.0.3
**  Description : 设置会员的Coupon的 IsRead, IsFavorite
**  Parameter :
  declare @a int
  --exec @a = SetMemberCouponFlag '23423423', '''01000001'',''01000002''',1,1
  exec @a = SetMemberCouponFlag '32', '''01000001'',''01000002''',1,1
  print @a
  select * from member
**  Created by: Gavin @2014-07-11
**  Modify by: Gavin @2014-08-08 (ver 1.0.0.1) 接受多个Coupon传入，类似： '''012312312'',''2325343643'''
**  Modify by: Gavin @2014-08-14 (ver 1.0.0.2) 增加参数@CardNumber。 @CardNumber和@CouponNumber不得同时为空。两者是 and 的关系
**  Modify by: Gavin @2015-01-15 (ver 1.0.0.3) 当频繁查询Coupon时,Update Coupon会造成互相之间的影响. 改进: 先读出Coupon, Update时加上判断,只从0 改成1
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(max), @TempStr varchar(64)
  declare @CouponList table(CouponNumber varchar(64), IsRead int, IsFavorite int)

  set @CardNumber = isnull(@CardNumber, '')
  set @CouponNumber = isnull(@CouponNumber, '')
  if @CardNumber = '' and @CouponNumber = ''
    return -8
  if @CouponNumber <> ''
    set @TempStr = '1'
  else
  begin
    set @TempStr = ''
    set @CouponNumber = ''''''
  end   
   
  set @SQLStr = ' select CouponNumber, IsRead, IsFavorite from Coupon '
       + ' where (CardNumber = ''' + @CardNumber + ''' or ''' + @CardNumber + ''' = '''') '
       + ' and (CouponNumber in (' + @CouponNumber + ') or ''' + @TempStr + ''' = '''') '            
  insert into @CouponList exec sp_executesql @SQLStr
    
  if @FlagType = 1
    update Coupon Set IsRead = @Value where CouponNumber in
      (select CouponNumber from @CouponList where IsRead <> @Value) 

  if @FlagType = 2 
    Update Coupon Set IsFavorite = @Value where CouponNumber in
      (select CouponNumber from @CouponList where IsFavorite <> @Value)  
   
  return 0
end

GO
