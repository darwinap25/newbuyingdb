USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewDepartment_IsOnlineSKU]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewDepartment_IsOnlineSKU]
AS
/*
*/
	select * from department 
	where departcode in	(select distinct DepartCode from product where IsOnlineSKU = 1)
	  or departcode in	(select distinct SUBSTRING(DepartCode,1,4) from product where IsOnlineSKU = 1)
	  or departcode in	(select distinct SUBSTRING(DepartCode,1,2) from product where IsOnlineSKU = 1)

GO
