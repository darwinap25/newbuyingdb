USE [NewBuying]
GO
/****** Object:  Table [dbo].[RelationUserUser]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RelationUserUser](
	[UserName] [varchar](50) NOT NULL,
	[SubUserName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_RELATIONUSERUSER] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC,
	[SubUserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts_Users表UserName' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RelationUserUser', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts_Users表UserName' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RelationUserUser', @level2type=N'COLUMN',@level2name=N'SubUserName'
GO
