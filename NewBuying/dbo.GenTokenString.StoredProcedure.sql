USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenTokenString]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenTokenString]
  @UserID       int,
  @Token        varchar(20) output
AS
/****************************************************************************
**  Name : GenTokenString
**  Version: 1.0.0.1
**  Description : 产生一个token。（随机字符串）
**  Example :
  declare @Token varchar(20), @A int
  exec @A = GenTokenString 1, @Token output
  print @A
  print @Token 
**  Created by: Gavin @2016-11-03
**  Modify by: Gavin @2017-01-06
**
****************************************************************************/
BEGIN
  declare @A int, @PWD varchar(512), @PWD_Encrypt varchar(512), @PWDMask varchar(512), @PWDPattern varchar(512), @PWDLength int, @PWDStyle int
  set @PWDMask = '999999'
  set @PWDPattern = '' 
  set @PWDLength = 20
  set @PWDStyle = 0
  exec @A = GenRandomPassword @PWDMask, @PWDPattern, @PWDLength, @PWDStyle, @PWD output, @PWD_Encrypt output
  if @A = 0
  begin
    set @Token = @PWD
  end
  return @a	 
END

GO
