USE [NewBuying]
GO
/****** Object:  Table [dbo].[AfterSales_T]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AfterSales_T](
	[SeqNo] [int] NOT NULL,
	[TxnNo] [varchar](64) NOT NULL,
	[TenderID] [int] NOT NULL,
	[TenderCode] [varchar](64) NOT NULL,
	[TenderDesc] [nvarchar](512) NULL,
	[TenderType] [int] NOT NULL,
	[TenderAmount] [dbo].[Buy_Amt] NOT NULL,
	[LocalAmount] [dbo].[Buy_Amt] NOT NULL,
	[ExchangeRate] [decimal](12, 4) NULL,
	[PaymentType] [int] NULL,
	[Additional] [nvarchar](512) NULL,
	[Status] [int] NULL,
	[PayConfirmDate] [datetime] NULL,
	[CardNumber] [varchar](64) NULL,
	[CardType] [varchar](64) NULL,
	[CardHolder] [varchar](64) NULL,
	[CardApprovalCode] [varchar](64) NULL,
	[CardExpiryDate] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_AFTERSALES_T] PRIMARY KEY CLUSTERED 
(
	[TxnNo] ASC,
	[SeqNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AfterSales_T] ADD  DEFAULT ((0)) FOR [TenderAmount]
GO
ALTER TABLE [dbo].[AfterSales_T] ADD  DEFAULT ((0)) FOR [LocalAmount]
GO
ALTER TABLE [dbo].[AfterSales_T] ADD  DEFAULT ((0)) FOR [PaymentType]
GO
ALTER TABLE [dbo].[AfterSales_T] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[AfterSales_T] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单中支付序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'SeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'TxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'TenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'TenderDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型（1是本币，其他暂定。）：  
0:  Type, This record just Type
1:  Local Cash  (只能有一个)
2:  Foreign Cash
3:  Cheque
4:  Credit Card
5:  Debit Card
6:  EPS Card
7:  Coupon
8:  Credit Card Installment
9:  Finance House Installment
10: On Account
11: On Account Inter Client
12: Credit Card Group
15: Burn Point
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'TenderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付货币金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'TenderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付货币金额 转换为 本币的金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'LocalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币汇率。 LocalAmount = TenderAmount * ExchangeRate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'ExchangeRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付的用途。默认0。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'PaymentType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留，附加信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：无效/支付未确认。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付确认日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'PayConfirmDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡支付时有效。记录卡号。可能需要加密，或者屏蔽No。
Coupon支付时，可以使用此字段记录CouponNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡支付时有效。支付卡的类型/CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'CardType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡支付时有效。卡持有人姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'CardHolder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡支付时有效。刷卡验证号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'CardApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡支付时有效。卡有效期，' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'CardExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单支付表。（子表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_T'
GO
