USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMessageAttachment]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMessageAttachment]
  @MemberID				int,          --接收方的会员ID
  @MessageID            int,          --消息ID
  @FileName             varchar(64),  --附件文件名
  @KeyID                int           --指定KeyID  
AS
/****************************************************************************
**  Name : GetMessageAttachment
**  Version: 1.0.0.0
**  Description : 获取会员附件 （必须有会员ID）
**
**  Parameter :
  declare @a int, @MessageID int
  exec @a = GetMessageAttachment 2,0,'', 0
  print @a  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin

  select KeyID, MessageID, MemberID, AttachFileName, AttachFilePath, AttachFileBinaryContent from MessageAttachment
  where MemberID = @MemberID 
    and (MessageID = @MessageID or isnull(@MessageID, 0) = 0)
    and (AttachFileName = @FileName or isnull(@FileName, '') = '')
    and (KeyID = @KeyID or isnull(@KeyID, 0) = 0)
    
  return 0    
end

GO
