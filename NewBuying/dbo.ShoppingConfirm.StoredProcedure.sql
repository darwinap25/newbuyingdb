USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ShoppingConfirm]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[ShoppingConfirm] 
  @TxnNo                varchar(64),       -- 交易单号
  @Type                 int,               -- confirm类型。 1：确认付款完成。 2：确认已送货。 3: 确认已收货（交易完成）
  @DeliveryNumber       varchar(512),
  @LogisticsProviderID  int
AS
/****************************************************************************
**  Name : ShoppingConfirm
**  Version: 1.0.0.4
**  Description : 交易单送货确认。（此版本用于bauhaus，使用的LINKSERVER DB是LINKBUY.Buying_621.Test）
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = ShoppingConfirm 'KTXNNO000000499', 1, '402055732846', 56
  print @a  
  print @count
  print @recordcount
**  Created by: Gavin @2013-02-20
**  Modify by: Gavin @2016-09-20 (ver 1.0.0.1) 去掉影响行数的判断，增加status为7的处理。
**  Modify by: Gavin @2017-03-20 (ver 1.0.0.2) 增加检查buying DB 的sales 交易的 状态。
**  Modify by: Gavin @2017-03-21 (ver 1.0.0.3) 增加发送消息的功能。
**  Modify by: Gavin @2017-04-18 (ver 1.0.0.4) sales_H判定状态改变。 12 是出库。 所以sales_H status
**
****************************************************************************/
begin
  declare @status int, @Newstatus int, @OldDeliveryNumber varchar(512), @OldLogisticsProviderID int,
          @MemberID int, @CardNumber varchar(64), @BuyingSalesStatus int 
  select top 1 @status = status, @OldDeliveryNumber = DeliveryNumber, @OldLogisticsProviderID = LogisticsProviderID,
        @MemberID = MemberID, @CardNumber = CardNumber
    from sales_H where TransNum = @TxnNo
  if isnull(@OldDeliveryNumber, '') = '' 
    set @OldDeliveryNumber = @DeliveryNumber
  if isnull(@OldLogisticsProviderID, 0) = 0 
    set @OldLogisticsProviderID = @LogisticsProviderID       
  
  -- ver 1.0.0.2
  select @BuyingSalesStatus = Status from Sales_H where TransNum=@TxnNo
  set @BuyingSalesStatus = isnull(@BuyingSalesStatus, 0)

  if (@status not in (3, 4, 6, 7, 8, 9, 13, 14))
    return -11
  if @Type = 1 and @status = 3
    set @Newstatus = 4
  if @Type = 2 and @status = 4
    set @Newstatus = 6 
  if @Type = 3 and (@status in (6, 7)) and (@BuyingSalesStatus in (5,6,8,14))   -- ver 1.0.0.2 
    set @Newstatus = 5
  if @Newstatus is null
    return -11		         
  update sales_h set status = @Newstatus,  DeliveryNumber = @OldDeliveryNumber, LogisticsProviderID = @OldLogisticsProviderID 
    where TransNum = @TxnNo

  -- 发送消息。
  Declare @OprID int, @CardTypeID int, @CardGradeID int, @MsgCardBrandID int

  if @Type = 1 
    set @OprID = 519
  else if @Type = 2 
    set @OprID = 520
  else if @Type = 3
    set @OprID = 521

  select @CardTypeID = C.CardTypeID, @CardGradeID = C.CardGradeID, @MsgCardBrandID = T.BrandID 
  from card C left join CardType T on C.CardTypeID = T.CardTypeID
  where CardNumber = @CardNumber

  exec GenBusinessMessage @OprID, @MemberID, @CardNumber, '', @CardTypeID, @CardGradeID, @MsgCardBrandID, 0, ''  
 return 0   
end

GO
