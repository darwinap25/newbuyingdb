USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardOrderForm_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardOrderForm_H](
	[CardOrderFormNumber] [varchar](64) NOT NULL,
	[PurchaseType] [int] NULL,
	[OrderType] [int] NULL,
	[BrandID] [int] NULL,
	[FromStoreID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[CustomerType] [int] NULL,
	[CustomerID] [int] NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[FromAddress] [nvarchar](512) NULL,
	[StoreContactName] [nvarchar](512) NULL,
	[StoreContactPhone] [nvarchar](512) NULL,
	[StoreContactEmail] [nvarchar](512) NULL,
	[StoreMobile] [nvarchar](512) NULL,
	[FromContactName] [nvarchar](512) NULL,
	[FromContactNumber] [nvarchar](512) NULL,
	[FromEmail] [nvarchar](512) NULL,
	[FromMobile] [nvarchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[Remark1] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_CARDORDERFORM_H] PRIMARY KEY CLUSTERED 
(
	[CardOrderFormNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CardOrderForm_H] ADD  DEFAULT ((1)) FOR [PurchaseType]
GO
ALTER TABLE [dbo].[Ord_CardOrderForm_H] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[Ord_CardOrderForm_H] ADD  DEFAULT ((2)) FOR [CustomerType]
GO
ALTER TABLE [dbo].[Ord_CardOrderForm_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CardOrderForm_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CardOrderForm_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CardOrderForm_H] ON [dbo].[Ord_CardOrderForm_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CardOrderForm_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2015-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @PurchaseType int, @ordernumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @PurchaseType = PurchaseType, @ordernumber = CardOrderFormNumber FROM INSERTED  
  if ISNULL(@PurchaseType, 1) = 1
    exec GenUserMessage @CreatedBy, 2, 0, @ordernumber
  else exec GenUserMessage @CreatedBy, 21, 0, @ordernumber
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CardOrderForm_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CardOrderForm_H] ON [dbo].[Ord_CardOrderForm_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CardOrderForm_H
* Version: 1.0.0.5
* Description : Card订单表的批核触发器
*   select * from Ord_CardOrderForm_H
* Create By Gavin @2012-09-21
** Modify by Gavin @2014-10-09 (ver 1.0.0.1) 类似Update_Ord_CardOrderForm_H改写
** Modify by Gavin @2015-02-11 (ver 1.0.0.2) 增加PurchaseType的区分。 PurchaseType=1 是原有的card库存订单。 PurchaseType=2是成card购买充值订单
** Modify by Gavin @2015-05-07 (ver 1.0.0.3) GenPickupCardOrder_Purchase 过程 在 A -> C 时调用
** Modify by Gavin @2015-07-20 (ver 1.0.0.4) 在approve时 就增加检查, 两边store 都必须要有cardnumber
** Modify by Gavin @2015-10-09 (ver 1.0.0.5) 增加消息发送
*/
/*==============================================================*/
BEGIN  
  declare @CardOrderFormNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @RecOrdStatus char(1), @PurchaseType int
  declare @FromStoreID int, @StoreID int
  
  DECLARE CUR_Ord_CardOrderForm_H CURSOR fast_forward FOR
    SELECT CardOrderFormNumber, ApproveStatus, CreatedBy, isnull(PurchaseType,1), FromStoreID, StoreID FROM INSERTED
  OPEN CUR_Ord_CardOrderForm_H
  FETCH FROM CUR_Ord_CardOrderForm_H INTO @CardOrderFormNumber, @ApproveStatus, @CreatedBy, @PurchaseType, @FromStoreID, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CardOrderFormNumber = @CardOrderFormNumber
    --  从 A 到 C 时，才产生Picking单。（for RRG）
    if (@OldApproveStatus = 'P' or ISNULL(@OldApproveStatus,'')='' ) and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      if @PurchaseType = 1
      begin
        exec GenApprovalCode @ApprovalCode output    
        update Ord_CardOrderForm_H set ApprovalCode = @ApprovalCode, UpdatedOn = GETDATE(), ApproveOn = GETDATE()
          where CardOrderFormNumber = @CardOrderFormNumber
      
        exec ChangeCardStockStatus 3, @CardOrderFormNumber, 1 
      end else if @PurchaseType = 2
      begin
        if exists(select * from 
                    (select * from Ord_CardOrderForm_D where CardOrderFormNumber = @CardOrderFormNumber) A 
                     left join (select * from Card where IssueStoreID = @FromStoreID) B 
                     on A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID
                  where isnull(B.CardNumber,'') = '')
        begin  
          RAISERROR ('No CardNumber, Order can''t be approve!', 16, 1)
          if (@@Trancount > 0)
            ROLLBACK TRANSACTION        
          return 
        end    
        if exists(select * from 
                    (select * from Ord_CardOrderForm_D where CardOrderFormNumber = @CardOrderFormNumber) A 
                     left join (select * from Card where IssueStoreID = @StoreID) B 
                     on A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID
                  where isnull(B.CardNumber,'') = '')
        begin  
          RAISERROR ('No CardNumber, Order can''t be approve!', 16, 1)
          if (@@Trancount > 0)
            ROLLBACK TRANSACTION        
          return 
        end 
                   
        update Ord_CardOrderForm_H set ApprovalCode = @ApprovalCode, UpdatedOn = GETDATE(), ApproveOn = GETDATE()
          where CardOrderFormNumber = @CardOrderFormNumber
      end  
      
      if ISNULL(@PurchaseType, 1) = 1
         exec GenUserMessage @CreatedBy, 2, 1, @CardOrderFormNumber
      else exec GenUserMessage @CreatedBy, 21, 1, @CardOrderFormNumber 
           
    end
    if (@OldApproveStatus = 'A') and @ApproveStatus = 'C' and Update(ApproveStatus)
    begin  
      if @PurchaseType = 1
      begin     
        exec GenPickupOrder_Card @CreatedBy, @CardOrderFormNumber, 1
        update Ord_CardOrderForm_H set UpdatedOn = GETDATE() where CardOrderFormNumber = @CardOrderFormNumber
        exec ChangeCardStockStatus 3, @CardOrderFormNumber, 3  
      end else if @PurchaseType = 2
      begin
        exec GenPickupCardOrder_Purchase @CreatedBy, @CardOrderFormNumber
        update Ord_CardOrderForm_H set UpdatedOn = GETDATE(), ApproveOn = GETDATE()
          where CardOrderFormNumber = @CardOrderFormNumber      
      end          
    end
    
    if @ApproveStatus = 'V' and Update(ApproveStatus) 
    begin
      if (@OldApproveStatus = 'A') or (@OldApproveStatus = 'C')
      begin
        select @RecOrdStatus = ApproveStatus from Ord_CardPicking_H where ReferenceNo = @CardOrderFormNumber  
        set @RecOrdStatus = isnull(@RecOrdStatus, 'V')
        if @RecOrdStatus = 'R' or @RecOrdStatus = 'P'
        begin 
          update Ord_CardPicking_H set ApproveStatus = 'V' where ReferenceNo = @CardOrderFormNumber  
          -- 库存数量变动
          exec ChangeCardStockStatus 3, @CardOrderFormNumber, 2            
        end
        else if isnull(@RecOrdStatus, 'V') = 'V'
        begin
          -- 库存数量变动
          exec ChangeCardStockStatus 3, @CardOrderFormNumber, 2           
        end else
        begin
          RAISERROR ('Order can''t be voided!', 16, 1)
          ROLLBACK TRANSACTION      
        end           
      end  
    end 
    
    FETCH FROM CUR_Ord_CardOrderForm_H INTO @CardOrderFormNumber, @ApproveStatus, @CreatedBy, @PurchaseType, @FromStoreID, @StoreID   
  END
  CLOSE CUR_Ord_CardOrderForm_H 
  DEALLOCATE CUR_Ord_CardOrderForm_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'CardOrderFormNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货种类。1：实体卡订货。 2： 指定卡购买金额或者积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'PurchaseType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源方店铺主键，一般可能是总部' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货方店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户类型（备用，由大客户引起订单时填写）。1：客戶訂貨。 2：店鋪訂貨
默认2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'CustomerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'customer表 主键， 外键.（有大客户时填写）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network，5：快递送货（实体Coupon）。
1到4 是虚拟Coupon。  5 是实体Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址（店铺地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void。 C:Picked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡订货单
@2014-09-17 改成和Ord_CouponOrderForm_H一样的结构
@2015-02-11 统一增加PurchaseType 字段, 用于区分 金额/积分类的 订单 或者是 实体卡的 订单' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_H'
GO
