USE [NewBuying]
GO
/****** Object:  Table [dbo].[iBeacon]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iBeacon](
	[iBeaconID] [int] IDENTITY(1,1) NOT NULL,
	[iBeaconCode] [varchar](64) NOT NULL,
	[iBeaconName] [varchar](64) NOT NULL,
	[iBeaconUuid] [varchar](64) NULL,
	[iBeaconMajor] [int] NULL,
	[iBeaconMinor] [int] NULL,
	[BrandID] [int] NULL,
	[AreaID] [int] NULL,
	[AreaX] [int] NULL,
	[AreaY] [int] NULL,
	[Longitude] [varchar](64) NULL,
	[Latitude] [varchar](64) NULL,
	[Battery] [decimal](12, 2) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_IBEACON] PRIMARY KEY CLUSTERED 
(
	[iBeaconID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'iBeaconID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'iBeaconCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'iBeaconName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备编号 uuid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'iBeaconUuid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备编号Major' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'iBeaconMajor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备编号minor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'iBeaconMinor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Brand表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区域ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'AreaID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'在Area中的X方向标号。（不应超过指定AreaID的MaxX）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'AreaX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'在Area中的Y方向标号。（不应超过指定AreaID的MaxY）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'AreaY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备的经度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'Longitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备的纬度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'Latitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电池余量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon', @level2type=N'COLUMN',@level2name=N'Battery'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeacon 主档表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon'
GO
