USE [NewBuying]
GO
/****** Object:  Table [dbo].[SODEOD]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SODEOD](
	[BusDate] [date] NOT NULL,
	[StoreCode] [varchar](64) NOT NULL,
	[SOD] [int] NULL DEFAULT ((0)),
	[EOD] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_SODEOD] PRIMARY KEY CLUSTERED 
(
	[BusDate] ASC,
	[StoreCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日期。（没有时间）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SODEOD', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SODEOD', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOD状态。0：未SOD。 1：已经SOD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SODEOD', @level2type=N'COLUMN',@level2name=N'SOD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOD状态。0：未EOD。 1：已经EOD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SODEOD', @level2type=N'COLUMN',@level2name=N'EOD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SODEOD', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SODEOD表，记录当前Busdate
@2016-01-07 增加StoreCode， busdate 根据storecode 分配。 不同store可以有不同busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SODEOD'
GO
