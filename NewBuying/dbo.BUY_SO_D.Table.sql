USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_SO_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_SO_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[POCode] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[Cost] [dbo].[Buy_Amt] NULL,
	[AverageCost] [dbo].[Buy_Amt] NULL,
	[UnitCost] [dbo].[Buy_Amt] NULL,
	[Qty] [dbo].[Buy_Qty] NULL,
	[FreeQty] [dbo].[Buy_Qty] NULL,
	[GRNQty] [dbo].[Buy_Qty] NULL,
 CONSTRAINT [PK_BUY_SO_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'POCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单中此货品总成本 （Unit_Cost*Qty*(100-discount)/100）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'Cost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此货品平均成本 （Unit_Cost*Qty/(Qty+Get) *(100-discount)/100）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'AverageCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单位成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'UnitCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'免费货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'FreeQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收到货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D', @level2type=N'COLUMN',@level2name=N'GRNQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺订单明细' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_D'
GO
