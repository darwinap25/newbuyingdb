USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoTelcoTransfer]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoTelcoTransfer]
  @UserID                 int,             --操作员ID
  @CardPickingNumber      varchar(64)     --卡创建单的号码
AS
/****************************************************************************
**  Name : DoTelcoTransfer
**  Version: 1.0.0.3
**  Description : 根据拣货单转移Card 金额 (telco card 的充值订单)
**  example :
  declare @a int  , @CardOrderFormNumber varchar(64)
  set @CardOrderFormNumber = 'COPOCP000000006'
  exec @a = DoTelcoTransfer 1, @CardOrderFormNumber  
  print @a  
  select * from Ord_CardOrderForm_H
    select * from Ord_CardOrderForm_D
  select * from Ord_CardPicking_H
  select * from Ord_CardPicking_D

**  Created by: Gavin @2015-02-11
**  Modify by: Gavin @2015-04-08 (Ver 1.0.0.1) 返回MemberCardTransfer 的执行结果. 取卡号时,加上Top 1
**  Modify by: Gavin @2015-04-20 (Ver 1.0.0.2) 中 ord_cardpicking_D中已经写入cardnumber了，所以这里直接取表中的卡号
**  Modify by: Gavin @2015-06-16 (Ver 1.0.0.3) 一个单子中,多个detail时, 任何一个detail出错,都要返回.
**
****************************************************************************/
begin
  declare @OprID int, @SourceCardNumber varchar(64), @DestCardNumber varchar(64), @BusDate datetime, @TxnDate datetime
  declare @ActAmount money, @ActPoints int, @StoreCode varchar(64), @RegisterCode varchar(64), @ServerCode varchar(64)
  declare @CreatedBy int,  @SourceCardAmount money, @SourceCardPoints int, @TargetCardAmount money, @TargetCardPoints int
  declare @FromStoreID int, @StoreID int, @CardTypeID int, @CardGradeID int
  declare @a int
  
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()    
  set @OprID = 48
  
  DECLARE CUR_DoTelcoTransfer CURSOR fast_forward local FOR
    select H.CreatedBy, H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, D.PickAmount, D.PickPoint,
       D.FirstCardNumber, D.EndCardNumber
     from Ord_CardPicking_D D 
      left join Ord_CardPicking_H H on H.CardPickingNumber = D.CardPickingNumber
      where H.CardPickingNumber = @CardPickingNumber
  OPEN CUR_DoTelcoTransfer
  FETCH FROM CUR_DoTelcoTransfer INTO @CreatedBy, @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @ActAmount, @ActPoints,
    @SourceCardNumber, @DestCardNumber
  WHILE @@FETCH_STATUS=0
  BEGIN 
    select @StoreCode = StoreCode from store where StoreID = @FromStoreID
--    select Top 1 @SourceCardNumber = CardNumber from Card where IssueStoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
--    select Top 1 @DestCardNumber = CardNumber 
--      from Card where IssueStoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
    exec @a= MemberCardTransfer @CreatedBy, @SourceCardNumber, @DestCardNumber, @StoreCode, 
       @RegisterCode, @ServerCode, '', @CardPickingNumber, @CardPickingNumber, @BusDate, @TxnDate, @OprID, @ActAmount, @ActPoints, '', '',
       null, 'Do Telco Transfer',  @SourceCardAmount output, @SourceCardPoints output, @TargetCardAmount output, @TargetCardPoints output 
    if @a <> 0
      break
         
    FETCH FROM CUR_DoTelcoTransfer INTO @CreatedBy, @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @ActAmount, @ActPoints,
       @SourceCardNumber, @DestCardNumber
  END
  CLOSE CUR_DoTelcoTransfer
  DEALLOCATE CUR_DoTelcoTransfer 
           
  return @a
end

GO
