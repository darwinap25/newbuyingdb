USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberMessageAccountToken]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[UpdateMemberMessageAccountToken]
  @MessageAccountID     int,
  @TokenUID             varchar(64),
  @TokenStr             varchar(512)  
AS
/****************************************************************************
**  Name : UpdateMemberMessageAccountToken
**  Version: 1.0.0.0
**  Description : 更新用户消息的指定账号的Token
**
**  Parameter :
  exec UpdateMemberMessageAccountToken 1, 1, 170, 1, 1, '234234342', 1, 'test'
  select * from MemberMessageAccount where memberid = 170
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-11-21
**MemberClause
****************************************************************************/
begin 
  update MemberMessageAccount 
	  set TokenUID = @TokenUID, TokenStr = @TokenStr, TokenUpdateDate = GetDate()
    where MessageAccountID = @MessageAccountID  
  if @@ROWCOUNT <> 0
    return 0 
  else return -1 
end

GO
