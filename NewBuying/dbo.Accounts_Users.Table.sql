USE [NewBuying]
GO
/****** Object:  Table [dbo].[Accounts_Users]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accounts_Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [binary](20) NOT NULL,
	[TrueName] [varchar](50) NULL,
	[Sex] [char](2) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[EmployeeID] [int] NULL,
	[DepartmentID] [varchar](15) NULL,
	[Activity] [bit] NULL,
	[UserType] [char](2) NULL,
	[Style] [int] NULL,
	[WeChatUID] [varchar](64) NULL,
	[SSCode] [char](3) NOT NULL,
	[Description] [nvarchar](200) NULL,
 CONSTRAINT [PK_Accounts_Users] PRIMARY KEY NONCLUSTERED 
(
	[UserName] ASC,
	[SSCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'微信的唯一ID号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts_Users', @level2type=N'COLUMN',@level2name=N'WeChatUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts_Users', @level2type=N'COLUMN',@level2name=N'SSCode'
GO
