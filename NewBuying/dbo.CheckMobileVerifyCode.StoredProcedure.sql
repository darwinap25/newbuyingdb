USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckMobileVerifyCode]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckMobileVerifyCode]
  @MemberID             int,                    --会员ID  
  @CountryCode          varchar(64),            --手机号码的国家码  
  @MobileNo             varchar(64),            --手机号码. (不带国家码)  
  @MobileVerifyCode     varchar(64)             -- 手机校验码  
as  
/****************************************************************************
**  Name : 验证手机校验码
**  Version: 1.0.0.1
**  Description : 
**  Example :
    declare @a int
    exec @a = CheckMobileVerifyCode 713, '086', '15921992301', '3434'
    print @a
**  Created by: Gavin @2014-08-23
**  Modify by: Gavin @2015-02-25 有效时间由原来的10分钟， 改为 12 个小时。精度还是分钟
**
****************************************************************************/
begin
  declare @MobileVerifyCodeStamp datetime, @VerifyCode varchar(64)
  declare @VerCodeValid int  -- 校验码有效时间,单位分钟
  --set @VerCodeValid = 10     -- hardcode 有效时间 10 分钟
  set @VerCodeValid = 12 * 60     -- hardcode 有效时间 12 小时
    
  select @MobileVerifyCodeStamp = MobileVerifyCodeStamp, @VerifyCode = MobileVerifyCode 
    from Member where MemberID = @MemberID
  
  if datediff(mi, @MobileVerifyCodeStamp, getdate()) <= @VerCodeValid    -- 校验码在有效期中
  begin
    if @MobileVerifyCode = @VerifyCode   -- 校验码正确
    begin
      update Membermessageaccount set VerifyFlag = 1 
        where MemberID = @MemberID and AccountNumber = isnull(@CountryCode,'') + RTrim(isnull(@MobileNo,'')) and MessageServiceTypeID = 2 
      update Member set MobileVerifyCode = '' where MemberID = @MemberID          
    end else      
      return -201  -- 校验码不正确
  end else
    return -202  --  校验码已经过期
           
  return 0
end

GO
