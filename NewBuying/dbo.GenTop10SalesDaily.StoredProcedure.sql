USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenTop10SalesDaily]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GenTop10SalesDaily]
  @Busdate datetime=0
AS
/****************************************************************************
**  Name : GenTop10SalesDaily    
**  Version: 1.0.0.1
**  Description : 产生当日交易的销售货品合计记录，以供报表查询. （每日EOD时定时执行）
**
  declare @a int
  exec @a = GenTop10SalesDaily
  print @a
			select * from product where hotsaleflag = 1
**  Created by: Gavin @2013-07-11
**  Modified by: Gavin @2013-07-30 (ver 1.0.0.1) 同时update product 的 hostsaleFlag标志, 按照最近7天的销售数量排名前10

**
****************************************************************************/
begin 
  declare @StartDate datetime
  set @StartDate = cast(Getdate() as int) - 7
   
  if isnull(@Busdate, 0) = 0
    select Top 1 @Busdate = Busdate from sodeod order by CreatedOn desc
  
  if not exists (select 1 from  sysobjects where  id = object_id('Top10Record') and type = 'U')
  begin
    select * into Top10Record from
      (select @Busdate as Busdate, A.ProdCode, max(P.DepartCode) as DepartCode, max(P.ProductBrandID) as ProductBrandID, sum(A.Qty) as Qty
        from (select H.Busdate, D.ProdCode, D.Qty from Sales_D D Left join Sales_H H on H.TransNum = D.TransNum where H.Busdate = @Busdate) A 
        left join Product P on A.ProdCode = P.ProdCode        
      group by A.ProdCode) S
  end else
  begin
    delete from Top10Record where Busdate = @Busdate
	insert into Top10Record(Busdate, ProdCode, DepartCode, ProductBrandID, Qty)
	select @Busdate as Busdate, A.ProdCode, max(P.DepartCode) as DepartCode, max(P.ProductBrandID) as ProductBrandID, sum(A.Qty) as Qty
        from (select H.Busdate, D.ProdCode, D.Qty from Sales_D D Left join Sales_H H on H.TransNum = D.TransNum where H.Busdate = @Busdate) A 
        left join Product P on A.ProdCode = P.ProdCode        
      group by A.ProdCode
  end
  
  -- ver 1.0.0.1 calc hotsale product
  update Product set HotSaleFlag = 0 
  update Product set HotSaleFlag = 1 
    from Product P 
     left join (select top 10 prodcode,sum(qty) as qty from Top10Record 
                  where busdate > @StartDate 
                 group by prodcode order by Qty desc) A on P.Prodcode = A.Prodcode
      where A.Prodcode is not null
  return 0
end

GO
