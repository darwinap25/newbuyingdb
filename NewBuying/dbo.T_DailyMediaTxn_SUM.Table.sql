USE [NewBuying]
GO
/****** Object:  Table [dbo].[T_DailyMediaTxn_SUM]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_DailyMediaTxn_SUM](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[Media] [int] NULL,
	[MediaType] [int] NULL,
	[BusDate] [datetime] NULL,
	[BrandID] [int] NULL,
	[StoreID] [int] NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[OprID] [int] NULL,
	[TxnType] [int] NULL,
	[TxnNo] [varchar](64) NULL,
	[Amount] [money] NULL,
	[MediaNumber] [varchar](64) NULL,
	[MediaID] [varchar](64) NULL,
	[Qty] [int] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[Reason] [varchar](64) NULL,
 CONSTRAINT [PK_T_DAILYMEDIATXN_SUM] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:coupon. 2:card' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxn_SUM', @level2type=N'COLUMN',@level2name=N'Media'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BI 报表使用， 存储内容改为 ID， name 用另外的表保存。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxn_SUM'
GO
