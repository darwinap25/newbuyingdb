USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoAllocationAfterHQReceive]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[AutoAllocationAfterHQReceive]
  @ReceiveOrderNumber varchar(64)
AS
/****************************************************************************
**  Name : AutoAllocationAfterHQReceive
**  Version: 1.0.0.0
**  Description : 订单收货后自动分配

    exec AutoAllocationAfterHQReceive
** 
**  Created by:  Gavin @2015-05-08
****************************************************************************/
begin
  declare @CardTypeID int, @CardGradeID int, @HQStoreID int, @OrderAmount money, @CardOrderFormNumber varchar(64),
          @ActualAmount money, @TempAmt money, @ApprovedOn DateTime
  declare @numbertable table(CardOrderFormNumber varchar(64))
  set @TempAmt = 0
  select @ApprovedOn = ApproveOn from Ord_CardReceive_H where CardReceiveNumber = @ReceiveOrderNumber
  
  DECLARE CUR_AutoAllocationAfterHQReceive CURSOR fast_forward local FOR
    select D.CardTypeID, D.CardGradeID, H.StoreID, sum(D.ActualAmount)
      from Ord_CardReceive_D D left join Ord_CardReceive_H H on H.CardReceiveNumber = D.CardReceiveNumber
      where D.CardReceiveNumber = @ReceiveOrderNumber and H.PurchaseType = 2 and H.ReceiveType = 1
    group by D.CardTypeID, D.CardGradeID, H.StoreID
  OPEN CUR_AutoAllocationAfterHQReceive
  FETCH FROM CUR_AutoAllocationAfterHQReceive INTO @CardTypeID, @CardGradeID, @HQStoreID, @ActualAmount
  WHILE @@FETCH_STATUS=0
  BEGIN   
    DECLARE CUR_d CURSOR fast_forward local FOR
      select D.OrderAmount,H.CardOrderFormNumber from ord_cardorderform_D D 
        left join Ord_CardOrderForm_H H on H.CardOrderFormNumber = D.CardOrderFormNumber
        where D.CardTypeID = @CardTypeID and D.CardGradeID = @CardGradeID and H.FromStoreID = @HQStoreID 
          and H.PurchaseType = 2 and H.ApproveStatus = 'A'
          and H.ApproveOn <= @ApprovedOn
        order by H.CreatedOn  
    OPEN CUR_d
    FETCH FROM CUR_d INTO @OrderAmount, @CardOrderFormNumber
    WHILE @@FETCH_STATUS=0
    BEGIN 
      if not exists(select * from @numbertable where CardOrderFormNumber = @CardOrderFormNumber)
      begin
        set @TempAmt = @TempAmt + @OrderAmount
        if @TempAmt <= @ActualAmount 
        begin
          update Ord_CardOrderForm_H set ApproveStatus = 'C' where CardOrderFormNumber = @CardOrderFormNumber        
          insert into @numbertable (CardOrderFormNumber)
          values(@CardOrderFormNumber)
        end 
        else
          break
      end
      FETCH FROM CUR_d INTO @OrderAmount, @CardOrderFormNumber
    END
    CLOSE CUR_d
    DEALLOCATE CUR_d     
    
    FETCH FROM CUR_AutoAllocationAfterHQReceive INTO @CardTypeID, @CardGradeID, @HQStoreID, @ActualAmount
  END
  CLOSE CUR_AutoAllocationAfterHQReceive
  DEALLOCATE CUR_AutoAllocationAfterHQReceive     

end

GO
