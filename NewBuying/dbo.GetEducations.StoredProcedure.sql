USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetEducations]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetEducations]
  @EducationID             int,       -- 主键, 为null或0, 则返回全部
  @LanguageAbbr         varchar(20)=''
AS
/****************************************************************************
**  Name : GetEducations
**  Version: 1.0.0.0
**  Description :查找Education数据
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetNations 1
  print @a  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select EducationID, EducationCode,
     case when @Language = 2 then EducationName2 when @Language = 3 then EducationName3 else EducationName1 end as EducationName   
   from Education where EducationID = @EducationID or isnull(@EducationID, 0) = 0
  return 0  
end

GO
