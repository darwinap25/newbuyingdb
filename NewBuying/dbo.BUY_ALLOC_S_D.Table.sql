USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_ALLOC_S_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_ALLOC_S_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[AllocCode] [varchar](64) NOT NULL,
	[StoreCode] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[Qty] [dbo].[Buy_Qty] NULL,
	[Balance] [dbo].[Buy_Qty] NULL,
	[RequestQty] [dbo].[Buy_Qty] NULL,
	[SuggestQty] [dbo].[Buy_Qty] NULL,
	[Status] [int] NULL,
	[Error] [nvarchar](512) NULL,
	[PostDate] [datetime] NULL,
	[PostBy] [int] NULL,
 CONSTRAINT [PK_BUY_ALLOC_S_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'AllocCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'补充数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺当前持有量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'Balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据补货规程计算出来的请求数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'RequestQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际分配数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'SuggestQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态：
0 :Waittting 
1: Outstanding 
2: Verified 
3: Send to LIS 
4: Complete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'错误信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'Error'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提交日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'PostDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提交人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D', @level2type=N'COLUMN',@level2name=N'PostBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺货品分配，明细表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_ALLOC_S_D'
GO
