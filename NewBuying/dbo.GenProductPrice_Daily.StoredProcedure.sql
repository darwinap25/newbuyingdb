USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenProductPrice_Daily]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenProductPrice_Daily]
AS
/****************************************************************************
**  Name : GenProductPrice_Daily
**  Version : 1.0.0.0
**  Description : 每日产生当前价格。
**
  declare  @a int
  exec @a = GenProductPrice_Daily 
  print @a
  select * from Product_Price
    select * from BUY_PROMO_M
	select * from BUY_RPrice_M

**  Created by Gavin @2017-06-01
****************************************************************************/
BEGIN
  DECLARE @BUSDATA DATETIME
  DECLARE @RP TABLE (ProdCode VARCHAR(64),ProdPriceType INT,StoreCode VARCHAR(64),StoreGroupCode VARCHAR(64),NetPrice MONEY,DefaultPrice MONEY,
    Status INT,StartDate DATETIME,EndDate DATETIME,CreatedOn DATETIME,CreatedBy INT,UpdatedOn DATETIME,UpdatedBy INT)
  DECLARE @PP TABLE (ProdCode VARCHAR(64),ProdPriceType INT,StoreCode VARCHAR(64),StoreGroupCode VARCHAR(64),NetPrice MONEY,DefaultPrice MONEY,
    Status INT,StartDate DATETIME,EndDate DATETIME,CreatedOn DATETIME,CreatedBy INT,UpdatedOn DATETIME,UpdatedBy INT)

  SET @BUSDATA = GETDATE()

  INSERT INTO @RP(ProdCode,ProdPriceType,StoreCode,StoreGroupCode,NetPrice,DefaultPrice,Status,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
  SELECT A.ProdCode,1,StoreCode,StoreGroupCode,Price,RefPrice,1,StartDate,EndDate,GETDATE(),A.UpdatedBy,GETDATE(),A.UpdatedBy 
    FROM BUY_RPrice_M A 
    LEFT JOIN (SELECT ProdCode, MAX(KeyID) IID FROM BUY_RPrice_M
                 WHERE Status = 1 AND DATEDIFF(DD,StartDate, @BUSDATA) >= 0 
				   AND DATEDIFF(DD,@BUSDATA, EndDate) >= 0
               GROUP BY ProdCode ) B ON A.ProdCode = B.ProdCode AND A.KeyID = B.IID
  WHERE B.ProdCode IS NOT NULL

  INSERT INTO @PP(ProdCode,ProdPriceType,StoreCode,StoreGroupCode,NetPrice,DefaultPrice,Status,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
  SELECT A.ProdCode,2,StoreCode,StoreGroupCode,Price,NULL,1,StartDate,EndDate,GETDATE(),A.UpdatedBy,GETDATE(),A.UpdatedBy 
    FROM BUY_PROMO_M A 
    LEFT JOIN (SELECT ProdCode, MAX(KeyID) IID FROM BUY_PROMO_M
                 WHERE Status = 1 AND DATEDIFF(DD,StartDate, @BUSDATA) >= 0 
				   AND DATEDIFF(DD,@BUSDATA, EndDate) >= 0
               GROUP BY ProdCode ) B ON A.ProdCode = B.ProdCode AND A.KeyID = B.IID
  WHERE B.ProdCode IS NOT NULL

  TRUNCATE TABLE Product_Price

  INSERT INTO Product_Price(ProdCode,ProdPriceType,StoreCode,StoreGroupCode,NetPrice,DefaultPrice,Status,StartDate,EndDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
  SELECT A.ProdCode,CASE WHEN ISNULL(B.NetPrice, 0) <> 0 THEN 2 ELSE 1 END,
    A.StoreCode,A.StoreGroupCode,
    CASE WHEN ISNULL(B.NetPrice, 0) <> 0 THEN B.NetPrice ELSE A.NetPrice END,
    A.DefaultPrice,A.Status,A.StartDate,A.EndDate,GETDATE(),A.CreatedBy,GETDATE(),A.UpdatedBy
  FROM @RP A LEFT JOIN @PP B ON A.ProdCode = B.ProdCode

  RETURN 0
END

GO
