USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBoxSaleDetails]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\darwin.pasco
 * Created At: 2017/10/06 07:06:12
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetBoxSaleDetails]
    
    @prodcode       [varchar](64)

/************************************
v 1.0 - Created by Darwin 10/06/2017

- Get all BoxSales Details


************************************/

AS
SET NOCOUNT ON;

BEGIN

    SELECT 
        [b].[BOMCode],     
        [b].[ProdCode],
        [p].[ProdDesc1],        
        [b].[DefaultQty],
        [b].[MinQty],
        [b].[MaxQty]
    FROM [dbo].[BUY_BOXSALE] b
        INNER JOIN [dbo].[BUY_PRODUCT] p
            ON [p].[ProdCode] = [b].[ProdCode]
    WHERE [b].[BOMCode] = @prodcode

END



GO
