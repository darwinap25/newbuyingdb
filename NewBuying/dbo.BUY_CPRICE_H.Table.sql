USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_CPRICE_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_CPRICE_H](
	[CPriceCode] [varchar](64) NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[StoreGroupCode] [varchar](64) NULL,
	[VendorCode] [varchar](64) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CurrencyCode] [varchar](64) NULL,
	[Note] [nvarchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_CPRICE_H] PRIMARY KEY CLUSTERED 
(
	[CPriceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_CPRICE_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_CPRICE_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'CPriceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺Code。空/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。空/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'StoreGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'VendorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码。一般应该是本币代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'CurrencyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成本价头表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_H'
GO
