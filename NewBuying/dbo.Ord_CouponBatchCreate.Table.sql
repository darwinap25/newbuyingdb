USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponBatchCreate]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponBatchCreate](
	[CouponCreateNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[CouponCount] [int] NOT NULL,
	[Note] [nvarchar](512) NULL,
	[IssuedDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[InitAmount] [money] NULL,
	[InitPoints] [int] NULL,
	[RandomPWD] [int] NULL,
	[InitPassword] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NOT NULL,
	[BatchCouponID] [int] NULL,
	[InitStatus] [int] NULL,
	[ReferenceNo] [varchar](64) NULL,
	[StoreTypeID] [int] NULL,
	[StoreID] [int] NULL,
	[OrderType] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CouponNatureID] [int] NULL,
 CONSTRAINT [PK_Ord_CouponBatchCreate] PRIMARY KEY CLUSTERED 
(
	[CouponCreateNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT ((0)) FOR [InitAmount]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT ((0)) FOR [InitPoints]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT ((0)) FOR [RandomPWD]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT ('P') FOR [ApproveStatus]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT ((0)) FOR [InitStatus]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT ((0)) FOR [StoreTypeID]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CouponBatchCreate] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CouponBatchCreate]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CouponBatchCreate] ON [dbo].[Ord_CouponBatchCreate]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CouponBatchCreate
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2014-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @CouponCreateNumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @CouponCreateNumber = CouponCreateNumber FROM INSERTED  
  exec GenUserMessage @CreatedBy, 7, 0, @CouponCreateNumber 
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponBatchCreate]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponBatchCreate] ON [dbo].[Ord_CouponBatchCreate]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponBatchCreate
* Version: 1.0.0.4
* Description : 批量创建卡
*     select * from Ord_CouponBatchCreate where CouponCreateNumber = ' BCC0000000510'
select * from Ord_CardBatchCreate
* Create By Gavin @2012-05-18
* Modify by Gavin @2013-08-30 (ver 1.0.0.2) Ord_CouponBatchCreate表增加InitStatus参数, 传入到创建过程中
* Modify by Gavin @2015-07-30 (ver 1.0.0.3) 因为一个单中有多个批次,所以 BatchCouponID无效了,直接填0
* Modify by Darwin @2017-09-09 (ver 1.0.0.4) Get CouponNatureID from Inserted
*/
/*==============================================================*/
BEGIN
  DECLARE @CouponCreateNumber varchar(512), @CouponTypeID int, @CouponCount int, @CouponNatureID int,@IssuedDate datetime, @ExpiryDate datetime, @InitAmount money, @InitPoints int, @ApproveStatus char(1), @CreatedBy int, @RandomPWD int, @InitPassword varchar(512)
  declare @OldApproveStatus char(1), @ReturnBatchID int, @ReturnStartNumber varchar(512), @ReturnEndNumber varchar(512)
  declare @ApprovalCode char(6), @InitStatus int  
  
  DECLARE CUR_CouponBatchCreate CURSOR fast_forward local FOR
    SELECT CouponCreateNumber, CouponTypeID, CouponCount, CouponNatureID, IssuedDate, ExpiryDate, ApproveStatus, ApproveBy, RandomPWD, InitPassword, InitAmount, InitPoints, InitStatus
     FROM INSERTED
  OPEN CUR_CouponBatchCreate
  FETCH FROM CUR_CouponBatchCreate INTO @CouponCreateNumber, @CouponTypeID, @CouponCount, @CouponNatureID, @IssuedDate, @ExpiryDate, @ApproveStatus, @CreatedBy, @RandomPWD, @InitPassword, @InitAmount, @InitPoints, @InitStatus
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CouponCreateNumber = @CouponCreateNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus) 
    begin
      exec GenApprovalCode @ApprovalCode output 
      exec BatchGenerateNumber @CreatedBy, 1, @CouponTypeID, null, null, @CouponCount, @IssuedDate, @InitAmount, @InitPoints, @RandomPWD, @InitPassword, @ExpiryDate, @CouponCreateNumber, @ApprovalCode, @Createdby, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber OUTPUT, '', @InitStatus, 0, @CouponNatureID     
      update Ord_CouponBatchCreate set BatchCouponID = 0, ApprovalCode = @ApprovalCode where CouponCreateNumber = @CouponCreateNumber
    end      
    FETCH FROM CUR_CouponBatchCreate INTO @CouponCreateNumber, @CouponTypeID, @CouponCount, @CouponNatureID, @IssuedDate, @ExpiryDate, @ApproveStatus, @CreatedBy, @RandomPWD, @InitPassword, @InitAmount, @InitPoints, @InitStatus
  END
  CLOSE CUR_CouponBatchCreate 
  DEALLOCATE CUR_CouponBatchCreate   
END


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建Coupon单号码， 主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'CouponCreateNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'CouponCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'IssuedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'ExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'InitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'InitPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否初始随机密码。0：不是。1:是的。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'RandomPWD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始密码。RandomPWD=1时，不需要填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'InitPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'BatchCouponID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建的Coupon的初始状态。默认0。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'InitStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果是根据其他订单产生的，需要填写相关单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提出创建申请的店铺类型。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提出创建申请的店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。0： 手动。1：自动' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵批量创建表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponBatchCreate'
GO
