USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberFriend]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberFriend](
	[MemberFriendID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[FriendMemberID] [int] NULL,
	[FriendFamilyName] [nvarchar](512) NULL,
	[FriendGivenName] [nvarchar](512) NULL,
	[FriendSex] [int] NULL,
	[MobileNumber] [varchar](512) NULL,
	[CountryCode] [varchar](64) NULL,
	[EMail] [nvarchar](512) NULL,
	[Status] [int] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MEMBERFRIEND] PRIMARY KEY CLUSTERED 
(
	[MemberFriendID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberFriend] ADD  DEFAULT ((0)) FOR [FriendSex]
GO
ALTER TABLE [dbo].[MemberFriend] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[MemberFriend] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[MemberFriend] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'MemberFriendID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'好友ID。（member表的ID）（不是member的成员则可以不填）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'FriendMemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'好友姓名（姓）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'FriendFamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'好友姓名（名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'FriendGivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'好友性别。null或0：保密。1：男性。 2：女性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'FriendSex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'MobileNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码的国家码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮件信箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'EMail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。
0：待审核。 1：好友状态。 2：审核不通过。 3：黑名单 。4：直接删除记录。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'好友表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFriend'
GO
