USE [NewBuying]
GO
/****** Object:  Table [dbo].[Promotion_Gift]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion_Gift](
	[PromotionCode] [varchar](64) NOT NULL,
	[GiftSeq] [int] NOT NULL,
	[PromotionGiftType] [int] NULL DEFAULT ((1)),
	[PromotionValue] [dbo].[Buy_Amt] NULL,
	[PromotionAdjValue] [decimal](12, 4) NULL DEFAULT ((0)),
	[PromotionMultipleValue] [int] NULL DEFAULT ((1)),
 CONSTRAINT [PK_PROMOTION_GIFT] PRIMARY KEY CLUSTERED 
(
	[PromotionCode] ASC,
	[GiftSeq] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Gift', @level2type=N'COLUMN',@level2name=N'GiftSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销实现方式.. 默认1
1：现金返还 (固定金额)
2：现金返还 (消费金额的百分数)  例如  0.1.  消费1000， 返还100
3：赠送Coupon。
4：积分返还 （固定值）
5：积分返还 （消费金额的系数）。 例如 0.1.   消费20， 返还2 分 
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionGiftType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销内容值。（根据PromotionGiftType 中设置，决定内容。）
PromotionGiftType = 5 时， 存放CouponTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销值的调整值。 应对以下情况：
满20￥开始，每10￥加送1 分。  即： 20￥ 1分、30￥ 2分、40￥ 3分......
此时，这里可以填写   -1。  按照线性规则计算得出的积分 - 1 后就是能获得的促销。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionAdjValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励积分时有效（PromotionGiftType= 4,5），计算获得的结果再乘以这个值。
（主要考虑计算结果有小数点会被截掉，这个计算需要在截掉小数点后实际获得积分结果的基础上再乘）
（711 还未使用。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionMultipleValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销礼品表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Gift'
GO
