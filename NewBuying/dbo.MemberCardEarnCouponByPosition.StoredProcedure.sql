USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardEarnCouponByPosition]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[MemberCardEarnCouponByPosition]
  @CardNumber varchar(64),    -- 卡号
  @PositionID varchar(64),    -- 地点编号
  @PositionType varchar(64),  -- 地点定位类型BlueTooth: 1; Mac:2; Defined Position: 3
  @ReturnAmount money output, -- 返回卡金额余额
  @ReturnPoint int output,    -- 返回卡积分余额
  @ReturnCouponNumber varchar(64) output, -- 返回卡积分余额
  @ReturnMessageStr varchar(max) output,	-- 返回信息。（json格式）
  @TxnNo varchar(64)='',         -- 可选，交易编号
  @SN    varchar(64)='',         -- 可选，默认为0，交易序号，当同一笔交易中有多条交易，需要设置交易SN编号
  @Additional varchar(512)='',   -- 可选，附加信息  
  @LanguageAbbr varchar(20)=''	 
AS
/****************************************************************************
**  Name : MemberCardEarnCouponByPosition
**  Version: 1.0.0.2
**  Description : 根据位置(比如提交的iBeaconID)，判断是否给予Coupon。
 
   declare @ReturnAmount money ,@ReturnPoint int ,@ReturnMessageStr varchar(max), @ReturnCouponNumber varchar(64)
   exec MemberCardEarnCouponByPosition '000300933', '112-100', '1',
     @ReturnAmount output, @ReturnPoint output, @ReturnCouponNumber output, @ReturnMessageStr output

**  Created by: Gavin @2015-01-15
**  Modify by: Gavin @2015-01-21 (ver 1.0.0.1) 只返回可以获得的CouponType列表， 不送Coupon
**  Modify by: Gavin @2015-08-05 (ver 1.0.0.2) 因为表ibeacon修改, iBeaconID 已经改成自增长主键,增加了ibeaconcode.所以同步修改SP
**
****************************************************************************/
begin
  declare @Language int, @MemberID int, @count int, @recordcount int, @StoreCode varchar(64), @SC varchar(64)
  declare @StoreID int, @AreaID int, @iBeaconGroupID int, @CouponTypeCode varchar(64), @CouponTypeID int
  declare @EachCoupon varchar(64), @MaxKeyID int, @iBeaconID int
 
  Create Table #Return (Title varchar(512), Association_Type int, Association_ID varchar(64), Assiocation_Code varchar(64), 
    PicFile varchar(512), URL varchar(512), [Desc] varchar(512))
     
  Declare @CanEarnTypes Table (KeyID int, CouponTypeStartDate datetime, CouponTypeEndDate datetime, CouponTypeName1 varchar(512), 
    CouponTypeID int, CouponTypeCode varchar(64), CouponName varchar(512), CampaignID int, CouponTypeNotes varchar(512), 
    CouponTypeLayoutFile varchar(512), ExchangeType int, ExchangeAmount money, ExchangePoint int, ExchangeCouponTypeID int, 
    ExchangeCouponTypeCode varchar(64), ExchangeCouponCount int, CouponTypePicFile varchar(512),CouponCountBalance int, 
    ClauseName varchar(512), ClauseName2 varchar(512), ClauseName3 varchar(512), MemberClauseDesc1 varchar(512), 
    MemberClauseDesc2 varchar(512), MemberClauseDesc3 varchar(512), CouponTypeNatureID int, MemberClauseDesc varchar(512), 
    StoreCode varchar(64))  
  DECLARE @CouponStoreList table(CouponTypeID int, StoreCode varchar(64))
  DECLARE @CouponNumberList table(KeyID int, CouponTypeStartDate datetime, CouponTypeEndDate datetime, CouponTypeName1 varchar(512), 
    CouponTypeID int, CouponTypeCode varchar(64), CouponName varchar(512), CampaignID int, CouponTypeNotes varchar(512), 
    CouponTypeLayoutFile varchar(512), ExchangeType int, ExchangeAmount money, ExchangePoint int, ExchangeCouponTypeID int, 
    ExchangeCouponTypeCode varchar(64), ExchangeCouponCount int, CouponTypePicFile varchar(512),CouponCountBalance int, 
    ClauseName varchar(512), ClauseName2 varchar(512), ClauseName3 varchar(512), MemberClauseDesc1 varchar(512), 
    MemberClauseDesc2 varchar(512), MemberClauseDesc3 varchar(512), CouponTypeNatureID int, MemberClauseDesc varchar(512), CouponNumber varchar(64))  
  DECLARE @icoupontype int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1	
  
  select @MemberID = MemberID, @ReturnAmount = TotalAmount, @ReturnPoint = TotalPoints 
    from Card where CardNumber = @CardNumber	
  select @AreaID = AreaID, @iBeaconID = iBeaconID from iBeacon where iBeaconCode = @PositionID
  select @iBeaconGroupID = iBeaconGroupID from iBeaconGroupMap where iBeaconID = @iBeaconID
  select @icoupontype = AssociationID from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID and AssociationType = 1

  DECLARE CUR_EarchStore CURSOR fast_forward FOR
    select StoreID from AreaStoreMap where AreaID = @AreaID 
  OPEN CUR_EarchStore
  FETCH FROM CUR_EarchStore INTO @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN 
    select @StoreCode = StoreCode from Store where StoreID = @StoreID
    insert into @CanEarnTypes
      (KeyID, CouponTypeStartDate, CouponTypeEndDate, CouponTypeName1, CouponTypeID, CouponTypeCode, CouponName, CampaignID, CouponTypeNotes, 
       CouponTypeLayoutFile, ExchangeType, ExchangeAmount, ExchangePoint, ExchangeCouponTypeID, 
       ExchangeCouponTypeCode, ExchangeCouponCount, CouponTypePicFile,CouponCountBalance, 
       ClauseName, ClauseName2, ClauseName3, MemberClauseDesc1, MemberClauseDesc2, MemberClauseDesc3, CouponTypeNatureID, MemberClauseDesc)
    exec GetMemberCanEarnCouponTypes 0, @CardNumber, @StoreID, '', '', 0, 
      0, 0, 0, @PositionType, '', 1, 0, @count output,
      @recordcount output, '', 0        -- @iBeaconGroupID 取消. 总是0, 在iBeaconGroup表中设置.
    update @CanEarnTypes set StoreCode = @StoreCode
     
 --   insert into @CouponStoreList
--    select CouponTypeID, @StoreCode from @CanEarnTypes   
    
    FETCH FROM CUR_EarchStore INTO @StoreID  
  END
  CLOSE CUR_EarchStore 
  DEALLOCATE CUR_EarchStore 
 
  -- 根据iBeaconGroup中AssociationID的设置，过滤允许发出的coupon类型。目前只有coupon
  
  --select *, '' as CouponNumber from @CanEarnTypes where CouponTypeID = @icoupontype 
--  insert into #Return
--  select CouponName, 1, cast(CouponTypeID as varchar(64)), CouponTypeCode, CouponTypePicFile, '', MemberClauseDesc 
 -- from @CanEarnTypes where CouponTypeID = @icoupontype
 
 --select * from #Return
 
  select CouponName as Title, 1 as Association_Type, cast(CouponTypeID as varchar(64)) as Association_ID, 
    CouponTypeCode as Assiocation_Code, CouponTypePicFile as PicFile, '' as URL, MemberClauseDesc as [Desc] 
  from @CanEarnTypes where CouponTypeID = @icoupontype

 return 0
end

GO
