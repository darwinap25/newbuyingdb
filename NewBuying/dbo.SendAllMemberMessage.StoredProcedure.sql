USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SendAllMemberMessage]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SendAllMemberMessage]
  @UserID               int,
  @SendMemberID			int,
  @MessageServiceTypeID int,
  @MessageType          int,
  @MessagePriority      int,
  @MessageCoding        int,
  @MessageTitle         nvarchar(512),
  @MessageBody          varbinary(Max),
  @IsInternal           int,     
  @MessageID            int output
AS
/****************************************************************************
**  Name : SendAllMemberMessage
**  Version: 1.0.0.0
**  Description : 产生消息数据.
**
**  Parameter :
  declare @a int, @MessageBody  varbinary(Max), @MessageID int, @SendMemberID int, 
    @MessageServiceTypeID int, @MessageType int, @MessagePriority int, @MessageCoding int,  @MessageTitle nvarchar(512),
    @IsInternal int, @ReceiveList varchar(max),  @MessageBodyChar  nvarchar(Max)
  
  set @SendMemberID = 1
  set @MessageServiceTypeID = 2
  set @MessageType  = 1
  set @MessagePriority = 1
  set @MessageCoding = 0
  set @MessageTitle = 'test message'
  set @MessageBodyChar = '发送给AllmemberID'
  set @MessageBody = cast(@MessageBodyChar as varbinary(max)) 
  set @IsInternal = 1   
  exec @a = SendAllMemberMessage 1, @SendMemberID, @MessageServiceTypeID, @MessageType, 
      @MessagePriority, @MessageCoding, @MessageTitle,  @MessageBody, @IsInternal, @MessageID output
  print @a  
  print @MessageID
  print cast(@MessageBody as nvarchar(max))
  
  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin

  declare @TempList varchar(max), @ReceiveMemberID int, @MemberID int, @AccountNumber nvarchar(512)
  declare @MessageStatus int, @ResponseCode int

  -- 内部消息默认就是成功发送的。 
  if @MessageServiceTypeID = 0
  begin
    set @MessageStatus = 2
    set @ResponseCode = 0
  end else set @MessageStatus = 0
  
  -- 插入MessageObject
  insert into MessageObject(MessageServiceTypeID, MessageType, MessagePriority, MessageCoding, MessageTitle, 
      MessageBody, FromMemberID, IsInternal, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ResponseCode) 
  values (@MessageServiceTypeID, @MessageType, @MessagePriority, @MessageCoding, @MessageTitle, 
      @MessageBody, @SendMemberID, @IsInternal, @MessageStatus, Getdate(), @UserID, Getdate(), @UserID, @ResponseCode)    
  set @MessageID = SCOPE_IDENTITY()
   
    insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy)  
    select @MessageID, M.MemberID, M.AccountNumber, 0, 0, getdate(), @UserID 
      from (select MemberID, MessageServiceTypeID, AccountNumber from MemberMessageAccount 
                  where MessageServiceTypeID = @MessageServiceTypeID and status = 1) M                  
    where M.MemberID <> 0 and isnull(M.AccountNumber, '') <> ''
  
  return 0
end

GO
