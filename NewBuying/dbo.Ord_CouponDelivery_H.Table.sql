USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponDelivery_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponDelivery_H](
	[CouponDeliveryNumber] [varchar](64) NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[BrandID] [int] NULL,
	[FromStoreID] [int] NULL,
	[StoreID] [int] NULL,
	[CustomerType] [int] NULL,
	[CustomerID] [int] NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[FromAddress] [nvarchar](512) NULL,
	[StoreContactName] [nvarchar](512) NULL,
	[StoreContactPhone] [nvarchar](512) NULL,
	[StoreContactEmail] [nvarchar](512) NULL,
	[StoreMobile] [nvarchar](512) NULL,
	[FromContactName] [nvarchar](512) NULL,
	[FromContactNumber] [nvarchar](512) NULL,
	[FromEmail] [nvarchar](512) NULL,
	[FromMobile] [nvarchar](512) NULL,
	[NeedActive] [int] NULL DEFAULT ((0)),
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[Remark1] [varchar](512) NULL,
 CONSTRAINT [PK_ORD_COUPONDELIVERY_H] PRIMARY KEY CLUSTERED 
(
	[CouponDeliveryNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CouponDelivery_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CouponDelivery_H] ON [dbo].[Ord_CouponDelivery_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CouponDelivery_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2014-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @CouponDeliveryNumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @CouponDeliveryNumber = CouponDeliveryNumber FROM INSERTED  
  exec GenUserMessage @CreatedBy, 12, 0, @CouponDeliveryNumber
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponDelivery_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponDelivery_H] ON [dbo].[Ord_CouponDelivery_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponDelivery_H
* Version: 1.0.0.4
* Description : delivery order 的触发器, 目的为了填写approvalcode
select * from Ord_CouponDelivery_H
update Ord_CouponDelivery_H set approvestatus = 'A' where CouponDeliveryNumber = 'CODO00000000009'
select count(*) from coupon_movement where oprid = 33
* Create By Gavin @2012-09-21
* Modify By Gavin @2012-09-26 (ver 1.0.0.1) 根据delivery的设置（needactive），是否要激活coupon
* Modify By Gavin @2012-10-11 (ver 1.0.0.2) void 发送单时，需要回滚被issue的 coupon。
* Modify By Gavin @2015-05-20 (ver 1.0.0.3) (RRG)  Ord_CouponDelivery_D大量数据时, 逐条update很耗时,需要批量update. (FirstCouponNumber 和 EndCouponNumber 必须是一样的)
* Modify By Gavin @2015-05-26 (ver 1.0.0.4) change cursor name
*/
/*==============================================================*/
BEGIN  
  declare @CouponDeliveryNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6),
          @NeedActive int
  declare @FirstCoupon varchar(64), @EndCoupon varchar(64), @CouponTypeID int, @ActExpireDate datetime, @NewCouponExpiryDate datetime
  declare @BusDate datetime, @TxnDate datetime, @StoreID int
   
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()
    
  DECLARE CURSOR_Update_Ord_CouponDelivery_H CURSOR fast_forward FOR
    SELECT CouponDeliveryNumber, ApproveStatus, CreatedBy, NeedActive, StoreID FROM INSERTED
  OPEN CURSOR_Update_Ord_CouponDelivery_H
  FETCH FROM CURSOR_Update_Ord_CouponDelivery_H INTO @CouponDeliveryNumber, @ApproveStatus, @CreatedBy, @NeedActive, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CouponDeliveryNumber = @CouponDeliveryNumber
    
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
    
      -- active 发送的coupon 
      DECLARE CUR_CouponDelivery_Detail CURSOR fast_forward for
        select CouponTypeID from Ord_CouponDelivery_D where CouponDeliveryNumber = @CouponDeliveryNumber
          group by CouponTypeID 
	    OPEN CUR_CouponDelivery_Detail
        FETCH FROM CUR_CouponDelivery_Detail INTO @CouponTypeID
        WHILE @@FETCH_STATUS=0 
        BEGIN
          update Coupon set StockStatus = 6, LocateStoreID = @StoreID
            where CouponTypeID = @CouponTypeID 
               and CouponNumber in (select FirstCouponNumber from Ord_CouponDelivery_D 
                                      where CouponDeliveryNumber = @CouponDeliveryNumber and CouponTypeID = @CouponTypeID) 
          if @NeedActive = 1
          begin        
            select top 1 @ActExpireDate = couponexpirydate from coupon where coupontypeid = @CouponTypeID 
            exec CalcCouponNewExpiryDate @CouponTypeID, 33, @ActExpireDate, @NewCouponExpiryDate output 	
            insert into Coupon_Movement
               (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                 BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                OrgExpirydate, OrgStatus, NewStatus)
            select 33, '', CouponNumber, CouponTypeID, null, null, @CouponDeliveryNumber, CouponAmount, 0, CouponAmount,
               @BusDate, @TxnDate, '', '', @CreatedBy, @NewCouponExpiryDate, @ApprovalCode, @StoreID, '', '',
               CouponExpiryDate, 1, 2
              from Coupon where CouponTypeID = @CouponTypeID 
                and CouponNumber in (select FirstCouponNumber from Ord_CouponDelivery_D 
                                        where CouponDeliveryNumber = @CouponDeliveryNumber and CouponTypeID = @CouponTypeID) 
          end      
	        FETCH FROM CUR_CouponDelivery_Detail INTO @CouponTypeID
    	  END
        CLOSE CUR_CouponDelivery_Detail 
        DEALLOCATE CUR_CouponDelivery_Detail 

      exec ChangeCouponStockStatus 7, @CouponDeliveryNumber, 1    
      update Ord_CouponDelivery_H set ApprovalCode = @ApprovalCode, [ApproveOn] = GETDATE() where CouponDeliveryNumber = @CouponDeliveryNumber      
    end
    
    if @ApproveStatus = 'V' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output        
      -- rollback 发送的coupon 
      DECLARE CUR_CouponDelivery_Detail CURSOR fast_forward for
        select CouponTypeID from Ord_CouponDelivery_D where CouponDeliveryNumber = @CouponDeliveryNumber
          group by CouponTypeID 
	    OPEN CUR_CouponDelivery_Detail
      FETCH FROM CUR_CouponDelivery_Detail INTO @CouponTypeID
      WHILE @@FETCH_STATUS=0 
      BEGIN 
          select top 1 @ActExpireDate = couponexpirydate from coupon where coupontypeid = @CouponTypeID 
          exec CalcCouponNewExpiryDate @CouponTypeID, 41, @ActExpireDate, @NewCouponExpiryDate output 	
          insert into Coupon_Movement
             (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
              BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
              OrgExpirydate, OrgStatus, NewStatus)
          select 41, '', CouponNumber, CouponTypeID, null, null, @CouponDeliveryNumber, CouponAmount, 0, CouponAmount,
             @BusDate, @TxnDate, '', '', @CreatedBy, @NewCouponExpiryDate, @ApprovalCode, @StoreID, '', '',
             CouponExpiryDate, 1, 0
            from Coupon where CouponTypeID = @CouponTypeID 
                and CouponNumber in (select FirstCouponNumber from Ord_CouponDelivery_D 
                                        where CouponDeliveryNumber = @CouponDeliveryNumber and CouponTypeID = @CouponTypeID)                
	      FETCH FROM CUR_CouponDelivery_Detail INTO @CouponTypeID
    	END
      CLOSE CUR_CouponDelivery_Detail 
      DEALLOCATE CUR_CouponDelivery_Detail
                 
      update Ord_CouponDelivery_H set ApprovalCode = @ApprovalCode where CouponDeliveryNumber = @CouponDeliveryNumber      
    end
        
/*    
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
    
      -- active 发送的coupon 
      DECLARE CUR_CouponDelivery_Detail CURSOR fast_forward for
        select FirstCouponNumber, EndCouponNumber, CouponTypeID from Ord_CouponDelivery_D where CouponDeliveryNumber = @CouponDeliveryNumber
	    OPEN CUR_CouponDelivery_Detail
        FETCH FROM CUR_CouponDelivery_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
        WHILE @@FETCH_STATUS=0 
        BEGIN
          update Coupon set StockStatus = 6, LocateStoreID = @StoreID
            where CouponTypeID = @CouponTypeID and CouponNumber >= @FirstCoupon and CouponNumber <= @EndCoupon 
          if @NeedActive = 1
          begin        
            select top 1 @ActExpireDate = couponexpirydate from coupon where coupontypeid = @CouponTypeID and CouponNumber = @FirstCoupon
            exec CalcCouponNewExpiryDate @CouponTypeID, 33, @ActExpireDate, @NewCouponExpiryDate output 	
            insert into Coupon_Movement
               (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                 BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                OrgExpirydate, OrgStatus, NewStatus)
            select 33, '', CouponNumber, CouponTypeID, null, null, @CouponDeliveryNumber, CouponAmount, 0, CouponAmount,
               @BusDate, @TxnDate, '', '', @CreatedBy, @NewCouponExpiryDate, @ApprovalCode, @StoreID, '', '',
               CouponExpiryDate, 1, 2
              from Coupon where CouponTypeID = @CouponTypeID and CouponNumber >= @FirstCoupon and CouponNumber <= @EndCoupon 
          end      
	      FETCH FROM CUR_CouponDelivery_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
    	END
        CLOSE CUR_CouponDelivery_Detail 
        DEALLOCATE CUR_CouponDelivery_Detail 

      exec ChangeCouponStockStatus 7, @CouponDeliveryNumber, 1    
      update Ord_CouponDelivery_H set ApprovalCode = @ApprovalCode where CouponDeliveryNumber = @CouponDeliveryNumber      
    end
    
    if @ApproveStatus = 'V' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output        
      -- rollback 发送的coupon 
      DECLARE CUR_CouponDelivery_Detail CURSOR fast_forward for
          select FirstCouponNumber, EndCouponNumber, CouponTypeID from Ord_CouponDelivery_D where CouponDeliveryNumber = @CouponDeliveryNumber
	    OPEN CUR_CouponDelivery_Detail
        FETCH FROM CUR_CouponDelivery_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
        WHILE @@FETCH_STATUS=0 
        BEGIN 
          select top 1 @ActExpireDate = couponexpirydate from coupon where coupontypeid = @CouponTypeID and CouponNumber = @FirstCoupon
          exec CalcCouponNewExpiryDate @CouponTypeID, 41, @ActExpireDate, @NewCouponExpiryDate output 	
          insert into Coupon_Movement
             (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
              BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
              OrgExpirydate, OrgStatus, NewStatus)
          select 41, '', CouponNumber, CouponTypeID, null, null, @CouponDeliveryNumber, CouponAmount, 0, CouponAmount,
             @BusDate, @TxnDate, '', '', @CreatedBy, @NewCouponExpiryDate, @ApprovalCode, @StoreID, '', '',
             CouponExpiryDate, 1, 0
            from Coupon where CouponTypeID = @CouponTypeID and CouponNumber >= @FirstCoupon and CouponNumber <= @EndCoupon     
	      FETCH FROM CUR_CouponDelivery_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
    	END
        CLOSE CUR_CouponDelivery_Detail 
        DEALLOCATE CUR_CouponDelivery_Detail           
      update Ord_CouponDelivery_H set ApprovalCode = @ApprovalCode where CouponDeliveryNumber = @CouponDeliveryNumber      
    end
*/    
    
    exec ChangeCouponStockStatus 7, @CouponDeliveryNumber, 2    
    FETCH FROM CURSOR_Update_Ord_CouponDelivery_H INTO @CouponDeliveryNumber, @ApproveStatus, @CreatedBy, @NeedActive, @StoreID  
  END
  CLOSE CURSOR_Update_Ord_CouponDelivery_H
  DEALLOCATE CURSOR_Update_Ord_CouponDelivery_H  
END


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'CouponDeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。指coupon拣货单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源店铺，一般可能是总部' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送达店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户类型。1：客戶訂貨。 2：店鋪訂貨' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'CustomerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'customer表 主键， 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'FromContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户收货确认后是否激活。0：不激活。1：激活。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'NeedActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare（送货中）。  A:Approve（送货完成，顾客签收） 。 V：Void（顾客拒收，card回到dormant状态）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵送货单主表 （即 收货确认表）
（从批核后的拣货单复制过来，过滤了拣货数量为0的记录。 头表状态略有不同）
（发货单产生后由收货方确认收货）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_H'
GO
