USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponReplenishRule_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponReplenishRule_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponReplenishCode] [varchar](64) NOT NULL,
	[StoreID] [int] NOT NULL,
	[OrderTargetID] [int] NULL,
	[MinStockQty] [int] NULL,
	[RunningStockQty] [int] NULL,
	[OrderRoundUpQty] [int] NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_COUPONREPLENISHRULE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'CouponReplenishCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据StoreTypeID选择店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货目标ID。（如果是店铺订货，则填写总部的ID。 如果总部订货，则填写供应商ID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'OrderTargetID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小允许库存。（低于此将可能触发自动补货）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'MinStockQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'正常库存数量。（补货数量为： 正常库存数量 - 当前库存数量）
注：如果有补货的倍数要求，按照倍数要求计算。
比如： 正常库存数量 - 当前库存数量 = 53.   供应商要求按50倍数订货，所以需要订货数量为 100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'RunningStockQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货数量的倍数：
Sample:
max=300 
current=97
max-current=203
203 will be rounded up to 250 since it is the next number divisible by 50.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'OrderRoundUpQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级。 （库存不足情况下。按 从小到大 优先分配。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon自动补货设置子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_D'
GO
