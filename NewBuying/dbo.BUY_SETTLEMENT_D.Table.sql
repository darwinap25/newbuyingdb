USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_SETTLEMENT_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_SETTLEMENT_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[SettlementCode] [varchar](64) NOT NULL,
	[TransNum] [varchar](64) NOT NULL,
	[SeqNum] [int] NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[Amount] [dbo].[Buy_Amt] NULL,
	[NetAmount] [dbo].[Buy_Amt] NULL,
	[CardNo] [varchar](64) NULL,
	[CardType] [varchar](64) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[BankCode] [varchar](64) NULL,
	[Status] [int] NOT NULL,
	[SettleDate] [datetime] NULL,
	[SettleBy] [char](8) NULL,
	[Installment] [int] NULL,
 CONSTRAINT [PK_BUY_SETTLEMENT_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_SETTLEMENT_D] ADD  DEFAULT ((1)) FOR [SeqNum]
GO
ALTER TABLE [dbo].[BUY_SETTLEMENT_D] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[BUY_SETTLEMENT_D] ADD  DEFAULT ((0)) FOR [Installment]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'SettlementCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'TransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'salesT的序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'SeqNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单的storecode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'salesT的本币金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'salesT的本币金额 * （1-buy_bank.commission）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'NetAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'信用卡卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'CardNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'信用卡类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'CardType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'信用卡的approvalcode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'BankCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。
0: Outstanding 
1: Settled' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结算日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'SettleDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计算人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'SettleBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否分期。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D', @level2type=N'COLUMN',@level2name=N'Installment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结算表子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT_D'
GO
