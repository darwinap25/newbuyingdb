USE [NewBuying]
GO
/****** Object:  Table [dbo].[STK_StockOnhand]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STK_StockOnhand](
	[StoreID] [int] NOT NULL,
	[StockTypeCode] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[OnhandQty] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_STK_STOCKONHAND] PRIMARY KEY CLUSTERED 
(
	[StoreID] ASC,
	[StockTypeCode] ASC,
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_Store表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_StockType表 Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand', @level2type=N'COLUMN',@level2name=N'OnhandQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品库存表。货品实际在库数量。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand'
GO
