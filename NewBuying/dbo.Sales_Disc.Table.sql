USE [NewBuying]
GO
/****** Object:  Table [dbo].[Sales_Disc]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales_Disc](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[TransNum] [varchar](64) NOT NULL,
	[ItemSeqNo] [varchar](64) NULL,
	[ProdCode] [dbo].[Buy_PLU] NULL,
	[TenderSeqNo] [int] NULL,
	[TenderID] [int] NULL,
	[TenderCode] [varchar](64) NULL,
	[SalesDiscCode] [varchar](64) NULL,
	[SalesDiscDesc] [nvarchar](512) NULL,
	[SalesDiscType] [int] NULL DEFAULT ((0)),
	[SalesDiscDataType] [int] NULL DEFAULT ((0)),
	[SalesDiscPrice] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[SalesPrice] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[SalesDiscOffAmount] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[SalesDiscQty] [dbo].[Buy_Qty] NULL,
	[AffectNetPrice] [int] NULL DEFAULT ((0)),
	[SalesDiscLevel] [int] NULL DEFAULT ((0)),
	[PromotionCode] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SALES_DISC] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'TransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单中货品序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'ItemSeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单中支付序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'TenderSeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'TenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣编码。（根据SalesDiscType，可能是discountcode，PromotionCode，MNMCode）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣类型。决定SalesDiscCode的含义。
0：没有折扣。
1：改价。      （SalesDiscCode记录改价原因code）
2：单项折扣。 （SalesDiscCode记录Discountcode）
3：整单折扣。 （SalesDiscCode记录Discountcode）
11：混配促销。 （SalesDiscCode记录MNMcode）
22：会员单项折扣 （SalesDiscCode记录Promotioncode）
23：会员整单折扣（SalesDiscCode记录Promotioncode）


' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣值的类型。 0：金额。1：百分比。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscDataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣值。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参与计算的原始价格。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此折扣/优惠在此货品上扣减的金额。（已包含数量）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscOffAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参与折扣的数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否在NetPrice上打折（即是否允许折上折）。0：不可以，必须在UnitPrice上打折。 1：可以' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'AffectNetPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣优先级。默认0.  0最低， 数字越大，优先级越高' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'SalesDiscLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果是因为promotion产生的discount，记录promotioncode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单发生的折扣记录。（子表，计算用表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_Disc'
GO
