USE [NewBuying]
GO
/****** Object:  Table [dbo].[DisneyPIN]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DisneyPIN](
	[PINID] [int] IDENTITY(1,1) NOT NULL,
	[PINCode] [varchar](64) NULL,
	[PINDesc1] [nvarchar](512) NULL,
	[PINDesc2] [nvarchar](512) NULL,
	[PINDesc3] [nvarchar](512) NULL,
	[PINPicFile] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_DISNEYPIN] PRIMARY KEY CLUSTERED 
(
	[PINID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[DisneyPIN] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DisneyPIN] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisneyPIN', @level2type=N'COLUMN',@level2name=N'PINID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisneyPIN', @level2type=N'COLUMN',@level2name=N'PINCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisneyPIN', @level2type=N'COLUMN',@level2name=N'PINDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisneyPIN', @level2type=N'COLUMN',@level2name=N'PINDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisneyPIN', @level2type=N'COLUMN',@level2name=N'PINDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisneyPIN', @level2type=N'COLUMN',@level2name=N'PINPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PIN主档表（Disney使用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisneyPIN'
GO
