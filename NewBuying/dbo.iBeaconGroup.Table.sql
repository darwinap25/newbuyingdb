USE [NewBuying]
GO
/****** Object:  Table [dbo].[iBeaconGroup]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iBeaconGroup](
	[iBeaconGroupID] [int] IDENTITY(1,1) NOT NULL,
	[iBeaconGroupCode] [varchar](64) NOT NULL,
	[iBeaconGroupName1] [varchar](512) NULL,
	[iBeaconGroupName2] [varchar](512) NULL,
	[iBeaconGroupName3] [varchar](512) NULL,
	[AssociationType] [int] NULL DEFAULT ((0)),
	[AssociationID] [varchar](64) NULL,
	[StartDateTime] [datetime] NULL DEFAULT (getdate()),
	[EndDateTime] [datetime] NULL DEFAULT (getdate()),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_IBEACONGROUP] PRIMARY KEY CLUSTERED 
(
	[iBeaconGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GroupID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'iBeaconGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'iBeaconGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'iBeaconGroupName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'iBeaconGroupName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'iBeaconGroupName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeaconGroup 相关联的类型 。  默认0.
1、Coupon （coupontypeid）。 
2、News （）
3、Product （Product表的 prodcode）
4、Message
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'AssociationType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联对象的ID .根据AssociationType， 内容可能是coupontypeid， PromotionMsg的KeyID， Product表的 prodcode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'AssociationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始日期时间。 （注：带有时间，需要考虑当前时间）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'StartDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期时间。 （注：带有时间，需要考虑当前时间）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup', @level2type=N'COLUMN',@level2name=N'EndDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeacon 组' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroup'
GO
