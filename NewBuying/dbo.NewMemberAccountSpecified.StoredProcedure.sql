USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[NewMemberAccountSpecified]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[NewMemberAccountSpecified]  
 @MemberRegisterMobile varchar(512),   -- 注册手机号  
 @MemberMobilePhone varchar(512),   -- 手机号  
 @CountryCode varchar(512),     -- 手机号的国家码  
 @CardTypeCode varchar(512),    -- 需要新增的CardTypeCode, 只绑定这个指定的CardTypeID的卡. 没有则返回 -36  
 @CardNumber varchar(512),    -- 指定卡号, 只绑定这个指定的CardTypeID的卡. 没有则返回 -36  
 @ReturnMemberID int output,            -- 返回新增的memberID  
 @ReturnMemberPassword varchar(512) output,          -- 返回会员的CardNumber    
 @ReturnCardNumber varchar(512)='' output,          -- 返回绑定的卡的CardNumber    
 @ReturnCardPassword varchar(512)='' output,          -- 返回绑定的卡的CardNumber    
 @BindCardTypeID int=0,     -- @AutoBindCard =1 时有效，需要自动绑定的卡类型ID   
 @MemberDateOfBirth datetime=0,                 -- 生日  
 @MemberSex char(1)='',       -- 性别。M，F，空  
 @MemberEngFamilyName nvarchar(30)='',   --   
 @MemberEngGivenName nvarchar(30)='',  
 @MemberChiFamilyName nvarchar(30)='',  
 @MemberChiGivenName nvarchar(30)='',  
 @AutoBindCard int = 1                         -- 0：不自动绑定卡。 1：自动绑定卡，没有卡时报错   
as  
/****************************************************************************  
**  Name : NewMemberAccountSpecified  
**  Version: 1.0.0.0  
**  Description :  新增Member记录。  (未使用)
**  Parameter :     
**  return:   
declare  @A int, @ReturnMemberID int, @ReturnCardNumber varchar(512), @ReturnMemberPassword varchar(512), @ReturnCardPassword varchar(512)  
 exec @A = NewMemberAccount '086098888902', '232434', '086', '0001', @ReturnMemberID output, @ReturnMemberPassword output , @ReturnCardNumber output, @ReturnCardPassword output  
 ,0,0,'','','','','',1  
print @A   
print  @ReturnMemberID  
print @ReturnMemberPassword  

select * from card where memberid = 93  
select * from member where memberregistermobile = '0865465684273412'  
select * from cardtype where status = 5  
select * from card where status = 1 and memberid not in (select memberid from member)  
update card set status = 5 where status = 1 and memberid not in (select memberid from member)  
delete from member where memberid=54  
**  Created by: Robin @2012-12-12  
**  Modified by: Robin @2012-12-12  
**  
****************************************************************************/  
begin  
  declare @MemberDayOfBirth int, @MemberMonthOfBirth int, @MemberYearOfBirth int, @PWDMinLength  int, @RandNum decimal(20,0) 
  declare @NewMemberID int,@NewCardNumber varchar(512), @InitPWD varchar(512), @CardTypeID int, @MD5InitPWD varchar(512)  
  select top 1 @PWDMinLength = PWDMinLength from passwordrule	where MemberPWDRule = 1
  set @PWDMinLength = isnull(@PWDMinLength, 6)
  if @PWDMinLength = 0 
    set @PWDMinLength = 6
        
  set @ReturnCardPassword = ''  
  if isnull(@MemberRegisterMobile, '') = ''  
    return -7  
  if exists(select * from member where MemberRegisterMobile = @MemberRegisterMobile)  
    return -33  
    
  if isnull(@MemberSex, '') = '' set @MemberSex = ''  
  if isnull(@MemberEngFamilyName, '') = '' set @MemberEngFamilyName = ''  
  if isnull(@MemberEngGivenName, '') = '' set @MemberEngGivenName = ''  
  if isnull(@MemberChiFamilyName, '') = '' set @MemberChiFamilyName = ''  
  if isnull(@MemberChiGivenName, '') = '' set @MemberChiGivenName = ''  
  if isnull(@MemberDateOfBirth, 0) = 0   
  begin  
    set @MemberDateOfBirth = 0  
    set @MemberDayOfBirth = 0  
    set @MemberMonthOfBirth = 0  
    set @MemberYearOfBirth = 0  
  end else  
  begin     
    set @MemberDayOfBirth = DatePart(mm, @MemberDateOfBirth)  
    set @MemberMonthOfBirth = DatePart(dd, @MemberDateOfBirth)  
    set @MemberYearOfBirth = DatePart(yy, @MemberDateOfBirth)   
  end  
  -- 随机密码: 
    select @RandNum = round(rand() * 100000000000000000, 0)
    set @InitPWD = right('00000000000000000' + convert(varchar(20), @RandNum), @PWDMinLength)     
  set @MD5InitPWD = dbo.EncryptMD5(@InitPWD)  
    
  -- 创建Member记录       
  insert into member(MemberRegisterMobile, MemberEngFamilyName,MemberEngGivenName, MemberChiFamilyName,MemberChiGivenName, MemberSex,   
        MemberMobilePhone, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth, MemberPassword, CountryCode)      
  values(@MemberRegisterMobile, @MemberEngFamilyName,@MemberEngGivenName, @MemberChiFamilyName,@MemberChiGivenName, @MemberSex,   
        @MemberMobilePhone, @MemberDateOfBirth, @MemberDayOfBirth, @MemberMonthOfBirth, @MemberYearOfBirth, @MD5InitPWD, @CountryCode)    
  set @NewMemberID = SCOPE_IDENTITY()  
  if isnull(@NewMemberID, 0) = 0  
    return -97  
/*      
  -- 同步增加 MemberMessageAccount 表记录.  
  exec SetMemberMessageAccount 0, 1, @NewMemberID, null, 2, @MemberMobilePhone, 0, ''  
  exec SetMemberMessageAccount 0, 1, @NewMemberID, null, 3, @MemberMobilePhone, 0, ''  
*/      
  --绑定一个新的SVA 卡号 (Demo 版本，每个cardtype 都给一个卡， 没有的跳过)  
  if isnull(@CardNumber, '') <>''  
  begin  
      select @CardTypeID = CardTypeID from CardType where CardTypeCode = @CardTypeCode  
      select Top 1 @NewCardNumber = CardNumber, @ReturnCardPassword = CardPassword from Card C   
   left join CardGrade G on C.CardGradeID = G.CardGradeID  
  where C.CardTypeID = @CardTypeID and CardNumber=@CardNumber and Status = 0   
    
      if isnull(@NewCardNumber, '') = ''  
        return -36  
      else    
        Update Card set Status = 2, MemberID = @NewMemberID, UpdatedOn = getdate()   
           where CardTypeID = @CardTypeID and CardNumber = @NewCardNumber                   
  end   
  else  
 return -36  
      
  set @ReturnMemberID = @NewMemberID  
  set @ReturnCardNumber = @NewCardNumber  
  --set @ReturnCardPassword =   
  set @ReturnMemberPassword  = @InitPWD    
  return 0        
end  

GO
