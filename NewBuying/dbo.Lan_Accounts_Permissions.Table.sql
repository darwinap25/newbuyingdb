USE [NewBuying]
GO
/****** Object:  Table [dbo].[Lan_Accounts_Permissions]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lan_Accounts_Permissions](
	[PermissionID] [int] NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Lan] [nvarchar](50) NOT NULL
) ON [PRIMARY]

GO
