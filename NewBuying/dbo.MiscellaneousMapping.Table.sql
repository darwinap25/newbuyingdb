USE [NewBuying]
GO
/****** Object:  Table [dbo].[MiscellaneousMapping]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MiscellaneousMapping](
	[MiscellaneousMappingID] [int] IDENTITY(1,1) NOT NULL,
	[MiscellaneousCode] [varchar](64) NULL,
	[TableName] [varchar](512) NULL,
	[FieldName] [varchar](512) NULL,
	[FieldValue] [int] NULL,
	[DescName1] [nvarchar](512) NULL,
	[DescName2] [nvarchar](512) NULL,
	[DescName3] [nvarchar](512) NULL,
 CONSTRAINT [PK_MISCELLANEOUSMAPPING] PRIMARY KEY CLUSTERED 
(
	[MiscellaneousMappingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'MiscellaneousMappingID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始设置编号 (查询用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'MiscellaneousCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关表名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'TableName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'FieldName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字段中的数字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'FieldValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对应数值的描述名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'DescName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对应数值的描述名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'DescName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对应数值的描述名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping', @level2type=N'COLUMN',@level2name=N'DescName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'杂项对应表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MiscellaneousMapping'
GO
