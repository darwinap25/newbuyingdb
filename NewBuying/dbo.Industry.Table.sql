USE [NewBuying]
GO
/****** Object:  Table [dbo].[Industry]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Industry](
	[IndustryID] [int] IDENTITY(1,1) NOT NULL,
	[IndustryCode] [varchar](64) NULL,
	[IndustryName1] [nvarchar](512) NULL,
	[IndustryName2] [nvarchar](512) NULL,
	[IndustryName3] [nvarchar](512) NULL,
	[UpdatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_INDUSTRY] PRIMARY KEY CLUSTERED 
(
	[IndustryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Industry', @level2type=N'COLUMN',@level2name=N'IndustryID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行业编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Industry', @level2type=N'COLUMN',@level2name=N'IndustryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行业名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Industry', @level2type=N'COLUMN',@level2name=N'IndustryName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行业名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Industry', @level2type=N'COLUMN',@level2name=N'IndustryName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行业名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Industry', @level2type=N'COLUMN',@level2name=N'IndustryName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行业表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Industry'
GO
