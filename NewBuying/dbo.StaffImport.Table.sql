USE [NewBuying]
GO
/****** Object:  Table [dbo].[StaffImport]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StaffImport](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[StaffNo] [varchar](64) NULL,
	[CompanyDesc] [nvarchar](512) NULL,
	[EngName] [varchar](64) NULL,
	[Alias] [varchar](64) NULL,
	[JoinDate] [datetime] NULL,
	[ResignDate] [datetime] NULL,
	[Sex] [char](1) NULL,
	[DOB] [datetime] NULL,
	[EmailAddr] [varchar](64) NULL,
	[EmailAddr2] [varchar](64) NULL,
	[MobilePhone] [varchar](64) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_STAFFIMPORT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'StaffNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'CompanyDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'英文名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'EngName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'别名，昵称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'Alias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'入职日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'JoinDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'离职日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'ResignDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'M，F' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'Sex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dateofbirth. 生日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'DOB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'EmailAddr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'EmailAddr2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'MobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0： 无效。 1： 在职。 2：离职。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Staff导入表。 
注：这个表导入的是staff的变动记录。 e.g. 新增员工， 离职员工。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffImport'
GO
