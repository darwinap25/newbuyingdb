USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ReindexTables]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ReindexTables]  
 -- Add the parameters for the stored procedure here  
 @Table VARCHAR(255)='',   
 @fillfactor INT =70  
AS  
/****************************************************************************  
**  Name : ReindexTables  
**  Version: 1.0.0.1  
**  Description : 重建所有表的Index  
**  
**  Parameter :  
**         @Table VARCHAR(255)='',  -重建特定表，默认所有表  
**         @fillfactor INT =70  -索引因子，默认70  
**  Created by: Robin @2014-12-16  
**  Modified by Robin @2015-01-29 对于2005版本以上加入在线重建索引，提高系统可用性  
**  
****************************************************************************/  
  
BEGIN  
 DECLARE @cmd NVARCHAR(500)    
  
  
 SET @cmd = 'DECLARE TableCursor CURSOR FOR SELECT ''['' + table_catalog + ''].['' + table_schema + ''].['' +   
 table_name + '']'' as tableName FROM INFORMATION_SCHEMA.TABLES   
 WHERE table_type = ''BASE TABLE'''  
  
 if isnull(@Table,'')<>''  
  SET @cmd=@cmd+' and table_name='''+@Table+''''  
  
 -- create table cursor    
 EXEC (@cmd)    
 OPEN TableCursor     
  
 FETCH NEXT FROM TableCursor INTO @Table     
 WHILE @@FETCH_STATUS = 0     
 BEGIN     
  
  IF (@@MICROSOFTVERSION / POWER(2, 24) >= 9)  
  BEGIN  
   -- SQL 2005 or higher command   
   SET @cmd = 'ALTER INDEX ALL ON ' + @Table + ' REBUILD WITH (FILLFACTOR = ' + CONVERT(VARCHAR(3),@fillfactor) + ',ONLINE =ON)'  
   EXEC (@cmd)   
  END  
  ELSE  
  BEGIN  
   -- SQL 2000 command   
   DBCC DBREINDEX(@Table,' ',@fillfactor)    
  END  
  
  FETCH NEXT FROM TableCursor INTO @Table     
 END     
  
 CLOSE TableCursor     
 DEALLOCATE TableCursor    
END  

GO
