USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[StockTakeRegister]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StockTakeRegister]
  @StockTakeType      INT,          -- 盘点类型, 0: 只盘点数量. 1: 盘点serialno (  serialno 输入方式 还未完成. UI 也需要修改.)
  @StoreID            INT,          -- 店铺ID ,必填
  @DeptCode           VARCHAR(64),  -- 部门代码
  @StockTakeDate      DATETIME,     -- 盘点日期
  @Remark             VARCHAR(512), -- 备注
  @UserID             INT           -- 创建人
AS
/****************************************************************************
**  Name : StockTakeRegister
**  Version: 1.0.0.2
**  Description : 库存盘点注册
**  Parameter :
declare @a int
exec @a = StockTakeRegister 0, 14, '', '2015-06-30', 'ttt', 1 
print @a
select * from stk_stockonhand
select * from STK_STAKEBOOK
select * from STK_STAKE01
select * from STK_STAKE02
select * from buy_store
select * from STK_STAKE_H
delete from STK_STAKE_H where storeid = 14
** Created by: Gavin @2015-06-24
** Modified by：Gavin @2015-09-17 (ver 1.0.0.1) 写入STK_STAKE01 时，数量都是0， 不需要从库存取在库数量
** Modified by：Gavin @2016-02-16 (ver 1.0.0.2) 判断前面的调整还未结束时， 条件改为 status <> 5
**
****************************************************************************/
BEGIN
  DECLARE @StockTakeNumber VARCHAR(64)

  SET @StoreID = ISNULL(@StoreID, 0)
  SET @DeptCode = ISNULL(@DeptCode, '')
  SET @StockTakeDate = ISNULL(@StockTakeDate, GETDATE())
  IF @StoreID = 0 RETURN -1    -- @StoreID 必填
  IF EXISTS(SELECT * FROM STK_STAKE_H WHERE StoreID = @StoreID AND Status < 5 )
    RETURN -2           -- 前面的判断还未结束.
  
  
  EXEC GetRefNoString 'SRKNUM', @StockTakeNumber output 
  -- 清空盘点表
--  TRUNCATE TABLE STK_STAKEBOOK
--  TRUNCATE TABLE STK_STAKE01
--  TRUNCATE TABLE STK_STAKE02
--  TRUNCATE TABLE STK_STAKEVAR  
--  TRUNCATE TABLE STK_STAKE_H
  DELETE FROM STK_STAKEBOOK 
    WHERE StoreID = @StoreID and (@DeptCode = '' or ProdCode in (SELECT ProdCode FROM BUY_PRODUCT WHERE Departcode = @DeptCode))
  DELETE FROM STK_STAKE01 
    WHERE StoreID = @StoreID and (@DeptCode = '' or ProdCode in (SELECT ProdCode FROM BUY_PRODUCT WHERE Departcode = @DeptCode))
  DELETE FROM STK_STAKE02 WHERE StoreID = @StoreID 
  DELETE FROM STK_STAKEVAR WHERE StoreID = @StoreID 
  ----------------------
 
  -- 读取onhand 数量 
  IF @StockTakeType = 0
  BEGIN
    INSERT INTO STK_STAKEBOOK(StoreID, StockTakeNumber, ProdCode, QTY, StockType, SerialNo)
    SELECT @StoreID, @StockTakeNumber, PRODCODE,(OnhandQTY) AS QTY, StockTypeCode, '' FROM STK_StockOnhand 
    WHERE StoreID = @StoreID AND (@DeptCode = '' or ProdCode in (SELECT ProdCode FROM BUY_PRODUCT WHERE Departcode = @DeptCode)) -- and StockTypeCode = 'G'

    INSERT INTO STK_STAKE01(StoreID, StockTakeNumber, ProdCode, QTY, StockType, SerialNo)
    SELECT @StoreID, @StockTakeNumber, A.PRODCODE, 0 AS QTY, A.StockTypeCode, '' 
     FROM (SELECT A.PRODCODE, B.StockTypeCode FROM BUY_Product A, BUY_STOCKTYPE B) A 
 --    LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StoreID = @StoreID) C 
 --      ON A.ProdCode = C.ProdCode AND A.StockTypeCode = C.StockTypeCode 
    WHERE (@DeptCode = '' or A.ProdCode in (SELECT ProdCode FROM BUY_PRODUCT WHERE Departcode = @DeptCode)) -- and StockTypeCode = 'G'
  END 
  ELSE
  BEGIN
    --  serialno 输入方式 还未完成. UI 也需要修改.
    INSERT INTO STK_STAKEBOOK(StoreID, StockTakeNumber, ProdCode, QTY, StockType, SerialNo)
    SELECT A.StoreID, @StockTakeNumber, A.PRODCODE,CASE WHEN ISNULL(B.SerialNo,'') <> '' THEN 1 else A.OnhandQTY END AS QTY, A.StockTypeCode, B.SerialNo 
     FROM (SELECT * FROM STK_StockOnhand WHERE StoreID = @StoreID) A
     LEFT JOIN (SELECT * FROM STK_StockOnhand_SN WHERE StoreID = @StoreID)  B ON A.ProdCode = B.ProdCode AND A.StoreID = B.StoreID AND A.StockTypeCode = B.StockTypeCode
    WHERE A.StoreID = @StoreID AND (@DeptCode = '' or A.ProdCode in (SELECT ProdCode FROM BUY_PRODUCT WHERE Departcode = @DeptCode)) -- and StockTypeCode = 'G'
  
    INSERT INTO STK_STAKE01(StoreID, StockTakeNumber, ProdCode, QTY, StockType, SerialNo)
    --SELECT A.StoreID, @StockTakeNumber, A.PRODCODE, CASE WHEN ISNULL(B.SerialNo,'') <> '' THEN 1 else A.OnhandQTY END AS QTY, A.StockTypeCode, B.SerialNo 
    SELECT A.StoreID, @StockTakeNumber, A.PRODCODE, 0 AS QTY, A.StockTypeCode, '' 
     FROM  (SELECT * FROM STK_StockOnhand WHERE StoreID = @StoreID)  A
     --LEFT JOIN  (SELECT * FROM STK_StockOnhand_SN WHERE StoreID = @StoreID) B ON A.ProdCode = B.ProdCode AND A.StoreID = B.StoreID AND A.StockTypeCode = B.StockTypeCode
    WHERE A.StoreID = @StoreID AND (@DeptCode = '' or A.ProdCode in (SELECT ProdCode FROM BUY_PRODUCT WHERE Departcode = @DeptCode)) -- and StockTypeCode = 'G' 
  END
  
  INSERT INTO STK_STAKE_H(StockTakeNumber, StockTakeDate, StoreID, Status, StockTakeType, Remark, CreatedBy, UpdatedBy)
  VALUES(@StockTakeNumber, @StockTakeDate, @StoreID, 1, @StockTakeType, @Remark, @UserID, @UserID)
  
  RETURN 0   
END

GO
