USE [NewBuying]
GO
/****** Object:  Table [dbo].[Department]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[DepartCode] [varchar](64) NOT NULL,
	[BrandID] [int] NULL,
	[DepartName1] [nvarchar](512) NULL,
	[DepartName2] [nvarchar](512) NULL,
	[DepartName3] [nvarchar](512) NULL,
	[DepartPicFile] [nvarchar](512) NULL,
	[DepartPicFile2] [nvarchar](512) NULL,
	[DepartPicFile3] [nvarchar](512) NULL,
	[DepartNote] [nvarchar](512) NULL,
	[GenderFlag] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_DEPARTMENT] PRIMARY KEY CLUSTERED 
(
	[DepartCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据要求，增加部门图片文件存储字段，存放不同语言的图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartPicFile2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据要求，增加部门图片文件存储字段，存放不同语言的图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartPicFile3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'DepartNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此部门下货品是否区分性别。0：不区分，1：区分。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department', @level2type=N'COLUMN',@level2name=N'GenderFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品部门表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Department'
GO
