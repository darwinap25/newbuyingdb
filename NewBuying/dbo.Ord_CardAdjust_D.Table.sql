USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardAdjust_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardAdjust_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardAdjustNumber] [varchar](64) NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[OrderAmount] [money] NULL,
	[ActAmount] [money] NULL,
	[OrderPoints] [int] NULL,
	[ActPoints] [int] NULL,
	[Additional1] [varchar](64) NULL,
 CONSTRAINT [PK_Ord_CardAdjust_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'CardAdjustNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求操作金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'OrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际操作金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'ActAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求操作积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'OrderPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际操作积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'ActPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留存储位。 目前用于RRG TelCo 的 Mobile Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D', @level2type=N'COLUMN',@level2name=N'Additional1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡调整子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_D'
GO
