USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberLogout]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[MemberLogout]  
  @MemberID				int,                    -- 返回Member表主键
  @MemberRegisterMobile	varchar(512),			--会员注册手机号
  @CardNumber			varchar(64)             -- 返回Card表主键
AS
/****************************************************************************
**  Name : MemberLogout 会员登出
**  Version: 1.0.0.2
**  Description : 会员登出, 三个条件任意一个就行。 都填的话互相是 and 的关系
**  Example :
  declare @MemberID int, @A int, @CardNumber varchar(64),  @MemberCreatedOn datetime 
  exec @A = MemberLogout 1,'',''
  print @A
  print @MemberID
  print @CardNumber
  select * from card where memberid  >0
  select * from member
    select * from useraction_movement
**  Created by: Gavin @2014-06-24
**  Modify by: Gavin @2015-02-09 修改查询语句，输入的3 个条件不再是and 条件。 MemberID > MemberRegisterMobile > CardNumber. 依次排序，只检查第一个非空的条件。
**  Modify by: Gavin @2015-07-22 (ver 1.0.0.2) 修改update的条件 
**
****************************************************************************/
begin
  declare @MID int
  set @MemberID = ISNULL(@MemberID, 0)
  set @MemberRegisterMobile = ISNULL(@MemberRegisterMobile, '')
  set @CardNumber = ISNULL(@CardNumber, '')
  
  if @MemberID > 0
    select @MID = MemberID from Member where MemberID = @MemberID
  else if @MemberRegisterMobile <> ''
    select @MID = MemberID from Member where MemberRegisterMobile = @MemberRegisterMobile
  else if @CardNumber <> ''
    select @MID = MemberID from Card where CardNumber = @CardNumber 
   
  if isnull(@MID, 0) > 0
  begin
    update MemberMessageAccount set AccountNumber = '' where MemberID = @MID and MessageServiceTypeID in (9,10)
  end else
    return -1
  
  return 0
end

GO
