USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetMemberInfo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetMemberInfo]
  @MemberID                 int,            --会员主键
  @MemberRegisterMobile     varchar(512),   --会员注册手机号
  @MemberMobilePhone        varchar(512),   --会员手机号
  @CountryCode              varchar(512),   --手机号的国家码
  @MemberEmail              varchar(512),   --会员邮箱
  @NickName                 varchar(512),   --会员昵称
  @MemberEngFamilyName      varchar(512),   --会员姓(英文)
  @MemberEngGivenName       varchar(512),   --会员名(英文)
  @MemberChiFamilyName      varchar(512),   --会员姓(中文)
  @MemberChiGivenName       varchar(512),   --会员名(中文)
  @MemberSex                varchar(512),   --会员性别
  @MemberDateOfBirth        Datetime,       --会员生日
  @MemberMarital            int,            --婚姻情况
  @MemberIdentityType       int,            --证件类型
  @MemberIdentityRef        varchar(512),   --证件号码
  @EducationID              varchar(512),   --教育
  @ProfessionID             varchar(512),   --职业
  @MemberPosition           varchar(512),   --职位
  @NationID                 int,            --国籍
  @HomeTelNum               varchar(512),   --家庭电话
  @HomeAddress              varchar(512),   --家庭地址
  @OtherContact             varchar(512),   --其他联系方式
  @Hobbies                  varchar(512),   --兴趣爱好
  @SpRemark                 varchar(512),   --备注
  @MemberPictureFile        varchar(512),    --会员照片文件上传文件名
  @ReceiveAllAdvertising    int=0,          -- 0:不接收所有广告（所有messageservicetypeid）。 1：同意接收。
  @TransPersonalInfo		int=0,           -- 同意传播个人信息。0：不同意。 1：同意      默认0.    
  @CompanyDesc              nvarchar(512)='',   -- 公司名称(学校名称)
  @OfficeAddress            nvarchar(512)='',   -- 公司地址(学校地址)
  @ViewHistoryTranDays      int=0,              -- 显示会员历史交易范围(设定查询多少天内的),默认0,表示不设限制
  @AcceptPhoneAdvertising   int=0,           -- 0: 不接受电话广告. 1:接受
  @RecSMSAdv                int=0,            -- 0： 不接受短信广告。  1:接受
  @RecEmailAdv              int=0,            -- 0： 不接受email广告。 1：接受
  @ReturnJSONMsg            varchar(max) output,  -- 返回的JSON消息
  @AddressCountry           varchar(64),       -- Member家庭地址的country (country表的countrycode)
  @AddressProvince          varchar(64),       -- Member家庭地址的Province (Province表的Provincecode)
  @AddressCity              varchar(64),      -- Member家庭地址的City (City表的Citycode)
  @AddressDistrict          varchar(64),      -- Member家庭地址的District (District表的Districtcode)
  @AddressDetail            varchar(512),      -- Member家庭地址的Detail
  @MemberAppellation        varchar(512)='',      -- 姓名的称谓。比如 先生，小姐....
  @LanguageAbbr			        varchar(20)='',    -- 会员默认语言。
  @AgeBracklet              int=0              -- 年龄段
AS
/****************************************************************************
**  Name : SetMemberInfo  
**  Version: 1.0.0.20
**  Description : update member 表记录
**  Parameter :
  declare @a int, @MemberID int, @ReturnJSONMsg varchar(max)
  set @MemberID = 169
  exec @a = SetMemberInfo @MemberID, '','','','','','','','','','','2000-10-10',0,0,'','','','',0,'','','','','','',0,0,'','',
    0,1,1,0,@ReturnJSONMsg output, '','','','','','','' 
  print @a
  print @ReturnJSONMsg

**  Created by: Gavin @2012-02-20
**  Modify by: Gavin @2012-12-14 (ver 1.0.0.1) 修改资料时，同时把MemberEmail更新到MemberMessageAccount
**  Modify by: Gavin @2013-06-05 (ver 1.0.0.2) 增加参数：@ReceiveAllAdvertising，@TransPersonalInfo
**  Modify by: Gavin @2013-07-09 (ver 1.0.0.3) 增加参数：@CompanyDesc(学校名称), @OfficeAddress（学校位置）, @ViewHistoryTranDays （查看历史记录的天数）
**  Modify by: Gavin @2013-10-11 (Ver 1.0.0.4) 记录操作到UserAction_Movement表
**  Modify by: Gavin @2014-06-25 (Ver 1.0.0.5) 同步更改MessageAccount的邮箱和手机号码. 修改时,把VerifyFlag标志改为 0
**  Modify by: Gavin @2014-07-04 (Ver 1.0.0.6) 增加输入参数@AcceptPhoneAdvertising，@RecSMSAdv，@RecEmailAdv。同步修改MemberMessageAccount(只能改同类型的第一条记录)
**  Modify by: Gavin @2014-07-08 (Ver 1.0.0.7) @MemberRegisterMobile,@MemberMobilePhone,@MemberEmail 任意一个变动,所有account都需要变成未验证.并发邮件通知.
**  Modify by: Gavin @2014-07-11 (Ver 1.0.0.8) 增加输入参数AddressCountry, AddressProvince, AddressCity, AddressDistrict, AddressDetail		
**  Modify by: Gavin @2014-07-18 (Ver 1.0.0.9) 修正重置验证标识的问题. 
**  Modify by: Gavin @2014-07-30 (Ver 1.0.0.10) 修改电话号码时增加判断： 所改的号码是否已经存在，如果已经存在，则是否超过了6个月的保护期。
**  Modify by: Gavin @2014-08-06 (Ver 1.0.0.11) 校验输入的@MemberRegisterMobile 是否会造成重复.如果有重复则返回-33
**  Modify by: Gavin @2014-11-10 (Ver 1.0.0.12) 增加输入参数@MemberAppellation
**  Modify by: Gavin @2014-11-10 (Ver 1.0.0.13) 增加更新PromotionFlagUpdatedOn
**  Modify by: Gavin @2014-11-13 (Ver 1.0.0.14) 把更新PromotionFlagUpdatedOn 的步骤放在最后
**  Modify by: Gavin @2014-11-18 (ver 1.0.1.15) 更改mobile或者email时, 只变动更改的记录的verifyflag。 取消联动。（原来改动任何一个，两个同时置为需要重新验证）
**  Modify by: Gavin @2015-02-16 (ver 1.0.1.16) 增加输入字段AgeBracklet. 
**  Modify by: Gavin @2015-07-17 (ver 1.0.1.17) (for inchacape) 更改email时需要同步更改mobile的VerifyFlag标志 。（一个member只能有一个手机号和一个邮箱）
**  Modify by: Gavin @2015-12-29 (ver 1.0.1.18) (for bauhaus) 更新member数据时，同时更新updateon
**  Modify by: Gavin @2016-04-14 (Ver 1.0.1.19) 取消update memberaddress 动作。一个会员有多个地址，所以不能update.
**  Modify by: Gavin @2016-09-13 (Ver 1.0.1.20) 按照新的流程，邮箱是验证通过后才调用setmemberinfo的， 所以默认verifyflag 是 1
**
****************************************************************************/
begin
  declare @EmailAccount varchar(512), @SMSAccount varchar(512), @NeedSendMsg int, @OldMemberRegisterMobile varchar(64), @Language int
  declare @OtherMemberID int, @OtherUpdateDate datetime, @MobileProtectPeriodValue int
  declare @OldSMSFlag int, @OldEmailFlag int, @OldPhoneFlag int, @NeedUpdateFlagDate int
  
  set @NeedSendMsg = 0
  set @MemberEmail = ISNULL(@MemberEmail, '')
  set @CountryCode = ISNULL(@CountryCode, '')
  set @MemberMobilePhone = ISNULL(@MemberMobilePhone, '')
  set @MemberDateOfBirth = isnull(@MemberDateOfBirth, 0)
  set @AcceptPhoneAdvertising = ISNULL(@AcceptPhoneAdvertising, 0)
  set @RecSMSAdv = ISNULL(@RecSMSAdv, 0)
  set @RecEmailAdv = ISNULL(@RecEmailAdv, 0)
  set @AgeBracklet = isnull(@AgeBracklet, 0)

  select @OldMemberRegisterMobile = MemberRegisterMobile, @OldPhoneFlag = isnull(AcceptPhoneAdvertising, 0) from member where MemberID = @MemberID 
  if exists(select * from Member where MemberID <> @MemberID and MemberRegisterMobile = @MemberRegisterMobile)
    return -33
    
  if @LanguageAbbr <> '' 
    select @Language = KeyID from LanguageMap where LanguageAbbr = @LanguageAbbr
 
  select top 1 @EmailAccount = ISNULL(AccountNumber,''), @OldEmailFlag = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 1
  select top 1 @SMSAccount = ISNULL(AccountNumber,''), @OldSMSFlag = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2

  -- ver 10.0.13
  if @OldPhoneFlag <> @AcceptPhoneAdvertising or
     @RecSMSAdv <> ISNULL(@OldSMSFlag, 0) or
     @RecEmailAdv <> ISNULL(@OldEmailFlag, 0)  
  begin
    set @NeedUpdateFlagDate = 1
    --update Member set PromotionFlagUpdatedOn = GETDATE() where MemberID = @MemberID 
  end else
    set @NeedUpdateFlagDate = 0  
  -- end
  
  -- Ver 1.0.0.10  
  if (@SMSAccount <> RTrim(@CountryCode) + @MemberMobilePhone)
  begin
    select top 1 @OtherMemberID = MemberID, @OtherUpdateDate = UpdatedOn 
      from MemberMessageAccount where MemberID <> @MemberID and MessageServiceTypeID = 2 and AccountNumber = RTrim(@CountryCode) + @MemberMobilePhone 
      order by UpdatedOn desc
    if isnull(@OtherMemberID, 0) > 0
    begin
      select Top 1 @MobileProtectPeriodValue = MobileProtectPeriodValue from Card C 
        left join CardGrade G on C.CardGradeID = G.CardGradeID
       where MemberID = @OtherMemberID 
      if isnull(@MobileProtectPeriodValue, 0) > 0
      begin
        if dateadd(m, @MobileProtectPeriodValue, @OtherUpdateDate) > GETDATE()
          return -204   -- 手机号码不可更改.
      end  
    end 
  end  
  --------------------------------------------------------
 
 /*   
   -- (@OldMemberRegisterMobile <> @MemberRegisterMobile) or 
  if (@EmailAccount <> @MemberEmail) or (@SMSAccount <> RTrim(@CountryCode) + @MemberMobilePhone)
  begin
    set @NeedSendMsg = 1
    update MemberMessageAccount set VerifyFlag = 0 
        where MemberID = @MemberID and MessageServiceTypeID in (1,2)  
  end  
*/
  
  update Member set MemberRegisterMobile = @MemberRegisterMobile, MemberMobilePhone = @MemberMobilePhone, CountryCode = @CountryCode, MemberEmail = @MemberEmail,  
	NickName = @NickName, MemberEngFamilyName = @MemberEngFamilyName, MemberEngGivenName = @MemberEngGivenName, MemberChiFamilyName = @MemberChiFamilyName, 
	MemberChiGivenName = @MemberChiGivenName, MemberSex = @MemberSex, MemberDateOfBirth = @MemberDateOfBirth, MemberMarital = @MemberMarital,
	MemberIdentityType = @MemberIdentityType, MemberIdentityRef = @MemberIdentityRef, EducationID = @EducationID, ProfessionID = @ProfessionID, 
	MemberPosition = @MemberPosition, NationID = @NationID, HomeTelNum = @HomeTelNum, HomeAddress = @HomeAddress, OtherContact = @OtherContact,
	Hobbies = @Hobbies, SpRemark = @SpRemark, MemberPictureFile = @MemberPictureFile,
	ReceiveAllAdvertising = @ReceiveAllAdvertising, TransPersonalInfo = @TransPersonalInfo,
	CompanyDesc = @CompanyDesc, OfficeAddress = @OfficeAddress, ViewHistoryTranDays = @ViewHistoryTranDays,
	AcceptPhoneAdvertising = @AcceptPhoneAdvertising, MemberDefLanguage = case when isnull(@Language,0)=0 then MemberDefLanguage else @Language end,
	MemberAppellation = @MemberAppellation, AgeBracklet=@AgeBracklet, UpdatedOn = GETDATE()
  where MemberID = @MemberID
  
  if @EmailAccount is not null
  begin
    if isnull(@MemberEmail,'') <> isnull(@EmailAccount, '')
    begin
      set @NeedSendMsg = 1
      update MemberMessageAccount set AccountNumber = @MemberEmail, VerifyFlag = 1, PromotionFlag = @RecEmailAdv 
        where MemberID = @MemberID and MessageServiceTypeID = 1 and AccountNumber = @EmailAccount
      -- ver 1.0.0.17     
      update MemberMessageAccount set VerifyFlag = 1
        where MemberID = @MemberID and MessageServiceTypeID in (1,2)          
    end else
      update MemberMessageAccount set PromotionFlag = @RecEmailAdv 
        where MemberID = @MemberID and MessageServiceTypeID = 1 and AccountNumber = @EmailAccount        
  end else
  begin 
    if isnull(@MemberEmail, '') <> ''    
      insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
         CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag, PromotionFlag)
      values(@MemberID, 1, @MemberEmail, 1, '', 1, getdate(), @MemberID, getdate(), @MemberID, 1, @RecEmailAdv)
  end

  if @SMSAccount is not null
  begin
    if RTrim(isnull(@CountryCode,'')) + isnull(@MemberMobilePhone,'') <> isnull(@SMSAccount, '')
    begin
      set @NeedSendMsg = 1
      update MemberMessageAccount set AccountNumber = RTrim(@CountryCode) + @MemberMobilePhone, VerifyFlag = 0, PromotionFlag = @RecSMSAdv 
        where MemberID = @MemberID and MessageServiceTypeID = 2 and AccountNumber = @SMSAccount
    end else
       update MemberMessageAccount set PromotionFlag = @RecSMSAdv 
        where MemberID = @MemberID and MessageServiceTypeID = 2 and AccountNumber = @SMSAccount     
  end
 /*  ver 1.0.1.19
  else
  begin
    if isnull(@MemberMobilePhone, '') <> ''
      insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
         CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag, PromotionFlag)
      values(@MemberID, 2, RTrim(isnull(@CountryCode,'')) + @MemberMobilePhone, 1, '', 1, getdate(), @MemberID, getdate(), @MemberID, 0, @RecSMSAdv)
  end
*/  
  -- 写入地址表
  if not exists(select * from MemberAddress where MemberID = @MemberID)
  begin
    insert into MemberAddress (MemberID, MemberFirstAddr, AddressCountry, AddressProvince, AddressCity, AddressDistrict,
      AddressDetail, AddressFullDetail, CreatedBy, UpdatedBy, CreatedOn, UpdatedOn)
    values (@MemberID, 1, @AddressCountry, @AddressProvince, @AddressCity, @AddressDistrict, @AddressDetail,
      @HomeAddress, @MemberID, @MemberID, GETDATE(), GETDATE())      
  end else
  begin
    update MemberAddress set AddressCountry = @AddressCountry, AddressProvince = @AddressProvince, 
      AddressCity = @AddressCity, AddressDistrict = @AddressDistrict,
      AddressDetail = @AddressDetail, AddressFullDetail = @HomeAddress
    where MemberID = @MemberID 
  end
  
  if @NeedSendMsg = 1
  begin
  -- 产生消息
    declare @Tempstr varchar(max)
    set @Tempstr = 'MSGACCOUNTTYPE=1;MSGACCOUNT=' +  @MemberEmail 
  
    exec GenMessageJSONStr 'SetMemberInfo', @Tempstr, @ReturnJSONMsg output       
  
  end  
  --exec RecordUserAction @MemberID, 2, '', '', 'Update Member Information', '', '', '', @MemberID
  
  -- Ver 1.0.0.14
  if @NeedUpdateFlagDate = 1
    update Member set PromotionFlagUpdatedOn = GETDATE() where MemberID = @MemberID 

  return 0
end

GO
