USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardAdjustment]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[MemberCardAdjustment]
  @UserID               varchar(10),
  @CardNumber			nvarchar(30),      -- 指定卡调整时填写卡号
  @OprID                int,			   -- 操作代码。 22：操作金额， 23：操作积分
  @TxnKeyID				varchar(30),
  @TxnNo				varchar(30),
  @BusDate				datetime, 
  @TxnDate				datetime,
  @ActAmount            money,             -- 操作金额（有正负）
  @ActPoints            int,			   -- 操作积分（有正负）
  @Remark				varchar(100),
  @SecurityCode			varchar(300)
AS
/****************************************************************************
**  Name : MemberCardAdjustment 
**  Version: 1.0.0.0
**  Description : 会员卡金额积分调整 (指定卡) (未使用)
**  Parameter :
**
  declare @MemberID varchar(36), @A int
  exec @A = MemberCardAdjustment 
  print @A
  print @MemberID
**  Created by: Gavin @2012-04-10
**
****************************************************************************/
begin
  declare @CardStatus int, @TotalAmount money, @TotalPoints int  
  
  select @CardStatus = Status, @TotalAmount = TotalAmount, @TotalPoints = TotalPoints  from Card where CardNumber = @CardNumber
  if isnull(@CardStatus, 0) <> 1 
    return -2
  if (isnull(@TotalAmount,0) - @ActAmount < 0) or (isnull(@TotalPoints,0) - @ActPoints < 0)
    return -10

  insert into Card_Movement
    (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
     CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy)
  values
    (@OprID, @CardNumber, null, @TxnKeyID, @TxnNo, @TotalAmount, @ActAmount, @TotalAmount + @ActAmount, @ActPoints, @BusDate, @TxnDate, 
     null, null, @remark, @SecurityCode, @UserID)
        
  return 0
end

GO
