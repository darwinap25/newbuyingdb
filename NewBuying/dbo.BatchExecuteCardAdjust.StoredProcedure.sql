USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BatchExecuteCardAdjust]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[BatchExecuteCardAdjust]
  @CardAdjustNo                    varchar(64),
  @IsOrderApprove                  int=0
AS
/****************************************************************************
**  Name : BatchExecuteCardAdjust
**  Version: 1.0.0.2
**  Description : 执行已经批核的还未执行过的，生效时间是当天的（或者已经过期的）Ord_CardAdjust_H订单。
**  Created by:  Gavin @2017-01-16
**  Modify by Gavin @2017-02-10 (ver 1.0.0.1) （for SWD） 增加处理 OprID = 111,68 （card状态变为 9）， 激活和充值时， 都修改 card状态为 激活。
**  Modify by Gavin @2017-02-13 (ver 1.0.0.2) （for SWD） 增加参数@IsOrderApprove， 0： 表示EOD时自动运行。 1：表示 Approve时运行。 默认0。   EOD时运行，EffectDate是第二天的记录也要执行。  Approve时，只执行EffectDate是当天的
****************************************************************************/
begin
  declare @CardAdjustNumber varchar(512), @OprID int, @RefTxnNo varchar(512), @TxnDate datetime, @ServerCode varchar(512), 
          @RegisterCode varchar(512), @ReasonID int, @ActAmountH money, @ActPointsH int, @ActExpireDate datetime, @ApproveStatus char(1), @CreatedBy int
  declare @KeyID int, @CardNumber varchar(512), @OrderAmount money, @ActAmount money, @OrderPoints int, @ActPoints int 
  declare @OldApproveStatus char(1), @OpenBal money, @CloseBal money, @Additional nvarchar(512), @Remark nvarchar(512), @CardExpiryDate datetime
  declare @ApprovalCode char(6), @ActStatus int, @OrgStatus int, @NewStatus int
  declare @BusDate date, @StoreID int, @NewAdditional nvarchar(512)
  declare @IssueStoreID int, @CardTypeID int, @CardGradeID int, @Note varchar(512)
  declare @CurrDate Datetime

  set @CardAdjustNo = isnull(@CardAdjustNo, '')
  if isnull(@IsOrderApprove, 0) = 0
    set @CurrDate = Getdate() + 1
  else
    set @CurrDate = Getdate() 

  DECLARE CUR_CARDADJUST CURSOR fast_forward local FOR
    SELECT CardAdjustNumber, OprID, OriginalTxnNo, isnull(TxnDate,getdate()), ServerCode, RegisterCode, 
        ReasonID, ActAmount, ActPoints, ActExpireDate, ApproveStatus, ApproveBy, ActStatus, StoreID, isnull(Note,'')
	from ord_cardadjust_h
	where (Approvestatus = 'A') and ((CardAdjustNumber = @CardAdjustNo) or (@CardAdjustNo = ''))
	      and DateDiff(dd, EffectiveDate, @CurrDate) >= 0
  OPEN CUR_CARDADJUST
  FETCH FROM CUR_CARDADJUST INTO @CardAdjustNumber, @OprID, @RefTxnNo, @TxnDate, @ServerCode, @RegisterCode, 
      @ReasonID, @ActAmountH, @ActPointsH, @ActExpireDate, @ApproveStatus, @CreatedBy, @ActStatus, @StoreID, @Note
  WHILE @@FETCH_STATUS=0
  BEGIN
      DECLARE CUR_Ord_CardAdjust_D CURSOR fast_forward local FOR
        SELECT KeyID, CardNumber, OrderAmount, ActAmount, OrderPoints, ActPoints, isnull(Additional1,'')
         FROM Ord_CardAdjust_D where CardAdjustNumber = @CardAdjustNumber         
      OPEN CUR_Ord_CardAdjust_D
      FETCH FROM CUR_Ord_CardAdjust_D INTO @KeyID, @CardNumber, @OrderAmount, @ActAmount, @OrderPoints, @ActPoints, @Additional
      WHILE @@FETCH_STATUS=0
      BEGIN
        select @OpenBal = isnull(TotalAmount,0), @CardExpiryDate = CardExpiryDate, @OrgStatus = Status,
            @IssueStoreID = IssueStoreID, @CardTypeID = CardTypeID, @CardGradeID = CardGradeID
          from Card where CardNumber = @CardNumber 
        set @ActAmount = isnull(@ActAmount, 0)
        set @ActPoints = ISNULL(@ActPoints, 0)
        set @CloseBal = ISNULL(@OpenBal, 0)
        set @OrderAmount = ISNULL(@OrderAmount, 0)
        set @OrderPoints = ISNULL(@OrderPoints, 0) 
        set @NewStatus = @OrgStatus   
        
		if @OprID in (2,68)
		begin
		  if @ActAmount = 0
		    set @ActAmount = isnull(@ActAmountH, 0)
		end

		if @OprID = 111
		  set @NewStatus = 9 
        if @OprID = 29
          set @NewStatus = @ActStatus
        if @OprID <> 24
          set @ActExpireDate = @CardExpiryDate         
        if @OprID in (5, 21, 24)
        begin
		  set @NewStatus = 2
          set @CloseBal = @OpenBal 
          set @ActAmount = 0
          set @ActPoints = 0
        end
        if @OprID in (22, 52)
        begin
		  set @NewStatus = 2
          set @CloseBal = @OpenBal + @OrderAmount
          set @ActAmount = @OrderAmount
          set @ActPoints = 0
        end
        if @OprID = 23
        begin
          set @CloseBal = @OpenBal
          set @ActAmount = 0
          set @ActPoints = @OrderPoints
        end  
        if @OprID in (3)
        begin
          set @CloseBal = @OpenBal - abs(@OrderAmount)
          set @ActAmount = -abs(@OrderAmount)
          set @ActPoints = 0
        end    
        if @OprID in (50, 60)
        begin
          set @CloseBal = @OpenBal - abs(@ActAmount)
          set @ActAmount = -abs(@ActAmount)
          set @ActPoints = 0
        end  
        
        -- ver 1.0.0.14
        set @NewAdditional = @Additional
        if ISNULL(@Additional, '') <> '' and ISNULL(@Note, '') <> ''  
          set @NewAdditional = @Additional + ',' + @Note
        ----                   
        insert into Card_Movement
             (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
              CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus,
              StoreID, RegisterCode, ServerCode)
        values
             (@OprID, @CardNumber, null, null, @CardAdjustNumber, @OpenBal, @ActAmount, @CloseBal, @ActPoints, @BusDate, @TxnDate, null,
              null, null, @NewAdditional, @Remark, '', @CreatedBy, @CardExpiryDate, @ActExpireDate, @OrgStatus, @NewStatus,
              @StoreID, @RegisterCode, @ServerCode)   
        
        -- 库存数量变动
        if @OprID = 28
          Update Card set StockStatus = 6, IssueStoreID = @StoreID where CardNumber = @CardNumber

        -- ver  1.0.0.11
 --       if @OprID = 5
 --       begin
 --         delete from WalletRule_D where StoreID = @IssueStoreID and CardTypeID = @CardTypeID and @CardGradeID = CardGradeID          
 --       end
        
        -- 发消息
        if @OprID in (50, 51, 52, 60)
          exec GenUserMessage @CreatedBy, 24, 0, @CardAdjustNumber
        else exec GenUserMessage @CreatedBy, 5, 0, @CardAdjustNumber         
        -----------------------  
                                  
        FETCH FROM CUR_Ord_CardAdjust_D INTO @KeyID, @CardNumber, @OrderAmount, @ActAmount, @OrderPoints, @ActPoints, @Additional
      END
      CLOSE CUR_Ord_CardAdjust_D 
      DEALLOCATE CUR_Ord_CardAdjust_D  
	             
    -- 库存数量变动
    if @OprID = 28
    begin
      exec ChangeCardStockStatus 8, @CardAdjustNumber, 1     
    end
            
    exec GenApprovalCode @ApprovalCode output      
    update Ord_CardAdjust_H set ApprovalCode = @ApprovalCode, ApproveBusDate = @BusDate, ApproveOn = getdate(), ApproveBy = @CreatedBy,
	  ApproveStatus = 'C'
    where CardAdjustNumber = @CardAdjustNumber 
    
	FETCH FROM CUR_CARDADJUST INTO @CardAdjustNumber, @OprID, @RefTxnNo, @TxnDate, @ServerCode, @RegisterCode, 
      @ReasonID, @ActAmountH, @ActPointsH, @ActExpireDate, @ApproveStatus, @CreatedBy, @ActStatus, @StoreID, @Note
  END
  CLOSE CUR_CARDADJUST
  DEALLOCATE CUR_CARDADJUST
end

GO
