USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ChangeReserverByPickOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[ChangeReserverByPickOrder]
  @UserID				         INT,	            -- 操作员ID
  @SalesPickOrderNumber          VARCHAR(64),       -- 拣货单号码
  @OldPickStoreCode              VARCHAR(64),       -- 修改前的storecode
  @NewPickStoreCode              VARCHAR(64),       -- 修改后的storecode
  @TxnNo                         VARCHAR(64)        -- 交易号
AS
/****************************************************************************
**  Name : ChangeReserverByPickOrder   
**  Version: 1.0.0.1
**  Description : SalesPicking单改变提货的storecode时,需要释放原先的Reserver库存,改成新的storecode的Reserver. (只有预扣才需要这么做. 直接扣库存的则不需要此操作)
                  Picking单没有批核,理论上来说,预扣的货应该还在的(R的库存数量总是应该够的)
exec ChangeReserverByPickOrder 1, 'SSO000000000007','KTXNNO000000440',
SELECT* FROM STK_StockMovement
SELECT* FROM Ord_SalesPickOrder_D
SELECT* FROM Ord_SalesPickOrder_H
**  Created by: Gavin @2016-10-19  
**  Modify By Gavin @2017-04-18 (1.0.0.1) 如果传入的@NewPickStoreCode为空, 表示调用方是void 被Reserver库存. 则只做回滚部分功能,不需要重新Reserver
****************************************************************************/
BEGIN
  DECLARE @OldStoreID INT, @NewStoreID INT, @WHStoreID INT
  
  SET @OldPickStoreCode = ISNULL(@OldPickStoreCode, '')
  SET @NewPickStoreCode = ISNULL(@NewPickStoreCode, '')
  SELECT @OldStoreID = StoreID FROM BUY_STORE WHERE StoreCode = @OldPickStoreCode
  SELECT @NewStoreID = StoreID FROM BUY_STORE WHERE StoreCode = @NewPickStoreCode
  SELECT @WHStoreID = StoreID FROM BUY_STORE WHERE StoreCode = 'WH'


  -- 回滚原先的Stock上的Reserver库存. 包括WH 的. 重新调用ReserveSalesStock来预扣库存 
	INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
		OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	SELECT 3, @OldStoreID, 'G', D.ProdCode, @TxnNo, '', M.Busdate, M.Txndate,
			isnull(O.OnhandQty,0),  isnull(D.OrderQty,0), isnull(O.OnhandQty,0) + isnull(D.OrderQty,0), M.SerialNoType, M.SerialNo, '', GETDATE(), @UserID
	  FROM Ord_SalesPickOrder_D D 
	  LEFT JOIN Ord_SalesPickOrder_H H ON H.SalesPickOrderNumber = D.SalesPickOrderNumber
	  LEFT JOIN (SELECT * FROM STK_StockMovement WHERE ReferenceNo = @TxnNo AND StoreID = @OldStoreID AND StockTypeCode = 'G') M ON D.ProdCode = M.ProdCode
	  LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StoreID = @OldStoreID AND StockTypeCode = 'G') O ON O.ProdCode = M.ProdCode
	WHERE H.SalesPickOrderNumber = @SalesPickOrderNumber AND D.StockTypeCode = 'G'

	INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
		OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	SELECT 3, @OldStoreID, 'R', D.ProdCode, @TxnNo, '', M.Busdate, M.Txndate,
			isnull(O.OnhandQty,0),  -isnull(D.OrderQty,0), isnull(O.OnhandQty,0) - isnull(D.OrderQty,0), M.SerialNoType, M.SerialNo, '', GETDATE(), @UserID
	  FROM Ord_SalesPickOrder_D D 
	  LEFT JOIN Ord_SalesPickOrder_H H ON H.SalesPickOrderNumber = D.SalesPickOrderNumber
	  LEFT JOIN (SELECT * FROM STK_StockMovement WHERE ReferenceNo = @TxnNo AND StoreID = @OldStoreID AND StockTypeCode = 'R') M ON D.ProdCode = M.ProdCode
	  LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StoreID = @OldStoreID AND StockTypeCode = 'R') O ON O.ProdCode = M.ProdCode
	WHERE H.SalesPickOrderNumber = @SalesPickOrderNumber

  -- 重新Reserver
  -- ver 1.0.0.1
  IF ISNULL(@NewStoreID, 0) <> 0 
    EXEC ReserveSalesStock_ByPickOrder @UserID, @SalesPickOrderNumber, @TxnNo, @NewStoreID

END

GO
