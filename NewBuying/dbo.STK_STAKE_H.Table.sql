USE [NewBuying]
GO
/****** Object:  Table [dbo].[STK_STAKE_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STK_STAKE_H](
	[StockTakeNumber] [varchar](64) NOT NULL,
	[StockTakeDate] [datetime] NULL DEFAULT (getdate()),
	[StoreID] [int] NULL,
	[Status] [int] NULL DEFAULT ((0)),
	[StockTakeType] [int] NULL DEFAULT ((0)),
	[Remark] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[StockTakeID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_STK_STAKE_H] PRIMARY KEY CLUSTERED 
(
	[StockTakeNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'盘点号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H', @level2type=N'COLUMN',@level2name=N'StockTakeNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'盘点日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H', @level2type=N'COLUMN',@level2name=N'StockTakeDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'盘点的店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 
1：注册结束。 
2：第一次盘点结束。 
3：第二次盘点结束。
4：产生差异表。（3 后自动产生差异表，则跳过4，直接到5）
5：盘点结束，并根据差异表产生调整单。
6：取消盘点 （作废）。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'盘点类型。 0：只盘数量。 1：盘点serialno，每条记录数量都是1.  默认0.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H', @level2type=N'COLUMN',@level2name=N'StockTakeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'为方便UI开发，增加的自增长序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H', @level2type=N'COLUMN',@level2name=N'StockTakeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存盘点头表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKE_H'
GO
