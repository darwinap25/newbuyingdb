USE [NewBuying]
GO
/****** Object:  Table [dbo].[ReceiveTxn]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReceiveTxn](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[BrandCode] [varchar](64) NULL,
	[StoreCode] [varchar](64) NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[TxnNoSN] [varchar](512) NULL,
	[TxnNo] [varchar](512) NULL,
	[BusDate] [datetime] NULL,
	[TxnDate] [datetime] NULL,
	[CardNumber] [varchar](64) NULL,
	[OprID] [int] NOT NULL,
	[Amount] [money] NULL,
	[Points] [int] NULL DEFAULT ((0)),
	[Status] [int] NOT NULL,
	[CouponNumber] [varchar](64) NULL,
	[CouponCount] [int] NULL DEFAULT ((1)),
	[VoidKeyID] [int] NULL,
	[VoidTxnNo] [varchar](512) NULL,
	[TenderID] [int] NULL,
	[Additional] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NOT NULL,
	[Remark] [varchar](512) NULL,
	[SecurityCode] [varchar](512) NULL,
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[UpdatedDate] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [varchar](512) NULL,
	[UpdatedBy] [varchar](512) NULL,
	[Approvedby] [varchar](512) NULL,
	[ApprovedDate] [datetime] NULL,
 CONSTRAINT [PK_RECEIVETXN] PRIMARY KEY NONCLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_ReceiveTxn]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_ReceiveTxn] ON [dbo].[ReceiveTxn]
after INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_ReceiveTxn
* Version: 1.0.0.12
* Description :
*     ReceiveTxn表的Update trigger，ReceiveTxn数据批核后（ApproveStatus： P->A），产生Card_Movement, Coupon_Movement数据，并通过Movement触发器实现到Card,Coupon等表。
*  select * from receivetxn
select * from card_movement
* Create By Gavin @2011-09-20 - @2011-10-18 for SOGO
* Modify By Gavin @2011-12-09 for SOGO 直接insert ApproveStatus = 'A' 的记录时， 也要触发这个触发器。
* Modify By Gavin @2012-08-10 (ver 1.0.0.1) 如果是销售时， cardnumber需要使用receivetxn中的，而不是coupon中的
* Modify By Gavin @2012-08-27 (ver 1.0.0.2) 因为要计算coupon的 forfeit, 所以使用coupon时,要计算closebal,不再是使用全部的coupon金额
* Modify By Gavin @2012-08-31 (ver 1.0.0.3) 修正使用coupon时（OprID=34）填写actamount， closebal不正确的问题
* Modify By Robin @2012-09-20 (ver 1.0.0.4) POS 交易跳过 receivetxn触发器. (Oprid = 53,54)
* Modify By Gavin @2012-11-21 (ver 1.0.0.5) 修正计算Coupon New expirydate后,填写到couponmovement的错误.
* Modify By Gavin @2012-12-31 (ver 1.0.0.6) 游标加上Local， 加快执行速度
* Modify By Gavin @2013-03-07 (ver 1.0.0.7) 增加OprID=56的处理(POS提交Postvoid),和53,54相同处理,跳过此触发器.
* Modify By Gavin @2013-03-12 (ver 1.0.0.8) 增加OprID=57,58,59的处理(POS提交的Card操作),和53,54相同处理,跳过此触发器.
* Modify By Gavin @2014-07-31 (ver 1.0.0.9) 处理POS提交的 53 到 59 的 记录。 需要忽略 负数的 记录。
* Modify By Gavin @2014-07-31 (ver 1.0.0.10)处理POS提交的 50 的 记录。50: TelCo 卡充值(店铺扣除)（实际操作：扣除店铺的TelCo卡中金额，成功后，调用程序调用外部接口执行操作（比如调用电信接口给SIM卡充值）。 SVA 端动作类似OprID：3）。但是没有积分,有效期等变动,只是简单的扣除金额
* Modify By Gavin @2015-06-24 (ver 1.0.0.11)接受53,54等POS的操作
* Modify By Gavin @2015-07-24 (ver 1.0.0.12)写入card_movement时也写入 ApprovalCode
*/
/*==============================================================*/
BEGIN
  declare @KeyID varchar(30), @ShopCode varchar(10), @RegisterCode varchar(10), @TxnNoSN varchar(30), @TxnNo varchar(30), @BusDate datetime, @TxnDate datetime, @CardNumber nvarchar(30), @CouponCardNumber nvarchar(30), 
          @CardTypeID int, @OprID int, @Amount money, @Status int, @VoidKeyID varchar(30), @VoidTxnNo varchar(30), @Additional varchar(200), @Points int,
          @Remark varchar(200), @SecurityCode varchar(300), @ServerCode varchar(10), @ApproveStatus char(1), @ApprovedBy varchar(10), @TenderID int
  declare @TotalAmount money, @OpenBal money, @CloseBal money, @BatchID varchar(60), 
		  @UID varchar(60), @RefKeyID int, @VoidMovementKeyID int
  declare @CouponNumber varchar(512), @CouponStatus int, @CouponCount int, @ApprovalCode varchar(512), @NewCouponStatus int
  declare @CouponLength int, @CouponNumberEnd Varchar(512), @ActAmount money
  declare @CouponExpiryDate datetime, @CouponTypeID int, @NewCouponExpiryDate datetime
  declare @BrandCode varchar(512),@BrandID int, @StoreID int
  declare @CouponOpenBal money, @CouponCloseBal money

  declare @OldSTATUS char(1), @CardTotalAmt money , @Card_CardType varchar(40), @CardStatus int, @CardTotalPoints int
  DECLARE CUR_RECTXN CURSOR fast_forward local for
    SELECT KeyID, OprID, BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber,CouponNumber, isnull(Amount,0), isnull(CouponCount,0),isnull(Points,0),
      VoidKeyID, VoidTxnNo, Additional, Remark, SecurityCode, ApproveStatus, ApprovedBy, ApprovalCode, TenderID
     FROM INSERTED where [status] = 0
  OPEN CUR_RECTXN
  FETCH FROM CUR_RECTXN INTO @KeyID, @OprID, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber,@CouponNumber, @Amount, 
  @CouponCount, @Points, @VoidKeyID, @VoidTxnNo, @Additional, @Remark, @SecurityCode, @ApproveStatus, @ApprovedBy,@ApprovalCode, @TenderID
  WHILE @@FETCH_STATUS=0
  BEGIN
--if @OprID not in (53, 54, 56, 57, 58, 59)  -- （先不改）
if @OprID > 0
begin
	select @BrandID=StoreBrandID from Brand where StoreBrandCode=@BrandCode
	select @StoreID=StoreID from Store where BrandID=@BrandID and StoreCode=@ShopCode    
    select @CardTotalAmt = TotalAmount, @CardTotalPoints = isnull(TotalPoints,0) from Card where CardNumber = @CardNumber
    set @OpenBal = isnull(@CardTotalAmt, 0)
    set @Amount = isnull(@Amount, 0)
    set @CloseBal = @OpenBal + @Amount
    select @OldSTATUS = ApproveStatus  FROM DELETED where KeyID = @KeyID
    IF ((@ApproveStatus = 'A') AND (@OldSTATUS = 'P') and UPDATE(ApproveStatus)) or ((@ApproveStatus = 'A') AND (@OldSTATUS is null))
    BEGIN 
      -- Void 需要更改原单数据。
      if @OprID = 4  
      begin     
        select @VoidMovementKeyID = KeyID from Card_Movement where RefReceiveKeyID = @VoidKeyID
        update ReceiveTxn set [status] = 1, UpdatedBy = @ApprovedBy, UpdatedDate = Getdate() where KeyID = @VoidKeyID and [Status] = 0     
      end  
      -- 作废card  
      if @OprID = 5           
--        update VenCardIDMap set ValidateFlag = 0 where CardNumber = @CardNumber and ValidateFlag = 1
        update CardUIDMap set status = 0 where CardNumber = @CardNumber 
        
       -- Card 相关操作进入Card_Movement  
      if @OprID < 31 or @OprID in (50,57,58)
        insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
           CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, ServerCode, RegisterCode, ApprovalCode)
        values
          (@OprID, @CardNumber, @VoidMovementKeyID, @KeyID, @TxnNo, @OpenBal, @Amount, @CloseBal, @Points, @BusDate, @TxnDate, @TenderID,
          null, null, @Additional, @Remark, @SecurityCode, @ApprovedBy, @StoreID, @ServerCode, @RegisterCode, @ApprovalCode)
          
      --处理Coupon交易
      --if @OprID in (33,34,35,53,54)
      if @OprID in (33,34,35,53,54)
      begin  
		-- Todo 异常处理 没有再次校验 
		set @CouponLength=len(cast(@CouponCount-1 as varchar(512)))
		set @CouponNumberEnd=Substring(@CouponNumber,1,len(@CouponNumber)-@CouponLength-1)+right('0'+Cast(Cast(Substring(@CouponNumber,len(@CouponNumber)-@CouponLength,@CouponLength+1) as int)+@CouponCount-1 as Varchar(512)),@CouponLength+1)
        DECLARE CUR_ReceiveTxnCoupon CURSOR fast_forward local FOR
			SELECT CouponNumber,isNull(CouponAmount,0), CouponExpiryDate, CardNumber, CouponTypeID, Status
			  from Coupon where CouponNumber between @CouponNumber and @CouponNumberEnd
        OPEN CUR_ReceiveTxnCoupon
        FETCH FROM CUR_ReceiveTxnCoupon INTO @CouponNumber,@CouponOpenBal, @CouponExpiryDate, @CouponCardNumber, @CouponTypeID, @CouponStatus
        WHILE @@FETCH_STATUS=0
        BEGIN
			if @OprID in (33,53)       -- 激活coupon， 使用输入的金额，作为coupon最终的实际金额
			begin
        set @CouponCloseBal=@Amount
			  set @ActAmount=@Amount-@CouponOpenBal
			end
			else if @OprID in (34,35,54)        
			begin
			  set @ActAmount = @Amount
			  set @CouponCloseBal = @CouponOpenBal + @Amount
			  set @CardNumber = @CouponCardNumber
			end
			
			exec CalcCouponNewStatus @CouponNumber, @CouponTypeID, @OprID, @CouponStatus, @NewCouponStatus output
			exec CalcCouponNewExpiryDate @CouponTypeID, @OprID, @CouponExpiryDate, @NewCouponExpiryDate output 
			insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
				BusDate, Txndate, Remark, SecurityCode, CreatedBy, OrgExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
				NewExpiryDate, OrgStatus, NewStatus)
			values
				(@OprID, @CardNumber, @CouponNumber, @CouponTypeID, '', @KeyID, @TxnNo, @CouponOpenBal, @ActAmount, @CouponCloseBal,
				@BusDate, @TxnDate, @Remark, '', @ApprovedBy, @CouponExpiryDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode,
				@NewCouponExpiryDate, @CouponStatus, @NewCouponStatus)
			FETCH FROM CUR_ReceiveTxnCoupon INTO @CouponNumber,@CouponOpenBal, @CouponExpiryDate, @CouponCardNumber, @CouponTypeID, @CouponStatus
		END
		CLOSE CUR_ReceiveTxnCoupon
		DEALLOCATE CUR_ReceiveTxnCoupon         
      end
      ---------------------------------------------------------------------------
    END
end    
    FETCH FROM CUR_RECTXN INTO @KeyID, @OprID, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber,@CouponNumber, @Amount, 
    @CouponCount, @Points, @VoidKeyID, @VoidTxnNo, @Additional, @Remark, @SecurityCode, @ApproveStatus, @ApprovedBy,@ApprovalCode, @TenderID         
  END
  CLOSE CUR_RECTXN 
  DEALLOCATE CUR_RECTXN   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'BrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调用方提供。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'TxnNoSN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'TxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易用卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作ID：
1、查询
2、充值
3、消费金额
4、void（被void的必须是本卡交易，要校验卡号是否和原单相同），return，staffadj 这类交易直接调用充值功能。
5、作废本卡
6、金额转出（包括积分）
7、金额转入（包括积分）
8、Return
9、卡过期，金额清空。
10、卡金额调整（直接增减金额）
11、批量卡激活(POS端)
12、手机充值
13、扣除积分（使用积分支付）
14、购买积分
15、兑换积分（使用卡中金额兑换积分）
16、旧卡换新卡时金额转出（包括积分）
17、旧卡换新卡时金额转入（包括积分）
21、卡激活
22、卡金额调整
23、卡积分调整
31、使用卡金额积分购买coupon并绑定
32、绑定coupon到卡
33、激活指定coupon
34、使用指定coupon
35、设置coupon无效（过期）
36、转赠coupon（转出）
37、转赠coupon（转入）
38、coupon有效期调整
39、Coupon绑定外部UID.(Coupon状态改为5)
40、发行优惠劵
41、优惠劵状态回滚（需要找到原状态）
46、捐赠金额或者积分（转出）
47、捐赠金额或者积分（转入）

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'Points'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调用时，业务逻辑判断。0 正常。 小于0异常：
0:成功。
-1：没有找到此用户。
-2：没有找到卡。
-3：用户密码不正确。
-4：卡密码不正确。
-5：会员状态不正确（已注销）。
-6：卡状态不正确（过期，作废...）。  
-7：没有输入MemberID
-8：没有输入CardNumber
-9: 需要重新设置密码

-10: 余额不够.
-11: 没有找到原单.
-12: 原单已经void不能再次void.
-13: 原单是void单，不能再void.
-14: 充值超过卡上限
-15: TxnNo已经存在（重复提交）

-99：卡号不对' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作的CouponNumber记录。CouponCount大于1时，为初始coupon。Coupon必须连号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'CouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作的Coupon数量。默认为1。 只有批量销售Coupon时，数量会大于1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'CouponCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果是void操作，有原单时，填写原单的receivetxn 的KeyID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'VoidKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果是void单，需要填写被void的原单号(TxnNo)。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'VoidTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息。银行卡支付时，填写卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用银行卡支付后返回的号码。或者SVA操作返回的ApprovalCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录状态。
P： 准备状态（未批核）。 A：已经批核。  V：已经作废' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注。（预留字段）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'签名校验字段。
使用CardNumber+Amount+OprID （去掉空格），进行加密。填入SecurityCode。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn', @level2type=N'COLUMN',@level2name=N'SecurityCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收到的操作指令记录表。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveTxn'
GO
