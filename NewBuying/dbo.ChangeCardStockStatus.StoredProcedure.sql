USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ChangeCardStockStatus]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[ChangeCardStockStatus]
  @TypeID                int,          -- 类型: 1. 补充货品规则运行,产生订单.  2. OrderToSupplier. 3: CardOrderForm (增加了是否产生picking单的状态). 4:收货单。 5：Picking单. 6；退货单, 7:送货单, 8:Card Issue.
  @OrderNumber           varchar(64),  -- 订单号码
  @Act                   int           -- 1. 批核动作.  2：作废。 3: 产生picking单.(特例，只对于CardOrderForm)
AS
/****************************************************************************
**  Name : ChangeCardStockStatus
**  Version: 1.0.0.5
**  Description : Card库存状态变化逻辑处理过程.(应订单操作产生的实体Card库存变动.) (ChangeCouponStockStatus 改写)
**  example :
**  Created by: Gavin @2014-10-09
**  Created by: Gavin @2014-11-05 (ver 1.0.0.1) Ord_CardPickup_H approve 时，update card status = 1
**  Created by: Gavin @2014-11-12 (ver 1.0.0.2) 增加店铺退货单 产生的收货单 做void时的处理. 
**  Created by: Gavin @2014-11-13 (ver 1.0.0.3) 收货单作废, 修改状态
**  Created by: Gavin @2014-11-26 (ver 1.0.0.4) 修正  @TypeID = 2 时  if exists(select * from Card_Onhand  的 where 条件. (漏了 cardgradeid 条件)
**  Created by: Gavin @2015-04-07 (ver 1.0.0.5) 增加card issue 时(OprID=28)的处理
**
****************************************************************************/
begin
  declare @StoreID int, @CardTypeID int, @CardCount int, @FromStoreID int, @ReceiveType int, @CardStockStatus int
  declare @PickQty int, @StartCardNum varchar(64), @EndCardNum varchar(64), @CardGradeID int
  declare @StoreTypeID int, @HQStoreID int

--  if @TypeID = 1    -- 产生Picking单时没有库存变动.
--  begin
--  end
  
  if @TypeID = 2     -- 给供应商订单批核 (@Act 只能是1， 不做判断)
  begin    
    DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
      select H.StoreID, D.CardTypeID, D.CardGradeID, count(*) as CardCount from Ord_OrderToSupplier_Card_D D 
         left join Ord_OrderToSupplier_Card_H H on H.OrderSupplierNumber = D.OrderSupplierNumber 
        where H.OrderSupplierNumber = @OrderNumber
      group by H.StoreID, D.CardTypeID, D.CardGradeID
    OPEN CUR_ChangeCardStockStatus
    FETCH FROM CUR_ChangeCardStockStatus INTO @StoreID, @CardTypeID, @CardGradeID, @CardCount
    WHILE @@FETCH_STATUS=0
    BEGIN 
      if exists(select * from Card_Onhand where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 1)
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
           where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 1 
      else
        Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
        values(@CardTypeID, @CardGradeID, 1, @StoreID, 0, 1, @CardCount, GETDATE(), 1)       
      FETCH FROM CUR_ChangeCardStockStatus INTO @StoreID, @CardTypeID, @CardGradeID, @CardCount
    END
    CLOSE CUR_ChangeCardStockStatus
    DEALLOCATE CUR_ChangeCardStockStatus 
    
    update Card set StockStatus = 1, IssueStoreID = @StoreID from Card C left join Ord_OrderToSupplier_Card_D D on C.CardNumber = D.FirstCardNumber
      where D.OrderSupplierNumber = @OrderNumber    
  end
    
  if @TypeID = 3      -- 店铺给总部的订单
  begin
    if @Act = 1       -- 只批核，未产生picking单, 但是Card库存需要预扣.....(因为没有选中Card，所以Card上的PickingFlag没有变化，只是Card_Onhand中有变化)
    begin    
      DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR    
        select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(D.CardQty) as CardCount from Ord_CardOrderForm_D D 
           left join Ord_CardOrderForm_H H on H.CardOrderFormNumber = D.CardOrderFormNumber 
          where H.CardOrderFormNumber = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
      OPEN CUR_ChangeCardStockStatus
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      WHILE @@FETCH_STATUS=0
      BEGIN 
      -- 店铺没有收到货品，数量不变。 如果自动订货时需要去掉这部分数据，则减去Order中的数量。             
      -- 总部减去好货
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
           where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2  
       -- 总部加上4：Picked (總部收到了店舖的訂單)
        if exists(select * from Card_Onhand where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID  
                   and CardStatus = 0 and CardStockStatus = 4)
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
             where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID  
                     and CardStatus = 0 and CardStockStatus = 4
        else       
          Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CardTypeID, @CardGradeID, 1, @FromStoreID, 0, 4, @CardCount, GETDATE(), 1)              
                                  
        FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      END
      CLOSE CUR_ChangeCardStockStatus
      DEALLOCATE CUR_ChangeCardStockStatus 
    end
    else if @Act = 2      -- void. 只有已经批核过或者 产生过程Picking单的,在void时需要调用
    begin    
      DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR    
        select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(D.CardQty) as CardCount from Ord_CardOrderForm_D D 
           left join Ord_CardOrderForm_H H on H.CardOrderFormNumber = D.CardOrderFormNumber 
          where H.CardOrderFormNumber = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
      OPEN CUR_ChangeCardStockStatus
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      WHILE @@FETCH_STATUS=0
      BEGIN                  
      -- 总部减去Picking的库存数量
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
           where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 4 

      -- 总部好货调整
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount   
           where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2              
        FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      END
      CLOSE CUR_ChangeCardStockStatus
      DEALLOCATE CUR_ChangeCardStockStatus 
    end    
    else if @Act = 3            -- 产生picking单。 （需要从Picking单中获取数据）
    begin  
      DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR    
        select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(isnull(D.PickQty,0)) from Ord_CardPicking_D D 
           left join Ord_CardPicking_H H on H.CardPickingNumber = D.CardPickingNumber
          where H.ReferenceNo = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
      OPEN CUR_ChangeCardStockStatus
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @PickQty
      WHILE @@FETCH_STATUS=0
      BEGIN 
      -- 这一步更改数量，目的是处理 OrderQty 和Picking Qty不同的情况。     
      -- 总部数量调整 （4：Picked (總部收到了店舖的訂單)）
        select @CardCount = sum(CardQty) from Ord_CardOrderForm_D 
          where CardOrderFormNumber = @OrderNumber and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
          
        if @PickQty <> @CardCount
        begin
          if exists(select * from Card_Onhand 
                      where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID 
                            and CardStatus = 0 and CardStockStatus = 4)
            update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + (@PickQty - @CardCount)  
               where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 4
          else       
            Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
            values(@CardTypeID, @CardGradeID, 1, @FromStoreID, 0, 4, @PickQty, GETDATE(), 1)    
            
        -- 总部好货调整
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - (@PickQty - @CardCount)   
             where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2                              
        end                                  
        FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @PickQty
      END
      CLOSE CUR_ChangeCardStockStatus
      DEALLOCATE CUR_ChangeCardStockStatus     
    end
  end
  
  if @TypeID = 4      -- 收货单批核。(@Act 只能是1， 不做判断)（收货单都是总部的。 店铺收货只有批核delivery单）
  begin
    DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
      select H.SupplierID, H.StoreID, D.CardTypeID, D.CardGradeID, D.CardStockStatus, sum(D.OrderQty) as CardCount, max(ReceiveType)
        from Ord_CardReceive_D D 
         left join Ord_CardReceive_H H on H.CardReceiveNumber = D.CardReceiveNumber 
        where H.CardReceiveNumber = @OrderNumber
      group by H.SupplierID, H.StoreID, D.CardTypeID, D.CardGradeID, D.CardStockStatus
    OPEN CUR_ChangeCardStockStatus
    FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardStockStatus, @CardCount, @ReceiveType
    WHILE @@FETCH_STATUS=0
    BEGIN 
      if @ReceiveType = 1  -- 供应商订单产生的收货单。
      begin
        if @Act = 1
        begin
          -- 加上好货
          if @CardStockStatus = 2
          begin
            if exists(select * from Card_Onhand where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2)
              update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
                 where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2 
            else
              Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
              values(@CardTypeID, @CardGradeID, 1, @StoreID, 0, 2, @CardCount, GETDATE(), 1)
      
            -- 减去订单的 For Printing 数量. 没有的话是数据错误
            update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
                 where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 1   

            -- 好货做Card状态变动
            update Card set Status = 0, StockStatus = 2, PickupFlag = 0, IssueStoreID = @StoreID 
              where CardNumber in (select FirstCardNumber from Ord_CardReceive_D 
                                     where CardReceiveNumber = @OrderNumber and CardStockStatus = 2 and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID)
          end 
          -- 坏货
          if @CardStockStatus = 3
          begin
            if exists(select * from Card_Onhand where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 3)
              update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
                 where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 3 
            else
              Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
              values(@CardTypeID, @CardGradeID, 1, @StoreID, 0, 3, @CardCount, GETDATE(), 1)
      
            -- 减去订单的 For Printing 数量. 没有的话是数据错误
            update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
                 where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 1  
               
            -- 好货做Card状态变动
            update Card set Status = 0, StockStatus = 3, PickupFlag = 0, IssueStoreID = @StoreID 
              where CardNumber in (select FirstCardNumber from Ord_CardReceive_D 
                                     where CardReceiveNumber = @OrderNumber and CardStockStatus = 3 and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID)               
          end 
        end ---else if @Act = 2    -- 供应商订单收货,不能void.

 /*       
        -- 未区分,需要另外产生收货单
        if @CardStockStatus = 0
        begin
          -- 减去订单的 For Printing 数量. 没有的话是数据错误
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
               where StoreID = @StoreID and CardTypeID = @CardTypeID and CardStatus = 0 and CardStockStatus = 1                         
        end 
*/                
      end
      else if @ReceiveType = 2  -- 店铺订单退货产生的收货单。
      begin 
        if @Act = 1
        begin   
          -- 总部加回好货  
          if exists(select * from Card_Onhand where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2)
            update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
              where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2 
          else
            Insert into Card_Onhand (CardTypeID, CardGradeID,  StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
            values(@CardTypeID, @CardGradeID, 1, @StoreID, 0, 2, @CardCount, GETDATE(), 1)
      
          -- 店铺扣除库存退货货品
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
            where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 7     
        
          -- Card状态变动  
          update Card set Status = 0, StockStatus = 2, PickupFlag = 0, IssueStoreID = @StoreID 
            where CardNumber in (select FirstCardNumber from Ord_CardReceive_D 
                                   where CardReceiveNumber = @OrderNumber and CardStockStatus = 2 and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID)              
        end else if @Act = 2
        begin
          -- 店铺扣除库存退货货品
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
            where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 7     
          -- 店铺加回库存货品
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
            where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 6         
        end
      end
      
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardStockStatus, @CardCount, @ReceiveType
    END
    CLOSE CUR_ChangeCardStockStatus
    DEALLOCATE CUR_ChangeCardStockStatus 
  end
   
  if @TypeID = 5      -- picking单。(@Act 只能是1， 不做判断)（picking单都是总部向店铺的）
  begin
    if @Act = 1 
    begin
      DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
        select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(isnull(D.PickQty,0)) from Ord_CardPicking_D D 
           left join Ord_CardPicking_H H on H.CardPickingNumber = D.CardPickingNumber
          where H.CardPickingNumber = @OrderNumber
          group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
      OPEN CUR_ChangeCardStockStatus
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      WHILE @@FETCH_STATUS=0
      BEGIN 
        -- 总部加上送货库存  
        if exists(select * from Card_Onhand where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 5)
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
            where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 5 
        else
          Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CardTypeID, @CardGradeID, 1, @FromStoreID, 0, 5, @CardCount, GETDATE(), 1)
      
        -- 总部扣除Pick库存
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
          where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 4         
      
        FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      END
      CLOSE CUR_ChangeCardStockStatus
      DEALLOCATE CUR_ChangeCardStockStatus 
      
      update card set Status = 1, StockStatus = 5, IssueStoreID = @FromStoreID 
        where CardNumber in (select FirstCardNumber from Ord_CardPicking_D where CardPickingNumber = @OrderNumber)         
    end 
    else if @Act = 2      -- void
    begin
      DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
        select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(isnull(D.OrderQty,0)), sum(isnull(D.PickQty,0)) from Ord_CardPicking_D D 
           left join Ord_CardPicking_H H on H.CardPickingNumber = D.CardPickingNumber
          where H.CardPickingNumber = @OrderNumber
          group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
      OPEN CUR_ChangeCardStockStatus
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount, @PickQty
      WHILE @@FETCH_STATUS=0
      BEGIN 
        -- 总部扣除Pick库存
        if (@PickQty <> @CardCount) 
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - (@PickQty - @CardCount)
            where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 4         
      
        FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount, @PickQty
      END
      CLOSE CUR_ChangeCardStockStatus 
      DEALLOCATE CUR_ChangeCardStockStatus 
      
     update card set PickupFlag = 0, StockStatus = 2, IssueStoreID = @FromStoreID 
        where CardNumber in (select FirstCardNumber from Ord_CardPicking_D where CardPickingNumber = @OrderNumber)   
    end  
  end  
    
  if @TypeID = 6      -- 退货单。
  begin
    DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
      select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(D.OrderQty) as CardCount from Ord_CardReturn_D D 
         left join Ord_CardReturn_H H on H.CardReturnNumber = D.CardReturnNumber 
        where H.CardReturnNumber = @OrderNumber
      group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
    OPEN CUR_ChangeCardStockStatus
    FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
    WHILE @@FETCH_STATUS=0
    BEGIN 
        -- 店铺加上库存退货货品  
        if exists(select * from Card_Onhand where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 7)
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
            where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 7 
        else
          Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CardTypeID, @CardGradeID, 2, @FromStoreID, 0, 7, @CardCount, GETDATE(), 1)
      
        -- 店铺扣除库存好货
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
          where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2
                                      
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
    END
    CLOSE CUR_ChangeCardStockStatus
    DEALLOCATE CUR_ChangeCardStockStatus 
    
    -- 更改Card库存状态
    update Card set StockStatus = 7
          where CardNumber in (select FirstCardNumber From Ord_CardReturn_D where CardReturnNumber = @OrderNumber)
  end 
   
  if @TypeID = 7      -- 送货单。(送货单批核后表示店铺已经收到）
  begin
    if @Act = 1
    begin
      DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
        select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(D.OrderQty) as CardCount from Ord_CardDelivery_D D 
           left join Ord_CardDelivery_H H on H.CardDeliveryNumber = D.CardDeliveryNumber
          where H.CardDeliveryNumber = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
      OPEN CUR_ChangeCardStockStatus
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      WHILE @@FETCH_STATUS=0
      BEGIN 
        -- 店铺加上送货数量  
        if exists(select * from Card_Onhand where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 6)
          update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
            where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 6
        else
          Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CardTypeID, @CardGradeID, 2, @StoreID, 0, 6, @CardCount, GETDATE(), 1)        
      
        -- 总部扣除运货
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
          where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 5  
                
        FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      END
      CLOSE CUR_ChangeCardStockStatus
      DEALLOCATE CUR_ChangeCardStockStatus
    end else if @Act = 2
    begin
      DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
        select H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID, sum(D.OrderQty) as CardCount from Ord_CardDelivery_D D 
           left join Ord_CardDelivery_H H on H.CardDeliveryNumber = D.CardDeliveryNumber
          where H.CardDeliveryNumber = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CardTypeID, D.CardGradeID
      OPEN CUR_ChangeCardStockStatus
      FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      WHILE @@FETCH_STATUS=0
      BEGIN 
        -- 总部扣除运货
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
          where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 5  
        -- 总部加上好货
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
          where StoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2
                   
        FETCH FROM CUR_ChangeCardStockStatus INTO @FromStoreID, @StoreID, @CardTypeID, @CardGradeID, @CardCount
      END
      CLOSE CUR_ChangeCardStockStatus
      DEALLOCATE CUR_ChangeCardStockStatus    
    end   
  end 
 
  if @TypeID = 8      -- Card issue。 (@Act 只能是1， 不做判断)
  begin    
    DECLARE CUR_ChangeCardStockStatus CURSOR fast_forward local FOR
      select H.StoreID, C.CardTypeID, C.CardGradeID, count(*) as CardCount from Ord_CardAdjust_D D 
         left join Ord_CardAdjust_H H on H.CardAdjustNumber = D.CardAdjustNumber
         left join Card C on C.CardNumber = D.CardNumber 
        where H.CardAdjustNumber = @OrderNumber
      group by H.StoreID, C.CardTypeID, C.CardGradeID
    OPEN CUR_ChangeCardStockStatus
    FETCH FROM CUR_ChangeCardStockStatus INTO @StoreID, @CardTypeID, @CardGradeID, @CardCount
    WHILE @@FETCH_STATUS=0
    BEGIN
      select @StoreTypeID = StoreTypeID from Store where StoreID = @StoreID
      -- 因为Ord_CardAdjust_D中不填总部.所以任意取一个总部
      select Top 1 @HQStoreID = StoreID from Card_Onhand where StoreTypeID = 1 and CardTypeID = @CardTypeID 
          and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 2
         order by OnhandQty desc
      
      -- 加上店铺的库存. 
      if exists(select * from Card_Onhand where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 1 and CardStockStatus = 6)
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CardCount  
           where StoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 1 and CardStockStatus = 6
      else
        Insert into Card_Onhand (CardTypeID, CardGradeID, StoreTypeID, StoreID, CardStatus, CardStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
        values(@CardTypeID, @CardGradeID, @StoreTypeID, @StoreID, 1, 6, @CardCount, GETDATE(), 1)
      -- 减去总部的库存.  
        update Card_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CardCount  
           where StoreID = @HQStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardStatus = 0 and CardStockStatus = 62

               
      FETCH FROM CUR_ChangeCardStockStatus INTO @StoreID, @CardTypeID, @CardGradeID, @CardCount
    END
    CLOSE CUR_ChangeCardStockStatus
    DEALLOCATE CUR_ChangeCardStockStatus 
    
    update Card set StockStatus = 1, IssueStoreID = @StoreID from Card C left join Ord_OrderToSupplier_Card_D D on C.CardNumber = D.FirstCardNumber
      where D.OrderSupplierNumber = @OrderNumber    
  end
     
  return 0
end

GO
