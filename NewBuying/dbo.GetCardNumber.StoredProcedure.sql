USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCardNumber]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCardNumber]
  @CardTypeID int,  
  @Status int,  
  @CardNumber varchar(64) output  
/****************************************************************************  
**  Name : GetCardNumber  
**  Version: 1.0.0.6 
**  Description : 获取可用的CardNumber，根据CardTypeID和Status  
**  Example :  
  declare @CardNumber varchar(64)  
  exec GetCardNumber 1,0, @CardNumber output  
  print @CardNumber
  select * from cardpool where status = 0 and cardtypeid = 1  
  delete from cardpool
**  Created by: Robin @2014-12-17  
**  1.0.0.2  Modify by: Robin @2014-12-18 修改重取记录逻辑，使用更新锁
**  1.0.0.3  Modify by: Robin @2015-01-30 修改重取记录逻辑，增加行锁定 ，扩展池容量到1000 
**  1.0.0.4  Modify by: Robin @2015-02-05 查询coupon和更新状态加入事务处理  
**  1.0.0.5  Modify by: Gavin @2016-11-17 防止并发时，try catch 引起调用方错误。所以取消try catch，取消delay。 第一次select时，就加上锁，阻止其他用户进入。 
**  1.0.0.6  Modify by: Gavin @2016-12-21 只取cardgraderank 最小的cardgrade。 
**  
****************************************************************************/  
as  
begin  
  declare @Times int=0
  declare @Number int=0   
  declare @CardGradeID int
  begin tran 
  select top 1 @CardNumber=CardNumber from CardPool with (RowLock,UPDLock,Readpast) where CardTypeID=@CardTypeID and [Status]=@Status
  select top 1 @CardGradeID = CardGradeID from CardGrade where  CardTypeID=@CardTypeID order by CardGradeRank 
  select @Number=count(*) from CardPool with (NOLock)  where CardTypeID=@CardTypeID and [Status]=@Status
  if @Number<=100  
  begin  
--	while @Times<20
--	begin 
--   		begin try  
  			insert into CardPool(CardNumber,CardTypeID,[Status]) 
			select top 1000 CardNumber,CardTypeID,[Status] from Card 
			where CardTypeID=@CardTypeID and [Status]=@Status   
   			  and CardNumber not in (select CardNumber from CardPool)  
			  and CardGradeID = @CardGradeID        -- ver 1.0.0.6  
  			delete from CardPool where [Status]=99 
			if  @CardNumber is null
  			   select top 1 @CardNumber=CardNumber from CardPool where CardTypeID=@CardTypeID and [Status]=@Status   
--			break
--		end try  
--		begin catch  
--			set @Times=@Times+1
--			waitfor delay '00:00:01'
--		end catch 
--	end 
  end
  update CardPool set [Status]=99 where CardNumber=@CardNumber 
  commit tran
end

GO
