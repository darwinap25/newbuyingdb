USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[PickSalesDetail]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[PickSalesDetail]
  @UserID         varchar(512),
  @TxnNo          varchar(512)
AS
/****************************************************************************
**  Name : PickSalesDetail
**  Version: 1.0.0.0
**  Description : 置sales记录的提货状态。（Demo 用）
**
**  Parameter :
  declare @a int, @TxnNo          varchar(512)
  exec @a = PickSalesDetail '1', ''
  print @a  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin
  update Sales_H set Status = 5, UpdatedBy = @UserID, UpdatedOn = Getdate() where TransNum = @TxnNo and Status <> 5
  update Sales_D set Collected = 4, UpdatedBy = @UserID, UpdatedOn = Getdate() where TransNum = @TxnNo and Collected <> 4
  
  return 0    
end

GO
