USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckedMemberShoppingCart]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[CheckedMemberShoppingCart]
  @MemberID             bigint,               -- 会员ID
  @KeyIDList            varchar(3000)      -- 选中的KeyID 列表，以 逗号分隔。 e.g.  4,16,24 
AS
/****************************************************************************
**  Name : CheckedMemberShoppingCart  
**  Version: 1.0.0.2
**  Description : 勾选会员的购物车内货品
**  Parameter :
  declare @a int , @PageCount int, @RecordCount int  
  exec @a = CheckedMemberShoppingCart 741, '45'
  print @a 
   select * from MemberShoppingCart
**  Created by: Gavin @2015-07-21
**  Modify by: Gavin @2015-09-29 (ver 1.0.0.1) 当@KeyIDList 传入为空时，表示全部清除购物车。
**  Modified by: Gavin @2016-10-17 (ver 1.0.0.2) @MemberID 类型由int改为 bigint
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000)
  set @KeyIDList = isnull(@KeyIDList, '')
  
  if @KeyIDList = ''
  begin
    set @SQLStr = ' update MemberShoppingCart set status = 0 where MemberID = ' + cast(@MemberID as varchar)
     exec sp_executesql @SQLStr    
  end 
  else
  begin
    set @SQLStr = ' update MemberShoppingCart set status = 0 where MemberID = ' + cast(@MemberID as varchar) + ' and KeyID not in (' + @KeyIDList + ')'  
    exec sp_executesql @SQLStr  
  
    set @SQLStr = ' update MemberShoppingCart set status = 1 where MemberID = ' + cast(@MemberID as varchar) + ' and KeyID in (' + @KeyIDList + ')'  
    exec sp_executesql @SQLStr   
  end
   
  return 0
end

GO
