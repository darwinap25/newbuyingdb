USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSizeChart]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetSizeChart]
  @ProductStyleCode           varchar(64),
  @Language                   varchar(20)=''   -- zh_BigZN,  zh_CN, en_CA 
AS
/****************************************************************************
**  Name : GetSizeChart
**  Version: 1.0.0.1
**  Description : 
**
  declare @a int
  exec @a = GetSizeChart '33824','zh_CN'
  print @a  
  select * from Product_add_bau
**  Created by: Gavin @2016-10-08
**  Modify by: Gavin @2016-10-12 (ver 1.0.0.1) 增加返回字段ProdStyleSizeFile
**  Modify by: Gavin @2016-10-14 (ver 1.0.0.2) 过滤ProductSizeCode空的记录。
**
****************************************************************************/
begin
  declare @LanguageFieldNo int

  set @ProductStyleCode = isnull(@ProductStyleCode, '')
  select @LanguageFieldNo = DescFieldNo from LanguageMap where LanguageAbbr = @Language
  if isnull(@LanguageFieldNo, 0) = 0
    set @LanguageFieldNo = 1

  select distinct D.ProductSizeCode, case @LanguageFieldNo when 2 then D.ProductSizeName2 when 3 then D.ProductSizeName3 else D.ProductSizeName1 end as ProductSizeName,
      A.SizeM1, A.SizeM2, A.SizeM3, A.SizeM4, A.SizeM5, A.SizeM6, A.SizeM7, A.SizeM8, S.ProdStyleSizeFile
	from Product_add_bau A 
	  full outer join Product B on A.ProdCode = B.ProdCode
	  full outer join Product_Size D on D.ProductSizeID = B.ProductSizeID
	  full outer join Product_Style S on A.ProdCode = S.ProdCode
    where B.ProdCode in (select ProdCode from Product_Style where ProdCodeStyle = @ProductStyleCode or @ProductStyleCode = '') AND B.IsOnlineSKU=1 
	  and D.ProductSizeCode is not null
  return 0
end

GO
