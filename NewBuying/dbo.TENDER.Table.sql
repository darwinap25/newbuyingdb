USE [NewBuying]
GO
/****** Object:  Table [dbo].[TENDER]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TENDER](
	[TenderID] [int] IDENTITY(1,1) NOT NULL,
	[TenderCode] [varchar](64) NOT NULL,
	[TenderType] [int] NULL,
	[TenderName1] [nvarchar](512) NULL,
	[TenderName2] [nvarchar](512) NULL,
	[TenderName3] [nvarchar](512) NULL,
	[CashSale] [int] NULL DEFAULT ((0)),
	[Status] [int] NULL DEFAULT ((1)),
	[Base] [decimal](16, 6) NULL DEFAULT ((0.01)),
	[Rate] [decimal](16, 6) NULL DEFAULT ((1)),
	[MinAmount] [float] NULL,
	[MaxAmount] [float] NULL,
	[CardBegin] [varchar](512) NULL,
	[CardEnd] [varchar](512) NULL,
	[CardLen] [int] NULL,
	[Additional] [varchar](512) NULL,
	[BankID] [int] NULL,
	[BrandID] [int] NULL,
	[TenderPicFile] [nvarchar](512) NULL,
	[TerminalFlag] [varchar](20) NULL DEFAULT ('YYYYYYYYYYYYYYYYYYYY'),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_TENDER] PRIMARY KEY CLUSTERED 
(
	[TenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型（1是本币，其他暂定。）：  
0:  Type, This record just Type （不使用）
1:  Local Cash  (只能有一个)现金（本币）。
2:  Foreign Cash 现金（外币）。
3:  Cheque  支票
4:  Credit Card   信用卡
5:  Debit Card  借记卡
6:  EPS Card   EPS卡
7:  Coupon    （所有coupon支付，包括记名和不记名）
8:  Credit Card Installment  信用卡分期支付
9:  Finance House Installment 金融公司分期付款
10: On Account   记账
11: On Account Inter Client  记账（国际客户）
12: Credit Card Group  信用卡组
15: Burn Point    使用积分支付使用SVA卡的积分支付）
16：余额支付（SVA卡支付）
17: Fast payment  快捷支付（第三方支付）（第三方支付接口，支付宝，微信，银联， 
18: Online Payment 在线支付。（网银支付）（转向银行卡的在线支付网站）
19：货到付款。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TenderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TenderName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TenderName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TenderName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否现金类型。0：不是。1：是的。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'CashSale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型状态。0：无效。1：有效。默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小付款单位， minimum payment unit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'Base'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'汇率.默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'Rate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最小支付金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'MinAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最大支付金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'MaxAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号起始字符串' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'CardBegin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号结尾字符串' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'CardEnd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'CardLen'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息。 如果是第三方支付，存放第三方的识别字符串。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'BankID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tender 图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TenderPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端是否可以使用此支付的标志。N：不允许。Y：允许。空字符表示不设限制. 
默认： YYYYYYYYYYYYYYYYYYYY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER', @level2type=N'COLUMN',@level2name=N'TerminalFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型表
@2015-12-22  统一buyingDB 的Buy_currency ,重新定义TenderType
部分TenderCode 需要hardcode 定义：
AllInPay： TenderType 17   通联支付
WxPay：  TenderType 17   微信
AliPay：   TenderType 17   支付宝
PayPal： TenderType 17   贝宝
@2015-12-25 增加字段TerminalFlag， 设置终端是否允许使用此tender。 20位长，每位为 Y 或者 N， 目前暂定：示例： YYYYY
1： MPOS
2：Kiosk
3：OnlineShopping
4：SVAWeb
5：BuyingWeb


' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENDER'
GO
