USE [NewBuying]
GO
/****** Object:  Table [dbo].[GamePlayLog]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GamePlayLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GameID] [int] NOT NULL,
	[GameCampaignID] [int] NOT NULL,
	[GameCampaignPrizeID] [int] NOT NULL,
	[PrizeInfo] [varchar](200) NULL,
	[MemberID] [bigint] NULL,
	[MemberInfo] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[ConsumeCouponNumber] [varchar](64) NULL,
	[IP] [varchar](64) NULL,
	[CardNumber] [varchar](64) NULL,
 CONSTRAINT [PK_GAMEPLAYLOG] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[GamePlayLog] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'GameID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏活动ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'GameCampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'中奖ID - 0:不中奖' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'GameCampaignPrizeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖品信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'PrizeInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'玩家ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'玩家备注信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'MemberInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费的coupon number（用金额或积分的话，这里为空）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog', @level2type=N'COLUMN',@level2name=N'ConsumeCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏日志表 - 记录及统计玩家的抽奖信息
@2016-11-23  Nick 添加
@2016-12-19 Robin 添加字段： IP  varchar(64) ,  cardnumber varchar(64)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GamePlayLog'
GO
