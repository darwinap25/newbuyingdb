USE [NewBuying]
GO
/****** Object:  Table [dbo].[ProductStockOnhand]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductStockOnhand](
	[ProdCode] [varchar](64) NOT NULL,
	[StockType] [int] NULL,
	[StockLocation] [int] NULL,
	[OnhandQty] [int] NULL,
	[LastStockTakeTime] [datetime] NULL,
 CONSTRAINT [PK_PRODUCTSTOCKONHAND] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProductStockOnhand] ADD  DEFAULT ((0)) FOR [StockType]
GO
ALTER TABLE [dbo].[ProductStockOnhand] ADD  DEFAULT ((0)) FOR [StockLocation]
GO
ALTER TABLE [dbo].[ProductStockOnhand] ADD  DEFAULT ((0)) FOR [OnhandQty]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStockOnhand', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类别。目前没有分类，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStockOnhand', @level2type=N'COLUMN',@level2name=N'StockType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库位。目前没有分类，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStockOnhand', @level2type=N'COLUMN',@level2name=N'StockLocation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'在库数量。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStockOnhand', @level2type=N'COLUMN',@level2name=N'OnhandQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后盘点时间。（初始化库存数量）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStockOnhand', @level2type=N'COLUMN',@level2name=N'LastStockTakeTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品库存数据。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStockOnhand'
GO
