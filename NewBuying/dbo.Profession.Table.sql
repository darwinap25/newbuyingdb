USE [NewBuying]
GO
/****** Object:  Table [dbo].[Profession]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Profession](
	[ProfessionID] [int] IDENTITY(1,1) NOT NULL,
	[ProfessionCode] [varchar](64) NULL,
	[ProfessionName1] [nvarchar](512) NULL,
	[ProfessionName2] [nvarchar](512) NULL,
	[ProfessionName3] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Profession] PRIMARY KEY CLUSTERED 
(
	[ProfessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profession', @level2type=N'COLUMN',@level2name=N'ProfessionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'专业编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profession', @level2type=N'COLUMN',@level2name=N'ProfessionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'专业名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profession', @level2type=N'COLUMN',@level2name=N'ProfessionName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'专业名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profession', @level2type=N'COLUMN',@level2name=N'ProfessionName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'专业名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profession', @level2type=N'COLUMN',@level2name=N'ProfessionName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'专业表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profession'
GO
