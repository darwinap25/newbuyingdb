USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProduct_Classify]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProduct_Classify]
  @Barcode              varchar(64),       -- 条码
  @Prodcode             varchar(64),       -- 货品编码
  @DepartCode           varchar(64),       -- 部门编码
  @ProdCodeStyle	    varchar(64),       -- 货品系列编码。
  @IsIncludeChild       int,               -- 0：不包括部门子部门。1：包括部门子部门。  
  @BrandID              int,               -- 品牌ID
  @ProdType             int,               -- 货品类型
  @ProductColorID       int,               -- 货品颜色条件
  @ProductSizeID        int,			   -- 货品尺寸条件
  @ClassifyTable        varchar(64),       -- 自定义货品分类表名
  @ClassifyKeyID        int,			   -- 自定义货品分类表的ID
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @OrderCondition		nvarchar(1000),	   -- 自定义排序条件
  @ReturnMode           int=0,             -- 0: 完整返回数据。 1：只返回部分字段。 默认0.  （目的：减少数据传输）
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''     -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1    
AS
/****************************************************************************
**  Name : GetProduct_Classify
**  Version: 1.0.0.9
**  Description : 获得货品数据	(根据product_Classify条件来查询product)
**
  declare @a int, @PageCount int, @RecordCount int, @IsIncludeChild int
  declare @Barcode varchar(64), @Prodcode varchar(64), @DepartCode varchar(64),@ProdCodeStyle varchar(64), @BrandID int, @ProdType int
  set @Barcode = ''
  set @Prodcode = ''
  set @DepartCode = ''
  set @BrandID = 0
  set @ProdType = 0
  set @ProdCodeStyle =''   --1020638
  set @IsIncludeChild=0
  exec @a = GetProduct_Classify @Barcode,@Prodcode,@DepartCode,@ProdCodeStyle, @IsIncludeChild, @BrandID, @ProdType, 0,0, '', 0, '','', 0, 0, 100, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
	
**  Created by: Gavin @2013-07-17
**
****************************************************************************/
begin

  declare @SQLStr nvarchar(4000), @Language int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  set @BrandID = isnull(@BrandID, 0)      
  set @Barcode = isnull(@Barcode, '')
  set @Prodcode = isnull(@Prodcode, '')
  set @DepartCode = isnull(@DepartCode, '')
  set @ProdType = isnull(@ProdType, 0)
  set @IsIncludeChild = isnull(@IsIncludeChild, 0)
  set @ProdCodeStyle = isnull(@ProdCodeStyle, '')  
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))
  set @ProductColorID = isnull(@ProductColorID, 0)
  set @ProductSizeID = isnull(@ProductSizeID, 0)          
  set @ClassifyTable = isnull(@ClassifyTable, '')
  set @ClassifyKeyID = isnull(@ClassifyKeyID, 0)
  set @QueryCondition = isnull(@QueryCondition, '')
  set @OrderCondition = isnull(@OrderCondition, '')
    
  set @SQLStr = 'select T.Barcode, P.ProdCode, D.DepartCode, D.BrandID, P.ProdType, P.ProductBrandID,  P.PackQty, P.OriginID, '
    + ' P.ColorID, P.NonSale, C.ColorCode, C.ColorPicFile, P.NewFlag, P.HotSaleFlag,'	
	+ '  case ' + cast(@Language as varchar) + ' when 2 then ColorName2 when 3 then ColorName3 else ColorName1 end as ColorName,  '	     
	+ '  case ' + cast(@Language as varchar) + ' when 2 then ProductSizeName2 when 3 then ProductSizeName3 else ProductSizeName1 end as ProductSizeName, '    
	+ '  R.ProdPriceType, R.NetPrice, R.DefaultPrice, Z.ProductSizeID, Z.ProductSizeCode, Q.OnhandQty '	
  if @ReturnMode = 0 
  begin
	set @SQLStr = @SQLStr + ' , ' 
	+ '  case ' + cast(@Language as varchar) + ' when 2 then NationName2 when 3 then NationName3 else NationName1 end as NationName, '
    + '  case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName , '
	+ '  case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '
	+ '  Z.ProductSizeNote, P.ProdPicFile, P.ProdNote, '
	+ '  case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName, '
	+ '  case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandDesc2 when 3 then B.ProductBrandDesc3 else B.ProductBrandDesc1 end as ProductBrandDesc, '
	+ '  ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, '
	+ '  A.ProdFunction, A.ProdIngredients, A.ProdInstructions, A.PackDesc, A.PackUnit, '
	+ '  A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6, A.MemoTitle1, A.MemoTitle2, A.MemoTitle3, A.MemoTitle4, A.MemoTitle5, A.MemoTitle6, '	
	+ '  P.Flag1, P.Flag2, P.Flag3, P.Flag4, P.Flag5, N.NationFlagFile '
  end	
  set @SQLStr = @SQLStr + ' from Product P '
          + ' left join (select distinct Barcode, ProdCode from ProductBarcode) T on T.ProdCode = P.ProdCode '    
          + ' left join (select * from Product_Price where status = 1) R on P.ProdCode = R.ProdCode '    
          + ' left join (select * from Product_Particulars where LanguageID = ' + cast(@Language as varchar) + ') A on P.ProdCode = A.Prodcode '
          + ' left join Product_Brand B on P.ProductBrandID = B.ProductBrandID '
          + ' left join Nation N on P.OriginID = N.NationID '         
          + ' left join Color C on P.ColorID = C.ColorID '
          + ' left join Product_Size Z on P.ProductSizeID = Z.ProductSizeID '
		  + ' left join Department D on D.DepartCode = P.DepartCode '  
		  + ' left join ProductStockOnhand Q on P.ProdCode = Q.ProdCode '  		  
		    
  set @SQLStr = @SQLStr + ' where (T.Barcode = ''' + @Barcode + ''' or ''' + @Barcode + ''' = '''')'
     + ' and (P.Prodcode = ''' + @Prodcode + ''' or ''' + @Prodcode + ''' = '''')'
     + ' and (D.BrandID = ' + cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + ' = 0)'
     + ' and (P.ProdType = ' + cast(@ProdType as varchar) + ' or ' + cast(@ProdType as varchar) + ' = 0)'
     + ' and (P.ColorID = ' + cast(@ProductColorID as varchar) + ' or ' + cast(@ProductColorID as varchar) + ' = 0)'
     + ' and (P.ProductSizeID = ' + cast(@ProductSizeID as varchar) + ' or ' + cast(@ProductSizeID as varchar) + ' = 0)'     
  if @IsIncludeChild = 0
    set @SQLStr = @SQLStr + ' and (P.DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + ''' = '''')'  
  else  
    set @SQLStr = @SQLStr + ' and (P.DepartCode like ''' + @DepartCode + '%'' or ''' + @DepartCode + '''='''') '   
  if @ProdCodeStyle <> ''
	set @SQLStr = @SQLStr + ' and(P.Prodcode in (select Prodcode from Product_Style where ProdCodeStyle = ''' + @ProdCodeStyle + '''))'
  if @ClassifyTable <> '' and @ClassifyKeyID <> 0
    set	@SQLStr = @SQLStr + ' and(P.Prodcode in (select Prodcode from Product_Classify where ForeignTable = ''' + @ClassifyTable + ''' and ForeignkeyID = ' + cast(@ClassifyKeyID as varchar) + '))'
    
  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @QueryCondition

  return 0
end

GO
