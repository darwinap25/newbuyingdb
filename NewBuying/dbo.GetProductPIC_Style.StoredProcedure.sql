USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductPIC_Style]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetProductPIC_Style]   
  @ProdcodeStyle        varchar(64),       -- 货品编码
  @LanguageAbbr			varchar(20)=''     -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1    
AS
/****************************************************************************
**  Name : GetProductPIC_Style    
**  Version: 1.0.0.0
**  Description : 获得ProductStyle下所有货品的图片组
**
  declare @a int
  exec @a = GetProductPIC_Style '46516', 'zh_CN'
  print @a
  select * from Product_Style where prodcode = '501216B465160105'
   select * from Product_PIC 
**  Created by: Gavin @2016-10-19
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int

  set @ProdcodeStyle = isnull(@ProdcodeStyle, '')
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select KeyID, ProdCode, ProductThumbnailsFile, ProductFullPicFile, 
      case @Language when 2 then ProductPicNote2 when 3 then ProductPicNote3 else ProductPicNote1 end as ProductPicNote
    from Product_PIC     
    where ProdCode in (select ProdCode from Product_Style where ProdCodeStyle = @ProdcodeStyle)
  return 0
end

GO
