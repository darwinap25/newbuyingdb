USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenUserMessage]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenUserMessage]
  @UserID              int,            -- 批核人员
  @MessageType         int,            -- UserMessageSetting_H 表的 UserMessageType
  @TriggeType          int,            -- 触发条件. 0：创建时。  1：批核时。 2: Void. 
  @OrderNumber         varchar(64)=''  -- 订单号码
AS
/****************************************************************************
**  Name : GenUserMessage
**  Version: 1.0.0.6
**  Description : 产生发送给内部用户的消息.
**  example :
  exec GenUserMessage 1, 7, 1, ''
  select * from UserMessageSetting_H  
  select * from UserMessageSetting_D
  select * from Accounts_Users
  select cast(MessageBody as nvarchar(max)), * from  messageobject
  select * from  messagereceivelist  
**  Created by: Gavin @2014-06-20
**  Modify by： Gavin @2015-02-27 (ver 1.0.0.1) 实现对@TriggeType的判断， @TriggeType增加Void类型。
**  Modify by： Gavin @2015-07-28 (ver 1.0.0.2) 读取订单的内容. 
**  Modify by： Gavin @2015-09-25 (ver 1.0.0.3) 金额数量读取detail的合计值. 
**  Modify by： Gavin @2015-09-28 (ver 1.0.0.4) 部分数据使用名称返回. 
**  Modify by： Gavin @2015-09-29 (ver 1.0.0.5) 修正bug,  使用@FromStoreName, @ToStoreName 存放名字
**  Modify by： Gavin @2015-09-30 (ver 1.0.0.6) 增加显示@OrderNumber
**
****************************************************************************/
begin
  declare @RefNumber varchar(64), @ApproveStatus varchar(64), @ApprovalCode varchar(64), 
          @CreatedOn datetime, @CreatedBy int, @ApprovalOn datetime, @Approvalby int,
          @OrderType int, @FromStoreID int, @ToStoreID int, @Amount money, @Qty int, @StartCard varchar(64),
          @EndCard varchar(64), @CreatedBusDate datetime, @ApprovalBusDate datetime, 
          @CreatedByName varchar(64), @ApprovalByName varchar(64),
          @FromStoreName varchar(512), @ToStoreName varchar(512)
  declare @C_RefNumber varchar(64), @C_ApproveStatus varchar(64), @C_ApprovalCode varchar(64), 
          @C_CreatedOn varchar(64), @C_CreatedBy varchar(64), @C_ApprovalOn varchar(64), @C_Approvalby varchar(64),
          @C_OrderType varchar(64), @C_FromStoreID varchar(64), @C_ToStoreID varchar(64), @C_Amount varchar(64), 
          @C_Qty varchar(64), @C_StartCard varchar(64),
          @C_EndCard varchar(64), @C_CreatedBusDate varchar(64), @C_ApprovalBusDate varchar(64), 
          @C_CreatedByName varchar(64), @C_ApprovalByName varchar(64), @C_OrderNumber varchar(64)
  DECLARE @TemplateContent nvarchar(Max), @MessageTitle varchar(512)
        
  set @C_RefNumber = '{REFNUMBER}'
  set @C_ApproveStatus = '{APPROVESTATUS}'
  set @C_ApprovalCode = '{APPROVALCODE}'
  set @C_CreatedOn = '{CREATEDON}'
  set @C_CreatedBy = '{CREATEDBY}'
  set @C_ApprovalOn = '{APPROVALON}'
  set @C_Approvalby = '{APPROVALBY}'
  set @C_FromStoreID = '{FROMSTOREID}'
  set @C_ToStoreID = '{TOSTOREID}'
  set @C_Amount = '{AMOUNT}'
  set @C_Qty = '{QTY}'
  set @C_StartCard = '{STARTCARD}'
  set @C_EndCard = '{ENDCARD}'
  set @C_CreatedBusDate = '{CREATEDBUSDATE}'
  set @C_ApprovalBusDate = '{APPROVALBUSDATE}'
  set @C_CreatedByName = '{CREATEDBYNAME}'
  set @C_ApprovalByName = '{APPROVALBYNAME}'
  set @C_OrderNumber = '{TRANSACTIONNUMBER}'
                        
  declare @UserMessageCode varchar(64), @MessageID int
  set @MessageType = ISNULL(@MessageType, 0)
  set @TriggeType = ISNULL(@TriggeType, -1)
  set @OrderNumber = isnull(@OrderNumber, '')

  -- ver 1.0.0.2
  if @OrderNumber <> ''
  begin
  
    if @MessageType in (24, 5)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = D.amt, @Qty = Isnull(CardCount, 0), @RefNumber = OriginalTxnNo, @OrderType = OprID,
        @StartCard = '', @EndCard = '', @FromStoreID = StoreID, @ToStoreID = StoreID
      from Ord_CardAdjust_H H left join (select CardAdjustNumber, sum(ISNULL(OrderAmount,0)) as amt, SUM(isnull(OrderPoints,0)) as pot 
                                           from Ord_CardAdjust_D where CardAdjustNumber = @OrderNumber group by CardAdjustNumber) D 
                                on H.CardAdjustNumber = D.CardAdjustNumber
        where H.CardAdjustNumber = @OrderNumber
    else if @MessageType in (9, 20)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = isnull(ActAmount, 0), @Qty = Isnull(CardCount, 0), @RefNumber = ReferenceNo, @OrderType = OrderType,
        @StartCard = '', @EndCard = '', @FromStoreID = SupplierID, @ToStoreID = StoreID        
      from Ord_OrderToSupplier_Card_H H 
        left join (select OrderSupplierNumber, sum(isnull(OrderAmount,0)) as ActAmount,  sum(isnull(OrderQty,0)) as CardCount
                     from Ord_OrderToSupplier_Card_D group by OrderSupplierNumber) D
           on H.OrderSupplierNumber = D.OrderSupplierNumber
      where H.OrderSupplierNumber = @OrderNumber 
    else if @MessageType in (10)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = ReferenceNo, @OrderType = OrderType,
        @StartCard = '', @EndCard = '', @FromStoreID = SupplierID, @ToStoreID = StoreID
      from Ord_CardReceive_H where CardReceiveNumber = @OrderNumber  
    else if @MessageType in (2,21)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = D.ActAmount, @Qty = D.CardCount, @RefNumber = '', @OrderType = OrderType,
        @StartCard = '', @EndCard = '', @FromStoreID = FromStoreID, @ToStoreID = StoreID
      from Ord_CardOrderForm_H H
           left join (select CardOrderFormNumber, sum(isnull(OrderAmount,0)) as ActAmount,  sum(isnull(CardQty,0)) as CardCount
                        from Ord_CardOrderForm_D group by CardOrderFormNumber) D
           on H.CardOrderFormNumber = D.CardOrderFormNumber   
      where H.CardOrderFormNumber = @OrderNumber 
      
    else if @MessageType in (3,22)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = ReferenceNo, @OrderType = OrderType,
        @StartCard = '', @EndCard = '', @FromStoreID = FromStoreID, @ToStoreID = StoreID
      from Ord_CardPicking_H where CardPickingNumber = @OrderNumber
    else if @MessageType in (7)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = '', @OrderType = 0,
        @StartCard = '', @EndCard = '', @FromStoreID = 0, @ToStoreID = 0
      from Ord_CouponBatchCreate where CouponCreateNumber = @OrderNumber     
    else if @MessageType in (8)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = '', @OrderType = OrderType,
        @StartCard = '', @EndCard = '', @FromStoreID = FromStoreID, @ToStoreID = StoreID
      from Ord_CouponOrderForm_H where CouponOrderFormNumber = @OrderNumber   
    else if @MessageType in (12)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = ReferenceNo, @OrderType = 0,
        @StartCard = '', @EndCard = '', @FromStoreID = FromStoreID, @ToStoreID = StoreID
      from Ord_CouponDelivery_H where CouponDeliveryNumber = @OrderNumber   
    else if @MessageType in (13)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = ReferenceNo, @OrderType = 0,
        @StartCard = '', @EndCard = '', @FromStoreID = FromStoreID, @ToStoreID = StoreID
      from Ord_CouponReturn_H where CouponReturnNumber = @OrderNumber
    else if @MessageType in (6,25)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = '', @OrderType = OrderType,
        @StartCard = '', @EndCard = '', @FromStoreID = 0, @ToStoreID = 0
      from Ord_CardTransfer_H where CardTransferNumber = @OrderNumber         
    else if @MessageType in (23)
      select @CreatedBusDate = CreatedBusDate, @ApprovalBusDate = ApproveBusDate, @CreatedOn = CreatedOn, @CreatedBy = CreatedBy,
        @ApprovalOn = ApproveOn, @Approvalby = Approveby, @ApprovalCode = ApprovalCode, @ApproveStatus = ApproveStatus,
        @Amount = 0, @Qty = 0, @RefNumber = ReferenceNo, @OrderType = 0,
        @StartCard = '', @EndCard = '', @FromStoreID = FromStoreID, @ToStoreID = StoreID
      from Ord_CardDelivery_H where CardDeliveryNumber = @OrderNumber   
  end

  -- ver 1.0.0.4
  select @FromStoreName = StoreName1 from store where storeid = @FromStoreID
  select @ToStoreName = StoreName1 from store where storeid = @ToStoreID
  select @CreatedByName = UserName from Accounts_Users where UserID = @CreatedBy
  select @ApprovalByName = UserName from Accounts_Users where UserID = @Approvalby
  if @ApproveStatus = 'P' 
    set @ApproveStatus = 'PENDING'
  else if @ApproveStatus = 'A' 
    set @ApproveStatus = 'APPROVED'
  else if @ApproveStatus = 'V' 
    set @ApproveStatus = 'VOID'
  else if @ApproveStatus = 'R' 
    set @ApproveStatus = 'PREPARE'
  else if @ApproveStatus = 'C' 
    set @ApproveStatus = 'PICKED'        
        
  DECLARE CUR_UserNoticeMessage CURSOR fast_forward local FOR
    select UserMessageCode from UserMessageSetting_H 
       where UserMessageType = @MessageType and status = 1 and TriggeType = @TriggeType
         and StartDate <= getdate() and enddate >= (getdate()-1)
  OPEN CUR_UserNoticeMessage
  FETCH FROM CUR_UserNoticeMessage INTO @UserMessageCode
  WHILE @@FETCH_STATUS=0
  BEGIN 
    select @TemplateContent = UserMessageContent, @MessageTitle = UserMessageTitle
        from UserMessageSetting_H where UserMessageCode = @UserMessageCode  
    SET @TemplateContent = REPLACE(@TemplateContent, @C_RefNumber, ISNULL(@RefNumber,''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_ApproveStatus, ISNULL(@ApproveStatus,''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_ApprovalCode, ISNULL(@ApprovalCode,''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_CreatedOn, ISNULL(convert(varchar(19), @CreatedOn, 120),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_CreatedBy, ISNULL(Cast(@CreatedBy as varchar(64)),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_ApprovalOn, ISNULL(convert(varchar(19), @ApprovalOn, 120),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_Approvalby, ISNULL(Cast(@Approvalby as varchar(64)),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_FromStoreID, ISNULL(Cast(@FromStoreName as varchar(64)),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_ToStoreID, ISNULL(Cast(@ToStoreName as varchar(64)),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_Amount, ISNULL(Cast(@Amount as varchar(64)),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_Qty, ISNULL(Cast(@Qty as varchar(64)),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_StartCard, ISNULL(@StartCard,''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_EndCard, ISNULL(@EndCard,''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_CreatedBusDate, ISNULL(convert(varchar(19), @CreatedBusDate, 120),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_ApprovalBusDate, ISNULL(convert(varchar(19), @ApprovalBusDate, 120),''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_CreatedByName, ISNULL(@CreatedByName,''))
    SET @TemplateContent = REPLACE(@TemplateContent, @C_ApprovalByName, ISNULL(@ApprovalByName,''))   
    SET @TemplateContent = REPLACE(@TemplateContent, @C_OrderNumber, ISNULL(@OrderNumber,''))   

    SET @MessageTitle = REPLACE(@MessageTitle, @C_RefNumber, ISNULL(@RefNumber,''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_ApproveStatus, ISNULL(@ApproveStatus,''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_ApprovalCode, ISNULL(@ApprovalCode,''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_CreatedOn, ISNULL(convert(varchar(19), @CreatedOn, 120),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_CreatedBy, ISNULL(Cast(@CreatedBy as varchar(64)),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_ApprovalOn, ISNULL(convert(varchar(19), @ApprovalOn, 120),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_Approvalby, ISNULL(Cast(@Approvalby as varchar(64)),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_FromStoreID, ISNULL(Cast(@FromStoreID as varchar(64)),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_ToStoreID, ISNULL(Cast(@ToStoreID as varchar(64)),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_Amount, ISNULL(Cast(@Amount as varchar(64)),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_Qty, ISNULL(Cast(@Qty as varchar(64)),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_StartCard, ISNULL(@StartCard,''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_EndCard, ISNULL(@EndCard,''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_CreatedBusDate, ISNULL(convert(varchar(19), @CreatedBusDate, 120),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_ApprovalBusDate, ISNULL(convert(varchar(19), @ApprovalBusDate, 120),''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_CreatedByName, ISNULL(@CreatedByName,''))
    SET @MessageTitle = REPLACE(@MessageTitle, @C_ApprovalByName, ISNULL(@ApprovalByName,'')) 
    SET @MessageTitle = REPLACE(@MessageTitle, @C_OrderNumber, ISNULL(@OrderNumber,''))              
    
    if exists(select KeyID from UserMessageSetting_D where MessageServiceTypeID = 1)
    begin
      insert into MessageObject(MessageServiceTypeID, MessageType, MessagePriority, MessageCoding, MessageTitle, 
         MessageBody, FromMemberID, IsInternal, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ResponseCode)   
      select 1, @MessageType, 0, 0, @MessageTitle, 
         cast(@TemplateContent as varbinary(max)), SendUserID, 1, 0, Getdate(), @UserID, Getdate(), @UserID, 0 
        from UserMessageSetting_H where UserMessageCode = @UserMessageCode 
      set @MessageID = SCOPE_IDENTITY() 
 
      insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy, ReceiverType)
      select @MessageID, D.ReceiveUserID, U.Email,  0, 0, getdate(), @UserID, 1 from UserMessageSetting_D D 
         left join  UserMessageSetting_H H on H.UserMessageCode = D.UserMessageCode
         left join  Accounts_Users U on U.UserID = D.ReceiveUserID
       where  H.UserMessageCode = @UserMessageCode and D.MessageServiceTypeID = 1 and isnull(U.Email, '') <> ''  
    end

    if exists(select KeyID from UserMessageSetting_D where MessageServiceTypeID = 2)
    begin
      insert into MessageObject(MessageServiceTypeID, MessageType, MessagePriority, MessageCoding, MessageTitle, 
         MessageBody, FromMemberID, IsInternal, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ResponseCode)   
      select 2, @MessageType, 0, 0, @MessageTitle, 
         cast(@TemplateContent as varbinary(max)), SendUserID, 1, 0, Getdate(), @UserID, Getdate(), @UserID, 0 
        from UserMessageSetting_H where UserMessageCode = @UserMessageCode 
      set @MessageID = SCOPE_IDENTITY() 
 
      insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy, ReceiverType)
      select @MessageID, D.ReceiveUserID, U.Phone,  0, 0, getdate(), @UserID, 1 from UserMessageSetting_D D 
         left join  UserMessageSetting_H H on H.UserMessageCode = D.UserMessageCode
         left join  Accounts_Users U on U.UserID = D.ReceiveUserID
       where  H.UserMessageCode = @UserMessageCode and D.MessageServiceTypeID = 2 and isnull(U.Phone, '') <> ''  
    end        

    FETCH FROM CUR_UserNoticeMessage INTO @UserMessageCode
  END
  CLOSE CUR_UserNoticeMessage
  DEALLOCATE CUR_UserNoticeMessage 
  
  return 0
end

GO
