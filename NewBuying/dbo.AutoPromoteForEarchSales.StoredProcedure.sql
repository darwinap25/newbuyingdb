USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoPromoteForEarchSales]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[AutoPromoteForEarchSales]
  @TransNo varchar(64)
AS
/*
Modify by Gavin @2014-09-03 修正StartTime，EndTime 的比较判断。（从字符串比较，改为浮点数比较）
Modify by Gavin @2014-09-10 计算时过滤
Modify by Gavin @2014-09-15 计算日期过滤时减去1 
Modify by Gavin @2014-09-16 计算有效时间时,如果时间是0,则认为没有限制.
Modify by Gavin @2014-09-19 修正bug
Modify by Gavin @2014-09-23 当交易salesD记录的netamount为负数时,循环出错的问题.
Modify by Gavin @2014-10-08 增加PromotionMultipleValue字段的计算
Modify by Gavin @2015-11-18 修正 CardGradeMaxPoint 的判断错误，  CardGradeMaxPoint 是 null 或者 0 时， 表示不设上限
Modify by Gavin @2016-01-21 增加Promotion_Segment表的限制。 按金额百分比计算积分时， 取整时 由 floor 改成 rount
*/
begin
  declare @TxnNo varchar(64), @StoreID int, @BrandID int, @CardNumber varchar(64), @MemberID int, @CardTypeID int, 
          @CardGradeID int, @MemberDateOfBirth datetime, @CardBrandID int, @StoreGroupID int, @CumulativeEarnPoints int,
          @ReferCardNumber varchar(64), @TopLimit int, @CashierID varchar(64), @RegisterCode varchar(64), @ServerCode varchar(64),
          @TotalAmount money, @CouponAmount money
  declare @PromotionCode varchar(64), @PromotionDesc varchar(512), @MutexFlag int, @HitRelation int
  Declare @D Table(SEQ int identity, PRODCODE varchar(512),DEPTCODE varchar(512),QTY int ,NETPRICE money, NETAMOUNT money)  
  Declare @HitPromotion Table(PromotionCode varchar(64), HitTotalAmount money, HitSeq int, HitCount int)
  declare @HitSeq int, @HitType int, @HitValue int, @HitOP int, @HitItem int
  declare @EntityType int, @EntityNum varchar(64)
  declare @IsHit int, @DQty int, @DAmt money, @A int, @ReturnValue int, @DPartQty int, @DPartAmt money
  declare @TempQty int, @TempCalcQty int, @TempSeq int
  declare @TempAmt money, @TempCalcAmt money, @TempAmtSeq int  
  declare @HitPromotionCode varchar(64), @HitTotalAmount money, @CouponTypeID int, @HitPromotionCount int
  declare @PromotionGiftType int, @PromotionGiftValue decimal(12,4),  @PromotionAdjValue decimal(12,4)
  declare @GiftAmount money, @GiftPoint int, @GiftCouponNumber varchar(64)
  declare @CouponNumber varchar(64), @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate datetime, @NewCouponExpiryDate datetime
  declare @BusDate datetime, @Txndate datetime, @HitMutex int, @HitCount int , @HitHitSeq int, @hitPromotionDesc varchar(512) 
  declare @i int, @CardGradeMaxAmount money, @CardGradeMaxPoint int, @CardTotalAmount money, @CardTotalPoint int
  declare @PromotionMultipleValue int
  
  select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  set @TxnDate = getdate()  
  set @HitMutex = -1    -- 命中的第一个Promotion 是否互斥类型。 -1：还没有命中。 0：不是（排斥所有互斥的Promotion）。1：是的（直接中止，只命中这条）
  
  DECLARE CUR_DoPromote CURSOR fast_forward FOR
    -- 需要计算的sales记录
    select H.TransNum, isnull(S.StoreID,0), isnull(S.StoreGroupID,0), B.StoreBrandID, H.CardNumber, C.MemberID, 
         C.CardTypeID, C.CardGradeID, M.MemberDateOfBirth, T.BrandID, C.CumulativeEarnPoints, M.ReferCardNumber,
         H.TotalAmount, H.CashierID, H.RegisterCode, H.ServerCode
      from sales_H H 
        left join Store S on H.StoreCode = S.StoreCode 
        left join Card C on H.CardNumber = C.CardNumber
        left join Brand B on H.BrandCode = B.StoreBrandCode
        left join CardType T on C.CardTypeID = T.CardTypeID 
        left join CardGrade G on C.CardGradeID = G.CardGradeID  
        left join Member M on C.MemberID = M.MemberID
      where isnull(H.PromotionFlag, 0) = 0 and (H.TransNum = @TransNo or isnull(@TransNo,'') = '')
  OPEN CUR_DoPromote
  FETCH FROM CUR_DoPromote INTO @TxnNo, @StoreID, @StoreGroupID, @BrandID, @CardNumber, @MemberID, @CardTypeID, @CardGradeID,
        @MemberDateOfBirth, @CardBrandID, @CumulativeEarnPoints, @ReferCardNumber, @TotalAmount, @CashierID,
        @RegisterCode, @ServerCode
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @CardGradeMaxPoint = isnull(CardGradeMaxPoint,0) , @CardGradeMaxAmount = isnull(CardGradeMaxAmount,0),
        @CardTotalAmount = TotalAmount, @CardTotalPoint = TotalPoints
      from Card C left join CardGrade G on C.CardGradeID = G.CardGradeID
     where CardNumber = @CardNumber 
 --print 'CUR_DoPromote'
 --print @TxnNo
    set @HitMutex = -1    -- 命中的第一个Promotion 是否互斥类型。 -1：还没有命中。 0：不是（排斥所有互斥的Promotion）。1：是的（直接中止，只命中这条）

    DECLARE CUR_PromoteRule_H CURSOR fast_forward FOR
      -- 符合条件的促销主记录.(storeid,brandid,cardtype,cardgrade,birthday ....)
      select PromotionCode, PromotionDesc1, isnull(TopLimit, 1), isnull(MutexFlag, 0), isnull(HitRelation, 2) from Promotion_H H
        where H.ApproveStatus = 'A' and StartDate <= Getdate() and EndDate >= (Getdate() - 1) 
           and ((isnull(StoreID,0) = @StoreID and isnull(StoreID,0)>0) or (isnull(StoreGroupID,0)=@StoreGroupID and isnull(StoreGroupID,0) > 0) or (isnull(StoreID,0)<=0 and isnull(StoreGroupID,0)<=0))
--           and (cast(DATEPART(hh, GETDATE()) as varchar) + ':' + cast(DATEPART(mi, GETDATE()) as varchar) >= cast(DATEPART(hh, StartTime) as varchar) + ':' + cast(DATEPART(mi, StartTime) as varchar) or  isnull(StartTime,0)<=0)
--           and (cast(DATEPART(hh, GETDATE()) as varchar) + ':' + cast(DATEPART(mi, GETDATE()) as varchar) <= cast(DATEPART(hh, EndTime) as varchar) + ':' + cast(DATEPART(mi, EndTime) as varchar) or  isnull(EndTime,0)<=0)
           and ( cast(getdate() as float) - floor(cast(getdate() as float)) >= cast(StartTime as float) - floor(cast(StartTime as float)) or (datepart(hh,StartTime) = 0 and datepart(n,StartTime) = 0 ) ) --isnull(StartTime,0)<=0)
           and ( cast(getdate() as float) - floor(cast(getdate() as float)) <= cast(EndTime as float) - floor(cast(EndTime as float)) or  (datepart(hh,EndTime) = 0 and datepart(n,EndTime) = 0) ) --isnull(EndTime,0)<=0)           
           and ( (isnull(LoyaltyFlag, 0) <= 0) or
                 H.PromotionCode in (select PromotionCode from Promotion_Member 
                                       where ( (LoyaltyType = 1 and LoyaltyValue = @CardBrandID) 
                                             or (LoyaltyType = 2 and LoyaltyValue = @CardTypeID) 
                                             or (LoyaltyType = 3 and LoyaltyValue = @CardGradeID) )
                                           and ( isnull(LoyaltyThreshold, 0) <= 0 or LoyaltyThreshold < @CumulativeEarnPoints )
                                           and ( isnull(LoyaltyPromoScope, 0) <= 0 
                                                 or (LoyaltyPromoScope = 1 
                                                      and (not exists(select * from sales_h where MemberID=@MemberID))) 
                                                 or (LoyaltyPromoScope = 2 and isnull(@ReferCardNumber,'') <> '') )
                                           and ( isnull(LoyaltyBirthdayFlag, 0) <= 0 
                                                or (LoyaltyBirthdayFlag = 1 and datepart(MM, @MemberDateOfBirth) = datepart(MM, Getdate()) and datepart(DD, @MemberDateOfBirth) = datepart(DD, Getdate()))
                                                or (LoyaltyBirthdayFlag = 2 and datepart(WW, @MemberDateOfBirth) = datepart(WW, Getdate()))
                                                or (LoyaltyBirthdayFlag = 3 and datepart(MM, @MemberDateOfBirth) = datepart(MM, Getdate())) )
                                    )
           )
           and ( (isnull(SegmentFlag, 0) <= 0 ) or
                 H.PromotionCode in (select PromotionCode from Promotion_Segment 
                                       where SegmentID in (select SegmentID from MemberSegmentMap where MemberID = @MemberID) 
                                    )
           )
           and
           ( 
               ( (isnull(H.DayFlagID, 0) <= 0) or ( (isnull(H.DayFlagID, 0) > 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
             and ( (isnull(H.WeekFlagID, 0) <= 0) or ( (isnull(H.WeekFlagID, 0) > 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
             and ( (isnull(H.MonthFlagID, 0) <= 0) or ( (isnull(H.MonthFlagID, 0) > 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		   )
      Order by Priority, PromotionCode      
    OPEN CUR_PromoteRule_H
    FETCH FROM CUR_PromoteRule_H INTO @PromotionCode, @PromotionDesc, @TopLimit, @MutexFlag, @HitRelation
    WHILE @@FETCH_STATUS=0
    BEGIN 
--print '@PromotionCode:' + @PromotionCode        
      if @HitMutex = 1   -- or 关系时，任何一个Hit命中，则不需要继续下面的Hit检查
         break   
         
      -- 取出SalesD数据，写入临时表备用。 （每次换PromotionCode时，都需要重新恢复数据。即参加过某个Promotion的货品还可以继续下一个促销）
      delete from @D
      insert into @D (PRODCODE, DEPTCODE,QTY, NETPRICE, NETAMOUNT)
      select D.ProdCode, MAX(P.DepartCode), SUM(D.Qty), MAX(D.NetPrice), sum(D.NetAmount) 
        from Sales_D D left join Product P on D.ProdCode = P.ProdCode 
       where TransNum = @TxnNo and D.NetPrice <> 0         -- add by gavin @2014-09-10 增加条件： D.NetPrice <> 0
      Group by D.ProdCode   
      
      -- 循环Hit 条件.
      DECLARE CUR_PromoteRule_Hit CURSOR fast_forward FOR
        select HitSeq, HitType, HitValue, HitOP, HitItem from Promotion_Hit D left join Promotion_H H on D.PromotionCode = H.PromotionCode
          where D.PromotionCode = @PromotionCode order by HitSeq   
      OPEN CUR_PromoteRule_Hit
      FETCH FROM CUR_PromoteRule_Hit INTO @HitSeq, @HitType, @HitValue, @HitOP, @HitItem
      WHILE @@FETCH_STATUS=0
      BEGIN 
--print '@HitSeq:' + cast(@HitSeq as varchar)
        if @HitType = 1   -- 无条件命中
        begin
          insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @TotalAmount, @HitSeq, 1)
          if @HitRelation = 2 
          begin 
            set @HitMutex = @MutexFlag                                     
            break
          end  
        end 
        else
        begin 
       
          -- 判断是否有必须要存在的货品要求.
          if exists(select * From Promotion_Hit_PLU where PLUHitType = 3 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
            if not exists(select * from @D where ProdCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 1 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                            or DEPTCODE in (select DepartCode from department where LEN(DepartCode) = 4 and 
                                                (DepartCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                                                  or 
                                                 Left(DepartCode,2) in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq))
                                            ) 
                         )
            begin
              FETCH FROM CUR_PromoteRule_Hit INTO @HitSeq, @HitType, @HitValue, @HitOP, @HitItem                                 
              continue 
            end
          -- 取得剔除了不参加促销的货品总计数量和金额。
          if exists(select PRODCODE From PromotionHitPLUList where PromotionCode= @PromotionCode and HitSeq = @HitSeq)
            delete from @D 
              where PRODCODE not in (select PRODCODE From PromotionHitPLUList where PromotionCode= @PromotionCode and HitSeq = @HitSeq)
          --delete from @D 
          --    where ( (PRODCODE in (select EntityNum from Promotion_Hit_PLU where (EntityType = 1 and HitSign = 0 and PromotionCode = @PromotionCode and HitSeq = @HitSeq)))
          --                  or (DEPTCODE in (select EntityNum from Promotion_Hit_PLU where (EntityType = 2 and HitSign = 0 and PromotionCode = @PromotionCode and HitSeq = @HitSeq)))
          --                ) 
          /*================================================================================*/                      
          select @DQTY = SUM(QTY), @DAmt = SUM(NETAmount) from @D

          if @HitItem = 0    -- 无具体货品条件.指整单
          begin
            if @HitType = 2   -- 数量条件
            begin
              exec @A = CheckHitOP @HitOP, @HitValue, @DQTY, @ReturnValue output, @HitCount output, @TopLimit
              if @A = 0
              begin           
                insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @DAmt, @HitSeq, @HitCount)
                if @HitRelation = 2 
                begin 
                  set @HitMutex = @MutexFlag                                     
                  break
                end 
              end                   
            end  
            if @HitType = 3   -- 金额条件
            begin
--print  '@HitType = 3 '
----print @HitOP
----print @HitValue
----print @DAmt
----print  @ReturnValue
----print  @HitCount 
----print @TopLimit       
              exec @A = CheckHitOP @HitOP, @HitValue, @DAmt, @ReturnValue output, @HitCount output, @TopLimit
              if @A = 0
              begin
                insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @DAmt, @HitSeq, @HitCount)
                if @HitRelation = 2 
                begin 
                  set @HitMutex = @MutexFlag                                     
                  break
                end 
              end
            end   
          end 
          else 
          if @HitItem = 1    -- 列表中任意货品
          begin
            if not exists(select * from Promotion_Hit_PLU where PLUHitType = 1 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)    
              select @DPartQty = sum(QTY), @DPartAmt = SUM(NETAMOUNT) from @D 
            else  
              select @DPartQty = sum(QTY), @DPartAmt = SUM(NETAMOUNT) from @D 
                where ProdCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 1 and EntityType = 1 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                  or DEPTCODE in (select DepartCode from department where LEN(DepartCode) = 4 and 
                                                (DepartCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                                                  or 
                                                 Left(DepartCode,2) in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq))
                                 )       
                                        
            if (@HitType = 2)   -- 数量条件
            begin
              set @ReturnValue = -1
              exec @A = CheckHitOP @HitOP, @HitValue, @DPartQty, @ReturnValue output, @HitCount output, @TopLimit    
                     
              if @A = 0
              begin               
                set @TempQty = @HitValue * @HitCount
                while @TempQty > 0
                begin
                  if not exists(select * from Promotion_Hit_PLU where PLUHitType = 1 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                    select top 1 @TempSeq = SEQ, @TempCalcQty = Qty from @D
                  else  
                    select top 1 @TempSeq = SEQ, @TempCalcQty = Qty from @D 
                      where ProdCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 1 and EntityType = 1 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                         or DEPTCODE in (select DepartCode from department where LEN(DepartCode) = 4 and 
                                                (DepartCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                                                  or 
                                                 Left(DepartCode,2) in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq))
                                          )
                                          
                  -- add by gavin @2014-09-24 确保意外数据能退出循环
                  set @TempQty = isnull(@TempQty, 0)
                  set @TempCalcQty = isnull(@TempCalcQty, 0)
                  if @TempCalcQty < 0 
                    set @TempCalcQty = @TempQty
                                                                                                            
                  if @TempCalcQty > @TempQty
                  begin
                    update @D set Qty = Qty - @TempQty where SEQ = @TempSeq
                    set @TempQty = 0
                  end else if @TempCalcQty <= @TempQty
                  begin
                    Delete from @D where SEQ = @TempSeq
                    set @TempQty = @TempQty - @TempCalcQty 
                  end                                                        
                end 
                insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @DAmt, @HitSeq, @HitCount)
                if @HitRelation = 2
                begin 
                  set @HitMutex = @MutexFlag
                  break
                end                      
              end  
            end
            if (@HitType = 3)   -- 金额条件
            begin
              set @ReturnValue = -1
              exec @A = CheckHitOP @HitOP, @HitValue, @DPartAmt, @ReturnValue output, @HitCount output, @TopLimit             
              if @A = 0
              begin
                set @TempAmt = @HitValue
                while @TempAmt > 0
                begin
                  if not exists(select * from Promotion_Hit_PLU where PLUHitType = 1 and PromotionCode= @PromotionCode and HitSeq = @HitSeq) 
                    select top 1 @TempAmtSeq = SEQ, @TempCalcAmt = NETAMOUNT from @D
                  else  
                    select top 1 @TempAmtSeq = SEQ, @TempCalcAmt = NETAMOUNT from @D
                      where ProdCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 1 and EntityType = 1 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                         or DEPTCODE in (select DepartCode from department where LEN(DepartCode) = 4 and 
                                                (DepartCode in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq)
                                                  or 
                                                 Left(DepartCode,2) in (select EntityNum from Promotion_Hit_PLU where PLUHitType = 3 and EntityType = 2 and PromotionCode= @PromotionCode and HitSeq = @HitSeq))
                                       )
                  -- add by gavin @2014-09-23
                  set @TempCalcAmt = ISNULL(@TempCalcAmt, 0)
                  set @TempAmt = ISNULL(@TempAmt, 0) 
                  if @TempCalcAmt <= 0
                    set @TempCalcAmt = @TempAmt
                                                                   
                  if @TempCalcAmt > @TempAmt
                  begin
                    update @D set NETAMOUNT = NETAMOUNT - @TempAmt where SEQ = @TempAmtSeq
                    set @TempAmt = 0
                  end else if @TempCalcAmt <= @TempAmt
                  begin
                    Delete from @D where SEQ = @TempSeq
                    set @TempAmt= @TempAmt - @TempCalcAmt
                  end  
                end                   
                insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @DAmt, @HitSeq, @HitCount)
                if @HitRelation = 2
                begin 
                  set @HitMutex = @MutexFlag
                  break
                end                       
              end              
            end                                   
          end 
          else 
          if @HitItem = 2    -- 有任意一个单独货品满足数量或者金额
          begin
--print '@HitItem = 2 '          
            -- 循环HitPLU 条件.
            DECLARE CUR_PromoteRule_HitPLU CURSOR fast_forward FOR
              select EntityType, EntityNum from Promotion_Hit_PLU 
                 where PLUHitType = 1 and PromotionCode = @PromotionCode and HitSeq = @HitSeq
               order by HitPLUSeq, KeyID
            OPEN CUR_PromoteRule_HitPLU 
            FETCH FROM CUR_PromoteRule_HitPLU INTO @EntityType, @EntityNum
            WHILE @@FETCH_STATUS=0
            BEGIN             
              select @DPartQty = sum(QTY), @DPartAmt = SUM(NETAMOUNT) from @D 
                 where (@EntityType = 0) or (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE like (@EntityNum + '%'))   
              -- @HitItem = 2 时, @HitType = 1 条件不能成立.    
/*                      
              if (@HitType = 1) and exists(select * from @D where (@EntityType = 0) or (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum))
              begin
                delete from @D where (@EntityType = 0) or (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum)              
                  insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq)  values(@PromotionCode, @DAmt, @HitSeq)                
                if @HitRelation = 2 
                begin 
                  set @HitMutex = @MutexFlag                                     
                  break
                end
              end
*/              
              if (@HitType = 2)   -- 数量条件
              begin
                set @ReturnValue = -1

--print '@HitType = 2'
--print @HitOP
--print @HitValue 
--print  @DPartQty              
                exec @A = CheckHitOP @HitOP, @HitValue, @DPartQty, @ReturnValue output, @HitCount output, @TopLimit  
--print @A                                 
                if @A = 0
                begin
               
                  set @TempQty = @HitValue
                  while @TempQty > 0
                  begin
                    select top 1 @TempSeq = SEQ, @TempCalcQty = Qty from @D where (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum)
                    
                    -- add by gavin @2014-09-24 确保意外数据能退出循环
                    set @TempQty = isnull(@TempQty, 0)
                    set @TempCalcQty = isnull(@TempCalcQty, 0)
                    if @TempCalcQty < 0 
                      set @TempCalcQty = @TempQty
                                        
                    if @TempCalcQty > @TempQty
                    begin
                      update @D set Qty = Qty - @TempQty where SEQ = @TempSeq
                      set @TempQty = 0
                    end else if @TempCalcQty <= @TempQty
                    begin
                      Delete from @D where SEQ = @TempSeq
                      set @TempQty = @TempQty - @TempCalcQty 
                    end  
                  end 
                  insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @DAmt, @HitSeq, @HitCount)
                  if @HitRelation = 2
                  begin 
                    set @HitMutex = @MutexFlag
                    break
                  end  
                end                  
              end
              if (@HitType = 3)   -- 金额条件
              begin
                set @ReturnValue = -1
                exec @A = CheckHitOP @HitOP, @HitValue, @DPartAmt, @ReturnValue output, @HitCount output, @TopLimit         
                if @A = 0
                begin
                  set @TempAmt = @HitValue
                  while @TempAmt > 0
                  begin
                    select top 1 @TempAmtSeq = SEQ, @TempCalcAmt = NETAMOUNT from @D where (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum)
                    
                    -- add by gavin @2014-09-23
                    set @TempCalcAmt = ISNULL(@TempCalcAmt, 0)
                    set @TempAmt = ISNULL(@TempAmt, 0) 
                    if @TempCalcAmt <= 0
                      set @TempCalcAmt = @TempAmt
                                        
                    if @TempCalcAmt > @TempAmt
                    begin
                      update @D set NETAMOUNT = NETAMOUNT - @TempAmt where SEQ = @TempAmtSeq
                      set @TempAmt = 0
                    end else if @TempCalcAmt <= @TempAmt
                    begin
                      Delete from @D where SEQ = @TempSeq
                      set @TempAmt= @TempAmt - @TempCalcAmt
                    end  
                  end                   
                  insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @DAmt, @HitSeq, @HitCount)
                  if @HitRelation = 2
                  begin 
                    set @HitMutex = @MutexFlag
                    break
                  end  
                end                                      
              end           
              FETCH FROM CUR_PromoteRule_HitPLU INTO @EntityType, @EntityNum
            END
            CLOSE CUR_PromoteRule_HitPLU 
            DEALLOCATE CUR_PromoteRule_HitPLU
          end 
          else 
          if @HitItem = 3    -- 每一个货品都需要满足数量或者金额
          begin
            -- 循环HitPLU 条件.
            set @IsHit = 1
            DECLARE CUR_PromoteRule_HitPLU CURSOR fast_forward FOR
              select EntityType, EntityNum from Promotion_Hit_PLU 
                 where PLUHitType = 1 and PromotionCode = @PromotionCode and HitSeq = @HitSeq
               order by HitPLUSeq,KeyID
            OPEN CUR_PromoteRule_HitPLU 
            FETCH FROM CUR_PromoteRule_HitPLU INTO @EntityType, @EntityNum
            WHILE @@FETCH_STATUS=0
            BEGIN
              if @IsHit = 0
                break
                
              select @DPartQty = sum(QTY), @DPartAmt = SUM(NETAMOUNT) from @D 
                 where (@EntityType = 0) or (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE like (@EntityNum + '%'))  
               -- @HitItem = 3 时, @HitType = 1 条件不能成立.              
--              if (@HitType = 1) and exists(select * from @D where (@EntityType = 0) or (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum))
--              begin
--                delete from @D where (@EntityType = 0) or (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum)              
--                  insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq)  values(@PromotionCode, @DAmt, @HitSeq)                
--                if @HitRelation = 2
--                begin 
--                  set @HitMutex = @MutexFlag                                     
--                  break
--                end
--              end
              if (@HitType = 2)   -- 数量条件
              begin
			          set @ReturnValue = -1
			          exec @A = CheckHitOP @HitOP, @HitValue, @DPartQty, @ReturnValue output, @HitCount output, @TopLimit                
			          if @A = 0
			          begin
				          set @TempQty = @HitValue
				          while @TempQty > 0
				          begin
				            select top 1 @TempSeq = SEQ, @TempCalcQty = Qty from @D where (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum)
				            
                    -- add by gavin @2014-09-24 确保意外数据能退出循环
                    set @TempQty = isnull(@TempQty, 0)
                    set @TempCalcQty = isnull(@TempCalcQty, 0)
                    if @TempCalcQty < 0 
                      set @TempCalcQty = @TempQty
                      				            
				            if @TempCalcQty > @TempQty
				            begin
					            update @D set Qty = Qty - @TempQty where SEQ = @TempSeq
					            set @TempQty = 0
				            end else if @TempCalcQty <= @TempQty
				            begin
					            Delete from @D where SEQ = @TempSeq
					            set @TempQty = @TempQty - @TempCalcQty 
				            end  
				          end 
				          set @IsHit = 1
			          end else
			            set @IsHit = 0 
              end
              if (@HitType = 3)   -- 金额条件
              begin
                set @ReturnValue = -1
                exec @A = CheckHitOP @HitOP, @HitValue, @DPartAmt, @ReturnValue output, @HitCount output, @TopLimit         
                if @A = 0
                begin
                  set @TempAmt = @HitValue
                  while @TempAmt > 0
                  begin
                    select top 1 @TempAmtSeq = SEQ, @TempCalcAmt = NETAMOUNT from @D where (@EntityType = 1 and PRODCODE = @EntityNum) or (@EntityType = 2 and DEPTCODE = @EntityNum)
                    
                    -- add by gavin @2014-09-23
                    set @TempCalcAmt = ISNULL(@TempCalcAmt, 0)
                    set @TempAmt = ISNULL(@TempAmt, 0) 
                    if @TempCalcAmt <= 0
                      set @TempCalcAmt = @TempAmt
                                          
                    if @TempCalcAmt > @TempAmt
                    begin
                      update @D set NETAMOUNT = NETAMOUNT - @TempAmt where SEQ = @TempAmtSeq
                      set @TempAmt = 0
                    end else if @TempCalcAmt <= @TempAmt
                    begin
                      Delete from @D where SEQ = @TempSeq
                      set @TempAmt= @TempAmt - @TempCalcAmt
                    end  
                  end                   
				          set @IsHit = 1
			          end else
			            set @IsHit = 0                                  
              end           
              FETCH FROM CUR_PromoteRule_HitPLU INTO @EntityType, @EntityNum
            END
            CLOSE CUR_PromoteRule_HitPLU 
            DEALLOCATE CUR_PromoteRule_HitPLU
            
            if @IsHit = 1
            begin
              insert into @HitPromotion(PromotionCode, HitTotalAmount, HitSeq, HitCount)  values(@PromotionCode, @DAmt, @HitSeq, @HitCount)
              if @HitRelation = 2
              begin 
                set @HitMutex = @MutexFlag
                break
              end  
            end
          end     
        end
 
        FETCH FROM CUR_PromoteRule_Hit INTO @HitSeq, @HitType, @HitValue, @HitOP, @HitItem
      END
      CLOSE CUR_PromoteRule_Hit 
      DEALLOCATE CUR_PromoteRule_Hit 
      
      -- 检查Hit 条件之间关系.
      if @HitRelation = 1    --   与的关系(and)   
      begin     
        if exists(select * from @HitPromotion)
        begin   
          if exists(select * from Promotion_Hit A 
                       left join @HitPromotion B on A.PromotionCode = B.PromotionCode and A.HitSeq = B.HitSeq 
                     where B.PromotionCode is null and A.PromotionCode = @PromotionCode)
            delete from @HitPromotion where PromotionCode = @PromotionCode 
          else 
            set @HitMutex = @MutexFlag
        end     
      end
          
      FETCH FROM CUR_PromoteRule_H INTO @PromotionCode, @PromotionDesc, @TopLimit, @MutexFlag, @HitRelation
    END
    CLOSE CUR_PromoteRule_H 
    DEALLOCATE CUR_PromoteRule_H 
 
    -- 计算Gift
    DECLARE CUR_HitPromotionCode CURSOR fast_forward FOR
      select A.PromotionCode, max(A.HitTotalAmount) as HitTotalAmount, max(B.PromotionDesc1) as PromotionDesc, 
         sum(HitCount) as HitCount 
        from @HitPromotion A 
        left join Promotion_H B on A.PromotionCode = B.PromotionCode
        group by A.PromotionCode   
    OPEN CUR_HitPromotionCode
    FETCH FROM CUR_HitPromotionCode INTO @HitPromotionCode, @HitTotalAmount, @hitPromotionDesc, @HitPromotionCount
    WHILE @@FETCH_STATUS=0
    BEGIN
      set @HitPromotionCount = ISNULL(@HitPromotionCount, 1)
      if @HitPromotionCount <= 0
        set @HitPromotionCount = 1
      set @i = @HitPromotionCount
      while @i > 0 
      begin  
        DECLARE CUR_HitPromotionGift CURSOR fast_forward FOR
          select PromotionGiftType, isnull(PromotionValue,0), isnull(PromotionAdjValue, 0), isnull(PromotionMultipleValue, 1)
            from Promotion_Gift where PromotionCode = @HitPromotionCode
          order by GiftSeq  
        OPEN CUR_HitPromotionGift
        FETCH FROM CUR_HitPromotionGift INTO @PromotionGiftType, @PromotionGiftValue, @PromotionAdjValue, @PromotionMultipleValue
        WHILE @@FETCH_STATUS=0
        BEGIN
          set @PromotionMultipleValue = ISNULL(@PromotionMultipleValue, 1)
          if @PromotionMultipleValue = 0
            set @PromotionMultipleValue = 1 
                   
          if @PromotionGiftType = 1    --现金返还 (固定金额)
          begin
            set @GiftAmount = @PromotionGiftValue + @PromotionAdjValue
            if @GiftAmount + @CardTotalAmount > @CardGradeMaxAmount
              set @GiftAmount = @CardGradeMaxAmount - @CardTotalAmount      
            insert into Card_Movement
              (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
               CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	             OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
	          select 27, @CardNumber, null, 0, @TxnNo, TotalAmount, @GiftAmount, TotalAmount+@GiftAmount, 0, @BusDate, @Txndate, 
	             null, null, @HitPromotionCode, null, '', @CashierID, @StoreID, @RegisterCode, @ServerCode,
	             TotalPoints, TotalPoints, CardExpiryDate, CardExpiryDate, Status, Status 
	           from card where CardNumber = @CardNumber  
  	                
          end else if @PromotionGiftType = 2  -- 现金返还 (消费金额的百分数)  例如  0.1.  消费1000， 返还100
          begin
            set @GiftAmount = isnull(@HitTotalAmount,0) * @PromotionGiftValue + @PromotionAdjValue
            if @GiftAmount + @CardTotalAmount > @CardGradeMaxAmount
              set @GiftAmount = @CardGradeMaxAmount - @CardTotalAmount                
            insert into Card_Movement
              (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
               CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
	          select 27, @CardNumber, null, 0, @TxnNo, TotalAmount, @GiftAmount, TotalAmount+@GiftAmount, 0, @BusDate, @Txndate, 
	             null, null, @HitPromotionCode, @hitPromotionDesc, '', @CashierID, @StoreID, @RegisterCode, @ServerCode,
	             TotalPoints, TotalPoints, CardExpiryDate, CardExpiryDate, Status, Status 
	           from card where CardNumber = @CardNumber 	                 
          end else if @PromotionGiftType = 4  -- 积分返还 （固定值）
          begin
            set @GiftPoint = @PromotionGiftValue + @PromotionAdjValue
            set @GiftPoint = @GiftPoint * @PromotionMultipleValue 
            if @GiftPoint + @CardTotalPoint > @CardGradeMaxPoint and @CardGradeMaxPoint > 0
              set @GiftPoint = @CardGradeMaxPoint - @CardTotalPoint                     
            insert into Card_Movement
              (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
               CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
	          select 27, @CardNumber, null, 0, @TxnNo, TotalAmount, 0, TotalAmount, @GiftPoint, @BusDate, @Txndate, 
	             null, null, @HitPromotionCode, @hitPromotionDesc, '', @CashierID, @StoreID, @RegisterCode, @ServerCode,
	             TotalPoints, TotalPoints+@GiftPoint, CardExpiryDate, CardExpiryDate, Status, Status 
	           from card where CardNumber = @CardNumber     
          end else if @PromotionGiftType = 5  -- 积分返还 （消费金额的系数）。 例如 0.1.   消费20， 返还2 分 
          begin
            set @GiftPoint = round(@HitTotalAmount * @PromotionGiftValue, 0) + @PromotionAdjValue
            set @GiftPoint = @GiftPoint * @PromotionMultipleValue
            if @GiftPoint + @CardTotalPoint > @CardGradeMaxPoint and @CardGradeMaxPoint > 0
              set @GiftPoint = @CardGradeMaxPoint - @CardTotalPoint                
            insert into Card_Movement
              (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
               CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
	          select 27, @CardNumber, null, 0, @TxnNo, TotalAmount, 0, TotalAmount, @GiftPoint, @BusDate, @Txndate, 
	             null, null, @HitPromotionCode, @hitPromotionDesc, '', @CashierID, @StoreID, @RegisterCode, @ServerCode,
	             TotalPoints, TotalPoints+@GiftPoint, CardExpiryDate, CardExpiryDate, Status, Status 
	           from card where CardNumber = @CardNumber      
          end else if @PromotionGiftType = 3  -- 赠送Coupon。
          begin
  ----print '赠送Coupon。' 
  ----print @CouponNumber      
  ----print @PromotionGiftValue
            set @CouponTypeID = cast(@PromotionGiftValue as int)
            --select top 1 @CouponNumber = CouponNumber from coupon where CouponTypeID = @CouponTypeID and Status = 1  
            exec GetCouponNumber @CouponTypeID, 1, @CouponNumber output    
            select @CouponStatus = Status, @CouponAmount = CouponAmount, @CouponExpiryDate=CouponExpiryDate from Coupon where CouponNumber = @CouponNumber              
            exec CalcCouponNewStatus @CouponNumber, @CouponTypeID, 32, @CouponStatus, @NewCouponStatus output
            exec CalcCouponNewExpiryDate @CouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
            insert into Coupon_Movement
              (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
               BusDate, Txndate, Additional, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
               OrgExpirydate, OrgStatus, NewStatus)    
            select  
                27, @CardNumber, CouponNumber, CouponTypeID, '', 0, @TxnNo, CouponAmount, 0, CouponAmount,
                @BusDate, @TxnDate, @HitPromotionCode, @hitPromotionDesc, '', @MemberID, @NewCouponExpiryDate, '', null, '', '',
                @CouponExpiryDate, 1, 2
              from Coupon where CouponNumber = @CouponNumber
          end

          FETCH FROM CUR_HitPromotionGift INTO @PromotionGiftType, @PromotionGiftValue, @PromotionAdjValue, @PromotionMultipleValue
        END
        CLOSE CUR_HitPromotionGift 
        DEALLOCATE CUR_HitPromotionGift 
        set @i = @i - 1
      end
      FETCH FROM CUR_HitPromotionCode INTO @HitPromotionCode, @HitTotalAmount, @hitPromotionDesc, @HitPromotionCount
    END
    CLOSE CUR_HitPromotionCode 
    DEALLOCATE CUR_HitPromotionCode    
   
    -- 清空临时表
    delete from @HitPromotion
           
    FETCH FROM CUR_DoPromote INTO @TxnNo, @StoreID, @StoreGroupID, @BrandID, @CardNumber, @MemberID, @CardTypeID, @CardGradeID, 
          @MemberDateOfBirth, @CardBrandID, @CumulativeEarnPoints, @ReferCardNumber, @TotalAmount, @CashierID,
          @RegisterCode, @ServerCode
  END
  CLOSE CUR_DoPromote 
  DEALLOCATE CUR_DoPromote  

  update sales_H set PromotionFlag = 1
    where isnull(PromotionFlag,0) = 0 and (TransNum = @TransNo or isnull(@TransNo,'') = '')   
end

GO
