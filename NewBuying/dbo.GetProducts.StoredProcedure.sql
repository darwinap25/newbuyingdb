USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProducts]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProducts]
  @StoreCode             VARCHAR(64),              -- 店铺编码
  @Barcode               VARCHAR(64),              -- 条码
  @Prodcode              VARCHAR(64),              -- 货品编码
  @DepartCode            VARCHAR(64),              -- 部门编码
  @IsIncludeChild        INT,                      -- 0：不包括部门子部门。1：包括部门子部门。  
  @BrandCode             VARCHAR(64),              -- 品牌ID
  @ProdType              INT,                      -- 货品类型
  @ReturnMode            INT=0,                    -- 0: 完整返回数据。 1：只返回部分字段。 默认0.  （目的：减少数据传输）
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetProducts
**  Version : 1.0.0.2
**  Description : 获得货品的数据.(必须要输入@StoreCode，因为货品和价格都是关联店铺的。如果没有输入storecode，货品的价格会有问题)
**
  declare @a int, @PageCount int, @RecordCount int
  exec @a = GetProducts  '', '','501217D332981304','',0,'',0,0,  '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
  select * from buy_product where prodcode = '01110001a01'
  SELECT * FROM BUY_RPRICE_M where prodcode = '01110001a01'
**  Created by Gavin @2015-03-06
**  Modify by Gavin @2017-09-27 (ver 1.0.0.1) 增加返回字段BOMCount(如果是BOM主货品的话，返回包含的子货品数量)
**  Modify by Gavin @2017-10-10 (ver 1.0.0.2) 修正多店铺取不同价格时的问题。 
                                              PPFile的内容改为BUY_PRODUCT->ProdPicFile
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1  
  SET @StoreCode = ISNULL(@StoreCode, '')
  SET @Barcode = ISNULL(@Barcode, '')
  SET @Prodcode = ISNULL(@Prodcode, '')
  SET @DepartCode = ISNULL(@DepartCode, '')
  SET @IsIncludeChild = ISNULL(@IsIncludeChild, 0)
  SET @BrandCode = ISNULL(@BrandCode, '')
  SET @ProdType = ISNULL(@ProdType, 0)  
  
  SET @WhereStr = ' WHERE 1=1 '
  IF @Barcode <> ''  
    SET @WhereStr = @WhereStr + ' AND A.ProdeCode in (SELECT ProdCode FROM BUY_BARCODE WHERE Bracode = ''' + @Barcode + ''') '
  IF @StoreCode <> ''  
    SET @WhereStr = @WhereStr + ' AND (A.StoreCode = ''' + @StoreCode + ''' OR A.StoreCode = '''') '
  IF @Prodcode <> ''  
    SET @WhereStr = @WhereStr + ' AND A.Prodcode = ''' + @Prodcode + ''' '   
  IF @DepartCode <> ''  
    SET @WhereStr = @WhereStr + ' AND A.DepartCode = ''' + @DepartCode + ''' '     
  IF @BrandCode <> ''
    SET @WhereStr = @WhereStr + ' AND A.ProductBrandCode = ''' + @BrandCode + ''' '   
  IF @ProdType > 0
    SET @WhereStr = @WhereStr + ' AND A.ProductType = ' + CAST(@ProdType AS VARCHAR)
      
  SET @SQLStr = 'SELECT A.ProdCode, CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN ProdDesc2 WHEN 3 THEN ProdDesc3 ELSE ProdDesc1 END AS ProdDesc, '
    + ' A.DepartCode, A.ProdPicFile, A.ProductBrandCode AS BrandCode,'
    + ' CASE WHEN ISNULL(B1.Price,0)=0 THEN B2.Price ELSE B1.Price END AS ProdPrice'
  IF @ReturnMode = 0
  BEGIN  
    SET @SQLStr = @SQLStr + ', CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN ScanDesc2 WHEN 3 THEN ScanDesc3 ELSE ScanDesc1 END AS ScanDesc, '
    + ' A.PackageSizeCode,A.StoreCode,A.MinOrderQty,A.OrderType,A.WarehouseCode, A.ProdClassCode, A.GapProdCode, A.ShelfLife, '
    + ' A.ProdSpec, A.ProdLength, A.ProdWidth, A.ProdHeight, A.RefGP, A.NonOrder, A.NonSale, A.Consignment, A.WeightItem, '
    + ' A.DiscAllow, A.CouponAllow, A.VisuaItem, A.TaxRate, A.ImportTax, A.Insurance, A.Freight, A.OthersExpense, '
    + ' A.OriginCountryCode, A.ProductType, A.Modifier, A.BOM, A.MutexFlag, A.OnAccount, A.FulfillmentHouseCode, '
    + ' A.ReplenFormulaCode, A.DiscountLimit, A.QuotaPerShopPeriod, A.CouponSKU, A.DefaultPickupStoreCode, A.ColorCode, '
    + ' A.InTax, A.Additional, ISNULL(BX.BOMCount, 0) AS BOMCount, A.ProdPicFile AS PPFile, '
    + ' CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN B.ProductBrandName2 WHEN 3 THEN B.ProductBrandName3 ELSE B.ProductBrandName1 END AS BrandName '
  END

  SET @SQLStr = @SQLStr + ' FROM BUY_PRODUCT A '
    + ' LEFT JOIN Product_BRAND B ON A.ProductBrandCode = B.ProductBrandCode '
    + ' LEFT JOIN (SELECT ProdCode, Price FROM BUY_RPRICE_M WHERE Status = 1 AND KeyID IN (SELECT max(KeyID) as KeyID FROM BUY_RPRICE_M WHERE (ISNULL(StoreCode,'''')='''' OR ISNULL(StoreCode,'''')=''' + + @StoreCode + ''') Group by ProdCode) AND StartDate <= GETDATE() AND EndDate >= GETDATE() AND ISNULL(StoreCode,'''') = ''' + @StoreCode + ''') B1 on A.ProdCode = B1.ProdCode '
    + ' LEFT JOIN (SELECT ProdCode, Price FROM BUY_RPRICE_M WHERE Status = 1 AND KeyID IN (SELECT max(KeyID) as KeyID FROM BUY_RPRICE_M WHERE (ISNULL(StoreCode,'''')='''' OR ISNULL(StoreCode,'''')=''' + + @StoreCode + ''') Group by ProdCode) AND StartDate <= GETDATE() AND EndDate >= GETDATE() AND ISNULL(StoreCode,'''') = '''') B2 on A.ProdCode = B2.ProdCode ' 
	+ ' LEFT JOIN (SELECT BOMCode, SUM(MinQty) AS BOMCount FROM BUY_BoxSale GROUP BY BOMCode) BX ON A.ProdCode = BX.BOMCode '
	+ ' LEFT JOIN (select ProdCode, MIN(ProductFullPicFile) AS PPFile from BUY_PRODUCT_PIC WHERE IsVideo = 0 AND Is360Pic = 0 AND IsSizeCategory = 0 GROUP BY ProdCode) PP ON A.ProdCode = PP.ProdCode '
  SET @SQLStr = @SQLStr + @WhereStr

  EXEC SelectDataInBatchs @SQLStr, 'DepartCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr
  
  RETURN 0
END

GO
