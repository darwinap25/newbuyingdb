USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardExtensionRule]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardExtensionRule](
	[ExtensionRuleID] [int] IDENTITY(1,1) NOT NULL,
	[RuleType] [int] NOT NULL DEFAULT ((0)),
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[ExtensionRuleSeqNo] [int] NOT NULL,
	[MaxLimit] [int] NULL,
	[RuleAmount] [money] NULL,
	[Extension] [int] NULL,
	[ExtensionUnit] [int] NULL DEFAULT ((0)),
	[ExtendType] [int] NULL DEFAULT ((0)),
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL DEFAULT ((0)),
	[SpecifyExpiryDate] [datetime] NULL,
	[LimitExpiryDate] [datetime] NULL DEFAULT ('2100-01-01'),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARDEXTENSIONRULE] PRIMARY KEY CLUSTERED 
(
	[ExtensionRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'ExtensionRuleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则类型：
0：卡有效期延长规则。 1：金额有效期规则。 2：积分有效期规则' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'RuleType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则记录序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'ExtensionRuleSeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'按照ExtensionUnit单位， 最大增加数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'MaxLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'增值金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'RuleAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expirydate 延长日期，单位看ExtendDateUnit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'Extension'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效期延长单位
0：永久。 1：年。 2：月。 3：星期。 4：天。5: 有效期不变更。 6：指定有效期。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'ExtensionUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效期延长方式：
0：增值时重置有效期。（以当前日期为基础增加有效期） 
1：增值时延长原有有效期。（以原有有效期为基础增加有效期） 
2：增值时不变更有效期，忽略cardtype，coupontype的有效期延长设置。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'ExtendType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0： 无效。  1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定的有效期。ExtensionUnit=6 时有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'SpecifyExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'极限有效期。（扩展有效期时的极限值），默认值 2100-01-01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule', @level2type=N'COLUMN',@level2name=N'LimitExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡有效期延长规则
   增值规则阶梯设置：
   增值满     延长时间
   100           90 天
   200           180天
   500           360天
   匹配时按照金额排序，按先匹配的执行'', 

@2014-09-15: 增加指定有效期功能，增加有效期时，加上设定限值。 
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardExtensionRule'
GO
