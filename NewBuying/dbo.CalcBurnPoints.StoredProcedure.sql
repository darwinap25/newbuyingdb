USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcBurnPoints]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[CalcBurnPoints]
   @CardNumber			varchar(512),    -- 操作的卡号
   @Amount           money,           -- 操作金额。（希望换取的金额）
   @Points           int,             -- 操作积分。（希望用于换取金额的积分）
   @OutAmount           money output,    -- 如果填写@ActAmount，则返回0， 如果填写@ActPoints，则返回能换取的金额
   @OutPoints	        int out          -- 如果填写@ActAmount，则返回需要支付的积分， 如果填写@ActPoints，则返回0
AS
/****************************************************************************  
**  Name : CalcBurnPoints  
**  Version: 1.0.0.0  
**  Description : 计算使用积分抵充消费时，按规则计算消费金额对应的积分。  
**  
select * from card where memberid = 1
  declare @a int, @OutAmount money, @OutPoints int
  exec @a = CalcBurnPoints '0001000001802', 0, 1000, @OutAmount output, @OutPoints output
  print @a
  print @OutAmount 
  print @OutPoints 
**       
**  Created by: Gavin @2011-04-13  
**  
****************************************************************************/  
begin
  declare @CardGradeID int, @CardPointToAmountRate decimal(16,6), @CardAmountToPointRate decimal(16,6), @TotalAmount money, @TotalPoints int
  declare @TempValue float,  @ActAmount money, @ActPoints int, @CardExpiryDate datetime, @CardStatus int

  set @Amount = abs(isnull(@Amount, 0))
  set @Points = abs(isnull(@Points, 0))
  set @OutAmount = 0  
  set @OutPoints = 0

  select @CardGradeID = CardGradeID, @TotalAmount = isnull(TotalAmount,0), @TotalPoints = isnull(TotalPoints,0), @CardExpiryDate = CardExpiryDate, @CardStatus = status
    from Card where CardNumber = @CardNumber  
  select @CardPointToAmountRate = CardPointToAmountRate, @CardAmountToPointRate = CardAmountToPointRate
    from CardGrade where CardGradeID = @CardGradeID  
   
    if isnull(@CardPointToAmountRate, 0) <> 0
    begin          
      if @Amount <> 0
      begin
        set @ActAmount = @Amount
        set @ActPoints = - (@Amount * @CardPointToAmountRate)
        set @OutAmount = 0
        set @OutPoints = abs(@ActPoints)
      end else if @Points <> 0 
      begin
        set @ActPoints = -@Points
        set @ActAmount = @Points / @CardPointToAmountRate   
        set @OutAmount = abs(@ActAmount)
        set @OutPoints = 0     
      end              
    end else
      return -65   
  return 0
end

GO
