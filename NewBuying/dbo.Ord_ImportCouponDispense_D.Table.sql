USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportCouponDispense_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportCouponDispense_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponDispenseNumber] [varchar](64) NOT NULL,
	[CampaignCode] [varchar](64) NULL,
	[CouponTypeCode] [varchar](64) NOT NULL,
	[MemberRegisterMobile] [nvarchar](512) NOT NULL,
	[ExportDatetime] [datetime] NULL,
	[CardNumber] [varchar](64) NULL,
 CONSTRAINT [PK_ORD_IMPORTCOUPONDISPENSE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D', @level2type=N'COLUMN',@level2name=N'CouponDispenseNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D', @level2type=N'COLUMN',@level2name=N'CampaignCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D', @level2type=N'COLUMN',@level2name=N'CouponTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员注册用手机号（加上国家代码的，例如0861392572219）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D', @level2type=N'COLUMN',@level2name=N'MemberRegisterMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导出时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D', @level2type=N'COLUMN',@level2name=N'ExportDatetime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡号，分发的coupon，绑定在此卡上' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分发优惠劵明细表
CardNumber：在填写数据时，根据MemberRegisterMobile，以及CouponTypeCode获得。 （cardnumber和coupontypecode 为相同的brandcode，同一个brandcode下，如果有1个以上的卡，cardnumber填空）。
绑定coupon时，绑定在此cardnumber下。 cardnumber为空则为错。

@2017-03-28 增加字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponDispense_D'
GO
