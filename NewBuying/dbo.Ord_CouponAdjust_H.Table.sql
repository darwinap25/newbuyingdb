USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponAdjust_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponAdjust_H](
	[CouponAdjustNumber] [varchar](64) NOT NULL,
	[OprID] [int] NOT NULL,
	[OriginalTxnNo] [varchar](512) NULL,
	[TxnDate] [datetime] NULL,
	[StoreCode] [varchar](64) NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[BrandCode] [varchar](64) NULL,
	[ReasonID] [int] NULL,
	[Note] [varchar](512) NULL,
	[ActAmount] [money] NULL,
	[ActExpireDate] [datetime] NULL,
	[CouponCount] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Ord_CouponAdjust] PRIMARY KEY CLUSTERED 
(
	[CouponAdjustNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponAdjust_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponAdjust_H] ON [dbo].[Ord_CouponAdjust_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponAdjust_H
* Version: 1.0.0.12
* Description : Coupon调整单据
*     
*  
update ord_couponadjust_h set approvestatus = 'A' where CouponAdjustNumber = 'CV0000000048'
select * from ord_couponadjust_h
select top 10 * from coupon_movement where reftxnno = 'CV0000000048'
* Create By Gavin @2012-05-22
* Modify by Gavin @2012-09-14 (ver 1.0.0.1) 按照实际输入金额来redeem. 同前台POS操作一样,需要加上forfeit记录.
* Modify by Gavin @2012-09-24 (ver 1.0.0.2) 增加 OprID=44的处理。
* Modify by Gavin @2012-10-09 (ver 1.0.0.3) 修正Oprid=38时的有效期处理。此时不需要调用CalcCouponNewExpiryDate，直接使用传入的有效期。
* Modify by Gavin @2012-10-15 (ver 1.0.0.4) 修正OprID=38，调整有效期的更新错误。
* Modify by Gavin @2012-10-24 (ver 1.0.0.5) 使用ApproveBy 填写到movement的 createdby。
* Modify by Gavin @2014-10-08 (ver 1.0.0.6) 修正：update Ord_CouponAdjust_H 语句放入if (@OldApproveStatus = 'P' .... 范围内。
* Modify by Gavin @2015-01-06 (ver 1.0.0.7) 增加对Coupon的统一判断,如果其中有Coupon的状态或者stockstatus 不正确，则raise error，统一回滚
* Modify by Gavin @2015-06-04 (ver 1.0.0.8) 增加对表单的校验(因为可能是导入的数据). 
* Modify by Gavin @2015-06-08 (ver 1.0.0.9) 对于ver 1.0.0.8的修改, ROLLBACK 不能中止下面的操作, 必须加上return
* Modify by Gavin @2015-06-10 (ver 1.0.0.10) 对于ver 1.0.0.8 的改动,增加判断条件: 除了 void的操作 （OprID <> 35）
* Modify by Gavin @2015-06-10 (ver 1.0.0.11) 对于ver 1.0.0.10 的改动,增加判断条件: 除了 void的操作还要加上 38,41,44
* Modify by Gavin @2018-08-22 (ver 1.0.0.12) (for bauhaus)去掉校验部分.bauhaus 不需要这么复杂，也不用raiseerror  
*/
/*==============================================================*/
BEGIN  
  declare @CouponAdjustNumber varchar(512), @OprID int, @RefTxnNo varchar(512), @TxnDate datetime, @ShopCode varchar(512),@BrandCode varchar(512), @ServerCode varchar(512), @RegisterCode varchar(512), @ReasonID int, @ActExpireDate datetime, @ApproveStatus char(1), @CreatedBy int, @Note nvarchar(512)
  declare @KeyID int, @CouponNumber varchar(512), @CouponExpiryDate datetime, @CardNumber varchar(512), @CouponTypeID int
  declare @OldApproveStatus char(1), @OpenBal money, @ActAmount money,@CloseBal money, @Additional nvarchar(512), @Remark nvarchar(512), @CardExpiryDate datetime
  declare @ApprovalCode char(6)  
  declare @BusDate date --Business Date
  declare @BrandID int, @StoreID int, @CouponStatus int, @NewCouponStatus int, @NewCouponExpiryDate datetime

  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())
  
  DECLARE CUR_Ord_CouponAdjust_H CURSOR fast_forward local FOR
    SELECT CouponAdjustNumber, OprID, isnull(OriginalTxnNo,''), isnull(TxnDate, getdate()), isnull(BrandCode,''), isnull(StoreCode,''), ServerCode, isnull(RegisterCode,''), ReasonID, Note, ActExpireDate, ApproveStatus, ApproveBy
     FROM INSERTED
  OPEN CUR_Ord_CouponAdjust_H
  FETCH FROM CUR_Ord_CouponAdjust_H INTO @CouponAdjustNumber, @OprID, @RefTxnNo, @TxnDate, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @ReasonID, @Note, @ActExpireDate, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN    
    set @Txndate=isnull(@Txndate,getdate()) 
    select @OldApproveStatus = ApproveStatus from Deleted where CouponAdjustNumber = @CouponAdjustNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A'    
    begin

/*  ver 1.0.0.12  mask
      if (@OprID not in (35,38,41,44)) and (@BrandCode = '' or @ShopCode = '' or @RegisterCode = '' or @RefTxnNo = '')
      begin
        RAISERROR ('Order missing data.', 16, 1)
        if (@@Trancount > 0)
          ROLLBACK TRANSACTION        
        return  
      end
      -- ver 1.0.0.7 -------------
      if @OprID = 33
      begin
        if exists(
                   select * from (
                                  select A.KeyID, B.Status, B.StockStatus 
                                     from Ord_CouponAdjust_D A left join Coupon B on A.CouponNumber = B.CouponNumber 
                                    where A.CouponAdjustNumber = @CouponAdjustNumber
                                 ) A where Status <> 1 or StockStatus <> 6
                 )
        begin
          RAISERROR ('Coupon status incorrect.', 16, 1)
          if (@@Trancount > 0)
            ROLLBACK TRANSACTION           
          return
        end     
      end
      if @OprID = 34
      begin
        if exists(
                   select * from (
                                  select A.KeyID, B.Status, B.StockStatus 
                                     from Ord_CouponAdjust_D A left join Coupon B on A.CouponNumber = B.CouponNumber 
                                    where A.CouponAdjustNumber = @CouponAdjustNumber
                                 ) A where Status <> 2 or StockStatus <> 6
                 )
        begin
          RAISERROR ('Coupon status incorrect.', 16, 1)
          if (@@Trancount > 0)
            ROLLBACK TRANSACTION 
          return          
        end     
      end      
      -- end -------------
*/      
      exec GenApprovalCode @ApprovalCode output 
      DECLARE CUR_Ord_CouponAdjust_D CURSOR fast_forward local FOR
        SELECT KeyID, CouponNumber,isNull(CouponAmount,0) FROM Ord_CouponAdjust_D where CouponAdjustNumber = @CouponAdjustNumber        
      OPEN CUR_Ord_CouponAdjust_D
      FETCH FROM CUR_Ord_CouponAdjust_D INTO @KeyID, @CouponNumber,@ActAmount
      WHILE @@FETCH_STATUS=0
      BEGIN
        select @CouponExpiryDate = CouponExpiryDate, @CardNumber = CardNumber, @CouponTypeID = CouponTypeID,  
          @OpenBal=CouponAmount, @CouponStatus = status from coupon where CouponNumber = @CouponNumber          
	
        if @OprID in (33, 44)
        begin
          set @CloseBal=@ActAmount
          set @ActAmount=@ActAmount-@OpenBal
        end
        else if @OprID = 34  
        begin		  
          set @CloseBal=@OpenBal - @ActAmount
        end
        else
        begin
          set @ActAmount=0
          set @CloseBal=@OpenBal
        end
     
        select @BrandID=StoreBrandID from Brand where StoreBrandCode=@BrandCode
        select @StoreID=StoreID from Store where BrandID=@BrandID and StoreCode=@ShopCode   
		
		    exec CalcCouponNewStatus @CouponNumber, @CouponTypeID, @OprID, @CouponStatus, @NewCouponStatus output
		    if @OprID = 38
		    begin
		      set @NewCouponExpiryDate = @ActExpireDate
		      --set @ActExpireDate = @CouponExpiryDate		
		    end else
        begin
          --set @ActExpireDate = @CouponExpiryDate		
          exec CalcCouponNewExpiryDate @CouponTypeID, @OprID, @CouponExpiryDate, @NewCouponExpiryDate output 	
        end  
        insert into Coupon_Movement
          (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
           BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode,ServerCode, StoreID, RegisterCode,
           OrgExpiryDate, OrgStatus, NewStatus)
        values
          (@OprID, @CardNumber, @CouponNumber, @CouponTypeID, '', 0, @CouponAdjustNumber, @OpenBal, @ActAmount, @CloseBal,
           @BusDate, @TxnDate, @Note, '', @CreatedBy, @NewCouponExpiryDate, @ApprovalCode,@ServerCode, @StoreID, @RegisterCode,
           @CouponExpiryDate, @CouponStatus, @NewCouponStatus)
        -- 增加forfeit处理   
        if @OprID = 34 and @Closebal > 0 
          exec DoCouponForfeit @CreatedBy, @CouponNumber, @CouponAdjustNumber, @BusDate, @TxnDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode, 0                        
        --End          
            
        FETCH FROM CUR_Ord_CouponAdjust_D INTO @KeyID, @CouponNumber,@ActAmount
      END
      CLOSE CUR_Ord_CouponAdjust_D 
      DEALLOCATE CUR_Ord_CouponAdjust_D    
      
      update Ord_CouponAdjust_H set ApprovalCode = @ApprovalCode, ApproveBusDate = @BusDate, ApproveOn = getdate(), ApproveBy = @CreatedBy  
      where CouponAdjustNumber = @CouponAdjustNumber                 
    end 
               
    FETCH FROM CUR_Ord_CouponAdjust_H INTO @CouponAdjustNumber, @OprID, @RefTxnNo, @TxnDate, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @ReasonID, @Note, @ActExpireDate, @ApproveStatus, @CreatedBy
  END
  CLOSE CUR_Ord_CouponAdjust_H 
  DEALLOCATE CUR_Ord_CouponAdjust_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'CouponAdjustNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作ID：
33、激活指定coupon
34、使用指定coupon
35、设置coupon无效（过期）
38、coupon有效期调整
40、发行优惠劵' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联的原始单据号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'OriginalTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'BrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调整原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'ReasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作金额。用于前台记录操作的金额。需要UI把这个金额填写到Detail表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'ActAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调整有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'ActExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'CouponCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵调整单据主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_H'
GO
