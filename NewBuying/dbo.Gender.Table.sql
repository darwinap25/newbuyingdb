USE [NewBuying]
GO
/****** Object:  Table [dbo].[Gender]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gender](
	[GenderID] [int] NOT NULL,
	[GenderCode] [varchar](64) NULL,
	[GenderDesc1] [varchar](64) NULL,
	[GenderDesc2] [varchar](64) NULL,
	[GenderDesc3] [varchar](64) NULL,
 CONSTRAINT [PK_GENDER] PRIMARY KEY CLUSTERED 
(
	[GenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gender', @level2type=N'COLUMN',@level2name=N'GenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gender', @level2type=N'COLUMN',@level2name=N'GenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gender', @level2type=N'COLUMN',@level2name=N'GenderDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gender', @level2type=N'COLUMN',@level2name=N'GenderDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gender', @level2type=N'COLUMN',@level2name=N'GenderDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别表。（基础表，不需要维护）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gender'
GO
