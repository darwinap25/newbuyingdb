USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetMemberMessageAccount]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SetMemberMessageAccount]
  @UserID               int,
  @ActionType           int,            -- 1:新增。 2：修改。 3：删除。 4: 验证通过。 5：失效（status置0）。 6：失效（status置1）
  @MemberID				int,
  @MessageAccountID		int,
  @MessageServiceTypeID int,
  @AccountNumber		nvarchar(512),  -- 如果是手机号的话，那么是带国家码的。
  @IsPrefer             int,
  @Note					nvarchar(512),
  @CountryCode          varchar(64),  -- 手机号码国家码。 @AccountNumber - @CountryCode = 不带国家码的手机号码   
  @PromotionFlag  int      
AS
/****************************************************************************
**  Name : SetMemberMessageAccount
**  Version: 1.0.0.5
**  Description : 维护用户的消息账号
**
**  Parameter :
  exec SetMemberMessageAccount 1, 1, 170, 1, 1, '234234342', 1, 'test', '', 1
  select * from MemberMessageAccount where memberid = 170
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-09-05
**  Modified by: Gavin @2014-01-15 (ver 1.0.0.1) 当更新的是email账号时，同步更新member表的 memberemail
**  Modified by: Gavin @2014-05-22 (ver 1.0.0.2) 增加手机号码验证后保存更新VerifyFlag功能
**  Modified by: Gavin @2014-06-25 (ver 1.0.0.3) 修改时,把VerifyFlag标志改为 0
**  Modified by: Gavin @2014-11-27 (ver 1.0.0.4) 增加输入参数@PromotionFlag 
**  Modified by: Gavin @2015-05-05 (ver 1.0.0.5) 增加@ActionType=5：  失效，  @ActionType=6：  生效
**
****************************************************************************/
begin
  set @CountryCode = isnull(@CountryCode, '') 
  set @PromotionFlag = ISNULL(@PromotionFlag, 0)
  if @ActionType = 1
    insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag, PromotionFlag)
    values (@MemberID, @MessageServiceTypeID, @AccountNumber, @IsPrefer, 1, @Note, Getdate(), @UserID, Getdate(), @UserID, 0, @PromotionFlag)
  else if @ActionType = 2
    update MemberMessageAccount 
	  set AccountNumber = @AccountNumber, MessageServiceTypeID = @MessageServiceTypeID, Note = @Note, IsPrefer = @IsPrefer, UpdatedOn = GetDate(), UpdatedBy = @UserID, VerifyFlag = 0, PromotionFlag = @PromotionFlag
    where MemberID = @MemberID and MessageAccountID = @MessageAccountID
  else if @ActionType = 3
    delete from MemberMessageAccount where MemberID = @MemberID and MessageAccountID = @MessageAccountID
  else if @ActionType = 5
    Update MemberMessageAccount set Status = 0, UpdatedOn = GetDate(), UpdatedBy = @UserID where MemberID = @MemberID and MessageAccountID = @MessageAccountID 
  else if @ActionType = 6
    Update MemberMessageAccount set Status = 1, UpdatedOn = GetDate(), UpdatedBy = @UserID where MemberID = @MemberID and MessageAccountID = @MessageAccountID  
  else if @ActionType = 4
  begin
    if exists(select * from MemberMessageAccount 
                where MemberID = @MemberID and AccountNumber = @AccountNumber)
    begin                
      update MemberMessageAccount set VerifyFlag = 1, UpdatedOn = GetDate(), UpdatedBy = @UserID
      where MemberID = @MemberID and AccountNumber = @AccountNumber
    end else
    begin
      insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag, PromotionFlag)
      values (@MemberID, @MessageServiceTypeID, @AccountNumber, @IsPrefer, 1, @Note, Getdate(), @UserID, Getdate(), @UserID, 1, @PromotionFlag)
    end 
    if exists(select MemberMobilePhone,* from Member where MemberID = @MemberID 
                    and isnull(MemberMobilePhone, '') = '')  
      update Member set MemberMobilePhone = substring(@AccountNumber, LEN(@CountryCode) + 1, len(@AccountNumber) - len(@CountryCode)),
             CountryCode = @CountryCode 
       where MemberID = @MemberID 
  end  

  -- 如果是更新email的话，则同步更新member表的memberemail
  if @ActionType in (1, 2) and @MessageServiceTypeID = 1
    update member set MemberEmail = @AccountNumber where MemberID = @MemberID

  return 0 
end

GO
