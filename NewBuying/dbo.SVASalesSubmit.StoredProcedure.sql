USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SVASalesSubmit]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[SVASalesSubmit]
  @UserID				int,		         -- 操作员ID
  @MemberID				int,                 -- 会员ID
  @CardUID				varchar(512),		 -- 会员卡的UID
  @CardNumber			varchar(64),        -- 会员卡号。
  @BrandID			    int, 		        -- 店铺的brandID
  @StoreCode            varchar(64),        -- 店铺编码   
  @RegisterCode         varchar(64),        -- 终端编码
  @ServerCode			varchar(64),        -- server编码
  @TxnNo				varchar(512) output,-- 交易号码
  @VoidTxnNo            varchar(64),         -- @2015-08-07 增加被void的交易号
  @BusDate				datetime output,     -- 交易busdate
  @TxnDate				datetime,			 -- 交易系统时间
  @SalesType			int,				 -- 交易类型
  @CashierCode			varchar(64),		 -- 收银员Code  
  @SalesManCode			varchar(64),		 -- 销售员Code
  @TotalAmount			money,				 -- 交易总额
  @TxnStatus			int,				 -- 交易状态
  @Contact				varchar(512),		 -- 联系人
  @ContactPhone			varchar(512),        -- 联系人电话
  @Courier				varchar(512),		 -- 送货员
  @DeliveryaAddr		nvarchar(512),		 -- 送货地址
  @DeliveryDate			datetime,			 -- 送货时间
  @DeliveryCompleteDate datetime,			 -- 送货完成时间
  @SalesDetail			nvarchar(max),		 -- 交易单货品明细(交易货品, XML格式字符串)
  @SalesTender			nvarchar(max),		 -- 交易单支付明细(交易支付记录, XML格式字符串)
  @SalesReceipt         varchar(max),		 -- 交易单小票（字符串格式）  
  @SalesReceiptBIN      varbinary(max),		 -- 交易单小票（二进制格式）
  @SalesAdditional		nvarchar(512),		 -- 交易单附加信息
  @IsShoppingCart       int=0,				 -- 是否购物车订单. 0:不是. 1:是的. 默认0
  @DeliveryNumber       varchar(512)='',     -- 送货单号.
  @LogisticsProviderID   int=0,			     -- 送货的物流供应商。
  @ReturnMessageStr     varchar(max) output, -- 返回信息。（json格式）
  @ReturnAmount			money output,		 -- msvc 支付时，返回完成后的余额。 （目前，登录的卡和msvc的卡必须相同的）
  @ReturnPoint          int output,          -- 支付后的余额
  @PickupType           int = 1,             -- 提货方式。 默认 1.  1：门店自提	 2：送货
  @PickupStoreCode      varchar(64) = '',    -- 门店自提时，设置提货店铺code
  @CODFlag              int = 0,             -- 0：先款后货。 1：钱货两讫。@PickupType =2 时，表示货到付款
  @IsCalcPoint          int = 1,             -- 此单是否积分。（for VIVA 暂时使用此参数屏蔽交易积分） 
  @ForceToComplete      int = 0,             -- 0：正常情况，检查数据错误则返回。 1：强制完成，忽略错误,不考虑负积分。默认0（主要针对void的情况,如果负积分，再增加一条OprID=90的补偿记录）
  @SalesDisc            varchar(max)='',     -- sales_disc 数据 同 webbuying db
  @IsDealWithSalesData  int = 1,             -- 0: 不处理SalesD,SalesT中的数据. 1: 处理salesD,salesT中的数据,指定类型的货品处理,比如卡充值.根据不同的tender类型, 执行操作. 默认是1.
  @BillContact          nvarchar(512)='',    -- 账单人    （是账单，不是发票）
  @BillContactPhone     varchar(64)='',      -- 账单人电话 
  @BillAddress          varchar(64)=''       -- 账单地址
AS
/****************************************************************************
**  Name : SVASalesSubmit    
**  Version: 1.0.2.6  (在1.0.1.20版本上修改,基础版本, 通过开关实现不同的需要)
**  Description : 提交Online shopping 的 销售数据。 销售数据为XML格式。 
      （SVA DB上取消库存处理。 必须计算积分，只能按照promotion的方式来计算，不再按照PointRule来计算）
**  
@SalesDetail格式范例:  
<ROOT>
  <PLU TxnNo=   SeqNo=   ProdCode= 0001  ProdDesc=   Serialno=   Collected=   RetailPrice=   NetPrice=   QTY=   NetAmount=   Additional=   ReservedDate=   PickupDate=  ></PLU>
  <PLU TxnNo=   SeqNo=   ProdCode= 0002  ProdDesc=   Serialno=   Collected=   RetailPrice=   NetPrice=   QTY=   NetAmount=   Additional=   ReservedDate=   PickupDate=  ></PLU>
<ROOT>
@SalesTender格式范例:  
<ROOT>
  <TENDER TxnNo=   SeqNo=   TenderID=   TenderCode=   TenderType=   TenderName=   TenderAmount=   LocalAmount=   ExchangeRate=   Additional=  ></TENDER>
  <TENDER TxnNo=   SeqNo=   TenderID=   TenderCode=   TenderType=   TenderName=   TenderAmount=   LocalAmount=   ExchangeRate=   Additional=  ></TENDER>
</ROOT> 

  declare @a int, 
  @UserID				int,		         -- 操作员ID
  @MemberID				int,                 -- 会员ID
  @CardUID				varchar(512),		 -- 会员卡的UID
  @CardNumber			varchar(64),        -- 会员卡号。
  @BrandCode			varchar(64), 		 -- 店铺brandcode 
  @StoreCode            varchar(64),        -- 店铺编码   
  @RegisterCode         varchar(64),        -- 终端编码
  @ServerCode			varchar(64),        -- server编码
  @TxnNo				varchar(512),        -- 交易号码
  @Busdate				datetime,            -- 交易busdate
  @Txndate				datetime,			 -- 交易系统时间
  @SalesType			int,				 -- 交易类型
  @CashierCode			varchar(64),		 -- 收银员Code  
  @SalesManCode			varchar(64),		 -- 销售员Code
  @TotalAmount			money,				 -- 交易总额
  @TxnStatus			int,				 -- 交易状态
  @Contact				varchar(512),		 -- 联系人
  @ContactPhone			varchar(512),        -- 联系人电话
  @Courier				varchar(512),		 -- 送货员
  @DeliveryaAddr		nvarchar(512),		 -- 送货地址
  @DeliveryDate			datetime,			 -- 送货时间
  @DeliveryCompleteDate datetime,			 -- 送货完成时间
  @SalesDetail			nvarchar(max),		 -- 交易单货品明细(交易货品, XML格式字符串)
  @SalesTender			nvarchar(max),		 -- 交易单支付明细(交易支付记录, XML格式字符串)
  @SalesReceipt         varchar(max),	 -- 交易单小票   
  @SalesReceiptBIN  varbinary(max)   ,
  @ReturnMessageStr     varchar(max), @ReturnAmount money, @ReturnPoint int
  set @UserID = 1
  set @MemberID = 1
  set @CardUID = ''
  set @CardNumber = '000100086'
  set @BrandCode = '123123'
  set @StoreCode = '1'
  set @RegisterCode = '001'
  set @ServerCode = '02'
  set @TxnNo = 'test201309182'
  set @Busdate = '2012-09-18'
  set @Txndate = '2012-09-18 12:12:12'  				
  set @SalesType = 0
  set @CashierCode = '4444'
  set @SalesManCode = '5555'
  set @TotalAmount = 100
  set @TxnStatus = 5
  set @Contact = 'rrg'
  set @ContactPhone = '120'
  set @Courier = 'sogo'
  set @DeliveryaAddr = 'sldkdfjjsdlkfjfgsd'
  set @DeliveryDate = '2012-09-18'
  set @DeliveryCompleteDate = '2012-09-18'
  set @SalesDetail = '<ROOT>
  <PLU TxnNo= test201309182  SeqNo= 1  ProdCode= 0001  ProdDesc= 是数据库的法律卡位  Serialno=   Collected= 1  RetailPrice= 100  NetPrice= 100  QTY= 1  NetAmount= 100  Additional=  ></PLU>
  <PLU TxnNo= test201309182  SeqNo= 2  ProdCode= 0002  ProdDesc= 塞德里克个记录款未付  Serialno=   Collected= 1  RetailPrice= 100  NetPrice= 100  QTY= 2  NetAmount= 200  Additional=  ></PLU>
</ROOT>'
  set @SalesTender = '<ROOT>
  <TENDER TxnNo= test201309182  SeqNo= 1  TenderID= 4  TenderCode= RMD  TenderName= RMD  TenderAmount= 200  LocalAmount= 200  ExchangeRate= 1  Additional=  ></TENDER>
  <TENDER TxnNo= test201309182  SeqNo= 2  TenderID= 5  TenderCode= HKD  TenderName= HKD  TenderAmount= 10  LocalAmount= 65  ExchangeRate= 6.5  Additional=  ></TENDER>
</ROOT> '
  set @SalesReceipt = ''
  exec @a = SVASalesSubmit @UserID,@MemberID,@CardUID,@CardNumber,@BrandCode,@StoreCode,@RegisterCode,@ServerCode,@TxnNo,@Busdate,
    @Txndate,@SalesType,@CashierCode,@SalesManCode,@TotalAmount,@TxnStatus,@Contact,@ContactPhone,@Courier,@DeliveryaAddr,@DeliveryDate,
    @DeliveryCompleteDate,@SalesDetail,@SalesTender,@SalesReceipt, @SalesReceiptBIN, '', 0, '', @ReturnMessageStr output,
    @ReturnAmount output, @ReturnPoint output
  print @a 
  print @ReturnMessageStr 
  print @ReturnAmount
  print @ReturnPoint

**  Created by: Gavin @2015-11-23 (1.0.2.0) (统一版本,from 1.0.1.20) (需要增加Void,refund,exchange交易处理)      
**  Modify by: Gavin @2015-12-23 (ver 1.0.2.1) Tender表的TenderID重新定义,需要做相应修改. 积分支付时,需要扣除的积分写在salesT的 additional中,不需要SP根据金额去计算                                        
**  Modify by: Gavin @2015-12-25 (ver 1.0.2.2) 如果非会员交易,则只保存销售记录,可以使用和销售coupon, 其他和卡有关操作不进行.
**  Modify by: Gavin @2016-01-25 (ver 1.0.2.3) (for demo) 特别处理: 本单交易的交易金额累加到卡的CumulativeConsumptionAmt上. (insert_card_movement中只做oprid = 3, 58 的) 
**  Modify by: Gavin @2016-03-22 (ver 1.0.2.4) (for YB) 使用积分支付时,考虑
**  Modify by: Gavin @2016-07-28 (ver 1.0.2.5) (for ALL) 通用标准： 要求调用方输入的salesD，salsT中（XML）的价格，金额都是放大100倍的。 然后在SP中做 *0.01, 恢复原价保存到DB。 这个业务逻辑将成为标准。调用方要求符合此条件。
**  Modify by: Gavin @2017-08-24 (ver 1.0.2.6) (for Merge DB) 因为DB上的Sales_T的TenderType是不可为空，所有都要填上值
                                               加上void ,refund时扣除积分的动作。
**
****************************************************************************/
begin
SET ARITHABORT ON

  declare @OldTxnStatus int, @Longitude varchar(512), @latitude varchar(512), @MemberName varchar(512), @MemberMobilePhone varchar(512),
		  @CardTotalAmount money, @CardTotalPoints int, @ExpiryDate datetime, @CardStatus int, @MemberAddress nvarchar(512), @ApprovalCode varchar(6)
  declare @SalesDData xml, @SalesTData xml,@SalesDiscData xml
  declare @EarnPoints int, @StoreID int, @CalcResult int
  declare @ReceiveList varchar(64), @MessageID int, @MessageServiceTypeID int
  declare @CardGradeID int, @BindCouponTypeID int, @BindCouponCount int, @CouponExpiryDate datetime, @CouponNumber varchar(64)
  declare @CouponAmount money, @i int
  declare @CouponStatus int, @NewCouponStatus int, @NewCouponExpiryDate date
  declare @Result int, @OprID int, @AddValueCardNumber varchar(64), @AddValue money, @TenderType int, @LocalAmount money, @SalesTAdditional varchar(512)
  declare @ReturnCouponTypeAmount money, @ReturnCouponTypePoint int , @ReturnCouponTypeDiscount decimal(16,6), @ReturnBindPLU varchar(64),
          @CardExpiryDate datetime,  @ReturnMessageStrCardSubmit varchar(max)
  declare @ProdType int, @CardAmountToPointRate decimal(16,6), @AddPoint int, @CardPointToAmountRate decimal(16,6)        
  declare @paymentDoneOn datetime, @PointRuleType int
  declare @MemberCardAmountBalance money, @MemberCardAmountAct money, @MemberCardPointAct int, @MemberCardPointBalance int,
          @ActCoupon varchar(512) 
  declare @InputString varchar(max), @AccountNumber nvarchar(512), @SalesDMsgString varchar(max), @SalesDMsgJSON varchar(max)            
  declare @JsonProdDesc nvarchar(512), @JsonNetPrice money, @JsonQty int, @JsonNetAmount money, @Memo1 varchar(100), @Memo2 varchar(100)
  declare @JsonTenderID int, @JsonTenderType int , @JsonTenderCode varchar(64), @JsonTenderName nvarchar(512), @JsonTenderAmount money, 
          @JsonLocalAmount money, @JsonExchangeRate decimal(16,6), @JsonSalesTAdditional varchar(512)
  declare @SalesTMsgString varchar(max), @SalesTMsgJSON varchar(max), @HasCOD int
  declare @BrandCode varchar(64), @BrandName varchar(512), @TenderCode varchar(64), @TenderID int
  declare @IsMemberSales int
   
  set @BrandID = isnull(@BrandID, 0)
  select @BrandCode = StoreBrandCode, @BrandName = StoreBrandName1  from brand where StorebrandID = @BrandID
  set @BrandCode = isnull(@BrandCode, '')
  set @BrandName = isnull(@BrandName, '')        
  set @PointRuleType = 0     
  set @HasCOD = 0        
  set @IsShoppingCart = isnull(@IsShoppingCart, 0)
    
  declare @SalesD table (TxnNo varchar(512), SeqNo int, ProdCode varchar(64), ProdDesc nvarchar(512), Serialno varchar(512), 
        Collected int, RetailPrice money, NetPrice money, Qty int, NetAmount money, Additional varchar(512),ReservedDate datetime, PickupDate datetime)
  declare @SalesT table (TxnNo varchar(512), SeqNo int, TenderCode varchar(64), TenderID int, TenderType int, TenderName nvarchar(512), TenderAmount money, 
        LocalAmount money, ExchangeRate decimal(16,6), TenderAdditional varchar(512), SalesTAdditional varchar(512))
  declare @SalesDS table(TxnNo varchar(64), ItemSeqNo varchar(64),ProdCode varchar(64), TenderSeqNo int, TenderCode varchar(64), 
        TenderID int, TenderType int, TenderName varchar(512), SalesDiscCode varchar(64),
        SalesDiscDesc varchar(512), SalesDiscType int, SalesDiscDataType int, SalesDiscPrice money, SalesPrice money, 
        SalesDiscOffAmount money, SalesDiscQty int, AffectNetPrice int, SalesDiscLevel int)  
                
  --PLU detail数据写入临时表，判断货品库存数量
  if isnull(@SalesDetail, '') <> ''
  begin
	set @SalesDData = @SalesDetail
	Insert into @SalesD(TxnNo, SeqNo, ProdCode, ProdDesc, Serialno, Collected, RetailPrice, NetPrice, Qty, NetAmount, Additional, ReservedDate, PickupDate)
	select @TxnNo, T.v.value('@SeqNo','int') as SeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode, 
	    T.v.value('@ProdDesc','nvarchar(512)') as ProdDesc, T.v.value('@Serialno','varchar(512)') as Serialno, T.v.value('@Collected','int') as Collected, 
	    T.v.value('@RetailPrice','money')* 0.01  as RetailPrice, 
	    T.v.value('@NetPrice','money')* 0.01 as NetPrice, T.v.value('@QTY','int') as QTY, T.v.value('@NetAmount','money')* 0.01 as NetAmount, 
	    T.v.value('@Additional','varchar(512)') as Additional,
	    T.v.value('@ReservedDate','datetime') as ReservedDate, T.v.value('@PickupDate','datetime') as PickupDate	    
	  from @SalesDData.nodes('/ROOT/PLU') T(v)         
  end	
  
  --Tender detail数据写入临时表
  if isnull(@SalesTender, '') <> ''
  begin
	set @SalesTData = @SalesTender
	Insert into @SalesT(TxnNo, SeqNo, TenderID, TenderCode, TenderType, TenderName, TenderAmount, LocalAmount, ExchangeRate, TenderAdditional, SalesTAdditional)
	select @TxnNo, T.v.value('@SeqNo','int') as SeqNo, T.v.value('@TenderID','int') as TenderID, T.v.value('@TenderCode','varchar(64)') as TenderCode, isnull(S.TenderType, 0) as TenderType,
	    T.v.value('@TenderName','nvarchar(512)') as TenderName, T.v.value('@TenderAmount','money')* 0.01 as TenderAmount, 
	    T.v.value('@LocalAmount','money')* 0.01  as LocalAmount, 
	    T.v.value('@ExchangeRate','decimal(16,6)') as ExchangeRate, S.Additional,  T.v.value('@Additional','varchar(512)') as SalesTAdditional
	  from @SalesTData.nodes('/ROOT/TENDER') T(v) left join Tender S on T.v.value('@TenderCode','varchar(64)') = S.TenderCode
	  where isnull(S.TenderID, 0) <> 0	   	  
  end  
  --Discount 数据写入临时表
  if isnull(@SalesDisc, '') <> ''
  begin
  	set @SalesDiscData = @SalesDisc
	  Insert into @SalesDS(TxnNo, ItemSeqNo, ProdCode, TenderSeqNo, TenderCode, TenderID, TenderType, TenderName, SalesDiscCode,
	     SalesDiscDesc, SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, SalesDiscLevel)
	  select @TxnNo, T.v.value('@ItemSeqNo','varchar(64)') as ItemSeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode,
	     T.v.value('@TenderSeqNo','int') as TenderSeqNo, T.v.value('@TenderCode','varchar(64)') as TenderCode,
	     T.v.value('@TenderID','int') as TenderID, T.v.value('@TenderType','int') as TenderType, 
	     T.v.value('@TenderName','varchar(64)') as TenderName, 
	     T.v.value('@SalesDiscCode','varchar(64)') as SalesDiscCode, T.v.value('@SalesDiscDesc','varchar(512)') as SalesDiscDesc, 
	     T.v.value('@SalesDiscType','int') as SalesDiscType, T.v.value('@SalesDiscDataType','int') as SalesDiscDataType, 
	     T.v.value('@SalesDiscPrice','money') as SalesDiscPrice, T.v.value('@SalesPrice','money') as SalesPrice, 
	     T.v.value('@SalesDiscOffAmount','money') as SalesDiscOffAmount, T.v.value('@SalesDiscQty','int') as SalesDiscQty, 
	     T.v.value('@AffectNetPrice','int') as AffectNetPrice, T.v.value('@SalesDiscLevel','int') as SalesDiscLevel 
	  from @SalesDiscData.nodes('/ROOT/DISCOUNT') T(v) 
  end        		  
      		  
  --准备数据--- 
  exec GenApprovalCode @ApprovalCode output       -- 产生approvalcode，以后用于验证。
  
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()

  -- 获取会员   
  if isnull(@CardNumber, '') = '' and isnull(@CardUID, '') <> ''
    select @CardNumber = CardNumber from CardUIDMap where CardUID = @CardUID
  if isnull(@CardNumber, '') <> ''
    select @MemberID = MemberID, @CardTotalAmount = TotalAmount, @CardTotalPoints = isnull(TotalPoints,0),
           @ExpiryDate = CardExpiryDate, @CardStatus = Status, @CardGradeID = CardGradeID, @ReturnAmount = isnull(TotalAmount,0),
           @ReturnPoint = isnull(TotalPoints, 0)
     from Card where CardNumber = @CardNumber  
  set @MemberCardAmountBalance = @ReturnAmount
  set @MemberCardPointBalance = @ReturnPoint
  select @MemberAddress = AddressFullDetail from memberaddress where MemberFirstAddr = 1 and MemberID = @MemberID  
  select @MemberName = RTrim(LTrim(MemberChiFamilyName)) + RTrim(LTrim(MemberChiGivenName)), 
--     @MemberMobilePhone = MemberRegisterMobile from Member where MemberID = @MemberID         
    @MemberMobilePhone = MemberMobilePhone from Member where MemberID = @MemberID   

  -- 必须是会员交易，否则无意义。强制完成时直接返回0
--  if @@rowcount = 0
--  begin
--    if @ForceToComplete = 1
--	  return 0
--    else
--      return -1 
--  end
   if isnull(@MemberID, 0) = 0 and isnull(@CardNumber, '') = ''
   begin 
     set @IsMemberSales = 0
   end else
   begin
     set @IsMemberSales = 1
	 set @CardNumber = isnull(@CardNumber, '')
   end

begin tran	   -- 开始事务
  set @Result = 0

  -- 获取交易号    
  if @IsShoppingCart = 1 
  begin
    --购物车交易订单号产生规则: 一个顾客只产生一个购物车订单号.
    set @TxnStatus = 0
    if isnull(@TxnNo, '') <> ''
      select @OldTxnStatus = Status from sales_H where TransNum = @TxnNo
    else    
      set @TxnNo = 'CARTNO' + 'MID' + cast(@MemberID as varchar)
  end else
  begin
    -- 如果没有单号,则自动产生一个.
    if isnull(@TxnNo, '') <> ''
      select @OldTxnStatus = Status from sales_H where TransNum = @TxnNo
    else begin
      exec GetRefNoString 'KTXNNO', @TxnNo output      
    end  
  end
  set @OldTxnStatus = isnull(@OldTxnStatus, -1)


   -- 非购物车, 首次完成支付或者交易完成.
  if (@IsShoppingCart = 0) and (isnull(@OldTxnStatus, -1) <> @TxnStatus) and (@TxnStatus = 4 or @TxnStatus = 5)	and @OldTxnStatus < 4
  begin
    set @paymentDoneOn = getdate()
  end          
  
  --写入PLU detail数据
  if isnull(@SalesDetail, '') <> ''
  begin
    if exists(select TransNum from sales_D where TransNum = @TxnNo)   
      delete from sales_D where TransNum = @TxnNo    
    Insert into Sales_D(TransNum, SeqNo, ProdCode, ProdDesc, Serialno, Collected, UnitPrice, NetPrice, Qty, NetAmount, Additional1, CreatedBy, UpdatedBy, ReservedDate, PickupDate)
    select @TxnNo, A.SeqNo, A.ProdCode, B.ProdName1, A.Serialno, A.Collected, A.RetailPrice, A.NetPrice, A.Qty, A.NetAmount, A.Additional, @UserID, @UserID, A.ReservedDate, A.PickupDate 
	from @SalesD A left join Product B on A.ProdCode = B.ProdCode
  end  
  --写入Tender detail数据
  if isnull(@SalesTender, '') <> ''
  begin
    if exists(select TransNum from sales_T where TransNum = @TxnNo) 
      delete from sales_T where TransNum = @TxnNo    
    Insert into Sales_T(TenderType, TransNum, SeqNo, TenderID, TenderCode, TenderDesc, TenderAmount, LocalAmount, ExchangeRate, Additional, CreatedBy, UpdatedBy)
    select TenderType, @TxnNo, SeqNo, TenderID, TenderCode, TenderName, TenderAmount, LocalAmount, ExchangeRate, SalesTAdditional, @UserID, @UserID from @SalesT
	if exists(select * from @SalesT where TenderType = 0)
	  set @HasCOD = 1
  end
  
  --写入Discount detail数据
  if isnull(@SalesDisc, '') <> ''
  begin
    if exists(select TransNum from sales_Disc where TransNum = @TxnNo)
      delete from sales_Disc where TransNum = @TxnNo    
    Insert into sales_Disc(TransNum, ItemSeqNo, ProdCode, TenderSeqNo, TenderID, TenderCode, SalesDiscCode, SalesDiscDesc,  
      SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, 
      SalesDiscLevel,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)      
    select @TxnNo, ItemSeqNo, ProdCode, TenderSeqNo, TenderID, TenderCode, SalesDiscCode, SalesDiscDesc,  
      SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, 
      SalesDiscLevel, GetDate(), @UserID, GetDate(),@UserID 
    from @SalesDS A 
  end

  -- 写入sales_H数据    
  if isnull(@OldTxnStatus, -1) <> -1
  begin      
    begin
      select  @Longitude = Longitude, @latitude = latitude from CourierPosition where CourierCode = @Courier   
      update sales_H set DeliveryBy = substring(@Courier, 1, 10),  RequestDeliveryDate = @DeliveryDate, 
            DeliveryDate = @DeliveryCompleteDate, DeliveryFullAddress = substring(@DeliveryaAddr, 1, 200),
            [Status] = @TxnStatus, SalesReceipt = @SalesReceipt, DeliveryLongitude = @Longitude, Deliverylatitude = @latitude,
            UpdatedOn = getdate(), UpdatedBy = @UserID, SalesReceiptBIN = @SalesReceiptBIN, Additional = @SalesAdditional,
            DeliveryNumber = @DeliveryNumber, PaySettleDate = @paymentDoneOn, LogisticsProviderID = @LogisticsProviderID
            ,BillAddress = @BillAddress, BillContact =@BillContact, BillContactPhone=@BillContactPhone
       where TransNum = @TxnNo
    end   
  end else
  begin      
    insert into sales_H
            (BrandCode, StoreCode, RegisterCode, TransNum, BusDate, TxnDate, TransType, CashierID, SalesManID, TotalAmount, 
			       Status, DeliveryBy, DeliveryFullAddress, RequestDeliveryDate, DeliveryDate, SalesReceipt, Contact, ContactPhone,
		         MemberID, MemberName, MemberMobilePhone, CardNumber, MemberAddress,
             CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, SalesReceiptBIN, Additional, DeliveryNumber,
             PickupType, PickupStoreCode, CODFlag, BillAddress, BillContact, BillContactPhone
             , LogisticsProviderID
             )
    values
            (@BrandID, @StoreCode, @RegisterCode, @TxnNo, @BusDate, @TxnDate, @SalesType, @CashierCode, @SalesManCode, @TotalAmount,
             @TxnStatus, @Courier, @DeliveryaAddr, @DeliveryDate, @DeliveryCompleteDate, @SalesReceipt, @Contact, @ContactPhone,
			 @MemberID, @MemberName, @MemberMobilePhone, @CardNumber, @MemberAddress,             
             getdate(), @UserID, getdate(), @UserID, @SalesReceiptBIN, @SalesAdditional, @DeliveryNumber,
             @PickupType, @PickupStoreCode, @CODFlag, @BillAddress, @BillContact, @BillContactPhone
             , @LogisticsProviderID) 
  end
    
   -- 非购物车, 首次完成支付或者交易完成.
  if (@IsShoppingCart = 0) and (isnull(@OldTxnStatus, -1) <> @TxnStatus) and (@TxnStatus = 4 or @TxnStatus = 5)	and @OldTxnStatus < 4
  begin
    -- 扣减库存(取消)。
/*
    if exists(select * from @SalesD where Collected = 4)
    begin
      update ProductStockOnhand set OnhandQty = OnhandQty - B.salesqty from ProductStockOnhand A
        left join (select ProdCode, sum(isnull(Qty,0)) as salesqty from @SalesD  where Collected = 4 group by ProdCode) B on A.ProdCode = B.ProdCode
        where B.ProdCode <> '' 
    end
*/    

    -- 卡充值, 销售Coupon
	IF @IsDealWithSalesData = 1   -- 处理salesD和salesT数据
	BEGIN

		  DECLARE CUR_SalesD CURSOR fast_forward local FOR  
			select NetAmount, Additional, ProdType from @SalesD D left join Product P on D.Prodcode = P.ProdCode where P.ProdType in (2, 4)
		  OPEN CUR_SalesD  
		  FETCH FROM CUR_SalesD INTO @AddValue, @AddValueCardNumber, @ProdType
		  WHILE @@FETCH_STATUS=0  
		  BEGIN
			if isnull(@AddValueCardNumber, '') = ''
			  set @AddValueCardNumber = @CardNumber
 
			if @ProdType = 2	  -- 卡充值金额
			begin
			  set @OprID = 2
			  if @SalesType in (9, 10)
				set @OprID = 4
			  exec @Result = MemberCardSubmit @UserID, @AddValueCardNumber, @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID, @AddValue, 0, @VoidTxnNo, '', '', '', '', @ReturnAmount output, @ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output
			  if @AddValueCardNumber = @CardNumber
			  begin
				set @MemberCardAmountAct = @AddValue
			  end    	    
			end
			if @ProdType = 4	  -- 卡充值积分
			begin
			  set @OprID = 14
			  if @SalesType in (9, 10)
				set @OprID = 4
			  select @CardAmountToPointRate = isnull(CardAmountToPointRate, 0) from Card C left join CardGrade G on C.CardGradeID = G.CardGradeID where C.CardNumber = @AddValueCardNumber
			  set @AddPoint = @AddValue  / @CardAmountToPointRate

			  if @AddValueCardNumber = @CardNumber
			  begin
				set @MemberCardPointAct = @AddPoint
			  end 	    	    
  			  exec @Result = MemberCardSubmit @UserID, @AddValueCardNumber, @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID, 0, @AddPoint, @VoidTxnNo, '', '', '', '', @ReturnAmount output, @ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output
			end

			if @Result <> 0
			begin
			  if @ForceToComplete = 1
			  begin 
				break
			  end
			  else
			  begin 
				rollback tran        
				return @Result
			  end
			end  
			FETCH FROM CUR_SalesD INTO @AddValue, @AddValueCardNumber, @ProdType 
		  END  
		  CLOSE CUR_SalesD   
		  DEALLOCATE CUR_SalesD 

	  
	   
      -- 卡消费,使用Coupon
      DECLARE CUR_SalesT CURSOR fast_forward local FOR  
        select TenderType, LocalAmount, SalesTAdditional, TenderID, TenderCode from @SalesT where TenderType in (7,15,16)
      OPEN CUR_SalesT 
      FETCH FROM CUR_SalesT INTO @TenderType, @LocalAmount, @SalesTAdditional, @TenderID, @TenderCode
      WHILE @@FETCH_STATUS=0  
      BEGIN
        if @TenderType = 16     -- 卡消费
        begin
          if isnull(@SalesTAdditional, '') = ''
            set @SalesTAdditional = @CardNumber      
	      set @OprID = 3
		  if @SalesType in (9, 10)
		    set @OprID = 4
	      exec @Result = MemberCardSubmit @UserID, @SalesTAdditional, @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID, @LocalAmount, 0, @VoidTxnNo, '', '', '', '', @ReturnAmount output, @ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output
          if @SalesTAdditional = @CardNumber
          begin
            set @MemberCardAmountAct = - abs(@LocalAmount)
          end  	    
        end 
        if @TenderType = 15     -- 卡积分消费 
        begin
          --if isnull(@SalesTAdditional, '') = ''
          --  set @SalesTAdditional = @CardNumber 
          --select @CardPointToAmountRate = isnull(CardPointToAmountRate, 0) from Card C left join CardGrade G on C.CardGradeID = G.CardGradeID where C.CardNumber = @SalesTAdditional
          --set @AddPoint = - (@LocalAmount * @CardPointToAmountRate)
		  if isnull(@SalesTAdditional, '') <> ''
		    set @AddPoint =  -abs(cast(@SalesTAdditional as int))   
	      else  
		    set @AddPoint = 0
	      set @OprID = 13
		  if @SalesType in (9, 10)
		    set @OprID = 4
	      exec @Result = MemberCardSubmit @UserID, @CardNumber, @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID, 0, @AddPoint, @VoidTxnNo, '', '', '', '', @ReturnAmount output, @ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output
          --if @SalesTAdditional = @CardNumber
          --begin
            set @MemberCardPointAct = - abs(@AddPoint)
          --end 	     
        end       
        if (@TenderType = 7)     -- Coupon支付   (@SalesTAdditional 是couponnumber,必须有值)	(增加类型 8)
        begin
	      if (@SalesType not in (9, 10))
	      begin
            exec @Result = MemberCardExchangeCoupon @UserID, @CardNumber, @SalesTAdditional, '', @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, '', '', @ReturnCouponTypeAmount output, @ReturnCouponTypePoint output, @ReturnCouponTypeDiscount OUTPUT, @ReturnBindPLU output
            set @ActCoupon = @SalesTAdditional
	      end
        end

        if @Result <> 0
        begin
		  if @ForceToComplete = 1
		  begin 
		    break
		  end
		  else
		  begin 
            rollback tran        
            return @Result
          end
        end  
      FETCH FROM CUR_SalesT INTO @TenderType, @LocalAmount, @SalesTAdditional, @TenderID, @TenderCode
    END  
      CLOSE CUR_SalesT   
      DEALLOCATE CUR_SalesT 
    END
  
    -- 计算积分. 
    if @IsCalcPoint = 1 and @IsMemberSales = 1
    begin
	  if @SalesType not in (9, 10)           
	    exec AutoPromoteForEarchSales @TxnNo
/*	 
     exec @CalcResult = CalcEarnPoints @CardNumber, @TotalAmount, @EarnPoints output, @PointRuleType   
     select top 1 @StoreID = StoreID from store     
  	   where StoreCode = @StoreCode and (BrandID = @BrandID or isnull(@BrandID, 0) = 0)
     if @EarnPoints > 0 
     begin
	   set @MemberCardPointAct = @EarnPoints
       insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
       values
        (25, @CardNumber, null, 0, @TxnNo, @CardTotalAmount + isnull(@MemberCardAmountAct,0), 0, @CardTotalAmount + isnull(@MemberCardAmountAct,0), @EarnPoints, @BusDate, @TxnDate, 
        null, null, null, null, '', @UserID, @StoreID, @RegisterCode, @ServerCode,
        @CardTotalPoints, @CardTotalPoints + @EarnPoints,  @ExpiryDate, @ExpiryDate, @CardStatus, @CardStatus)  
     end
*/
		-- 计算return,void时归还积分.
		--ver 1.0.0.12
		if @SalesType = 9 
		begin
			declare @VoidPoint int, @AfterVoid int, @VoidCouponNumber varchar(64), @VoidCouponStatus int 

			select @VoidPoint = Points from Card_Movement 
			where RefTxnNO = @VoidTxnNo and OprID = 27 and CardNumber = @CardNumber
			if ISNULL(@VoidPoint, 0) <> 0
			begin  
			set @AfterVoid = isnull(@CardTotalPoints,0) - isnull(@VoidPoint, 0)      
			if @ForceToComplete = 0 and @AfterVoid < 0
			begin
				rollback tran
				Return -58
			end
             
			insert into Card_Movement
				(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
					CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
					OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
			select 4, CardNumber, KeyID, null, @TxnNo, OpenBal, Amount, CloseBal, - Points, @BusDate, @Txndate,
					null, null, Additional, 'SVASalesSubmit Void', '', CreatedBy, StoreID, RegisterCode, ServerCode,
					@CardTotalPoints, @CardTotalPoints - Points, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus
				from Card_Movement where RefTxnNO = @VoidTxnNo and OprID = 27 and CardNumber = @CardNumber   -- 取出原单交易的 promotion 加积分的movement记录。 
			if @AfterVoid < 0 and @ForceToComplete = 1  
			begin
				insert into Card_Movement
				(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
					CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
  					OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
				select 90, CardNumber, KeyID, null, @TxnNo, OpenBal, Amount, CloseBal,  -(@AfterVoid), @BusDate, @Txndate,
					null, null, Additional, 'SVASalesSubmit Void', '', CreatedBy, StoreID, RegisterCode, ServerCode,
					@AfterVoid, 0, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus
				from Card_Movement where RefTxnNO = @VoidTxnNo and OprID = 27 and CardNumber = @CardNumber   -- 取出原单交易的 promotion 加积分的movement记录。 
			end 
			end
     
			insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
					BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode,ServerCode, StoreID, RegisterCode,
					OrgExpiryDate, OrgStatus, NewStatus) 
			select 35, A.CardNumber, A.CouponNumber, A.CouponTypeID, A.KeyID, null, @TxnNo, A.OpenBal, A.Amount, A.CloseBal, 
					@BusDate, @Txndate, 'SVASalesSubmit Void', '', A.CreatedBy, A.NewExpiryDate, A.ApprovalCode, A.ServerCode, A.StoreID, A.RegisterCode,
					A.OrgExpiryDate, 2, 5
				from Coupon_Movement A left join Coupon B on A.CouponNumber = B.CouponNumber
				where RefTxnNO = @VoidTxnNo and OprID = 27 and A.CardNumber = @CardNumber and A.CouponNumber =@VoidCouponNumber              
					and B.Status = 2  
			 
			insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
					BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode,ServerCode, StoreID, RegisterCode,
					OrgExpiryDate, OrgStatus, NewStatus) 
			select 41, A.CardNumber, A.CouponNumber, A.CouponTypeID, A.KeyID, null, @TxnNo, A.OpenBal, A.Amount, A.CloseBal, 
					@BusDate, @Txndate, 'SVASalesSubmit Void', '', A.CreatedBy, A.NewExpiryDate, A.ApprovalCode, A.ServerCode, A.StoreID, A.RegisterCode,
					A.OrgExpiryDate, 3, 2
				from Coupon_Movement A left join Coupon B on A.CouponNumber = B.CouponNumber
				where RefTxnNO = @VoidTxnNo and OprID in (34,54) and A.CardNumber = @CardNumber --and A.CouponNumber =@VoidCouponNumber              
					and B.Status = 3  			       
		end
    
    
		--ver 1.0.0.14  -- ver 1.0.0.15
		if @SalesType in (10, 11) 
		begin
			declare @reducePoint int, @Afterreduce int, @reduceamt money, @AmtSign int
			set @AmtSign = sign(@TotalAmount)
			set @reduceamt = abs(@TotalAmount)
			exec CalcEarnPoints @CardNumber, @reduceamt, @reducePoint output, 0, @StoreID 
      
			if @SalesType = 10
			set @AmtSign = -1
			if ISNULL(@reducePoint, 0) <> 0
			begin  
			set @reducePoint = isnull(@reducePoint, 0) * @AmtSign        
			set @Afterreduce = isnull(@CardTotalPoints,0) + @reducePoint    
			if @ForceToComplete = 0 and @Afterreduce < 0
			begin
				rollback tran
				Return -58
			end

			insert into Card_Movement
				(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
					CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
					OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
			values ( 4, @CardNumber, null, null, @TxnNo, @ReturnAmount, 0, @ReturnAmount, @reducePoint, @BusDate, @Txndate,
					null, null, '', 'SVASalesSubmit Return', '', @UserID, @StoreID, @RegisterCode, @ServerCode,
					@ReturnPoint, @ReturnPoint + @reduceamt, @ExpiryDate, @ExpiryDate, @CardStatus, @CardStatus)
	          
			if @Afterreduce < 0 and @ForceToComplete = 1  
			begin
				insert into Card_Movement
				(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
					CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
  					OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
				values( 90, @CardNumber, null, null, @TxnNo, @ReturnAmount, 0, @ReturnAmount, -(@Afterreduce), @BusDate, @Txndate,
					null, null, '', 'SVASalesSubmit Return', '', @UserID, @StoreID, @RegisterCode, @ServerCode,
					@Afterreduce, 0, @ExpiryDate, @ExpiryDate, @CardStatus, @CardStatus)
			end 
			end                    
		end     
		----------------

   end   


  -- (ver 1.0.0.6) 如果是销售Coupon的货品,那么就要自动分配coupon
    /*分配coupon=====================================================*/          
    DECLARE CUR_SalesD CURSOR fast_forward local FOR
    select B.CouponTypeID, D.TotalQty             
        from CouponTypeExchangeBinding B 
        left join (select ProdCode, sum(qty) as TotalQty from sales_D where TransNum = @TxnNo group by Prodcode) D on D.Prodcode = B.ProdCode               
        where B.BindingType = 1 and isnull(D.ProdCode, '') <> ''             
    OPEN CUR_SalesD  
    FETCH FROM CUR_SalesD INTO @BindCouponTypeID, @BindCouponCount  
    WHILE @@FETCH_STATUS=0  
    BEGIN
    set @CouponNumber = ''
    set @i = 1
	set @BindCouponCount = ISNULL(@BindCouponCount, 0)
	WHILE @i <= @BindCouponCount
	BEGIN
		exec GetCouponNumber @BindCouponTypeID, 1, @CouponNumber output  
        if isnull(@CouponNumber, '') <> ''
        begin
            exec GenApprovalCode @ApprovalCode output
            exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
            exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
            insert into Coupon_Movement
            (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                OrgExpirydate, OrgStatus, NewStatus)    
            select  
                32, @CardNumber, CouponNumber, CouponTypeID, '', 0, '', CouponAmount, 0, CouponAmount,
                @BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                @CouponExpiryDate, 1, 2
            from Coupon where CouponNumber = @CouponNumber
        end
        set @i = @i + 1			   
	END
            
    FETCH FROM CUR_SalesD INTO @BindCouponTypeID, @BindCouponCount  
    END  
    CLOSE CUR_SalesD   
    DEALLOCATE CUR_SalesD 
    /*=====================================================*/         
  end

  if @Result = 0  
  begin
	delete from MemberShoppingCart where MemberID = @MemberID and ProdCode in (select ProdCode from @SalesD)
  end
  -- END 1.0.1.17   
      
  -- ver 1.0.2.3
  if isnull(@CardNumber,0) <> ''
  begin
    Update Card set CumulativeConsumptionAmt = CumulativeConsumptionAmt + isnull(@TotalAmount,0) where CardNumber = @CardNumber
  end
  -------------------

  if @Result = 0 
    commit tran
  else 
    rollback tran 
    
/* 消息部分删除
  --使用了@CODFlag,不再使用这个判断: or (@TxnStatus = 3 and @HasCOD = 1)
  if (@IsShoppingCart = 0) and (isnull(@OldTxnStatus, -1) <> @TxnStatus) and 
     (@TxnStatus = 4 or @TxnStatus = 5  or (@PickupType = 2 and @CODFlag = 1) ) and @OldTxnStatus < 4
  begin
  -- 返回信息JSON   @SalesDMsgString
    -- 获得销售清单的JSON字符串(子项)
    set @SalesDMsgString = ''
    DECLARE CUR_MemberCardSubmit_SalesD CURSOR fast_forward FOR
       select D.ProdDesc, D.NetPrice, D.Qty, D.NetAmount, S.ProductSizeName1, C.ColorName1 from @SalesD D Left join Product P on D.ProdCode = P.ProdCode
         Left join Product_Size S on S.ProductSizeID = P.ProductSizeID
         Left join Color C on C.ColorID = P.ColorID
    OPEN CUR_MemberCardSubmit_SalesD
    FETCH FROM CUR_MemberCardSubmit_SalesD INTO @JsonProdDesc, @JsonNetPrice, @JsonQty, @JsonNetAmount, @Memo1, @Memo2
    WHILE @@FETCH_STATUS=0
    BEGIN
      set @JsonProdDesc = REPLACE(@JsonProdDesc, ' ', '\ ')
      set @JsonProdDesc = REPLACE(@JsonProdDesc, ',', '\,')
 	  if @SalesDMsgString <> ''
	    set @SalesDMsgString = @SalesDMsgString + '|' 
      set @SalesDMsgString = @SalesDMsgString + 'DESCRIPTION=' + @JsonProdDesc
     	  + ';QTY=' + cast(isnull(@JsonQty,0) as varchar) + ';PRICE=' + cast(isnull(@JsonNetPrice,0) as varchar)
     	  + ';Amount=' + cast(isnull(@JsonNetAmount,0) as varchar)	
     	  + ';Memo1=' +  isnull(@Memo1,'') + ';Memo2=' + isnull(@Memo2,'')
      FETCH FROM CUR_MemberCardSubmit_SalesD INTO @JsonProdDesc, @JsonNetPrice, @JsonQty, @JsonNetAmount, @Memo1, @Memo2  
    END
    CLOSE CUR_MemberCardSubmit_SalesD 
    DEALLOCATE CUR_MemberCardSubmit_SalesD
     
    exec GenMessageJSONStr 'ItemList', @SalesDMsgString, @SalesDMsgJSON output
    declare @StrLen int
    if @SalesDMsgJSON <> '' 
    begin
      set @StrLen = Len(@SalesDMsgJSON)
      if @StrLen > 2
        set @SalesDMsgJSON = substring(@SalesDMsgJSON, 2, @StrLen - 2)
    end else
      set @SalesDMsgJSON = ' ItemList :[]'    
    --
    
    -- 获得支付清单的JSON字符串(子项)
    set @SalesTMsgString = ''
    DECLARE CUR_MemberCardSubmit_SalesT CURSOR fast_forward FOR
      select TenderID, TenderType, TenderCode, TenderName, TenderAmount, LocalAmount, ExchangeRate, SalesTAdditional from @SalesT
    OPEN CUR_MemberCardSubmit_SalesT
    FETCH FROM CUR_MemberCardSubmit_SalesT INTO @JsonTenderID, @JsonTenderType, @JsonTenderCode, @JsonTenderName, @JsonTenderAmount, 
       @JsonLocalAmount, @JsonExchangeRate, @JsonSalesTAdditional
    WHILE @@FETCH_STATUS=0
    BEGIN
 	  if @SalesTMsgString <> ''
	    set @SalesTMsgString = @SalesTMsgString + '|' 
      set @SalesTMsgString = @SalesTMsgString + 'TenderID=' + cast(isnull(@JsonTenderID,0) as varchar) 
          + ';TenderCode=' + isnull(@JsonTenderCode,'') + ';TenderType=' + cast(isnull(@JsonTenderType,0) as varchar)
          + ';TenderName=' + isnull(@JsonTenderName,'')
     	  + ';TenderAmount=' + cast(isnull(@JsonTenderAmount,0) as varchar) + ';LocalAmount=' + cast(isnull(@JsonLocalAmount,0) as varchar)
     	  + ';ExchangeRate=' + cast(isnull(@JsonExchangeRate,0) as varchar)	
     	  + ';Additional=' +  isnull(@JsonSalesTAdditional,'') 
      FETCH FROM CUR_MemberCardSubmit_SalesT INTO @JsonTenderID, @JsonTenderType, @JsonTenderCode, @JsonTenderName, @JsonTenderAmount, 
         @JsonLocalAmount, @JsonExchangeRate, @JsonSalesTAdditional
    END
    CLOSE CUR_MemberCardSubmit_SalesT 
    DEALLOCATE CUR_MemberCardSubmit_SalesT
     
    exec GenMessageJSONStr 'TenderList', @SalesTMsgString, @SalesTMsgJSON output
    declare @StrLenT int
    if @SalesTMsgJSON <> '' 
    begin
      set @StrLenT = Len(@SalesTMsgJSON)
      if @StrLenT > 2
        set @SalesTMsgJSON = substring(@SalesTMsgJSON, 2, @StrLenT - 2)
    end else
      set @SalesTMsgJSON = ' TenderList :[]'    
    --
 
  set @InputString = ''
  set @MemberCardAmountBalance = isnull(@MemberCardAmountBalance, 0)
  set @MemberCardAmountAct = isnull(@MemberCardAmountAct, 0)
  set @MemberCardPointAct = isnull(@MemberCardPointAct, 0)
  set @MemberCardPointBalance = isnull(@MemberCardPointBalance, 0)
  
  DECLARE CUR_MemberCardSubmit CURSOR fast_forward FOR
    select MessageServiceTypeID, AccountNumber from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1  
  OPEN CUR_MemberCardSubmit
  FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber
  WHILE @@FETCH_STATUS=0
  BEGIN
	if isnull(@AccountNumber, '') <> '' 
	begin
	  if @InputString <> ''
	    set @InputString = @InputString + '|' 
      set @InputString = @InputString + 'SALESTYPE=' + cast(isnull(@SalesType,0) as varchar) + ';MPASSWORD=' + ';MEMBERID=' + cast(@MemberID as varchar)
     	  + ';MSGACCOUNT=' + @AccountNumber + ';MSGACCOUNTTYPE=' + cast(isnull(@MessageServiceTypeID,0) as varchar)	
          + ';TXNNO=' + isnull(@TxnNo,'') + ';EXPDATE=' + convert(varchar(10), isnull(@ExpiryDate,getdate()), 120)	
          + ';AMOUNT=' + cast(@MemberCardAmountAct as varchar)	+ ';AMOUNTBALANCE=' + cast((@MemberCardAmountBalance + @MemberCardAmountAct) as varchar)
     	  + ';POINT=' + cast(@MemberCardPointAct as varchar) + ';POINTBALANCE=' + cast((@MemberCardPointBalance + @MemberCardPointAct) as varchar)
     	  + ';COUPONS=' + isnull(@ActCoupon,'')     	  
     	  + ';DeliveryDate=' +  convert(varchar(10), isnull(@DeliveryDate,getdate()), 120)
     	  + ';DeliveryaAddr=' + @DeliveryaAddr
     	  + ';Contact=' + @Contact
     	  + ';ContactPhone=' + @ContactPhone
     	  + ';IP='
     	  + ';TotalAmount=' +  cast(isnull(@TotalAmount, 0) as varchar)
     	  + ';BrandID=' + cast(@BrandID as varchar)
     	  + ';BrandCode=' + @BrandCode
     	  + ';BrandName=' + @BrandName     
     	  + ';JSON=' + @SalesDMsgJSON
     	  + ';JSON=' + @SalesTMsgJSON	  
	end
    FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber   
  END
  CLOSE CUR_MemberCardSubmit 
  DEALLOCATE CUR_MemberCardSubmit 
  
    exec GenMessageJSONStr 'SVASalesSubmit', @InputString, @ReturnMessageStr  output
  end else
    set @ReturnMessageStr = '' 
*/
  
  set @ReturnMessageStr = '' 

  return @Result
end

GO
