USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCoupons]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberCoupons]
  @MemberID				int,               -- 頗埜ID
  @CardNumber			varchar(512),      -- 沓迡CardNumberㄛ綺謹MemberID	  
  @CouponKinds			int,			   -- coupon笱濬: null 麼氪0 桶尨窒, 1: 籵coupon,  2:荂豪)
  @CouponStatus			int,			   -- 党蜊ㄩ森統杅眻諉釬峈coupon腔statusㄛ-1麼氪null桶尨窒﹝//(null 麼氪0 桶尨窒ㄛ1ㄩ帤妏蚚(帤徹) 2ㄩ眒妏蚚  3:眒徹(帤妏蚚))
  @ConditionStr			nvarchar(1000),
  @PageCurrent			int=1,			   -- 菴撓珜暮翹ㄛ蘇1ㄛ苤衾脹衾0奀珩峈1
  @PageSize				int=0,			   -- 藩珜暮翹杅ㄛ 峈0奀ㄛ祥煦珜ㄛ蘇0
  @PageCount			int=0 output,	   -- 殿隙軞珜杅﹝
  @RecordCount			int=0 output,	   -- 殿隙軞暮翹杅﹝
  @LanguageAbbr			varchar(20)=''	   -- 1:CardTypeName1,2ㄩCardTypeName2. 3ㄩCardTypeName3.  坻ㄩCardTypeName1    
AS
/****************************************************************************
**  Name : GetMemberCoupons  鳳腕頗埜腔Coupon杅擂
**  Version: 1.0.0.10
**  Description : 怀頗埜翋瑩ㄛ殿隙扽衾頗埜腔Coupon
**  Parameter :

  declare @CardNumber nvarchar(30), @count int, @recordcount int, @a int  
  set @CardNumber = '01000454' -- '000301138'--'1111279'--'000100001' 
  exec @a = GetMemberCoupons 0, @CardNumber, 0, -1, '', 1, 0, @count output, @recordcount output, ''
  print @a  
  print @count
  print @recordcount

select * from coupon where cardnumber <> ''  
  select * from coupon  
  
**
**  Created by: Gavin @2012-02-20
**  Modify by: Gavin @2013-01-06 (ver 1.0.0.1) 換腔@CouponStatusㄛ眻諉釬峈coupon status腔袨怓沭璃, -1麼氪null桶尨窒﹝鍚俋偌桽Coupon腔updatedOn齬唗ㄛ森党蜊剒猁載蜊弝芞ViewMemberCoupons
**  Modified by: Gavin @2013-06-21 (Ver 1.0.0.2) 妏蚚1.0.0.6唳掛腔SelectDataInBatchs	,怀腔@ConditionStr眻諉換跤SelectDataInBatchs 
**  Modified by: Gavin @2014-07-11 (Ver 1.0.0.3) 崝樓殿隙hardcode杅擂 URL
                                                 崝樓殿隙hardcode杅擂ㄩ 5 as ExchangeQty, ''CPT008'' as GiftCouponType 
                                                 ㄗfor 711ㄘ蛁ㄩRayㄛ King猁崝樓﹝
**  Modified by: Gavin @2014-08-12 (Ver 1.0.0.4) ㄗfor 711ㄘ跦擂CardGrade笢腔NumberOfCouponDisplay 懂扢离@PageSize     
**  Modified by: Gavin @2014-08-28 (Ver 1.0.0.5) ㄗfor 711ㄘhardcode 腔URL 囀載蜊   
**  Modified by: Gavin @2014-12-15 (Ver 1.0.0.6) ㄗfor 711ㄘ蚥趙脤戙厒僅, 秏弝芞ViewMemberCoupons   
**  Modified by: Gavin @2015-02-02 (Ver 1.0.0.7)  崝樓殿隙趼僇 MemberClauseDesc, 
**  Modified by: Gavin @2015-03-18 (Ver 1.0.0.8)  党淏CouponTypeNatureID沭璃腔Bug      
**  Modified by: Gavin @2016-11-04 (Ver 1.0.0.9)  茼覃蚚源猁ㄛMemberClauseDesc 殿隙趼僇濬倰蜊峈 Text﹝  
**  Modified by: Gavin @2017-06-22 (Ver 1.0.0.10) (for Newbuying DB) Brand党蜊趼僇﹝                        
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @StateofCoupon int --, @ConditionStr nvarchar(400)
  declare @OrderCondition nvarchar(max), @NumberOfCouponDisplay int
  declare @URL varchar(500)
  declare @NatureTypeStr varchar(100)  
  --set @URL = 'http://7fansdemo.value-exch.com:81/Test_7Fans/html/coupons/%#.html'
  set @URL = 'http://www.7fans.com.hk/SVA/html/Coupon/%#.html'
  
  set @CardNumber = isnull(@CardNumber, '') 
  set @MemberID = isnull(@MemberID, 0)
  if (@MemberID = 0) and (@CardNumber = '')  
    return -7  
  set @CouponKinds = ISNULL(@CouponKinds, 0)
  set @StateofCoupon = @CouponStatus 

  -- Ver 1.0.0.4
  if @CardNumber = ''
    select Top 1 @CardNumber = CardNumber from Card where MemberID = @MemberID
  select @NumberOfCouponDisplay = G.NumberOfCouponDisplay from Card C 
    left join CardGrade G on C.CardGradeID = G.CardGradeID where C.CardNumber = @CardNumber
  if @PageSize = 0
    set @PageSize = isnull(@NumberOfCouponDisplay, 0)

  if @CouponKinds = 1
    set @NatureTypeStr = 'where Y.CouponTypeNatureID <> 4'
  else if @CouponKinds = 2  
    set @NatureTypeStr = 'where Y.CouponTypeNatureID = 4'
  else set @NatureTypeStr = ''
    
  if isnull(@StateofCoupon, -1) <> -1
  begin
    if @ConditionStr<>'' set @ConditionStr=@ConditionStr+' and '
    set @ConditionStr = @ConditionStr + ' CouponStatus = ' + cast(@StateofCoupon as varchar)
  end  

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  
  if @Language = 1
    set @URL = REPLACE(@URL, '#', '_en-us')  
  else if @Language = 2
    set @URL = REPLACE(@URL, '#', '_zh-cns')  
  else if @Language = 3
    set @URL = REPLACE(@URL, '#', '_zh-hk')  
  else if @Language = 3
    set @URL = REPLACE(@URL, '#', '_en-us')  
    
  set @SQLStr =  ' select case ' + CAST(@Language as varchar) + ' when 2 then CouponTypeName2 when 3 then CouponTypeName3 else CouponTypeName1 end as CouponTypeName, '
  + ' *, replace(''' + @URL + ''', ''%'', CouponTypeCode) as URL, 5 as ExchangeQty, ''CPT008'' as GiftCouponType '
  + ' , case ' + CAST(@Language as varchar) + ' when 2 then cast(MemberClauseDesc2 as text) when 3 then cast(MemberClauseDesc3 as text) else cast(MemberClauseDesc1 as text) end as MemberClauseDesc '
  + ' from '      
  + '( select (RTrim(C.CouponTypeID) + CouponNumber) as SeqNo, CouponNumber, C.CouponTypeID, Y.CouponTypeCode, CouponExpiryDate, '
  + ' D.MemberID, C.Status as CouponStatus, Y.CouponTypeAmount, Y.CouponTypePoint, Y.CouponTypeDiscount, '''' as CouponTypeBindPLU, '
	+ ' Y.CouponTypeName1, Y.CouponTypeName2, Y.CouponTypeName3, '
	+ ' M.MemberEmail, M.MemberEngFamilyName, M.MemberEngGivenName, M.MemberChiFamilyName, M.MemberChiGivenName,'
	+ ' M.MemberSex, M.MemberDateOfBirth, M.MemberDayofBirth, M.MemberMonthofBirth, M.MemberYearofBirth, M.HomeTelNum, '
	+ ' Y.BrandID, B.StoreBrandName1 as BrandName, C.BatchCouponID, C.CardNumber, Y.CouponTypeNotes, Y.CouponTypeLayoutFile, C.CouponAmount, Y.CouponTypePicFile, C.UpdatedOn,'
	+ ' Y.CampaignID, G.CampaignCode, G.CampaignType, Y.IsPhysicalCoupon, Y.IsNeedVerifyNum, Y.VerifyNumLifecycle, Y.CouponNumMask, Y.CouponTypeNatureID, Y.CouponTypeTransfer,'
	+ ' Y.AllowShareCoupon, C.CouponRedeemDate, C.IsRead, C.IsFavorite, '
	+ ' Y.AllowDeleteCoupon, Y.QRCodePrefix, Y.MaxDownloadCoupons, Y.CouponReturnValue, Y.TrainingMode, Y.UnlimitedUsage'
	+ ' , A.ClauseName, A.ClauseName2, A.ClauseName3, cast(A.MemberClauseDesc1 as text) as MemberClauseDesc1, cast(A.MemberClauseDesc2 as text) as MemberClauseDesc2, cast(A.MemberClauseDesc3 as text) as MemberClauseDesc3'	
  + '   from Coupon C '
  + '     left join Card D on C.CardNumber = D.CardNumber'
  + '     left join Member M on D.MemberID = M.MemberID'
  + '     left join CouponType Y on C.CouponTypeID = Y.CouponTypeID '
  + '     left join CouponTypeNature N on Y.CouponTypeNatureID = N.CouponTypeNatureID '
  + '     left join Brand B on Y.BrandID = B.StoreBrandID'
  + '     left join (select CouponTypeID, max(CampaignID) as CampaignID from CampaignCouponType group by CouponTypeID) P on C.CouponTypeID = P.CouponTypeID  '
  + '     left join Campaign G on P.CampaignID = G.CampaignID  '
  + '     left join (select * from MemberClause where ClauseTypeCode = ''CouponType'') A on A.ClauseSubCode = cast(C.CouponTypeID as varchar) '
  + @NatureTypeStr
  + ' ) A '
  + ' where (CardNumber = ''' + @CardNumber + ''' or ''' + @CardNumber + ''' = '''')'
  + '      and (MemberID = ' + cast(@MemberID as varchar) + ' or ' + cast(@MemberID as varchar) + ' = 0)'
                					    
  set @OrderCondition='order by UpdatedOn desc'

  exec SelectDataInBatchs @SQLStr, 'SeqNo', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr    
  
  return 0
end

GO
