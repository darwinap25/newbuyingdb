USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ImportCardUID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[ImportCardUID]
  @ImportCardNumber varchar(64),   -- coupon导入单的单号
  @NeedActive int,                   -- 是否需要立即激活（0：不需要，1：需要）
  @NeedNewBatch int,                 -- 是否需要建立新批次。（0）
  @ApproveStatus char(1), 
  @CreatedBy int
AS
/****************************************************************************
**  Name : ImportCardUID
**  Version: 1.0.0.5
**  Description : Import Card UID 的 触发器中调用，用于执行写入UID和 Card表
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = ImportCardUID 0, ''
  print @a  
**  Created by: Gavin @2012-08-28
**  Modify by: Gavin @2012-11-13 (ver 1.0.0.4) 修改CalcCheckDigit_EAN13 为CalcCheckDigit。	
**  Modify by: Gavin @2014-10-16 (ver 1.0.0.5) 创建时stockstatus 设置为2 			
**
****************************************************************************/
begin
  declare @CardGradeID int, @CardCount int, @OprID int, @BatchCardID int
  declare @KeyID int, @CardUID varchar(512), @ExpiryDate datetime, @CardExpiryDate datetime, @CardTypeInitPoints int
  declare @OldApproveStatus char(1), @CardNumber varchar(512), @CardValidityDuration int, @CardTypeInitAmount money,
		@CardValidityUnit int, @IsConsecutiveUID int, @UIDToCardNumber int, @UIDCheckDigit int, @CardTypeID int
  declare @A int, @ReturnBatchID varchar(30), @ReturnStartNumber nvarchar(30), @ReturnEndNumber nvarchar(30), @IssuedDate datetime
  declare @ApprovalCode char(6), @CardBatchCode varchar(512), @ImportBatchCode varchar(512), @Denomination money
  declare @IsImportCardNumber int, @NewCardNumber varchar(512), @i int, @TranCount int, @NewCardStatus int
  declare @BusDate date --Business Date
  declare @TxnDate datetime --Transaction Date
  
  --设置Busdate
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  if @BusDate is null
    set @BusDate=convert(varchar(10), getdate(), 120)
  set @TxnDate=getdate()
  set @IssuedDate =convert(varchar(10), getdate(), 120)    
  
  --设置是否激活. (根据业务逻辑, 目前一律不激活,都是dormant)
  set @NewCardStatus = 0

  --获得Approvalcode  
  exec GenApprovalCode @ApprovalCode output 
  
  DECLARE CUR_Ord_ImportCardUID_D_Group CURSOR fast_forward local FOR
    SELECT CardGradeID, count(KeyID) as CardCount, BatchCode FROM Ord_ImportCardUID_D where ImportCardNumber = @ImportCardNumber
       Group By CardGradeID, BatchCode 
     order by CardGradeID, BatchCode 
  OPEN CUR_Ord_ImportCardUID_D_Group
  FETCH FROM CUR_Ord_ImportCardUID_D_Group INTO @CardGradeID, @CardCount, @ImportBatchCode
  WHILE @@FETCH_STATUS=0
  BEGIN
    set @BatchCardID = 0
    select @CardTypeID = G.CardTypeID, @CardValidityDuration = G.CardValidityDuration, @CardValidityUnit = G.CardValidityUnit, @CardTypeInitAmount = G.CardTypeInitAmount,
		@UIDToCardNumber = G.UIDToCardNumber, @UIDCheckDigit = G.UIDCheckDigit, @CardTypeInitPoints = G.CardTypeInitPoints
	from CardGrade G left join CardType T on G.CardTypeID = T.CardTypeID
	where CardGradeID = @CardGradeID
    --设置有效期 
    if @CardValidityUnit = 1
      set @CardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, isnull(@CardValidityDuration, 0), @IssuedDate))))
    else if @CardValidityUnit = 2
      set @CardExpiryDate = convert(datetime, floor(convert(float, DateAdd(mm, isnull(@CardValidityDuration, 0), @IssuedDate))))
    else if @CardValidityUnit = 3
      set @CardExpiryDate = convert(datetime, floor(convert(float, DateAdd(ww, isnull(@CardValidityDuration, 0), @IssuedDate))))
    else if @CardValidityUnit = 4
      set @CardExpiryDate = convert(datetime, floor(convert(float, DateAdd(dd, isnull(@CardValidityDuration, 0), @IssuedDate))))  
    else if @CardValidityUnit = 5    -- 不变更。
      set @CardExpiryDate = 0
    else if isnull(@CardValidityUnit, 0) = 0  
      set @CardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, 100, @IssuedDate))))  
    
    --产生Coupon记录    
    if (@UIDToCardNumber = 0)
    begin
      if isnull(@NeedNewBatch, 0) = 1        -- 要求新建一批Coupon来绑定
      begin
        exec @A = BatchGenerateNumber @CreatedBy, 0, null, @CardGradeID,0, @CardCount, @IssuedDate, @CardTypeInitAmount, @CardTypeInitPoints, 0, '', 
		   @CardExpiryDate, @ImportCardNumber, @ApprovalCode, @CreatedBy, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, @ImportBatchCode, 0, 2
        if @A = 0
          set @BatchCardID = @ReturnBatchID               
      end    
      -- 绑定Card
      insert into CardUIDMap(CardUID, ImportCardNumber, BatchCardID, CardGradeID, CardNumber, Status, CreatedOn, CreatedBy)          
      select U.CardUID, U.ImportCardNumber, C.BatchCardID, C.CardGradeID, C.CardNumber, 1, @TxnDate, @CreatedBy
        from (SELECT ROW_NUMBER() over (order by KeyID) as SeqNo, ImportCardNumber, KeyID, CardUID, ExpiryDate, BatchCode, Denomination FROM Ord_ImportCardUID_D 
                where ImportCardNumber = @ImportCardNumber and CardGradeID = @CardGradeID and BatchCode = @ImportBatchCode) U 
            left join (select ROW_NUMBER() over (order by CardNumber) as SeqNo, BatchCardID, CardGradeID,CardNumber from card where CardGradeID = @CardGradeID and Status = 0 and (BatchCardID = isnull(@BatchCardID,0) or @NeedNewBatch = 0)) C 
            on U.SeqNo = C.SeqNo
       where U.KeyID is not null and C.CardNumber is not null
      --如果有调整面值，有效期，Card状态的，则插入card_movement               
      insert into Card_Movement
             (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
              CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      select 43, M.CardNumber, null, null, D.ImportCardNumber, 0, isnull(D.Denomination, C.TotalAmount), isnull(D.Denomination, C.TotalAmount), C.TotalPoints,
	     @BusDate, @TxnDate, null, null, null, '', '', '', @CreatedBy, C.CardExpiryDate, C.CardExpiryDate, 0, @NewCardStatus
             from Ord_ImportCardUID_D D left join CardUIDMap M on D.CardUID = M.CardUID
               left join Card C on M.CardNumber = C.CardNumber
          where D.ImportCardNumber = @ImportCardNumber and D.CardGradeID = @CardGradeID and BatchCode = @ImportBatchCode
    end else            -- 根据导入的UID来产生CouponNumber
    begin
       if isnull(@ImportBatchCode, '') = ''
          exec GetRefNoString 'BTHCAD', @CardBatchCode output   
       else set @CardBatchCode = @ImportBatchCode
        
	   insert into BatchCard
		  (BatchCardCode, SeqFrom, SeqTo, Qty, CardGradeID, CreatedOn, UpdatedOn, CreatedBy, UpdatedBy)
       values (@CardBatchCode, 0, 0, 0, @CardGradeID, @TxnDate, @TxnDate, @CreatedBy, @CreatedBy)  
     
        set @BatchCardID = SCOPE_IDENTITY()

        insert into Card
           (CardNumber, CardTypeID, CardIssueDate, CardExpiryDate, MemberID, Status, UsedCount, CardGradeID,
            BatchCardID, TotalPoints, TotalAmount, CardPassword, StockStatus)     
        select
              case when @UIDToCardNumber = 1 then CardUID
                when @UIDToCardNumber = 2 then  substring(CardUID, 1, len(RTrim(LTrim(CardUID))) - 1)
                when @UIDToCardNumber = 3 then  dbo.CalcCheckDigit(CardUID, isnull(M.CheckDigitModeCode, 'EAN13'))
              end,  
               @CardTypeID, @IssuedDate, isnull(ExpiryDate,@CardExpiryDate), null, @NewCardStatus, 0, @CardGradeID, 
               @BatchCardID, @CardTypeInitPoints, @CardTypeInitAmount, null, 2
             from Ord_ImportCardUID_D D left join CardGrade G on D.CardGradeID = G.CardGradeID
               left join CheckDigitMode M on G.CheckDigitModeID = M.CheckDigitModeID
             where ImportCardNumber = @ImportCardNumber and D.CardGradeID = @CardGradeID and BatchCode = @ImportBatchCode
            
        --插入Card_Movement,OprID=0新增 
        insert into Card_Movement
          (CardNumber, OprID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
           CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy)
        select
              case when @UIDToCardNumber = 1 then CardUID
                when @UIDToCardNumber = 2 then  substring(CardUID, 1, len(RTrim(LTrim(CardUID))) - 1)
                when @UIDToCardNumber = 3 then  dbo.CalcCheckDigit(CardUID, isnull(M.CheckDigitModeCode, 'EAN13'))
              end,        
              0, null, null, @ImportCardNumber, 0, isnull(Denomination, G.CardTypeInitAmount), isnull(Denomination, G.CardTypeInitAmount), G.CardTypeInitPoints,
	          @BusDate, @TxnDate, null, null, null, '', '', '', @CreatedBy
          from Ord_ImportCardUID_D D left join CardGrade G on D.CardGradeID = G.CardGradeID
            left join CheckDigitMode M on G.CheckDigitModeID = M.CheckDigitModeID
        where ImportCardNumber = @ImportCardNumber and D.CardGradeID = @CardGradeID and BatchCode = @ImportBatchCode	  
            
        insert into CardUIDMap(CardNumber, CardUID, ImportCardNumber, BatchCardID, CardGradeID, Status, CreatedOn, CreatedBy)
        select 
              case when @UIDToCardNumber = 1 then CardUID
                when @UIDToCardNumber = 2 then  substring(CardUID, 1, len(RTrim(LTrim(CardUID))) - 1)
                when @UIDToCardNumber = 3 then  dbo.CalcCheckDigit(CardUID, isnull(M.CheckDigitModeCode, 'EAN13'))
              end,        
             CardUID, @ImportCardNumber, @BatchCardID, D.CardGradeID, 1, @TxnDate, @CreatedBy
           from Ord_ImportCardUID_D D left join CardGrade G on D.CardGradeID = G.CardGradeID
             left join CheckDigitMode M  on G.CheckDigitModeID = M.CheckDigitModeID
          where ImportCardNumber = @ImportCardNumber and D.CardGradeID = @CardGradeID and BatchCode = @ImportBatchCode	          
    end    
    FETCH FROM CUR_Ord_ImportCardUID_D_Group INTO @CardGradeID, @CardCount, @ImportBatchCode
  END
  CLOSE CUR_Ord_ImportCardUID_D_Group 
  DEALLOCATE CUR_Ord_ImportCardUID_D_Group              
 
  update Ord_ImportCardUID_H set ApprovalCode = @ApprovalCode where ImportCardNumber = @ImportCardNumber 
end

GO
