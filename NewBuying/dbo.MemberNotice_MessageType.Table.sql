USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberNotice_MessageType]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberNotice_MessageType](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[NoticeNumber] [varchar](64) NOT NULL,
	[MessageTemplateID] [int] NOT NULL,
	[Status] [int] NULL,
	[FrequencyUnit] [int] NULL,
	[FrequencyValue] [int] NULL,
	[SendTime] [datetime] NULL,
	[DesignSendTimes] [int] NULL,
	[SendCount] [int] NULL,
 CONSTRAINT [PK_MEMBERNOTICE_MESSAGETYPE] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberNotice_MessageType] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[MemberNotice_MessageType] ADD  DEFAULT ((1)) FOR [FrequencyValue]
GO
ALTER TABLE [dbo].[MemberNotice_MessageType] ADD  DEFAULT ((0)) FOR [DesignSendTimes]
GO
ALTER TABLE [dbo].[MemberNotice_MessageType] ADD  DEFAULT ((0)) FOR [SendCount]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'NoticeNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'MessageTemplateID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否生效。0：无效。1：生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送频率单位：0：仅一次。 1：月。 2：星期。3：天。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'FrequencyUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送频率数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'FrequencyValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定的起始发送时间。 （比如设置为 10月1号，早上9:00 开始发送。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'SendTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设计发送次数。0：表示不计发送次数。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'DesignSendTimes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已经发送的次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType', @level2type=N'COLUMN',@level2name=N'SendCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员消息 发送类型表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberNotice_MessageType'
GO
