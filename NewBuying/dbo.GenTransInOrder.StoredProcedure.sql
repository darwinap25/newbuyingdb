USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenTransInOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenTransInOrder]
  @OrderNumber VARCHAR(64)     -- 订单号码
AS
/****************************************************************************
**  Name : GenTransInOrder
**  Version: 1.0.0.1
**  Description : 根据店铺间转出单产生转入单
**  Parameter :
select * from Ord_ShipmentOrder_H
**  Created by: Gavin @2015-04-14
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
**
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @StoreCode varchar(64)
  EXEC GetRefNoString 'TRANIN', @NewNumber OUTPUT

  select @StoreCode = StoreCode FROM Ord_TransOutOrder_H A left join Buy_STORE B on A.StoreID = B.StoreID
    WHERE TransOutOrderNumber = @OrderNumber
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC
  
    INSERT INTO Ord_TransInOrder_H (TransInOrderNumber,ReferenceNo,FromStoreID,FromContactName,
        FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
        StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
        ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    SELECT @NewNumber, TransOutOrderNumber, FromStoreID, FromContactName,
        FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
        StoreMobile,StoreAddress,Remark,@BusDate,NULL,'','P',
        NULL,ApproveBy,GETDATE(),CreatedBy,GETDATE(),UpdatedBy
    FROM Ord_TransOutOrder_H 
    WHERE TransOutOrderNumber = @OrderNumber
    
    INSERT INTO Ord_TransInOrder_D (TransInOrderNumber, ProdCode, TransInQty, ReasonID, Remark)
    SELECT @NewNumber, ProdCode, TransOutQty, ReasonID, D.Remark 
    FROM Ord_TransOutOrder_D D left join Ord_TransOutOrder_H H on H.TransOutOrderNumber = D.TransOutOrderNumber
    WHERE H.TransOutOrderNumber = @OrderNumber

  RETURN 0   
END

GO
