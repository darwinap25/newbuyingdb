USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGroupDetail]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardGroupDetail](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardGroupID] [int] NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[IsMaster] [int] NULL,
	[Status] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARDGROUPDETAIL] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CardGroupDetail] ADD  DEFAULT ((0)) FOR [IsMaster]
GO
ALTER TABLE [dbo].[CardGroupDetail] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[CardGroupDetail] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CardGroupDetail] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroupDetail', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroupDetail', @level2type=N'COLUMN',@level2name=N'CardGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成员卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroupDetail', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否群管理者。1：是的。 0：不是。管理者只能有一个。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroupDetail', @level2type=N'COLUMN',@level2name=N'IsMaster'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：失效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroupDetail', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员组成员表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroupDetail'
GO
