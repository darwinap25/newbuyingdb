USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UnReserverStockByPicking]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[UnReserverStockByPicking]
  @UserID				         INT,	            -- 操作员ID
  @SalesPickOrderNumber          VARCHAR(64)        -- 单号码
AS
/****************************************************************************
**  Name : UnReserverStockByPicking 
**  Version: 1.0.0.0
**  Description : 暂不实现,逻辑还有问题.

**  Created by: Gavin @2016-12-14  
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @TxnDate DATETIME, @StoreCode varchar(64)

  IF NOT EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE ReferenceNo = @SalesPickOrderNumber AND ISNULL(Status, 0) <> 0)  -- 销售的货品都没有出库. 有出库了就不能这么做.
  BEGIN
	  EXEC GetRefNoString 'SSO', @NewNumber OUTPUT

  END
  RETURN 0
END

GO
