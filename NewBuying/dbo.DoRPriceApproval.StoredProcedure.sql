USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoRPriceApproval]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DoRPriceApproval]
  @RPriceCode varchar(64),
  @UserID int 
AS
/****************************************************************************
**  Name : DoRPriceApproval
**  Version : 1.0.0.0
**  Description : 零售价批核过程
** 
declare @REFNO NVARCHAR(50)
exec GetRefNoString 'BTHCAD', @REFNO output
print @REFNO
**  
**  Create by Gavin @2013-12-11
****************************************************************************/
begin
  insert into buy_RPrice_M(RPriceCode, ProdCode, RPriceTypeCode, RefPrice, Price, PromotionPrice, MemberPrice, 
      StoreCode, StoreGroupCode, StartDate, EndDate, Status, ApproveOn, ApproveBy, UpdatedOn, UpdatedBy)
  select D.RPriceCode, D.ProdCode, D.RPriceTypeCode, D.RefPrice, D.Price, D.PromotionPrice, D.MemberPrice, 
      H.StoreCode, H.StoreGroupCode, H.StartDate, H.EndDate, 1, getdate(), @UserID, Getdate(), @UserID
    from buy_RPrice_D D left join buy_RPrice_H H on H.RPriceCode = D.RPriceCode      
end

GO
