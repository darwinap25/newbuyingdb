USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponOrderForm_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponOrderForm_H](
	[CouponOrderFormNumber] [varchar](64) NOT NULL,
	[OrderType] [int] NULL DEFAULT ((0)),
	[BrandID] [int] NULL,
	[FromStoreID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[CustomerType] [int] NULL DEFAULT ((2)),
	[CustomerID] [int] NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[FromAddress] [nvarchar](512) NULL,
	[StoreContactName] [nvarchar](512) NULL,
	[StoreContactPhone] [nvarchar](512) NULL,
	[StoreContactEmail] [nvarchar](512) NULL,
	[StoreMobile] [nvarchar](512) NULL,
	[FromContactName] [nvarchar](512) NULL,
	[FromContactNumber] [nvarchar](512) NULL,
	[FromEmail] [nvarchar](512) NULL,
	[FromMobile] [nvarchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[Remark1] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_COUPONORDERFORM_H] PRIMARY KEY CLUSTERED 
(
	[CouponOrderFormNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CouponOrderForm_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CouponOrderForm_H] ON [dbo].[Ord_CouponOrderForm_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CouponOrderForm_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2014-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @CouponOrderFormNumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @CouponOrderFormNumber = CouponOrderFormNumber FROM INSERTED  
  exec GenUserMessage @CreatedBy, 8, 0, @CouponOrderFormNumber  
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponOrderForm_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponOrderForm_H] ON [dbo].[Ord_CouponOrderForm_H]
FOR UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponOrderForm_H
* Version: 1.0.1.3
* Description : Coupon订单表的批核触发器
*  
** Create By Gavin @2012-09-21
** Modify by Gavin @2014-04-14 (ver 1.0.1.0) 实体Coupon订单表结构修改, 存储过程做相应修改。
** Modify by Gavin @2014-07-01 (ver 1.0.1.1) 因为插入收货单时,直接触发,会导致错误. 所以这里只用于批核. 
** Modify by Gavin @2014-07-09 (ver 1.0.1.2) (for RRG)订单状态增加一种:C:Picked 。业务流程改为：创建订单 -> 批核订单 -> 产生提货单 (P -> A -> C)
** Modify by Gavin @2014-07-10 (ver 1.0.1.3) (for RRG) 触发器中不再执行GenPickupOrder_Coupon,SVAWeb负责调用
*/
/*==============================================================*/
BEGIN  
  declare @CouponOrderFormNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @RecOrdStatus char(1)
  
  DECLARE CUR_Ord_CouponOrderForm_H CURSOR fast_forward FOR
    SELECT CouponOrderFormNumber, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_Ord_CouponOrderForm_H
  FETCH FROM CUR_Ord_CouponOrderForm_H INTO @CouponOrderFormNumber, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    -- ver 1.0.1.2 从 A 到 C 时，才产生Picking单。（for RRG）
    select @OldApproveStatus = ApproveStatus from Deleted where CouponOrderFormNumber = @CouponOrderFormNumber
    if (@OldApproveStatus = 'P' or ISNULL(@OldApproveStatus,'')='' ) and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
    --  exec GenPickupOrder_Coupon @CreatedBy, @CouponOrderFormNumber, 1
      update Ord_CouponOrderForm_H set ApprovalCode = @ApprovalCode, UpdatedOn = GETDATE(), ApproveOn = GETDATE()
        where CouponOrderFormNumber = @CouponOrderFormNumber
      
      exec ChangeCouponStockStatus 3, @CouponOrderFormNumber, 1   
    end
    
    if (@OldApproveStatus = 'A') and @ApproveStatus = 'C' and Update(ApproveStatus)
    begin   
      exec GenPickupOrder_Coupon @CreatedBy, @CouponOrderFormNumber, 1
      update Ord_CouponOrderForm_H set UpdatedOn = GETDATE() where CouponOrderFormNumber = @CouponOrderFormNumber
      exec ChangeCouponStockStatus 3, @CouponOrderFormNumber, 3  
    end
    
    if @ApproveStatus = 'V' and Update(ApproveStatus) 
    begin
      if (@OldApproveStatus = 'A') or (@OldApproveStatus = 'C')
      begin
        select @RecOrdStatus = ApproveStatus from Ord_CouponPicking_H where ReferenceNo = @CouponOrderFormNumber  
        set @RecOrdStatus = isnull(@RecOrdStatus, 'V')
        if @RecOrdStatus = 'R' or @RecOrdStatus = 'P'
        begin 
          update Ord_CouponPicking_H set ApproveStatus = 'V' where ReferenceNo = @CouponOrderFormNumber  
          -- 库存数量变动
          exec ChangeCouponStockStatus 3, @CouponOrderFormNumber, 2            
        end
        else if isnull(@RecOrdStatus, 'V') = 'V'
        begin
          -- 库存数量变动
          exec ChangeCouponStockStatus 3, @CouponOrderFormNumber, 2           
        end else
        begin
          RAISERROR ('Order can''t be voided!', 16, 1)
          if (@@Trancount > 0)
            ROLLBACK TRANSACTION        
          return                
        end           
      end  
    end 
              
    FETCH FROM CUR_Ord_CouponOrderForm_H INTO @CouponOrderFormNumber, @ApproveStatus, @CreatedBy    
  END
  CLOSE CUR_Ord_CouponOrderForm_H 
  DEALLOCATE CUR_Ord_CouponOrderForm_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'CouponOrderFormNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源方店铺主键，一般可能是总部' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货方店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户类型（备用，由大客户引起订单时填写）。1：客戶訂貨。 2：店鋪訂貨
默认2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'CustomerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'customer表 主键， 外键.（有大客户时填写）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network，5：快递送货（实体Coupon）。
1到4 是虚拟Coupon。  5 是实体Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址（店铺地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void。 C:Picked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵订货单。（店铺给总部的订单）
（for RRG） 订单状态增加一种:C:Picked 。业务流程改为：创建订单 -> 批核订单 -> 产生提货单 (P -> A -> C)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponOrderForm_H'
GO
