USE [NewBuying]
GO
/****** Object:  Table [dbo].[LogisticsProvider]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogisticsProvider](
	[LogisticsProviderID] [int] IDENTITY(1,1) NOT NULL,
	[LogisticsProviderCode] [varchar](64) NOT NULL,
	[ProviderName1] [nvarchar](512) NULL,
	[ProviderName2] [nvarchar](512) NULL,
	[ProviderName3] [nvarchar](512) NULL,
	[ProviderContactTel] [varchar](64) NULL,
	[ProviderContact] [varchar](64) NULL,
	[ProviderContactEmail] [varchar](64) NULL,
	[OrdQueryAddr] [nvarchar](512) NULL,
	[Remark] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_LOGISTICSPROVIDER] PRIMARY KEY CLUSTERED 
(
	[LogisticsProviderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'LogisticsProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'LogisticsProviderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'ProviderName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'ProviderName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'ProviderName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'ProviderContactTel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'ProviderContact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'ProviderContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单查询地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'OrdQueryAddr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流供应商' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsProvider'
GO
