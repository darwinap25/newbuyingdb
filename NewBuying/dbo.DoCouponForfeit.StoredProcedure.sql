USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCouponForfeit]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[DoCouponForfeit]
  @UserID			varchar(512),
  @CouponNumber		varchar(512),    -- 如果不为空，则指定的Coupon做forfeit. 否则就是所有状态为redeem，并且余额不为0的 coupon， 都做forfeit操作。
  @RefTxnNo			varchar(512),
  @BusDate			datetime,
  @TxnDate			datetime,
  @ApprovalCode		varchar(6),
  @StoreID			int, 
  @ServerCode		varchar(512), 
  @RegisterCode		varchar(512),
  @ForfeitControl	int=0			-- 0: 直接forfeit，coupon_movement触发器中调用。 1：EOD时调用。
AS
/****************************************************************************
**  Name : DoCouponForfeit
**  Version: 1.0.0.2
**  Description : DoCouponForfeit 过程，每日执行。 检查和更新Card 状态，
**
**  Created by:  Gavin @2012-07-30
**  Modify by Gavin @2012-09-06 (ver 1.0.0.1) 如果传入的@RefTxnNo， @ApprovalCode 为空，则产生一个填入
**  Modify by Gavin @2012-09-11 (ver 1.0.0.2) @CouponExpiryDate 变量需要从Coupon中读取
****************************************************************************/
begin
  declare @CouponTypeID int, @CouponforfeitControl int, @CouponAmount money, @ForfeitTxnNo varchar(50)
  declare @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate datetime, @NewCouponExpiryDate datetime
  
  if isnull(@RefTxnNo, '') = ''
  begin
    exec GetRefNoString 'FORFEI', @ForfeitTxnNo output   
    set @RefTxnNo = @ForfeitTxnNo
  end  
  if isnull(@ApprovalCode, '') = ''  
    exec GenApprovalCode @ApprovalCode output 

  select @CouponTypeID = CouponTypeID, @CouponAmount = CouponAmount, @CouponStatus = status, @CouponExpiryDate = CouponExpiryDate
    from coupon where CouponNumber = @CouponNumber and Status = 3 and CouponAmount > 0
  select @CouponforfeitControl = CouponforfeitControl from CouponType where CouponTypeID = @CouponTypeID
  
  if isnull(@CouponTypeID, 0) > 0 and isnull(@CouponforfeitControl, 0)=@ForfeitControl  -- 单个Coupon做forfeit。
  begin    
    insert into Coupon_Movement
      (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
       BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
       OrgExpiryDate, OrgStatus, NewStatus)    
    values
      (55, '', @CouponNumber, @CouponTypeID, '', 0, @RefTxnNo, @CouponAmount, -@CouponAmount, 0,
       @BusDate, @TxnDate, '','', @UserID, @CouponExpiryDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode,
       @CouponExpiryDate, @CouponStatus, @CouponStatus)
  end else if isnull(@CouponNumber, '') = '' and @ForfeitControl = 1   -- EOD时做forfeit
  begin
    insert into Coupon_Movement
      (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
       BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
       OrgExpiryDate, OrgStatus, NewStatus) 
    select
       55, '', C.CouponNumber, C.CouponTypeID, '', 0, @RefTxnNo, CouponAmount, -CouponAmount, 0,
       @BusDate, @TxnDate, '','', @UserID, CouponExpiryDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode,
       CouponExpiryDate, C.status, C.status
    from Coupon C left join CouponType T on C.CouponTypeID = T.CouponTypeID 
	where C.Status = 3 and C.CouponAmount > 0 and T.CouponforfeitControl = 1
  end
  return 0
end

GO
