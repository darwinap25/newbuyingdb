USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCouponOnhand_CouponOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[UpdateCouponOnhand_CouponOrder]
  @OprID  int,              -- 操作ID
  @OutStoreID int,          -- 出库的Store(减库存), 可能是供应商,如果是供应商则不用考虑
  @InStoreID int,           -- 入库store(加库存),一般是总部
  @CouponTypeID int,        -- 优惠劵类型ID
  @ActQty int               -- 操作的数量.总是正数
AS
/****************************************************************************
**  Name : UpdateCouponOnhand_CouponOrder
**  Version: 1.0.0.0
**  Description : 根据OprID分析订单操作,更改Onhand 的数量.
         sp_helptext UpdateCouponOnhand_CouponOrder
**
**  Created by: Gavin @2014-06-26
**
****************************************************************************/
begin
  Return 0
  declare @OutStoreTypeID int, @InStoreTypeID int, @A int, @CouponStatus int, @CouponStockStatus int
      
  select @OutStoreTypeID = StoreTypeID from store where StoreID = @OutStoreID
  select @InStoreTypeID = StoreTypeID from store where StoreID = @InStoreID
  
  -- Coupon上的 CouponStockStatus 和 Onhand上的有些不同. 应该只有 1(预定),2(好货),3(坏货) 状态. 这里计算出来的只是做参考
  exec @A = CalcCouponNewStockStatus @OprID,  @CouponStatus output, @CouponStockStatus output
  
  set @ActQty = ISNULL(@ActQty, 0)
  set @InStoreTypeID = ISNULL(@InStoreTypeID, 1)
  set @OutStoreTypeID = ISNULL(@OutStoreTypeID, 2)
   
  if @OprID in (70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80)  -- 只处理订单的OprID
  begin
    if @OprID = 70         --总部向供应商订单批核
    begin
      if exists(select * from Coupon_Onhand where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = @CouponStockStatus)
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = @CouponStockStatus 
      else
        Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
        values(@CouponTypeID, @InStoreTypeID, @InStoreID, @CouponStatus, @CouponStockStatus, @ActQty, GETDATE(), 1)                         
    end
    if @OprID = 71      --收货单批核 （好货）
    begin
      -- 加上好货
      if exists(select * from Coupon_Onhand where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = @CouponStockStatus)
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = @CouponStockStatus 
      else
        Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
        values(@CouponTypeID, @InStoreTypeID, @InStoreID, @CouponStatus, @CouponStockStatus, @ActQty, GETDATE(), 1)
      
      -- 减去订单的 For Printing 数量. 没有的话是数据错误
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 1                                  
    end
/*  这两个状态不影响库存数量.暂时不考虑改变.
    if @OprID = 72           --店铺向总部订单批核
    begin
      
    end
    if @OprID = 73           -- 拣货单批核 （批核之前，拣货单添加：(Dormant	Picked)，拣货单删除：(Dormant	Good for Release））
    begin
      set @NewStatus = 1
      set @NewStockStatus = 5
    end
*/    
    if @OprID = 74            -- 送货单批核, 认为货已经到店铺入库. 总部和店铺库存数量都有变动
    begin 
      -- 店铺加上好货
      if exists(select * from Coupon_Onhand where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = @CouponStockStatus)
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 2 
      else
        Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
        values(@CouponTypeID, @InStoreTypeID, @InStoreID, @CouponStatus, 2, @ActQty, GETDATE(), 1)
      
      -- 总部减去好货
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @ActQty  
           where StoreID = @OutStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 2 
    end
    if @OprID = 75            -- 退货单批核
    begin
      -- 店铺减去好货
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @ActQty  
           where StoreID = @OutStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 2 
    end
    if @OprID = 76           -- 退货的收货单批核
    begin
      -- 总部加上好货
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 2 
    end 
    if @OprID = 77           -- 收货单批核 （坏货）
    begin
      -- 总部加上坏货
      if exists(select * from Coupon_Onhand where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = @CouponStockStatus)
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 3 
      else
        Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
        values(@CouponTypeID, @InStoreTypeID, @InStoreID, @CouponStatus, 3, @ActQty, GETDATE(), 1)
        
      -- 减去订单的 For Printing 数量. 没有的话是数据错误
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 1            
    end 
    if @OprID = 80           -- 收货单批核 （其中未定状态,将产生新的收货单）
    begin
      -- 减去订单的 For Printing 数量. 没有的话是数据错误
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @ActQty  
           where StoreID = @InStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = @CouponStatus and CouponStockStatus = 1   
    end     
    
        
/*  这两个状态不影响库存数量.暂时不考虑改变.    
    if @OprID = 78           -- 拣货单添加
    begin
      set @NewStatus = 0
      set @NewStockStatus = 4
    end 
    if @OprID = 79           -- 拣货单删除
    begin
      set @NewStatus = 0
      set @NewStockStatus = 2
    end             
*/    
    return 0                      
  end
  else
    return -1   
end

GO
