USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSVACardNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetSVACardNo]
   @UID                  varchar(20),             -- 可能物理卡ID，可能卡面ID
   @InputCardNo          nvarchar(30),            -- 输入的卡号，可能是密文，可能是空
   @InputCardTypeCode    nvarchar(20),            -- 卡类别编码， 可能是空
   @OutputUID            varchar(20) Output,      -- 返回物理卡ID (UID)
   @OutputCardNo         nvarchar(30) Output,     -- 返回的卡号(明文)
   @OutputCardTypeID     int Output               -- 返回卡类别ID.  
AS
/****************************************************************************
**  Name : GetSVACardNo (未确定Card是否与SOGO一样的逻辑, 将根据情况修改)
**  Version: 1.0.0.0
**  Description : 输入UID，密文卡号等多种输入方式，返回SVA卡号和SVA CardType
**          1、 只输入UID：               从VenCardIDMap，获得Card表 CardNumber， CardTypeID
**          2、 输入UID，密文卡号，CardTypeCode：       通过解密来获得Card表 CardNumber， CardTypeID为输入的值，如果没有输入卡类型，则从VenCardIDMap获得
**          3、 没有UID，输入明文卡号，CardTypeCode，   返回 CardNumber， CardTypeID
**          
**  Created by: Gavin @2011-04-13
**
****************************************************************************/
begin
  -- 解析UID
  declare @TempUID varchar(20)
  exec ConvertToUID @UID, @TempUID output
  set @UID = @TempUID
  set @OutputUID = @UID
  
  --卡号解密--- 
  declare @CardNo nvarchar(30), @CardType nvarchar(20)
  if ISNULL(@UID, '') <> '' and isnull(@InputCardNo, '') <> ''
  begin
    set @OutputCardNo = dbo.DecryptCardNumber(RTrim(LTrim(@InputCardNo)), @UID)    
    select @OutputCardNo = CardNumber from VenCardIDMap where VendorCardNumber = @UID and CardNumber = @OutputCardNo and ValidateFlag = 1    
    select @OutputCardTypeID = CardTypeID from Card where CardNumber = @OutputCardNo
  end  
  else 
  begin
    if ISNULL(@UID, '') <> ''
      select @OutputCardNo = M.CardNumber, @OutputCardTypeID = C.CardTypeID from VenCardIDMap M left join Card C on M.CardNumber = C.CardNumber
        where VendorCardNumber = @UID and ValidateFlag = 1
    else if isnull(@InputCardNo, '') <> ''    
      select @OutputCardNo = CardNumber, @OutputCardTypeID = CardTypeID from Card where CardNumber = @InputCardNo  
    else
      set @OutputCardNo = ''
  end  
  if isnull(@OutputCardNo, '')  = ''
    return -1
  else    
    return 0   
end

GO
