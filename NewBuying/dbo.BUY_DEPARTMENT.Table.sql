USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_DEPARTMENT]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_DEPARTMENT](
	[DepartCode] [varchar](64) NOT NULL,
	[DepartName1] [nvarchar](512) NULL,
	[DepartName2] [nvarchar](512) NULL,
	[DepartName3] [nvarchar](512) NULL,
	[DepartPicFile] [nvarchar](512) NULL,
	[DepartPicFile2] [nvarchar](512) NULL,
	[DepartPicFile3] [nvarchar](512) NULL,
	[DepartNote] [nvarchar](512) NULL,
	[ReplenFormulaCode] [varchar](64) NULL,
	[FulfillmentHouseCode] [varchar](64) NULL,
	[CostCCC] [varchar](64) NULL DEFAULT ('      '),
	[CostWO] [varchar](64) NULL DEFAULT ('      '),
	[RevenueCCC] [varchar](64) NULL DEFAULT ('      '),
	[RevenueWO] [varchar](64) NULL DEFAULT ('      '),
	[NonOrder] [int] NOT NULL DEFAULT ((0)),
	[NonSale] [int] NOT NULL DEFAULT ((0)),
	[Consignment] [int] NOT NULL DEFAULT ((0)),
	[WeightItem] [int] NOT NULL DEFAULT ((0)),
	[DiscAllow] [int] NOT NULL DEFAULT ((1)),
	[CouponAllow] [int] NOT NULL DEFAULT ((1)),
	[VisuaItem] [int] NOT NULL DEFAULT ((0)),
	[CouponSKU] [int] NULL DEFAULT ((0)),
	[BOM] [int] NOT NULL DEFAULT ((0)),
	[MutexFlag] [int] NULL DEFAULT ((0)),
	[DiscountLimit] [decimal](12, 4) NULL DEFAULT ((100)),
	[OnAccount] [int] NULL DEFAULT ((1)),
	[WarehouseCode] [varchar](64) NULL,
	[DefaultPickupStoreCode] [varchar](64) NULL,
	[Additional] [nvarchar](512) NOT NULL,
	[Flag1] [int] NULL DEFAULT ((0)),
	[Flag2] [int] NULL DEFAULT ((0)),
	[Flag3] [int] NULL DEFAULT ((0)),
	[Flag4] [int] NULL DEFAULT ((0)),
	[Flag5] [int] NULL DEFAULT ((0)),
	[Flag6] [int] NULL DEFAULT ((0)),
	[Flag7] [int] NULL DEFAULT ((0)),
	[Flag8] [int] NULL DEFAULT ((0)),
	[Flag9] [int] NULL DEFAULT ((0)),
	[Flag10] [int] NULL DEFAULT ((0)),
	[Memo1] [nvarchar](512) NULL,
	[Memo2] [nvarchar](512) NULL,
	[Memo3] [nvarchar](512) NULL,
	[Memo4] [nvarchar](512) NULL,
	[Memo5] [nvarchar](512) NULL,
	[Memo6] [nvarchar](512) NULL,
	[Memo7] [nvarchar](512) NULL,
	[Memo8] [nvarchar](512) NULL,
	[Memo9] [nvarchar](512) NULL,
	[Memo10] [nvarchar](512) NULL,
	[StoreBrandCode] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_DEPARTMENT] PRIMARY KEY CLUSTERED 
(
	[DepartCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编码。 部门树状表示时，按照位数区分关系。例如：部门Code： A01001， 含义如下：
A  ： 第一级部门 code：  A
01： 第二级部门 code：  A01
001：第三级部门 code： A01001

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据要求，增加部门图片文件存储字段，存放不同语言的图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartPicFile2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据要求，增加部门图片文件存储字段，存放不同语言的图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartPicFile3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DepartNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自动补货公式表code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'ReplenFormulaCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_FulfillmentHouse表Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'FulfillmentHouseCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非订单货品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'NonOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非零售货品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'NonSale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'委托销售货品。（货品所有权归供应商所有）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Consignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'称重销售货品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'WeightItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许折扣。0：不允许，1：允许' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DiscAllow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许用Coupon支付。0：不允许，1：允许' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'CouponAllow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'虚拟货品。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'VisuaItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否Coupon货品。0：不是。 1：是。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'CouponSKU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否BOM货品，默认0
0： 不是BOM主货品
1： 是BOM主货品
（所有货品都可以加入BOM中）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'BOM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'互斥标志。 必须BOM主货品，此设置才生效。默认0。  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'MutexFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许的最大百分比折扣限制。 0~100， 默认100.   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DiscountLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许记账销售。0：不允许。1：允许。  默认1
注：此设置和POS端有关。 允许记账销售时，才可以用记账的 支付方式。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'OnAccount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'仓库编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'WarehouseCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'默认提货仓库code。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'DefaultPickupStoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag8'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag9'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Flag10'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo8'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo9'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'Memo10'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运营商品牌。（店铺的品牌（BUY_BRAND））' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT', @level2type=N'COLUMN',@level2name=N'StoreBrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品部门表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DEPARTMENT'
GO
