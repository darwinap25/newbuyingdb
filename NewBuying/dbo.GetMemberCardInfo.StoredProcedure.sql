USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCardInfo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberCardInfo]  
  @CardNumber			varchar(512),			-- 会员卡号 
  @LanguageAbbr			varchar(20)=''			 -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1
AS
/****************************************************************************
**  Name : GetMemberCardInfo
**  Version: 1.0.0.7
**  Description : 得到指定的会员卡的信息. 返回一条记录
**
**  Parameter :
**         return:  返回数据集   0：成功， -1：@MemberID和@CardNumber，@CardTypeID 联合查询，没有找到记录。
**
select * from card
  declare @A int
  exec @a = GetMemberCardInfo '000300049', 'en_CA'
  print @a
  sp_helptext GetMemberCardInfo
**  Created by: Gavin @2012-03-07
**  Modify by: Gavin @2013-07-16 (ver 1.0.0.1) 增加返回UpdatedOn。
**  Modify by: Gavin @2013-12-02 (ver 1.0.0.2) 增加返回C.CardAmountExpiryDate, C.CardPointExpiryDate
**  Modify by: Robin @2014-05-30 (ver 1.0.0.3) 根据CardUID获取CardInfo
**  Modify by: Robin @2014-05-30 (ver 1.0.0.4) 增加返回字段NickName
**  Modify by: Gavin @2014-07-17 (ver 1.0.0.5) 如果cardNumber不存在,则返回 errorcode: -2
**  Modify by: Gavin @2014-07-22 (ver 1.0.0.6) 增加返回字段：LoginFailureCount, QRCodePeriodValue, AllowOfflineQRCode, QRCodePrefix, TrainingMode
**  Modify by: Gavin @2014-08-11 (ver 1.0.0.7) (for 711) 如果card状态不是激活(status=2),则返回error -2 .  (仅711使用,其他客户使用需要改回)
**
****************************************************************************/
begin
  declare @Language int 
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  /*Add By Robin 2014-5-30 for getting information by UID*/
  declare @Temp_CardNumber varchar(512)
  select @Temp_CardNumber=CardNumber from CardUIDMap where CardUID=@CardNumber
  if @@Rowcount>0 set @CardNumber=@Temp_CardNumber
  /*End*/
  select top 1 case when @Language = 1 then CardTypeName1 when @Language = 2 then CardTypeName2 when @Language = 3 then CardTypeName3 else CardTypeName1 end as CardTypeName,
    case when @Language = 1 then CardGradeName1 when @Language = 2 then CardGradeName2 when @Language = 3 then CardGradeName3 else CardGradeName1 end as CardGradeName,
    1 as SeqNo, C.CardNumber, C.CardTypeID, Y.CardTypeCode, CardIssueDate, CardExpiryDate, 
    C.MemberID, C.CardGradeID, C.Status as CardStatus, C.TotalPoints, C.TotalAmount, 
	M.MemberEmail, G.CardGradeMaxAmount, C.ResetPassword, 
	Y.CardTypeName1, Y.CardTypeName2, Y.CardTypeName3, 
	M.MemberEngFamilyName, M.MemberEngGivenName, M.MemberChiFamilyName, M.MemberChiGivenName,
	M.MemberSex, M.MemberDateOfBirth, M.MemberDayofBirth, M.MemberMonthofBirth, M.MemberYearofBirth, M.HomeTelNum,
	V.vendorcardNumber as [UID], V.LaserID, Y.BrandID, B.StoreBrandName1 as BrandName, 
	C.BatchCardID, C.ParentCardNumber, M.MemberIdentityRef, G.CardGradeName1, G.CardGradeName2, G.CardGradeName3, G.CardGradeLayoutFile, M.MemberRegisterMobile,
	M.MemberIdentityType, M.MemberMobilePhone, G.CardGradePicFile, G.CardGradeNotes, G.CardPointToAmountRate, G.CardAmountToPointRate, M.CountryCode,
	G.IsAllowStoreValue, G.CardPointTransfer, G.CardAmountTransfer, G.MinAmountPreAdd, G.MaxAmountPreAdd, G.MinAmountPreTransfer, G.MaxAmountPreTransfer, 
	G.MinPointPreTransfer, G.MaxPointPreTransfer, G.DayMaxAmountTransfer, G.DayMaxPointTransfer, G.MinBalanceAmount, G.MinBalancePoint, G.MinConsumeAmount,
	B.StoreBrandPicSFile as BrandPicSFile, B.StoreBrandPicMFile as BrandPicMFile, B.StoreBrandPicGFile as BrandPicGFile, G.CardGradeMaxPoint, G.MinPointPreAdd, G.MaxPointPreAdd, G.GracePeriodValue, G.GracePeriodUnit,
	C.UpdatedOn, C.CardAmountExpiryDate, C.CardPointExpiryDate, M.NickName,
	G.LoginFailureCount, G.QRCodePeriodValue, G.AllowOfflineQRCode, G.QRCodePrefix, G.TrainingMode
    from [card] C left join Member M on C.MemberID = M.MemberID      
      left join CardGrade G on C.CardGradeID = G.CardGradeID
      left join CardType Y on C.CardTypeID = Y.CardTypeID 
      left join Brand B on Y.BrandID = B.StoreBrandID
      left join (select vendorcardNumber, LaserID, CardNumber from VenCardIDMap where ValidateFlag = 1) V on C.CardNumber = V.CardNumber
   where C.CardNumber = @CardNumber and C.Status = 2 
  if @@Rowcount = 0
    return -2
  else    
    return 0
end

GO
