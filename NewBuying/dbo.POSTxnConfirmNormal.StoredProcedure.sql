USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSTxnConfirmNormal]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[POSTxnConfirmNormal]    
  @UserID             varchar(64),   --用户ＩＤ    
  @TxnNo              varchar(64),   --交易号    
  @TxnSNList          varchar(max),   --交易SN清单， 格式： XXXXX,XXXXX...  每个TxnSN之间用 “,” 隔开
  @ApprovalCode       varchar(64) output --返回的approvalCode    
as    
/****************************************************************************    
**  Name : POSTxnConfirmNormal    
**  Version: 1.2.0.0   (通用版本)    
**  Description : POS提交的receivetxn 记录， 提交完成。 根据提供的@TxnSNList来完成confirm. 清单为空,则全部记录confirm   
**                注：将修改ReceiveTxn的触发器，允许直接提交批核（创建时状态就是 A）。所以此过程将只根据approvestatus来判断是否要批核（只接受approvestatus=P）
                  注：只会对Coupon做红冲。Card的每条记录都提交。Coupon做void的话, 必须提供VoidTxnNoSN, OprID和原操作相同，金额和数量 相反。
                  注：同一单，如果分多次Confirm，Coupon不能对已经confirm的操作做Void
**  Parameter :
    
declare @A int, @UserID varchar(512), @TxnNo varchar(512), @ApprovalCode varchar(512)    
set @UserID = '1'    
set @TxnNo = '20156181R01000029'    
exec @A = POSTxnConfirmNormal @UserID, @TxnNo,'20156181R01000029FS', @ApprovalCode output    
print @A    
print @ApprovalCode
select * from receivetxn    
	 select * from coupon where status = 1
   
**  Created by: Gavin @2015-06-24 
****************************************************************************/    
begin    
  declare @result int, @denomination money    
  declare @CouponNumber varchar(64), @RefTxnNo varchar(512), @BusDate datetime,     
          @TxnDate datetime, @StoreID int, @StoreCode varchar(64), @BrandCode varchar(64),    
          @ServerCode varchar(512), @RegisterCode varchar(512),    
          @CouponNumPatternLen int, @ApprovalcodeCount int, @CouponNumberDetail varchar(64)
      
  declare @T Table(OprID int, [Status] int, CreatedBy varchar(512), CouponNumber varchar(512), TxnNo varchar(512), Busdate DateTime, TxnDate DateTime,     
                   ApprovalCode varchar(512), StoreCode varchar(512), BrandCode varchar(512), ServerCode varchar(512), RegisterCode varchar(512), CouponCount int)    
  declare @SNList Table(TxnSN varchar(64)) 
  DECLARE @idoc int , @Tempstr varchar(max), @SNNO varchar(64), @Index int    
  declare @ActiveReceiveRecordCount int, @RecordCount int

  declare @KeyID varchar(30), @ShopCode varchar(10), @TxnNoSN varchar(30), @CardNumber nvarchar(30), @CouponCardNumber nvarchar(30),     
          @CardTypeID int, @OprID int, @Amount money, @Status int, @VoidKeyID varchar(30), @VoidTxnNo varchar(30), @Additional varchar(200), @Points int,    
          @Remark varchar(200), @SecurityCode varchar(300), @ApproveStatus char(1), @ApprovedBy varchar(10), @TenderID int    
  declare @TotalAmount money, @OpenBal money, @CloseBal money, @BatchID varchar(60), @UID varchar(60), @RefKeyID int, @VoidMovementKeyID int    
  declare @CouponStatus int, @CouponCount int, @NewCouponStatus int    
  declare @CouponNumberEnd Varchar(512),@CouponLength int, @ActAmount money    
  declare @CouponExpiryDate datetime, @CouponTypeID int, @NewCouponExpiryDate datetime    
  declare @BrandID int    
  declare @CouponOpenBal money, @CouponCloseBal money 
  declare @OldSTATUS char(1), @CardTotalAmt money , @Card_CardType varchar(40), @CardStatus int, @CardTotalPoints int      

  if isnull(@TxnNo, '') = ''
    return 0
        
  -- 准备TxnNOSN的 内存表
  set @Tempstr = isnull(@TxnSNList, '')
  if @Tempstr = ''
  begin
    insert into @SNList(TxnSN)
    select TxnNoSN from receivetxn where TxnNo = @TxnNo and ApproveStatus = 'P'
  end else
  begin
    -- 如果调用方提交列表,则不做检查 
    while @Tempstr <> ''
    begin
      set @Index = CHARINDEX(',', @Tempstr)
      if @Index > 0
      begin
        set @SNNO = SUBSTRING(@Tempstr,1, @Index - 1)
        set @Tempstr = SUBSTRING(@Tempstr,@Index + 1, Len(@Tempstr) - @Index) 
      end else
      begin
        set @SNNO = @Tempstr
        set @Tempstr = ''  
      end 
      if @SNNO <> ''
      begin
        insert into @SNList(TxnSN)
        values(@SNNO)
      end  
    end
  end
  -------------------------------------------
  
  --再次检查Coupon状态-----------------------    
  insert into @T    
  select OprID, max(Status) as Status, max(CreatedBy) as CreatedBy, CouponNumber, TxnNo, max(Busdate) as Busdate, max(TxnDate) as TxnDate,       
         max(ApprovalCode) as ApprovalCode, max(StoreCode) as StoreCode, max(BrandCode) as BrandCode, max(ServerCode) as ServerCode,     
         max(RegisterCode) as RegisterCode, sum(CouponCount) as CouponCount    
   from (     
          select OprID, C.Status, R.CreatedBy, R.CouponNumber, TxnNo, Busdate, TxnDate,       
                ApprovalCode, StoreCode, BrandCode, ServerCode, RegisterCode, CouponCount      
            from ReceiveTxn R inner join Coupon C on R.CouponNumber=C.CouponNumber
             left join CouponType T on C.CouponTypeID = T.CouponTypeID      
          where TxnNo = @TxnNo and ApproveStatus = 'P' and R.[Status] = 0 and R.CouponCount > 0   -- ver 1.1.0.15
                and TxnNoSN in (select TxnSN from @SNList)
        ) A group by OprID, TxnNo, CouponNumber    
  if exists(select * from @T Where (OprID in (33,53) and [Status]<>1))    
  begin    
    set @result= -23    
    return @result    
  end   
  if exists(select * from @T Where (OprID in (34,54) and [Status]<>2))    
  begin 
    set @result= -2703      
    return @result    
  end     
-------------------------------------------
  
  
  -- 开始操作 
  Begin Tran    
  
  /* 有效的对冲，只有Coupon需要合并操作, Card每条都执行（比如销售和void需要对冲）。*/  
  /* 对冲操作记录在输入时做过voidtxnsn 的检查, 和原记录相比，是相同的Oprid，相同的金额值，只是金额和数量为 负数。*/             
  /********执行插入Coupon_Movement**************/   
  exec GenApprovalCode @ApprovalCode output 
      
  DECLARE CUR_RECTXN CURSOR fast_forward local for
    select * from 
    (  
      select max(KeyID) KeyID, max(OprID) OprID, max(BrandCode) BrandCode, max(StoreCode) StoreCode, max(ServerCode) ServerCode, 
         max(RegisterCode) RegisterCode, max(TxnNoSN) TxnNoSN, TxnNo, max(BusDate) BusDate, max(TxnDate) TxnDate, CardNumber,  
         CouponNumber, sum(isnull(Amount,0)) Amount, sum(isnull(CouponCount,0)) as Counts,sum(isnull(Points,0)) Points,    
         max(VoidKeyID) VoidKeyID, max(VoidTxnNo) VoidTxnNo, max(Additional) Additional, max(Remark) Remark, max(SecurityCode) SecurityCode, 
         max(ApproveStatus) ApproveStatus, max(ApprovedBy) ApprovedBy, max(ApprovalCode) ApprovalCode, max(TenderID) TenderID
         , count(A.KeyID) as RecordCount 
      from (    
            SELECT KeyID, OprID, BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber,CouponNumber, isnull(Amount,0) as Amount, isnull(CouponCount,0) as CouponCount,isnull(Points,0) as Points,    
               VoidKeyID, VoidTxnNo, Additional, Remark, SecurityCode, ApproveStatus, ApprovedBy, ApprovalCode, TenderID    
             FROM receivetxn where TxnNo = @TxnNo and ApproveStatus = 'P' and [Status] = 0 and TxnNoSN in (select TxnSN from @SNList) 
               and isnull(CouponNumber,'') <> '' and OprID > 30      
           ) A group by TxnNo, CardNumber, CouponNumber, OprID
    ) B    
  OPEN CUR_RECTXN    
  FETCH FROM CUR_RECTXN INTO @KeyID, @OprID, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber,@CouponNumber, @Amount,     
    @CouponCount, @Points, @VoidKeyID, @VoidTxnNo, @Additional, @Remark, @SecurityCode, @ApproveStatus, @ApprovedBy,@ApprovalCode, @TenderID, @RecordCount 
  WHILE @@FETCH_STATUS=0    
  BEGIN
    -- Coupon 的 操作 (Coupon 应该只有OprID  53, 54)
    if (@CouponCount > 0) and (@CouponNumber <> '')
    begin
     Update ReceiveTxn set ApproveStatus = 'A', ApprovedBy = @UserID, ApprovedDate = Getdate(),ApprovalCode=@ApprovalCode     
        where KeyID = @KeyID         
    end    

    FETCH FROM CUR_RECTXN INTO @KeyID, @OprID, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber,@CouponNumber, @Amount,     
    @CouponCount, @Points, @VoidKeyID, @VoidTxnNo, @Additional, @Remark, @SecurityCode, @ApproveStatus, @ApprovedBy,@ApprovalCode, @TenderID, @RecordCount            
  END    
  CLOSE CUR_RECTXN     
  DEALLOCATE CUR_RECTXN          
  /********执行插入Movement**************/      
  
  -- Card 记录的 批核
  Update ReceiveTxn set ApproveStatus = 'A', ApprovedBy = @UserID, ApprovedDate = Getdate(),ApprovalCode=@ApprovalCode     
     where TxnNo = @TxnNo and ApproveStatus = 'P' and [Status] = 0 and TxnNoSN in (select TxnSN from @SNList) and OprID < 30    
                
  Commit Tran      
      
       
  return @result    
end    

GO
