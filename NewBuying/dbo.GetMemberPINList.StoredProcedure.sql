USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberPINList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetMemberPINList]
  @MemberID             int,               -- 会员主键 
  @CardNumber			varchar(512),      -- 如填写CardNumber，忽略MemberID
  @PINID                int,               -- 主键 
  @PINCode              varchar(64),       -- 编码
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''	  
AS
/****************************************************************************
**  Name : GetMemberPINList  
**  Version: 1.0.0.0
**  Description : 获得会员拥有的PIN.(Disney使用)
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = GetMemberPINList 17, '', 0, '', 1, 30, @count output, @recordcount output, 'zh_CN'
  print @a  
  print @count
  print @recordcount
**  Created by: Gavin @2013-02-20
**
****************************************************************************/
begin  
  declare @SQLStr nvarchar(4000), @Language int --, @ConditionStr nvarchar(400)
  
  set @PINID = isnull(@PINID, 0)
  set @PINCode = isnull(@PINCode, '')
  if isnull(@MemberID, 0) = 0 
    select @MemberID = MemberID from Card where CardNumber = @CardNumber
  if (isnull(@MemberID, 0) = 0) and (isnull(@CardNumber, '') = '')  
    return -7  
    
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1

  set @SQLStr = ' select case ' + cast(@Language as varchar) + ' when 2 then PINDesc2 when 3 then PINDesc3 else PINDesc1 end as PINDesc, '
      + ' M.PINID, P.PINCode, PINDesc1, PINDesc2, PINDesc3, M.MemberPINID, M.MemberID, M.Qty, M.Remark '
      + ' from MemberPIN M left join DisneyPIN P on M.PINID = P.PINID '
      + ' where Qty > 0 and (M.PINID = ' + cast(@PINID as varchar) + ' or ' + cast(@PINID as varchar) + ' = 0) '
      + ' and (P.PINCode = ''' + @PINCode + ''' or ''' + @PINCode + ''' = '''') '
      + ' and (M.MemberID = ' + cast(@MemberID as varchar) + ' or ' + cast(@MemberID as varchar) + ' = 0) '
     
  exec SelectDataInBatchs @SQLStr, 'PINID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', ''
  return 0
end

GO
