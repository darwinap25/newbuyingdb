USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SVAServiceApplyUpdate]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SVAServiceApplyUpdate]
    @TxnNo                 varchar(64),         -- 交易编号
    @Status			     int ,				    -- 交易状态
	@DeliveryFlag          int ,                -- 送货标志（0：自提，不送货。 1：送货）
    @DeliveryCountry       varchar(64),         -- 送货所在国家
    @DeliveryProvince      varchar(64),         -- 送货所在省 
    @DeliveryCity          varchar(64),         -- 送货所在城市
    @DeliveryDistrict      varchar(64),         -- 送货地址所在区县
    @DeliveryAddressDetail varchar(512),        -- 送货详细地址
    @DeliveryFullAddress   varchar(512),        -- 送货完整地址
    @DeliveryNumber        varchar(512)='',     -- 送货单号.
    @LogisticsProviderID   int=0,			    -- 送货的物流供应商。
    @DeliveryDate		   datetime,		    -- 货物送达日期  
    @DeliveryBy		       int,		            -- 送货员
    @Contact               varchar(512),        -- 收货联系人
    @ContactPhone          varchar(512),        -- 收货联系手机
    @PickupType            int = 1,             -- 提货方式。 默认 1.  1：门店自提	 2：送货
    @PickupStoreCode       varchar(64) = ''     -- 快递送货到达的店铺编号。 （PickupType=1 时有效）
AS
/****************************************************************************
**  Name : [[SVAServiceApplyUpdate]]
**  Version: 1.0.0.1   
**  Description : 修改退货状态。
**  Created by: Lisa @2016-01-05
**  Update  by: Lisa @2016-01-06
**  Update  by: Lisa @2016-01-07 新增的时候从store表中把区域、县、城市、地区默认读取出来
****************************************************************************/
BEGIN
	SET NOCOUNT ON
	declare @Result int,@Collected int ,@SeqNo varchar(64)

	begin tran  
	begin
		--修改AfterSales_H数据
		update AfterSales_H set Status=@Status,DeliveryFlag=@DeliveryFlag,DeliveryCountry=@DeliveryCountry,
		DeliveryProvince=@DeliveryProvince,DeliveryCity=@DeliveryCity,DeliveryDistrict=@DeliveryDistrict,
		DeliveryAddressDetail=@DeliveryAddressDetail,DeliveryFullAddress=@DeliveryFullAddress,
		DeliveryNumber=@DeliveryNumber,LogisticsProviderID=@LogisticsProviderID,DeliveryDate=@DeliveryDate,
		DeliveryBy=@DeliveryBy,Contact=@Contact,ContactPhone =@ContactPhone,PickupType=@PickupType,
		PickupStoreCode=@PickupStoreCode,UpdatedOn=GetDate()
		where TxnNo=@TxnNo	   
	end

	begin
	    --修改SalesD数据
		if(@Status=7)
		select @SeqNo=RefTransNum from AfterSales_H where TxnNo =@TxnNo
	     update Sales_D set Collected = case Collected when 80 then 0 when 81 then 1 when 82 then 2 when 84 then 4 end
	     where TransNum = @SeqNo
	    and SeqNo in (select RefSeqNo from AfterSales_D where TxnNo = @TxnNo ) 
	end
   IF @@ERROR <> 0
    begin
	set @Result=-1;
     rollback tran
    end
   else
     begin
	 set @Result=0;
	  commit tran
	 end

    SET NOCOUNT OFF
	return @Result
END

GO
