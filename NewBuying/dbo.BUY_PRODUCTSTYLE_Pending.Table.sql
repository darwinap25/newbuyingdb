USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PRODUCTSTYLE_Pending]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PRODUCTSTYLE_Pending](
	[ProdCodeStyle] [varchar](64) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[ProductSizeType] [varchar](64) NOT NULL,
 CONSTRAINT [PK_BUY_PRODUCTSTYLE_PENDING] PRIMARY KEY CLUSTERED 
(
	[ProdCodeStyle] ASC,
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品系列编码，联合主键。（以prodcode作为主键，例如，同一系列不同颜色的口红，以第一个颜色的货品作为 productline 的主键，其他货品作为子货品。 主键货品也需要在子货品列表中）
货品A系列： A1，A2，A3
StyleCode         ProdCode
A1                     A1
A1                     A2
A1                     A3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTSTYLE_Pending', @level2type=N'COLUMN',@level2name=N'ProdCodeStyle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码，联合主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTSTYLE_Pending', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此类商品使用的尺寸类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTSTYLE_Pending', @level2type=N'COLUMN',@level2name=N'ProductSizeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品同类分组表。（相同货品名称，不同货品code，不同颜色，不同尺寸）
Pending表 @2016-08-08 (for bauhaus)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTSTYLE_Pending'
GO
