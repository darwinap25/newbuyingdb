USE [NewBuying]
GO
/****** Object:  UserDefinedFunction [dbo].[EncryptCardNumber]    Script Date: 12/13/2017 2:45:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[EncryptCardNumber]
(
  @CardNumber varchar(40), @KeyIDea varchar(30)
)
RETURNS varchar(40) 
AS
BEGIN
  declare @EncryptCardNumber varchar(40)
  DECLARE @position int, @string char(15), @Total int, @CharNum int, @TempNum int
  set @EncryptCardNumber = ''

  SET @position = 1
  set @Total = 0
  WHILE @position <= DATALENGTH(@KeyIDea)
  BEGIN
    SELECT @Total = @Total + ASCII(SUBSTRING(@KeyIDea, @position, 1))
    SET @position = @position + 1
  END
  Set @Total = @Total % 10
  
  SET @position = 1
  WHILE @position <= DATALENGTH(@CardNumber)
  BEGIN
    --SELECT @CharNum = ASCII(SUBSTRING(@CardNumber, @position, 1)) ^ @Total   
    SELECT @CharNum = ASCII(SUBSTRING(@CardNumber, @position, 1)) * (@position + @Total) 
    
    -- 检查是否是XML不允许的符号，是的话转换
    set @TempNum = (@CharNum / 75) + 48
    if @TempNum = 60 
      set @TempNum = 40
    if @TempNum = 62 
      set @TempNum = 41    
    if @TempNum = 64 
      set @TempNum = 37           
    set @EncryptCardNumber = @EncryptCardNumber + char(@TempNum)
    set @TempNum = (@CharNum % 75) + 48
    if @TempNum = 60 
      set @TempNum = 40
    if @TempNum = 62 
      set @TempNum = 41        
    if @TempNum = 64 
      set @TempNum = 37          
       
    set @EncryptCardNumber = @EncryptCardNumber + char(@TempNum)
 
    SET @position = @position + 1
  END
  
  return @EncryptCardNumber
END

GO
