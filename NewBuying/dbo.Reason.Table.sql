USE [NewBuying]
GO
/****** Object:  Table [dbo].[Reason]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Reason](
	[ReasonID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonCode] [varchar](64) NOT NULL,
	[ReasonDesc1] [nvarchar](512) NULL,
	[ReasonDesc2] [nvarchar](512) NULL,
	[ReasonDesc3] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [Reason_PK] PRIMARY KEY CLUSTERED 
(
	[ReasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Reason', @level2type=N'COLUMN',@level2name=N'ReasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原因编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Reason', @level2type=N'COLUMN',@level2name=N'ReasonCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原因列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Reason'
GO
