USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCardGrade_Schedule]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[UpdateCardGrade_Schedule]
as
/****************************************************************************
**  Name : UpdateCardGrade_Schedule  
**  Version: 1.0.0.2
**  Description :  根据卡积分升级。每日执行。(可以是JOB，或者放在EOD过程中执行)
**  Parameter :   
**  exec UpdateCardGrade_Schedule
    
**  Created by: Gavin @2015-10-08
**  Modify by: Gavin @2015-10-09 (ver 1.0.0.1) 逻辑修改, CardGrade中CardGradeUpdThreshold的设置，表示升级入这个grade的最低门槛
**  Modify by: Gavin @2015-10-12 (ver 1.0.0.2) 如果升级到这个级别，则赠送此级别的Coupon （如果有赠送的话） 
**
****************************************************************************/
begin
  DECLARE @CardTypeID int  
  DECLARE @PrevCardGradeID int, @PrevCardGradeUpdMethod int, @PrevCardGradeUpdThreshold int, @PrevCardGradeRank int
  DECLARE @CardGradeID int, @CardGradeUpdMethod int, @CardGradeUpdThreshold int, @CardGradeRank int
  DECLARE @BindCouponTypeID int, @BindCouponCount int, @CouponNumber varchar(64), @CardNumber varchar(64), @MemberID int
  declare @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate date, @NewCouponExpiryDate date, @CouponAmount money, 
          @ApprovalCode varchar(6), @i int, @BusDate datetime, @TxnDate datetime

  -- 取busdate
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 
  if isnull(@TxnDate, 0) = 0
    set @TxnDate = getdate()
      
  DECLARE CUR_UpdateGrade_SCHE_CARDTYPE CURSOR fast_forward local FOR
      select CardTypeID from CardType
  OPEN CUR_UpdateGrade_SCHE_CARDTYPE
  FETCH FROM CUR_UpdateGrade_SCHE_CARDTYPE INTO @CardTypeID 
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @PrevCardGradeID = 0, @PrevCardGradeUpdMethod = 0, @PrevCardGradeUpdThreshold = 0, @PrevCardGradeRank = 0
    DECLARE CUR_UpdateGrade_SCHE_CARDGRADE CURSOR fast_forward local FOR
        select CardGradeID, CardGradeUpdMethod, CardGradeUpdThreshold, CardGradeRank 
          from cardGrade where CardTypeID = @CardTypeID
         order by CardGradeRank
    OPEN CUR_UpdateGrade_SCHE_CARDGRADE
    FETCH FROM CUR_UpdateGrade_SCHE_CARDGRADE INTO @CardGradeID, @CardGradeUpdMethod, @CardGradeUpdThreshold, @CardGradeRank 
    WHILE @@FETCH_STATUS=0
    BEGIN      
      -- ver 1.0.0.2
          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR
            select A.CardNumber, B.CouponTypeID, B.HoldCount from
            (
              select @CardGradeID as NextGradeID, * from Card                        
                where CardTypeID = @CardTypeID and CardGradeID = @PrevCardGradeID 
                   and ((isnull(@CardGradeUpdMethod, 0) = 1 and CumulativeConsumptionAmt >= @CardGradeUpdThreshold)
                    or (isnull(@CardGradeUpdMethod, 0) = 2 and CumulativeEarnPoints >= @CardGradeUpdThreshold) )   
            ) A left join (select * from CardGradeHoldCouponRule where RuleType = 1) B on A.NextGradeID = B.CardGradeID         
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN
            set @CouponNumber = ''
            set @i = 1
            while @i <= @BindCouponCount
            begin
              exec GetCouponNumber @BindCouponTypeID, 1, @CouponNumber output            
              if isnull(@CouponNumber, '') <> ''
              begin
                select @CouponExpiryDate = CouponExpiryDate, @CouponAmount = CouponAmount from Coupon where CouponNumber = @CouponNumber
                
                exec GenApprovalCode @ApprovalCode output
                exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                values(  
                      32, @CardNumber, @CouponNumber, @BindCouponTypeID, '', 0, '', @CouponAmount, 0, @CouponAmount,
                      @BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2)
              end
              set @i = @i + 1              
            end                         
            FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
          /*=====================================================*/  
      ------------------- 
      -- 插入Card_Movement
      insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
           CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      select 1001, CardNumber, '', 0, Convert(varchar(10), Getdate(), 120), TotalAmount, 0, TotalAmount, 0, @BusDate, @TxnDate, 
          null, null, '', '', 1, null, null, null,
          TotalPoints, TotalPoints, CardExpiryDate, CardExpiryDate, Status, Status
      from Card
        where CardTypeID = @CardTypeID and CardGradeID = @PrevCardGradeID 
            and ((isnull(@CardGradeUpdMethod, 0) = 1 and CumulativeConsumptionAmt >= @CardGradeUpdThreshold)
                   or (isnull(@CardGradeUpdMethod, 0) = 2 and CumulativeEarnPoints >= @CardGradeUpdThreshold) )      
      -- 更新Card                 
      update Card set CardGradeID = @CardGradeID
      where CardTypeID = @CardTypeID and CardGradeID = @PrevCardGradeID 
        and ((isnull(@CardGradeUpdMethod, 0) = 1 and CumulativeConsumptionAmt >= @CardGradeUpdThreshold)
            or (isnull(@CardGradeUpdMethod, 0) = 2 and CumulativeEarnPoints >= @CardGradeUpdThreshold) )                  

      select @PrevCardGradeID = @CardGradeID, @PrevCardGradeUpdMethod = @CardGradeUpdMethod, 
        @PrevCardGradeUpdThreshold = @CardGradeUpdThreshold, @PrevCardGradeRank = @CardGradeRank  
      FETCH FROM CUR_UpdateGrade_SCHE_CARDGRADE INTO @CardGradeID, @CardGradeUpdMethod, @CardGradeUpdThreshold, @CardGradeRank 
    END
    CLOSE CUR_UpdateGrade_SCHE_CARDGRADE 
    DEALLOCATE CUR_UpdateGrade_SCHE_CARDGRADE  
  
         
    FETCH FROM CUR_UpdateGrade_SCHE_CARDTYPE INTO @CardTypeID
  END
  CLOSE CUR_UpdateGrade_SCHE_CARDTYPE 
  DEALLOCATE CUR_UpdateGrade_SCHE_CARDTYPE  
END

GO
