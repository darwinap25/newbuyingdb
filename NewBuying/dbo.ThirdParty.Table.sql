USE [NewBuying]
GO
/****** Object:  Table [dbo].[ThirdParty]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThirdParty](
	[ThirdPartyID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyCode] [varchar](64) NOT NULL,
	[ThirdPartyName1] [varchar](512) NULL,
	[ThirdPartyName2] [varchar](512) NULL,
	[ThirdPartyName3] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_THIRDPARTY] PRIMARY KEY CLUSTERED 
(
	[ThirdPartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ThirdParty] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ThirdParty] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdParty', @level2type=N'COLUMN',@level2name=N'ThirdPartyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三方代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdParty', @level2type=N'COLUMN',@level2name=N'ThirdPartyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdParty', @level2type=N'COLUMN',@level2name=N'ThirdPartyName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdParty', @level2type=N'COLUMN',@level2name=N'ThirdPartyName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdParty', @level2type=N'COLUMN',@level2name=N'ThirdPartyName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三方。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdParty'
GO
