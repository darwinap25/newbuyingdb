USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberMessageTotal]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberMessageTotal]
  @MemberID				int
AS
/****************************************************************************
**  Name : GetMemberMessageTotal
**  Version: 1.0.0.0
**  Description : 读取会员接收的未读消息总计
**
**  Parameter :
  declare @a int, @MessageID int
  exec @a = GetMemberMessageTotal 170
  print @a  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin
  select count(*) as UnReadMessageCount from MessageReceiveList R left join MessageObject O on R.MessageID = O.MessageID
  where O.IsInternal = 1 and R.IsRead = 0 and R.MemberID = @MemberID 
  return 0    
end

GO
