USE [NewBuying]
GO
/****** Object:  Table [dbo].[OperationTable]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OperationTable](
	[OprID] [int] NOT NULL,
	[OprIDDesc1] [nvarchar](512) NULL,
	[OprIDDesc2] [nvarchar](512) NULL,
	[OprIDDesc3] [nvarchar](512) NULL,
	[Remark] [nvarchar](512) NULL,
 CONSTRAINT [PK_OPERATIONTABLE] PRIMARY KEY CLUSTERED 
(
	[OprID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OprID主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperationTable', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OprID描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperationTable', @level2type=N'COLUMN',@level2name=N'OprIDDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OprID描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperationTable', @level2type=N'COLUMN',@level2name=N'OprIDDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OprID描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperationTable', @level2type=N'COLUMN',@level2name=N'OprIDDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperationTable', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OprID表。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperationTable'
GO
