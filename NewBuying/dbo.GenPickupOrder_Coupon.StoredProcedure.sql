USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenPickupOrder_Coupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenPickupOrder_Coupon]
  @UserID                 int,             --操作员ID
  @CouponOrderFormNumber    varchar(64),     --卡创建单的号码
  @IsEarchCoupon            int=0            -- 1: 输入每个coupon，即一个coupon一行。 0：连续coupon合并。 默认0
AS
/****************************************************************************
**  Name : GenPickupOrder_Coupon
**  Version: 1.0.1.5
**  Description : 自动根据Coupon订单产生拣货单
**  example :
  declare @a int  , @CouponOrderFormNumber varchar(64)
  set @CouponOrderFormNumber = 'PR0000000000335'
  exec @a = GenPickupOrder_Coupon 1, @CouponOrderFormNumber, 1
  print @a   
**  Created by: Gavin @2012-09-21
**  Modify by: Gavin @2012-09-28 （ver 1.0.0.1）没有库存的coupon， 要根据Order的 detail记录， 加入pickup=0 的记录。
**  Modify by: Gavin @2012-10-08 （ver 1.0.0.2）增加对导入coupon的处理。（没有couponnummask）
**  Modify by: Gavin @2012-10-09  (ver 1.0.0.3) fix bug: 修正update coupon 的 pickupflag的问题.
**  Modify by: Gavin @2012-10-18 (ver 1.0.0.4) Ord_CouponPicking_D表增加字段，BatchCouponCode，Pickupdatetime（默认值系统时间）
**  Modify by: Gavin @2012-11-13 (ver 1.0.0.5) 修改CalcCheckDigit_EAN13 为CalcCheckDigit。
**  Modify by: Gavin @2014-04-14 (ver 1.0.1.0) 实体Coupon订单表结构修改, 存储过程做相应修改。
**  Modify by: Gavin @2014-06-18 (ver 1.0.1.1) Ord_CouponPicking_H 增加 OrderType 字段。
**  Modify by: Gavin @2014-07-01 (ver 1.0.1.2) 增加参数@IsEarchCoupon，控制是否一个Coupon一条子记录
**  Modify by: Gavin @2014-07-10 (ver 1.0.1.3) (for RRG) 执行完更新Ord_CouponOrderForm_H的approveStatus=C
**  Modify by: Gavin @2015-05-20 (ver 1.0.1.4) 订单表中的数据太大,会导致执行时间过长, 采用 分批insert 的方式解决
**  Modify by: Gavin @2015-05-21 (ver 1.0.1.5) 合并成一次insert， update 也合并成一次
**
****************************************************************************/
begin
  declare @CouponPickingNumber varchar(64), @BusDate date
  declare @KeyID int, @BrandID int, @CouponQty int, @i int, @CouponNumber varchar(64)
  declare @CouponTypeID int, @CouponNumMask nvarchar(30), @CouponNumPattern nvarchar(30), @PatternLen int, @Checkdigit int
  declare @StartCouponNumber varchar(64), @EndCouponNumber varchar(64), @PrevCouponNumber varchar(64), @CouponSeqNo bigint, @PickQty int
  declare @FirstCoupon varchar(64), @EndCoupon varchar(64)
  declare @IsImportCouponNumber int, @IsConsecutiveUID int, @BatchCouponCode varchar(64), @StartBatchCouponCode varchar(64)
  declare @ModeID int, @ModeCode varchar(64)
  declare @tempqty int, @actqty int
  
  -- 获取busdate
  select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1    

  -- 获取pickup单的单号
  exec GetRefNoString 'COPO', @CouponPickingNumber output 
  -- 插入pickup单的头表数据 
  insert into Ord_CouponPicking_H(CouponPickingNumber, ReferenceNo, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod,
     SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, StoreMobile, FromContactName, FromContactNumber, 
     FromEmail, FromMobile, Remark, CreatedBusDate, ApproveStatus, CreatedBy, UpdatedBy, OrderType, Remark1) 
  select @CouponPickingNumber, CouponOrderFormNumber, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod,
     SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, StoreMobile, FromContactName, FromContactNumber, 
     FromEmail, FromMobile, Remark, @BusDate,'R', @UserID, @UserID, OrderType, Remark1 
    from Ord_CouponOrderForm_H
   where CouponOrderFormNumber = @CouponOrderFormNumber
  
  -- 插入pickup单的子表数据
  DECLARE CUR_CouponOrderForm_D CURSOR fast_forward for
    select max(BrandID) as BrandID, CouponTypeID, sum(isnull(D.CouponQty,0))  
       from Ord_CouponOrderForm_D D Left join Ord_CouponOrderForm_H H on H.CouponOrderFormNumber = D.CouponOrderFormNumber 
      where D.CouponOrderFormNumber = @CouponOrderFormNumber
     group by CouponTypeID   
  OPEN CUR_CouponOrderForm_D
  FETCH FROM CUR_CouponOrderForm_D INTO @BrandID, @CouponTypeID, @CouponQty  
  WHILE @@FETCH_STATUS=0
  BEGIN
    if @IsEarchCoupon = 1
    begin
       insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, 
           FirstCouponNumber, EndCouponNumber, BatchCouponCode)
       select CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, 
           FirstCouponNumber, EndCouponNumber, BatchCouponCode
         from (           
          select ROW_NUMBER() OVER(order by C.CouponNumber) as IID, @CouponPickingNumber as CouponPickingNumber , @CouponTypeID as CouponTypeID, '' as [Description], 
                1 as OrderQty, 1 as PickQty, C.CouponNumber as FirstCouponNumber, C.CouponNumber as EndCouponNumber, B.BatchCouponCode 
             from Coupon C left join BatchCoupon B on C.BatchCouponID = B.BatchCouponID
            where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CouponTypeID = @CouponTypeID
          ) A
        where A.IID <= @CouponQty 
       order by IID 
    
       update coupon set PickupFlag = 1, StockStatus = 4 
       where CouponNumber in 
         (select FirstCouponNumber from 
             (           
               select ROW_NUMBER() OVER(order by C.CouponNumber) as IID,
                      1 as OrderQty, 1 as PickQty, C.CouponNumber as FirstCouponNumber, C.CouponNumber as EndCouponNumber
                from Coupon C 
               where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CouponTypeID = @CouponTypeID
             ) A
           where A.IID <= @CouponQty 
         )
/*  
      -- insert 的数据量太多, 只能分批执行.
      set @tempqty = @CouponQty
      while @tempqty > 0 
      begin
        if @tempqty >= 1000
        begin
          set @actqty = 1000
          set @tempqty = @tempqty - 1000
        end else
        begin
          set @actqty = @tempqty
          set @tempqty = 0       
        end  
       
       begin tran A  
          
       insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, 
           FirstCouponNumber, EndCouponNumber, BatchCouponCode)
       select CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, 
           FirstCouponNumber, EndCouponNumber, BatchCouponCode
         from (           
          select ROW_NUMBER() OVER(order by C.CouponNumber) as IID, @CouponPickingNumber as CouponPickingNumber , @CouponTypeID as CouponTypeID, '' as [Description], 
                1 as OrderQty, 1 as PickQty, C.CouponNumber as FirstCouponNumber, C.CouponNumber as EndCouponNumber, B.BatchCouponCode 
             from Coupon C left join BatchCoupon B on C.BatchCouponID = B.BatchCouponID
            where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CouponTypeID = @CouponTypeID
          ) A
        where A.IID <= @actqty 
       order by IID 

       update coupon set PickupFlag = 1, StockStatus = 4 
       where CouponNumber in 
       (
         select FirstCouponNumber
         from (           
          select ROW_NUMBER() OVER(order by C.CouponNumber) as IID,
                1 as OrderQty, 1 as PickQty, C.CouponNumber as FirstCouponNumber, C.CouponNumber as EndCouponNumber
             from Coupon C 
            where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 --and C.CouponTypeID = @CouponTypeID
          ) A
         where A.IID <= @actqty 
       ) 
             
       commit tran A   
     end 
*/   
                 
    end
    else  
    BEGIN 
       
      -- 获得CouponType的设置
      select @CouponNumMask = CouponNumMask, @CouponNumPattern = CouponNumPattern, @CouponTypeID = CouponTypeID,
          @Checkdigit = isnull(CouponCheckdigit,0), @IsImportCouponNumber = isnull(IsImportCouponNumber,0), @IsConsecutiveUID = isnull(IsConsecutiveUID,1),
          @ModeID = CheckDigitModeID
        from CouponType where CouponTypeID = @CouponTypeID
      select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
      set @ModeCode = isnull(@ModeCode, 'EAN13')
          
      set @PatternLen = 0
      while @PatternLen < Len(@CouponNumMask)
      begin      
        if substring(@CouponNumMask, @PatternLen + 1, 1) = 'A'
          set @PatternLen = @PatternLen + 1
        else  
          break  
      end
    
     -- 自动获取 Pickup Detail 的 Coupon号码 （循环） 
     set @i = 1
     set @PickQty = 0
     set @CouponNumber = ''
     set @StartCouponNumber = ''
     set @EndCouponNumber = ''
     set @PrevCouponNumber = ''
     DECLARE CUR_Coupon CURSOR fast_forward for
       select C.CouponNumber, B.BatchCouponCode from Coupon C left join BatchCoupon B on C.BatchCouponID = B.BatchCouponID
          where C.status = 0 and C.PickupFlag = 0 and C.StockStatus = 2 and C.CouponTypeID = @CouponTypeID order by C.CouponNumber
	   OPEN CUR_Coupon
     FETCH FROM CUR_Coupon INTO @CouponNumber, @BatchCouponCode
     WHILE @@FETCH_STATUS=0 and @i <= @CouponQty
     BEGIN    
       if @StartCouponNumber = ''
       begin
         set @StartCouponNumber = @CouponNumber
         set @StartBatchCouponCode = @BatchCouponCode
         set @EndCouponNumber = @CouponNumber
       end
       if isnull(@PrevCouponNumber, '') <> ''
       begin  
            if isnull(@IsImportCouponNumber, 0) = 0
            begin
              -- 获得上一个couponnumber的序号。
              if @Checkdigit = 1 
                set @CouponSeqNo = substring(@PrevCouponNumber, @PatternLen + 1,  Len(@CouponNumMask) - @PatternLen - 1) 
              else  
                set @CouponSeqNo = substring(@PrevCouponNumber, @PatternLen + 1,  Len(@CouponNumMask) - @PatternLen)      
              -- 计算从上一个序号开始的连续序号和卡号
              set @CouponSeqNo = @CouponSeqNo + 1
              if @Checkdigit = 1 
              begin
                set @EndCouponNumber = substring(@CouponNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CouponSeqNo), Len(@CouponNumMask) - @PatternLen - 1)        
                select @EndCouponNumber = dbo.CalcCheckDigit(@EndCouponNumber, @ModeCode)
              end else        
                set @EndCouponNumber = substring(@CouponNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CouponSeqNo), Len(@CouponNumMask) - @PatternLen)
            end else  
            begin
              if isnull(@IsConsecutiveUID, 1) = 1
              begin
                if @Checkdigit = 1
                begin 
                  set @EndCouponNumber =  cast((cast(substring(@PrevCouponNumber,1,len(@PrevCouponNumber)-1) as bigint) + 1) as varchar)
                  select @EndCouponNumber = dbo.CalcCheckDigit(@EndCouponNumber, @ModeCode)
                end else  
                  set @EndCouponNumber =  cast((cast(@PrevCouponNumber as bigint) + 1) as varchar)
              end else
                set @EndCouponNumber =  ''  
            end          
          -- 比较根据上一个序号计算出的号码是否和当前号码相同，如果相同则表示连续，如果不同，则表示不连续，需要另起一条记录。（上一条为空不包括）   
          if (@EndCouponNumber <> @CouponNumber) and (isnull(@PrevCouponNumber,'') <> '')
          begin                      
	          insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode)
	          values(@CouponPickingNumber, @CouponTypeID, '', @CouponQty, @PickQty, @StartCouponNumber, @PrevCouponNumber, @StartBatchCouponCode) 
	          set @PickQty = 0 
	          set @StartCouponNumber = @CouponNumber
	          set @StartBatchCouponCode = @BatchCouponCode
          end
          set @EndCouponNumber = @CouponNumber
	     end
	     set @PickQty = @PickQty + 1	  
	     set @i = @i + 1
	     set @PrevCouponNumber = @CouponNumber	  
	     FETCH FROM CUR_Coupon INTO @CouponNumber, @BatchCouponCode  
	   END
     CLOSE CUR_Coupon 
     DEALLOCATE CUR_Coupon 	
      -- END （循环） 自动获取 Pickup Detail 的 Coupon号码 
      -- 自动获取 Pickup Detail 的 Coupon号码 （循环外）
      if (@PickQty > 0) 
      begin
      insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode)
	    values(@CouponPickingNumber, @CouponTypeID, '', @CouponQty, @PickQty, @StartCouponNumber, @EndCouponNumber, @StartBatchCouponCode) 	
	    set @PickQty = 0   
	  end 
	
  	  -- 没有库存的coupon， 要根据Order的 detail记录， 加入pickup=0 的记录
    	if not exists(select * from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber and CouponTypeID = @CouponTypeID) 
	    begin
        insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, FirstCouponNumber, EndCouponNumber)
	      values(@CouponPickingNumber, @CouponTypeID, '', @CouponQty, 0, '', '') 	
	      set @PickQty = 0  	
	    end	   	    
    END
  	    
    FETCH FROM CUR_CouponOrderForm_D INTO @BrandID, @CouponTypeID, @CouponQty  
  END
  CLOSE CUR_CouponOrderForm_D 
  DEALLOCATE CUR_CouponOrderForm_D 


  -- 更新Ord_CouponOrderForm_H状态到  Ｃ
  --update Ord_CouponOrderForm_H set ApproveStatus = 'C' where CouponOrderFormNumber = @CouponOrderFormNumber  
  -- update coupon的 PickupFlag
  -- @IsEarchCoupon = 1 的， 中 insert 时，直接 update coupon
  if @IsEarchCoupon <> 1
  begin
    DECLARE CUR_PickupCoupon_Detail CURSOR fast_forward for
      select FirstCouponNumber, EndCouponNumber, CouponTypeID from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber
	  OPEN CUR_PickupCoupon_Detail
    FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
    WHILE @@FETCH_STATUS=0 
    BEGIN 
      update coupon set PickupFlag = 1, StockStatus = 4 where CouponNumber >= @FirstCoupon and  CouponNumber <= @EndCoupon and CouponTypeID = @CouponTypeID
	    FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
	  END
    CLOSE CUR_PickupCoupon_Detail 
    DEALLOCATE CUR_PickupCoupon_Detail 
  end
                
  return 0
end

GO
