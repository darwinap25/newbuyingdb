USE [NewBuying]
GO
/****** Object:  Table [dbo].[Age]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Age](
	[AgeID] [int] IDENTITY(1,1) NOT NULL,
	[AgeCode] [varchar](64) NULL,
	[AgeName1] [varchar](512) NULL,
	[AgeName2] [varchar](512) NULL,
	[AgeName3] [varchar](512) NULL,
 CONSTRAINT [PK_AGE] PRIMARY KEY CLUSTERED 
(
	[AgeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Age', @level2type=N'COLUMN',@level2name=N'AgeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年龄编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Age', @level2type=N'COLUMN',@level2name=N'AgeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Age', @level2type=N'COLUMN',@level2name=N'AgeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Age', @level2type=N'COLUMN',@level2name=N'AgeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Age', @level2type=N'COLUMN',@level2name=N'AgeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年龄。  for Bauhaus
@2016-08-02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Age'
GO
