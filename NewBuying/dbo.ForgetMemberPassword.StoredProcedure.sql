USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ForgetMemberPassword]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[ForgetMemberPassword]
  @UserID            int, --  操作员ID
  @MemberRegisterMobile    nvarchar(512),  --  会员注册ID （Member表MemberRegisterMobile） 
  @MemberMobilePhone       nvarchar(512),  --  会员手机号  （默认Member表的MemberMobilePhone，可根据实际情况改）
  @MemberEmail             nvarchar(512),  --  会员注册的邮箱 （默认Member表的MemberEmail，可根据实际情况改）
  @VerifyType              int,            --  验证方式。 0：DB产生新密码。 1：DB产生VerifyCode。（verifycode 有时效性，需要用户在时间内完成reset PWD）
  @SendMessageType         int,            -- 发送消息方式。0：默认方式（通过会员SVA中设定的账号发送）。1：指定发送渠道。2：指定发送渠道和账号
  @NewPassword             varchar(512) output, --  会员新密码。@VerifyType = 0 时有效
  @NewVerifyCode           varchar(512) output, --  会员的校验码。@VerifyType = 1 时有效
  @AssignedServiceID       int output,     -- 指定发送消息的ServiceID，@SendMessageType= 1,2 时有效。ID 参照messageserviceID内容
  @AssignedMessageAccount  varchar(512) output,    -- 指定发送消息的AccountNumber，@SendMessageType=2 时有效
  @ReturnXMLStr            varchar(max) output -- 发消息需要的XML字符串
AS
/****************************************************************************
**  Name : ForgetMemberPassword  会员密码遗忘 (按照要求把LostMemberPassword_New 更名为 ForgetMemberPassword)
**  Version: 1.0.0.2
**  Description : 会员密码遗失重置。注册账号，手机，邮箱，只需一个即可。
**  Parameter :
  declare @A int, @NewPassword varchar(512), @NewVerifyCode varchar(512), @SendMessageType int, 
    @AssignedServiceID int, @AssignedMessageAccount varchar(512), @ReturnXMLStr varchar(max),
    @AssignedMessageAccountOut varchar(64)
  set @SendMessageType = 2
  set @AssignedServiceID = 2  
  set @AssignedMessageAccountOut = '234243543565' 
  exec @A = ForgetMemberPassword 1,'','','Frank.Feng@tap-group.com.cn',1,@SendMessageType, @NewPassword output, @NewVerifyCode output, 
    @AssignedServiceID output, @AssignedMessageAccountOut output, @ReturnXMLStr output
  Print @a
  print @NewPassword
  print @NewVerifyCode
  print @AssignedServiceID
  print @AssignedMessageAccountOut
  print @ReturnXMLStr
 select * from member 
 select * from membermessageaccount  where MemberID = 680 and IsPrefer = 1 and MessageServiceTypeID =1

**  Created by: Gavin @2014-07-08
**  Modified by: Gavin @2014-08-05 (ver 1.0.0.1) 如果Card状态是frozen（status=9），则返回errorCode： -205
**  Modified by: Gavin @2015-02-12 (ver 1.0.0.2) 返回的JSON内容中,增加会员的默认语言
****************************************************************************/
begin
 
  declare @MemberPassword varchar(512), @PWDValidDays int, @PWDValidDaysUnit int, @ResetPWDDays int, @NewExpiryDate datetime,
         @Email nvarchar(512), @MemberID int, @PWD varchar(512)
  declare @MemberIDStr varchar(64), @MessageID int, @ResetPWDCount int, @AccountNumber nvarchar(512),
          @CountryCode	varchar(64), @PWDMinLength  int, @RandNum decimal(20, 0) 
  declare @InputString varchar(max), @Tempstr varchar(512), @MessageServiceTypeID int, @CardStatus int
  declare @MemberDefLanguage int, @LanguageAbbr varchar(20)

  set @MemberRegisterMobile = isnull(@MemberRegisterMobile, '')
  set @MemberMobilePhone = isnull(@MemberMobilePhone, '')
  set @MemberEmail = isnull(@MemberEmail, '')
  set @VerifyType = isnull(@VerifyType, 0)
  set @SendMessageType = isnull(@SendMessageType, 0)   
  
  select @MemberID = memberID, @MemberPassword = MemberPassword, @Email = isnull(MemberEmail, ''), 
    @ResetPWDCount = isnull(ResetPWDCount, 0), @CountryCode = isnull(CountryCode, '') --, = isnull(MemberMobilePhone, '')
    , @MemberDefLanguage = MemberDefLanguage
    from member 
    where (MemberRegisterMobile = @MemberRegisterMobile and @MemberRegisterMobile <> '') 
       or (MemberMobilePhone = @MemberMobilePhone and @MemberMobilePhone <> '') 
       or (MemberEmail = @MemberEmail and @MemberEmail <> '')     
  if @@ROWCOUNT = 0 
    return -1 

  select @LanguageAbbr = LanguageAbbr from LanguageMap where KeyID = ISNULL(@MemberDefLanguage, 1)
  
  -- ver 1.0.0.1  只能拿第一个card
  select Top 1 @CardStatus = Status from Card where MemberID = @MemberID
  if @CardStatus = 9
    return -205  --  Card已经frozen，不能做forget

  if @VerifyType = 1        -- 产生校验码
  begin
    exec GenRandNumber 6, @NewVerifyCode output
    Update Member set ResetPWDVerCode = @NewVerifyCode,  ResetPWDVerCodeTime = GETDATE(),
           UpdatedBy = @UserID, UpdatedOn = Getdate()
        where memberID = @MemberID  
        
    set @Tempstr = 'MVERIFYCODE=' + @NewVerifyCode           
  end
  else if @VerifyType = 0   -- 产生新密码
  begin               
    select Top 1 @PWDValidDays = PWDValidDays, @PWDValidDaysUnit = PWDValidDaysUnit, @ResetPWDDays = ResetPWDDays, @PWDMinLength = PWDMinLength 
      from PasswordRule where MemberPWDRule = 1
    set @PWDMinLength = isnull(@PWDMinLength, 6)
    if @PWDMinLength = 0 
      set @PWDMinLength = 6    
     set @NewExpiryDate = convert(varchar(10), getdate(), 120)     
     if @PWDValidDaysUnit = 0
       set @NewExpiryDate = '2100-01-01'
     if @PWDValidDaysUnit = 1
       set @NewExpiryDate = DATEADD(yy, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 2
       set @NewExpiryDate = DATEADD(mm, @PWDValidDays, @NewExpiryDate)   
     if @PWDValidDaysUnit = 3
       set @NewExpiryDate = DATEADD(ww, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 4
       set @NewExpiryDate = DATEADD(dd, @PWDValidDays, @NewExpiryDate)  
    select @RandNum = round(rand() * 100000000000000000, 0)
    set @NewPassword = right('00000000000000000' + convert(varchar(20), @RandNum), @PWDMinLength)    
    set @PWD = @NewPassword   
    if isnull(@PWD, '') <> ''
       set @PWD = dbo.EncryptMD5(@PWD)    
    Update Member set MemberPassword = @PWD,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
           UpdatedBy = @UserID, UpdatedOn = Getdate(), ResetPWDCount = @ResetPWDCount + 1
        where memberID = @MemberID   
        
    set @Tempstr = 'MPASSWORD=' + @NewPassword                 
  end 
  -- 产生消息记录
  set @InputString = ''
  if @SendMessageType = 0           -- 0：默认方式
  begin
    DECLARE CUR_Lost CURSOR fast_forward FOR
      select MessageServiceTypeID, AccountNumber from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1  
    OPEN CUR_Lost
    FETCH FROM CUR_Lost INTO @MessageServiceTypeID, @AccountNumber
    WHILE @@FETCH_STATUS=0
    BEGIN    
	  if isnull(@AccountNumber, '') <> '' 
	  begin
	    if @InputString <> ''
	      set @InputString = @InputString + '|' 
	    set @InputString = @InputString + @Tempstr
	       + ';LANGUAGE=' + @LanguageAbbr
	       + ';MEMBERID=' + cast(@MemberID as varchar)	
     	   + ';MSGACCOUNT=' + @AccountNumber + ';MSGACCOUNTTYPE=' + cast(@MessageServiceTypeID as varchar)	
     	set @AssignedServiceID = @MessageServiceTypeID   
     	set @AssignedMessageAccount = @AccountNumber   
	  end
      FETCH FROM CUR_Lost INTO @MessageServiceTypeID, @AccountNumber   
    END
    CLOSE CUR_Lost 
    DEALLOCATE CUR_Lost   
 
  end   
  else if @SendMessageType = 1      -- 1：指定发送渠道。
  begin
    select top 1 @AssignedServiceID = MessageServiceTypeID, @AssignedMessageAccount = AccountNumber
      from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1 and MessageServiceTypeID = @AssignedServiceID   
	set @InputString = @Tempstr
	       + ';LANGUAGE=' + @LanguageAbbr
	       + ';MEMBERID=' + cast(@MemberID as varchar)	
     	   + ';MSGACCOUNT=' + isnull(@AssignedMessageAccount,'') + ';MSGACCOUNTTYPE=' + cast(@AssignedServiceID as varchar)  
  end
  else if @SendMessageType = 2      -- 2：指定发送渠道和账号
  begin
	set @InputString = @Tempstr
	       + ';LANGUAGE=' + @LanguageAbbr
	       + ';MEMBERID=' + cast(@MemberID as varchar)	
     	   + ';MSGACCOUNT=' + @AssignedMessageAccount + ';MSGACCOUNTTYPE=' + cast(@AssignedServiceID as varchar)   
  end
   
  exec GenMessageJSONStr 'ForgetMemberPassword', @InputString, @ReturnXMLstr output      

  return 0
end

GO
