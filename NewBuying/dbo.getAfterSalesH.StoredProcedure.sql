USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[getAfterSalesH]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAfterSalesH]
  @MemberID				int,              -- 会员ID
  @CardNumber			varchar(64),       -- 交会员卡号
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数
  @ConditionStr			nvarchar(400)	   -- 查询自定义条件   
AS
/****************************************************************************
**  Name : [GetAfterSalesH]
**  Version: 1.0.0.1   
**  Description : 根据会员号，会员卡号获得此交易的AfterSalesH] 数据。
**  Created by: Lisa @2016-01-07
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @OrderCondition varchar(max),  @Language int
  
  set @ConditionStr = isnull(@ConditionStr, '')

  select @Language = MemberDefLanguage from member where MemberID = @MemberID
  set @Language = ISNULL(@Language, 1)


  set @SQLStr = 'select '
    + ' TxnNo,TxnType,case TxnType when 1 then ''维修'' when 2 then ''退货'' else ''换货'' end TxnTypeName,RefTransNum,StoreCode, RegisterCode,BusDate, TxnDate,  '
	+'  CashierID, SalesManID, TotalAmount,H.Status,InvalidateFlag,case H.Status when 0 then ''初建状态'' when 1 then ''已经批核'' when 2 then ''用户已经发出快递'' when 3 then ''退货已收到'' when 4 then ''换新货品已发出'' '
	+'  when 5 then ''售后单流程完成'' when 6 then ''拒绝'' else ''取消'' end as StatusName ,'
	+ ' case InvalidateFlag when 0 then ''订单有效'' when 1 then ''已经被Void'' when 2 then ''已经被refund'' else ''已经被exchange'' end as InvalidateFlagName,'
	+'  MemberSalesFlag,MemberID,CardNumber,DeliveryFlag, case DeliveryFlag when 0 then ''自提，不送货'' else ''送货'' end as DeliveryFlagName,'
	+'  DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict, DeliveryAddressDetail, DeliveryFullAddress,DeliveryNumber,H.LogisticsProviderID,'
	+'  DeliveryDate,DeliveryBy,H.Contact,H.ContactPhone,PickupType,case PickupType when 1 then ''用叫快递送'' else ''店铺上门取货'' end as PickupTypeName,PickupStoreCode,'
	+ ' case ' + CAST(@Language as varchar) + ' when 2 then L.ProviderName2 when 3 then L.ProviderName3 else L.ProviderName1 end as ProviderName, '
	+ ' L.OrdQueryAddr + ''?type='' + L.LogisticsProviderCode + ''&postid='' + H.DeliveryNumber as QueryURL'
    + ' from AfterSales_H H '
	+ ' left join LogisticsProvider L on H.LogisticsProviderID = L.LogisticsProviderID '
    + ' where '
    + ' (MemberID = ' + convert(varchar(10), @MemberID) + ' or isnull(' + convert(varchar(10), @MemberID) + ', 0) = 0) and '
    + ' (CardNumber = ''' + @CardNumber + ''' or isnull(''' + @CardNumber + ''', '''') = '''')'
  set @OrderCondition=' order by TxnDate Desc '
  exec SelectDataInBatchs @SQLStr, 'TxnNo', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr         
  return 0  
end

GO
