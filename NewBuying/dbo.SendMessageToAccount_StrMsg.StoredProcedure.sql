USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SendMessageToAccount_StrMsg]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SendMessageToAccount_StrMsg]
  @UserID               int,
  @SendMemberID			int,  
  @MessageServiceTypeID int,
  @MessageAccount       nvarchar(512),  
  @MessageType          int,
  @MessagePriority      int,
  @MessageCoding        int,
  @MessageTitle         nvarchar(512),
  @MessageBody          nvarchar(Max),   
  @IsInternal           int,      
  @ReceiveList			varchar(max),  -- 接收者的MemberID 列表. 格式：12,14,16,103
  @MessageID            int output
AS
/****************************************************************************
**  Name : SendMessageToAccount_StrMsg
**  Version: 1.0.0.1
**  Description : 发送消息到指定的账号  (发送的消息内容为纯文本)
**
**  Parameter :
**  Created by: Gavin @2013-06-26
**
****************************************************************************/
begin  
  declare @MessageBodyBin varbinary(max)      
  set @MessageBodyBin = cast(@MessageBody as varbinary(max))
  
  exec SendMessageToAccount @UserID, @SendMemberID, @MessageServiceTypeID, @MessageAccount, @MessageType, 
          @MessagePriority, @MessageCoding,@MessageTitle,  @MessageBodyBin, @IsInternal, @ReceiveList, @MessageID output 
  return 0
end

GO
