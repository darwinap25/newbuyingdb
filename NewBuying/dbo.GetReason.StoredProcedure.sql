USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetReason]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetReason]
  @ReasonID			 int,      -- 指定的ReasonID
  @LanguageAbbr			varchar(20)=''	   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1  
AS
/****************************************************************************
**  Name : GetReason  
**  Version: 1.0.0.2
**  Description : 获得指定的Reason信息
**  Parameter :
**
**  Created by: Gavin @2012-06-28

**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @StateofCoupon int, @CouponRealNumber varchar(512) --, @ConditionStr nvarchar(400)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @ReasonID = isnull(@ReasonID, 0)
  select ReasonID, ReasonCode, 
      case when @Language = 2 then ReasonDesc2 when @Language = 3 then ReasonDesc3 else ReasonDesc1 end as ReasonDesc    
   from reason
   where  ReasonID = @ReasonID or @ReasonID = 0

  return 0
end

GO
