USE [NewBuying]
GO
/****** Object:  Table [dbo].[Store_Attribute]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Store_Attribute](
	[SAID] [int] IDENTITY(1,1) NOT NULL,
	[SACode] [varchar](64) NULL,
	[SADesc1] [varchar](512) NULL,
	[SADesc2] [varchar](512) NULL,
	[SADesc3] [varchar](512) NULL,
	[SAPic] [varchar](512) NULL,
 CONSTRAINT [PK_STORE_ATTRIBUTE] PRIMARY KEY CLUSTERED 
(
	[SAID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store_Attribute', @level2type=N'COLUMN',@level2name=N'SAID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺属性编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store_Attribute', @level2type=N'COLUMN',@level2name=N'SACode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store_Attribute', @level2type=N'COLUMN',@level2name=N'SADesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store_Attribute', @level2type=N'COLUMN',@level2name=N'SADesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store_Attribute', @level2type=N'COLUMN',@level2name=N'SADesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺属性图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store_Attribute', @level2type=N'COLUMN',@level2name=N'SAPic'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺属性表基础。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store_Attribute'
GO
