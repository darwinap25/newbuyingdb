USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportMemMsgDispense_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportMemMsgDispense_H](
	[MemberMsgDispenseNumber] [varchar](64) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[Note] [nvarchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_IMPORTMEMMSGDISPENSE_H] PRIMARY KEY CLUSTERED 
(
	[MemberMsgDispenseNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_ImportMemMsgDispense_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_ImportMemMsgDispense_H] ON [dbo].[Ord_ImportMemMsgDispense_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_ImportMemMsgDispense_H
* Version: 1.0.0.4
* Description : 批核时产生消息写入MessageObject表。等待消息中心发送。
*  
select * from Ord_ImportMemMsgDispense_H
select * from Ord_ImportMemMsgDispense_D
update Ord_ImportMemMsgDispense_H set approvestatus = 'A' where MemberMsgDispenseNumber = 'MMD000000000042'
select * from messageobject order by messageid desc  --16782
select * from messageobject where messageid > 16821
select * from messagereceivelist where messageid > 16821
* Create By Gavin @2016-05-18
* Modify By Gavin @2016-06-01 (ver 1.0.0.1) Account为空的记录不发消息
* Modify By Gavin @2016-06-03 (ver 1.0.0.2) 写receivelist时也需要按照 memberdefaultlanguage 过滤
* Modify By Gavin @2016-12-14 (ver 1.0.0.3) 加快速度。INSERT INTO @TempTable 分开操作
* Modify By Gavin @2017-08-24 (ver 1.0.0.4) 写入MessageReceiveList 时，需要加上@MessageTemplateCode 的条件。否则回重复。
*/
/*==============================================================*/
BEGIN
  DECLARE @MemberMsgDispenseNumber VARCHAR(64),@Description nvarchar(512),@Note nvarchar(512),@ApprovalCode VARCHAR(64),@ApproveStatus CHAR(1),@ApproveBy INT,@UpdatedBy INT
  DECLARE @OldApproveStatus CHAR(1), @MessageTemplateCode VARCHAR(64), @MemberDefLanguage INT,  @MessageServiceTypeID INT, @MDL INT
  DECLARE @MessageTitle nvarchar(512), @TemplateContent nvarchar(max), @MessageID INT
  DECLARE @TempTable table(MessageTemplateCode VARCHAR(64), MemberDefLanguage INT)

  DECLARE CUR_ImportMemMsgDispense CURSOR fast_forward local FOR
    SELECT MemberMsgDispenseNumber,Description,Note,ApprovalCode,ApproveStatus,ApproveBy,UpdatedBy FROM INSERTED
  OPEN CUR_ImportMemMsgDispense
  FETCH FROM CUR_ImportMemMsgDispense INTO @MemberMsgDispenseNumber,@Description,@Note,@ApprovalCode,@ApproveStatus,@ApproveBy,@UpdatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where MemberMsgDispenseNumber = @MemberMsgDispenseNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus) 
    begin
      exec GenApprovalCode @ApprovalCode output

	  delete from @TempTable
/*
	  INSERT INTO @TempTable(MessageTemplateCode, MemberDefLanguage)
	  select D.MessageTemplateCode, M.MemberDefLanguage from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberRegisterMobile,'') = M.MemberRegisterMobile 
		                         or (isnull(D.MemberPhoneNumber, '') <> '' and D.MemberPhoneNumber = M.MemberMobilePhone) 
								 or (isnull(D.MemberEmail, '') <> '' and D.MemberEmail = M.MemberEmail)
		where M.MemberID is not null and  D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
		group by D.MessageTemplateCode, M.MemberDefLanguage
*/
	  INSERT INTO @TempTable(MessageTemplateCode, MemberDefLanguage)
	  select D.MessageTemplateCode, M.MemberDefLanguage from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberRegisterMobile,'') <> '' and isnull(D.MemberRegisterMobile,'') = M.MemberRegisterMobile 	                         
		where M.MemberID is not null and  D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
		group by D.MessageTemplateCode, M.MemberDefLanguage

	  INSERT INTO @TempTable(MessageTemplateCode, MemberDefLanguage)
	  select D.MessageTemplateCode, M.MemberDefLanguage from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberPhoneNumber,'') <> '' and isnull(D.MemberPhoneNumber,'') = M.MemberMobilePhone 	
		                         and isnull(D.MemberRegisterMobile,'') = ''                         
		where M.MemberID is not null and  D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
		group by D.MessageTemplateCode, M.MemberDefLanguage

	  INSERT INTO @TempTable(MessageTemplateCode, MemberDefLanguage)
	  select D.MessageTemplateCode, M.MemberDefLanguage from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberEmail,'') <> '' and isnull(D.MemberEmail,'') = M.MemberEmail 	
		                         and isnull(D.MemberRegisterMobile,'') = ''
								 and isnull(D.MemberPhoneNumber,'') = ''                           
		where M.MemberID is not null and  D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
		group by D.MessageTemplateCode, M.MemberDefLanguage


	  DECLARE CUR_ImportMemMsgDispense_D CURSOR fast_forward local FOR
		select C.MessageTemplateCode, A.MessageServiceTypeID, 
		  case C.MemberDefLanguage when 2 then A.MessageTitle2 when 3 then A.MessageTitle3 else A.MessageTitle1 end as MessageTitle,
		  case C.MemberDefLanguage when 2 then A.TemplateContent2 when 3 then A.TemplateContent3 else A.TemplateContent1 end as TemplateContent,
		  C.MemberDefLanguage
		  from MessageTemplateDetail A 
		  left join MessageTemplate B on A.MessageTemplateID = B.MessageTemplateID
		  left join @TempTable C on B.MessageTemplateCode = C.MessageTemplateCode
        where C.MessageTemplateCode is not null
	  OPEN CUR_ImportMemMsgDispense_D
	  FETCH FROM CUR_ImportMemMsgDispense_D INTO @MessageTemplateCode, @MessageServiceTypeID, @MessageTitle, @TemplateContent, @MDL
	  WHILE @@FETCH_STATUS=0
	  BEGIN
		insert into MessageObject(MessageServiceTypeID, MessageType, MessagePriority, MessageCoding, MessageTitle, 
		   MessageBody, FromMemberID, IsInternal, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ResponseCode)
		values (@MessageServiceTypeID, 1, 1, 1, @MessageTitle, 
		   cast(@TemplateContent as varbinary(max)), 1, 0, 0, Getdate(), 1, Getdate(), 1, 0)         
		set @MessageID = SCOPE_IDENTITY()
/*
		insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy) 
	    select @MessageID, M.MemberID, S.AccountNumber, 0, 0, Getdate(), 1  from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberRegisterMobile,'') = M.MemberRegisterMobile 
		                         or (isnull(D.MemberPhoneNumber, '') <> '' and D.MemberPhoneNumber = M.MemberMobilePhone) 
								 or (isnull(D.MemberEmail, '') <> '' and D.MemberEmail = M.MemberEmail)
           left join (select * from MemberMessageAccount where MessageServiceTypeID = @MessageServiceTypeID) S on M.MemberID = S.MemberID
		  where M.MemberID is not null and S.MemberID is not null and D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
	         and ISNULL(S.AccountNumber,'') <> '' and M.MemberDefLanguage = @MDL and D.MessageTemplateCode = @MessageTemplateCode
*/
		insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy) 
	    select @MessageID, M.MemberID, S.AccountNumber, 0, 0, Getdate(), 1  from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberRegisterMobile,'') <> '' and isnull(D.MemberRegisterMobile,'') = M.MemberRegisterMobile 
           left join (select * from MemberMessageAccount where MessageServiceTypeID = @MessageServiceTypeID) S on M.MemberID = S.MemberID
		  where M.MemberID is not null and S.MemberID is not null and D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
	         and ISNULL(S.AccountNumber,'') <> '' and M.MemberDefLanguage = @MDL and D.MessageTemplateCode = @MessageTemplateCode

		insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy) 
	    select @MessageID, M.MemberID, S.AccountNumber, 0, 0, Getdate(), 1  from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberPhoneNumber,'') <> '' and isnull(D.MemberPhoneNumber,'') = M.MemberMobilePhone 	
		                         and isnull(D.MemberRegisterMobile,'') = ''  
           left join (select * from MemberMessageAccount where MessageServiceTypeID = @MessageServiceTypeID) S on M.MemberID = S.MemberID
		  where M.MemberID is not null and S.MemberID is not null and D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
	         and ISNULL(S.AccountNumber,'') <> '' and M.MemberDefLanguage = @MDL and D.MessageTemplateCode = @MessageTemplateCode

		insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy) 
	    select @MessageID, M.MemberID, S.AccountNumber, 0, 0, Getdate(), 1  from Ord_ImportMemMsgDispense_D D 
		   left join Member M on isnull(D.MemberEmail,'') <> '' and isnull(D.MemberEmail,'') = M.MemberEmail 	
		                         and isnull(D.MemberRegisterMobile,'') = ''
								 and isnull(D.MemberPhoneNumber,'') = ''    
           left join (select * from MemberMessageAccount where MessageServiceTypeID = @MessageServiceTypeID) S on M.MemberID = S.MemberID
		  where M.MemberID is not null and S.MemberID is not null and D.MemberMsgDispenseNumber = @MemberMsgDispenseNumber
	         and ISNULL(S.AccountNumber,'') <> '' and M.MemberDefLanguage = @MDL and D.MessageTemplateCode = @MessageTemplateCode

		FETCH FROM CUR_ImportMemMsgDispense_D INTO @MessageTemplateCode, @MessageServiceTypeID, @MessageTitle, @TemplateContent,@MDL 
	  END
	  CLOSE CUR_ImportMemMsgDispense_D 
	  DEALLOCATE CUR_ImportMemMsgDispense_D 		          

      update Ord_ImportMemMsgDispense_H set ApproveOn=Getdate(), ApprovalCode = @ApprovalCode, UpdatedOn = GETDATE()
	    where MemberMsgDispenseNumber = @MemberMsgDispenseNumber
    end  
    FETCH FROM CUR_ImportMemMsgDispense INTO @MemberMsgDispenseNumber,@Description,@Note,@ApprovalCode,@ApproveStatus,@ApproveBy,@UpdatedBy
  END
  CLOSE CUR_ImportMemMsgDispense 
  DEALLOCATE CUR_ImportMemMsgDispense   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_H', @level2type=N'COLUMN',@level2name=N'MemberMsgDispenseNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_H', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'@2016-05-12 分发消息给指定会员的单据表。（主表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_H'
GO
