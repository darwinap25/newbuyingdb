USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PROMO_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PROMO_H](
	[PPriceCode] [varchar](64) NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[StoreGroupCode] [varchar](64) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CurrencyCode] [varchar](64) NULL,
	[Note] [nvarchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_PROMO_H] PRIMARY KEY CLUSTERED 
(
	[PPriceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_BUY_PROMO_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_BUY_PROMO_H] ON [dbo].[BUY_PROMO_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_BUY_PROMO_H
* Version: 1.0.0.0
* Description : BUY_PROMO_H的批核触发器

  select * from BUY_PROMO_H
** Create By Gavin @2017-06-01
*/
/*==============================================================*/
BEGIN  
  DECLARE @PPriceCode VARCHAR(64), @ApproveStatus CHAR(1), @CreatedBy INT, @OldApproveStatus CHAR(1), @ApprovalCode CHAR(6)
  
  DECLARE CUR_BUY_PPRICE_H CURSOR FAST_FORWARD FOR
    SELECT PPriceCode, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_BUY_PPRICE_H
  FETCH FROM CUR_BUY_PPRICE_H INTO @PPriceCode, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    SELECT @OldApproveStatus = ApproveStatus FROM Deleted WHERE PPriceCode = @PPriceCode
    IF (@OldApproveStatus = 'P' OR ISNULL(@OldApproveStatus, '') = '') AND @ApproveStatus = 'A' AND UPDATE(ApproveStatus)
    BEGIN
      EXEC GenApprovalCode @ApprovalCode OUTPUT  
	    
	  INSERT INTO BUY_PROMO_M(PPriceCode,ProdCode,RPriceTypeCode,Price,
	    StoreCode,StoreGroupCode,StartDate,EndDate,Status,ApproveOn,ApproveBy,UpdatedOn,UpdatedBy) 
      SELECT D.PPriceCode,D.ProdCode,D.RPriceTypeCode,D.Price,
	    H.StoreCode,H.StoreGroupCode,H.StartDate,H.EndDate,1,GETDATE(),@CreatedBy,GETDATE(),@CreatedBy
	    FROM BUY_PROMO_D D LEFT JOIN BUY_PROMO_H H ON D.PPriceCode = H.PPriceCode
	  WHERE D.PPriceCode = @PPriceCode

      UPDATE BUY_PROMO_H SET ApprovalCode = @ApprovalCode, ApproveOn = GETDATE(), ApproveBy = @CreatedBy,
	      UpdatedOn = GETDATE(), UpdatedBy = @CreatedBy
	  WHERE PPriceCode = @PPriceCode
    END      
    FETCH FROM CUR_BUY_PPRICE_H INTO @PPriceCode, @ApproveStatus, @CreatedBy  
  END
  CLOSE CUR_BUY_PPRICE_H 
  DEALLOCATE CUR_BUY_PPRICE_H     
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'PPriceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺Code。空/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。空/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'StoreGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码。一般应该是本币代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'CurrencyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销设置主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PROMO_H'
GO
