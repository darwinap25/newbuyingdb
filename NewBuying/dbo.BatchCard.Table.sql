USE [NewBuying]
GO
/****** Object:  Table [dbo].[BatchCard]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BatchCard](
	[BatchCardID] [int] IDENTITY(1,1) NOT NULL,
	[BatchCardCode] [varchar](64) NOT NULL,
	[SeqFrom] [bigint] NULL,
	[SeqTo] [bigint] NULL,
	[Qty] [int] NOT NULL DEFAULT ((1)),
	[CardGradeID] [int] NOT NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BatchCard] PRIMARY KEY CLUSTERED 
(
	[BatchCardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCard', @level2type=N'COLUMN',@level2name=N'BatchCardID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次ID。 组成规则： CardTypeID（或CouponTypeID） + 序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCard', @level2type=N'COLUMN',@level2name=N'BatchCardCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号范围（开始序号）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCard', @level2type=N'COLUMN',@level2name=N'SeqFrom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号范围（结束序号）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCard', @level2type=N'COLUMN',@level2name=N'SeqTo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次创建数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCard', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID， 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCard', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCard'
GO
