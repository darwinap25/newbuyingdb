USE [NewBuying]
GO
/****** Object:  Table [dbo].[Accounts_UsersRelation]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts_UsersRelation](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[SubUserID] [int] NOT NULL,
 CONSTRAINT [PK_ACCOUNTS_USERSRELATION] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts_UsersRelation', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts_Users 表主键。（Master UserID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts_UsersRelation', @level2type=N'COLUMN',@level2name=N'UserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'下属userid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts_UsersRelation', @level2type=N'COLUMN',@level2name=N'SubUserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户关系表。 只保存上下级关系。  
使用时，只需要使用一级关系。
注： 在表结构中只有上下级关系，没有防止循环。 维护数据时，需要递归检查是否循环。最多递归检查五层。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts_UsersRelation'
GO
