USE [NewBuying]
GO
/****** Object:  Table [dbo].[Brand]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Brand](
	[StoreBrandID] [int] IDENTITY(1,1) NOT NULL,
	[StoreBrandCode] [varchar](64) NOT NULL,
	[StoreBrandName1] [nvarchar](512) NULL,
	[StoreBrandName2] [nvarchar](512) NULL,
	[StoreBrandName3] [nvarchar](512) NULL,
	[StoreBrandDesc1] [nvarchar](max) NULL,
	[StoreBrandDesc2] [nvarchar](max) NULL,
	[StoreBrandDesc3] [nvarchar](max) NULL,
	[StoreBrandPicSFile] [nvarchar](512) NULL,
	[StoreBrandPicMFile] [nvarchar](512) NULL,
	[StoreBrandPicGFile] [nvarchar](512) NULL,
	[CardIssuerID] [int] NULL,
	[IndustryID] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[BrandCode] [varchar](64) NULL,
	[BrandName1] [nvarchar](512) NULL,
	[BrandName2] [nvarchar](512) NULL,
	[BrandName3] [nvarchar](512) NULL,
 CONSTRAINT [PK_BRAND] PRIMARY KEY CLUSTERED 
(
	[StoreBrandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌小图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandPicSFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌中图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandPicMFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌大图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'StoreBrandPicGFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌发行方ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'CardIssuerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand', @level2type=N'COLUMN',@level2name=N'IndustryID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌表。（运营商的品牌）
@2016-08-09 添加' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand'
GO
