USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberTxnList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[GetMemberTxnList]   

  @MemberID				int,				  -- member 表主键

  @CardNumber			varchar(64),         -- 或者根据卡号查找。过滤条件

  @Storecode            varchar(64),         -- 店铺条件

  @StartBusDate         datetime,			  -- 交易日期，开始范围，过滤条件

  @EndBusDate			datetime,			  -- 交易日期，结束范围，过滤条件

  @MinTotalAmount		money,				  -- 最小交易金额，过滤条件

  @MaxTotalAmount		money,				  -- 最小交易金额，过滤条件

  @TxnNo				varchar(64),          -- 交易单号

  @SalesType            int,                  -- 交易单类型

  @TxnStatus            varchar(512),         -- 交易单状态，多个状态之间用 , 隔开

  @ConditionStr			nvarchar(400),		  -- 查询自定义条件

  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1

  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0

  @PageCount			int=0 output,	   -- 返回总页数。

  @RecordCount			int=0 output,	   -- 返回总记录数

  @TotalAmount			money output       -- 返回交易总金额   

AS

/****************************************************************************

**  Name : GetMemberTxnList

**  Version: 1.0.0.18

**  Description : 获得会员交易的单号

**  Parameter :

declare @PageCount int, @RecordCount int, @TotalAmount money, @a int

exec @a = GetMemberTxnList 0,'','','','',0.0,0.0, 'KTXNNO000003101', -1, '', '', 0, 99, @PageCount output, @RecordCount output, @TotalAmount output

print @a

print @PageCount

print @RecordCount

1037,'01001035','','','',0,0,'',-1,0,'',0,99,@p17 output,@p18 output,0

select   StoreCode, RegisterCode, TxnNo, BusDate, TxnDate, SalesType, CashierID, SalesManID, TotalAmount, 		 H.Status, DeliverBy, DeliveryAddress, DeliverStartOn, DeliverEndOn, SalesReceipt, H.Contact, H.ContactPhone, 		 MemberID, MemberName, MemberMobile
Phone, CardNumber, MemberAddress,   case when H.[status] in (6, 9) then D.Longitude else H.DeliveryLongitude end as Longitude ,   case when H.[status] in (6, 9) then D.latitude else H.DeliveryLatitude end as latitude  from sales_h H  left join CourierPosi
tion D on H.DeliverBy = D.CourierCode  where   (MemberID = 1 or isnull(1, 0) = 0) and   (CardNumber = '' or isnull('', '') = '') and   (Storecode = '' or isnull('', '') = '') and   (BusDate >= 1900-01-01 and   (BusDate <= 2028-11-17 and   (TotalAmount >= 
0.00 and   (TotalAmount <= 0.00 or 0.00 = 0)  

order by Txndate desc 

select top 10 * from sales_H where txnno = '2016927Bauhaus214201000400'

select * from sales_h  where cardnumber = '1111279'

  select * from card where memberid = 599

**  Created by: Gavin @2012-05-28

**  Modify by Gavin @2012-09-24 (ver 1.0.0.1) 增加参数@TxnNo，作为过滤参数。

**  Modify by Gavin @2012-10-17 (ver 1.0.0.2) 增加参数@SalesType，作为过滤参数。

**  Modify by Gavin @2012-10-23 (ver 1.0.0.3) 增加参数@TxnStatus，作为过滤参数。多个状态之间用 , 隔开

**  Modify by Gavin @2013-02-06 (ver 1.0.0.4) 增加返回参数SalesTag, additional

**  Modify by Gavin @2013-03-04 (ver 1.0.0.5) 增加参数@ConditionStr

**  Modified by: Gavin @2013-06-21 (Ver 1.0.0.6) 使用1.0.0.6版本的SelectDataInBatchs	,输入的@ConditionStr直接传给SelectDataInBatchs 

**  Modified by: Gavin @2013-08-19 (Ver 1.0.0.7) 修正使用TxnNo作为过滤条件时,返回的TotalAmount统计值和交易不匹配问题

**  Modified by: Gavin @2013-09-04 (Ver 1.0.0.8) 增加返回字段:	DeliveryNumber, LogisticsProviderID

**  Modified by: Gavin @2013-09-11 (Ver 1.0.0.9) 传入@StartBusDate为null时, 初始设置为 0.

**  Modified by: Gavin @2013-12-24 (Ver 1.0.0.10) 增加返回字段:	  PickupType, PickupStoreCode

**  Modified by: Gavin @2015-08-26 (Ver 1.0.0.11) 增加返回字段:	 BillContact, BillContactPhone, BillAddress

**  Modified by: Gavin @2015-09-22 (Ver 1.0.0.12) 允许totalamount为负数的交易. 如果@MinTotalAmount为0的话.

**  Modified by: Gavin @2015-11-17 (Ver 1.0.0.13) 增加 快递公司名称，编号的返回

**  Modified by: Gavin @2015-12-15 (Ver 1.0.0.14) 增加返回字段, 组合快递单查询URL

**  Modified by: Gavin @2016-10-28 (Ver 1.0.0.15) 增加返回字段, PickupStoreID

**  Modified by: Gavin @2017-01-03 (Ver 1.0.0.16) 增加返回字段, MemberEmail,

**  Modified by: Gavin @2017-01-04 (Ver 1.0.0.17) 增加返回字段, RewardPoints, Freight (@TxnNo 不为空时, 才有值, 这个值是另外计算的. 只返回一条记录时才可以用 (for bauhaus))

**  Modified by: Gavin @2017-03-08 (Ver 1.0.0.17) 增加返回字段 NickName 

******************************************************************************/

begin

  declare @SQLStr nvarchar(4000), @OrderCondition varchar(max),  @Language int

  declare @RewardPoints int, @Freight money

  

  if isnull(@EndBusDate, 0) = 0

    set @EndBusDate = Getdate() + 6000

  set @TxnStatus = isnull(@TxnStatus, '')

  if @TxnStatus = ''

    set @TxnStatus = '-1'

  set @MemberID = isnull(@MemberID, 0)

  set @CardNumber = isnull(@CardNumber, '')

  set @Storecode = isnull(@Storecode, '')

  --set @StartBusDate = isnull(@StartBusDate, getdate()-1)

  set @StartBusDate = isnull(@StartBusDate, 0)

  set @MinTotalAmount = isnull(@MinTotalAmount, 0)

  set @MaxTotalAmount = isnull(@MaxTotalAmount, 0)

  set @TxnNo = isnull(@TxnNo, '')

  set @SalesType = isnull(@SalesType, -1)

  set @ConditionStr = isnull(@ConditionStr, '')

  set @RewardPoints = 0

  set @Freight = 0.00



  select @Language = MemberDefLanguage from member where MemberID = @MemberID

  set @Language = ISNULL(@Language, 1)



  if @TxnNo <> ''

  begin

    select @RewardPoints = sum(Points) from Card_Movement where OprID = 27 and RefTxnNo = @TxnNo

	set @RewardPoints = isnull(@RewardPoints, 0)

	select @Freight = sum(NetAmount) from Sales_D where ProdCode = '9999' and TransNum = @TxnNo

	set @Freight = isnull(@Freight, 0.00)

  end

   

  set @SQLStr = 'select '

    + '  H.StoreCode, RegisterCode, TransNum as TxnNo, BusDate, TxnDate, TransType as SalesType, CashierID, SalesManID, TotalAmount, '

	+ '		 H.Status, DeliveryBy DeliverBy, DeliveryFullAddress DeliveryAddress, RequestDeliveryDate DeliverStartOn, DeliveryDate DeliverEndOn, H.Contact, H.ContactPhone, '

	+ '		 H.MemberID, H.MemberName, H.MemberMobilePhone, CardNumber, MemberAddress, '

    + '  case when H.[status] in (6, 9) then D.Longitude else H.DeliveryLongitude end as Longitude , '

    + '  case when H.[status] in (6, 9) then D.latitude else H.DeliveryLatitude end as latitude, '

    + '  H.Additional, H.SalesTag, H.DeliveryNumber, H.LogisticsProviderID, H.PickupType, H.PickupStoreCode, '

    + '  H.BillContact, H.BillContactPhone, H.BillAddress, L.LogisticsProviderCode, L.OrdQueryAddr, '

	+ ' case ' + CAST(@Language as varchar) + ' when 2 then L.ProviderName2 when 3 then L.ProviderName3 else L.ProviderName1 end as ProviderName, '

	+ ' L.OrdQueryAddr + ''?type='' + L.LogisticsProviderCode + ''&postid='' + H.DeliveryNumber as QueryURL, S.StoreID as PickupStoreID'

	+ ' ,M.MemberEmail, ' + cast(@RewardPoints as varchar) + ' as RewardPoints, ' + cast(@Freight as varchar) + ' as Freight '

	+ ' ,M.NickName '

    + ' from sales_h H '

    + ' left join CourierPosition D on H.DeliveryBy = D.CourierCode '

	+ ' left join LogisticsProvider L on H.LogisticsProviderID = L.LogisticsProviderID '

	+ ' left join Store S on H.PickupStoreCode = S.StoreCode '

	+ ' left join Member M on H.MemberID = M.MemberID '

    + ' where '

    + '  (H.MemberID = ' + convert(varchar(10), @MemberID) + ' or isnull(' + convert(varchar(10), @MemberID) + ', 0) = 0) and '

    + '  (H.CardNumber = ''' + @CardNumber + ''' or isnull(''' + @CardNumber + ''', '''') = '''') and '

    + '  (H.Storecode = ''' + @Storecode + ''' or isnull(''' + @Storecode + ''', '''') = '''') and '

    + '  (H.BusDate >= ''' + Convert(varchar(10), isnull(@StartBusDate,0), 120) + ''') and '

    + '  (H.BusDate <= ''' + Convert(varchar(10), @EndBusDate, 120) + ''') and '

    + '  (H.Status in (' + @TxnStatus + ') or ''' + @TxnStatus + ''' = ''-1'') and '

    + '  (H.TotalAmount >= ' + convert(varchar(12), isnull(@MinTotalAmount, 0)) + ' or ' + convert(varchar(12), isnull(@MinTotalAmount, 0)) + ' = 0) and '    

    + '  (H.TotalAmount <= ' + convert(varchar(12), isnull(@MaxTotalAmount, 0)) + ' or ' + convert(varchar(12), isnull(@MaxTotalAmount, 0)) + ' = 0) and '

    + '  (H.TransType = ' + cast(isnull(@SalesType,-1) as varchar) + ' or ' + cast(isnull(@SalesType,-1) as varchar) + ' = -1) '

    + ' and (H.TransNum = ''' + @TxnNo + ''' or isnull(''' + @TxnNo + ''', '''') = '''') '

--  if isnull(@ConditionStr, '') <> ''    

--    set @SQLStr = @SQLStr + ' and (' + @ConditionStr +  ')'   



    select @TotalAmount=sum(TotalAmount) from sales_h H 

    left join CourierPosition D on H.DeliveryBy = D.CourierCode

    where

    (MemberID = convert(varchar(10), @MemberID) or isnull(convert(varchar(10), @MemberID), 0) = 0) and

    (CardNumber = @CardNumber or isnull(@CardNumber,'') = '') and 

    (Storecode = @Storecode or isnull(@Storecode, '') = '') and

    (BusDate >= Convert(varchar(10), isnull(@StartBusDate,0), 120)) and 

    (BusDate <= Convert(varchar(10), @EndBusDate, 120)) and 

    (TotalAmount >= isnull(@MinTotalAmount, 0) or isnull(@MinTotalAmount, 0)= 0) and 

    (TotalAmount <= isnull(@MaxTotalAmount, 0) or isnull(@MaxTotalAmount, 0)= 0) and

    (TransType = isnull(@SalesType,-1) or isnull(@SalesType,-1) = -1)  and

    (H.Status like @TxnStatus or @TxnStatus = '-1')

	and (H.TransNum =  @TxnNo  or isnull(@TxnNo, '') = '')



  set @OrderCondition=' order by TxnDate Desc '

  exec SelectDataInBatchs @SQLStr, 'TransNum', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr         

  return 0  

end

GO
