USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGradeExchangeBinding]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardGradeExchangeBinding](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[BrandID] [int] NULL,
	[ProdCode] [varchar](64) NULL,
	[DepartCode] [varchar](64) NULL,
	[TenderCode] [varchar](64) NULL,
	[BindingType] [int] NOT NULL,
 CONSTRAINT [PK_CARDGRADEEXCHANGEBINDING] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CardGradeExchangeBinding] ADD  DEFAULT ((1)) FOR [BindingType]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS的TenderCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding', @level2type=N'COLUMN',@level2name=N'TenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绑定类型。
1：销售Card的 货号。
2：Card使用条件的货品绑定。
3：Card使用时Tender的设定。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding', @level2type=N'COLUMN',@level2name=N'BindingType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡绑定的兑换列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeExchangeBinding'
GO
