USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCardInfo_P]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberCardInfo_P]  
  @CardNumber			varchar(64),			-- 会员卡号 
  @CardPassword			varchar(512)			-- password（MD5密文）
AS
/****************************************************************************
**  Name : GetMemberCardInfo_P
**  Version: 1.0.0.0
**  Description : 得到指定的会员卡的信息. 返回一条记录 (需要校验密码)
**
**  Parameter :
**         return:  返回数据集   0：成功， -1：@MemberID和@CardNumber，@CardTypeID 联合查询，没有找到记录。
**
select * from card
  declare @A int
  exec @a = GetMemberCardInfo_P '000300049', ''
  print @a
  select * from card where cardnumber = '000300049'
**  Created by: Gavin @2015-12-25
**
****************************************************************************/
begin
  /*Add By Robin 2014-5-30 for getting information by UID*/
  declare @Temp_CardNumber varchar(512), @PWD varchar(512), @Result int
  set @Result = 0
  select @Temp_CardNumber=CardNumber from CardUIDMap where CardUID=@CardNumber
  if @@Rowcount>0 set @CardNumber=@Temp_CardNumber
  /*End*/
  select @PWD = isnull(CardPassword, '') from Card where CardNumber = @CardNumber
  if @@Rowcount = 0
    set @Result = -2  -- return -2
  else if isnull(@PWD,'') <> isnull(@CardPassword, '')
    set @Result = -4  -- return -4

  select top 1 C.CardNumber, C.CardTypeID, Y.CardTypeCode, CardIssueDate, CardExpiryDate, 
    G.CardPointToAmountRate, G.CardAmountToPointRate, C.CardAmountExpiryDate, C.CardPointExpiryDate,
    C.MemberID, C.CardGradeID, C.Status as CardStatus, C.TotalPoints, C.TotalAmount 
    from [card] C      
      left join CardGrade G on C.CardGradeID = G.CardGradeID
      left join CardType Y on C.CardTypeID = Y.CardTypeID 
   where C.CardNumber = @CardNumber and C.Status = 2 and @Result = 0
      
  return @Result
end

GO
