USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetTop10Sales]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetTop10Sales]   
  @StartBusdate         datetime,		   -- 起始日期，查询交易范围。
  @EndBusdate           datetime,		   -- 结束日期，查询交易范围。
  @DepartCode           varchar(64),	   -- 指定部门货品
  @QueryCondition		nvarchar(1000),    -- 自定义查询条件
  @OrderCondition		nvarchar(1000),	   -- 自定义排序条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1      
AS
/****************************************************************************
**  Name : GetTop10Sales    
**  Version: 1.0.0.2
**  Description : 获得销售排名记录
**
  declare @a int ,@PageCount int, @RecordCount int
  exec @a = GetTop10Sales 0,0, '', '', '', 1,0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  
**  Created by: Gavin @2013-07-04
**  Modify by: Gavin @2013-07-12 (ver 1.0.0.1) 增加返回字段 ProdName，DepartName，NetPrice，ProdPicFile 
**  Modify by: Gavin @2013-07-30 (ver 1.0.0.2) 需要加上group by prodcode
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @CurBusdate datetime

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  select Top 1 @CurBusdate = Busdate from sodeod order by CreatedOn desc
  set @StartBusdate = isnull(@StartBusdate, @CurBusdate)
  set @EndBusdate = isnull(@EndBusdate, @CurBusdate)
  set @DepartCode = isnull(@DepartCode, '')
  set @QueryCondition = isnull(@QueryCondition, '')
  set @OrderCondition = isnull(@OrderCondition, '')  
  
  set @SQLStr = 'select R.BusDate, R.ProdCode, R.DepartCode, R.ProductBrandID, R.Qty, '
    + '  case ' + cast(@Language as varchar) + ' when 2 then P.ProdName2 when 3 then P.ProdName3 else P.ProdName1 end as ProdName , '
    + '  case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '
    + ' A.NetPrice, P.ProdPicFile '
    + ' from (select max(busdate) as Busdate, ProdCode, Max(departCode) as DepartCode, max(ProductBrandID) as ProductBrandID, '
    + '          sum(Qty) as Qty from Top10Record '
    + '        where Busdate >= ''' + convert(varchar(10), @StartBusdate, 120) + ''' and Busdate <= ''' + convert(varchar(10), @EndBusdate, 120) + ''' '
    + '          and (DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + ''' = '''')'
    + '        group by prodcode ) R '
    + ' left join Product P on R.ProdCode = P.ProdCode '
    + ' left join (select * from Product_Price where status = 1) A on R.ProdCode = A.ProdCode '
    + ' left join Department D on D.DepartCode = R.DepartCode '      
  
  exec SelectDataInBatchs @SQLStr, 'Qty desc', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @QueryCondition     
  return 0
end

GO
