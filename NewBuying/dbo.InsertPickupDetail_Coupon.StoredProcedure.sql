USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[InsertPickupDetail_Coupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[InsertPickupDetail_Coupon]
  @CouponPickingNumber      varchar(64),     --单号
  @CouponTypeID             int,             --优惠劵类型ID
  @StartCouponNumber        varchar(64),      --起始号码
  @OrderFormQty             int,             --Order单的数量。
  @OrderQty                 int,             --要求的数量。
  @IsEarchCoupon            int=0,            -- 1: 输入每个coupon，即一个coupon一行。 0：连续coupon合并。 默认0
  @damaged                  int,
  @returned                 int
AS
/****************************************************************************
**  Name : InsertPickupDetail_Coupon
**  Version: 1.0.0.10
**  Description : 检查同一单pickup的CouponNUmber数据是否重复
**  example : 判断的前提： coupon表中同一个coupontypeid的记录按照couponnumber排序时，couponnumber是连续的。（可能状态不同）
  declare @a int  , @CouponOrderFormNumber varchar(64)
  set @CouponOrderFormNumber = ''
  exec @a = CheckPickupDetail_Coupon 1, @CouponOrderFormNumber, 1, 2, 10, 0, '', ''   
  print @a  

**  Created by: Gavin @2012-09-21
**  Modify by: Gavin @2012-09-26 (ver 1.0.0.1) 增加Order form 的orderqty 传入
**  Modify by: Gavin @2012-09-28 (ver 1.0.0.2) fix bug, 当给的号码在order中有重复时的校验问题。
**  Modify by: Gavin @2012-10-08 (ver 1.0.0.3) fix bug, 
**  Modify by: Gavin @2012-10-11 (ver 1.0.0.4) 如果没有coupon 插入到 Ord_CouponPicking_D, 则返回1
**  Modify by: Gavin @2012-10-18 (ver 1.0.0.5) Ord_CouponPicking_D表增加字段，BatchCouponCode，Pickupdatetime（默认值系统时间）
*   Modify by: Gavin @2012-10-24 (ver 1.0.0.6) 判断起始couponnumber时，增加coupontypeid的条件，防止couponnumber和 coupontypeid不一致
**  Modify by: Gavin @2012-11-13 (ver 1.0.0.7) 修改CalcCheckDigit_EAN13 为CalcCheckDigit。
**  Modify by: Gavin @2014-06-26 (ver 1.0.0.8) 增加@CouponStockStatus过滤条件
**  Modify by: Gavin @2014-06-26 (ver 1.0.0.8) 增加参数@IsEarchCoupon。 1: 输入每个coupon，即一个coupon一行。 0：连续coupon合并。 默认0
**  Modify by: DArwin @2017-11-08 (ver 1.0.0.10) line 42, commented ourt paramteer PickupFlag = 0 --and Status = 0 --and StockStatus = 2**

****************************************************************************/
begin
  declare @KeyID int, @PickQty int, @FirstNumber varchar(64), @EndNumber varchar(64)
  declare @StartCoupon varchar(64), @EndCoupon varchar(64), @InvalidCoupon varchar(64), @CouponStatus int  
  declare @i int, @EndCouponNumber varchar(64), @PrevCouponNumber varchar(64), @CouponNumber varchar(64)
  declare @CouponNumMask nvarchar(30), @CouponNumPattern nvarchar(30), @PatternLen int, @Checkdigit int, @CouponSeqNo bigint
  declare @IsImportCouponNumber int, @IsConsecutiveUID int, @BatchCouponCode varchar(64), @StartBatchCouponCode varchar(64)
  declare @HasPickup int, @ModeID int, @ModeCode varchar(64), @CouponStockStatus int
  set @HasPickup = 0
    
  select @CouponStatus = status from Coupon 
     where CouponTypeID = @CouponTypeID and CouponNumber = @StartCouponNumber and Status = 0 and StockStatus IN (2,@damaged,4,@returned)
  -- 判断首coupon状态是否正确。
  if isnull(@CouponStatus, -1) <> 0
    return -1                         
  set @StartCoupon = isnull(@StartCouponNumber, '')
 
  if @IsEarchCoupon = 1
  begin
    insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, 
           FirstCouponNumber, EndCouponNumber, BatchCouponCode)
    select CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, 
           FirstCouponNumber, EndCouponNumber, BatchCouponCode
    from (           
          select ROW_NUMBER() OVER(order by C.CouponNumber) as IID, @CouponPickingNumber as CouponPickingNumber , @CouponTypeID as CouponTypeID, '' as [Description], 
                1 as OrderQty, 1 as PickQty, C.CouponNumber as FirstCouponNumber, C.CouponNumber as EndCouponNumber, B.BatchCouponCode 
             from Coupon C left join BatchCoupon B on C.BatchCouponID = B.BatchCouponID
            where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CouponTypeID = @CouponTypeID and CouponNumber >= @StartCoupon     
        ) A
     where A.IID <= @OrderQty 
    order by IID 
      
    return 0
  end
  else
  begin 
    -- 获得CouponType的设置
    select @CouponNumMask = CouponNumMask, @CouponNumPattern = CouponNumPattern, @CouponTypeID = CouponTypeID,
        @Checkdigit = CouponCheckdigit, @IsImportCouponNumber = isnull(IsImportCouponNumber,0), @IsConsecutiveUID = isnull(IsConsecutiveUID,1),
        @ModeID = CheckDigitModeID
      from CouponType where CouponTypeID = @CouponTypeID
    select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
    set @ModeCode = isnull(@ModeCode, 'EAN13')
          
    set @PatternLen = 0
    while @PatternLen < Len(@CouponNumMask)
    begin      
        if substring(@CouponNumMask, @PatternLen + 1, 1) = 'A'
          set @PatternLen = @PatternLen + 1
        else  
          break  
    end
      
      set @i = 1
      set @PickQty = 0
      set @CouponNumber = ''
      set @StartCouponNumber = ''
      set @EndCouponNumber = ''
      set @PrevCouponNumber = ''
      DECLARE CUR_Coupon CURSOR fast_forward for
        select C.CouponNumber, B.BatchCouponCode from Coupon C left join BatchCoupon B on C.BatchCouponID = B.BatchCouponID
          where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CouponTypeID = @CouponTypeID and CouponNumber >= @StartCoupon
          order by C.CouponNumber	    
	  OPEN CUR_Coupon
      FETCH FROM CUR_Coupon INTO @CouponNumber, @BatchCouponCode
      WHILE @@FETCH_STATUS=0 and @i <= @OrderQty
      BEGIN 
--print '@CouponNumber'      
--print @CouponNumber            
        if @StartCouponNumber = ''
        begin
          set @StartCouponNumber = @CouponNumber
          set @StartBatchCouponCode = @BatchCouponCode
          set @EndCouponNumber = @CouponNumber
        end
        if isnull(@PrevCouponNumber, '') <> ''
        begin
--print  '@PrevCouponNumber'
--print  @PrevCouponNumber          
          -- 判断这个couponnumber 是否在pickup单中
          if not exists(select * from Ord_CouponPicking_D 
            where CouponPickingNumber = @CouponPickingNumber and CouponTypeID = @CouponTypeID 
              and FirstCouponNumber <= @CouponNumber and @CouponNumber <= EndCouponNumber)
          begin 
--print 'ok'         select * from coupontype  
            if isnull(@IsImportCouponNumber, 0) = 0
            begin
              -- 获得上一个couponnumber的序号。
              if @Checkdigit = 1 
                set @CouponSeqNo = substring(@PrevCouponNumber, @PatternLen + 1,  Len(@CouponNumMask) - @PatternLen - 1) 
              else  
                set @CouponSeqNo = substring(@PrevCouponNumber, @PatternLen + 1,  Len(@CouponNumMask) - @PatternLen)      
              -- 计算从上一个序号开始的连续序号和卡号
              set @CouponSeqNo = @CouponSeqNo + 1
              if @Checkdigit = 1 
              begin
                set @EndCouponNumber = substring(@CouponNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CouponSeqNo), Len(@CouponNumMask) - @PatternLen - 1)        
                select @EndCouponNumber = dbo.CalcCheckDigit(@EndCouponNumber, @ModeCode)
              end else        
                set @EndCouponNumber = substring(@CouponNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CouponSeqNo), Len(@CouponNumMask) - @PatternLen)
            end else  
            begin
              if isnull(@IsConsecutiveUID, 1) = 1
              begin
                if @Checkdigit = 1
                begin 
                  set @EndCouponNumber =  cast((cast(substring(@PrevCouponNumber,1,len(@PrevCouponNumber)-1) as bigint) + 1) as varchar)
                  select @EndCouponNumber = dbo.CalcCheckDigit(@EndCouponNumber, @ModeCode)
                end else  
                  set @EndCouponNumber =  cast((cast(@PrevCouponNumber as bigint) + 1) as varchar)
              end else
                set @EndCouponNumber =  ''  
            end
            -- 比较根据上一个序号计算出的号码是否和当前号码相同，如果相同则表示连续，如果不同，则表示不连续，需要另起一条记录。（上一条为空不包括）   
            if (@EndCouponNumber <> @CouponNumber) and (isnull(@PrevCouponNumber,'') <> '')
            begin              
	          insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode)
  	          values(@CouponPickingNumber, @CouponTypeID, '', @OrderFormQty, @PickQty, @StartCouponNumber, @PrevCouponNumber, @StartBatchCouponCode) 	             
  	          set @HasPickup = 1
	          set @PickQty = 0 
              set @StartCouponNumber = @CouponNumber
              set @StartBatchCouponCode = @BatchCouponCode
            end
            set @EndCouponNumber = @CouponNumber
 	        set @PickQty = @PickQty + 1	  
	        set @i = @i + 1	        	              
	      end else
	      begin
--print 'faile'
--print @PickQty
            if @PickQty > 0
            begin
              insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode)
	          values(@CouponPickingNumber, @CouponTypeID, '', @OrderFormQty, @PickQty, @StartCouponNumber, @PrevCouponNumber, @StartBatchCouponCode) 	
	          set @PickQty = 0   
	          set @StartCouponNumber = ''
	          set @HasPickup = 1
	        end  	      
	      end
	    end
	    if isnull(@PrevCouponNumber, '') = ''   -- 需要计算第一个号码
	    begin
 	      set @PickQty = @PickQty + 1	  
	      set @i = @i + 1	 	    
	    end
	    set @PrevCouponNumber = @CouponNumber 
	    FETCH FROM CUR_Coupon INTO @CouponNumber, @BatchCouponCode	  
	  END
      CLOSE CUR_Coupon 
      DEALLOCATE CUR_Coupon 	
--print 'end'      
--print @PickQty
      if @PickQty > 0
      begin
          insert into Ord_CouponPicking_D(CouponPickingNumber, CouponTypeID, [Description], OrderQty, PickQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode)
	      values(@CouponPickingNumber, @CouponTypeID, '', @OrderFormQty, @PickQty, @StartCouponNumber, @EndCouponNumber, @StartBatchCouponCode) 	
	      set @PickQty = 0   
	      set @HasPickup = 1
	  end  

    if @HasPickup = 1
      return 0
    else
      return 1  
  end  
end




GO
