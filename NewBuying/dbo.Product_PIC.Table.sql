USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_PIC]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_PIC](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[ProductThumbnailsFile] [nvarchar](512) NULL,
	[ProductFullPicFile] [nvarchar](512) NULL,
	[ProductPicNote1] [nvarchar](512) NULL,
	[ProductPicNote2] [nvarchar](512) NULL,
	[ProductPicNote3] [nvarchar](512) NULL,
 CONSTRAINT [PK_PRODUCT_PIC] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PIC', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品缩略图' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PIC', @level2type=N'COLUMN',@level2name=N'ProductThumbnailsFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品完整图' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PIC', @level2type=N'COLUMN',@level2name=N'ProductFullPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PIC', @level2type=N'COLUMN',@level2name=N'ProductPicNote1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片备注2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PIC', @level2type=N'COLUMN',@level2name=N'ProductPicNote2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片备注3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PIC', @level2type=N'COLUMN',@level2name=N'ProductPicNote3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商品图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PIC'
GO
