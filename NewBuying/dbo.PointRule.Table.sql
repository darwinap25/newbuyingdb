USE [NewBuying]
GO
/****** Object:  Table [dbo].[PointRule]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PointRule](
	[PointRuleID] [int] IDENTITY(1,1) NOT NULL,
	[PointRuleCode] [varchar](64) NOT NULL,
	[PointRuleDesc1] [varchar](512) NULL,
	[PointRuleDesc2] [varchar](512) NULL,
	[PointRuleDesc3] [varchar](512) NULL,
	[HitItem] [int] NULL DEFAULT ((0)),
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[PointRuleType] [int] NULL DEFAULT ((0)),
	[PointRuleSeqNo] [int] NOT NULL DEFAULT ((0)),
	[PointRuleOper] [int] NOT NULL,
	[PointRuleAmount] [money] NOT NULL,
	[PointRulePoints] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[PointRuleLevel] [int] NULL DEFAULT ((0)),
	[MemberDateType] [int] NULL DEFAULT ((0)),
	[EffectiveDateType] [int] NULL DEFAULT ((0)),
	[EffectiveDate] [int] NULL DEFAULT ((0)),
	[DayFlagCode] [varchar](64) NULL,
	[WeekFlagCode] [varchar](64) NULL,
	[MonthFlagCode] [varchar](64) NULL,
	[Status] [int] NULL DEFAULT ((1)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PointRules_PK] PRIMARY KEY CLUSTERED 
(
	[PointRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设置规则主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则生效条件。 默认0。0：没有限制。1：有限制。需要查看对应的 PointRule_Item 表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'HitItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计算规则类型，默认0：
0：参与计算的金额类型为：消费金额
1：参与计算的金额类型为：使用SVA卡支付的金额
2：参与计算的金额类型为：SVA卡充值的金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleSeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计算方式设置：
0：每当消费PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分。  如：PointRuleAmount=100，PointRulePoints=10， 消费2000， 就可以获得 （2000 /100 ）* 10 = 200
1：当消费金额大于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分
2：当消费金额小于等于PointRuleAmount指定的金额，就能获得PointRulePoints指定的积分
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleOper'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设定的积分规则金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设定的积分规则积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRulePoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则级别。 默认0.  数字越大级别越高。 高级别优先。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'PointRuleLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员范围类型。 
0：不起作用。
1：生日当月。 
2：生日当周。 
3：生日当日。
4：卡激活当月。 
5：卡激活当周。 
6：卡激活当日。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'MemberDateType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'（不使用）生效日期类型。 0：不做要求。 1：月。2：周。3：日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'EffectiveDateType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'（不使用）设置月中哪一天生效。（根据EffectiveDateType判断含义）。 默认0：不起作用。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'EffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一月中促销生效日 （Buy_DayFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'DayFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一周中促销生效日 （Buy_WeekFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'WeekFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效月 （Buy_MonthFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'MonthFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。默认1。0：无效。1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡消费赚积分规则表。（只根据金额计算积分）
一次性奖励积分，在SVAReward中设置。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule'
GO
