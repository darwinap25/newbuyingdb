USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenHQReceiveOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenHQReceiveOrder]
  @OrderNumber VARCHAR(64),     -- 订单号码
  @Type INT                     -- 1：总部给供应商的订单的收货。 此时，FromStoreID 的内容是 供应商ID。 2：店铺退货单的收货. 
AS
/****************************************************************************
**  Name : GenHQReceiveOrder
**  Version: 1.0.0.3
**  Description : 供应商订单批核后产生收货单。(包括店铺退货单产生的收货)
**  Parameter :
select * from Ord_ShipmentOrder_H
**  Created by: Gavin @2015-04-14
**  Modify by: Gavin @2016-01-06 (ver 1.0.0.1) 退货单，退回的库存回到G ， 而不是 F 
**  Modify by Gavin @2016-01-07 (1.0.0.2) 获取busdate时增加StoreCode条件
**  Modify by Gavin @2016-11-03 (1.0.0.3)  BUY_STORE 的 brandcode 改为storebrandcode, 关联的buy_brand表 改为brand表
**
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @StoreCode varchar(64)
  EXEC GetRefNoString 'RECIO', @NewNumber OUTPUT

  IF @Type = 1
  BEGIN
    select @StoreCode = StoreCode FROM BUY_PO_H A left join Buy_STORE B on A.StoreID = B.StoreID
      WHERE POCode = @OrderNumber
    SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC

    INSERT INTO Ord_HQReceiveOrder_H (HQReceiveOrderNumber,OrderType,ReceiveType,ReferenceNo,FromStoreID,FromContactName,
        FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
        StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
        ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    SELECT @NewNumber, 1, 1, POCode, A.VendorID, B.Contact,
        B.ContactTel,B.ContactMobile,B.ContactEmail,B.VendorAddress1,A.StoreID,C.Contact,C.ContactPhone,C.StoreEmail,
        C.StoreFax,C.StoreFullDetail1, A.Note1, @BusDate,NULL,'','P',
        NULL,A.ApproveBy,GETDATE(),A.CreatedBy, GETDATE(), A.UpdatedBy
    FROM BUY_PO_H A LEFT JOIN BUY_VENDOR B ON A.VendorID = B.VendorID
      LEFT JOIN BUY_STORE C ON A.StoreID = C.StoreID
      LEFT JOIN BRAND D ON C.StoreBrandCode = D.StoreBrandCode
    WHERE POCode = @OrderNumber
    
    INSERT INTO Ord_HQReceiveOrder_D (HQReceiveOrderNumber, ProdCode, OrderQty, ReceiveQty, StockTypeCode, Remark)
    SELECT @NewNumber, ProdCode, Qty, Qty, 'G', 'PO Order Auto Gen Receive Order'  
    FROM BUY_PO_D D left join BUY_PO_H H on H.POCode = D.POCode
    WHERE H.POCode = @OrderNumber
  END
  
  IF @Type = 2 
  BEGIN
    select @StoreCode = StoreCode FROM Ord_ReturnOrder_H A left join Buy_STORE B on A.StoreID = B.StoreID
      WHERE ReturnOrderNumber = @OrderNumber
    SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC

    -- 退货单(fromstoreid 指总部) 写入 总部收货单(fromstoreid 指店铺) 时,  fromstoreid 和 storeid 颠倒一下.
    INSERT INTO Ord_HQReceiveOrder_H (HQReceiveOrderNumber,OrderType,ReceiveType,ReferenceNo,FromStoreID, FromContactName,
        FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
        StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
        ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    SELECT @NewNumber, 1, 2, ReturnOrderNumber, StoreID, StoreContactName,        
        StoreContactPhone,StoreMobile,StoreContactEmail,StoreAddress, FromStoreID, FromContactName,FromContactPhone,FromEmail,
        FromMobile,FromAddress,Remark,@BusDate,NULL,'','P',
        NULL,ApproveBy,GETDATE(),CreatedBy,GETDATE(),UpdatedBy
    FROM Ord_ReturnOrder_H 
    WHERE ReturnOrderNumber = @OrderNumber
    
    INSERT INTO Ord_HQReceiveOrder_D (HQReceiveOrderNumber, ProdCode, OrderQty, ReceiveQty, StockTypeCode, Remark)
    SELECT @NewNumber, ProdCode, ReturnQty, ReturnQty, 'G', 'Return Order Auto Gen Receive Order'  
    FROM Ord_ReturnOrder_D D left join Ord_ReturnOrder_H H on H.ReturnOrderNumber = D.ReturnOrderNumber
    WHERE H.ReturnOrderNumber = @OrderNumber
  END  
      
  RETURN 0   
END

GO
