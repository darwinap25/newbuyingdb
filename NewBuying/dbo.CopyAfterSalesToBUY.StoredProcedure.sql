USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CopyAfterSalesToBUY]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyAfterSalesToBUY]
 @AfterSalesNo				varchar(64) 
AS
/*==============================================================*/
/*                
* Name: CopyAfterSalesToBUY 
* Version: 1.0.0.8
* Description : (for bauhaus) 使用linkserver
select * from AfterSales_H
exec CopyAfterSalesToBUY 'ASSNNO000000081'
truncate table Buying_621_Test.DBO.AfterSales_H
truncate table Buying_621_Test.DBO.AfterSales_D
truncate table Buying_621_Test.DBO.AfterSales_T
select * from aftersales_D where txnno = 'ASSNNO000000081'
** Create By Gavin @2016-10-24
** Modify By Gavin @2016-11-22 (ver 1.0.0.1) 增加调用GenSalesPickupOrder 过程。
** Modify By Gavin @2016-11-24 (ver 1.0.0.2) 调用GenSalesPickupOrder 的 IsReserver 参数设置为0。 交易单号使用SVA的sales_H的单号。 buyingDB产生的sales_H单号，确保和SVA的方式不同。
** Modify By Gavin @2016-12-20 (ver 1.0.0.3) 修正 LINKBUY.Buying_621_Test.DBO.GenSalesPickupOrder 的 调用参数， 改为@salestxnno
** Modify By Gavin @2016-12-28 (ver 1.0.0.4) 修正取新增交易号的方式。 根据@AfterSalesNo从aftersales中取相关交易号，再根据这个相关交易号查到reftxno为这个号码的交易单
** Modify By Gavin @2017-03-16 (ver 1.0.0.5) 除了估计aftersales单产生的refund 和 exchange单, 还得传入新的交易单. 旧的交易单需要做 void 处理.
** Modify By Gavin @2017-03-21 (ver 1.0.0.6) 修正换货时的bug
** Modify By Gavin @2017-03-22 (ver 1.0.0.7) @NewTransNum <> '' 时，先调用ReserveSalesStock， 再调用GenSalesPickupOrder，需要使用SalesReserverDetail表
** Modify By Gavin @2017-03-24 (ver 1.0.0.8) 修正 @NewTransNum <> '' 时 bug, 新单的collected需要是0, 否则不能产生pick单          
*/
/*==============================================================*/ 
BEGIN 
/* 
  declare @salestxnno varchar(64), @txntype int, @newtxnno varchar(64),  
          @StoreCode varchar(64), @RegisterCode varchar(64), @Busdate datetime, @Txndate datetime,
          @TotalAmt money  
  declare  @NewTransNum varchar(64), @NewStoreCode varchar(64), @NewRegisterCode varchar(64), @NewBusdate datetime, @NweTxndate datetime

  select @txntype = txntype, @NewTransNum = NewTransNum FROM AfterSales_H WHERE TxnNo = @AfterSalesNo  	
  select @salestxnno = TransNum, @StoreCode = StoreCode, @RegisterCode = RegisterCode, @Busdate = Busdate 
       FROM Sales_H WHERE RefTransNum = @AfterSalesNo  
  select @NewTransNum = TransNum, @NewStoreCode = StoreCode, @NewRegisterCode = RegisterCode, @NewBusdate = Busdate, @NweTxndate = TxnDate 
       FROM Sales_H WHERE TransNum = @NewTransNum  

  set @NewTransNum = isnull(@NewTransNum, '')

   if @NewTransNum <> ''
   begin
     insert  openquery(LINKBUY, 'select SeqNo, TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,TenderID,TenderCode,TenderDesc,  
       TenderType,TenderAmount,LocalAmount,ExchangeRate,PaymentType,Additional,Status,PayConfirmDate,CardNumber,CardType,  
       CardHolder,CardApprovalCode,CardExpiryDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Buying_621_Test.DBO.Sales_T where 1=0')  
     select SeqNo, T.TransNum, H.SalesType,StoreCode,RegisterCode,BusDate,TxnDate,T.TenderID,T.TenderCode,TenderName,  
       TD.TenderType,TenderAmount,LocalAmount,ExchangeRate,0,T.Additional,1,H.BusDate,'',null,  
       '','',null,getdate(),1,getdate(),1   
       FROM Sales_T T left join Sales_H H on H.TransNum = T.TxnNo 
	     left join Tender TD on T.TenderID = TD.TenderID
       WHERE H.TxnNo = @NewTransNum  
  
     insert  openquery(LINKBUY, 'select TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, TxnDate, ProdCode, ProdDesc, DepartCode,  
        Qty, OrgPrice, UnitPrice, NetPrice, OrgAmount, UnitAmount, NetAmount, TotalQty, DiscountPrice, DiscountAmount, ExtraPrice,  
        POReasonCode, Additional1, Additional2, Additional3, POPrice, RPriceTypeCode, IsBOM, IsCoupon, IsBuyBack, IsService,   
        SerialNoStockFlag, SerialNoType, SerialNo, IMEI, StockTypeCode, Collected,  ReservedDate, PickupLocation, PickupStaff,   
        DeliveryDate, DeliveryBy, ActPrice, ActAmount, OrgTransNum, OrgSeqNo, Remark, RefGUID, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn 
        from Buying_621_Test.DBO.Sales_D where 1=0')  
     select D.TxnNo, SeqNo, H.SalesType, H.StoreCode, H.RegisterCode, H.BusDate, H.TxnDate, D.ProdCode, D.ProdDesc, P.DepartCode,  
        D.Qty, D.RetailPrice, D.RetailPrice, D.NetPrice, D.Qty * D.RetailPrice, D.Qty * D.RetailPrice, D.NetAmount, D.Qty, DiscountOffPrice, DiscountOffValue, Null,  
        '', D.Additional, '', '', POPrice, '', 0, 0, 0, 0,   
         0,0,'','','G', case isnull(Collected,0) when 9 then 9 else 0 end, null,@StoreCode,null,   
        null, null, 0,0,'','','','',1,getdate(),1,getdate()  
     FROM Sales_D D left join Sales_H H on H.TxnNo = D.TxnNo 
	   left join Product P on D.ProdCode = P.ProdCode
     WHERE H.TxnNo = @NewTransNum  

     insert  openquery(LINKBUY, 'select TransNum,RefTransNum,StoreCode,RegisterCode,BusDate,TxnDate,TransType,CashierID,SalesManID,TotalAmount,
			Status,MemberID,CardNumber,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
			PickupType,PickupStoreCode,CODFlag, CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Buying_621_Test.DBO.Sales_H where 1=0 ')  
     select TxnNo,RefTxnNo,StoreCode,RegisterCode,BusDate,TxnDate,SalesType,CashierID,SalesManID,TotalAmount,
			Status,MemberID,CardNumber,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
			PickupType,PickupStoreCode,CODFlag, CreatedOn,CreatedBy,UpdatedOn,UpdatedBy
	  FROM Sales_H WHERE TxnNo = @NewTransNum 	     
   end	
   else
   begin       
	   if @txntype = 2
	   begin  
		  select  @TotalAmt = case @txntype when 2 then -TotalAMount else TotalAMount end FROM AfterSales_H WHERE TxnNo = @AfterSalesNo  
    
		  -- 使用 SVA 的 sales的 txnno产生过程。 
		  --exec GetRefNoString 'KTXNNO', @newtxnno output
    
		 insert  openquery(LINKBUY, 'select SeqNo, TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,TenderID,TenderCode,TenderDesc,  
		   TenderType,TenderAmount,LocalAmount,ExchangeRate,PaymentType,Additional,Status,PayConfirmDate,CardNumber,CardType,  
		   CardHolder,CardApprovalCode,CardExpiryDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Buying_621_Test.DBO.Sales_T where 1=0')  
		 select SeqNo, @salestxnno, 0, @StoreCode, @RegisterCode, @BusDate, getdate(), A.TenderID, A.TenderCode, A.TenderDesc,   
			  B.TenderType, 
			  case @txntype when 2 then -A.TenderAmount else A.TenderAmount end,
			  case @txntype when 2 then -A.LocalAmount else A.LocalAmount end,A. ExchangeRate, 0, '', 1, @BusDate, '', null,   
			  '', '', '', GetDate(), 1, GetDate(),1    
		   FROM AfterSales_T A left join Tender B on A.TenderID = B.TenderID  
			WHERE A.TxnNo = @AfterSalesNo  
  
		 insert  openquery(LINKBUY, 'select TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, TxnDate, ProdCode, ProdDesc, DepartCode,  
			Qty, OrgPrice, UnitPrice, NetPrice, OrgAmount, UnitAmount, NetAmount, TotalQty, DiscountPrice, DiscountAmount, ExtraPrice,  
			POReasonCode, Additional1, Additional2, Additional3, POPrice, RPriceTypeCode, IsBOM, IsCoupon, IsBuyBack, IsService,   
			SerialNoStockFlag, SerialNoType, SerialNo, IMEI, StockTypeCode, Collected,  ReservedDate, PickupLocation, PickupStaff,   
			DeliveryDate, DeliveryBy, ActPrice, ActAmount, OrgTransNum, OrgSeqNo, Remark, RefGUID, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn 
			from Buying_621_Test.DBO.Sales_D where 1=0')  
		 select @salestxnno,SeqNo,0,@StoreCode, @RegisterCode, @BusDate, getdate(),AF.ProdCode,AF.ProdDesc,P.DepartCode,
			  case @txntype when 2 then -AF.Qty else AF.Qty end, AF.RetailPrice, AF.RetailPrice, AF.NetPrice, 
			  case @txntype when 2 then -AF.Qty else AF.Qty end*AF.RetailPrice, 
			  case @txntype when 2 then -AF.Qty else AF.Qty end*AF.RetailPrice, 
			  case @txntype when 2 then -AF.NetAmount else AF.NetAmount end, 
			  case @txntype when 2 then -AF.Qty else AF.Qty end, 0,0,0,
			  null, '','','', POPrice,'', 0,0,0,0,
			  0,0,'','','G',case when Collected is null then 0 else Collected end, null,@StoreCode,null,
			  null, null, 0,0,'','','','',1,getdate(),1,getdate()  
		 FROM AfterSales_D AF 
		   left join Product P on AF.ProdCode = P.ProdCode 
		 WHERE AF.TxnNo = @AfterSalesNo AND AF.Qty <> 0 
 
		 insert  openquery(LINKBUY, 'select TransNum,RefTransNum,StoreCode,RegisterCode,BusDate,TxnDate,TransType,CashierID,SalesManID,TotalAmount,
				Status,MemberID,CardNumber,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
				PickupType,PickupStoreCode,CODFlag, CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Buying_621_Test.DBO.Sales_H where 1=0 ')  
		 select TxnNo,RefTxnNo,StoreCode,RegisterCode,BusDate,TxnDate,SalesType,CashierID,SalesManID,
				@TotalAmt,
				Status,MemberID,CardNumber,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
				PickupType,PickupStoreCode,CODFlag, CreatedOn,CreatedBy,UpdatedOn,UpdatedBy
		  FROM Sales_H WHERE TxnNo = @salestxnno  
	   end

	   if @txntype = 3
	   begin  
		  select  @TotalAmt = case @txntype when 2 then -TotalAMount else TotalAMount end FROM AfterSales_H WHERE TxnNo = @AfterSalesNo  
 
		  -- 使用 SVA 的 sales的 txnno产生过程。 
		  --exec GetRefNoString 'KTXNNO', @newtxnno output
 
		  insert  openquery(LINKBUY, 'select SeqNo, TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,TenderID,TenderCode,TenderDesc,  
		   TenderType,TenderAmount,LocalAmount,ExchangeRate,PaymentType,Additional,Status,PayConfirmDate,CardNumber,CardType,  
		   CardHolder,CardApprovalCode,CardExpiryDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Buying_621_Test.DBO.Sales_T where 1=0')  
		  select SeqNo, @salestxnno, 0, @StoreCode, @RegisterCode, @BusDate, getdate(), A.TenderID, A.TenderCode, A.TenderDesc,   
			  B.TenderType, 
			  case @txntype when 2 then -A.TenderAmount else A.TenderAmount end,
			  case @txntype when 2 then -A.LocalAmount else A.LocalAmount end,A. ExchangeRate, 0, '', 1, @BusDate, '', null,   
			  '', '', '', GetDate(), 1, GetDate(),1    
		   FROM AfterSales_T A left join Tender B on A.TenderID = B.TenderID  
			WHERE A.TxnNo = @AfterSalesNo  
   
		 insert  openquery(LINKBUY, 'select TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, TxnDate, ProdCode, ProdDesc, DepartCode,  
			Qty, OrgPrice, UnitPrice, NetPrice, OrgAmount, UnitAmount, NetAmount, TotalQty, DiscountPrice, DiscountAmount, ExtraPrice,  
			POReasonCode, Additional1, Additional2, Additional3, POPrice, RPriceTypeCode, IsBOM, IsCoupon, IsBuyBack, IsService,   
			SerialNoStockFlag, SerialNoType, SerialNo, IMEI, StockTypeCode, Collected,  ReservedDate, PickupLocation, PickupStaff,   
			DeliveryDate, DeliveryBy, ActPrice, ActAmount, OrgTransNum, OrgSeqNo, Remark, RefGUID, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn 
			from Buying_621_Test.DBO.Sales_D where 1=0')  
		 select @salestxnno,SeqNo,0,@StoreCode, @RegisterCode, @BusDate, getdate(),AF.ProdCode,AF.ProdDesc,P.DepartCode,
			  case @txntype when 2 then -AF.Qty else AF.Qty end, AF.RetailPrice, AF.RetailPrice, AF.NetPrice, 
			  case @txntype when 2 then -AF.Qty else AF.Qty end*AF.RetailPrice, 
			  case @txntype when 2 then -AF.Qty else AF.Qty end*AF.RetailPrice, 
			  case @txntype when 2 then -AF.NetAmount else AF.NetAmount end, 
			  case @txntype when 2 then -AF.Qty else AF.Qty end, 0,0,0,
			  null, '','','', POPrice,'', 0,0,0,0,
			  0,0,'','','G',case when Collected is null then 0 else Collected end, null,@StoreCode,null,
			  null, null, 0,0,'','','','',1,getdate(),1,getdate()  
		 FROM AfterSales_D AF 
		   left join Product P on AF.ProdCode = P.ProdCode 
		 WHERE AF.TxnNo = @AfterSalesNo AND AF.Qty > 0 
  
		 insert  openquery(LINKBUY, 'select TransNum,RefTransNum,StoreCode,RegisterCode,BusDate,TxnDate,TransType,CashierID,SalesManID,TotalAmount,
				Status,MemberID,CardNumber,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
				PickupType,PickupStoreCode,CODFlag, CreatedOn,CreatedBy,UpdatedOn,UpdatedBy from Buying_621_Test.DBO.Sales_H where 1=0 ')  
		 select TxnNo,RefTxnNo,StoreCode,RegisterCode,BusDate,TxnDate,SalesType,CashierID,SalesManID,
				@TotalAmt,
				Status,MemberID,CardNumber,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
				PickupType,PickupStoreCode,CODFlag, CreatedOn,CreatedBy,UpdatedOn,UpdatedBy
		  FROM Sales_H WHERE TxnNo = @salestxnno  
	   end
   end   


   IF ISNULL(@NewTransNum, '') <> ''
   BEGIN
     EXEC LINKBUY.Buying_621_Test.DBO.VoidToStockByAfter @AfterSalesNo
	 EXEC LINKBUY.Buying_621_Test.DBO.ReserveSalesStock @NewTransNum
     EXEC LINKBUY.Buying_621_Test.DBO.GenSalesPickupOrder 1, @NewTransNum, 1
   END 
   ELSE
   BEGIN
     EXEC LINKBUY.Buying_621_Test.DBO.ReturnToStockByAfter @AfterSalesNo
     IF @txntype = 3
	 BEGIN
	   EXEC LINKBUY.Buying_621_Test.DBO.ReserveSalesStock @salestxnno
       EXEC LINKBUY.Buying_621_Test.DBO.GenSalesPickupOrder 1, @salestxnno, 1
	 END
   END
*/
return 0
END

GO
