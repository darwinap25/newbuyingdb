USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardTypeNature]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardTypeNature](
	[CardTypeNatureID] [int] IDENTITY(1,1) NOT NULL,
	[CardTypeNatureName1] [nvarchar](512) NULL,
	[CardTypeNatureName2] [nvarchar](512) NULL,
	[CardTypeNatureName3] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARDTYPENATURE] PRIMARY KEY CLUSTERED 
(
	[CardTypeNatureID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CardTypeNature] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CardTypeNature] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardTypeNature', @level2type=N'COLUMN',@level2name=N'CardTypeNatureID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型种类名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardTypeNature', @level2type=N'COLUMN',@level2name=N'CardTypeNatureName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型种类名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardTypeNature', @level2type=N'COLUMN',@level2name=N'CardTypeNatureName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型种类名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardTypeNature', @level2type=N'COLUMN',@level2name=N'CardTypeNatureName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型种类表。固定数据：
1：member card。 2：staff card。 3：store value card。 4：cash card（含有初始金额，不可充值）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardTypeNature'
GO
