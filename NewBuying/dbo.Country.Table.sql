USE [NewBuying]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryCode] [varchar](64) NOT NULL,
	[CountryName1] [nvarchar](512) NULL,
	[CountryName2] [nvarchar](512) NULL,
	[CountryName3] [nvarchar](512) NULL,
	[DeliveryCharge] [money] NULL,
	[FreeThreshold] [money] NULL,
 CONSTRAINT [PK_COUNTRY] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'CountryName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'CountryName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'CountryName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送收费' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'DeliveryCharge'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'免费阈值。 当订单金额超过这个值时，免运费' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'FreeThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家资料表
@2016-10-12 增加DeliveryCharge。
@2017-03-27 增加FreeThreshold.  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country'
GO
