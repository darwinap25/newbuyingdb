USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductStyles_Classify]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductStyles_Classify]
  @BrandID              int,               -- 品牌ID
  @DepartCode           varchar(64),       -- 部门编码
  @IsIncludeChild       int,               -- 0：不包括部门子部门。1：包括部门子部门。    
  @ProdType             int,               -- 货品类型
  @ClassifyTable        varchar(64),       -- 自定义货品分类表名
  @ClassifyKeyID        int,			   -- 自定义货品分类表的ID
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @OrderCondition		nvarchar(1000),	   -- 自定义排序条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr         varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1     
AS
/****************************************************************************
**  Name : GetProductStyles_Classify
**  Version: 1.0.0.0
**  Description : 获得有系列的货品。（例如相同的货品名，prodcode，颜色，尺寸不同，合成一个货号。 此时查询此过程只返回一个记录）
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = '01' 
  exec @a = GetProductStyles_Classify 0, @DepartCode, 0, 0, '', 0, '', '',  0,1000,@PageCount output,@RecordCount output,'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount  
  
**  Created by: Gavin @2013-07-17
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  
  set @BrandID = isnull(@BrandID, 0)        
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))
  set @ProdType = isnull(@ProdType, 0)
  set @IsIncludeChild = isnull(@IsIncludeChild, 0)
  set @ClassifyTable = isnull(@ClassifyTable, '')
  set @ClassifyKeyID = isnull(@ClassifyKeyID, 0)
  set @QueryCondition = isnull(@QueryCondition, '')
  set @OrderCondition = isnull(@OrderCondition, '')
    
  set @SQLStr = 'select P.ProdCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName , '	  
	  + '	 P.ProdPicFile, P.DepartCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '	  
	  + ' D.BrandID, P.ProdType, P.ProdNote, P.ProductBrandID, '	  
      + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName, '
	  + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandDesc2 when 3 then B.ProductBrandDesc3 else B.ProductBrandDesc1 end as ProductBrandDesc, '
	  + '   ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, S.ProdCodeStyle, '
	  + '   A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6 '
  set @SQLStr = @SQLStr + ' from product P '      
      + ' left join Product_Brand B on P.ProductBrandID = B.ProductBrandID '
      + ' left join Color C on P.ColorID = C.ColorID '
      + ' left join Department D on D.DepartCode = P.DepartCode ' 
      + ' left join (select ProdCodeStyle, min(ProdCode) as PPCode from Product_style group by ProdCodeStyle) S on P.ProdCode = S.PPCode ' 
      + ' left join (select * from Product_Particulars where LanguageID = ' + cast(@Language as varchar) + ') A on P.ProdCode = A.Prodcode '		
      
  set @SQLStr = @SQLStr + ' where isnull(S.PPCode, '''') <> '''' '    
    + ' and (P.ProductBrandID = ' + cast(@BrandID as varchar)+ ' or ' + cast(@BrandID as varchar) + '= 0)' 
    + ' and (P.ProdType = ' + cast(@ProdType as varchar)+ ' or ' + cast(@ProdType as varchar) + '= 0)'   
  if @IsIncludeChild = 0
    set @SQLStr = @SQLStr + ' and (P.DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + ''' = '''')'  
  else  
    set @SQLStr = @SQLStr + ' and (P.DepartCode like ''' + @DepartCode + '%'' or ''' + @DepartCode + '''='''') '   
       
  if @ClassifyTable <> '' and @ClassifyKeyID <> 0
    set	@SQLStr = @SQLStr + ' and(P.Prodcode in (select Prodcode from Product_Classify where ForeignTable = ''' + @ClassifyTable + ''' and ForeignkeyID = ' + cast(@ClassifyKeyID as varchar) + '))'
  
  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @QueryCondition
  
  return 0
end

GO
