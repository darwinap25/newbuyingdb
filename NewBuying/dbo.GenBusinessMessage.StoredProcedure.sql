USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenBusinessMessage]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenBusinessMessage]
  @OprID                int,                 -- 操作的OprID 
  @MemberID             int,                 -- 发生此操作的会员
  @CardNumber			      varchar(64),         -- 发生此操作的会员的卡
  @CouponNumber			    varchar(64),          -- 发生此操作的会员的Coupon
  @CardTypeID           int,
  @CardGradeID          int,
  @CardBrandID          int,
  @CouponTypeID         int,
  @ReceiveMobileNumber  varchar(64)='',       -- 仅转赠时使用
  @OtherMemberID        int=0,
  @CustomMsg            nvarchar(2000)='',    -- 用户自定义消息, 考虑还要加上模板消息, 限制在2000长度
  @CUSTOMNAME           varchar(500)='',       -- 用户自定义名字。
  @ActAmout             money=0,              -- 传入的操作金额
  @ActPoint             int=0                 -- 传入的操作积分  
AS
/****************************************************************************
**  Name : GenBusinessMessage
**  Version: 1.0.0.8
**  Description : 产生业务消息.(消息插入MessageObject.MessageReceiveList)，消息不包含内容，需要二次处理，结合模板产生正式消息。
     @MemberID,@CardNumber,@CouponNumber 可以任意填。

 exec GenBusinessMessage 37, 282, '00001359', '05014007700004879', 1, 1, 29, ''
 select * from card
** 
**  Created by:  Gavin @2014-08-22
**  Created by:  Gavin @2014-08-29 (ver 1.0.0.1) 修改查询的SQL，增加输入参数，加快查询速度。增加消息文本中的变量替换功能 
**  Modify by:  Gavin @2014-09-11 (ver 1.0.0.2) 如果accountnumber为空,则不产生此消息. 消息内容去前后空格. IOS消息截取长度(hardcode长度,超过则截断)(英文200,中文69)
**  Modify by:  Gavin @2014-09-22 (ver 1.0.0.3) 增加自定义内容 
**  Modify by:  Gavin @2014-09-22 (ver 1.0.0.4) @PointExpiryDate 加上isnull 
**  Modify by:  Gavin @2014-10-29 (ver 1.0.0.5) 按需求增加内容的变量支持   
**  Modify by:  Gavin @2014-11-11 (ver 1.0.0.6) {FLAGUPDATEON} 标志取的时间,显示格式改为 YYYY-MM-DD HH:NN:SS  
**  Modify by:  Gavin @2014-11-13 (ver 1.0.0.7) 增加性别,默认语言 
**  Modify by:  Gavin @2014-11-26 (ver 1.0.0.8)  Receive/Not Receives 改为  Accept/Decline                  
****************************************************************************/
begin
  declare @MessageID int, @MessageServiceTypeID int, @AccountNumber varchar(512)
  declare @MessageTitle nvarchar(512), @TemplateContent nvarchar(max), @MemberDefLanguage int, @MemberNickName varchar(512)
          , @CouponTypeName varchar(512), @OthenMemberNickName varchar(512), @CardAmt money, @CardPoint int, @CardExpiry datetime
          , @ExpiringPoint int, @PointExpiryDate datetime
  declare @ParamCouponNumber varchar(64),@ParamCardNumber varchar(64), @ParamMemberID varchar(64), @ParamMemberName varchar(64),
          @ParamReceiveMobileNumber  varchar(64), @ParamCouponTypeName varchar(64), @ParamSystemDateTime varchar(64),
          @ParamOtherMemberName varchar(64), @ParamCustomMsg varchar(64)
  declare @ParamCUSTOMNAME varchar(64), @ParamCardAmtBalance varchar(64), @ParamCardPointBalance varchar(64), @ParamCardExpiryDate varchar(64),
          @ParamActAmount varchar(64), @ParamActPoint varchar(64), @ParamExpiringPoint varchar(64), @ParamPointExpirydate varchar(64)
  declare @ParamEmail varchar(64), @ParamMobileNumber varchar(64), @ParamMemberPassword varchar(64), @ParamBirthDay varchar(64), 
          @ParamGender varchar(64), @ParamAcceptSMSDM varchar(64), @ParamAcceptEmailDM varchar(64), @ParamAcceptPhoneDM varchar(64), 
          @ParamLanguage varchar(64), @ParamMobileNotification varchar(64), @ParamFlagUpdatedate varchar(64) 
  declare @MemberEmail varchar(128), @MemberMobilePhone varchar(64), @MemberPassword varchar(512), @MemberDateOfBirth datetime, 
          @AcceptPhoneAdvertising int, @PromotionFlagUpdatedOn datetime, @AcceptSMSDM int, @AcceptEmailDM int
  declare @RecSMSAdv varchar(64), @RecEmailAdv varchar(64), @RecPhoneAdv varchar(64), @MemberSex int, @MemberGender varchar(64),
          @Language varchar(64) 
          
  set @ParamCouponNumber = '{COUPONNUMBER}'
  set @ParamCardNumber = '{CARDNUMBER}'
  set @ParamMemberID = '{MEMBERID}'
  set @ParamMemberName = '{MEMBERNAME}'
  set @ParamReceiveMobileNumber = '{RECEIVEMEMBER}'
  set @ParamCouponTypeName = '{COUPONNAME}'
  set @ParamSystemDateTime = '{SYSTEMDATETIME}' 
  set @ParamOtherMemberName = '{OTHERMEMBERNAME}'
  set @ParamCustomMsg = '{CUSTOMMSG}'
  set @ParamCUSTOMNAME = '{CUSTOMNAME}'
  set @ParamCardAmtBalance = '{CARDAMOUTBALANCE}'
  set @ParamCardPointBalance = '{CARDPOINTBALANCE}'
  set @ParamCardExpiryDate = '{CARDEXPIRYDATE}'
  set @ParamActAmount = '{ACTAMOUNT}'
  set @ParamActPoint = '{ACTPOINT}'
  set @ParamExpiringPoint = '{EXPIRINGPOINT}'
  set @ParamPointExpirydate = '{POINTEXPIRYDATE}'
  -- add @2014-10-29
  set @ParamEmail = '{EMAIL}'
  set @ParamMobileNumber = '{MOBILENUMBER}'
  set @ParamMemberPassword = '{PASSWORD}'
  set @ParamBirthDay = '{BIRTHDAY}'
  set @ParamAcceptSMSDM = '{ACCEPTSMSDM}'
  set @ParamAcceptEmailDM = '{ACCEPTEMAILDM}' 
  set @ParamAcceptPhoneDM = '{ACCEPTPHONEDM}'
  set @ParamMobileNotification = '{ACCEPTMOBILENOTIFICATION}'
  set @ParamFlagUpdatedate = '{FLAGUPDATEON}'
  set @ParamGender = '{MEMBERSEX}'
  set @ParamLanguage = '{MEMBERDEFLANGUAGE}'
  ----
  if isnull(@OtherMemberID, 0 ) > 0
    select @OthenMemberNickName = NickName from Member where MemberID = @OtherMemberID
  select @CardAmt = TotalAmount, @CardPoint = TotalPoints, @CardExpiry = CardExpiryDate from Card where CardNumber = @CardNumber
  select top 1 @ExpiringPoint = sum(isnull(BalancePoint,0)), @PointExpiryDate = ExpiryDate from CardPointDetail 
    where CardNumber = @CardNumber and BalancePoint > 0 
    group by ExpiryDate
   order by ExpiryDate
  select top 1 @AcceptSMSDM = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2 
  select top 1 @AcceptEmailDM = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 1
         
  DECLARE CUR_GenBusinessMessage CURSOR fast_forward FOR
    select MMA.MessageServiceTypeID,MMA.AccountNumber, PP.DescFieldNo, M.NickName,
       case PP.DescFieldNo when 2 then MT1.MessageTitle2 when 3 then MT1.MessageTitle3 else MT1.MessageTitle1 end, 
       case PP.DescFieldNo when 2 then MT1.TemplateContent2 when 3 then MT1.TemplateContent3 else MT1.TemplateContent1 end,
       M.MemberEmail, M.MemberMobilePhone, M.MemberPassword, M.MemberDateOfBirth, M.AcceptPhoneAdvertising, M.PromotionFlagUpdatedOn, M.MemberSex  
     from MemberMessageAccount MMA left join (
      select distinct M.MemberID, M.MemberDefLanguage, M.NickName, M.MemberEmail, M.MemberMobilePhone, M.MemberPassword, 
         M.MemberDateOfBirth, M.AcceptPhoneAdvertising, M.PromotionFlagUpdatedOn, M.MemberSex from 
        (select * from member where MemberID = @MemberID) M        
       inner join dbo.MemberNotice_Filter MF 
               on (MF.BrandID is null or MF.BrandID='-1' or MF.BrandID=@CardBrandID) 
			      and (MF.CardTypeID is null or MF.CardTypeID='-1' or MF.CardTypeID=@CardTypeID)
                  and (MF.CardGradeID is null or MF.CardGradeID='-1' or MF.CardGradeID=@CardGradeID)
                  and (MF.CouponTypeID is null or MF.CouponTypeID='-1' or MF.CouponTypeID=@CouponTypeID)
      ) M on MMA.memberid=M.memberid
      inner join 
        (SELECT     MNMT.KeyID, MNMT.NoticeNumber, MNMT.MessageTemplateID, 
                              MNMT.SendCount, MNMT.DesignSendTimes, 
                              MNMT.SendTime, MNMT.FrequencyValue, MNMT.FrequencyUnit, 
                              MTD.MessageServiceTypeID, MTD.MessageTitle1, MTD.MessageTitle3, 
                              MTD.MessageTitle2, MTD.TemplateContent1, MTD.TemplateContent2, 
                              MTD.TemplateContent3, MT.OprID
           FROM         MemberNotice_MessageType MNMT INNER JOIN
                              MessageTemplate MT ON MNMT.MessageTemplateID = MT.MessageTemplateID INNER JOIN
                              MessageTemplateDetail MTD ON MT.MessageTemplateID = MTD.MessageTemplateID
          WHERE (MNMT.Status = 1)  and (MT.OprID = @OprID) ) MT1 on MMA.MessageServiceTypeID=MT1.MessageServiceTypeID and M.MemberID is not null
     left join LanguageMap PP on M.MemberDefLanguage = PP.KeyID    
    where MMA.MemberID = @MemberID and MMA.IsPrefer = 1 and isnull(MMA.AccountNumber,'') <> ''            -- ver 1.0.0.2
    order by MMA.MemberID, MMA.MessageServiceTypeID   
  OPEN CUR_GenBusinessMessage
  FETCH FROM CUR_GenBusinessMessage INTO @MessageServiceTypeID, @AccountNumber, @MemberDefLanguage, @MemberNickName, 
     @MessageTitle, @TemplateContent, @MemberEmail, @MemberMobilePhone, @MemberPassword, @MemberDateOfBirth, 
     @AcceptPhoneAdvertising, @PromotionFlagUpdatedOn, @MemberSex
  WHILE @@FETCH_STATUS=0
  BEGIN
    if @MemberDefLanguage = 1 
    begin
      set @Language = 'English'
      if @AcceptSMSDM = 1
        set @RecSMSAdv = 'Accept' 
      else 
        set @RecSMSAdv = 'Decline'   
      if @AcceptEmailDM = 1
        set @RecEmailAdv = 'Accept' 
      else 
        set @RecEmailAdv = 'Decline'
      if @AcceptPhoneAdvertising = 1
        set @RecPhoneAdv = 'Accept' 
      else 
        set @RecPhoneAdv = 'Decline'   
      if @MemberSex = 1 
        set @MemberGender = 'Male' 
      else if @MemberSex = 2 
        set @MemberGender = 'Female' 
      else 
        set @MemberGender = ''                 
    end else
    begin
      set @Language = '中文'
      if @AcceptSMSDM = 1
        set @RecSMSAdv = '接收' 
      else 
        set @RecSMSAdv = '不接收'   
      if @AcceptEmailDM = 1
        set @RecEmailAdv = '接收' 
      else 
        set @RecEmailAdv = '不接收'
      if @AcceptPhoneAdvertising = 1
        set @RecPhoneAdv = '接收' 
      else 
        set @RecPhoneAdv = '不接收' 
      if @MemberSex = 1 
        set @MemberGender = '男' 
      else if @MemberSex = 2 
        set @MemberGender = '女' 
      else 
        set @MemberGender = ''                                  
    end
  
    select @CouponTypeName = case @MemberDefLanguage when 2 then CouponTypeName2 when 3 then CouponTypeName3 else CouponTypeName1 end 
      from CouponType where CouponTypeID = @CouponTypeID
    set @MemberNickName = isnull(@MemberNickName, '')
    set @CouponTypeName = ISNULL(@CouponTypeName, '')    
    
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCouponNumber, ISNULL(@CouponNumber,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCardNumber, ISNULL(@CardNumber,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamMemberID, cast(ISNULL(@MemberID,0) as varchar))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamMemberName, ISNULL(@MemberNickName,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamReceiveMobileNumber, ISNULL(@ReceiveMobileNumber,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCouponTypeName, ISNULL(@CouponTypeName,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamSystemDateTime, CONVERT(varchar(10), getdate(), 120))  
    set @TemplateContent = REPLACE(@TemplateContent, @ParamOtherMemberName, ISNULL(@OthenMemberNickName,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCustomMsg, ISNULL(@CustomMsg,''))      
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCUSTOMNAME, ISNULL(@CUSTOMNAME,''))   
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCardAmtBalance, cast(ISNULL(@CardAmt,0) as varchar))   
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCardPointBalance, cast(ISNULL(@CardPoint,0) as varchar))   
    set @TemplateContent = REPLACE(@TemplateContent, @ParamCardExpiryDate, convert(varchar(10), ISNULL(@CardExpiry,0), 120))   
    set @TemplateContent = REPLACE(@TemplateContent, @ParamActAmount, cast(ISNULL(@ActAmout,0) as varchar))   
    set @TemplateContent = REPLACE(@TemplateContent, @ParamActPoint, cast(ISNULL(@ActPoint,0) as varchar))  
    set @TemplateContent = REPLACE(@TemplateContent, @ParamExpiringPoint, cast(ISNULL(@ExpiringPoint,0) as varchar))   
    set @TemplateContent = REPLACE(@TemplateContent, @ParamPointExpirydate, CONVERT(varchar(10), ISNULL(@PointExpiryDate,0), 120))
    
    set @TemplateContent = REPLACE(@TemplateContent, @ParamEmail, ISNULL(@MemberEmail,'')) 
    set @TemplateContent = REPLACE(@TemplateContent, @ParamMobileNumber, ISNULL(@MemberMobilePhone,''))   
    set @TemplateContent = REPLACE(@TemplateContent, @ParamMemberPassword, ISNULL(@MemberPassword,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamBirthDay, convert(varchar(10),ISNULL(@MemberDateOfBirth,0), 120))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamFlagUpdatedate, convert(varchar(19),ISNULL(@PromotionFlagUpdatedOn,0), 120))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamAcceptSMSDM, ISNULL(@RecSMSAdv,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamAcceptEmailDM, ISNULL(@RecEmailAdv,''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamAcceptPhoneDM, ISNULL(@RecPhoneAdv, ''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamGender, ISNULL(@MemberGender, ''))
    set @TemplateContent = REPLACE(@TemplateContent, @ParamLanguage, ISNULL(@Language, ''))       
     
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCouponNumber, ISNULL(@CouponNumber,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCardNumber, ISNULL(@CardNumber,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamMemberID, cast(ISNULL(@MemberID,0) as varchar))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamMemberName, ISNULL(@MemberNickName,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamReceiveMobileNumber, ISNULL(@ReceiveMobileNumber,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCouponTypeName, ISNULL(@CouponTypeName,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamSystemDateTime, CONVERT(varchar(10), getdate(), 120))  
    set @MessageTitle = REPLACE(@MessageTitle, @ParamOtherMemberName, ISNULL(@OthenMemberNickName,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCustomMsg, ISNULL(@CustomMsg,''))  
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCUSTOMNAME, ISNULL(@CUSTOMNAME,''))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCardAmtBalance, cast(ISNULL(@CardAmt,0) as varchar))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCardPointBalance, cast(ISNULL(@CardPoint,0) as varchar))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamCardExpiryDate, convert(varchar(10), ISNULL(@CardExpiry,0), 120))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamActAmount, cast(ISNULL(@ActAmout,0) as varchar))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamActPoint, cast(ISNULL(@ActPoint,0) as varchar))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamExpiringPoint, cast(ISNULL(@ExpiringPoint,0) as varchar))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamPointExpirydate, CONVERT(varchar(10), ISNULL(@PointExpiryDate,0), 120))

    set @MessageTitle = REPLACE(@MessageTitle, @ParamEmail, ISNULL(@MemberEmail,'')) 
    set @MessageTitle = REPLACE(@MessageTitle, @ParamMobileNumber, ISNULL(@MemberMobilePhone,''))   
    set @MessageTitle = REPLACE(@MessageTitle, @ParamMemberPassword, ISNULL(@MemberPassword,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamBirthDay, convert(varchar(10),ISNULL(@MemberDateOfBirth,0), 120))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamFlagUpdatedate, convert(varchar(19),ISNULL(@PromotionFlagUpdatedOn,0), 120))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamAcceptSMSDM, ISNULL(@RecSMSAdv,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamAcceptEmailDM, ISNULL(@RecEmailAdv,''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamAcceptPhoneDM, ISNULL(@RecPhoneAdv, ''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamGender, ISNULL(@MemberGender, ''))
    set @MessageTitle = REPLACE(@MessageTitle, @ParamLanguage, ISNULL(@Language, ''))        
              
    -- test code
 --   set @TemplateContent = ISNULL(@CouponNumber,'') + ',' + ISNULL(@CardNumber,'') + ',' + cast(ISNULL(@MemberID,0) as varchar) + ',' 
--         + ISNULL(@MemberNickName,'') + ',' + ISNULL(@ReceiveMobileNumber,'') + ISNULL(@CouponTypeName,'') + ',' + @TemplateContent

    -- ver 1.0.0.2
    set @TemplateContent = RTRIM(LTrim(@TemplateContent))
    if @MessageServiceTypeID = 9   -- IOS
    begin
      if len(cast(@TemplateContent as varchar)) = datalength(cast(@TemplateContent as varchar))
      begin  -- 英文
        if Len(@TemplateContent) > 200
          set @TemplateContent = SUBSTRING(@TemplateContent, 1, 200)
      end else
      begin  -- 中文
         if Len(@TemplateContent) > 69
          set @TemplateContent = SUBSTRING(@TemplateContent, 1, 69)     
      end  
    end
    
    insert into MessageObject(MessageServiceTypeID, MessageType, MessagePriority, MessageCoding, MessageTitle, 
       MessageBody, FromMemberID, IsInternal, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ResponseCode)
    values (@MessageServiceTypeID, 1, 1, 1, @MessageTitle, 
       cast(@TemplateContent as varbinary(max)), 1, 0, 0, Getdate(), 1, Getdate(), 1, 0)         
    set @MessageID = SCOPE_IDENTITY()
    insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy) 
    values (@MessageID, @MemberID, @AccountNumber, 0, 0, Getdate(), 1)  
    FETCH FROM CUR_GenBusinessMessage INTO @MessageServiceTypeID, @AccountNumber, @MemberDefLanguage, @MemberNickName, 
      @MessageTitle, @TemplateContent, @MemberEmail, @MemberMobilePhone, @MemberPassword, @MemberDateOfBirth, 
      @AcceptPhoneAdvertising, @PromotionFlagUpdatedOn, @MemberSex
  END
  CLOSE CUR_GenBusinessMessage 
  DEALLOCATE CUR_GenBusinessMessage     
    
  return 0   
end

GO
