USE [NewBuying]
GO
/****** Object:  Table [dbo].[UserAction_Movement]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserAction_Movement](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[ActionTypeID] [int] NOT NULL,
	[CardNumber] [varchar](64) NULL,
	[RefNumber] [varchar](64) NULL,
	[Description] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[SecurityCode] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_USERACTION_MOVEMENT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Member ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User operation type ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'ActionTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'reference number（e.g. txn no）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'RefNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'operation description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'remark' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'approval code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security check Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement', @level2type=N'COLUMN',@level2name=N'SecurityCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户操作流水表
ActionTypeID：
1、Login （First time Login，Login (7 days) 不区分）
2、Update Member Information （Email Address Hint，New Password Email ，Add a Contact ，Edit a Contact，Delete a Contact ， Update My Profile ）
3、Change Password（Submit Password ，lost password）
4、Register （New Member）
5、Logout 
6、Add to Cart 
7、Confirm Purchase Order（sales status = 3） 
8、Confirm Payment Details （sales status = 4） 
9、Confirm Shipping Details （sales status = 6） 
10、Shopping Order Complete（sales status = 5） 

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserAction_Movement'
GO
