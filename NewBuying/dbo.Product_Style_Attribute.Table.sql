USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_Style_Attribute]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Style_Attribute](
	[ProdCodeStyle] [varchar](64) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[RGB1] [varchar](64) NULL,
	[RGB2] [varchar](64) NULL,
	[RGB3] [varchar](64) NULL,
	[RGB4] [varchar](64) NULL,
	[RGB5] [varchar](64) NULL,
	[MinDefaultPrice] [money] NULL,
	[MaxPrice] [money] NULL,
	[MinPrice] [money] NULL,
	[MaxDefaultPrice] [money] NULL,
 CONSTRAINT [PK_PRODUCT_STYLE_ATTRIBUTE] PRIMARY KEY CLUSTERED 
(
	[ProdCodeStyle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品系列编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Style_Attribute', @level2type=N'COLUMN',@level2name=N'ProdCodeStyle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主打货品编码. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Style_Attribute', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'@2016-11-14 创建， 用于预先归纳属于 prodcodestyle 的 color 和 price。
预先执行过程，归纳数据填入表中。 表中数据用于网页显示。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Style_Attribute'
GO
