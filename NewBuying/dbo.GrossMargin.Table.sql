USE [NewBuying]
GO
/****** Object:  Table [dbo].[GrossMargin]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GrossMargin](
	[GrossMarginCode] [varchar](64) NOT NULL,
	[Description1] [varchar](512) NULL,
	[Description2] [varchar](512) NULL,
	[Description3] [varchar](512) NULL,
	[ProdCode] [varchar](64) NULL,
	[VenderType] [int] NULL,
	[VenderID] [int] NULL,
	[BuyerType] [int] NULL,
	[BuyerID] [int] NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[VAT] [decimal](16, 6) NULL,
	[Report] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_GROSSMARGIN] PRIMARY KEY CLUSTERED 
(
	[GrossMarginCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[GrossMargin] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[GrossMargin] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'GrossMarginCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'Description1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'Description2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'Description3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商类型。 1; Brand.  2: Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'VenderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VenderType=1时，填写BrandID。 等于2时，填写SupplierID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'VenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'购买者类型。 1; Company  2: Store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'BuyerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BuyerType=1时，填写CompanyID。 等于2时，填写StoreID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'BuyerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gross Margin。百分百数。 e.g. 10 表示 10% ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin', @level2type=N'COLUMN',@level2name=N'VAT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gross Margin 
RRG TelCo 类型卡。
这个表只是用于记录， SVAWeb 维护。 SVA系统中不使用此表数据。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin'
GO
