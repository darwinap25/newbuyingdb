USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcEarnPoints_Base]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[CalcEarnPoints_Base]
   @CardNumber          varchar(64),  --卡号
   @ConsumeAMT          money,         --参与计算的金额
   @ProdCode            varchar(64),   --参与计算的货品
   @DeptCode            varchar(64),   --此货品的部门
   @ProductBrandCode	varchar(64),   --此货品的品牌
   @EarnPoint           int output,     --获得的积分
   @AddPointFlag        int,		    -- 货品的参与增加积分的标志
   @AddPointValue       int,		    -- 货品的参与增加积分的值
   @PointRuleType       int = 0
AS
/****************************************************************************
**  Name : CalcEarnPoints_Base
**  Version: 1.0.0.1
**  Description : 新计算方式，销售的各个货品可以不同积分规则。一个货品可能符合多个积分规则，只取最先一个（按级别高到低）。
                  有货品条件的规则优先。如果填写了货品或者部门，品牌，但是没有特指的积分规则，则返回0.
**
**  Parameter :
**         @CardNumber			varchar(512),	-卡号 (CardNumber 是唯一号码, Card表主键)
**         @ConsumeAMT float   - 消费的金额
**         @EarnPoint     int output  - 获得的积分
**         return:  0 成功， -1 失败
  declare @EarnPoint int
  exec CalcEarnPoints_Base '1111002', 200, '', '', '', @EarnPoint output, 0
  print @EarnPoint
**                                         -- PointRuleOper: 0 = @, 1 = >=, 2 = <=
**  Created by: Gavin @2013-11-20
**  Modified by: Gavin @2014-12-31 (Ver 1.0.0.1) 统一startdate和enddate的比较规则：如果填写的日期不带时间（时间为0），则和 getdate（）比较时，startdate可以直接判断 < getdate， enddate 需要 >= (getdate() - 1)                                                
**
****************************************************************************/
begin 
  declare @TempPoint int, @CardTypeID int, @CardGradeID int
  declare @PointRuleOper int, @PointRuleAmount float, @PointRulePoints int
  declare @MemberBirthDate datetime, @MemberID int, @CardIssueDate Datetime 
  declare @CurDay int, @CurWeek int, @CurMonth int, @TempEarnPoint int
  declare @PointRuleCode varchar(64), @HitItem int	 
  	
  set @ProdCode = isnull(@ProdCode, '')
  set @DeptCode = isnull(@DeptCode, '')
  set @ProductBrandCode = isnull(@ProductBrandCode, '')   		 
  set @ConsumeAMT = abs(isnull(@ConsumeAMT,0))
  set @EarnPoint = 0
  set @PointRuleType = isnull(@PointRuleType, 0)
  set @CurMonth = datepart(mm, getdate()) 
  set @CurWeek = datepart(wk, getdate())  
  set @CurDay = datepart(dd, getdate())  
  set @TempEarnPoint = 0
  set @AddPointFlag = isnull(@AddPointFlag, 0)
  set @AddPointValue = isnull(@AddPointValue, 0)
  if @AddPointFlag = 0 
  begin
    return 0
  end
  if @AddPointFlag = 2 
  begin
    set @EarnPoint = @AddPointValue
    return 0
  end
    
  select @CardTypeID = CardTypeID, @CardGradeID = CardGradeID, @MemberID = MemberID, @CardIssueDate = CardIssueDate 
  from Card where CardNumber = @CardNumber
  select @MemberBirthDate = isnull(MemberDateOfBirth,0) from Member where MemberID = @MemberID
 
    select Top 1 @PointRuleOper = isnull(PointRuleOper,0), @PointRuleAmount = isnull(PointRuleAmount,0), 
       @PointRulePoints = isnull(PointRulePoints,0), @PointRuleCode = PointRuleCode, @HitItem = isnull(HitItem, 0)
      from PointRule
       where StartDate <= Getdate() and EndDate >= (GetDate()-1) and CardTypeID = @CardTypeID
         and (CardGradeID = @CardGradeID or (isnull(@CardGradeID, 0) = 0))
         and PointRuleType = @PointRuleType
         and ( (MemberDateType = 0) or (MemberDateType = 1 and @CurMonth = datepart(mm,@MemberBirthDate)) 
                 or (MemberDateType = 2 and @CurWeek = datepart(wk,@MemberBirthDate)) 
                 or (MemberDateType = 3 and @CurDay = datepart(dd,@MemberBirthDate)) 
                 or (MemberDateType = 4 and @CurMonth = datepart(mm,@CardIssueDate))
                 or (MemberDateType = 5 and @CurWeek = datepart(wk,@CardIssueDate))
                 or (MemberDateType = 6 and @CurDay = datepart(dd,@CardIssueDate)) ) 
         and (EffectiveDateType = 0 or (EffectiveDateType = 1 and EffectiveDate = @CurMonth) 
                 or (EffectiveDateType = 2 and EffectiveDate = @CurWeek) 
                 or (EffectiveDateType = 3 and EffectiveDate = @CurDay) ) 
         and ( (HitItem = 0) or 
               ( ( (@ProdCode = '') or (@ProdCode <> '' and PointRuleCode in (select PointRuleCode from PointRule_Item where EntityType = 3 and EntityNum = @ProdCode)) ) 
				 and ( (@DeptCode = '') or (@DeptCode <> '' and PointRuleCode in (select PointRuleCode from PointRule_Item where EntityType = 2 and EntityNum = @DeptCode)) ) 
				 and ( (@ProductBrandCode = '') or (@ProductBrandCode <> '' and PointRuleCode in (select PointRuleCode from PointRule_Item where EntityType = 1 and EntityNum = @ProductBrandCode)) ) 
               )
             )             
      order by PointRuleLevel, PointRuleSeqNo desc    

    if @@rowcount = 0 
    begin
	  set @EarnPoint = 0
	  return 0
    end
    
    set @PointRuleAmount = isnull(@PointRuleAmount, 0)  
    -- PointRuleOper: 0 = @, 1 = >=, 2 = <=
    if @PointRuleOper = 0
    begin    
      if @PointRuleAmount <> 0
      begin    
        select @TempPoint =  Round(@ConsumeAMT/@PointRuleAmount, 0, 1) 
        if @TempPoint > 1
          set @TempEarnPoint = @TempPoint * @PointRulePoints
      end else
        set @TempEarnPoint = 0     
    end
    if @PointRuleOper = 1
    begin
      if @ConsumeAMT >= @PointRuleAmount
        set @TempEarnPoint = @PointRulePoints
    end
    if @PointRuleOper = 2
    begin
      if @ConsumeAMT <= @PointRuleAmount
        set @TempEarnPoint = @PointRulePoints
    end  
    
    set @EarnPoint = @TempEarnPoint

  if @AddPointFlag = 3
    set @EarnPoint = @EarnPoint * @AddPointValue 
             
  return 0
end

GO
