USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_INSTALLMENT]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_INSTALLMENT](
	[InstallmentID] [int] IDENTITY(1,1) NOT NULL,
	[InstallmentCode] [varchar](64) NOT NULL,
	[BankID] [int] NULL,
	[InstallmentName1] [nvarchar](512) NULL,
	[InstallmentName2] [nvarchar](512) NULL,
	[InstallmentName3] [nvarchar](512) NULL,
	[NoOfLast] [int] NULL,
	[PAInterest] [decimal](12, 4) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Note] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_INSTALLMENT] PRIMARY KEY CLUSTERED 
(
	[InstallmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_INSTALLMENT] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_INSTALLMENT] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'InstallmentID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'InstallmentCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_bank 表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'BankID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分期付款描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'InstallmentName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分期付款描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'InstallmentName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分期付款描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'InstallmentName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分期长度（单位月）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'NoOfLast'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分期利息（百分比）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'PAInterest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付分期表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_INSTALLMENT'
GO
