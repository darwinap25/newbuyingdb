USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SalesSubmit_20171019]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SalesSubmit_20171019]
  @Type                          int,               -- 1: SVA DB, call svasalessubmit.  2: buying DB call possalessubmit
  @UserID				                 int,		            -- 操作员ID
  @MemberID				               int,               -- 会员ID
  @CardUID				               varchar(64),		    -- 会员卡的UID
  @CardNumber			               varchar(64),       -- 会员卡号。
  @CustomerName                  VARCHAR(64),       -- 顾客名称
  @CustomerPhone                 VARCHAR(64),       -- 顾客电话 
  @BrandID			                 int,               -- 店铺的brandID
  @StoreCode                     varchar(64),       -- 店铺编码   
  @RegisterCode                  varchar(64),       -- 终端编码
  @ServerCode			               varchar(64),       -- server编码
  @TxnNo				                 varchar(64) output,-- 交易号码 (如果DB产生,则会返回)
  @VoidTxnNo                     varchar(64),       -- 被void的交易号
  @RefTxnNo                      VARCHAR(64),       -- 相关交易号码
  @Busdate				               datetime output,   -- 交易busdate (如果DB产生,则会返回)
  @Txndate				               datetime,			    -- 交易系统时间
  @SalesType                     INT,               -- 交易类型      0:normal sales  9:Void sales, 10: return. 11:exchange
  @MemberSalesFlag               INT,               -- 是否会员销售
  @CashierCode			             varchar(64),		    -- 收银员Code  
  @SalesManCode			             varchar(64),		    -- 销售员Code
  @TotalAmount			             money,             -- 交易总额
  @TxnStatus			               int,               -- 交易状态
  @Contacter                     varchar(512),		  -- 联系人
  @ContactPhone                  varchar(512),      -- 联系人电话
  @Courier                       varchar(512),		  -- 送货员
  @DeliveryFlag                  INT,               -- 送货标志
  @DeliveryaAddr                 nvarchar(512),     -- 送货地址
  @DeliveryDate                  datetime,          -- 送货时间
  @DeliveryCompleteDate          datetime,          -- 送货完成时间
  @SalesDetail                   nvarchar(max),     -- 交易单货品明细(交易货品, XML格式字符串)
  @SalesTender                   nvarchar(max),     -- 交易单支付明细(交易支付记录, XML格式字符串)
  @SalesDisc                     NVARCHAR(max),     -- 交易单折扣明细(交易中使用的改价，促销，折扣，BOM等等改变销售价格的操作, XML格式字符串)
  @SalesReceipt                  varchar(max),      -- 交易单小票（字符串格式）  
  @SalesReceiptBIN               varbinary(max),    -- 交易单小票（二进制格式）
  @SalesAdditional               nvarchar(512),     -- 交易单附加信息
  @IsShoppingCart                int=0,             -- 是否购物车订单. 0:不是. 1:是的. 默认0
  @DeliveryNumber                varchar(512)='',   -- 送货单号.
  @LogisticsProviderID           int=0,             -- 送货的物流供应商。
  @ReturnMessageStr              varchar(max) output, -- 返回信息。（json格式）
  @ReturnAmount                  money output,      -- msvc 支付时，返回完成后的余额。 （目前，登录的卡和msvc的卡必须相同的）
  @ReturnPoint                   int output,        -- 支付后的余额
  @PickupType                    int = 1,           -- 提货方式。 默认 1.  1：门店自提	 2：送货
  @PickupStoreCode               varchar(64) = '',  -- 门店自提时，设置提货店铺code
  @CODFlag                       int = 0,           -- 0：先款后货。 1：钱货两讫。@PickupType =2 时，表示货到付款
  @IsCalcPoint                   int = 1,           -- 此单是否积分。（for VIVA 暂时使用此参数屏蔽交易积分）
  @ForceToComplete               int = 0,           -- 0：正常情况，检查数据错误则返回。 1：强制完成，忽略错误,不考虑负积分。默认0（主要针对void的情况,如果负积分，再增加一条OprID=90的补偿记录）
  @IsDealWithSalesData           int = 1,            -- 0: 不处理SalesD,SalesT中的数据. 1: 处理salesD,salesT中的数据,指定类型的货品处理,比如卡充值.根据不同的tender类型, 执行操作. 默认是1.
  @BillContact                   varchar(64)='',     -- 账单人    （是账单，不是发票）
  @BillContactPhone              varchar(64)='',      -- 账单人电话 
  @BillAddress                   nvarchar(512)='',      -- 账单地址
  @Remark                        nvarchar(512)='',     -- 备注 
  @Deliveryer                    INT=0               -- 送货人. (是员工,使用ID. 如果非员工,则不使用此字段)
AS
/****************************************************************************
**  Name : SalesSubmit
**  Version : 1.0.0.6
**  Description : 上传交易数据. 目的是统一WebBuying和SVA DB,合并统一保存交易.不再分开SVASalesSubmit,POSSalesSubmit
@SalesDetail格式范例:  
<ROOT>
  <PLU TxnNo="" SeqNo="" ProdCode="0001" ProdDesc="" Serialno="" Collected="" RetailPrice="" NetPrice="" QTY="" NetAmount="" Additional="" ReservedDate="" PickupDate="" RefSeqNo=""></PLU>
<ROOT>	 
@SalesTender格式范例:  
<ROOT>
  <TENDER TxnNo="" SeqNo="" TenderID="" TenderCode="" TenderType="" TenderName="" TenderAmount="" LocalAmount="" ExchangeRate="" Additional=""></TENDER>
</ROOT> 
@SalesDisc格式范例:  
<ROOT>
  <DISCOUNT TxnNo="" ItemSeqNo="" ProdCode="" TenderSeqNo="" TenderID="" TenderCode="" TenderType="" TenderName=""SalesDiscCode="" SalesDiscDesc="" SalesDiscType="" SalesDiscDataType="" SalesDiscPrice="" SalesPrice="" SalesDiscOffAmount="" SalesDiscQty="" AffectNetPrice="" SalesDiscLevel=""></DISCOUNT>
</ROOT> 

**  Created by Gavin @2017-05-26
**  Modify by Gavin @2017-08-22 (ver 1.0.0.1) 只要提交salessubmit，就清空顾客的购物车。增加填写MemberMobilePhone
**  Modify by Gavin @2017-08-31 (ver 1.0.0.2) 如果是exchange，需要补钱时，会通过自动积分过程计算并加分，所以这种情况下不需要再用oprid=4的加分。 如果是退钱的，则需要oprid=4的扣分
**  Modify by Gavin @2017-09-05 (ver 1.0.0.3) 增加处理prodType=3的情况（货品是Coupon，销售coupon，同时激活Coupon）
**  Modify by Gavin @2017-09-06 (ver 1.0.0.4) 取消@IsMemberSales的控制。
**  Modify by Gavin @2017-09-28 (ver 1.0.0.5) 增加exchange的处理。TransType = 11
**  Modify by Gavin @2017-09-29 (ver 1.0.0.6) buy_product和product没有同步。现在改为直接使用product表 
****************************************************************************/
BEGIN 
  DECLARE @OLDTXNSTATUS INT, @LONGITUDE VARCHAR(512), @LATITUDE VARCHAR(512), @MEMBERNAME VARCHAR(512), @MEMBERMOBILEPHONE VARCHAR(512),
		  @CARDTOTALAMOUNT MONEY, @CARDTOTALPOINTS INT, @EXPIRYDATE DATETIME, @CARDSTATUS INT, @MEMBERADDRESS NVARCHAR(512), 
		  @APPROVALCODE VARCHAR(6)
  DECLARE @SALESDDATA XML, @SALESTDATA XML, @SALESDISCDATA XML
  DECLARE @EARNPOINTS INT, @STOREID INT, @CALCRESULT INT
  DECLARE @RECEIVELIST VARCHAR(64), @MESSAGEID INT, @MESSAGESERVICETYPEID INT
  DECLARE @CARDGRADEID INT, @BINDCOUPONTYPEID INT, @BINDCOUPONCOUNT INT, @COUPONEXPIRYDATE DATETIME, @COUPONNUMBER VARCHAR(64)
  DECLARE @COUPONAMOUNT MONEY, @I INT
  DECLARE @COUPONSTATUS INT, @NEWCOUPONSTATUS INT, @NEWCOUPONEXPIRYDATE DATE
  DECLARE @RESULT INT, @OPRID INT, @ADDVALUECARDNUMBER VARCHAR(64), @ADDVALUE MONEY, @TENDERTYPE INT, @LOCALAMOUNT MONEY, @SALESTADDITIONAL VARCHAR(512)
  DECLARE @RETURNCOUPONTYPEAMOUNT MONEY, @RETURNCOUPONTYPEPOINT INT , @RETURNCOUPONTYPEDISCOUNT DECIMAL(16,6), @RETURNBINDPLU VARCHAR(64),
          @CARDEXPIRYDATE DATETIME,  @RETURNMESSAGESTRCARDSUBMIT VARCHAR(MAX)
  DECLARE @PRODTYPE INT, @CARDAMOUNTTOPOINTRATE DECIMAL(16,6), @ADDPOINT INT, @CARDPOINTTOAMOUNTRATE DECIMAL(16,6)        
  DECLARE @PAYMENTDONEON DATETIME, @POINTRULETYPE INT
  DECLARE @MEMBERCARDAMOUNTBALANCE MONEY, @MEMBERCARDAMOUNTACT MONEY, @MEMBERCARDPOINTACT INT, @MEMBERCARDPOINTBALANCE INT,
          @ACTCOUPON VARCHAR(512), @STOCKTYPEID INT, @STOCKTYPECODE VARCHAR(64)
  DECLARE @INPUTSTRING VARCHAR(MAX), @ACCOUNTNUMBER NVARCHAR(512), @SALESDMSGSTRING VARCHAR(MAX), @SALESDMSGJSON VARCHAR(MAX)            
  DECLARE @JSONPRODDESC NVARCHAR(512), @JSONNETPRICE MONEY, @JSONQTY INT, @JSONNETAMOUNT MONEY, @MEMO1 VARCHAR(100), @MEMO2 VARCHAR(100)
  DECLARE @JSONTENDERID INT, @JSONTENDERTYPE INT , @JSONTENDERCODE VARCHAR(64), @JSONTENDERNAME NVARCHAR(512), @JSONTENDERAMOUNT MONEY, 
          @JSONLOCALAMOUNT MONEY, @JSONEXCHANGERATE DECIMAL(16,6), @JSONSALESTADDITIONAL VARCHAR(512)
  DECLARE @SALESTMSGSTRING VARCHAR(MAX), @SALESTMSGJSON VARCHAR(MAX), @HASCOD INT
  DECLARE @BRANDCODE VARCHAR(64), @BRANDNAME VARCHAR(512)
    
  DECLARE @SALESD TABLE (TXNNO VARCHAR(64), SEQNO VARCHAR(64), PRODCODE VARCHAR(64), PRODDESC NVARCHAR(512), SERIALNO VARCHAR(512), 
        COLLECTED INT, RETAILPRICE MONEY, NETPRICE MONEY, QTY INT, NETAMOUNT MONEY, ADDITIONAL VARCHAR(512),RESERVEDDATE DATETIME, PICKUPDATE DATETIME, RefSeqNo VARCHAR(64))
  DECLARE @SALEST TABLE (TXNNO VARCHAR(64), SEQNO INT, TENDERCODE VARCHAR(64), TENDERID INT, TENDERTYPE INT, TENDERNAME NVARCHAR(512), TENDERAMOUNT MONEY, 
        LOCALAMOUNT MONEY, EXCHANGERATE DECIMAL(16,6), TENDERADDITIONAL VARCHAR(512), SALESTADDITIONAL VARCHAR(512))
  DECLARE @SALESDS TABLE(TXNNO VARCHAR(64), ITEMSEQNO VARCHAR(64),PRODCODE VARCHAR(64), TENDERSEQNO INT, TENDERCODE VARCHAR(64), 
        TENDERID INT, TENDERTYPE INT, TENDERNAME VARCHAR(512), SALESDISCCODE VARCHAR(64),
        SALESDISCDESC VARCHAR(512), SALESDISCTYPE INT, SALESDISCDATATYPE INT, SALESDISCPRICE MONEY, SALESPRICE MONEY, 
        SALESDISCOFFAMOUNT MONEY, SALESDISCQTY INT, AFFECTNETPRICE INT, SALESDISCLEVEL INT)  
  DECLARE @SIGN INT,  @ISNICK INT, @ISINT INT      
  DECLARE @STORENAME VARCHAR(512), @STOREADDRESSDETAIL VARCHAR(512), @DELIVERYFULLADDRESS VARCHAR(512)
  DECLARE @ReserverResult INT, @IsMemberSales INT, @TenderCode VARCHAR(64), @TenderID INT
  DECLARE @ReturnStatus INT

  DECLARE @CASHIERID INT, @SALESMANID INT
  SET @CASHIERID = CAST(@CASHIERCODE AS INT)
  SET @SALESMANID = CAST(@SALESMANCODE AS INT)

  SET @IsShoppingCart = ISNULL(@IsShoppingCart, 0)
  SET @UserID = ISNULL(@UserID, 0)
  IF ISNULL(@UserID, 0) = 0
    SELECT TOP 1 @UserID = UserID FROM Accounts_Users

  -- Nick 调用的数据，金额是不放大100倍的。所以这里要做区别，不做 *0.01
  SET @Result = 0
  SET @Sign = 1
  IF @SalesDisc = 'standard' 
    SET @IsNick = 1
  ELSE SET @IsNick = 0   
  
  IF @DeliveryDate = 0
    SET @DeliveryDate = null
  IF @DeliveryCompleteDate = 0
    SET @DeliveryCompleteDate = null      

  SET @BrandID = ISNULL(@BrandID, 0)
  SELECT @BrandCode = StoreBrandCode, @BrandName = StoreBrandName1  FROM brand WHERE StorebrandID = @BrandID
  SET @BrandCode = ISNULL(@BrandCode, '')
  SET @BrandName = ISNULL(@BrandName, '')        
  SET @PointRuleType = 0     
  SET @HasCOD = 0        
  SET @IsShoppingCart = ISNULL(@IsShoppingCart, 0)
  SET @SalesDisc = ISNULL(@SalesDisc, '')
  IF  @SalesDisc = 'standard' 
    SET @IsNick = 1
  ELSE SET @IsNick = 0


  --PLU detail数据写入临时表，判断货品库存数量
  IF isnull(@SalesDetail, '') <> ''
  BEGIN
  	SET @SalesDData = @SalesDetail
	IF @IsNick = 1
	  INSERT INTO @SalesD(TxnNo, SeqNo, ProdCode, ProdDesc, Serialno, Collected, RetailPrice, NetPrice, Qty, NetAmount, Additional, ReservedDate, PickupDate,RefSeqNo)
	  SELECT @TxnNo, T.v.value('@SeqNo',' varchar(64)') AS SeqNo, T.v.value('@ProdCode','varchar(64)') AS ProdCode, 
	    T.v.value('@ProdDesc','nvarchar(512)') AS ProdDesc, T.v.value('@Serialno','varchar(512)') AS Serialno, 
	    CASE WHEN isnull(T.v.value('@Collected','varchar(64)'),'') = '' THEN 0 ELSE T.v.value('@Collected','int') END  AS Collected, 
	    T.v.value('@RetailPrice','money') AS RetailPrice, 
	    T.v.value('@NetPrice','money') AS NetPrice, T.v.value('@QTY','int') AS QTY, T.v.value('@NetAmount','money') AS NetAmount, 
	    T.v.value('@Additional','varchar(512)') AS Additional,
	    T.v.value('@ReservedDate','datetime') AS ReservedDate, T.v.value('@PickupDate','datetime') AS PickupDate,T.v.value('@RefSeqNo','varchar(64)') AS RefSeqNo	    
	  FROM @SalesDData.nodes('/ROOT/PLU') T(v)
    ELSE
	  INSERT INTO @SalesD(TxnNo, SeqNo, ProdCode, ProdDesc, Serialno, Collected, RetailPrice, NetPrice, Qty, NetAmount, Additional, ReservedDate, PickupDate, RefSeqNo)
	  SELECT @TxnNo, T.v.value('@SeqNo',' varchar(64)') AS SeqNo, T.v.value('@ProdCode','varchar(64)') AS ProdCode, 
	    T.v.value('@ProdDesc','nvarchar(512)') AS ProdDesc, T.v.value('@Serialno','varchar(512)') AS Serialno, 
	    CASE WHEN isnull(T.v.value('@Collected','varchar(64)'),'') = '' THEN 0 ELSE T.v.value('@Collected','int') END AS Collected, 
	    T.v.value('@RetailPrice','money') * 0.01 AS RetailPrice, 
	    T.v.value('@NetPrice','money') * 0.01 AS NetPrice, T.v.value('@QTY','int') AS QTY, T.v.value('@NetAmount','money') * 0.01 AS NetAmount, 
	    T.v.value('@Additional','varchar(512)') AS Additional,
	    T.v.value('@ReservedDate','datetime') AS ReservedDate, T.v.value('@PickupDate','datetime') AS PickupDate,T.v.value('@RefSeqNo','varchar(64)') AS RefSeqNo		    
	  FROM @SalesDData.nodes('/ROOT/PLU') T(v)
  END	
						
  --Tender detail数据写入临时表
  IF isnull(@SalesTender, '') <> ''
  BEGIN
  	SET @SalesTData = @SalesTender
	IF @IsNick = 1
	  INSERT INTO @SalesT(TxnNo, SeqNo, TenderID, TenderCode, TenderType, TenderName, TenderAmount, LocalAmount, ExchangeRate, TenderAdditional, SalesTAdditional)
	  SELECT @TxnNo, T.v.value('@SeqNo','int') AS SeqNo, T.v.value('@TenderID','int') AS TenderID, T.v.value('@TenderCode','varchar(64)') AS TenderCode, isnull(S.TenderType, 0) AS TenderType,
	    --T.v.value('@TenderName','nvarchar(512)') as TenderName, 
		S.CurrencyName1 AS TenderName, 
		T.v.value('@TenderAmount','money') AS TenderAmount, 
	    T.v.value('@LocalAmount','money') AS LocalAmount, 
	    T.v.value('@ExchangeRate','decimal(16,6)') as ExchangeRate, '',  T.v.value('@Additional','varchar(512)') AS SalesTAdditional
	  FROM @SalesTData.nodes('/ROOT/TENDER') T(v) left join BUY_CURRENCY S ON T.v.value('@TenderCode','varchar(64)') = S.CURRENCYCODE
	  WHERE isnull(S.CURRENCYCODE, '') <> ''	
    ELSE
	  INSERT INTO @SalesT(TxnNo, SeqNo, TenderID, TenderCode, TenderType, TenderName, TenderAmount, LocalAmount, ExchangeRate, TenderAdditional, SalesTAdditional)
	  SELECT @TxnNo, T.v.value('@SeqNo','int') AS SeqNo, T.v.value('@TenderID','int') AS TenderID, T.v.value('@TenderCode','varchar(64)') AS TenderCode, isnull(S.TenderType, 0) AS TenderType, 
		S.CurrencyName1 AS TenderName, 
		T.v.value('@TenderAmount','money') * 0.01 AS TenderAmount, 
	    T.v.value('@LocalAmount','money') * 0.01 AS LocalAmount, 
	    T.v.value('@ExchangeRate','decimal(16,6)') AS ExchangeRate, '',  T.v.value('@Additional','varchar(512)') AS SalesTAdditional
	  FROM @SalesTData.nodes('/ROOT/TENDER') T(v) left join BUY_CURRENCY S ON T.v.value('@TenderCode','varchar(64)') = S.CURRENCYCODE
	  WHERE isnull(S.CURRENCYCODE, '') <> ''	   
  END        		 

  --Discount 数据写入临时表
  IF isnull(@SalesDisc, '') <> ''
  BEGIN
  	SET @SalesDiscData = @SalesDisc
	  INSERT INTO @SalesDS(TxnNo, ItemSeqNo, ProdCode, TenderSeqNo, TenderCode, TenderID, TenderType, TenderName, SalesDiscCode,
	     SalesDiscDesc, SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, SalesDiscLevel)
	  SELECT @TxnNo, T.v.value('@ItemSeqNo','varchar(64)') as ItemSeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode,
	     T.v.value('@TenderSeqNo','int') as TenderSeqNo, T.v.value('@TenderCode','varchar(64)') as TenderCode,
	     T.v.value('@TenderID','int') as TenderID, T.v.value('@TenderType','int') as TenderType, 
	     T.v.value('@TenderName','varchar(64)') as TenderName, 
	     T.v.value('@SalesDiscCode','varchar(64)') as SalesDiscCode, T.v.value('@SalesDiscDesc','varchar(512)') as SalesDiscDesc, 
	     T.v.value('@SalesDiscType','int') as SalesDiscType, T.v.value('@SalesDiscDataType','int') as SalesDiscDataType, 
	     T.v.value('@SalesDiscPrice','money') as SalesDiscPrice, T.v.value('@SalesPrice','money') * 0.01 as SalesPrice, 
	     T.v.value('@SalesDiscOffAmount','money') as SalesDiscOffAmount, T.v.value('@SalesDiscQty','int') as SalesDiscQty, 
	     T.v.value('@AffectNetPrice','int') as AffectNetPrice, T.v.value('@SalesDiscLevel','int') as SalesDiscLevel 
	  FROM @SalesDiscData.nodes('/ROOT/DISCOUNT') T(v) 
  END  

    -- 检查Busdate
    DECLARE @SODEODBUSDATE DATE
    SELECT TOP 1 @SODEODBusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate desc
    IF @SODEODBusDate is null
      RETURN -10  
	IF ISNULL(@BusDate,0) = 0
      SET @BusDate = @SODEODBusDate
    --=======  
    IF ISNULL(@TxnDate, 0) = 0 
      SET @TxnDate = getdate()

    -- 获取会员   
    IF ISNULL(@CardNumber, '') = '' AND ISNULL(@CardUID, '') <> ''
      SELECT @CardNumber = CardNumber FROM CardUIDMap WHERE CardUID = @CardUID
    IF ISNULL(@CardNumber, '') <> ''
      SELECT @MemberID = MemberID, @CardTotalAmount = TotalAmount, @CardTotalPoints = ISNULL(TotalPoints,0),
           @ExpiryDate = CardExpiryDate, @CardStatus = Status, @CardGradeID = CardGradeID, @ReturnAmount = ISNULL(TotalAmount,0),
           @ReturnPoint = ISNULL(TotalPoints, 0)
      FROM Card WHERE CardNumber = @CardNumber  
    SET @MemberCardAmountBalance = @ReturnAmount
    SET @MemberCardPointBalance = @ReturnPoint
    SELECT @MemberAddress = AddressFullDetail FROM memberaddress WHERE MemberFirstAddr = 1 and MemberID = @MemberID  
    SELECT @MemberName = RTrim(LTrim(ISNULL(MemberChiFamilyName,''))) + RTrim(LTrim(ISNULL(MemberChiGivenName,''))), @MemberMobilePhone = MemberMobilePhone 
	  FROM Member WHERE MemberID = @MemberID   

    IF ISNULL(@MemberID, 0) = 0 AND ISNULL(@CardNumber, '') = ''
      SET @IsMemberSales = 0
    ELSE
    BEGIN
      SET @IsMemberSales = 1
	  SET @CardNumber = isnull(@CardNumber, '')
    END


  -- 开始插入数据    
  BEGIN TRAN	   -- 开始事务
  		
	SELECT @DeliveryFullAddress = h.DeliveryFullAddress, @StoreAddressDetail = s.StoreAddressDetail1, @StoreName = s.StoreName1 
	  FROM sales_h h 
	  left join buy_store s ON h.PickupStoreCode = s.StoreCode	 
	 WHERE h.TransNum = @TxnNo
	
	IF ISNULL(@DeliveryFullAddress,'')<>''
	  SET @DeliveryaAddr = @DeliveryFullAddress

	IF @PickupType = 1
	BEGIN
	  IF ISNULL(@StoreAddressDetail, '')<>''
	    SET @DeliveryaAddr = @StoreName + '-' + @PickupStoreCode + '  ' + @StoreAddressDetail
	END
	
    -- 如果没有单号,则自动产生一个.  如果有输入，则检查这个txnno是否已经存在。如果已经存在的，则可以是update数据
    -- 获取交易号    
    IF @IsShoppingCart = 1 
    BEGIN
      --购物车交易订单号产生规则: 一个顾客只产生一个购物车订单号.
      SET @TxnStatus = 0
      IF isnull(@TxnNo, '') <> ''
        SELECT @OldTxnStatus = Status FROM sales_H WHERE TransNum = @TxnNo
      ELSE    
        SET @TxnNo = 'CARTNO' + 'MID' + cast(@MemberID AS VARCHAR)
    END ELSE
    BEGIN
      IF isnull(@TxnNo, '') <> ''
        SELECT @OldTxnStatus = Status FROM sales_H WHERE TransNum = @TxnNo
      ELSE
        EXEC GenNewTxnNo @StoreCode, @RegisterCode, @Busdate, @TxnNo OUTPUT
    END
    SET @OldTxnStatus = ISNULL(@OldTxnStatus, -1)

	-- 非购物车, 首次完成支付或者交易完成.
	IF (@IsShoppingCart = 0) and (ISNULL(@OldTxnStatus, -1) <> @TxnStatus) and (@TxnStatus = 4 or @TxnStatus = 5)	and @OldTxnStatus < 4
      SET @paymentDoneOn = GETDATE()


  --写入PLU detail数据
    IF ISNULL(@SalesDetail, '') <> ''
    BEGIN
      IF EXISTS(SELECT TransNum FROM sales_D WHERE TransNum = @TxnNo)
        DELETE FROM sales_D WHERE TransNum = @TxnNo 

      INSERT INTO Sales_D(TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, TxnDate, ProdCode, ProdDesc, DepartCode,
          Qty, OrgPrice, UnitPrice, NetPrice, OrgAmount, UnitAmount, NetAmount, TotalQty, DiscountPrice, DiscountAmount, ExtraPrice,
          POReasonCode, Additional1, Additional2, Additional3, POPrice, RPriceTypeCode, IsBOM, IsCoupon, IsBuyBack, IsService, 
          SerialNoStockFlag, SerialNoType, SerialNo, IMEI, StockTypeCode, Collected,  ReservedDate, PickupLocation, PickupStaff, 
          DeliveryDate, DeliveryBy, ActPrice, ActAmount, OrgTransNum, OrgSeqNo, Remark, RefGUID, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn)                          
      SELECT @TxnNo, SeqNo, @SalesType,@StoreCode, @RegisterCode, @BusDate, @TxnDate, A.ProdCode, B.ProdDesc1, B.DepartCode, 
          A.Qty, A.RetailPrice, A.RetailPrice, A.NetPrice, A.Qty*A.RetailPrice, A.Qty*A.RetailPrice, A.NetAmount, A.Qty, A.NetPrice - A.RetailPrice, A.NetAmount - A.Qty*A.RetailPrice, 0,
          '', A.Additional, '', '', 0, '', B.BOM, B.CouponSKU, 0, B.VisuaItem,
          0, 1, Serialno, '', 'G', CASE B.ProductType WHEN 0 THEN 0 ELSE 9 END, ReservedDate, B.DefaultPickupStoreCode, @UserID, 
          @DeliveryDate, @Deliveryer, 0, 0, '', RefSeqNo, '', '', @UserID, GETDATE(), @UserID, GETDATE()
      FROM @SalesD A left join Buy_Product B on A.ProdCode = B.ProdCode
    END  

    --写入Tender detail数据      
    IF ISNULL(@SalesTender, '') <> ''
    BEGIN
      IF EXISTS(SELECT TransNum FROM sales_T WHERE TransNum = @TxnNo)
        DELETE FROM sales_T WHERE TransNum = @TxnNo    
      INSERT INTO Sales_T(SeqNo, TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,TenderID,TenderCode,TenderDesc,
          TenderType,TenderAmount,LocalAmount,ExchangeRate,PaymentType,Additional,Status,PayConfirmDate,CardNumber,CardType,
          CardHolder,CardApprovalCode,CardExpiryDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
      SELECT SeqNo, @TxnNo, @SalesType,@StoreCode, @RegisterCode, @BusDate,@TxnDate, A.TenderID, A.TenderCode, A.TenderName, 
          B.TenderType, A.TenderAmount, A.LocalAmount, A.ExchangeRate, 0, A.SalesTAdditional, 1, @Busdate, substring(isnull(A.SalesTAdditional,''),1,64), B.CardType, 
          '', '', '', GETDATE(), @UserID, GETDATE(),@UserID 
      FROM @SalesT A left join BUY_CURRENCY B ON A.TenderCode = B.CurrencyCode
    END

    --写入Discount detail数据
    IF ISNULL(@SalesDisc, '') <> ''
    BEGIN
      IF EXISTS(SELECT TransNum FROM sales_Disc WHERE TransNum = @TxnNo)
        DELETE FROM sales_Disc WHERE TransNum = @TxnNo    
      INSERT INTO sales_Disc(TransNum, ItemSeqNo, ProdCode, TenderSeqNo, TenderID, TenderCode, SalesDiscCode, SalesDiscDesc,  
          SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, 
          SalesDiscLevel,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)      
      SELECT @TxnNo, ItemSeqNo, ProdCode, TenderSeqNo, TenderID, TenderCode, SalesDiscCode, SalesDiscDesc,  
          SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, 
          SalesDiscLevel, GETDATE(), @UserID, GETDATE(),@UserID 
      FROM @SalesDS A 
    END
  
   -- 写入sales_H数据    
    IF ISNULL(@OldTxnStatus, -1) <> -1
    BEGIN 
      IF EXISTS(SELECT TransNum FROM Sales_H WHERE TransNum = @TxnNo)     
      BEGIN
        UPDATE sales_H SET [Status] = @TxnStatus, SalesReceipt = CASE WHEN ISNULL(@SalesReceipt,'') <> '' THEN @SalesReceipt ELSE SalesReceipt END, 
            UpdatedOn = GETDATE(), UpdatedBy = @UserID, SalesReceiptBIN = case when (@SalesReceiptBIN is not null) THEN @SalesReceiptBIN ELSE SalesReceiptBIN END,
            DeliveryNumber = @DeliveryNumber, TotalAmount = @TotalAmount,PickupType = @PickupType,
			Contact=@CustomerName, ContactPhone=@CustomerPhone,	PickupStoreCode = @PickupStoreCode,
			DeliveryFullAddress = @DeliveryaAddr   /*1.0.1.8*/
        WHERE TransNum = @TxnNo
      END
    END ELSE
    BEGIN
      INSERT INTO sales_H(TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,TotalAmount,Status,
          TransDiscount,TransDiscountType,TransReason,RefTransNum,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
          DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
          RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
          SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,MemberMobilePhone)
      VALUES
          (@TxnNo, @SalesType,@StoreCode, @RegisterCode, @BusDate, @TxnDate, @CashierID, @SalesManID, @TotalAmount, @TxnStatus, 
          0, 0, '', @RefTxnNo, 0, @MemberSalesFlag, @MemberID, @CardNumber,  @DeliveryFlag, 
          '', '', '', '', '', @DeliveryaAddr, @DeliveryNumber, 
          @DeliveryDate, @DeliveryCompleteDate, @Deliveryer, @CustomerName, @CustomerPhone, @PickupType, @PickupStoreCode, @CODFlag, @remark, null,
          0, getdate(), @SalesReceipt, @SalesReceiptBIN,  getdate(), @UserID, getdate(), @UserID,@MEMBERMOBILEPHONE)	
    END


    -- 如果是Void交易,必须更改原单.
    IF @SalesType = 9
    BEGIN
      UPDATE Sales_H SET InvalidateFlag = 1, RefTransNum = @TxnNo, UpdatedOn = GETDATE(), UpdatedBy = @CashierID 
      WHERE TransNum = @RefTxnNo
    END
    -- 如果是Refund交易,必须更改原单.
    IF @SalesType = 10
    BEGIN
      UPDATE Sales_H set InvalidateFlag = 2, RefTransNum = @TxnNo, UpdatedOn = GETDATE(), UpdatedBy = @CashierID 
      WHERE TransNum = @RefTxnNo
	  UPDATE Sales_D SET Collected = D.Collected + 80, UpdatedOn = GETDATE(), UpdatedBy = @CashierID 
	    FROM Sales_D D LEFT JOIN @SalesD X ON D.SeqNo = X.RefSeqNo
      WHERE TransNum = @RefTxnNo AND X.SeqNo IS NOT NULL
    END

    -- 首次付款完成
    IF (@IsShoppingCart = 0) and (isnull(@OldTxnStatus, -1) <> @TxnStatus) and (@TxnStatus = 4 or @TxnStatus = 5)	and @OldTxnStatus < 4
    BEGIN
		-- 如果是Exchange交易,必须更改原单.
		IF @SalesType = 11
		BEGIN
		  UPDATE Sales_H set InvalidateFlag = 3, RefTransNum = @TxnNo, UpdatedOn = GETDATE(), UpdatedBy = @CashierID 
		  WHERE TransNum = @RefTxnNo
		  UPDATE Sales_D SET Collected = D.Collected + 90, UpdatedOn = GETDATE(), UpdatedBy = @CashierID 
			FROM Sales_D D LEFT JOIN @SalesD X ON D.SeqNo = X.RefSeqNo
		  WHERE TransNum = @RefTxnNo AND X.SeqNo IS NOT NULL AND X.Qty < 0
		END

      -- 预扣库存。
	  EXEC @ReserverResult = ReserveSalesStock @TxnNo

      -- 产生pickup单
      EXEC GenSalesPickupOrder @UserID, @TxnNo, 1             -- for bauhaus

--print '@IsMemberSales'
--print @IsMemberSales

--	  IF @IsMemberSales = 1

	  BEGIN
--print '@IsDealWithSalesData'
--print @IsDealWithSalesData
		-- 卡充值, 销售Coupon
		IF @IsDealWithSalesData = 1   -- 处理salesD和salesT数据
		BEGIN
		      DECLARE @QTY INT 
			  DECLARE CUR_SalesD CURSOR FAST_FORWARD LOCAL FOR  
				SELECT NetAmount, D.Additional, ProductType,Qty FROM @SalesD D left join Buy_Product P ON D.Prodcode = P.ProdCode WHERE P.ProductType in (2, 3, 4)
			  OPEN CUR_SalesD  
			  FETCH FROM CUR_SalesD INTO @AddValue, @AddValueCardNumber, @ProdType,@QTY
			  WHILE @@FETCH_STATUS=0  
			  BEGIN
				IF ISNULL(@AddValueCardNumber, '') = ''
				  SET @AddValueCardNumber = @CardNumber
				IF @ProdType = 2	  -- 卡充值金额
				BEGIN
				  SET @OprID = 2
				  IF @SalesType in (9, 10) or (@SalesType = 11 and @QTY < 0)
					SET @OprID = 4
				  EXEC @Result = MemberCardSubmit @UserID, @AddValueCardNumber, @StoreCode, @RegisterCode, @ServerCode, '', 
				    @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID, @AddValue, 0, @VoidTxnNo, '', '', '', '', @ReturnAmount output, 
					@ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output
				  IF @AddValueCardNumber = @CardNumber
					SET @MemberCardAmountAct = @AddValue		  
				END
				IF @ProdType = 4	  -- 卡充值积分
				BEGIN
				  SET @OprID = 14
				  IF @SalesType in (9, 10) or (@SalesType = 11 and @QTY < 0)
					SET @OprID = 4
				  SELECT @CardAmountToPointRate = isnull(CardAmountToPointRate, 0) 
				    FROM CARD C left join CardGrade G ON C.CardGradeID = G.CardGradeID WHERE C.CardNumber = @AddValueCardNumber
				  SET @AddPoint = @AddValue  / @CardAmountToPointRate

				  IF @AddValueCardNumber = @CardNumber				 
					SET @MemberCardPointAct = @AddPoint				  
  				  EXEC @Result = MemberCardSubmit @UserID, @AddValueCardNumber, @StoreCode, @RegisterCode, @ServerCode, '', 
				    @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID, 0, @AddPoint, @VoidTxnNo, '', '', '', '', @ReturnAmount output, 
					@ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output
				END
--print '@ProdType'
--print @ProdType
			    -- ver 1.0.2.15
			    IF @ProdType = 3	  -- 销售Coupon货品
			    BEGIN
			      SET @OprID = 33
			      IF @SalesType in (9, 10) or (@SalesType = 11 and @QTY < 0)
				    SET @OprID = 35
                  EXEC @Result = MemberCouponSubmit @UserID,@CardNumber,@AddValueCardNumber,@StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID,0,'','','','','',@ReturnAmount OUTPUT,@ReturnStatus OUTPUT,@ReturnMessageStrCardSubmit OUTPUT,0,'',0
--print @Result
			    END

				IF @Result <> 0
				BEGIN
				  IF @ForceToComplete = 1				  
					BREAK
				  ELSE
				  BEGIN 
					ROLLBACK TRAN        
					RETURN @RESULT
				  END
				END  
				FETCH FROM CUR_SalesD INTO @AddValue, @AddValueCardNumber, @ProdType, @QTY
			  END  
			  DEALLOCATE CUR_SalesD 

			  IF @SalesType NOT IN (9,10,11)  -- 退换货等交易不出来tender的情况
			  BEGIN
				  -- 卡消费,使用Coupon
				  DECLARE CUR_SalesT CURSOR FAST_FORWARD LOCAL FOR  
					SELECT TenderType, LocalAmount, SalesTAdditional, TenderID, TenderCode FROM @SalesT WHERE TenderType in (7,15,16)
				  OPEN CUR_SalesT 
				  FETCH FROM CUR_SalesT INTO @TenderType, @LocalAmount, @SalesTAdditional, @TenderID, @TenderCode
				  WHILE @@FETCH_STATUS=0  
				  BEGIN
					IF @TenderType = 16     -- 卡消费
					BEGIN
					  IF ISNULL(@SalesTAdditional, '') = ''
						SET @SalesTAdditional = @CardNumber      
					  SET @OprID = 3
					  IF @SalesType IN (9, 10)
						SET @OprID = 4
					  EXEC @Result = MemberCardSubmit @UserID, @SalesTAdditional, @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, @OprID, @LocalAmount, 0, @VoidTxnNo, '', '', '', '', @ReturnAmount output, @ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output
					  IF @SalesTAdditional = @CardNumber				  
						SET @MemberCardAmountAct = - ABS(@LocalAmount)				    	    
					END 
					IF @TenderType = 15     -- 卡积分消费 
					BEGIN
					  IF ISNULL(@SalesTAdditional, '') <> ''
						SET @AddPoint =  -ABS(CAST(@SalesTAdditional AS INT))   
					  ELSE  
						SET @AddPoint = 0
					  SET @OprID = 13
					  IF @SalesType in (9, 10)
						SET @OprID = 4
					  EXEC @Result = MemberCardSubmit @UserID, @CardNumber, @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, 
						@Busdate, @Txndate, @OprID, 0, @AddPoint, @VoidTxnNo, '', '', '', '', @ReturnAmount output, 
						@ReturnPoint output, @CardExpiryDate output, @ReturnMessageStrCardSubmit output

					  SET @MemberCardPointAct = - ABS(@AddPoint) 	     
					END       
					if (@TenderType = 7)     -- Coupon支付   (@SalesTAdditional 是couponnumber,必须有值)	(增加类型 8)
					begin
					  if (@SalesType not in (9, 10))
					  begin
						exec @Result = MemberCardExchangeCoupon @UserID, @CardNumber, @SalesTAdditional, '', @StoreCode, @RegisterCode, @ServerCode, '', @TxnNo, @TxnNo, @Busdate, @Txndate, '', '', @ReturnCouponTypeAmount output, @ReturnCouponTypePoint output, @ReturnCouponTypeDiscount OUTPUT, @ReturnBindPLU output
						set @ActCoupon = @SalesTAdditional
					  end
					end

					if @Result <> 0
					begin
					  if @ForceToComplete = 1
					  begin 
						break
					  end
					  else
					  begin 
						rollback tran        
						return @Result
					  end
					end  
				  FETCH FROM CUR_SalesT INTO @TenderType, @LocalAmount, @SalesTAdditional, @TenderID, @TenderCode
				END  
				  CLOSE CUR_SalesT   
				  DEALLOCATE CUR_SalesT 
			  END
		END
  

           -- 计算积分. 
           IF @IsCalcPoint = 1 and @IsMemberSales = 1
           BEGIN
             IF @SalesType not in (9, 10)           
               EXEC AutoPromoteForEarchSales @TxnNo

			   -- 计算return,void时归还积分.
				--ver 1.0.0.12
				if @SalesType = 9 
				begin
				  declare @VoidPoint int, @AfterVoid int, @VoidCouponNumber varchar(64), @VoidCouponStatus int 

				  select @VoidPoint = Points from Card_Movement 
					where RefTxnNO = @VoidTxnNo and OprID = 27 and CardNumber = @CardNumber
				  if ISNULL(@VoidPoint, 0) <> 0
				  begin  
					set @AfterVoid = isnull(@CardTotalPoints,0) - isnull(@VoidPoint, 0)      
					if @ForceToComplete = 0 and @AfterVoid < 0
					begin
					  rollback tran
					  Return -58
					end
             
					insert into Card_Movement
						(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
						 CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
						   OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
					select 4, CardNumber, KeyID, null, @TxnNo, OpenBal, Amount, CloseBal, - Points, @BusDate, @Txndate,
						 null, null, Additional, 'SVASalesSubmit Void', '', CreatedBy, StoreID, RegisterCode, ServerCode,
						  @CardTotalPoints, @CardTotalPoints - Points, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus
					  from Card_Movement where RefTxnNO = @VoidTxnNo and OprID = 27 and CardNumber = @CardNumber   -- 取出原单交易的 promotion 加积分的movement记录。 
					if @AfterVoid < 0 and @ForceToComplete = 1  
					begin
					  insert into Card_Movement
						(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
						 CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
  						 OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
					  select 90, CardNumber, KeyID, null, @TxnNo, OpenBal, Amount, CloseBal,  -(@AfterVoid), @BusDate, @Txndate,
						 null, null, Additional, 'SVASalesSubmit Void', '', CreatedBy, StoreID, RegisterCode, ServerCode,
						   @AfterVoid, 0, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus
					   from Card_Movement where RefTxnNO = @VoidTxnNo and OprID = 27 and CardNumber = @CardNumber   -- 取出原单交易的 promotion 加积分的movement记录。 
					end 
				  end
     
				  insert into Coupon_Movement
						(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
						 BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode,ServerCode, StoreID, RegisterCode,
						 OrgExpiryDate, OrgStatus, NewStatus) 
				  select 35, A.CardNumber, A.CouponNumber, A.CouponTypeID, A.KeyID, null, @TxnNo, A.OpenBal, A.Amount, A.CloseBal, 
						 @BusDate, @Txndate, 'SVASalesSubmit Void', '', A.CreatedBy, A.NewExpiryDate, A.ApprovalCode, A.ServerCode, A.StoreID, A.RegisterCode,
						   A.OrgExpiryDate, 2, 5
					 from Coupon_Movement A left join Coupon B on A.CouponNumber = B.CouponNumber
					 where RefTxnNO = @VoidTxnNo and OprID = 27 and A.CardNumber = @CardNumber and A.CouponNumber =@VoidCouponNumber              
						 and B.Status = 2  
			 
				  insert into Coupon_Movement
						(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
						 BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode,ServerCode, StoreID, RegisterCode,
						 OrgExpiryDate, OrgStatus, NewStatus) 
				  select 41, A.CardNumber, A.CouponNumber, A.CouponTypeID, A.KeyID, null, @TxnNo, A.OpenBal, A.Amount, A.CloseBal, 
						 @BusDate, @Txndate, 'SVASalesSubmit Void', '', A.CreatedBy, A.NewExpiryDate, A.ApprovalCode, A.ServerCode, A.StoreID, A.RegisterCode,
						   A.OrgExpiryDate, 3, 2
					 from Coupon_Movement A left join Coupon B on A.CouponNumber = B.CouponNumber
					 where RefTxnNO = @VoidTxnNo and OprID in (34,54) and A.CardNumber = @CardNumber --and A.CouponNumber =@VoidCouponNumber              
						 and B.Status = 3  			       
				end
    
    
				--ver 1.0.0.14  -- ver 1.0.0.15
				if @SalesType in (10, 11) 
				begin
				  declare @reducePoint int, @Afterreduce int, @reduceamt money, @AmtSign int
				  set @AmtSign = sign(@TotalAmount)
				  set @reduceamt = abs(@TotalAmount)
				  exec CalcEarnPoints @CardNumber, @reduceamt, @reducePoint output, 0, @StoreID 
      
				  if @SalesType = 10
					set @AmtSign = -1
				  if ISNULL(@reducePoint, 0) <> 0
				  begin  
					set @reducePoint = isnull(@reducePoint, 0) * @AmtSign        
					set @Afterreduce = isnull(@CardTotalPoints,0) + @reducePoint    
					if @ForceToComplete = 0 and @Afterreduce < 0
					begin
					  rollback tran
					  Return -58
					end

					if @reducePoint < 0   -- 只有扣除积分时才做。需要加积分的话，使用AutoPromoteForEarchSales
					begin
						insert into Card_Movement
							(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
							 CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
							   OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
						values ( 4, @CardNumber, null, null, @TxnNo, @ReturnAmount, 0, @ReturnAmount, @reducePoint, @BusDate, @Txndate,
							 null, null, '', 'SVASalesSubmit Return', '', @UserID, @StoreID, @RegisterCode, @ServerCode,
							  @ReturnPoint, @ReturnPoint + @reduceamt, @ExpiryDate, @ExpiryDate, @CardStatus, @CardStatus)
	                end

					if @Afterreduce < 0 and @ForceToComplete = 1  
					begin
					  insert into Card_Movement
						(OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
						 CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
  						 OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus) 
					  values( 90, @CardNumber, null, null, @TxnNo, @ReturnAmount, 0, @ReturnAmount, -(@Afterreduce), @BusDate, @Txndate,
						 null, null, '', 'SVASalesSubmit Return', '', @UserID, @StoreID, @RegisterCode, @ServerCode,
						  @Afterreduce, 0, @ExpiryDate, @ExpiryDate, @CardStatus, @CardStatus)
					end 
				  end                    
				end     
				----------------
			END
		  IF ISNULL(@CardNumber,0) <> ''
			UPDATE Card SET CumulativeConsumptionAmt = CumulativeConsumptionAmt + ISNULL(@TotalAmount,0) WHERE CardNumber = @CardNumber
		  -------------------	    
		  SET @ReturnMessageStr = '' 
		  EXEC UpdateCardGrade @CardNumber, 0, @TotalAmount
	  END

		/*分配coupon=====================================================*/          
		DECLARE CUR_SalesD CURSOR fast_forward local FOR
		  select B.CouponTypeID, D.TotalQty             
		 	from CouponTypeExchangeBinding B 
			left join (select ProdCode, sum(qty) as TotalQty from sales_D where TransNum = @TxnNo and qty > 0 group by Prodcode) D on D.Prodcode = B.ProdCode               
			where B.BindingType = 1 and isnull(D.ProdCode, '') <> ''        
		OPEN CUR_SalesD  
		FETCH FROM CUR_SalesD INTO @BindCouponTypeID, @BindCouponCount  
		WHILE @@FETCH_STATUS=0  
		BEGIN
		set @CouponNumber = ''
		set @i = 1
		set @BindCouponCount = ISNULL(@BindCouponCount, 0)
		WHILE @i <= @BindCouponCount
		BEGIN
			exec GetCouponNumber @BindCouponTypeID, 1, @CouponNumber output  
			if isnull(@CouponNumber, '') <> ''
			begin
				exec GenApprovalCode @ApprovalCode output
				exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
				exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
				insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
					BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
					OrgExpirydate, OrgStatus, NewStatus)    
				select  
					32, @CardNumber, CouponNumber, CouponTypeID, '', 0, '', CouponAmount, 0, CouponAmount,
					@BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
					@CouponExpiryDate, 1, 2
				from Coupon where CouponNumber = @CouponNumber
			end
			set @i = @i + 1			   
		END
            
		FETCH FROM CUR_SalesD INTO @BindCouponTypeID, @BindCouponCount  
		END  
		CLOSE CUR_SalesD   
		DEALLOCATE CUR_SalesD 
		/*=====================================================*/  			  
    END


  IF @Result = 0  		  
    DELETE FROM MemberShoppingCart WHERE MemberID = @MemberID and ProdCode IN (SELECT ProdCode FROM @SalesD)		 

  IF @RESULT = 0 
    COMMIT TRAN
  ELSE 
    ROLLBACK TRAN 

  RETURN 0
END


GO
