USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_BRAND]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_BRAND](
	[BrandID] [int] IDENTITY(1,1) NOT NULL,
	[BrandCode] [varchar](64) NOT NULL,
	[BrandName1] [nvarchar](512) NULL,
	[BrandName2] [nvarchar](512) NULL,
	[BrandName3] [nvarchar](512) NULL,
	[BrandDesc1] [nvarchar](max) NULL,
	[BrandDesc2] [nvarchar](max) NULL,
	[BrandDesc3] [nvarchar](max) NULL,
	[BrandPicSFile] [nvarchar](512) NULL,
	[BrandPicMFile] [nvarchar](512) NULL,
	[BrandPicGFile] [nvarchar](512) NULL,
	[CardIssuerID] [int] NULL,
	[IndustryID] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_BRAND] PRIMARY KEY CLUSTERED 
(
	[BrandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandPicSFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'中图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandPicMFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'大图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'BrandPicGFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行方ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'CardIssuerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND', @level2type=N'COLUMN',@level2name=N'IndustryID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌表。
@2016-08-09   定义为货品的品牌。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BRAND'
GO
