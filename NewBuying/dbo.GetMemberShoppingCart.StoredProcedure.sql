USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberShoppingCart]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetMemberShoppingCart]
  @MemberID             bigint,               -- 会员ID
  @ConditionStr         nvarchar(400),
  @PageCurrent          int=1,-- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize             int=0,             -- 每页记录数， 为0时，不分页，默认0
  @PageCount            int=0 output,      -- 返回总页数。
  @RecordCount          int=0 output,      -- 返回总记录数。
  @LanguageAbbr         varchar(20)=''  
AS
/****************************************************************************
**  Name : GetMemberShoppingCart  
**  Version: 1.0.0.8
**  Description : 获取会员的购物车内信息 (同一个memberid的 购物车和收藏中，product必须唯一)
**  Parameter :
  declare @a int , @PageCount int, @RecordCount int  
  exec @a = GetMemberShoppingCart 705, '', 1,10, @PageCount output, @RecordCount output,  'zh_BigCN'
  print @a
  print @PageCount 
  print @RecordCount  
   select * from MemberShoppingCart
**  Created by: Gavin @2013-12-20
**  Modified by: Gavin @2013-12-26 (ver 1.0.0.1) 增加分页
**  Modified by: Robin @2013-12-27 (ver 1.0.0.2) 修改取FavoriteType的值，从0改为1，商品收藏
**  Modified by: Gavin @2014-01-07 (ver 1.0.0.3) 增加返回UpdatedOn字段，用户可以根据此字段自定义过滤条件。
**  Modified by: Gavin @2015-07-21 (ver 1.0.0.4) MemberShoppingCart表增加字段 status. 这个过程需要添加返回
**  Modified by: Gavin @2015-08-19 (ver 1.0.0.5) MemberFavorite增加prodcode, 和MemberShoppingCart关联时使用prodcode
**  Modified by: Gavin @2016-08-18 (ver 1.0.0.6) 调用方无法读取 varchar(max)类型， 所以转换成TEXT的。（调用方使用的是旧版2005）
**  Modified by: Gavin @2016-10-17 (ver 1.0.0.7) @MemberID 类型由int改为 bigint
**  Modified by: Gavin @2017-04-24 (ver 1.0.0.8) 价格不再从MemberShoppingCart中获取，而是从Product_Price中获取。
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  if @Language = 2
  set @MemberID = isnull(@MemberID, 0)
  
  set @SQLStr = --'select C.KeyID, C.MemberID, C.ProdCodeStyle, C.ProdCode, C.ProdDesc, C.ProdPIC, C.Qty, C.RetailPrice, C.RetailAmount, CAST(C.memo as TEXT) as Memo,  '
               'select C.KeyID, C.MemberID, C.ProdCodeStyle, C.ProdCode, C.ProdDesc, C.ProdPIC, C.Qty, P.NetPrice as RetailPrice, (P.NetPrice * C.Qty) as RetailAmount, CAST(C.memo as nTEXT) as Memo,  '
               + ' isnull(case F.KeyID when null then 0 else F.KeyID end, 0) as IsFavorite, C.ProdTitle, C.UpdatedOn, C.Status '
               + ' from MemberShoppingCart C '
               --+ ' left join (select * from MemberFavorite where FavoriteType = 1 and MemberID = ' + cast(@MemberID as varchar) + ') F  on C.ProdCodeStyle = F.ProdCodeStyle '
               + ' left join (select * from MemberFavorite where FavoriteType = 1 and MemberID = ' + cast(@MemberID as varchar) + ') F  on C.ProdCode = F.ProdCode '
			   
			   --ver 1.0.0.8
               + ' left join (select ProdCode, NetPrice from Product_Price where KeyID in '
               + ' ( ' 
               + ' select max(KeyID) as IID from Product_Price'
               + ' where status = 1 and DateDiff(DD, StartDate, GetDate()) > 0 and DateDiff(DD, GetDate(), EndDate) > 0'
               + '  and ProdCode in (select ProdCode from MemberShoppingCart where MemberID = ' + cast(@MemberID as varchar) + ')'
               + ' group by ProdCode '
			   + ' ) ) P on C.ProdCode = P.ProdCode '

               + ' where C.MemberID = ' + cast(@MemberID as varchar)

  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr     
  return 0
end

GO
