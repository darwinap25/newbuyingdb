USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardReceive_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardReceive_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardReceiveNumber] [varchar](64) NOT NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[Description] [nvarchar](512) NULL,
	[OrderQTY] [int] NULL,
	[ActualQTY] [int] NULL,
	[FirstCardNumber] [varchar](64) NULL,
	[EndCardNumber] [varchar](64) NULL,
	[BatchCardCode] [varchar](64) NULL,
	[CardStockStatus] [int] NULL,
	[ReceiveDateTime] [datetime] NULL,
	[OrderAmount] [money] NULL,
	[ActualAmount] [money] NULL,
	[OrderPoint] [int] NULL,
	[ActualPoint] [int] NULL,
 CONSTRAINT [PK_ORD_CARDRECEIVE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CardReceive_D] ADD  DEFAULT ((2)) FOR [CardStockStatus]
GO
ALTER TABLE [dbo].[Ord_CardReceive_D] ADD  DEFAULT (getdate()) FOR [ReceiveDateTime]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'CardReceiveNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述。（例如改为损坏状态时填写的具体描述）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货的订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'OrderQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收到的数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'ActualQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收货批次的首Card号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'FirstCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收货批次的末Card号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'EndCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstCardNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'BatchCardCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收到的Card状况。默认 2
0：未定
1：Damaged (總部發現有優惠劵損壞)
2. Good For Release (總部確認收貨)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'CardStockStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建此明细的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'ReceiveDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单收到金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'OrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收到金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'ActualAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单收到积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'OrderPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收到积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D', @level2type=N'COLUMN',@level2name=N'ActualPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货确认单子表
把ActAmount 改成 ActualAmount， 为了和ActualQty 统一名称， 其他表不改了。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_D'
GO
