USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenApprovalCode]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenApprovalCode]
  @ApprovalCode varchar(6) output
AS
/****************************************************************************
**  Name : GenApprovalCode
**  Version: 1.0.0.3
**  Description : 产生approval code。 Approval code 为 seqno + 校验位 = 6位， 校验位计算规则：seqno的 ascii值* 倒数位置后 依次相加，取10的余数, 再用10减。结果如果为10，则作为0
**  Example :
  declare @ApprovalCode varchar(6), @A int
  exec @A = GenApprovalCode @ApprovalCode output
  print @A
  print @ApprovalCode   005014, 005023  005256
**  Created by: Gavin @2012-02-20
**  Modify by: Gavin @2012-09-06 (ver 1.0.0.1)当SeqNo 大于 5位时，则清零。
**  Modify by: Gavin @2012-09-06 (ver 1.0.0.2)直接用随机数来获得 5位数,不再从refno中读取。
**  Modify by: Gavin @2012-09-13 (ver 1.0.0.3)随机数加上时间作为种子数,降低重复概率
**
****************************************************************************/
begin
  declare @Seq int, @ApprovalCodeSeq varchar(20), @i int, @sum int, @CheckDigit int, @AppSeq int
  declare @seed int
  set @seed = round(abs(cast(getdate() as float) * 10000000 - round(cast(getdate() as float) * 10000000, 0)) * 100000, 0)
    
  set @ApprovalCode = Right('000000' + LTRIM(str(round(rand(@seed) * 100000, 0))), 5)
  if Len(@ApprovalCode) <> 5
    set @ApprovalCode = '00001'
  set @i = 1
  set @sum = 0
  set @CheckDigit = 0
  while @i <= 5 
  begin    
    set @sum = @sum + (ASCII(SubString(@ApprovalCode, @i, 1)) * (6 - @i))   
    set @i = @i + 1
  end  
  set @CheckDigit = @sum % 10 
  set @CheckDigit = 10 - @CheckDigit
  if @CheckDigit = 10 
    set @CheckDigit = 0 
  set @ApprovalCode = @ApprovalCode + convert(varchar(1), @CheckDigit) 

  return 0    
end

GO
