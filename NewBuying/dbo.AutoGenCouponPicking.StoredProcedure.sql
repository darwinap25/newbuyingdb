USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoGenCouponPicking]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AutoGenCouponPicking]
  @CouponAutoPickingRuleCode varchar(64)='',
  @IsSchedule           int=0              -- 是否 日程安排 自动执行. 0:不是. 1: 是的  
AS
/****************************************************************************
**  Name : AutoGenCouponPicking 
**  Version: 1.0.0.2
**  Description : Coupon订单的日常操作 (一般在EOD中调用)
**  Parameter :  （for RRG） 实体Coupon订单的日常操作。（包含用户定制的业务要求）
**
**  Created by: Gavin @2014-07-09
**  Modified by: Gavin @2014-07-13 (ver 1.0.0.1) 增加输入参数 @CouponOrderFormNumber
**  Modified by: Gavin @2014-11-07 (ver 1.0.0.2) 增加Card的 picking单生成. 逻辑都一样, 所以 表名和过程名字都没有修改. 设置的窗体也不需要修改. 直接增加card部分. 
select * from ord_couponpicking_D
**
****************************************************************************/
begin  
  declare @OrderFormNumber varchar(64), @StoreID int, @BrandID int
  set @CouponAutoPickingRuleCode = isnull(@CouponAutoPickingRuleCode,'')

  DECLARE CUR_DoDailyOperCouponOrder CURSOR fast_forward FOR  
    select H.CouponOrderFormNumber, H.StoreID, S.BrandID from Ord_CouponOrderForm_H H left join Store S on H.StoreID = S.StoreID        
      where H.Approvestatus = 'A' 
  OPEN CUR_DoDailyOperCouponOrder
  FETCH FROM CUR_DoDailyOperCouponOrder INTO @OrderFormNumber, @StoreID, @BrandID
  WHILE @@FETCH_STATUS=0
  BEGIN
    if exists (select StoreID, BrandID from CouponAutoPickingRule_d D 
                  left join CouponAutoPickingRule_H H on H.CouponAutoPickingRuleCode = D.CouponAutoPickingRuleCode
               where ( (@IsSchedule = 0) or (
                           ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
                       and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
                       and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )  
                       ) 
		             ) and (StoreID = @StoreID or BrandID = @BrandID) 
		             and (D.CouponAutoPickingRuleCode = @CouponAutoPickingRuleCode or @CouponAutoPickingRuleCode = '')
		       )  		             
	  begin
	    update Ord_CouponOrderForm_H set Approvestatus = 'C' where CouponOrderFormNumber = @OrderFormNumber
	  end	 
	            		  
    FETCH FROM CUR_DoDailyOperCouponOrder INTO @OrderFormNumber, @StoreID, @BrandID
  END
  CLOSE CUR_DoDailyOperCouponOrder
  DEALLOCATE CUR_DoDailyOperCouponOrder 

  -- for Card
  DECLARE CUR_DoDailyOperCardOrder CURSOR fast_forward FOR  
    select H.CardOrderFormNumber, H.StoreID, S.BrandID from Ord_CardOrderForm_H H left join Store S on H.StoreID = S.StoreID        
      where H.Approvestatus = 'A' 
  OPEN CUR_DoDailyOperCardOrder
  FETCH FROM CUR_DoDailyOperCardOrder INTO @OrderFormNumber, @StoreID, @BrandID
  WHILE @@FETCH_STATUS=0
  BEGIN
    if exists (select StoreID, BrandID from CouponAutoPickingRule_d D 
                  left join CouponAutoPickingRule_H H on H.CouponAutoPickingRuleCode = D.CouponAutoPickingRuleCode
               where ( (@IsSchedule = 0) or (
                           ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
                       and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
                       and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )  
                       ) 
		             ) and (StoreID = @StoreID or BrandID = @BrandID) 
		             and (D.CouponAutoPickingRuleCode = @CouponAutoPickingRuleCode or @CouponAutoPickingRuleCode = '')
		       )  		             
	  begin
	    update Ord_CardOrderForm_H set Approvestatus = 'C' where CardOrderFormNumber = @OrderFormNumber
	  end	 
	            		  
    FETCH FROM CUR_DoDailyOperCardOrder INTO @OrderFormNumber, @StoreID, @BrandID
  END
  CLOSE CUR_DoDailyOperCardOrder
  DEALLOCATE CUR_DoDailyOperCardOrder 
  
end

GO
