USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ResetMemberCardPassword]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ResetMemberCardPassword]
  @UserID               int,			--  操作员ID
  @CardNumber			varchar(512),   --  卡号
  @RandomPWD			int,			--  是否随机密码。
  @OldPassword			varchar(512),   --  会员旧密码    
  @NewPassword			varchar(512) output   --  会员新密码
AS
/****************************************************************************
**  Name : ResetMemberCardPassword  重置会员卡密码
**  Version: 1.0.0.1
**  Description : 重置会员卡密码。 @RandomPWD = 1时， 不校验旧密码，产生随机密码后返回参数。 @RandomPWD=0时，校验旧密码
**  Parameter :
**
 declare @NewPassword			varchar(512)
 set @NewPassword = '123456'
  exec ResetMemberCardPassword  1, '000100001', 0, '123', @NewPassword output
  print @NewPassword
  select * from card
**  Created by: Gavin @2012-05-21
**  Modify by: Gavin @2013-12-09 （ver 1.0.0.1）按照passwordrule的设置，设置密码的长度。
**
****************************************************************************/
begin
  if isnull(@OldPassword, '') <> ''
    set @OldPassword = dbo.EncryptMD5(@OldPassword) 

  declare @CardPassword varchar(512), @CardGradeID int, @PWDValidDays int, @PWDValidDaysUnit int, @ResetPWDDays int, @NewExpiryDate datetime,
		  @PasswordRuleID int
  declare @PWDMinLength  int, @RandNum decimal(20,0)
  
  select top 1 @PWDMinLength = PWDMinLength from passwordrule	where MemberPWDRule = 1
  set @PWDMinLength = isnull(@PWDMinLength, 6)
  if @PWDMinLength = 0 
    set @PWDMinLength = 6  
  select @CardPassword = C.CardPassword, @CardGradeID = C.CardGradeID, @PasswordRuleID = T.PasswordRuleID
    from card C left join CardType T on C.CardTypeID = T.CardTypeID
      left join CardGrade G on C.CardGradeID = G.CardGradeID where C.CardNumber = @CardNumber
  if @@ROWCOUNT = 0 
    return -2  
    
  select Top 1 @PWDValidDays = PWDValidDays, @PWDValidDaysUnit = PWDValidDaysUnit, @ResetPWDDays = ResetPWDDays from PasswordRule where PasswordRuleID = @PasswordRuleID
  if @@ROWCOUNT = 0 
  begin
    set @PWDValidDaysUnit = 0
    set @PWDValidDays = 10
  end  
     set @NewExpiryDate = convert(varchar(10), getdate(), 120)     
     if @PWDValidDaysUnit = 0
       set @NewExpiryDate = '2100-01-01'
     if @PWDValidDaysUnit = 1
       set @NewExpiryDate = DATEADD(yy, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 2
       set @NewExpiryDate = DATEADD(mm, @PWDValidDays, @NewExpiryDate)   
     if @PWDValidDaysUnit = 3
       set @NewExpiryDate = DATEADD(ww, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 4
       set @NewExpiryDate = DATEADD(dd, @PWDValidDays, @NewExpiryDate)  
         
  if @RandomPWD = 0 
  begin      
    if isnull(@CardPassword,'') = isnull(@OldPassword, '')
    begin
      if isnull(@NewPassword, '') <> ''
         set @NewPassword = dbo.EncryptMD5(@NewPassword)     
      Update Card set CardPassword = @NewPassword,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
           UpdatedBy = @UserID, UpdatedOn = Getdate()
        where CardNumber = @CardNumber
    end else  
      return -3      
  end else
  begin    
    select @RandNum = round(rand() * 100000000000000000, 0)
    set @NewPassword = right('00000000000000000' + convert(varchar(20), @RandNum), @PWDMinLength)  
    if isnull(@NewPassword, '') <> ''
       set @NewPassword = dbo.EncryptMD5(@NewPassword)     
    Update Card set CardPassword = @NewPassword,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
           UpdatedBy = @UserID, UpdatedOn = Getdate()
        where CardNumber = @CardNumber
  end  
  return 0
end

GO
