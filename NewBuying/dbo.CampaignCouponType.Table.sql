USE [NewBuying]
GO
/****** Object:  Table [dbo].[CampaignCouponType]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignCouponType](
	[CampaignID] [int] NOT NULL,
	[CouponTypeID] [int] NOT NULL,
 CONSTRAINT [PK_CAMPAIGNCOUPONTYPE] PRIMARY KEY CLUSTERED 
(
	[CampaignID] ASC,
	[CouponTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CampaignCouponType', @level2type=N'COLUMN',@level2name=N'CampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Type ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CampaignCouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Campaign和CouponType的关系表。 联合主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CampaignCouponType'
GO
