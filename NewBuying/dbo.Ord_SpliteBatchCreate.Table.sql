USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_SpliteBatchCreate]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_SpliteBatchCreate](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[RefNo] [varchar](64) NOT NULL,
	[CardOrCoupon] [int] NOT NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[CouponTypeID] [int] NULL,
	[Qty] [int] NOT NULL,
	[IssuedDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[InitAmount] [money] NULL,
	[InitPoints] [int] NULL,
	[InitStatus] [int] NULL,
	[RandomPWD] [int] NULL,
	[InitPassword] [varchar](512) NULL,
	[Status] [int] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_ORD_SPLITEBATCHCREATE] PRIMARY KEY NONCLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_SpliteBatchCreate] ADD  DEFAULT ((0)) FOR [InitAmount]
GO
ALTER TABLE [dbo].[Ord_SpliteBatchCreate] ADD  DEFAULT ((0)) FOR [InitPoints]
GO
ALTER TABLE [dbo].[Ord_SpliteBatchCreate] ADD  DEFAULT ((0)) FOR [InitStatus]
GO
ALTER TABLE [dbo].[Ord_SpliteBatchCreate] ADD  DEFAULT ((0)) FOR [RandomPWD]
GO
ALTER TABLE [dbo].[Ord_SpliteBatchCreate] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Ord_SpliteBatchCreate] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_SpliteBatchCreate] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'RefNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card还是Coupon。 1：Card。 2：Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'CardOrCoupon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建的数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'IssuedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'ExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'InitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'InitPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始状态。默认0。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'InitStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否初始随机密码。0：不是。1:是的。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'RandomPWD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始密码。RandomPWD=1时，不需要填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'InitPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态：0：等待创建。 1：创建完成。 -1： 取消创建' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批量创建 拆分订单表。
（Ord_CouponBatchCreate, Ord_CardBatchCreate 表批核后，自动拆分到此表等待创建，目前拆分基数1000）
配合过程：AutoBatchCreateNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SpliteBatchCreate'
GO
