USE [NewBuying]
GO
/****** Object:  Table [dbo].[CustomerComments]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerComments](
	[KeyID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[CustomerName] [varchar](64) NULL,
	[Comments] [nvarchar](max) NULL,
	[CustomerRating] [int] NULL,
	[ReplyContent] [nvarchar](512) NULL,
	[MemberID] [int] NULL,
	[Language] [varchar](64) NULL,
	[Title] [varchar](512) NULL,
	[VerifyFlag] [int] NULL DEFAULT ((0)),
	[UploadPicFile1] [varchar](512) NULL,
	[UploadPicFile2] [varchar](512) NULL,
	[UploadPicFile3] [varchar](512) NULL,
	[UploadPicFile4] [varchar](512) NULL,
	[UploadPicFile5] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_CUSTOMERCOMMENTS] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'顾客名称（NickName）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'CustomerName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'顾客评论' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'Comments'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'顾客评价（级别）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'CustomerRating'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商家答复' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'ReplyContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员语言' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'Language'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'核查标志。 0：没有核查过。1：已经核查. 默认0. 没有核查过的不显示' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'VerifyFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址1。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'UploadPicFile1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址2。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'UploadPicFile2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址3。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'UploadPicFile3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址4。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'UploadPicFile4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址5。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments', @level2type=N'COLUMN',@level2name=N'UploadPicFile5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'顾客评论表
@2016-02-15 for Bauhaus
@2016-04-06 增加  VerifyFlag
@2016-09-05 增加 createdon， updatedon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerComments'
GO
