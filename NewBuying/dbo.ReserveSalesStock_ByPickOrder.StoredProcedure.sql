USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ReserveSalesStock_ByPickOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[ReserveSalesStock_ByPickOrder]
  @UserID				         INT,	            -- 操作员ID
  @SalesPickOrderNumber          VARCHAR(64),       -- 拣货单号码
  @TxnNo                         VARCHAR(64),       -- 交易单号
  @NewStoreID                    INT                -- 拣货单中的pickuplocation正在更新,可能不准,另外输入
AS
/****************************************************************************
**  Name : ReserveSalesStock_ByPickOrder (bauhaus)
**  Version: 1.0.0.1
**  Description : 预扣库存,  WH 仓库是 hardcode 的。 根据ord_salespickorder 的内容来 预扣。 （适用场景：产生picking单后， 修改picking的提货仓库，此时需要返回之前的Reserver）
                  
select * from Ord_SalesPickOrder_H
select * from sales_D
update Ord_SalesPickOrder_D set actualqty = 4
select * from stk_stockonhand
select * from stk_stockmovement
select * from Ord_SalesPickOrder_D where status = 4
exec ReserveSalesStock_ByPickOrder 1,'SPUO00000000037', 'KTXNNO000000440'
select * 
		FROM Ord_SalesPickOrder_D D         -- D.PickupLocation 目前没使用 
		LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber
**  Created by: Gavin @2016-10-18 : 这个ReserveSalesStock目前只有POSSalesSubmit使用,为减少update sales_D次数,可以提交的collected直接是1， 那时只取collected=1的记录。
**  Modified by Gavin @2017-02-08 : (ver 1.0.0.1) 没有onhand记录时， OpenQty是 null，会出错， 改为 0.
**
****************************************************************************/
BEGIN
  DECLARE @StoreCode VARCHAR(64), @PickupLocation VARCHAR(64), @StockTypeCode VARCHAR(64), @ProdCode VARCHAR(64), @TotalQty INT,
          @PickupStoreCode VARCHAR(64), @StoreOnhandQty INT, @WHOnhandQty INT, @WHStoreID INT, @StoreID INT,
		  @BusDate DATETIME, @TxnDate DATETIME, @SerialNoType INT, @SerialNo VARCHAR(64), @CreatedBy INT, @SeqNo VARCHAR(64)
  DECLARE @ReserveQty INT, @WHReserveQty INT

  SELECT @WHStoreID = StoreID FROM BUY_STORE WHERE StoreCode = 'WH'
  SELECT @BusDate = BusDate, @TxnDate = TxnDate FROM Sales_H WHERE TransNum = @TxnNo
  SET @SerialNoType = ''
  SET @SerialNo = ''

  DECLARE CUR_ReserveSalesStock_P CURSOR fast_forward FOR
      SELECT D.StockTypeCode, D.ProdCode, D.OrderQty, O.OnhandQty, W.OnhandQty, H.Createdby
		FROM Ord_SalesPickOrder_D D      
		LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber
	    LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @NewStoreID) O ON D.ProdCode = O.ProdCode
		LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @WHStoreID) W ON D.ProdCode = W.ProdCode
	  WHERE D.SalesPickOrderNumber = @SalesPickOrderNumber
  OPEN CUR_ReserveSalesStock_P
  FETCH FROM CUR_ReserveSalesStock_P INTO @StockTypeCode, @ProdCode, @TotalQty, @StoreOnhandQty, @WHOnhandQty, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    SELECT @ReserveQty = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'R' AND ProdCode = @ProdCode AND StoreID = @NewStoreID
	SELECT @WHReserveQty = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'R' AND ProdCode = @ProdCode AND StoreID = @WHStoreID
    
	SET @StockTypeCode = 'G'
	SET @StoreOnhandQty = ISNULL(@StoreOnhandQty, 0)
	SET @WHOnhandQty = ISNULL(@WHOnhandQty, 0)
	SET @ReserveQty = ISNULL(@ReserveQty, 0)
	SET @WHReserveQty = ISNULL(@WHReserveQty, 0)

	-- insert 时使用values，即使STK_StockOnhand中没有数据，也可以插入movement记录。
	IF @StoreOnhandQty >= @TotalQty
	BEGIN
      INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	  VALUES (1, @NewStoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
	         isnull(@StoreOnhandQty,0), - isnull(@TotalQty,0), isnull(@StoreOnhandQty,0) - isnull(@TotalQty,0), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby )	   
      INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	  VALUES (1, @NewStoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
	         isnull(@ReserveQty,0), isnull(@TotalQty,0), isnull(@ReserveQty,0) + isnull(@TotalQty,0), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 	  
    END ELSE 
	BEGIN 
	  IF @StoreOnhandQty > 0 
	  BEGIN
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	      VALUES (1, @NewStoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
	          isnull(@StoreOnhandQty,0), - isnull(@StoreOnhandQty,0), 0, @SerialNoType, @SerialNo, '', GETDATE(), @Createdby )	   	   
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
		  VALUES (1, @NewStoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				 isnull(@WHReserveQty,0), isnull(@StoreOnhandQty,0), isnull(@WHReserveQty,0) + isnull(@StoreOnhandQty,0), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 

		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
		  VALUES (1, @WHStoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				 isnull(@WHOnhandQty,0), - (isnull(@TotalQty,0) - isnull(@StoreOnhandQty,0)), isnull(@WHOnhandQty,0) - (isnull(@TotalQty,0) - isnull(@StoreOnhandQty,0)), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 	   
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
		  VALUES (1, @WHStoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				 isnull(@WHReserveQty,0), (isnull(@TotalQty,0) - isnull(@StoreOnhandQty,0)), isnull(@WHReserveQty,0) + (isnull(@TotalQty,0) - isnull(@StoreOnhandQty,0)), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby)		  		  	    
	  END ELSE
	  BEGIN
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
		  VALUES (1, @WHStoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				 isnull(@WHOnhandQty,0), - isnull(@TotalQty,0), isnull(@WHOnhandQty,0) - isnull(@TotalQty,0), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby)
	   
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
		  VALUES (1, @WHStoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				 isnull(@WHReserveQty,0), isnull(@TotalQty,0), isnull(@WHReserveQty,0) + isnull(@TotalQty,0), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 
	  END  
	END
    FETCH FROM CUR_ReserveSalesStock_P INTO  @StockTypeCode, @ProdCode, @TotalQty, @StoreOnhandQty, @WHOnhandQty, @CreatedBy
  END
  CLOSE CUR_ReserveSalesStock_P 
  DEALLOCATE CUR_ReserveSalesStock_P

  RETURN 0
END

GO
