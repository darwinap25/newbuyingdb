USE [NewBuying]
GO
/****** Object:  Table [dbo].[PromotionCardCondition]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionCardCondition](
	[PromotionMsgID] [int] NOT NULL,
	[CardGradeID] [int] NOT NULL,
 CONSTRAINT [PK_PROMOTIONCARDCONDITION] PRIMARY KEY CLUSTERED 
(
	[PromotionMsgID] ASC,
	[CardGradeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PromotionMsg 主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionCardCondition', @level2type=N'COLUMN',@level2name=N'PromotionMsgID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Grade 主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionCardCondition', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销信息（广告）的卡条件。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionCardCondition'
GO
