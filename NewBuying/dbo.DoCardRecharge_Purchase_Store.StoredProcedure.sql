USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCardRecharge_Purchase_Store]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoCardRecharge_Purchase_Store]
  @UserID                   int,             --操作员ID
  @CardReceiveNumber        varchar(64),
  @IsVoid  int = 0
AS
/****************************************************************************
**  Name : DoCardRecharge_Purchase_Store
**  Version: 1.0.0.1
**  Description : 退货单产生的收货单批核.(店铺退给总部)

    exec DoCardRecharge_Purchase_Store
** 
**  Created by:  Gavin @2015-04-07
**  Modify by:  Gavin @2015-06-04 (ver 1.0.0.1) 增加校验,如果收货单中的卡不存在,则返回错误. trigger将会raise error
****************************************************************************/
begin
  declare @BusDate datetime, @TxnDate datetime
  declare @FirstNumber varchar(64), @EndNumber varchar(64), @CardTypeID int, @CardGradeID int, @CardCount int
  declare @IssuedDate datetime, @InitAmount money, @InitPoints int, @RandomPWD int, @InitPassword varchar(512), 
          @ExpiryDate datetime, @ApprovalCode varchar(512), @ReturnBatchID varchar(30), 
          @ReturnStartNumber varchar(64), @ReturnEndNumber varchar(64), @InitStatus int, @StoreID int
  declare @OrderAmount money, @OrderPoint int , @Sign int   
   
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()  
  if ISNULL(@IsVoid, 0) = 1
    set @Sign = -1  
  else
    set @Sign = 1
     
  -- ver 1.0.0.1 
  if exists(select * from Ord_CardReceive_D D left join Card C on D.FirstCardNumber = C.CardNumber 
              where D.CardReceiveNumber = @CardReceiveNumber and C.CardNumber is null)
    return -1
             
    insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus, ApprovalCode)
	  select 64, C.CardNumber, null, 0, D.CardReceiveNumber, C.TotalAmount, @Sign * D.ActualAmount, C.TotalAmount + (@Sign * D.ActualAmount), 0, @BusDate, @Txndate, 
	      null, null, null, null, '', @UserID, H.StoreID, '', '',
	      C.TotalPoints, C.TotalPoints, C.CardExpiryDate, C.CardExpiryDate, C.Status, C.Status, ''
	    from Ord_CardReceive_D D
	      left join Ord_CardReceive_H H on D.CardReceiveNumber = H.CardReceiveNumber
	      left join Card C on D.EndCardNumber = C.CardNumber
	   where H.PurchaseType = 2 and H.CardReceiveNumber = @CardReceiveNumber and D.ActualAmount > 0

    insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus, ApprovalCode)
	  select 65, C.CardNumber, null, 0, D.CardReceiveNumber, C.TotalAmount, @Sign * D.ActualAmount, C.TotalAmount - (@Sign * D.ActualAmount), 0, @BusDate, @Txndate, 
	      null, null, null, null, '', @UserID, H.StoreID, '', '',
	      C.TotalPoints, C.TotalPoints, C.CardExpiryDate, C.CardExpiryDate, C.Status, C.Status, ''
	    from Ord_CardReceive_D D
	      left join Ord_CardReceive_H H on D.CardReceiveNumber = H.CardReceiveNumber
	      left join Card C on D.FirstCardNumber = C.CardNumber
	   where H.PurchaseType = 2 and H.CardReceiveNumber = @CardReceiveNumber and D.ActualAmount > 0
	 
	 return 0  	   
end

GO
