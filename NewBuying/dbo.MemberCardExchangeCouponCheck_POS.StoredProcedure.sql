USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardExchangeCouponCheck_POS]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[MemberCardExchangeCouponCheck_POS]
  @CardNumber           varchar(64),            -- 会员卡号
  @CouponUID            varchar(64),            -- 使用的coupon号码  （--（未实现，目前只接受一个coupon）,多个coupon号码用“,” 隔开。）
  @StoreID              int,                    -- 店铺编号    
  @TotalAmount          money,                  -- 交易总额
  @SalesDetail			nvarchar(max),          -- 交易单货品明细(交易货品, XML格式字符串)
  @CouponDeductAmount   money output            -- 计算获得,此coupon在此订单中抵扣的金额
AS
/****************************************************************************
**  Name : MemberCardExchangeCouponCheck_POS  
**  Version: 1.0.0.3
**  Description :  使用coupon前的校验过程,并计算caoupon抵扣的金额
**  Parameter :
**
  declare @SchoolID int, @count int, @recordcount int, @a int  
  set @SchoolID = 0
  exec @a = MemberCardExchangeCouponCheck @SchoolID, N'SchoolDistrict = ''Islands''', '', 0, 2, 5, @count output, @recordcount output, 'zh_Big'
  print @a  
  print @count
  print @recordcount
  select * from languageMap
  select * from school
**  Created by: Gavin @2013-08-01
**  Modify by: Gavin @2013-08-05 (ver 1.0.0.1) 修正Product list 过滤条件问题
**  Modify by: Gavin @2013-10-30 (ver 1.0.0.2) 增加CouponTypeRedeemCondition表的条件判断
**  Modify by: Gavin @2014-09-19 (ver 1.0.0.3) 修改读入的XML格式。 修改检查绑定货品的逻辑 (考虑 是否使用Product_Catalog的情况, (一个货品属于多个部门))
                                               条件设定中,Cardtypeid,cardgradeid 小于等于0,即表示跳过此设定
                                               金额条件判定时,等于设定金额也算通过.
**  Modify by: Gavin @2016-03-14 (ver 1.0.0.4) 不检查Coupon的Expirydate，只检查状态。 等待 EOD时，根据expirydate来设置status 
**  Modify by: Vinnce @2016-11-04 (ver 1.0.0.5) Comment the limit value part
****************************************************************************/
begin
  declare @CardTypeID int, @CardStatus int, @CardExpiryDate datetime, @CardGradeID int, @CardTotalAmount money, 
          @TotalPoints int, @MemberID int, @CumulativeConsumptionAmt money, @CumulativeEarnPoints int
  declare @CouponStatus int, @CouponExpiryDate datetime, @ReturnCouponTypeAmount money,  @ReturnCouponTypePoint int,
          @ReturnCouponTypeDiscount decimal(16,6), @CouponNumber varchar(64), @CouponTypeID int, @LocateStore int, 
          @CouponIssueStoreID int, @CouponRedeemStoreID int, @CouponAmount money
  declare @SalesDData xml, @SaleDTotalAmt money                 
  declare @SalesD table (DepartCode varchar(64), ProductBrandID int, SeqNo int, ProdCode varchar(64), Collected int, RetailPrice money, NetPrice money, Qty int, NetAmount money)
  declare @ItemListTempStr varchar(64), @ItemsIndex int, @idoc int
  set @SaleDTotalAmt = 0
  set @SalesDetail = isnull(@SalesDetail, '')
  set @CouponDeductAmount = 0
  set @SaleDTotalAmt = isnull(@TotalAmount, 0)
   
  -- ver 1.0.0.3 
  if isnull(@SalesDetail, '') <> ''
  begin
    select @ItemsIndex = charindex('<Items>', @SalesDetail)
    if @ItemsIndex > 0 
      set @ItemListTempStr = '/Root/Items/ITEM'
    begin
       select @ItemsIndex = charindex('<PLU>', @SalesDetail)
       if @ItemsIndex > 0 
         set @ItemListTempStr = '/ROOT/PLU' 
       else      
         set @ItemListTempStr = '/ROOT/ITEM' 
    end    
  end
     
  -- 检查卡数据
if @CardNumber <> ''
begin
  select top 1 @CardTypeID = C.CardTypeID,  @CardStatus = C.Status, @CardExpiryDate = CardExpiryDate, 
       @CardGradeID = CardGradeID, @CardTotalAmount = isnull(TotalAmount, 0), @TotalPoints = isnull(TotalPoints, 0),
       @MemberID = MemberID, @CumulativeConsumptionAmt = isnull(CumulativeConsumptionAmt, 0), 
       @CumulativeEarnPoints = isnull(CumulativeEarnPoints, 0)
    from Card C left join CardType T on C.CardTypeID = T.CardTypeID
    where CardNumber = @CardNumber
  if @@ROWCOUNT = 0
    return -2
end
    
  -- 检查Coupon数据
  select @CouponNumber=CouponNumber from CouponUIDMap where CouponUID=@CouponUID  
  if isnull(@CouponNumber,'')=''   
    return -19 
  select @CouponStatus = C.Status, @CouponExpiryDate = C.CouponExpiryDate, @ReturnCouponTypeAmount = isnull(T.CouponTypeAmount, 0), 
        @ReturnCouponTypePoint = isnull(T.CouponTypePoint, 0), @ReturnCouponTypeDiscount = isnull(T.CouponTypeDiscount,0), @CouponTypeID = C.CouponTypeID,
        @LocateStore = T.LocateStore, @CouponIssueStoreID = isnull(C.StoreID, 0), @CouponRedeemStoreID = C.RedeemStoreID,
        @CouponAmount = CouponAmount
     from Coupon C left join CouponType T on C.CouponTypeID = T.CouponTypeID
    where C.CouponNumber = @CouponNumber
  if @@ROWCOUNT = 0
    return -19																	  
  -- Coupon状态是否可用
  if @CouponStatus <> 2
    return -20      
  -- Coupon是否过期 
  -- ver 1.0.0.4 remove this check 
  --if datediff(dd, @CouponExpiryDate, Getdate()) > 0
 --   return -21 
              
  -- 检查coupon是否可以在此店铺使用
  if isnull(@CouponIssueStoreID, 0) <> 0
  begin
    if @LocateStore = 1 and @CouponIssueStoreID <> @StoreID
	   return -28
    if exists(select * from CouponTypeStoreCondition_List where StoreConditionType = 2 and CouponTypeID = @CouponTypeID)  
    begin
	  if not exists(select * from CouponTypeStoreCondition_List where StoreConditionType = 2 and CouponTypeID = @CouponTypeID and StoreID = @StoreID)
	    return -28
    end
  end
  /*
  -- BEGIN   CouponTypeRedeemCondition的条件判断 （只判断整单总金额，不判断指定货品的金额）
  if exists(select * from CouponTypeRedeemCondition  where CouponTypeID = @CouponTypeID)
  begin
    if not exists(select * from CouponTypeRedeemCondition  
      where CouponTypeID = @CouponTypeID and 
        (CardTypeID = @CardTypeID or isnull(CardTypeID,0) <= 0) and 
        (CardGradeID = @CardGradeID or isnull(CardGradeID,0) <= 0) and
        ((RedeemLimitType = 1 and LimitValue <= @SaleDTotalAmt) or 
          (RedeemLimitType = 2 and LimitValue <= @CumulativeConsumptionAmt) or
          (RedeemLimitType = 3 and LimitValue <= @CumulativeEarnPoints) )  )
    begin
	  return -104
    end      
  end
  -- END
  */
  --PLU detail数据写入临时表
  if @SalesDetail <> ''
  begin   
    if @ItemListTempStr = '/ROOT/PLU' 
    begin   
	    set @SalesDData = @SalesDetail	  
	    Insert into @SalesD(DepartCode, ProductBrandID, SeqNo, ProdCode, Collected, RetailPrice, NetPrice, Qty, NetAmount)
	    select P.DepartCode, P.ProductBrandID, A.* from (
	      select T.v.value('@SeqNo','int') as SeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode, 
	          T.v.value('@Collected','int') as Collected, T.v.value('@RetailPrice','money') as RetailPrice, 
	          T.v.value('@NetPrice','money') as NetPrice, T.v.value('@QTY','int') as QTY, 
	          T.v.value('@NetAmount','money') as NetAmount	         
	        from @SalesDData.nodes('/ROOT/PLU') T(v) ) A 
	      left join Product P on A.ProdCode = P.ProdCode
	  end 
	  else
	  begin
	    EXEC sp_xml_preparedocument @idoc OUTPUT, @SalesDetail 
	    Insert into @SalesD(DepartCode, ProductBrandID, SeqNo, ProdCode, Collected, RetailPrice, NetPrice, Qty, NetAmount) 
	    select P.DepartCode, P.ProductBrandID, A.* from (
	          select ROW_NUMBER() OVER(order by PRODCODE) as SeqNo, PRODCODE, 4 as Collected, NETPRICE as RetailPrice, 
	            NETPRICE, QTY,  (NETPRICE * QTY) as NetAmount	         
	          FROM OPENXML (@idoc, @ItemListTempStr, 1) WITH (PRODCODE varchar(512),DEPTCODE varchar(512),QTY int, NETPRICE money)
	        ) A 
	     left join Product P on A.ProdCode = P.ProdCode   
      if @idoc>0  
        EXECUTE sp_xml_removedocument @idoc 
    end 
    select @SaleDTotalAmt = sum(NetAmount) from @SalesD where Collected > 0 
       
    -- 检查销售的货品是否可以使用Coupon
    if exists(select * from CouponTypeExchangeBinding where CouponTypeID = @CouponTypeID and BindingType=2)
    begin   
      if not exists(select * from 
                      (select * from CouponTypeExchangeBinding where CouponTypeID = @CouponTypeID and BindingType=2) A 
                        left join (select Y.DepartCode, Y.ProdCode, X.ProductBrandID from @SalesD X 
                        left join Product_Catalog Y on X.ProdCode = Y.ProdCode) B 
                         on (A.ProdCode = B.ProdCode and isnull(A.ProdCode,'') <> '') or 
                            (A.DepartCode = B.DepartCode and isnull(A.DepartCode,'') <> '') or 
                            (A.BrandID = B.ProductBrandID and isnull(A.BrandID, 0) <> 0) or
                            (isnull(A.ProdCode,'') = '' and isnull(A.DepartCode,'') = '' and isnull(A.DepartCode,'') = '')
                      where isnull(B.ProdCode, '') <> ''
               )
      begin          
        if not exists(select *  from CouponTypeExchangeBinding B inner join @SalesD I on B.ProdCode=I.ProdCode  
                        where CouponTypeID=@CouponTypeID and BindingType=2)          
           return -29
      end  
    end  											   
  end 
  
  -- 计算Coupon抵扣金额
  if @ReturnCouponTypeAmount > 0   -- 金额类型Coupon，使用Coupon的余额
    set @CouponDeductAmount = @CouponAmount      
  else if @ReturnCouponTypeDiscount > 0   -- 百分比Coupon，计算出抵扣值。
    set @CouponDeductAmount = @SaleDTotalAmt * (1-@ReturnCouponTypeDiscount)
                    
  return 0
end
GO
