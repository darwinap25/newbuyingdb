USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenerateBatchTag]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/09 09:46:49
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GenerateBatchTag]

  @tagtypeid            [int],
  @tagtypecode          varchar(64),  
  @tagdesigncode		varchar(64),
  @startingid           [bigint],
  @endingid             [bigint],
  @createdby            [int],
  @batchtagcode         varchar(64),
  @batchtagid           int OUTPUT 

AS
SET NOCOUNT ON;

  
BEGIN   
  

  DECLARE @quantity [int]  
  DECLARE @createdon [datetime] = GETDATE()
  DECLARE @lastsquence [bigint] 
  
  SET @quantity = (@endingid - @startingid) + 1
  
  /*
  SET @lastsquence = (SELECT TOP(1) CAST(REPLACE([b].[BatchCouponCode],'BCC','')AS bigint ) 
                            FROM [dbo].[BatchCoupon] b
                            ORDER BY [BatchCouponCode] DESC)
  
  SET @batchtagcode = 'BCC' + RIGHT ('000000000' + CAST(@lastsquence + 1 AS varchar(9)), 9)
  */
    INSERT INTO [dbo].[BatchCoupon] 
    ([BatchCouponCode], 
        [Qty], 
        [CouponTypeID], 
        [CreatedOn], 
        [UpdatedOn], 
        [CreatedBy], 
        [UpdatedBy], 
        [SeqFrom], 
        [SeqTo],
		[TagDesignCode]
    )
    VALUES 
    (
        @batchtagcode,
        @quantity,
        @tagtypeid,
        @createdon,
        @createdon,
        @createdby,
        @createdby,
        @startingid,
        @endingid,
		@tagdesigncode
    )
   
   SET @batchtagid = @@IDENTITY;
  
END






GO
