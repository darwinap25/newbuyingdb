USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CompleteSalesOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[CompleteSalesOrder]
  @Type                          int,               -- 1: SVA DB, call svasalessubmit.  2: buying DB call possalessubmit
  @UserID				         int,		        -- 操作员ID
  @TxnNo                         VARCHAR(64),       -- 交易号码
  @ApprovalCode                  VARCHAR(64) output --返回的approvalCode
AS
/****************************************************************************
**  Name : CompleteSalesOrder 
**  Version: 1.0.0.0
**  Description : 完成交易单。（指定的交易单数据必须都已经保存，这个过程只是做完成交易的动作。
                  根据业务逻辑，可以在完成交易的动作中做库存，redeemcoupon等操作。
				  用户可以使用SalesSubmit提交status=5的交易数据,来完成交易. 也可以使用complete过程来完成交易.

**  Created by: Gavin @2016-05-05   
****************************************************************************/
BEGIN
  DECLARE @A INT
  SET @A = 0
  SET @Type = ISNULL(@Type, 0)

  IF @Type = 1 OR @Type = 0      -- SVA DB
  BEGIN
    IF EXISTS (SELECT name FROM sysobjects WHERE  name = N'SVACompleteSalesOrder' AND TYPE = 'P')
    BEGIN
	  EXEC @A = SVACompleteSalesOrder @UserID, @TxnNo, @ApprovalCode OUTPUT
	END
  END 

  IF @Type = 2 OR @Type = 0      -- BUYING DB
  BEGIN
    IF EXISTS (SELECT name FROM sysobjects WHERE  name = N'POSCompleteSalesOrder' AND TYPE = 'P')
    BEGIN
	  EXEC @A = POSCompleteSalesOrder @UserID, @TxnNo, @ApprovalCode OUTPUT
	END
  END

  RETURN @A
END

GO
