USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckSalesTag]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckSalesTag]
  @TxnNo    varchar(512),
  @SalesTag varchar(512)
AS
/****************************************************************************
**  Name : CheckSalesTag  
**  Version: 1.0.0.0
**  Description : 校验SalesTag
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = CheckSalesTag 'KTXNNO000000147','KTXNNO000000147,45.00  '
  print @a  
**  Created by: Gavin @2013-02-01
**
****************************************************************************/
begin
  declare @t1 varchar(64)
  select @t1 = TransNum from sales_h where TransNum = @TxnNo and salestag = @SalesTag
  if @@ROWCOUNT = 0
    return -91
  else  
    return 0
end

GO
