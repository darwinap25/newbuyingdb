USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGroup]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardGroup](
	[CardGroupID] [int] IDENTITY(1,1) NOT NULL,
	[CardGroupName] [varchar](512) NULL,
	[CardGroupMaster] [varchar](64) NULL,
	[IsSharePoint] [int] NULL,
	[Status] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARDGROUP] PRIMARY KEY CLUSTERED 
(
	[CardGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CardGroup] ADD  DEFAULT ((0)) FOR [IsSharePoint]
GO
ALTER TABLE [dbo].[CardGroup] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[CardGroup] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CardGroup] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroup', @level2type=N'COLUMN',@level2name=N'CardGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroup', @level2type=N'COLUMN',@level2name=N'CardGroupName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人。（CardNumber）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroup', @level2type=N'COLUMN',@level2name=N'CardGroupMaster'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否成员可以共享创建人的积分.0：不共享，1：共享' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroup', @level2type=N'COLUMN',@level2name=N'IsSharePoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：失效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroup', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员组。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGroup'
GO
