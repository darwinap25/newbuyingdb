USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSCouponSellingVoid]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[POSCouponSellingVoid]
  @UserID             varchar(512),   --用户ＩＤ  
  @TxnNo              varchar(512),   --交易号  
  @TxnNoSN               varchar(512),   --交易号  
  @CouponUID            varchar(512),           -- 需要激活的Coupon号码.如果多coupon则填写首号码    
  @CouponCount          int                    -- 默认1, 大于1则表示从 @CouponNumber 开始的连续Coupon, 如果中间有不同状态的coupon,则返回错误.            
AS
/****************************************************************************
**  Name : POSCouponSellingVoid
**  Version: 1.0.0.0
**  Description : 交易单中，void 指定 的单项Item （selling 过程中）
**
**  Parameter :
  declare @a int, @TxnNo          varchar(512)
  exec @a = UpdateMemberReadReguFlag '1'
  print @a  
**  Created by: Gavin @2012-12-21
**
****************************************************************************/
begin
  declare @status int, @CouponNumber varchar(64)
  set @status=0    
  select @CouponNumber=CouponNumber from CouponUIDMap where CouponUID=@CouponUID    
  if isnull(@CouponNumber,'')=''     
  begin    
    set @status=-19    
    return @status    
  end    
  set @CouponCount = abs(@CouponCount)
    
  insert into ReceiveTxn  
      (BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber, OprID, Amount, Points, TenderID,  
      [Status], VoidKeyID, VoidTxnNo, Additional, ApproveStatus, Remark, SecurityCode, CreatedBy, UpdatedBy,Approvedby, ApprovalCode,CouponNumber,CouponCount)  
  select top 1
      BrandCode, StoreCode, ServerCode, RegisterCode, @TxnNoSN, @TxnNo, BusDate, TxnDate, CardNumber, OprID, -Amount, Points, TenderID,  
      [Status], VoidKeyID, VoidTxnNo, Additional, ApproveStatus, Remark, SecurityCode, CreatedBy, UpdatedBy, Approvedby, ApprovalCode,CouponNumber, -@CouponCount  
    from ReceiveTxn where TxnNo = @TxnNo and CouponNumber = @CouponNumber and CouponCount = @CouponCount
   order by KeyID desc

  if @@ROWCOUNT = 0
    set @status = -11
  else  
    set @status = 0  
    
  return @status    
end

GO
