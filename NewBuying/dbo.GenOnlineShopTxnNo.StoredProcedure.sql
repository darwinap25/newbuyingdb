USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenOnlineShopTxnNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenOnlineShopTxnNo]   
  @TxnNo				varchar(512) output 
AS
/****************************************************************************
**  Name : GenOnlineShopTxnNo
**  Version: 1.0.0.0
**  Description : 获得交易单的单号
**  Parameter :
**
declare @a int, @TxnNo varchar(64)
exec @a = GenOnlineShopTxnNo @TxnNo output
print @a
print @TxnNo

****************************************************************************/
begin
  exec GetRefNoString 'KTXNNO', @TxnNo output   
  return 0
end

GO
