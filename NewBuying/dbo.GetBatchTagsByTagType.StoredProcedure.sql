USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBatchTagsByTagType]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBatchTagsByTagType]
	-- Add the parameters for the stored procedure here
	@tagtypecode			varchar(64)			
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

    -- Insert statements for procedure here

	IF (@tagtypecode is null) or (@tagtypecode = '')
		BEGIN
			SELECT
				b.BatchCouponCode
				,b.BatchCouponID
			FROM BatchCoupon b
			INNER JOIN CouponType c
				ON b.CouponTypeID = c.CouponTypeID
		END
	ELSE
		BEGIN

			SELECT
				b.BatchCouponCode
				,b.BatchCouponID
			FROM BatchCoupon b
			INNER JOIN CouponType c
				ON b.CouponTypeID = c.CouponTypeID
			WHERE c.CouponTypeCode = @tagtypecode

		END



END
GO
