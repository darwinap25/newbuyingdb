USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_SalesPickOrder_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_SalesPickOrder_H](
	[SalesPickOrderNumber] [varchar](64) NOT NULL,
	[OrderType] [int] NULL DEFAULT ((1)),
	[MemberID] [varchar](64) NULL,
	[CardNumber] [varchar](64) NULL,
	[ReferenceNo] [varchar](64) NULL,
	[PickupLocation] [varchar](64) NULL,
	[PickupStaff] [varchar](64) NULL,
	[PickupDate] [datetime] NULL,
	[DeliveryFlag] [int] NULL DEFAULT ((0)),
	[DeliveryCountry] [varchar](64) NULL,
	[DeliveryProvince] [varchar](64) NULL,
	[DeliveryCity] [varchar](64) NULL,
	[DeliveryDistrict] [varchar](64) NULL,
	[DeliveryAddressDetail] [nvarchar](512) NULL,
	[DeliveryFullAddress] [nvarchar](512) NULL,
	[DeliveryNumber] [varchar](64) NULL,
	[LogisticsProviderID] [int] NULL,
	[Contact] [nvarchar](512) NULL,
	[ContactPhone] [nvarchar](512) NULL,
	[RequestDeliveryDate] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryBy] [int] NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_SALESPICKORDER_H] PRIMARY KEY CLUSTERED 
(
	[SalesPickOrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_SalesPickOrder_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_SalesPickOrder_H] ON [dbo].[Ord_SalesPickOrder_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_SalesPickOrder_H
* Version: 1.0.0.2
* Description :
     select * from Ord_SalesPickOrder_H
     update Ord_SalesPickOrder_H set approvestatus = 'A' where SalesPickOrderNumber = 'SPUO00000000001'
** Create By Gavin @2016-05-26
** Modify By Gavin @2016-10-20 (1.0.0.1) 批核前修改，如果改动了Pickup的店铺，那么需要调整 Reserver 的库存数据。
** Modify By Gavin @2017-04-18 (1.0.0.2) 批核前, 如果作废这个Pick单，则需要把Reserver 的库存还回去。 调用改进版ChangeReserverByPickOrder， 传入的新的@PickupLocation为空
*/
/*==============================================================*/
BEGIN  
  DECLARE @SalesPickOrderNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  DECLARE @Busdate DATE, @StoreID int, @StoreCode varchar(64), @OldPickupLocation varchar(64), @PickupLocation varchar(64), @ReferenceNo varchar(64) 
  
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 ORDER BY BusDate DESC

  DECLARE CUR_Ord_SalesPickOrder_H CURSOR fast_forward FOR
    SELECT SalesPickOrderNumber, ApproveStatus, CreatedBy, PickupLocation, ReferenceNo FROM INSERTED
  OPEN CUR_Ord_SalesPickOrder_H
  FETCH FROM CUR_Ord_SalesPickOrder_H INTO @SalesPickOrderNumber, @ApproveStatus, @CreatedBy, @PickupLocation, @ReferenceNo
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus, @OldPickupLocation = PickupLocation from Deleted where SalesPickOrderNumber = @SalesPickOrderNumber

	IF (@OldApproveStatus = 'P' and @ApproveStatus = 'P') AND (@OldPickupLocation <> @PickupLocation) 
	BEGIN
	  EXEC ChangeReserverByPickOrder @CreatedBy, @SalesPickOrderNumber, @OldPickupLocation, @PickupLocation, @ReferenceNo
	END

    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
	  -- 根据拣货单拣货情况,判断是否需要产生新的拣货单.
      exec GenRemainSalesPickOrder @CreatedBy, @SalesPickOrderNumber

	  -- 产生送货单
      EXEC GenApprovalCode @ApprovalCode output
      EXEC GenSalesShipOrder @CreatedBy, @SalesPickOrderNumber
      -- 更新拣货单状态
	  update Ord_SalesPickOrder_H set ApprovalCode = @ApprovalCode, ApproveOn = GETDATE(), ApproveBusDate = @Busdate, ApproveBy = @CreatedBy
        where SalesPickOrderNumber = @SalesPickOrderNumber
    end

    -- ver 1.0.0.2
	IF (@OldApproveStatus = 'P' and @ApproveStatus = 'V') and Update(ApproveStatus)
	BEGIN
	  EXEC ChangeReserverByPickOrder @CreatedBy, @SalesPickOrderNumber, @OldPickupLocation, '', @ReferenceNo
	END

    FETCH FROM CUR_Ord_SalesPickOrder_H INTO @SalesPickOrderNumber, @ApproveStatus, @CreatedBy, @PickupLocation, @ReferenceNo
  END
  CLOSE CUR_Ord_SalesPickOrder_H 
  DEALLOCATE CUR_Ord_SalesPickOrder_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'SalesPickOrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。  1：需要第三方中转（比如送到门店，顾客门店自提） 2：直接产生送货单，交付快递就算完成。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员号码。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡号码。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货仓库（店铺）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'PickupLocation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'PickupStaff'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货日期 （business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'PickupDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货标志。0： 自提，不送货。 1：送货' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在国家（存放Country表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryCountry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在省 （存放Province表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryProvince'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在城市 （存放City表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryCity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址所在区县 （存放District表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryDistrict'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货详细地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryAddressDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货完整地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryFullAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货单号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流供应商ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'LogisticsProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要求送货日期  （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'RequestDeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货物送达日期   （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货订单。
@2016-05-19. 面向订单的拣货单。 一个订单可能拆分成多个拣货单。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesPickOrder_H'
GO
