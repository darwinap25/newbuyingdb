USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_BANK]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_BANK](
	[BankID] [int] IDENTITY(1,1) NOT NULL,
	[BankCode] [varchar](64) NOT NULL,
	[BankName1] [nvarchar](512) NULL,
	[BankName2] [nvarchar](512) NULL,
	[BankName3] [nvarchar](512) NULL,
	[Commissionrate] [decimal](10, 4) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_BANK] PRIMARY KEY CLUSTERED 
(
	[BankID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_BANK] ADD  DEFAULT ((0)) FOR [Commissionrate]
GO
ALTER TABLE [dbo].[BUY_BANK] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_BANK] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BANK', @level2type=N'COLUMN',@level2name=N'BankID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BANK', @level2type=N'COLUMN',@level2name=N'BankCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BANK', @level2type=N'COLUMN',@level2name=N'BankName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BANK', @level2type=N'COLUMN',@level2name=N'BankName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BANK', @level2type=N'COLUMN',@level2name=N'BankName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'佣金率。 （百分制）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BANK', @level2type=N'COLUMN',@level2name=N'Commissionrate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BANK'
GO
