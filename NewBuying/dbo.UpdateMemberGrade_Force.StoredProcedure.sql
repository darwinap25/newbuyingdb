USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberGrade_Force]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[UpdateMemberGrade_Force]
  @MemberID  int
AS
/****************************************************************************
**  Name : UpdateMemberGrade_Force
**  Version: 1.0.0.3
**  Description : 强制升级。 此会员在MemberImport中没有记录，需要先插入，然后再升级。
   declare @A int
   exec @A=UpdateMemberGrade_Force 
   print @A

**  Created by: Gavin @2016-03-23
**  Modify by: Gavin @2016-04-01 (ver 1.0.0.1) 不管怎么样都是要升级的。 没有memberimport数据就先插入。有的话，就直接调用UpdateMemberGrade
**  Modify by: Gavin @2016-09-01 (ver 1.0.0.2) UpdateMemberGrade 中取消update member。 改在这里做update 
**  Modify by Gavin @2017-07-03 (ver 1.0.0.3) (for bauhaus) MemberImport 中的字段为空时，不作为比较字段
**
****************************************************************************/
begin
  declare @MemberMobilePhone varchar(64), @MemberRegisterMobile varchar(64), @MemberDateOfBirth datetime, 
    @MemberDayOfBirth int, @MemberMonthOfBirth int, @MemberYearOfBirth int

  select @MemberMobilePhone = MemberMobilePhone, @MemberRegisterMobile = MemberRegisterMobile,
      @MemberDateOfBirth = MemberDateOfBirth, @MemberDayOfBirth = MemberDayOfBirth, 
	  @MemberMonthOfBirth = MemberMonthOfBirth, @MemberYearOfBirth = MemberYearOfBirth 
    from Member where MemberID = @MemberID
  
  if not exists(select * from MemberImport where (ISNULL(MemberRegisterMobile,'') <> '' and MemberRegisterMobile = @MemberRegisterMobile) or (ISNULL(MemberMobilePhone,'') <> '' and MemberMobilePhone = @MemberMobilePhone))
  begin
    insert into MemberImport(MemberMobilePhone, MemberRegisterMobile, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth)
    values(@MemberMobilePhone, @MemberRegisterMobile, @MemberDateOfBirth, @MemberDayOfBirth, @MemberMonthOfBirth, @MemberYearOfBirth)  
  end

  exec UpdateMemberGrade @MemberID

  update Member set UpdatedOn = Getdate() where MemberID = @MemberID

  return 0
end


GO
