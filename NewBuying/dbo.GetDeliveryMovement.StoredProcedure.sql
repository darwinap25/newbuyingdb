USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetDeliveryMovement]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDeliveryMovement]
  @TxnNo                      VARCHAR(64),              -- 交易单号
  @DeliveryNumber             VARCHAR(64),			    -- 部门编码
  @ConditionStr               NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	          NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent                INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize                   INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount                  INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount                INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			      VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetDeliveryMovement
**  Version : 1.0.0.1
**  Description : 获得快递公司返回的快递单跟踪信息。
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  exec @a = GetDeliveryMovement '', '', '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
  select * from delivery_movement
**  Created by Gavin @2015-03-06
**  Modify by Gavin @2016-07-29 (ver 1.0.0.1) 增加输入条件 @TxnNo
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1  
    
  SET @DeliveryNumber = ISNULL(@DeliveryNumber, '')
  SET @TxnNo = ISNULL(@TxnNo, '')
  IF @DeliveryNumber <> ''  
    SET @WhereStr = ' WHERE DeliveryNumber=''' + @DeliveryNumber + ''' '
  ELSE  
    SET @WhereStr = ''
    
  SET @SQLStr = 'SELECT A.KeyID,A.DeliveryNumber,A.ReferenceNo,A.LogisticsProviderID,A.LogisticsProviderName,A.Status,'
    + ' A.Seller,A.ShipAddress,A.Shipper,A.ShipperContact,A.DeliveryAddress,A.Consignee,A.ConsigneeContact,A.Content,A.CreatedOn '
    + ' FROM delivery_movement A '
	+ ' LEFT JOIN Ord_SalesShipOrder_H B ON A.DeliveryNumber = B.DeliveryNumber '
    + ' WHERE (A.DeliveryNumber=''' + @DeliveryNumber + ''' OR ''' + @DeliveryNumber + ''' = '''' )'
	+ ' AND (B.TxnNo = ''' + @TxnNo + ''' OR ''' + @TxnNo + ''' = '''')'

  EXEC SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr  
END

GO
