USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[Reserve_ByExchangePick]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[Reserve_ByExchangePick]
  @UserID				         INT,	            -- 操作员ID
  @SalesPickOrderNumber          VARCHAR(64),       -- 拣货单号码
  @TxnNo                         VARCHAR(64),       -- 交易单号
  @NewStoreID                    INT                -- 拣货单中的pickuplocation正在更新,可能不准,另外输入
AS
/****************************************************************************
**  Name : Reserve_ByExchangePick (bauhaus)
**  Version: 1.0.0.0
**  Description : 预扣库存,  用于exchange产生的新单的Reserver. 目前exchange产生的新单的pickup是hardcode ECO的. 因为ship单是根据pick单来扣的,所以stockmovement只能强制对应pick单.
                 
**  Created by: Gavin @2017-03-23
**
****************************************************************************/
BEGIN
  DECLARE @StoreCode VARCHAR(64), @PickupLocation VARCHAR(64), @StockTypeCode VARCHAR(64), @ProdCode VARCHAR(64), @TotalQty INT,
          @PickupStoreCode VARCHAR(64), @StoreOnhandQty INT, @WHOnhandQty INT, @WHStoreID INT, @StoreID INT,
		  @BusDate DATETIME, @TxnDate DATETIME, @SerialNoType INT, @SerialNo VARCHAR(64), @CreatedBy INT, @SeqNo VARCHAR(64)
  DECLARE @ReserveQty INT, @WHReserveQty INT

  SELECT @WHStoreID = StoreID FROM BUY_STORE WHERE StoreCode = 'WH'
  SELECT @BusDate = BusDate, @TxnDate = TxnDate FROM Sales_H WHERE TransNum = @TxnNo
  SET @SerialNoType = ''
  SET @SerialNo = ''

  DECLARE CUR_ReserveSalesStock_P CURSOR fast_forward FOR
      SELECT D.StockTypeCode, D.ProdCode, D.OrderQty, O.OnhandQty, W.OnhandQty, H.Createdby
		FROM Ord_SalesPickOrder_D D      
		LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber
	    LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @NewStoreID) O ON D.ProdCode = O.ProdCode
		LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @WHStoreID) W ON D.ProdCode = W.ProdCode
	  WHERE D.SalesPickOrderNumber = @SalesPickOrderNumber
  OPEN CUR_ReserveSalesStock_P
  FETCH FROM CUR_ReserveSalesStock_P INTO @StockTypeCode, @ProdCode, @TotalQty, @StoreOnhandQty, @WHOnhandQty, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    SELECT @ReserveQty = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'R' AND ProdCode = @ProdCode AND StoreID = @NewStoreID
	SELECT @WHReserveQty = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'R' AND ProdCode = @ProdCode AND StoreID = @WHStoreID
    
	SET @StockTypeCode = 'G'
	SET @StoreOnhandQty = ISNULL(@StoreOnhandQty, 0)
	SET @WHOnhandQty = ISNULL(@WHOnhandQty, 0)
	SET @ReserveQty = ISNULL(@ReserveQty, 0)
	SET @WHReserveQty = ISNULL(@WHReserveQty, 0)


      INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	  VALUES (1, @NewStoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
	         isnull(@StoreOnhandQty,0), - isnull(@TotalQty,0), isnull(@StoreOnhandQty,0) - isnull(@TotalQty,0), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby )	   
      INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	  VALUES (1, @NewStoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
	         isnull(@ReserveQty,0), isnull(@TotalQty,0), isnull(@ReserveQty,0) + isnull(@TotalQty,0), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 	  


    FETCH FROM CUR_ReserveSalesStock_P INTO  @StockTypeCode, @ProdCode, @TotalQty, @StoreOnhandQty, @WHOnhandQty, @CreatedBy
  END
  CLOSE CUR_ReserveSalesStock_P 
  DEALLOCATE CUR_ReserveSalesStock_P

  RETURN 0
END

GO
