USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_MNM_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_MNM_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MNMCode] [varchar](64) NOT NULL,
	[EntityNum] [varchar](64) NOT NULL,
	[EntityType] [int] NOT NULL,
	[Type] [int] NULL,
	[Qty] [int] NULL,
 CONSTRAINT [PK_BUY_MNM_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_MNM_D] ADD  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [dbo].[BUY_MNM_D] ADD  DEFAULT ((0)) FOR [Qty]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_D', @level2type=N'COLUMN',@level2name=N'MNMCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_D', @level2type=N'COLUMN',@level2name=N'EntityNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'决定EntityNum中内容的含义。
0：所有货品。EntityNum中为空
1：EntityNum内容为 prodcode。 销售EntityNum中指定货品时，可以生效
2：EntityNum内容为 DepartCode。。 销售EntityNum中指定部门的货品时，可以生效
3：EntityNum内容为 TenderCode。 使用EntityNum中指定货币支付时，可以生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_D', @level2type=N'COLUMN',@level2name=N'EntityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EntityNum所指货品的类型。
0: Hit
1: Gift
2: Hit&Gift' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_D', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_D', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销明细表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_D'
GO
