USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardImport]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardImport](
	[CardImportNumber] [varchar](64) NOT NULL,
	[CardBarcode] [varchar](64) NOT NULL,
	[CardTypeCode] [varchar](64) NOT NULL,
	[CardGradeCode] [varchar](64) NOT NULL,
	[Status] [int] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_CARDIMPORT] PRIMARY KEY CLUSTERED 
(
	[CardImportNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CardImport] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[CardImport] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入的卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardImport', @level2type=N'COLUMN',@level2name=N'CardImportNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入的卡的条码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardImport', @level2type=N'COLUMN',@level2name=N'CardBarcode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型。Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardImport', @level2type=N'COLUMN',@level2name=N'CardTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别。Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardImport', @level2type=N'COLUMN',@level2name=N'CardGradeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态： 0：未绑定SVA Card表。 1：已经绑定。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardImport', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardImport', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实体卡导入。 （导入卡号和条码）
卡号和条码可能不同。 卡号作为Card表的 Number， 条码作为CardUIDMap表的CardUID 
add by Gavin @20170116' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardImport'
GO
