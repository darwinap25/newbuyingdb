USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSInsertSalesT]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[POSInsertSalesT]
  @TxnNo				varchar(64),-- 交易号码
  @TenderCode           varchar(64),
  @TenderName           varchar(64),
  @TenderAmount         money
as
/****************************************************************************
**  NAME : POSInsertSalesT  （buying DB）
**  VERSION: 1.0.0.0
**  DESCRIPTION :  在Sales_T中插入记录(只插入一条，反复执行时做update处理)。(bauhaus,仅供Nick使用)
**  PARAMETER :
  declare @a int 
  exec @a=GetOrderInfo '12345'
  print @a  
**  CREATED BY: GAVIN @2017-07-26
select top 1 * from sales_T
**
****************************************************************************/
begin
  declare @SeqNo int, @TenderType int, @TenderID int, @Rate decimal(12,4)
  select top 1 @SeqNo = SeqNo from sales_T where TransNum = @TxnNo
  select @TenderID = CurrencyID, @TenderType = TenderType, @Rate=Rate from BUY_CURRENCY where CurrencyCode = @TenderCode
  if isnull(@SeqNo, 0) > 0 
    update Sales_T set TenderID = @TenderID, TenderCode = @TenderCode, TenderDesc = @TenderName, TenderType = @TenderType,
	    TenderAmount = @TenderAmount, LocalAmount = @TenderAmount, ExchangeRate = @Rate, UpdatedOn = GETDATE()
	where SeqNo = @SeqNo and TransNum = @TxnNo
  else    
    Insert into Sales_T(SeqNo, TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,TenderID,TenderCode,TenderDesc,
       TenderType,TenderAmount,LocalAmount,ExchangeRate,PaymentType,Additional,Status,PayConfirmDate,CardNumber,CardType,
       CardHolder,CardApprovalCode,CardExpiryDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    select 1, @TxnNo, TransType,StoreCode,RegisterCode,BusDate,TxnDate,@TenderID, @TenderCode,@TenderName,
	  @TenderType,@TenderAmount,@TenderAmount, @Rate,1,'',0, null, '', 0,
	  '', '', '', GetDate(), 1, GetDate(),1  
	from sales_H 
	where  TransNum = @TxnNo 
end

GO
