USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_OrderToSupplier_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_OrderToSupplier_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[OrderSupplierNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NULL,
	[OrderQty] [int] NULL DEFAULT ((1)),
	[FirstCouponNumber] [varchar](64) NULL,
	[EndCouponNumber] [varchar](64) NULL,
	[BatchCouponCode] [varchar](64) NULL,
	[PackageQty] [int] NULL DEFAULT ((1)),
	[OrderRoundUpQty] [int] NULL DEFAULT ((1)),
	[CouponNatureID] [int] NULL,
 CONSTRAINT [PK_ORD_ORDERTOSUPPLIER_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提交供应商订单单号，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'OrderSupplierNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'OrderQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提供给供应商的首coupon号。（如果IsProvideNumber=1，那么需要提供Coupon号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'FirstCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提供给供应商的末coupon号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'EndCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstCouponNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'BatchCouponCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装数量。默认为1. 不得为0 或者 null
@2014-08-28：由每一包中的Coupon数量， 改为 包裹的数量。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'PackageQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每一包中的Coupon数量，来源于规则表中的 OrderRoundUpQty。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D', @level2type=N'COLUMN',@level2name=N'OrderRoundUpQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'面向供应商的订货单，明细表
如果要求向供应商提供CouponNumber。  并且要求每个Number都记录下来。 那么OrderQty=1， FirstCouponNumber=EndCouponNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_D'
GO
