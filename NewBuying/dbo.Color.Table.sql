USE [NewBuying]
GO
/****** Object:  Table [dbo].[Color]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Color](
	[ColorID] [int] IDENTITY(1,1) NOT NULL,
	[ColorCode] [varchar](64) NOT NULL,
	[ColorName1] [nvarchar](512) NULL,
	[ColorName2] [nvarchar](512) NULL,
	[ColorName3] [nvarchar](512) NULL,
	[ColorPicFile] [nvarchar](512) NULL,
	[RGB] [varchar](64) NULL,
 CONSTRAINT [PK_COLOR] PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color', @level2type=N'COLUMN',@level2name=N'ColorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色编号（可以使用：RGB编号。 e.g. FFFFFF）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color', @level2type=N'COLUMN',@level2name=N'ColorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color', @level2type=N'COLUMN',@level2name=N'ColorName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color', @level2type=N'COLUMN',@level2name=N'ColorName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color', @level2type=N'COLUMN',@level2name=N'ColorName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color', @level2type=N'COLUMN',@level2name=N'ColorPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GB编号。 e.g. FFFFFF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color', @level2type=N'COLUMN',@level2name=N'RGB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Color'
GO
