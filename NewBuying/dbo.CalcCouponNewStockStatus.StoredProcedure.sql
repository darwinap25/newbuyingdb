USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcCouponNewStockStatus]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[CalcCouponNewStockStatus]
  @OprID  int,                      -- 订单操作的OprID
  @NewStatus int output,            -- coupon新的状态。
  @NewStockStatus int output        -- Coupon新的库存状态
AS
/****************************************************************************
**  Name : CalcCouponNewStockStatus
**  Version: 1.0.0.0
**  Description : 计算coupon在订单操作后，应该变更成的库存状态
          （此过程不一定使用，主要用于定义订单操作批核后的库存状态变化）（Couponstatus hardcode 为 0， 不考虑其他Coupon状态）
**
**  Created by: Gavin @2014-06-26
**
****************************************************************************/
begin
  if @OprID in (70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80)  -- 只处理订单的OprID
  begin
    if @OprID = 70         --总部向供应商订单批核
    begin
      set @NewStatus = 0
      set @NewStockStatus = 1
    end
    if @OprID = 71      --收货单批核 （好货）
    begin
      set @NewStatus = 0
      set @NewStockStatus = 2
    end
    if @OprID = 72           --店铺向总部订单批核
    begin
      set @NewStatus = 0
      set @NewStockStatus = 4
    end
    if @OprID = 73           -- 拣货单批核 （批核之前，拣货单添加：(Dormant	Picked)，拣货单删除：(Dormant	Good for Release））
    begin
      set @NewStatus = 1
      set @NewStockStatus = 5
    end
    if @OprID = 74            -- 送货单批核
    begin
      set @NewStatus = 1
      set @NewStockStatus = 6
    end
    if @OprID = 75            -- 退货单批核
    begin
      set @NewStatus = 1
      set @NewStockStatus = 7
    end
    if @OprID = 76           -- 退货的收货单批核
    begin
      set @NewStatus = 0
      set @NewStockStatus = 2
    end 
    if @OprID = 77           -- --收货单批核 （坏货）
    begin
      set @NewStatus = 0
      set @NewStockStatus = 3
    end 
    if @OprID = 78           -- 拣货单添加
    begin
      set @NewStatus = 0
      set @NewStockStatus = 4
    end 
    if @OprID = 79           -- 拣货单删除
    begin
      set @NewStatus = 0
      set @NewStockStatus = 2
    end 
    if @OprID = 80          -- 收货单批核 （未区分好坏）
    begin
      set @NewStatus = 0
      set @NewStockStatus = 0
    end              
    set @NewStatus = 0     
    return 0                      
  end
  else
    return -1   
end

GO
