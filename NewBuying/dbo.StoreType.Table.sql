USE [NewBuying]
GO
/****** Object:  Table [dbo].[StoreType]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StoreType](
	[StoreTypeID] [int] IDENTITY(1,1) NOT NULL,
	[StoreTypeCode] [varchar](64) NOT NULL,
	[StoreTypeName1] [nvarchar](512) NULL,
	[StoreTypeName2] [nvarchar](512) NULL,
	[StoreTypeName3] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_STORETYPE] PRIMARY KEY CLUSTERED 
(
	[StoreTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreType', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreType', @level2type=N'COLUMN',@level2name=N'StoreTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreType', @level2type=N'COLUMN',@level2name=N'StoreTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreType', @level2type=N'COLUMN',@level2name=N'StoreTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreType', @level2type=N'COLUMN',@level2name=N'StoreTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型表。
固定数据：
StoreTypeID = 1：  总部
StoreTypeID = 2：  店铺
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreType'
GO
