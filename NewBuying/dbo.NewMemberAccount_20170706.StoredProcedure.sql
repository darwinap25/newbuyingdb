USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[NewMemberAccount_20170706]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[NewMemberAccount_20170706]  
 @MemberRegisterMobile varchar(512),   -- 注册手机号  
 @MemberMobilePhone varchar(512),   -- 手机号  
 @CountryCode varchar(512),     -- 手机号的国家码  
 @CardTypeCode varchar(512),    -- 需要新增的CardTypeCode, 只绑定这个指定的CardTypeID的卡. 没有则返回 -36  
 @CardNumber varchar(512),    -- 指定卡号, 只绑定这个指定的CardTypeID的卡. 没有则返回 -36  
 @ReturnMemberID int output,            -- 返回新增的memberID  
 @ReturnMemberPassword varchar(512) output,          -- 返回会员的CardNumber    
 @ReturnCardNumber varchar(512)='' output,          -- 返回绑定的卡的CardNumber    
 @ReturnCardPassword varchar(512)='' output,          -- 返回绑定的卡的CardNumber  
 @ReturnMessageStr		varchar(max) output,	-- 返回信息。（json格式）  
 @BindCardTypeID int=0,     -- @AutoBindCard =1 时有效，需要自动绑定的卡类型ID   
 @MemberDateOfBirth datetime=0,                 -- 生日  
 @MemberSex char(1)='',       -- 性别。M，F，空  
 @MemberEngFamilyName nvarchar(30)='',   --   
 @MemberEngGivenName nvarchar(30)='',  
 @MemberChiFamilyName nvarchar(30)='',  
 @MemberChiGivenName nvarchar(30)='',  
 @AutoBindCard int = 1,                         -- 0：不自动绑定卡。 1：自动绑定卡，没有卡时报错  
 @ReferCardNumber varchar(64)='',                -- 推荐人的CardNumber, 默认为''. 
 @NickName varchar(512)='',                     -- 昵称
 @Email varchar(512)='',                        -- 邮箱地址
 @InitPassword varchar(512)='',                  -- 初始密码
 @LanguageAbbr	varchar(20)='', 
 @RecPromotion int = 0,                          -- 是否接受优惠信息
 @RecEmailAdv  int = 0,                          -- 是否接受邮件的优惠信息
 @RecSMSAdv  int = 0,                            -- 是否接受短信的优惠信息
 @RecPhoneAdv  int = 0,                          -- 是否接受电话的优惠信息
 @AppleNotification  varchar(512)='',            -- Apple的ID记录
 @GoogleNotification  varchar(512)='',            -- Google的ID记录
 @AgeBracklet int = 0                             -- 年龄段选项。 默认0， 表示不填。
as  
/****************************************************************************  
**  Name : NewMemberAccount    
**  Version: 1.0.1.11
**  Description :  新增Member记录。  
**  Parameter :     
**  return:   
declare  @A int, @ReturnMemberID int, @ReturnCardNumber varchar(512), @ReturnMemberPassword varchar(512), @ReturnCardPassword varchar(512)  
declare @ReturnMessageStr		varchar(max)
 exec @A = NewMemberAccount '0082225284274825', '1225921992301', '086', '', '', @ReturnMemberID output, @ReturnMemberPassword output , @ReturnCardNumber output, @ReturnCardPassword output, @ReturnMessageStr output  
 ,0,0,'','','','','',1,'','Gavin','Gavin.Wu@tap-group.com.cn'  
print @A 
print @ReturnCardNumber  
print @ReturnMemberID
print @ReturnMemberPassword 
print @ReturnMessageStr 
 
**  Created by: Gavin @2012-05-29  
**  Modify by: Gavin @2012-06-08  
**  Modify by: Gavin @2012-08-29 (ver 1.0.0.1) 初始会员密码加上MD5加密  
**  Modify by: Gavin @2012-09-05 (ver 1.0.0.2) 新增member时,同步更新MemberMessageAccount表  
**  Modify by: Robin @2012-11-26 (ver 1.0.0.3) 新增member时,产生的密码从6位改为4位  
**  Modify by: Gavin @2012-12-12 (ver 1.0.0.4) 增加CardNumber输入，如果CardNumber不为空，则绑定这个cardnumber
**  Modify by: Gavin @2012-12-13 (ver 1.0.0.5) 自动分配card时，按cardgrade 的CardGradeRank,CardGradeID,CardNumber 次序取号。
**  Modify by: Gavin @2012-12-13 (ver 1.0.0.6) 改成 0,1 状态的卡 都可以取出
**  Modify by: Gavin @2012-12-14 (ver 1.0.0.7) 分配的卡自动赠送Coupon， 同时把@MemberRegistID更新到MemberMessageAccount,作为手机号
**  Modify by: Gavin @2012-12-28 (ver 1.0.0.8) 绑定卡给Member时，再插入一条OprID=26的card_movement记录。由card_movement触发器完成绑定卡,更新有效期的操作.
**  Modify by: Gavin @2013-01-10 (ver 1.0.0.9) 赠送coupon时, 需要根据CardGradeHoldCouponRule的设置来决定，有配置的CouponType才可以赠送（不检查数量限制）
                                               新增会员,插入的MemberMessageAcount账号，IsPrefer 默认改为0
**  Modify by: Gavin @2013-01-17 (ver 1.0.0.10) 赠送coupon时, 需要根据CardGradeHoldCouponRule的设置来决定。按照RuleType=1的holdcount数量来赠送。                                             
**  Modify by: Gavin @2013-01-22 (ver 1.0.0.11) 新增会员时,如果@AutoBindCard=1,并且没有绑定到卡，则返回失败，整个操作rollback。
**  Modify by: Gavin @2013-08-28 (ver 1.0.0.12) 新增参数 @ReferCardNumber （推荐人CardNumber），保存入Member 表的 ReferCardNumber. 如果有推荐人，按照推荐奖励规则奖励。
											    增加返回参数@ReturnMessageStr,返回JSON格式信息.
											    校验@ReferCardNumber是否存在.
**  Modify by: Gavin @2013-08-30 (ver 1.0.0.13) 新增会员,插入的MemberMessageAcount账号，IsPrefer 默认改为1
**  Modify by: Gavin @2013-10-14 (ver 1.0.0.14) 记录操作到UserAction_Movement表
**  Modify by: Gavin @2013-10-14 (ver 1.0.0.15) 按照passwordrule的设置，设置密码的长度。
**  Modify by: Gavin @2013-12-19 (ver 1.0.0.16) 新会员注册时，检查会员手机号（MemberMobilePhone），如果这个手机号已经存在，则认为此人已经注册
**  Modify by: Gavin @2014-06-19 (ver 1.0.0.17) 增加输入参数。 手机号码写入 account时，默认为已经验证过的。（因为会通过手机发送密码）
									            增加写入 email 的account。@InitPassword 原样写入DB
**  Modify by: Gavin @2014-06-23 (ver 1.0.0.18) 增加@RecPromotion,@RecEmailAdv,@RecSMSAdv,@RecPhoneAdv,控制接收优惠消息
                                                增加@AppleNotification，@GoogleNotification。如果用户有这些输入，则写入MessageAccount。
                                                （新的MessageServiceType = 9， 10）
**  Modify by: Gavin @2014-06-30 (ver 1.0.0.19)  取消1.0.0.17的修改中:手机号码写入 account时，默认为已经验证过的.  手机账号默认未验证                                              
                                                 修改Member表中MemberDefLanguage存放的值,确定为languageMap表的KeyID
**  Modify by: Gavin @2014-07-11 (ver 1.0.1.0)  (for 711) 如果输入的@MemberMobilePhone存在已有的记录。则判断此记录是否做过验证，如果没有验证的话就把这个member返回。
**  Modify by: Gavin @2014-07-14 (ver 1.0.1.1)  (for 711) 如果取回memberID,那应该要更新Email,registermobile 
**  Modify by: Gavin @2014-07-31 (ver 1.0.1.2)  (for 711) 增加写入MessageObject表功能, 添加一条内部的欢迎信息. 内容hardcode. 
**  Modify by: Gavin @2014-08-07 (ver 1.0.1.3)  (for 711) 多增加一条MessageServiceTypeID=0的记录       
**  Modify by: Gavin @2014-08-13 (ver 1.0.1.4)  (for 711) 取消随机密码.   
**  Modify by: Gavin @2014-08-13 (ver 1.0.1.5)  (for 711) 增加手机保护时间检查。
**  Modify by: Gavin @2014-08-19 (ver 1.0.1.6)  (for 711) 修正CharIndex使用错误的bug.   
**  Modify by: Gavin @2014-09-10 (ver 1.0.1.7)  (for 711) 轉?用戶正式註冊后,更新默認語言                   
**  Modify by: Gavin @2014-10-22 (ver 1.0.1.8)  (for 711) 赠送的coupon，初始有效期需要从Coupon表中取		     
**  Modify by: Gavin @2014-11-05 (ver 1.0.1.9)  (for 711) 转赠产生的会员，第二次调用此过程时，更新CreatedOn的时间。 即 Member的CreatedOn记录 成为正式会员的时间。      
**  Modify by: Gavin @2015-02-16 (ver 1.0.1.10)  增加输入参数 @AgeBracklet int (注,此版本没有使用 CouponPool取号)
**  Modify by: Gavin @2015-08-24 (ver 1.0.1.11)  修正birthday中截取月份和日期 搞错的问题。 
**  remark：指定CardNumber绑定时，这个Card为status=1，绑定后为status=2。 不指定时，取的是Status=0的Card，绑定后也是转成status=2
**  
****************************************************************************/  
begin  
  declare @MemberDayOfBirth int, @MemberMonthOfBirth int, @MemberYearOfBirth int  
  declare @NewMemberID int,@NewCardNumber varchar(512), @InitPWD varchar(512), @CardTypeID int, @MD5InitPWD varchar(512)  
  declare @BusDate datetime, @TxnDate datetime, @BindCouponTypeID int, @CouponNumber varchar(64)
  declare @ApprovalCode varchar(6), @StoreID int, @BrandID int, @CouponAmount money
  declare @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate date, @NewCouponExpiryDate date
  declare @CardExpiryDate datetime, @CardStatus int, @CardGradeID int, @BindCouponCount int, @i int
  declare @Return int, @BindCardNumber varchar(64), @PWDMinLength  int, @RandNum decimal(20, 0), @MemberDefLanguage int
  declare @InputString varchar(max), @MessageServiceTypeID int, @AccountNumber nvarchar(512)
  declare @VerifyFlag int, @OldMemberRegisterMobile varchar(64)
  declare @MessageBody varbinary(max), @MessageID int, @Messagecontent nvarchar(max), @ReceiveList varchar(100)
  declare @HasTransferMember int, @OtherMemberID int, @OtherUpdateDate datetime, @MobileProtectPeriodValue int
  
  select @MemberDefLanguage = KeyID from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@MemberDefLanguage, 0) = 0
    set @MemberDefLanguage = 1
    
  select top 1 @PWDMinLength = PWDMinLength from passwordrule	where MemberPWDRule = 1
  set @PWDMinLength = isnull(@PWDMinLength, 6)
  if @PWDMinLength = 0 
    set @PWDMinLength = 6
  set @MemberMobilePhone = isnull(@MemberMobilePhone, '')      
  set @return = 0
  set @BindCardNumber = ''   
  set @ReferCardNumber = isnull(@ReferCardNumber, '') 
  set @MemberDateOfBirth = isnull(@MemberDateOfBirth, 0)
  set @AgeBracklet = ISNULL(@AgeBracklet, 0)
  if isnull(@MemberSex, '') = '' set @MemberSex = ''  
  if isnull(@MemberEngFamilyName, '') = '' set @MemberEngFamilyName = ''  
  if isnull(@MemberEngGivenName, '') = '' set @MemberEngGivenName = ''  
  if isnull(@MemberChiFamilyName, '') = '' set @MemberChiFamilyName = ''  
  if isnull(@MemberChiGivenName, '') = '' set @MemberChiGivenName = ''  
  if isnull(@MemberDateOfBirth, 0) = 0   
  begin  
    set @MemberDateOfBirth = 0  
    set @MemberDayOfBirth = 0  
    set @MemberMonthOfBirth = 0  
    set @MemberYearOfBirth = 0  
  end else  
  begin     
    set @MemberDayOfBirth = DatePart(dd, @MemberDateOfBirth)  
    set @MemberMonthOfBirth = DatePart(mm, @MemberDateOfBirth)  
    set @MemberYearOfBirth = DatePart(yy, @MemberDateOfBirth)   
  end  
    
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 
  if isnull(@TxnDate, 0) = 0
    set @TxnDate = getdate()        
  set @ReturnCardPassword = ''  
  if isnull(@MemberRegisterMobile, '') = ''  
    return -7    
  if exists(select * from member where MemberRegisterMobile = @MemberRegisterMobile)  
    return -33 
    
  --ver 1.0.1.5 -- ver 1.0.0.16   --ver 1.0.1.0
  if @MemberMobilePhone <> ''
  begin
    set @HasTransferMember = 0
    select @ReturnMemberID = MemberID, @ReturnMemberPassword = MemberPassword, @OldMemberRegisterMobile = MemberRegisterMobile from member 
      where MemberMobilePhone = @MemberMobilePhone and MemberRegisterMobile = @MemberMobilePhone
    if isnull(@ReturnMemberID, 0) > 0   --     -- 转赠时创建的member
    begin
      set @HasTransferMember = 1
    end
    else         -- 存在其他相同手机号码的用户
    begin     
      select top 1 @OtherMemberID = MemberID, @OtherUpdateDate = UpdatedOn from MemberMessageAccount 
        where MemberID in (select MemberID from member where MemberMobilePhone = @MemberMobilePhone)
           and MessageServiceTypeID = 2 
       order by UpdatedOn desc
      if isnull(@OtherMemberID, 0) > 0
      begin
        select Top 1 @MobileProtectPeriodValue = MobileProtectPeriodValue from Card C 
          left join CardGrade G on C.CardGradeID = G.CardGradeID
         where MemberID = @OtherMemberID 
        if isnull(@MobileProtectPeriodValue, 0) > 0
        begin
          if dateadd(m, @MobileProtectPeriodValue, @OtherUpdateDate) > GETDATE()
            return -204   -- 手机号码不可更改.
        end  
      end        
    end
    
    if @HasTransferMember > 0
    begin
      select top 1 @ReturnCardNumber = CardNumber from Card where MemberID = @ReturnMemberID
      select top 1 @VerifyFlag = VerifyFlag from MemberMessageAccount where MemberID = @ReturnMemberID and MessageServiceTypeID = 1
  
      if isnull(@VerifyFlag,0) = 1 or @OldMemberRegisterMobile <> @MemberMobilePhone
      begin
        set @ReturnMemberID = ''  
        set @ReturnCardNumber = ''  
        set @ReturnCardPassword = '' 
        return -33       
      end else
      begin
        update Member set MemberRegisterMobile = @MemberRegisterMobile, MemberEmail = @Email,
            MemberPassword = @InitPassword, MemberDefLanguage = @MemberDefLanguage, MemberEngFamilyName = @MemberEngFamilyName,
            MemberEngGivenName = @MemberEngGivenName, MemberChiFamilyName = @MemberChiFamilyName,
            MemberChiGivenName = @MemberChiGivenName, MemberSex = @MemberSex, MemberMobilePhone = @MemberMobilePhone, 
            MemberDateOfBirth = @MemberDateOfBirth, MemberDayOfBirth = @MemberDayOfBirth, MemberMonthOfBirth= @MemberMonthOfBirth, 
            MemberYearOfBirth = @MemberYearOfBirth, CountryCode = @CountryCode, ReferCardNumber = @ReferCardNumber,
            NickName= @NickName, ReceiveAllAdvertising = @RecPromotion, AcceptPhoneAdvertising = @RecPhoneAdv,
            CreatedOn = Getdate(), AgeBracklet = @AgeBracklet 
          where MemberID = @ReturnMemberID
        if ISNULL(@Email, '') <> ''
          if exists(select * from MemberMessageAccount where MemberID = @ReturnMemberID and MessageServiceTypeID = 1)
            update MemberMessageAccount set AccountNumber = @Email where MemberID = @ReturnMemberID and MessageServiceTypeID = 1
          else  
            insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
                CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag, PromotionFlag)
            values(@ReturnMemberID, 1,  @Email, 1, '', 1, getdate(), @ReturnMemberID, getdate(), @ReturnMemberID, 0, @RecEmailAdv)
              
        -- 返回信息JSON
        set @InputString = ''
        set @InputString = 'MPASSWORD=' + isnull(@ReturnMemberPassword,'') + ';MEMBERID=' + cast(@ReturnMemberID as varchar)
     	   + ';MSGACCOUNT=' + cast(@ReturnMemberID as varchar) + ';MSGACCOUNTTYPE=0' 
     	  if CharIndex('@', @MemberRegisterMobile) > 0      	    
          set @InputString = @InputString + '|' + 'MPASSWORD=' + isnull(@ReturnMemberPassword,'') + ';MEMBERID=' + cast(@ReturnMemberID as varchar)
   	       + ';MSGACCOUNT=' + @MemberRegisterMobile + ';MSGACCOUNTTYPE=1' 
   	    else
          set @InputString = @InputString + '|' + 'MPASSWORD=' + isnull(@ReturnMemberPassword,'') + ';MEMBERID=' + cast(@ReturnMemberID as varchar)
     	     + ';MSGACCOUNT=' + @CountryCode + @MemberMobilePhone + ';MSGACCOUNTTYPE=2'      	         
        exec GenMessageJSONStr 'NewMemberAccount', @InputString, @ReturnMessageStr  output    
        
        -- 写入内部欢迎消息
        set @Messagecontent = '歡迎加入7FANS'
        set @MessageBody = CAST(@Messagecontent as varbinary)
        set @ReceiveList = CAST(@ReturnMemberID as varchar)
        exec SendMemberMessageObject 1, 0, 0, 1, 1, 0, '歡迎加入7FANS',  @MessageBody, 1, @ReceiveList, @MessageID output        
        return 0
      end 
    end 
  end       
  ---   
  if @ReferCardNumber <> ''
  begin  
    if not exists(select * from Card where CardNumber = @ReferCardNumber)
      return -2
  end
    
  if isnull(@InitPassword, '') <> ''
  begin
    -- 使用提供的密码
    set @InitPWD = @InitPassword
    set @MD5InitPWD = @InitPassword
  end 
  else
  begin
    --ver 1.0.1.4
    set @InitPWD = @InitPassword
    set @MD5InitPWD = @InitPassword    
  -- 随机密码: 
--    select @RandNum = round(rand() * 100000000000000000, 0)
--    set @InitPWD = right('00000000000000000' + convert(varchar(20), @RandNum), @PWDMinLength)       
--    set @MD5InitPWD = dbo.EncryptMD5(@InitPWD)  
  end

begin tran    

  -- 创建Member记录       
  insert into member(MemberRegisterMobile, MemberEngFamilyName,MemberEngGivenName, MemberChiFamilyName,MemberChiGivenName, MemberSex,   
        MemberMobilePhone, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth, MemberPassword, CountryCode, ReferCardNumber,
        NickName, MemberEmail, MemberDefLanguage, ReceiveAllAdvertising, AcceptPhoneAdvertising, AgeBracklet)      
  values(@MemberRegisterMobile, @MemberEngFamilyName,@MemberEngGivenName, @MemberChiFamilyName,@MemberChiGivenName, @MemberSex,   
        @MemberMobilePhone, @MemberDateOfBirth, @MemberDayOfBirth, @MemberMonthOfBirth, @MemberYearOfBirth, @MD5InitPWD, @CountryCode, @ReferCardNumber,
        @NickName, @Email, @MemberDefLanguage, @RecPromotion, @RecPhoneAdv, @AgeBracklet)    
  set @NewMemberID = SCOPE_IDENTITY()  
  if isnull(@NewMemberID, 0) = 0 
  begin 
    rollback tran
    return -97  
  end  
       
/*      
  -- 同步增加 MemberMessageAccount 表记录.  
  exec SetMemberMessageAccount 0, 1, @NewMemberID, null, 2, @MemberMobilePhone, 0, ''  
  exec SetMemberMessageAccount 0, 1, @NewMemberID, null, 3, @MemberMobilePhone, 0, ''  
*/

  if isnull(@CardNumber, '') <> ''    -- 绑定指定的卡
  begin  
    set @NewCardNumber = ''
    select @NewCardNumber = CardNumber, @CardExpiryDate = CardExpiryDate, @CardStatus = Status, @CardGradeID = CardGradeID 
      from Card where CardNumber = @CardNumber and Status in (0,1) and isnull(MemberID, 0) = 0
    if isnull(@NewCardNumber, '') = ''  
      set @Return = -36  
    else begin  
      
--      Update Card set Status = 2, MemberID = @NewMemberID, UpdatedOn = getdate()   
--         where CardNumber = @NewCardNumber        
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      values
        (26, @NewCardNumber, null, 0, '', 0, 0, 0, 0, @BusDate, @TxnDate, 
         null, null, @NewMemberID, null, '', @NewMemberID, null, null, null,
          0, 0,  @CardExpiryDate, @CardExpiryDate, @CardStatus, 2)  

          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR  
            select CouponTypeID, HoldCount from CardGradeHoldCouponRule where CardGradeID = @CardGradeID and RuleType = 1
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN

            set @CouponNumber = ''
            set @i = 1
            DECLARE CUR_MemberBindCouponCount CURSOR fast_forward local FOR  
              select CouponNumber, CouponExpiryDate from coupon where CouponTypeID = @BindCouponTypeID and Status = 1 
            OPEN CUR_MemberBindCouponCount  
            FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate 
            WHILE @@FETCH_STATUS=0  
            BEGIN
              set @CouponExpiryDate = isnull(@CouponExpiryDate, getdate())            
              if @i <= @BindCouponCount
              begin              
                if isnull(@CouponNumber, '') <> ''
                begin
                  exec GenApprovalCode @ApprovalCode output
                  exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                  exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                  insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                  select  
                      32, @NewCardNumber, CouponNumber, CouponTypeID, '', 0, '', CouponAmount, 0, CouponAmount,
                      @BusDate, @TxnDate, '','', @NewMemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2
                    from Coupon where CouponNumber = @CouponNumber
                end
                set @i = @i + 1
              end else
                break
              FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate  
            END  
            CLOSE CUR_MemberBindCouponCount   
            DEALLOCATE CUR_MemberBindCouponCount                 
            
            FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
          /*=====================================================*/                                     
    end     
  end else
  begin
    --绑定一个新的SVA 卡号 (Demo 版本，每个cardtype 都给一个卡， 没有的跳过)  
    if isnull(@AutoBindCard, 0) = 1  
    begin  
      if isnull(@CardTypeCode, '') = ''  
      begin  
        DECLARE CUR_MemberBindCard CURSOR fast_forward local FOR  
          select CardTypeID from CardType  
        OPEN CUR_MemberBindCard  
        FETCH FROM CUR_MemberBindCard INTO @CardTypeID  
        WHILE @@FETCH_STATUS=0  
        BEGIN 
          set @NewCardNumber = '' 
          select Top 1 @NewCardNumber = C.CardNumber, @CardExpiryDate = C.CardExpiryDate, @CardStatus = C.Status, @CardGradeID = C.CardGradeID
            from Card C left join CardGrade G on C.CardGradeID = G.CardGradeID 
             where C.CardTypeID = @CardTypeID and C.Status in (0,1) and isnull(C.MemberID, 0) = 0  
            order by G.CardGradeRank, G.CardGradeID, C.CardNumber
--          Update Card set Status = 2, MemberID = @NewMemberID, UpdatedOn = getdate()   
--            where CardTypeID = @CardTypeID and CardNumber = @NewCardNumber   
if isnull(@NewCardNumber, '') <> ''
begin
  set @BindCardNumber = @NewCardNumber
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      values
        (26, @NewCardNumber, null, 0, '', 0, 0, 0, 0, @BusDate, @TxnDate, 
         null, null, @NewMemberID, null, '', @NewMemberID, null, null, null,
          0, 0,  @CardExpiryDate, @CardExpiryDate, @CardStatus, 2)  
                   
          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR  
            select CouponTypeID, HoldCount from CardGradeHoldCouponRule where CardGradeID = @CardGradeID and RuleType = 1
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN            
            set @CouponNumber = ''
            set @i = 1
            DECLARE CUR_MemberBindCouponCount CURSOR fast_forward local FOR  
              select CouponNumber, CouponExpiryDate from coupon where CouponTypeID = @BindCouponTypeID and Status = 1 
            OPEN CUR_MemberBindCouponCount  
            FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate  
            WHILE @@FETCH_STATUS=0  
            BEGIN
              set @CouponExpiryDate = isnull(@CouponExpiryDate, getdate())
              if @i <= @BindCouponCount
              begin              
                if isnull(@CouponNumber, '') <> ''
                begin
                  exec GenApprovalCode @ApprovalCode output
                  exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                  exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                  insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                  select  
                      32, @NewCardNumber, CouponNumber, CouponTypeID, '', 0, '', CouponAmount, 0, CouponAmount,
                      @BusDate, @TxnDate, '','', @NewMemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2
                    from Coupon where CouponNumber = @CouponNumber
                end
                set @i = @i + 1
              end else
                break							   
              FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate  
            END  
            CLOSE CUR_MemberBindCouponCount   
            DEALLOCATE CUR_MemberBindCouponCount                 
            
            FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
          /*=====================================================*/                                
end                                           
          FETCH FROM CUR_MemberBindCard INTO @CardTypeID  
        END  
        CLOSE CUR_MemberBindCard   
        DEALLOCATE CUR_MemberBindCard       
        
        if @BindCardNumber = ''
          set @return = -36
      end else  
      begin  
        select @CardTypeID = CardTypeID from CardType where CardTypeCode = @CardTypeCode  
        select Top 1 @NewCardNumber = CardNumber, @ReturnCardPassword = CardPassword, @CardExpiryDate = C.CardExpiryDate, @CardStatus = C.Status, @CardGradeID = C.CardGradeID from Card C   
             left join CardGrade G on C.CardGradeID = G.CardGradeID  
          where C.CardTypeID = @CardTypeID and Status in (0,1) and isnull(C.MemberID, 0) = 0
          order by G.CardGradeRank, G.CardGradeID, C.CardNumber        
    
        if isnull(@NewCardNumber, '') = ''  
          set @Return =  -36  
        else 
        begin         
--          Update Card set Status = 2, MemberID = @NewMemberID, UpdatedOn = getdate()   
--             where CardTypeID = @CardTypeID and CardNumber = @NewCardNumber                  
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      values
        (26, @NewCardNumber, null, 0, '', 0, 0, 0, 0, @BusDate, @TxnDate, 
         null, null, @NewMemberID, null, '', @NewMemberID, null, null, null,
          0, 0,  @CardExpiryDate, @CardExpiryDate, @CardStatus, 2)  
             
          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR  
            select CouponTypeID, HoldCount from CardGradeHoldCouponRule where CardGradeID = @CardGradeID and RuleType = 1
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN            
            set @CouponNumber = ''
            set @i = 1
            DECLARE CUR_MemberBindCouponCount CURSOR fast_forward local FOR  
              select CouponNumber, CouponExpiryDate from coupon where CouponTypeID = @BindCouponTypeID and Status = 1 
            OPEN CUR_MemberBindCouponCount  
            FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate  
            WHILE @@FETCH_STATUS=0  
            BEGIN
              set @CouponExpiryDate = isnull(@CouponExpiryDate, getdate())
              if @i <= @BindCouponCount
              begin              
                if isnull(@CouponNumber, '') <> ''
                begin
                  exec GenApprovalCode @ApprovalCode output
                  exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                  exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                  insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                  select  
                      32, @NewCardNumber, CouponNumber, CouponTypeID, '', 0, '', CouponAmount, 0, CouponAmount,
                      @BusDate, @TxnDate, '','', @NewMemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2
                    from Coupon where CouponNumber = @CouponNumber
                end
                set @i = @i + 1
              end else
                break
              FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate  
            END  
            CLOSE CUR_MemberBindCouponCount   
            DEALLOCATE CUR_MemberBindCouponCount                 
            
            FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
          /*=====================================================*/                                  
          
        end               
      end    
    end   
  end
 
  -- 考虑到registerNo是邮箱,或者其他号码, Account中手机号码用 @CountryCode+@MemberMobilePhone
 -- insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
 --      CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
 --   values(@NewMemberID, 2, @MemberRegisterMobile, 1, '', 1, getdate(), @NewMemberID, getdate(), @NewMemberID)
  insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
       CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag, PromotionFlag)
    values(@NewMemberID, 2,  isnull(@CountryCode,'')+isnull(@MemberMobilePhone,''), 1, '', 1, 
       getdate(), @NewMemberID, getdate(), @NewMemberID, 0, @RecSMSAdv)
  
  -- 增加插入邮箱
  if ISNULL(@Email, '') <> ''
    insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
         CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag, PromotionFlag)
      values(@NewMemberID, 1,  @Email, 1, '', 1, getdate(), @NewMemberID, getdate(), @NewMemberID, 0, @RecEmailAdv)
  -- 增加插入AppleNotification
    insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
         CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag)
      values(@NewMemberID, 9,  @AppleNotification, 1, '', 1, getdate(), @NewMemberID, getdate(), @NewMemberID, 0)
  -- 增加插入GoogleNotificatio
    insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
         CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag)
      values(@NewMemberID, 10,  @GoogleNotification, 1, '', 1, getdate(), @NewMemberID, getdate(), @NewMemberID, 0)
  -- 增加插入内部消息
    insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
         CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, VerifyFlag)
      values(@NewMemberID, 0,  @NewMemberID, 1, '', 1, getdate(), @NewMemberID, getdate(), @NewMemberID, 0)
                               
  set @ReturnMemberID = @NewMemberID  
  set @ReturnCardNumber = @NewCardNumber  
  --set @ReturnCardPassword =   
  set @ReturnMemberPassword  = @InitPWD    
  
  -- 返回信息JSON
  set @InputString = ''
  set @InputString = 'MPASSWORD=' + isnull(@ReturnMemberPassword,'') + ';MEMBERID=' + cast(@ReturnMemberID as varchar)
     	   + ';MSGACCOUNT=' + cast(@ReturnMemberID as varchar) + ';MSGACCOUNTTYPE=0'
  if CharIndex('@', @MemberRegisterMobile) > 0      	    
    set @InputString = @InputString + '|' + 'MPASSWORD=' + isnull(@ReturnMemberPassword,'') + ';MEMBERID=' + cast(@ReturnMemberID as varchar)
     	   + ';MSGACCOUNT=' + @MemberRegisterMobile + ';MSGACCOUNTTYPE=1'   
  else
    set @InputString = @InputString + '|' + 'MPASSWORD=' + isnull(@ReturnMemberPassword,'') + ';MEMBERID=' + cast(@ReturnMemberID as varchar)
     	   + ';MSGACCOUNT=' + @CountryCode + @MemberRegisterMobile + ';MSGACCOUNTTYPE=2'      	      

  exec GenMessageJSONStr 'NewMemberAccount', @InputString, @ReturnMessageStr  output
  

        -- 写入内部欢迎消息
/*          
        set @Messagecontent = '歡迎加入7FANS'
        set @MessageBody = CAST(@Messagecontent as varbinary)
        set @ReceiveList = CAST(@ReturnMemberID as varchar)
        exec SendMemberMessageObject 1, 0, 0, 1, 1, 0, '歡迎加入7FANS',  @MessageBody, 1, @ReceiveList, @MessageID output    
*/
  
--  exec GenBusinessMessage 508, @NewMemberID, @CardNumber, @MsgCouponNumber, @CardTypeID, @CardGradeID, @MsgCardBrandID,  @MsgCouponTypeID, ''  
              
  if @return = 0 
  begin
    commit tran
    exec RecordUserAction @NewMemberID, 4, @CardNumber, '', 'New Member', '', '', '', @NewMemberID
  end else 
    rollback tran
      
  return @return        
end  


GO
