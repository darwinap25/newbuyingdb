USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductStyles]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductStyles]
  @DepartCode           varchar(64),       -- 部门编码
  @BrandID              int,               -- 品牌ID
  @ProdType             int,               -- 货品类型
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr         varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1     
AS
/****************************************************************************
**  Name : GetProductStyles
**  Version: 1.0.0.7
**  Description : 获得Product Line数据	 
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = '01' 
  exec @a = GetProductStyles @DepartCode, '', null,'', 1,10,@PageCount output,@RecordCount output,'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount  
  
**  Created by: Gavin @2013-05-07
**  Modify by: Gavin @2013-05-15 （ver 1.0.0.1）只取Product_style表中ProdCodeStyle记录的货品
**  Modify by: Gavin @2013-05-17 （ver 1.0.0.2）修正bug	. 增加分页
**  Modify by: Gavin @2013-06-08  (ver 1.0.0.3) (for SASA) 类似GetProduct,根据传入的@DepartCode，到Product_Catalog表中查询
**  Modify by: Gavin @2013-07-09  (ver 1.0.0.4) 取消SASA的特别修改。
**  Modify by: Gavin @2013-07-10  (ver 1.0.0.5) 增加返回字段 ProdCodeStyle
**  Modify by: Gavin @2013-07-12  (ver 1.0.0.6) 增加返回字段 A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6，增加输入参数@QueryCondition
**  Modify by: Gavin @2013-07-15  (ver 1.0.0.7) 为配合WebService, 更换一下输出字段的顺序
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  
  set @BrandID = isnull(@BrandID, 0)        
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))
  set @ProdType = isnull(@ProdType, 0)
  
  set @SQLStr = 'select P.ProdCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName , '	  
	  + '	 P.ProdPicFile, P.DepartCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '	  
	  + ' D.BrandID, P.ProdType, P.ProdNote, P.ProductBrandID, '	  
      + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName, '
	  + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandDesc2 when 3 then B.ProductBrandDesc3 else B.ProductBrandDesc1 end as ProductBrandDesc, '
	  + '   ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, S.ProdCodeStyle, '
	  + '   A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6 '
  set @SQLStr = @SQLStr + ' from product P '      
      + ' left join Product_Brand B on P.ProductBrandID = B.ProductBrandID '
      + ' left join Color C on P.ColorID = C.ColorID '
      + ' left join Department D on D.DepartCode = P.DepartCode ' 
      + ' left join (select ProdCodeStyle, max(ProdCode) as PPCode from Product_style group by ProdCodeStyle) S on P.ProdCode = S.PPCode ' 
      + ' left join (select * from Product_Particulars where LanguageID = ' + cast(@Language as varchar) + ') A on P.ProdCode = A.Prodcode '		
      
  set @SQLStr = @SQLStr + ' where isnull(S.PPCode, '''') <> '''' '
    + ' and (P.DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + '''= '''')'      
    + ' and (P.ProductBrandID = ' + cast(@BrandID as varchar)+ ' or ' + cast(@BrandID as varchar) + '= 0)'      
    + ' and (P.ProdType = ' + cast(@ProdType as varchar)+ ' or ' + cast(@ProdType as varchar) + '= 0)'      

  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @QueryCondition
  
  return 0
end

GO
