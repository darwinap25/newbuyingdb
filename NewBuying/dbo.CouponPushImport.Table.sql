USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponPushImport]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponPushImport](
	[MemberOnlyNumber] [varchar](64) NOT NULL,
	[NumberType] [int] NOT NULL,
	[CouponTypeCode] [varchar](64) NOT NULL,
	[MemberOtherNumber] [varchar](64) NULL,
	[Qty] [int] NULL DEFAULT ((1)),
	[Status] [int] NULL DEFAULT ((0)),
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_COUPONPUSHIMPORT] PRIMARY KEY CLUSTERED 
(
	[MemberOnlyNumber] ASC,
	[NumberType] ASC,
	[CouponTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员的MemberRegisterMobile 或者 MemberMobilePhone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponPushImport', @level2type=N'COLUMN',@level2name=N'MemberOnlyNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1： MemberOnlynumber里面是MemberRegisterMobile。 2： 里面是MemberMobilePhone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponPushImport', @level2type=N'COLUMN',@level2name=N'NumberType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'需要推送的CouponTypeCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponPushImport', @level2type=N'COLUMN',@level2name=N'CouponTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果两个号码都提供的话， 这里可以放 mobilephone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponPushImport', @level2type=N'COLUMN',@level2name=N'MemberOtherNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'需要推送的coupon数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponPushImport', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0：未发送。1：已发送。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponPushImport', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入Coupon的推送列表（for bauhaus） @2017-01-09

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponPushImport'
GO
