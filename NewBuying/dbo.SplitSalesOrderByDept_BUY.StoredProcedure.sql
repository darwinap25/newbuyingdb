USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SplitSalesOrderByDept_BUY]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[SplitSalesOrderByDept_BUY]
  @UserID                           INT,                   -- 操作员ID
  @TxnNo                            VARCHAR(64)            -- 交易号码
AS
/****************************************************************************
**  Name : SplitSalesOrderByDept_BUY  （BUYING DB）
**  Version: 1.0.0.0
**  Description : 拆分交易单.
    exec SplitSalesOrderByDept 666,'20151010watsons214R01000006'
**  Created by: Gavin @2016-05-05   
****************************************************************************/
BEGIN
  DECLARE @DeptCode VARCHAR(64), @NewTxnNo VARCHAR(64), @NewTotal MONEY
  DECLARE @DeptList TABLE(DepartCode VARCHAR(64), NewTotal MONEY)

  SELECT * INTO #Sales_D_Temp FROM Sales_D WHERE TransNum = @TxnNo
  SELECT * INTO #Sales_H_Temp FROM Sales_H WHERE TransNum = @TxnNo

  INSERT INTO @DeptList(DepartCode, NewTotal)
    SELECT DepartCode, SUM(NetAmount) as NewTotal FROM #Sales_D_Temp D 
    WHERE TransNum = @TxnNo
	GROUP BY DepartCode
  DECLARE CUR_SplitSalesOrderByDept CURSOR fast_forward local for
    SELECT DepartCode,NewTotal FROM @DeptList
  OPEN CUR_SplitSalesOrderByDept    
  FETCH FROM CUR_SplitSalesOrderByDept INTO @DeptCode,@NewTotal
  WHILE @@FETCH_STATUS=0    
  BEGIN
    exec GetRefNoString 'SPTXNNO', @NewTxnNo output 

	INSERT INTO Sales_D(TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, DepartCode,
	    ProdCode, ProdDesc, Serialno, Collected, OrgPrice, OrgAmount, UnitAmount, TotalQty, DiscountPrice, DiscountAmount, POPrice, 
		ExtraPrice, POReasonCode, RPriceTypeCode, SerialNoType, IMEI, StockTypeCode, PickupLocation, PickupStaff,
		PickupDate, DeliveryDate, DeliveryBy, OrgTransNum, OrgSeqNo, Remark, RefGUID,
	    UnitPrice, NetPrice, Qty, NetAmount, Additional1,Additional2,Additional3, CreatedBy, UpdatedBy, ReservedDate)
    select @NewTxnNo, SeqNo, TransType, StoreCode, RegisterCode, BusDate, DepartCode,
	    ProdCode, ProdDesc, Serialno, Collected, OrgPrice, OrgAmount, UnitAmount,TotalQty, DiscountPrice, DiscountAmount, POPrice, 
		ExtraPrice, POReasonCode, RPriceTypeCode, SerialNoType, IMEI, StockTypeCode, PickupLocation, PickupStaff,
		PickupDate, DeliveryDate, DeliveryBy, OrgTransNum, OrgSeqNo, Remark, RefGUID,
		UnitPrice, NetPrice, Qty, NetAmount, Additional1,Additional2,Additional3, @UserID, @UserID, ReservedDate 
	  from #Sales_D_Temp
	WHERE DepartCode = @DeptCode
    
	INSERT INTO Sales_H (TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,TotalAmount,Status,
      TransDiscount,TransDiscountType,TransReason,RefTransNum,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
      DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
      RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
      SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    SELECT @NewTxnNo,20,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,@NewTotal,Status,
      TransDiscount,TransDiscountType,TransReason,@TxnNo,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
      DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
      RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
      SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy
	 FROM #Sales_H_Temp

    FETCH FROM CUR_SplitSalesOrderByDept INTO @DeptCode,@NewTotal
  END    
  CLOSE CUR_SplitSalesOrderByDept    
  DEALLOCATE CUR_SplitSalesOrderByDept 

  UPDATE Sales_H SET Status = 10, UpdatedOn = GETDATE(), UpdatedBy = @UserID  WHERE TransNum = @TxnNo

  DROP TABLE #Sales_D_Temp
  DROP TABLE #Sales_H_Temp
  return 0
END

GO
