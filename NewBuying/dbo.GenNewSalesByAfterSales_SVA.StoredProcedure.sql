USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenNewSalesByAfterSales_SVA]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenNewSalesByAfterSales_SVA]  
  @AfterSalesTxnNo				varchar(64),    -- 售后单号
  @RefSalesTxnNo				varchar(64),    -- 售后单中相关的交易单号
  @CreatedBy                    int, 
  @TxnType                      int,            -- 2: 退货。  3： 换货
  @NewTransNum                  varchar(64) output     -- 新产生的交易单号
AS
/****************************************************************************
**  Name : GenNewSalesByAfterSales_SVA
**  Version: 1.0.0.1
**  Description : (bauhaus需求) (SVA DB) 根据aftersales单，产生新的交易单, Tender使用Card支付.
**  Parameter :
**
declare @a int, @TxnNo varchar(64)
exec @a = VoidSalesByAfterSales @TxnNo output
print @a
print @TxnNo
** Create By Gavin @2017-03-15
** Modify By Gavin @2017-05-17 (ver 1.0.0.1) 去掉insert into card_movement部分
** 
****************************************************************************/
begin
  declare @MemberID int, @TotalAmt money, @CardNumber varchar(64), @CardOpenAmt money, @CardOpenPoint int, 
          @BusDate datetime, @TxnDate datetime, @StoreID int, @StoreCode varchar(64), @RegCode varchar(64), 
		  @ServCode varchar(64), @CardStatus int, @CardExpiryDate datetime, @TxnNo varchar(64), @ReturnAmount money
  declare @salesDCount int

  select @MemberID = memberid, @BusDate = BusDate, @TxnDate = TxnDate, @StoreCode = StoreCode, 
         @RegCode = RegisterCode, @ServCode = ServerCode
    from sales_H where TransNum = @RefSalesTxnNo
  select @TotalAmt = sum(isnull(NetAmount,0)) 
    from sales_D where TransNum = @RefSalesTxnNo
  select @CardNumber = CardNumber, @CardOpenAmt = TotalAmount, @CardOpenPoint = TotalPoints, 
         @CardStatus = Status, @CardExpiryDate = CardExpiryDate
    from Card where MemberID = @MemberID
  select @StoreID = StoreID from Store where StoreCode = @StoreCode 
  select @ReturnAmount = sum(isnull(netamount,0)) from aftersales_D where TxnNO = @AfterSalesTxnNo
  
  -- 产生新的交易
  if @TxnType = 2
  begin
    exec GetRefNoString 'KTXNNO', @TxnNo output   
	insert into sales_D (TransNum,SeqNo,ProdCode,ProdDesc,Serialno,Collected,UnitPrice,NetPrice,Qty,NetAmount,Additional1,POPrice,POReasonCode,
	    DiscountPrice,DiscountAmount,ReservedDate,PickupDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	select @TxnNo,ROW_NUMBER() OVER(order by A.ProdCode) as iid,A.ProdCode,A.ProdDesc,A.Serialno, A.Collected,
		A.UnitPrice,A.NetPrice, A.Qty - abs(isnull(B.Qty,0)), (A.Qty - abs(isnull(B.Qty,0))) * A.NetPrice, A.Additional1,A.POPrice,A.POReasonCode,
		A.DiscountPrice,A.DiscountAmount,A.ReservedDate,
		A.PickupDate,Getdate(),@CreatedBy,Getdate(),@CreatedBy 
	from (select * from sales_D where TransNum = @RefSalesTxnNo) A 
	left join (select * from aftersales_D where txnno = @AfterSalesTxnNo) B on A.SeqNo = B.RefSeqNo and A.ProdCode = B.ProdCode
    where (A.Qty - abs(isnull(B.Qty,0))) > 0

	insert into sales_T (TransNum,SeqNo,TenderID,TenderCode,TenderDesc,TenderAmount,LocalAmount,ExchangeRate,Additional,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	select Top 1 @TxnNo, 1, TenderID, TenderCode, TenderName1,@TotalAmt - abs(@ReturnAmount), @TotalAmt - abs(@ReturnAmount),1,@CardNumber,getdate(),@CreatedBy,getdate(),@CreatedBy  
	from TENDER where TenderType = 16

	insert into sales_h (TransNum,RefTransNum,StoreCode,RegisterCode,ServerCode,BrandCode,BusDate,TxnDate,TransType,CashierID,SalesManID,Channel,TotalAmount,
		Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliveryBy,DeliveryFullAddress,RequestDeliveryDate,DeliveryDate,
		DeliveryLongitude,DeliveryLatitude,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
		Additional,SalesTag,PaySettleDate,PickupType,PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
		BillContactPhone,InvalidateFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	select top 1 @TxnNo,@AfterSalesTxnNo,H.StoreCode,H.RegisterCode,ServerCode,BrandCode,AH.BusDate,AH.TxnDate,0,H.CashierID,H.SalesManID,Channel,@TotalAmt - abs(@ReturnAmount),
		4,H.MemberID,MemberName,MemberMobilePhone,H.CardNumber,MemberAddress,H.DeliveryBy,H.DeliveryFullAddress,RequestDeliveryDate,H.DeliveryDate,
		DeliveryLongitude,DeliveryLatitude,H.DeliveryNumber,H.LogisticsProviderID,H.Contact,H.ContactPhone,SalesReceipt,SalesReceiptBIN,
		Additional,SalesTag,H.PaySettleDate,H.PickupType,H.PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
		BillContactPhone,0,getdate(),@CreatedBy,getdate(),@CreatedBy
	from sales_h H left join aftersales_H AH on H.TransNum = AH.RefTransNum
	where H.TransNum = @RefSalesTxnNo

	-- 扣除卡中金额
    insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
        CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
        OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
    values
        (3, @CardNumber, '', 0, @RefSalesTxnNo, @CardOpenAmt, @TotalAmt - abs(@ReturnAmount), @CardOpenAmt + @TotalAmt - abs(@ReturnAmount), 0, @BusDate, @TxnDate, 
        null, null, '', '', @CreatedBy, @StoreID, @RegCode, @ServCode,
        @CardOpenPoint, @CardOpenPoint, @CardExpiryDate, @CardExpiryDate, @CardStatus, @CardStatus)     
  end
  else if @TxnType = 3 
  begin
    exec GetRefNoString 'KTXNNO', @TxnNo output  
	
	-- 根据原单插入salesD，去掉将被return 的 
	insert into sales_D (TransNum,SeqNo,ProdCode,ProdDesc,Serialno,Collected,UnitPrice,NetPrice,Qty,NetAmount,Additional1,POPrice,POReasonCode,
	    DiscountPrice,DiscountAmount,ReservedDate,PickupDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	select @TxnNo,ROW_NUMBER() OVER(order by A.ProdCode) as iid,A.ProdCode,A.ProdDesc,A.Serialno, A.Collected,
		A.UnitPrice,A.NetPrice, A.Qty - abs(isnull(B.Qty,0)), (A.Qty - abs(isnull(B.Qty,0))) * A.NetPrice, A.Additional1,A.POPrice,A.POReasonCode,
		A.DiscountPrice,A.DiscountAmount,A.ReservedDate,
		A.PickupDate,Getdate(),@CreatedBy,Getdate(),@CreatedBy 
	from (select * from sales_D where TransNum = @RefSalesTxnNo) A 
	left join (select * from aftersales_D where TxnNo = @AfterSalesTxnNo and Qty < 0) B on A.SeqNo = B.RefSeqNo and A.ProdCode = B.ProdCode
    where (A.Qty - abs(isnull(B.Qty,0))) > 0
	
	set @salesDCount = @@ROWCOUNT
	-- 插入新加的货品
	insert into sales_D (TransNum,SeqNo,ProdCode,ProdDesc,Serialno,Collected,UnitPrice,NetPrice,Qty,NetAmount,Additional1,POPrice,POReasonCode,
	    DiscountPrice,DiscountAmount,ReservedDate,PickupDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    select @TxnNo, (ROW_NUMBER() OVER(order by ProdCode) + @salesDCount) as iid,ProdCode,ProdDesc,'', Collected,
	    RetailPrice,NetPrice,Qty,NetAmount,'',POPrice,'',0,0,null,null,Getdate(),@CreatedBy,Getdate(),@CreatedBy 
	from aftersales_D A 
	where txnno = @AfterSalesTxnNo and Qty > 0

	insert into sales_T (TransNum,SeqNo,TenderID,TenderCode,TenderDesc,TenderAmount,LocalAmount,ExchangeRate,Additional,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	select Top 1 @TxnNo, 1, TenderID, TenderCode, TenderName1,@TotalAmt - abs(@ReturnAmount), @TotalAmt - abs(@ReturnAmount),1,@CardNumber,getdate(),@CreatedBy,getdate(),@CreatedBy  
	from TENDER where TenderType = 16

	insert into sales_h (TransNum,RefTransNum,StoreCode,RegisterCode,ServerCode,BrandCode,BusDate,TxnDate,TransType,CashierID,SalesManID,Channel,TotalAmount,
		Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliveryBy,DeliveryFullAddress,RequestDeliveryDate,DeliveryDate,
		DeliveryLongitude,DeliveryLatitude,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
		Additional,SalesTag,PaySettleDate,PickupType,PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
		BillContactPhone,InvalidateFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	select top 1 @TxnNo,@AfterSalesTxnNo,H.StoreCode,H.RegisterCode,ServerCode,BrandCode,AH.BusDate,AH.TxnDate,0,H.CashierID,H.SalesManID,Channel,@TotalAmt - abs(@ReturnAmount),
		4,H.MemberID,MemberName,MemberMobilePhone,H.CardNumber,MemberAddress,H.DeliveryBy,H.DeliveryFullAddress,RequestDeliveryDate,H.DeliveryDate,
		DeliveryLongitude,DeliveryLatitude,H.DeliveryNumber,H.LogisticsProviderID,H.Contact,H.ContactPhone,SalesReceipt,SalesReceiptBIN,
		Additional,SalesTag,H.PaySettleDate,H.PickupType,H.PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
		BillContactPhone,0,getdate(),@CreatedBy,getdate(),@CreatedBy
	from sales_h H left join aftersales_H AH on H.TransNum = AH.RefTransNum
	where H.TransNum = @RefSalesTxnNo

	-- 扣除卡中金额
/*
    insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
        CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
        OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
    values
        (3, @CardNumber, '', 0, @RefSalesTxnNo, @CardOpenAmt, -(@TotalAmt - (@ReturnAmount)), @CardOpenAmt - (@TotalAmt - (@ReturnAmount)), 0, @BusDate, @TxnDate, 
        null, null, '', '', @CreatedBy, @StoreID, @RegCode, @ServCode,
        @CardOpenPoint, @CardOpenPoint, @CardExpiryDate, @CardExpiryDate, @CardStatus, @CardStatus)     
*/
  end

  set @NewTransNum = @TxnNo
  return 0
end

GO
