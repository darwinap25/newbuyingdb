USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportCardUID_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportCardUID_H](
	[ImportCardNumber] [varchar](64) NOT NULL,
	[ImportCardDesc1] [nvarchar](512) NULL,
	[ImportCardDesc2] [nvarchar](512) NULL,
	[ImportCardDesc3] [nvarchar](512) NULL,
	[NeedActive] [int] NULL,
	[NeedNewBatch] [int] NULL,
	[CardCount] [int] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
 CONSTRAINT [PK_ORD_IMPORTCARDUID_H] PRIMARY KEY CLUSTERED 
(
	[ImportCardNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_ImportCardUID_H] ADD  DEFAULT ((0)) FOR [NeedActive]
GO
ALTER TABLE [dbo].[Ord_ImportCardUID_H] ADD  DEFAULT ((1)) FOR [NeedNewBatch]
GO
ALTER TABLE [dbo].[Ord_ImportCardUID_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_ImportCardUID_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Update_Ord_ImportCardUID_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_ImportCardUID_H] ON [dbo].[Ord_ImportCardUID_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_ImportCardUID_H
* Version: 1.0.0.0
* Description : 导入CardNumber的批核触发器

  select * from Ord_ImportCardUID_H
** Create By Gavin @2014-06-04
*/
/*==============================================================*/
BEGIN  
  declare @ImportCardNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  
  DECLARE CUR_Ord_ImportCardUID_H CURSOR fast_forward FOR
    SELECT ImportCardNumber, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_Ord_ImportCardUID_H
  FETCH FROM CUR_Ord_ImportCardUID_H INTO @ImportCardNumber, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where ImportCardNumber = @ImportCardNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
      exec ImportCardUID @ImportCardNumber, 0, 1, '', @CreatedBy
      update Ord_ImportCardUID_H set ApprovalCode = @ApprovalCode where ImportCardNumber = @ImportCardNumber
    end      
    FETCH FROM CUR_Ord_ImportCardUID_H INTO @ImportCardNumber, @ApproveStatus, @CreatedBy  
  END
  CLOSE CUR_Ord_ImportCardUID_H 
  DEALLOCATE CUR_Ord_ImportCardUID_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键。导入单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'ImportCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'ImportCardDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'ImportCardDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'ImportCardDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绑定时激活Card。0：不激活。1：激活' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'NeedActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否先创建新批次的Card，使用这些新的Card来绑定。
0：不创建，使用已有的Card，没有足够Card则报错。
1：先创建此数量的Card，再绑定这些Card。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'NeedNewBatch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card的数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'CardCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入Card的UID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCardUID_H'
GO
