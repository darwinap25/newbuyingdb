USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberPreference]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[GetMemberPreference]
  @DeparteCode               varchar(64),    -- 
  @MemberID                  int             -- Member ID 
AS
/****************************************************************************
**  Name : GetMemberPreference  (bauhaus)
**  Version: 1.0.0.1
**  Description :
exec GetMemberPreference '', '', 1
**  Created by: Gavin @2016-06-24 
**  Modify by: Gavin @2016-07-05 (ver 1.0.0.1) add response colorcode 
****************************************************************************/
BEGIN
  declare @ColorID int,  @SizeID int, @MemberDefLanguage int
  set @DeparteCode = isnull(@DeparteCode, '')
  if @DeparteCode = '' 
    return -1

  select @MemberDefLanguage = MemberDefLanguage from Member where MemberID = @MemberID

  select top 1 D.ProdCode, S.ProdCodeStyle as ProductStyleCode,
    case @MemberDefLanguage when 2 then  O.ColorName2 when 3 then O.ColorName3 else O.ColorName1 end as ColorName, 
    case @MemberDefLanguage when 2 then  Z.ProductSizeName2 when 3 then Z.ProductSizeName3 else Z.ProductSizeName1 end as ProductSizeName,
	O.ColorCode 
   from Sales_D D 
    left join Sales_H H on D.TransNum = H.TransNum
	left join Product P on D.ProdCode = P.ProdCode
	left join Product_Style S on D.ProdCode = S.ProdCode 
	left join Color O on P.ColorID = O.ColorID
	left join Product_Size Z on P.ProductSizeID = Z.ProductSizeID
  where H.MemberID = @MemberID 
	--and (S.ProdCodeStyle = @ProductStyleCode or @ProductStyleCode = '')
	and (P.DepartCode = @DeparteCode or @DeparteCode = '')
   order by H.CreatedOn desc
  return 0
END

GO
