USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardDonate]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[MemberCardDonate]
  @UserID		        varchar(512),			-- 登录用户
  @CardNumber			nvarchar(512),			-- 会员卡号
  @OrganizationID       int,                    -- 被捐赠方的机构ID
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode           varchar(512),          -- 终端号码
  @ServerCode             varchar(512)='',		-- serverid 暂时用不到,预留
  @BrandCode			varchar(512),
  @TxnNoKeyID           varchar(512),			-- ReceiveTxn 表主键
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间
  @ActAmount		    money,					-- 操作金额
  @ActPoint		        int,					-- 操作积分    
  @SecurityCode         varchar(512),			-- 签名
  @Additional           varchar(512),			-- 附加信息
  @Remark				nvarchar(1000),			-- 注释。（或者transfer原因）
  @SourceCardAmount     money output,			-- 转出卡的余额
  @SourceCardPoints     int output			    -- 转出卡的积分余额
as
/****************************************************************************
**  Name : MemberCardDonate  
**  Version: 1.0.0.2
**  Description : 捐赠金额，积分给机构 
**  Parameter :
  declare
    @Result int	, @SourceCardAmount money,  @TargetCardAmount money, @SourceCardPoints int, @TargetCardPoints int
 -- exec @Result = MemberCardDonate '1', '000300001', '', 1, 'store1', '001', '01', '201205010100001', '2012050101', '2012-04-26', '2012-04-26', 100, 0, '', '','',
  exec @Result = MemberCardDonate '1', '000400001', '', 1, 'store1', '001', '01', '201205010100001', '2012050101', '2012-04-26', '2012-04-26', 100, 0, '', '','',
     @SourceCardAmount output, @TargetCardAmount output
  print @Result    
  print @SourceCardAmount
  print @SourceCardPoints
**  return: 

**  Created by: Gavin @2012-04-01
**  Modify by: Gavin @2012-08-29 (ver 1.0.0.1) 增加累计机构获得的捐赠金额和积分。 存放在机构表中。
**  Modify by: Gavin @2012-10-22 （ver 1.0.0.2）不传入Busdate，由存储过程自己获取
**
****************************************************************************/
begin
  declare @CardTypeID int, @TargetCardTypeID int, @ActCouponTypeID int, @MemberID int, @TargetCardNumber varchar(512), @OprID int
  declare @TargetCarNumber varchar(512),   @TargetCardAmount money, @TargetCardPoints int, @result int
  select @CardTypeID = CardTypeID from Card where CardNumber = @CardNumber
  select @TargetCardNumber = CardNumber from Organization where OrganizationID = @OrganizationID
  select @TargetCardTypeID = CardTypeID from Card where CardNumber = @TargetCarNumber
  
  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()              
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120) + cast(cast(floor(@ActAmount) as int) as varchar)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
    return @CheckResult
  -- end

  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
  
  if isnull(@CardNumber, '') = ''
    return -2
  if isnull(@TargetCardNumber, '') = ''
    return -2    

  set @OprID = 46  
  
  exec @result = MemberCardTransfer   @UserID, @CardNumber, @TargetCardNumber, @StoreCode, 
    @RegisterCode, @ServerCode, @BrandCode, @TxnNoKeyID, @TxnNo, @BusDate, @TxnDate, @OprID, @ActAmount, @ActPoint, '', @SecurityCode,
    @Additional, @Remark,  @SourceCardAmount output, @SourceCardPoints output, @TargetCardAmount output, @TargetCardPoints output
  
  -- 转赠成功，累计机构获得金额和积分。（目前存放在机构表中，因为card表中只能存放累计获得积分，和累计消费的金额，缺少存放累计获得金额的字段）
  if @result = 0
  begin
    update Organization set CumulativePoints = isnull(CumulativePoints, 0) + isnull(@ActPoint, 0), 
      CumulativeAmt = isnull(CumulativeAmt, 0) + isnull(@ActAmount, 0) 
    where OrganizationID = @OrganizationID
  end   
  
  return @result   
end

GO
