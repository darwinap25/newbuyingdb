USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetStoreTypeList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetStoreTypeList]    
  @StoreTypeID          int,               --店铺类型ID  
  @BrandID              int,               --品牌ID
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。   
  @LanguageAbbr			varchar(20)=''
AS
/****************************************************************************
**  Name : GetStoreTypeList
**  Version: 1.0.0.1
**  Description :查找SVA中的StoreType列表
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetStoreTypeList 0, 0 ,1, 30, @count output, @recordcount output, 1
  print @a  
  print @count
  print @recordcount
  select * from brand
**  Created by: Gavin @2013-01-29
**  Modify by: Gavin @2013-01-31 (ver 1.0.0.1) 按要求不增加brand和storetype的关联表, 先根据brandid到store查找storetypeid, 以此数据为过滤条件
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  
  set @StoreTypeID = isnull(@StoreTypeID, 0)
  set @BrandID = isnull(@BrandID, 0)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  set @SQLStr =  'select case ' + cast(@Language as varchar) + ' when 2 then StoreTypeName2 when 3 then StoreTypeName3 else StoreTypeName1 end as StoreTypeName, '
       + ' StoreTypeID, StoreTypeCode, StoreTypeName1, StoreTypeName2, StoreTypeName3 '
       + ' from storetype where (StoreTypeID = '	+ cast(@StoreTypeID as varchar) + ' or ' + cast(@StoreTypeID as varchar) + ' = 0)'  
       + ' and (' + cast(@BrandID as varchar) + '= 0 or StoreTypeID in (select storetypeid from store where brandid = ' + cast(@BrandID as varchar) + '))'   
  exec SelectDataInBatchs @SQLStr, 'StoreTypeID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output  
  return 0
end

GO
