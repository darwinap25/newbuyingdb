USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCouponTypeStoreCondition]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateCouponTypeStoreCondition]
  @CouponTypeID			int,			-- 指定CouponTypeID. 
  @BrandID				int,			-- 条件中brand变动，或者brand 内容变动
  @LocationID			int,			-- 条件中location 变动，或者location 内容变动
  @StoreID				int				-- 条件中location 变动
AS
/****************************************************************************
**  Name : UpdateCouponTypeStoreCondition 
**  Version: 1.0.0.1
**  Description :  优惠劵的店铺条件。把条件（brandid，locationid 转换到storeid list）
**
  declare @MemberID int, @A int
  exec @A = UpdateCouponTypeStoreCondition 0,0,0,0
  print @A
 select * from CouponTypeStoreCondition_List

**  Created by: Gavin @2012-05-28
**  Modify By Gavin @2012-10-31 (ver 1.0.0.1) 修正Bug，删除List记录时，必须加上StoreConditionType条件
**
****************************************************************************/
begin
  declare @LocationFullPath varchar(512), @SQLStr nvarchar(4000)
  declare @CouponTypeStoreConditionID int, @StoreConditionType int, @ConditionType int, @ConditionID int, @CreatedBy int, @UpdatedBy int

  if isnull(@CouponTypeID, 0) = 0    -- 未指定CouponTypeID，表示修改的是brand或者location。 需要同步CouponTypeStoreCondition_List中所有使用到的记录。
  begin        
    DECLARE CUR_AllStoreCondition CURSOR fast_forward local FOR
      select CouponTypeID, CouponTypeStoreConditionID, StoreConditionType, ConditionType, ConditionID, CreatedBy, UpdatedBy from CouponTypeStoreCondition        
    OPEN CUR_AllStoreCondition
    FETCH FROM CUR_AllStoreCondition INTO @CouponTypeID, @CouponTypeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    WHILE @@FETCH_STATUS=0
    BEGIN
      if @ConditionType = 1
      begin
        delete from CouponTypeStoreCondition_List where ConditionType = @ConditionType and ConditionID = @ConditionID and CouponTypeID = @CouponTypeID  and StoreConditionType = @StoreConditionType
        insert into CouponTypeStoreCondition_List (CouponTypeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        select @CouponTypeID as CouponTypeID, @StoreConditionType as StoreConditionType, @ConditionType as ConditionType, @ConditionID as ConditionID, StoreID, getdate() as CreatedOn, @CreatedBy as CreatedBy, getdate() as UpdatedOn, @UpdatedBy as UpdatedBy          
        from Store where BrandID = @ConditionID        
      end
      if @ConditionType = 2
      begin
        delete from CouponTypeStoreCondition_List where ConditionType = @ConditionType and ConditionID = @ConditionID and CouponTypeID = @CouponTypeID  and StoreConditionType = @StoreConditionType
        select @LocationFullPath = LocationFullPath from Location where LocationID = @ConditionID        
        set @SQLStr = ' insert into CouponTypeStoreCondition_List (CouponTypeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy) '
          + ' select @CouponTypeID as CouponTypeID, @StoreConditionType as StoreConditionType, @ConditionType as ConditionType, @ConditionID as ConditionID, StoreID, getdate() as CreatedOn, @CreatedBy as CreatedBy, getdate() as UpdatedOn, @UpdatedBy as UpdatedBy '          
          + ' from Store where LocationID in (select LocationID from Location where LocationID in (' + @LocationFullPath + '))'
        exec sp_executesql @SQLStr  
      end
      if @ConditionType = 3
      begin
        delete from CouponTypeStoreCondition_List where ConditionType = @ConditionType and ConditionID = @ConditionID and CouponTypeID = @CouponTypeID  and StoreConditionType = @StoreConditionType
        insert into CouponTypeStoreCondition_List (CouponTypeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values(@CouponTypeID, @StoreConditionType, @ConditionType, @ConditionID, @ConditionID, getdate(), @CreatedBy, getdate(), @UpdatedBy)                  
      end          
      FETCH FROM CUR_AllStoreCondition INTO @CouponTypeID, @CouponTypeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    END
    CLOSE CUR_AllStoreCondition 
    DEALLOCATE CUR_AllStoreCondition     
  end else                           -- 指定CouponTypeID，则清空CouponTypeStoreCondition_List中此CouponTypeID的记录，重新产生。 
  begin
    delete from CouponTypeStoreCondition_List where CouponTypeID = @CouponTypeID
    DECLARE CUR_StoreCondition CURSOR fast_forward local FOR
      select CouponTypeStoreConditionID, StoreConditionType, ConditionType, ConditionID, CreatedBy, UpdatedBy from CouponTypeStoreCondition
        where CouponTypeID = @CouponTypeID
    OPEN CUR_StoreCondition
    FETCH FROM CUR_StoreCondition INTO @CouponTypeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    WHILE @@FETCH_STATUS=0
    BEGIN
      if @ConditionType = 1
      begin
        insert into CouponTypeStoreCondition_List (CouponTypeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        select @CouponTypeID as CouponTypeID, @StoreConditionType as StoreConditionType, @ConditionType as ConditionType, @ConditionID as ConditionID, StoreID, getdate() as CreatedOn, @CreatedBy as CreatedBy, getdate() as UpdatedOn, @UpdatedBy as UpdatedBy          
        from Store where BrandID = @ConditionID       
      end
      if @ConditionType = 2
      begin
        select @LocationFullPath = LocationFullPath from Location where LocationID = @ConditionID        
        set @SQLStr = ' insert into CouponTypeStoreCondition_List (CouponTypeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy) '
          + ' select @CouponTypeID as CouponTypeID, @StoreConditionType as StoreConditionType, @ConditionType as ConditionType, @ConditionID as ConditionID, StoreID, getdate() as CreatedOn, @CreatedBy as CreatedBy, getdate() as UpdatedOn, @UpdatedBy as UpdatedBy '
          + ' into CouponTypeStoreCondition_List '
          + ' from Store where LocationID in (select LocationID from Location where LocationID in (' + @LocationFullPath + '))'
        exec sp_executesql @SQLStr  
      end  
      if @ConditionType = 3  
        insert into CouponTypeStoreCondition_List
          (StoreID, CouponTypeID, StoreConditionType, ConditionType, ConditionID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)    
        values
          (@ConditionID, @CouponTypeID, @StoreConditionType, @ConditionType, @ConditionID, getdate(), @CreatedBy, getdate(), @UpdatedBy)  
      FETCH FROM CUR_StoreCondition INTO @CouponTypeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    END
    CLOSE CUR_StoreCondition 
    DEALLOCATE CUR_StoreCondition              
  end
  return 0
end

GO
