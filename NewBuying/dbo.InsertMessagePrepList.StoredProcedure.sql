USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[InsertMessagePrepList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InsertMessagePrepList]
  @OprID                int,                 -- 操作的OprID 
  @MemberID             int,                 -- 发生此操作的会员
  @CardNumber			      varchar(64),         -- 发生此操作的会员的卡
  @CouponNumber			    varchar(64),          -- 发生此操作的会员的Coupon
  @CardTypeID           int,
  @CardGradeID          int,
  @CardBrandID          int,
  @CouponTypeID         int,
  @ReceiveMobileNumber  varchar(64)='',       -- 仅转赠时使用
  @OtherMemberID        int=0,
  @OtherMemberCard      varchar(64)='',
  @CustomMsg            nvarchar(2000)=''    -- 用户自定义消息, 考虑还要加上模板消息, 限制在2000长度  
AS
/****************************************************************************
**  Name : InsertMessagePrepList
**  Version: 1.0.0.0
**  Description : 产生业务消息.(消息插入MessageObject.MessageReceiveList)，消息不包含内容，需要二次处理，结合模板产生正式消息。
** 
**  Created by:  Gavin @2014-09-01
****************************************************************************/
begin

  insert into MessagePrepList(MemberID,CardNumber,CardTypeID,CardGradeID,CardBrandID,CouponNumber,CouponTypeID,OprID,
        RelateMemberID,RelateCardNumber,RelateMemberMobile,CustomMsg)
  values(@MemberID,@CardNumber,@CardTypeID,@CardGradeID,@CardBrandID,@CouponNumber,@CouponTypeID,@OprID,
        @OtherMemberID,@OtherMemberCard,@ReceiveMobileNumber,@CustomMsg)  
              
  return 0   
end

GO
