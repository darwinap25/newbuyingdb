USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBatchTagDetails]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/14 09:51:34
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetBatchTagDetails]
  @batchtagcode         varchar(64)
AS
SET NOCOUNT ON;

BEGIN

    SELECT 
        [b].[BatchCouponCode] AS TransactionNUmber,
        --'ISSUED' AS STATUS,
		CASE
			WHEN c.Status = 0 THEN 'ENROLLED' --'REGISTERED'
			WHEN c.Status = 1 THEN 'ISSUED' --'ACTIVE'
			WHEN c.Status = 2 THEN 'VOID' --'UNREGISTERED'
            WHEN c.Status = 3 THEN 'REPORT LOST'
		END  AS STATUS,
        [b].[CreatedOn],        
        [c].[CouponTypeName1],
        [b].[Qty],
        [c].[CouponTypeCode] + RIGHT('0000000000' + CAST([b].[SeqFrom] AS varchar), 10) AS BatchStartingID,
        [c].[CouponTypeCode] + RIGHT('0000000000' + CAST([b].[SeqTo] AS varchar), 10) AS BatchEndingID,
        [a].[UserName] AS CreatedBy,
        [c].[CouponTypeCode] + RIGHT('0000000000' + CAST([c].[LastSequenceGenerated] AS varchar), 10) AS LastCreatedTagID,
        ISNULL([b].[Remark],'') AS Remark
    FROM [dbo].[BatchCoupon] b
        INNER JOIN [dbo].[CouponType] c
            ON [c].[CouponTypeID] = [b].[CouponTypeID]
        INNER JOIN [dbo].[Accounts_Users] a
            ON [a].[UserID] = [b].[CreatedBy]
    WHERE [b].[BatchCouponCode] = @batchtagcode    



END

GO
