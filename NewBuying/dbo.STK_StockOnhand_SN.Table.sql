USE [NewBuying]
GO
/****** Object:  Table [dbo].[STK_StockOnhand_SN]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STK_StockOnhand_SN](
	[SerialNo] [varchar](64) NOT NULL,
	[SerialNoType] [int] NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[StoreID] [int] NOT NULL,
	[StockTypeCode] [varchar](64) NOT NULL,
 CONSTRAINT [PK_STK_STOCKONHAND_SN] PRIMARY KEY CLUSTERED 
(
	[SerialNo] ASC,
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[STK_StockOnhand_SN] ADD  DEFAULT ((1)) FOR [SerialNoType]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'serialNo， 唯一号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand_SN', @level2type=N'COLUMN',@level2name=N'SerialNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'serialNo类型。用于以后扩展。 默认1. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand_SN', @level2type=N'COLUMN',@level2name=N'SerialNoType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand_SN', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_Store表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand_SN', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_StockType表 Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand_SN', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品的SerialNo库存。（SerialNo是唯一的）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockOnhand_SN'
GO
