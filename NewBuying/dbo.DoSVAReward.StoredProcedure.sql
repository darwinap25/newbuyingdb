USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoSVAReward]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoSVAReward]
  @CardNumber			varchar(64),
  @TxnNo                varchar(512),
  @RewardType           int    -- 奖励类型. 0:一般. 1:推荐新会员. 2:新会员奖励 3:facebook推广 4:会员生日 5：会员第一笔交易奖励。（给会员本人）6：会员第一笔交易奖励。（给会员的推荐人）
AS
/****************************************************************************
**  Name : DoSVAReward
**  Version: 1.0.0.3
**  Description : 按照SVARewardRules给予奖励。
**
**  Parameter :
  declare @a int
  exec @a = DoSVAReward '1111002','',0
  print @a
  select * from card where cardnumber = '1111002'	   1106630	68085.00
  update coupon set cardnumber = '' where  cardnumber = '1111002'   
  select * from coupon where  cardnumber = '1111002'  
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2013-08-30
**  Modify by: Gavin @2013-09-11 (ver 1.0.0.1)   赠送的coupon, 因为直接激活，初始的expiryDate使用coupon的expiryDate，而不是getdate
**  Modified by: Gavin @2014-12-31 (Ver 1.0.0.2) 统一startdate和enddate的比较规则：如果填写的日期不带时间（时间为0），则和 getdate（）比较时，startdate可以直接判断 < getdate， enddate 需要 >= (getdate() - 1)
**  Modified by: Gavin @2015-10-26 (Ver 1.0.0.3) 修正计算Coupon有效期时，使用的coupontypeid不正确的问题。修正获得未使用的coupon的方法 
**
****************************************************************************/
begin
  declare @RewardAmount money, @RewardPoint int, @RewardCouponTypeID int, @RewardCouponCount int
  declare @CardExpiryDate datetime, @CardStatus int, @CardTypeID int, @CardGradeID int, @BindCouponCount int, @i int
  declare @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate date, @NewCouponExpiryDate date
  declare @BusDate datetime, @TxnDate datetime, @BindCouponTypeID int, @CouponNumber varchar(64) 
  declare @CardAmount money, @CardPoint int, @MemberID int, @ApprovalCode varchar(6)
  
  set @TxnNo = isnull(@TxnNo, '')
  set @CardNumber = isnull(@CardNumber, '')
  if @TxnNo <> ''	 -- 不允许重复获得奖励
  begin
	if exists(select * from Card_Movement where CardNumber = @CardNumber and OprID = 27 and RefTxnNo = @TxnNo)
	  return -15
	if exists(select * from Coupon_Movement where CardNumber = @CardNumber and OprID = 27 and RefTxnNo = @TxnNo)
	  return -15	  
  end
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 
  if isnull(@TxnDate, 0) = 0
    set @TxnDate = getdate() 
  select @CardExpiryDate = CardExpiryDate, @CardStatus = Status, @CardGradeID = CardGradeID, @CardTypeID = CardTypeID,
       @CardAmount = TotalAmount, @CardPoint = TotalPoints, @MemberID = MemberID 
    from card where Cardnumber = @CardNumber
  if @@ROWCOUNT = 0	   -- 找不到卡直接退出.
    return -2
      
  DECLARE CUR_Reward CURSOR fast_forward local FOR  
    select isnull(RewardAmount,0), isnull(RewardPoint,0), isnull(RewardCouponTypeID,0), isnull(RewardCouponCount,0)  
     from SVARewardRules where startdate <= getdate() and enddate >= (getdate()-1) and status = 1 and CardGradeID = @CardGradeID
       and SVARewardType = @RewardType
  order by SVARewardRulesID desc
  OPEN CUR_Reward  
  FETCH FROM CUR_Reward INTO @RewardAmount,	@RewardPoint, @RewardCouponTypeID, @RewardCouponCount
  WHILE @@FETCH_STATUS=0  
  BEGIN
    if @RewardAmount > 0 or @RewardPoint > 0
    begin       
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      values
        (27, @CardNumber, null, 0, @TxnNo, @CardAmount, isnull(@RewardAmount,0), @CardAmount + isnull(@RewardAmount,0), isnull(@RewardPoint,0), @BusDate, @TxnDate, 
         null, null, @MemberID, null, '', @MemberID, null, null, null,
         @CardPoint, @CardPoint + isnull(@RewardPoint,0),  @CardExpiryDate, @CardExpiryDate, @CardStatus, @CardStatus)  
	 end
	  if @RewardCouponTypeID > 0 and @RewardCouponCount > 0
	  begin
          /*赠送coupon=====================================================*/                      
            exec GenApprovalCode @ApprovalCode output
            set @i = 1           
              while @i <= @RewardCouponCount
              begin 
                set @CouponNumber = ''
                exec GetCouponNumber @RewardCouponTypeID, 1, @CouponNumber output 
                             
                if isnull(@CouponNumber, '') <> ''
                begin
                  select @CouponExpiryDate = CouponExpiryDate, @CouponStatus = Status from Coupon where CouponNumber = @CouponNumber
                  if @CouponExpiryDate is null
                     set @CouponExpiryDate = getdate()                                    
                  exec CalcCouponNewStatus @CouponNumber, @RewardCouponTypeID, 27, @CouponStatus, @NewCouponStatus output
                  exec CalcCouponNewExpiryDate @RewardCouponTypeID, 27, @CouponExpiryDate, @NewCouponExpiryDate output     
                  insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                  select  
                      27, @CardNumber, CouponNumber, CouponTypeID, '', 0, @TxnNo, CouponAmount, 0, CouponAmount,
                      @BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2
                    from Coupon where CouponNumber = @CouponNumber
                end
                set @i = @i + 1
              end          
            
          /*=====================================================*/ 
    end 
    FETCH FROM CUR_Reward INTO @RewardAmount,	@RewardPoint, @RewardCouponTypeID, @RewardCouponCount      
  END  
  CLOSE CUR_Reward   
  DEALLOCATE CUR_Reward
        
  Return 0
end

GO
