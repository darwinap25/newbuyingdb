USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenPickupCardOrder_Purchase]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenPickupCardOrder_Purchase]
  @UserID                 int,             --操作员ID
  @CardOrderFormNumber    varchar(64)     --卡创建单的号码
AS
/****************************************************************************
**  Name : GenPickupCardOrder_Purchase
**  Version: 1.0.0.1
**  Description : 根据卡订单产生拣货单 (telco card 的充值订单(picking单))
**  example :
  declare @a int  , @CardOrderFormNumber varchar(64)
  set @CardOrderFormNumber = ''
  exec @a = GenPickupOrder_Card 1, @CardOrderFormNumber  
  print @a  
  select * from Ord_CardOrderForm_H
    select * from Ord_CardOrderForm_D
  select * from Ord_CardPicking_H
  select * from Ord_CardPicking_D
**  Created by: Gavin @2015-02-11
**  Modify by: Gavin @2015-04-20
**
****************************************************************************/
begin
  declare @BusDate datetime, @CardPickingNumber varchar(64), @HQStoreID int, @StoreID int
      
  -- 获取busdate
  select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1    

  -- 获取pickup单的单号
  exec GetRefNoString 'COPOCP', @CardPickingNumber output 
  -- 插入pickup单的头表数据 
  insert into Ord_CardPicking_H(CardPickingNumber, ReferenceNo, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod,
     SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, StoreMobile, FromContactName, FromContactNumber, 
     FromEmail, FromMobile, Remark, CreatedBusDate, ApproveStatus, CreatedBy, UpdatedBy, OrderType, Remark1, PurchaseType) 
  select @CardPickingNumber, CardOrderFormNumber, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod,
     SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, StoreMobile, FromContactName, FromContactNumber, 
     FromEmail, FromMobile, Remark, @BusDate,'R', @UserID, @UserID, OrderType, Remark1, PurchaseType 
    from Ord_CardOrderForm_H
   where CardOrderFormNumber = @CardOrderFormNumber and PurchaseType = 2
  
  select @HQStoreID = FromStoreID, @StoreID = StoreID
  from Ord_CardOrderForm_H
   where CardOrderFormNumber = @CardOrderFormNumber and PurchaseType = 2
   
  if @@ROWCOUNT > 0
  begin
    insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, 
        FirstCardNumber, EndCardNumber, BatchCardCode, OrderAmount, ActualAmount, PickAmount, OrderPoint, ActualPoint, PickPoint)
    select @CardPickingNumber, D.CardTypeID, D.CardGradeID, '', 0, 0, 
        C1.CardNumber, C2.CardNumber, '', OrderAmount, OrderAmount, OrderAmount, OrderPoint, OrderPoint, OrderPoint
     from Ord_CardOrderForm_D D 
       left join (select CardTypeID, CardGradeID, MIN(CardNumber) as CardNumber from Card where IssueStoreID = @HQStoreID and Status = 2 group by CardTypeID, CardGradeID) C1 on C1.CardTypeID = D.CardTypeID and C1.CardGradeID = D.CardGradeID
       left join (select CardTypeID, CardGradeID, MIN(CardNumber) as CardNumber from Card where IssueStoreID = @StoreID and Status = 2 group by CardTypeID, CardGradeID) C2 on C2.CardTypeID = D.CardTypeID and C2.CardGradeID = D.CardGradeID
    where CardOrderFormNumber = @CardOrderFormNumber
  end
  return 0
end

GO
