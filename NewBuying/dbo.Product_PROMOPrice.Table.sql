USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_PROMOPrice]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_PROMOPrice](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PromotionCode] [varchar](64) NOT NULL,
	[Description1] [nvarchar](512) NULL,
	[Description2] [nvarchar](512) NULL,
	[Description3] [nvarchar](512) NULL,
	[StoreCode] [varchar](64) NULL,
	[StoreGroupCode] [varchar](64) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[EntityNum] [varchar](64) NOT NULL,
	[EntityType] [int] NOT NULL,
	[HitAmount] [dbo].[Buy_Amt] NOT NULL,
	[HitOP] [int] NOT NULL,
	[DiscPrice] [dbo].[Buy_Amt] NOT NULL,
	[DiscType] [int] NULL,
	[DayFlagCode] [varchar](64) NULL,
	[WeekFlagCode] [varchar](64) NULL,
	[MonthFlagCode] [varchar](64) NULL,
	[LoyaltyFlag] [int] NULL,
	[LoyaltyCardTypeCode] [varchar](64) NULL,
	[LoyaltyCardGradeCode] [varchar](64) NULL,
	[BirthdayFlag] [int] NULL,
	[Status] [int] NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PRODUCT_PROMOPRICE] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Product_PROMOPrice] ADD  DEFAULT ('0') FOR [StartTime]
GO
ALTER TABLE [dbo].[Product_PROMOPrice] ADD  DEFAULT ('0') FOR [EndTime]
GO
ALTER TABLE [dbo].[Product_PROMOPrice] ADD  DEFAULT ((0)) FOR [LoyaltyFlag]
GO
ALTER TABLE [dbo].[Product_PROMOPrice] ADD  DEFAULT ((0)) FOR [BirthdayFlag]
GO
ALTER TABLE [dbo].[Product_PROMOPrice] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Product_PROMOPrice] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'Description1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'Description2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'Description3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺Code。空/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。空/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'StoreGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效时间（例如 上午9:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'StartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销失效时间（例如 下午20:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'EndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'EntityNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'决定EntityNum中内容的含义。
0：所有货品。EntityNum中为空
1：EntityNum内容为 prodcode。 销售EntityNum中指定货品时，可以生效
2：EntityNum内容为 DepartCode。。 销售EntityNum中指定部门的货品时，可以生效
3：EntityNum内容为 TenderCode。 使用EntityNum中指定货币支付时，可以生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'EntityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'HitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中金额关系操作符。
0：没有操作符，不和HitAmt比较
1： =     （等于时， 如果金额大于此值，也符合条件）
2： <>
3： <=
4：>=
5：<
6：>
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'HitOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣金额或者折扣比例' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'DiscPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣类型。
1：DiscPrice为 折扣减去的金额。（即 实际销售价格= 销售价格 - DiscPrice）
2：DiscPrice为 折扣销售的金额。（即  实际销售价格 = DiscPrice）
3：DiscPrice为 折扣减去的百分比。（范围1~100） （即 实际销售价格 = 销售价格 * （1 - DiscPrice）* 0.01）
4：DiscPrice为 折扣销售的百分比。（范围1~100） （即 实际销售价格 = 销售价格 * DiscPrice* 0.01）
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'DiscType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一月中促销生效日 （Buy_DayFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'DayFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一周中促销生效日 （Buy_WeekFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'WeekFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效月 （Buy_MonthFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'MonthFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否会员促销。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'LoyaltyFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡类型Code。（会员促销有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'LoyaltyCardTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡级别Code。（会员促销有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'LoyaltyCardGradeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否销售当日生日促销。0：不是。1：是的。（会员促销有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'BirthdayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。1：有效。0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品促销价格表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_PROMOPrice'
GO
