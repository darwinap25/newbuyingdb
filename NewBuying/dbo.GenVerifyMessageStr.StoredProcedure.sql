USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenVerifyMessageStr]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GenVerifyMessageStr]   
  @MemberID             int,           -- 会员ID
  @MemberAccountNo      varchar(512),  -- 会员账号
  @MemberAccountType    int,           -- 会员账号类型。（MessageServiceTypeID）
  @ReturnVerifyStr      varchar(1023) output,  -- 返回的验证字符串
  @LanguageAbbr		    varchar(20) output,
  @NickName             varchar(512) output,
  @password             varchar(512) output,
  @Email                varchar(512) output,
  @MemberMobilePhone    varchar(512) output,
  @MemberDateOfBirth    varchar(64) output,
  @MemberSex            varchar(64) output,
  @RecSMSAdv            varchar(64) output,
  @RecEmailAdv          varchar(64) output,
  @RecPhoneAdv          varchar(64) output,
  @CreatedOn            varchar(64) output
AS
/****************************************************************************
**  Name : GenVerifyMessageStr    
**  Version: 1.0.0.8
**  Description : 产生提交用户的验证消息（加密字符串部分）
  declare @a int, @ReturnVerifyStr varchar(1023), @LanguageAbbr varchar(20), @NickName varchar(512), @password varchar(512),
    @Email varchar(512),@MemberMobilePhone varchar(512), @MemberDateOfBirth varchar(64), @MemberSex varchar(64), @RecSMSAdv varchar(64), 
    @RecEmailAdv varchar(64), @RecPhoneAdv varchar(64), @CreatedOn varchar(64)
  set @LanguageAbbr = 'zh_CN'  
  exec @a = GenVerifyMessageStr 1029, 'nick.ye@value-exch.com', 1, @ReturnVerifyStr output, @LanguageAbbr output, @NickName output, @password output,
    @Email output,@MemberMobilePhone output, @MemberDateOfBirth output, @MemberSex output, @RecSMSAdv output, 
    @RecEmailAdv output, @RecPhoneAdv output, @CreatedOn output
  print @a
  print @ReturnVerifyStr
  print  @LanguageAbbr
  print @RecSMSAdv
  print @CreatedOn
  select * from MemberMessageAccount where memberid = 1029
  sp_helptext GenVerifyMessageStr
select * from membermessageaccount										
**  Created by: Gavin @2013-08-20
**  Modified by: Gavin @2014-07-16 (ver 1.0.0.1) 判断输入的账号是否已经验证,如果已经验证,则返回 -107
**  Modified by: Gavin @2014-11-03 (ver 1.0.0.2) 增加接口参数，用于webservice 的消息模板
                                                 返回内容做mapping.
**  Modified by: Gavin @2014-11-04 (ver 1.0.0.3) 增加返回的接口参数 @CreatedOn  
**  Modified by: Gavin @2014-11-06 (ver 1.0.0.4) 生日返回格式为: MM-DD  
**  Modified by: Gavin @2014-11-13 (ver 1.0.0.5) @CreatedOn 返回的格式为 YYYY-MM-DD hh:nn:ss   
**  Modified by: Gavin @2014-11-27 (ver 1.0.0.6) Receive/Not Receives 改为  Accept/Decline 
**  Modified by: Gavin @2014-11-27 (ver 1.0.0.7) 如果@LanguageAbbr 输入为空,则根据 MemberDefLanguage 来取  
**  Modified by: Gavin @2016-09-13 (ver 1.0.0.8) 按照Nick要求，增加未验证邮箱字段（NoVerifyEMail）。用于在GenVerifyMessageStr 时，记录要验证的邮箱。 VerifyMemberAccount 时检测这个邮箱。                                            
**
****************************************************************************/
begin
  declare @MsgStr varchar(4000), @VerifyFlag int, @Language int
  declare @gender int, @SMSAdv int, @EmailAdv int, @PhoneAdv int, @DateOfBirth datetime, @MemberDefLanguage int

  set @MemberID = isnull(@MemberID, 0)
  set @MemberAccountType = isnull(@MemberAccountType, 1)
  set @MemberAccountNo = isnull(@MemberAccountNo, '')
  if @MemberAccountNo = '' 
    select Top 1 @MemberAccountNo = AccountNumber from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = @MemberAccountType

  select @VerifyFlag = VerifyFlag from MemberMessageAccount 
    where MemberID = @MemberID and MessageServiceTypeID = @MemberAccountType and AccountNumber = @MemberAccountNo
  if @@ROWCOUNT > 0
  begin
    select @NickName = NickName, @password = MemberPassword,@Email = MemberEmail, @MemberMobilePhone = MemberMobilePhone,  
        @DateOfBirth = MemberDateOfBirth, @gender= MemberSex, @PhoneAdv = AcceptPhoneAdvertising, 
        @CreatedOn = Convert(varchar(19), CreatedOn, 120), @MemberDefLanguage = MemberDefLanguage
      from Member where MemberID = @MemberID
    select top 1 @EmailAdv = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 1  
    select top 1 @SMSAdv = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2 
    
    set @MemberDateOfBirth = substring(Convert(varchar(10), @DateOfBirth, 120), 6, 5)
 
    if isnull(@LanguageAbbr, '') = ''
    begin  
      select @LanguageAbbr = LanguageAbbr from LanguageMap where KeyID = @MemberDefLanguage
    end 
  
    if @LanguageAbbr = 'zh_CN' or @LanguageAbbr = 'zh_BigCN'
    begin
      set @LanguageAbbr = '中文'
      if @gender = 1
        set @MemberSex = '男'
      else if @gender = 2
        set @MemberSex = '女'
      else
        set @MemberSex = ''  
      if @SMSAdv = 1
        set @RecSMSAdv = '接收' 
      else 
        set @RecSMSAdv = '不接收'   
      if @EmailAdv = 1
        set @RecEmailAdv = '接收' 
      else 
        set @RecEmailAdv = '不接收'
      if @PhoneAdv = 1
        set @RecPhoneAdv = '接收' 
      else 
        set @RecPhoneAdv = '不接收'                                    
    end else
    begin
      set @LanguageAbbr = 'English'
      if @gender = 1
        set @MemberSex = ' Male'
      else if @gender = 2
        set @MemberSex = 'Female'
      else
        set @MemberSex = ''  
      if @SMSAdv = 1
        set @RecSMSAdv = 'Accept' 
      else 
        set @RecSMSAdv = 'Decline'   
      if @EmailAdv = 1
        set @RecEmailAdv = 'Accept' 
      else 
        set @RecEmailAdv = 'Decline'
      if @PhoneAdv = 1
        set @RecPhoneAdv = 'Accept' 
      else 
        set @RecPhoneAdv = 'Decline'        
    end
  end
    
  if @VerifyFlag = 1 
    return -107
  else
  begin
    -- ver 1.0.0.8
    update Member set NoVerifyEMail = @MemberAccountNo where MemberID = @MemberID
	  
    set @MsgStr = 'MemberID=' + cast(@MemberID as varchar) + ',MemberAccountType=' + cast(@MemberAccountType as varchar) 
    + ',MemberAccount=' + @MemberAccountNo + ',VerifyDate='	+ convert(varchar(10), getdate(), 120)
    exec EncryptString @MsgStr, @ReturnVerifyStr output
    return 0
  end
end

GO
