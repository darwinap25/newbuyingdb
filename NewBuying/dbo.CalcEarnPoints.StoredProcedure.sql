USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcEarnPoints]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[CalcEarnPoints]
   @CardNumber          varchar(512),  --卡号
   @ConsumeAMT          money,         --消费金额
   @EarnPoint           int output,     --获得的积分
   @PointRuleType       int = 0,
   @StoreID             int = 0
AS
/****************************************************************************
**  Name : CalcEarnPoints
**  Version: 1.0.0.5
**  Description : 计算消费时按规则应该获得的积分。（内部调用，输入的cardnumber 无加密，无UID）
**
**  Parameter :
**         @CardNumber			varchar(512),	-卡号 (CardNumber 是唯一号码, Card表主键)
**         @ConsumeAMT float   - 消费的金额
**         @EarnPoint     int output  - 获得的积分
**         return:  0 成功， -1 失败
  declare @EarnPoint int
  exec CalcEarnPoints '111111000001', 100, @EarnPoint output, 0, 374
  print @EarnPoint
  select * from PointRule
   select * from PointRule_store 
  select * from card where cardnumber = '111111000001' 
**                                         -- PointRuleOper: 0 = @, 1 = >=, 2 = <=
**  Created by: Gavin @2011-04-13
**  Modify by: Gavin @2012-07-17 (ver 1.0.0.1)
**  Modify by: Gavin @2013-08-29 (ver 1.0.0.2)  增加参数,选择哪一种计算规则. 修改PointRule表，扩展规则
**  Modify by: Gavin @2013-09-02 (ver 1.0.0.3)  修改PointRule表结构，相应修改存储过程
**  Modify by: Gavin @2013-09-02 (ver 1.0.0.4)  修改PointRule表结构，相应修改存储过程(不使用EffectiveDateType,使用新增的WeekFlagCode 等新字段)
                                                增加weekflagcode等日期控制字段, 增加店铺适用控制.   增加storeid输入
**  Modified by: Gavin @2014-12-31 (Ver 1.0.0.5) 统一startdate和enddate的比较规则：如果填写的日期不带时间（时间为0），则和 getdate（）比较时，startdate可以直接判断 < getdate， enddate 需要 >= (getdate() - 1)                                                
**
****************************************************************************/
begin 
  declare @TempPoint int, @CardTypeID int, @CardGradeID int
  declare @PointRuleOper int, @PointRuleAmount float, @PointRulePoints int
  declare @MemberBirthDate datetime, @MemberID int, @CardIssueDate Datetime 
  declare @CurDay int, @CurWeek int, @CurMonth int, @TempEarnPoint int, @CurWeekDay int
  declare @StoreRuleCode varchar(64)
		 
  set @ConsumeAMT = abs(isnull(@ConsumeAMT,0))
  set @EarnPoint = 0
  set @PointRuleType = isnull(@PointRuleType, 0)
  set @CurMonth =  datepart(mm, getdate()) 
  set @CurWeek =  datepart(wk, getdate())  
  set @CurDay = datepart(dd, getdate())  
  set @CurWeekDay = datepart(weekday, getdate()) 
  set @TempEarnPoint = 0
  set @StoreID = isnull(@StoreID, 0)
  select top 1 @StoreRuleCode = PointRuleCode from PointRule_Store 
    where StoreID = @StoreID and @StoreID <> 0
  set @StoreRuleCode = isnull(@StoreRuleCode, '')
  
  select @CardTypeID = CardTypeID, @CardGradeID = CardGradeID, @MemberID = MemberID, 
     @CardIssueDate = CardIssueDate 
  from Card where CardNumber = @CardNumber
  select @MemberBirthDate = isnull(MemberDateOfBirth,0) from Member where MemberID = @MemberID

    select top 1 isnull(PointRuleOper,0), isnull(PointRuleAmount,0), isnull(PointRulePoints,0)   
      from PointRule P  
		 left join DayFlag D on P.DayFlagCode = D.DayFlagCode 
		 left join weekflag W  on P.WeekFlagCode = W.WeekFlagCode
		 left join MonthFlag M on P.MonthFlagCode = M.MonthFlagCode 
       where StartDate <= Getdate() and EndDate >= (GetDate()-1) and CardTypeID = @CardTypeID
         and (CardGradeID = @CardGradeID or (isnull(@CardGradeID, 0) = 0))
         and PointRuleType = @PointRuleType
         and ( MemberDateType = 0 or (MemberDateType = 1 and @CurMonth = datepart(mm,@MemberBirthDate)) 
                 or (MemberDateType = 2 and @CurWeek = datepart(wk,@MemberBirthDate)) 
                 or (MemberDateType = 3 and @CurDay = datepart(dd,@MemberBirthDate)) 
                 or (MemberDateType = 4 and @CurMonth = datepart(mm,@CardIssueDate))
                 or (MemberDateType = 5 and @CurWeek = datepart(wk,@CardIssueDate))
                 or (MemberDateType = 6 and @CurDay = datepart(dd,@CardIssueDate)) 
              ) 
         and ( (@StoreRuleCode <> '' and P.PointRuleCode in (select PointRuleCode from PointRule_Store where StoreID = @StoreID and @StoreID <> 0))
               or (@StoreRuleCode = '' and P.PointRuleCode not in (select PointRuleCode from PointRule_Store))
             )
         and ( (isnull(P.DayFlagCode, '') = '') or ( (isnull(P.DayFlagCode, '') <> '') and (dbo.CheckDateFlag(P.DayFlagCode, 1, getdate(), 0) = 1) ) )
         and ( (isnull(P.WeekFlagCode, '') = '') or ( (isnull(P.WeekFlagCode, '') <> '') and (dbo.CheckDateFlag(P.WeekFlagCode, 2, getdate(), 0) = 1) ) )
         and ( (isnull(P.WeekFlagCode, '') = '') or ( (isnull(P.MonthFlagCode, '') <> '') and (dbo.CheckDateFlag(P.MonthFlagCode, 3, getdate(), 0) = 1) ) )       

  
  DECLARE CUR_CalcEarnPoint CURSOR FOR
    select top 1 isnull(PointRuleOper,0), isnull(PointRuleAmount,0), isnull(PointRulePoints,0)   
      from PointRule P  
		 left join DayFlag D on P.DayFlagCode = D.DayFlagCode 
		 left join weekflag W  on P.WeekFlagCode = W.WeekFlagCode
		 left join MonthFlag M on P.MonthFlagCode = M.MonthFlagCode 
       where StartDate <= Getdate() and EndDate >= (GetDate()-1) and CardTypeID = @CardTypeID
         and (CardGradeID = @CardGradeID or (isnull(@CardGradeID, 0) = 0))
         and PointRuleType = @PointRuleType
         and ( MemberDateType = 0 or (MemberDateType = 1 and @CurMonth = datepart(mm,@MemberBirthDate)) 
                 or (MemberDateType = 2 and @CurWeek = datepart(wk,@MemberBirthDate)) 
                 or (MemberDateType = 3 and @CurDay = datepart(dd,@MemberBirthDate)) 
                 or (MemberDateType = 4 and @CurMonth = datepart(mm,@CardIssueDate))
                 or (MemberDateType = 5 and @CurWeek = datepart(wk,@CardIssueDate))
                 or (MemberDateType = 6 and @CurDay = datepart(dd,@CardIssueDate)) 
              ) 
         and ( (@StoreRuleCode <> '' and P.PointRuleCode in (select PointRuleCode from PointRule_Store where StoreID = @StoreID and @StoreID <> 0))
               or (@StoreRuleCode = '' and P.PointRuleCode not in (select PointRuleCode from PointRule_Store))
             )
         and ( (isnull(P.DayFlagCode, '') = '') or ( (isnull(P.DayFlagCode, '') <> '') and (dbo.CheckDateFlag(P.DayFlagCode, 1, getdate(), 0) = 1) ) )
         and ( (isnull(P.WeekFlagCode, '') = '') or ( (isnull(P.WeekFlagCode, '') <> '') and (dbo.CheckDateFlag(P.WeekFlagCode, 2, getdate(), 0) = 1) ) )
         and ( (isnull(P.WeekFlagCode, '') = '') or ( (isnull(P.MonthFlagCode, '') <> '') and (dbo.CheckDateFlag(P.MonthFlagCode, 3, getdate(), 0) = 1) ) )       
--         and (EffectiveDateType = 0 or (EffectiveDateType = 1 and EffectiveDate = @CurMonth) 
--                 or (EffectiveDateType = 2 and EffectiveDate = @CurWeek) 
--                 or (EffectiveDateType = 3 and EffectiveDate = @CurDay) )
                
      order by PointRuleLevel desc  --, PointRuleSeqNo desc    
  OPEN CUR_CalcEarnPoint
  FETCH FROM CUR_CalcEarnPoint INTO @PointRuleOper, @PointRuleAmount, @PointRulePoints
  WHILE @@FETCH_STATUS=0
  BEGIN   
    set @PointRuleAmount = isnull(@PointRuleAmount, 0)  
    -- PointRuleOper: 0 = @, 1 = >=, 2 = <=
    if @PointRuleOper = 0
    begin    
      if @PointRuleAmount <> 0
      begin    
        select @TempPoint =  Round(@ConsumeAMT/@PointRuleAmount, 0, 1) 
        if @TempPoint >= 1
          set @TempEarnPoint = @TempPoint * @PointRulePoints
      end else
        set @TempEarnPoint = 0     
    end
    if @PointRuleOper = 1
    begin
      if @ConsumeAMT >= @PointRuleAmount
        set @TempEarnPoint = @PointRulePoints
    end
    if @PointRuleOper = 2
    begin
      if @ConsumeAMT <= @PointRuleAmount
        set @TempEarnPoint = @PointRulePoints
    end  
    
    set @EarnPoint = @TempEarnPoint + @EarnPoint
    FETCH FROM CUR_CalcEarnPoint INTO @PointRuleOper, @PointRuleAmount, @PointRulePoints
  END
  CLOSE CUR_CalcEarnPoint 
  DEALLOCATE CUR_CalcEarnPoint 
           
  return 0
end

GO
