USE [NewBuying]
GO
/****** Object:  Table [dbo].[POSSTAFF]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POSSTAFF](
	[StaffID] [int] IDENTITY(1,1) NOT NULL,
	[StaffCode] [varchar](64) NOT NULL,
	[StaffName] [varchar](64) NULL,
	[StaffPWD] [varchar](64) NULL,
	[StaffLevel] [int] NULL DEFAULT ((0)),
	[Status] [int] NULL DEFAULT ((1)),
	[LanguageID] [int] NULL,
	[Def_Reset_PWD_DAYS] [int] NULL DEFAULT ((0)),
	[Grace_Login_Count] [int] NULL DEFAULT ((0)),
	[PWD_Valid_Days] [int] NULL DEFAULT ((0)),
	[Last_Reset_PWD] [datetime] NULL DEFAULT (getdate()),
	[LastLoginTime] [datetime] NULL,
	[LastLoginStore] [varchar](64) NULL,
	[LastLoginRegister] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_POSSTAFF] PRIMARY KEY CLUSTERED 
(
	[StaffID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'StaffID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'StaffCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'StaffName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'StaffPWD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工级别。数字转换为二进制数。 一共3位。 第一位管理员权限。 第二位促销员权限。第三位收银员权限
例如：
001---  1： 收银员
010---  2： 促销员
100---  4： 管理员
101...  5:    收银员 + 管理员
......
111...  7:   收银员 + 管理员 + 促销员

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'StaffLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态. 0: 无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'默认语言ID。应该同LanguageMap表中的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'LanguageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'默认允许超过reset pwd 的天数。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'Def_Reset_PWD_DAYS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'login允许连续失败次数。默认0，表示不设限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'Grace_Login_Count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码有效天数。 默认0，表示永久有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'PWD_Valid_Days'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后更新密码的日期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'Last_Reset_PWD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后登录的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'LastLoginTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后登录的StoreCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'LastLoginStore'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后登录的RegisterCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF', @level2type=N'COLUMN',@level2name=N'LastLoginRegister'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工表。（POS登录用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSSTAFF'
GO
