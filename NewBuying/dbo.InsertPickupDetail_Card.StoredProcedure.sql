USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[InsertPickupDetail_Card]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[InsertPickupDetail_Card]
  @CardPickingNumber      varchar(64),     --单号
  @CardTypeID             int,             --卡类型ID
  @CardGradeID             int,             --卡等级ID  
  @StartCardNumber         varchar(64),      --起始号码
  @OrderFormQty             int,             --Order单的数量。
  @OrderQty                 int,             --要求的数量。
  @IsEarchCard            int=0            -- 1: 输入每个卡，即一个卡一行。 0：连续卡合并。 默认0
AS
/****************************************************************************
**  Name : InsertPickupDetail_Card
**  Version: 1.0.0.0
**  Description : 根据 InsertPickupDetail_Coupon （ver 1.0.0.9） 改写
  declare @a int  , @CouponOrderFormNumber varchar(64)
  set @CouponOrderFormNumber = ''
  exec @a = CheckPickupDetail_Coupon 1, @CouponOrderFormNumber, 1, 2, 10, 0, '', ''   
  print @a  

**  Created by: Gavin @2014-10-08
**  (取消) Modify by: Gavin @2015-04-30 (ver 1.0.0.1) 功能扩展，增加金额和积分。增加输入参数（@OrderFormAmt，@OrderAmt， @OrderFormPoint，@OrderPoint）
**
****************************************************************************/
begin
  declare @KeyID int, @PickQty int, @FirstNumber varchar(64), @EndNumber varchar(64)
  declare @StartCard varchar(64), @EndCard varchar(64), @InvalidCard varchar(64), @CardStatus int  
  declare @i int, @EndCardNumber varchar(64), @PrevCardNumber varchar(64), @CardNumber varchar(64)
  declare @CardNumMask nvarchar(30), @CardNumPattern nvarchar(30), @PatternLen int, @Checkdigit int, @CardSeqNo bigint
  declare @IsImportCardNumber int, @IsConsecutiveUID int, @BatchCardCode varchar(64), @StartBatchCardCode varchar(64)
  declare @HasPickup int, @ModeID int, @ModeCode varchar(64), @CardStockStatus int
  set @HasPickup = 0
    
  select @CardStatus = status from Card
     where CardTypeID = @CardTypeID  and CardGradeID = @CardGradeID and CardNumber = @StartCardNumber 
         and PickupFlag = 0 and Status = 0 and StockStatus = 2
  -- 判断首coupon状态是否正确。
  if isnull(@CardStatus, -1) <> 0
    return -1                         
  set @StartCard = isnull(@StartCardNumber, '')
 
  if @IsEarchCard = 1
  begin
  select * from Ord_CardPicking_D
    insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, 
           FirstCardNumber, EndCardNumber, BatchCardCode)        --- , OrderAmount, PickAmount, OrderPoint, PickPoint
    select CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, 
           FirstCardNumber, EndCardNumber, BatchCardCode
    from (           
          select ROW_NUMBER() OVER(order by C.CardNumber) as IID, @CardPickingNumber as CardPickingNumber , 
                @CardTypeID as CardTypeID, @CardGradeID as CardGradeID, '' as [Description], 
                1 as OrderQty, 1 as PickQty, C.CardNumber as FirstCardNumber, C.CardNumber as EndCardNumber, B.BatchCardCode 
             from Card C left join BatchCard B on C.BatchCardID = B.BatchCardID
            where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CardTypeID = @CardTypeID 
                  and C.CardGradeID = @CardGradeID and CardNumber >= @StartCard 
        ) A
     where A.IID <= @OrderQty 
    order by IID 
      
    return 0
  end
  else
  begin 
      -- 获得CardType的设置
      select @CardNumMask = case when isnull(G.CardNumMask, '') = '' then T.CardNumMask else G.CardNumMask end, 
         @CardNumPattern = case when isnull(G.CardNumPattern, '') = '' then T.CardNumPattern else G.CardNumPattern end,
         @Checkdigit =  case when isnull(G.CardCheckdigit, -1) = -1 then T.CardCheckdigit else G.CardCheckdigit end,
         @ModeID = case when isnull(G.CheckDigitModeID, -1) = -1 then T.CheckDigitModeID else G.CheckDigitModeID end
        from Card C 
        Left join CardType T on C.CardTypeID = T.CardTypeID 
        left join CardGrade G on C.CardGradeID = G.CardGradeID          
      select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
      set @ModeCode = isnull(@ModeCode, 'EAN13')
          
    set @PatternLen = 0
    while @PatternLen < Len(@CardNumMask)
    begin      
        if substring(@CardNumMask, @PatternLen + 1, 1) = 'A'
          set @PatternLen = @PatternLen + 1
        else  
          break  
    end
      
      set @i = 1
      set @PickQty = 0
      set @CardNumber = ''
      set @StartCardNumber = ''
      set @EndCardNumber = ''
      set @PrevCardNumber = ''
      DECLARE CUR_Card CURSOR fast_forward for
        select C.CardNumber, B.BatchCardCode from Card C left join BatchCard B on C.BatchCardID = B.BatchCardID
          where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CardTypeID = @CardTypeID and C.CardGradeID = @CardGradeID and CardNumber >= @StartCard
          order by C.CardNumber	    
	    OPEN CUR_Card
      FETCH FROM CUR_Card INTO @CardNumber, @BatchCardCode
      WHILE @@FETCH_STATUS=0 and @i <= @OrderQty
      BEGIN            
        if @StartCardNumber = ''
        begin
          set @StartCardNumber = @CardNumber
          set @StartBatchCardCode = @BatchCardCode
          set @EndCardNumber = @CardNumber
        end
        if isnull(@PrevCardNumber, '') <> ''
        begin       
          -- 判断这个cardnumber 是否在pickup单中
          if not exists(select * from Ord_CardPicking_D 
            where CardPickingNumber = @CardPickingNumber and CardTypeID = @CardTypeID 
              and FirstCardNumber <= @CardNumber and @CardNumber <= EndCardNumber)
          begin 
            if isnull(@IsImportCardNumber, 0) = 0
            begin
              -- 获得上一个cardnumber的序号。
              if @Checkdigit = 1 
                set @CardSeqNo = substring(@PrevCardNumber, @PatternLen + 1,  Len(@CardNumMask) - @PatternLen - 1) 
              else  
                set @CardSeqNo = substring(@PrevCardNumber, @PatternLen + 1,  Len(@CardNumMask) - @PatternLen)      
              -- 计算从上一个序号开始的连续序号和卡号
              set @CardSeqNo = @CardSeqNo + 1
              if @Checkdigit = 1 
              begin
                set @EndCardNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CardSeqNo), Len(@CardNumMask) - @PatternLen - 1)        
                select @EndCardNumber = dbo.CalcCheckDigit(@EndCardNumber, @ModeCode)
              end else        
                set @EndCardNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CardSeqNo), Len(@CardNumMask) - @PatternLen)
            end else  
            begin
              if isnull(@IsConsecutiveUID, 1) = 1
              begin
                if @Checkdigit = 1
                begin 
                  set @EndCardNumber =  cast((cast(substring(@PrevCardNumber,1,len(@PrevCardNumber)-1) as bigint) + 1) as varchar)
                  select @EndCardNumber = dbo.CalcCheckDigit(@EndCardNumber, @ModeCode)
                end else  
                  set @EndCardNumber =  cast((cast(@PrevCardNumber as bigint) + 1) as varchar)
              end else
                set @EndCardNumber =  ''  
            end
            -- 比较根据上一个序号计算出的号码是否和当前号码相同，如果相同则表示连续，如果不同，则表示不连续，需要另起一条记录。（上一条为空不包括）   
            if (@EndCardNumber <> @CardNumber) and (isnull(@PrevCardNumber,'') <> '')
            begin              
	            insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, FirstCardNumber, EndCardNumber, BatchCardCode)
  	            values(@CardPickingNumber, @CardTypeID, @CardGradeID, '', @OrderFormQty, @PickQty, @StartCardNumber, @PrevCardNumber, @StartBatchCardCode) 	             
  	          set @HasPickup = 1
	            set @PickQty = 0 
              set @StartCardNumber = @CardNumber
              set @StartBatchCardCode = @BatchCardCode
            end
            set @EndCardNumber = @CardNumber
 	          set @PickQty = @PickQty + 1	  
	          set @i = @i + 1	        	              
	        end else
	        begin
            if @PickQty > 0
            begin
	           insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, FirstCardNumber, EndCardNumber, BatchCardCode)
  	          values(@CardPickingNumber, @CardTypeID, @CardGradeID, '', @OrderFormQty, @PickQty, @StartCardNumber, @PrevCardNumber, @StartBatchCardCode) 	             
	          set @PickQty = 0   
	          set @StartCardNumber = ''
	          set @HasPickup = 1
	        end  	      
	      end
	      end
	      if isnull(@PrevCardNumber, '') = ''   -- 需要计算第一个号码
	      begin
 	        set @PickQty = @PickQty + 1	  
	        set @i = @i + 1	 	    
	      end
	      set @PrevCardNumber = @CardNumber 
	      FETCH FROM CUR_Card INTO @CardNumber, @BatchCardCode	  
	    END
      CLOSE CUR_Card
      DEALLOCATE CUR_Card 	

      if @PickQty > 0
      begin
        insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, FirstCardNumber, EndCardNumber, BatchCardCode)
	      values(@CardPickingNumber, @CardTypeID, @CardGradeID, '', @OrderFormQty, @PickQty, @StartCardNumber, @EndCardNumber, @StartBatchCardCode) 	
	      set @PickQty = 0   
	      set @HasPickup = 1
	    end  

    if @HasPickup = 1
      return 0
    else
      return 1  
  end  
end

GO
