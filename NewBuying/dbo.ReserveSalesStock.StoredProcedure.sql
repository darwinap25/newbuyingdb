USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ReserveSalesStock]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[ReserveSalesStock]
  @TxnNo                         VARCHAR(64)        -- 交易号码
AS
/****************************************************************************
**  Name : ReserveSalesStock (bauhaus)
**  Version: 1.0.1.0
**  Description : 预扣库存,  WH 仓库是 hardcode 的。  onlineshopping 应该是 ‘ECO’ store， 如果不是，则使用onlineshopping中指定的store
select * from Ord_SalesPickOrder_H
select * from sales_D
update Ord_SalesPickOrder_D set actualqty = 4
select * from sales_H where status = 4
exec GenSalesPickupOrder 1,'KTXNNO000001028' 
**  Created by: Gavin @2016-10-18 : 这个ReserveSalesStock目前只有POSSalesSubmit使用,为减少update sales_D次数,可以提交的collected直接是1， 那时只取collected=1的记录。
**  Modify by: Gavin @2016-10-20 (1.0.0.1) (暂时不用)增加库存数量判断，如果数量不足则不能预扣，并且返回 -1
**  Modify by: Gavin @2017-07-13 (1.0.1.0) 传入的sales单中可能有负数的货品。所以，需要过滤负数货品
**
****************************************************************************/
BEGIN
  DECLARE @StoreCode VARCHAR(64), @PickupLocation VARCHAR(64), @StockTypeCode VARCHAR(64), @ProdCode VARCHAR(64), @TotalQty INT,
          @PickupStoreCode VARCHAR(64), @StoreOnhandQty INT, @WHOnhandQty INT, @StoreID INT, @WHStoreID INT,
		  @BusDate DATETIME, @TxnDate DATETIME, @SerialNoType INT, @SerialNo VARCHAR(64), @CreatedBy INT, @SeqNo VARCHAR(64)
  DECLARE @ReserveQty INT, @WHReserveQty INT

  SELECT @WHStoreID = StoreID FROM BUY_STORE WHERE StoreCode = 'WH'

  BEGIN TRAN

	  DECLARE CUR_ReserveSalesStock CURSOR fast_forward FOR
		  SELECT  D.StoreCode, D.PickupLocation, D.StockTypeCode, D.ProdCode, D.TotalQty, O.OnhandQty, W.OnhandQty, S.StoreID, 
			  D.BusDate,D.TxnDate,D.SerialNoType,D.SerialNo, D.Createdby, D.SeqNo 
			FROM Sales_D D         -- D.PickupLocation 目前没使用 
			LEFT JOIN BUY_STORE S ON D.StoreCode = S.StoreCode
			LEFT JOIN BUY_Product P ON D.ProdCode = P.ProdCode
			LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G') O ON D.ProdCode = O.ProdCode AND O.StoreID = S.StoreID                   -- StoreCode 应该是 ECO
			LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = (SELECT StoreID FROM BUY_STORE WHERE StoreCode = 'WH')) W ON D.ProdCode = W.ProdCode
		  WHERE D.TransNum = @TxnNo AND D.Collected = 0 AND P.ProductType = 0 AND ISNULL(D.TotalQty, 0) > 0   -- 目前按正常流程走  (可能没onhand数据，目前允许没库存数量，库存数可以是负数)
	  OPEN CUR_ReserveSalesStock
	  FETCH FROM CUR_ReserveSalesStock INTO @StoreCode, @PickupLocation, @StockTypeCode, @ProdCode, @TotalQty, @StoreOnhandQty, @WHOnhandQty, @StoreID, 
											@BusDate, @TxnDate,@SerialNoType,@SerialNo,@CreatedBy,@SeqNo
	  WHILE @@FETCH_STATUS=0
	  BEGIN
/*	    
		IF (@StoreOnhandQty + @WHOnhandQty) < @TotalQty
		BEGIN
		  ROLLBACK
		  RETURN -1
		END
*/	    
		SELECT @ReserveQty = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'R' AND ProdCode = @ProdCode AND StoreID = @StoreID
		SELECT @WHReserveQty = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'R' AND ProdCode = @ProdCode AND StoreID = @WHStoreID
    
		SET @StockTypeCode = 'G'
		SET @StoreOnhandQty = ISNULL(@StoreOnhandQty, 0)
		SET @WHOnhandQty = ISNULL(@WHOnhandQty, 0)
		SET @ReserveQty = ISNULL(@ReserveQty, 0)
		SET @WHReserveQty = ISNULL(@WHReserveQty, 0)

		-- insert 时使用values，即使STK_StockOnhand中没有数据，也可以插入movement记录。
		IF @StoreOnhandQty >= @TotalQty
		BEGIN
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
		  VALUES (1, @StoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				 @StoreOnhandQty, - @TotalQty, @StoreOnhandQty - @TotalQty, @SerialNoType, @SerialNo, '', GETDATE(), @Createdby )	   
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
		  VALUES (1, @StoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				 @ReserveQty, @TotalQty, @ReserveQty + @TotalQty, @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 
	  
		  INSERT INTO SalesReserverDetail(TransNum, ProdCode, SeqNo, StoreCode, StockTypeCode, Qty, CreatedOn)
		  VALUES(@TxnNo, @ProdCode, @SeqNo, @StoreCode, 'R', @TotalQty, GETDATE())		  
		END ELSE 
		BEGIN 
		  IF @StoreOnhandQty > 0 
		  BEGIN
			  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
			  VALUES (1, @StoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
				  @StoreOnhandQty, - @StoreOnhandQty, 0, @SerialNoType, @SerialNo, '', GETDATE(), @Createdby )	   	   
			  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
			  VALUES (1, @StoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
					 @WHReserveQty, @StoreOnhandQty, @WHReserveQty + @StoreOnhandQty, @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 
	  
			  INSERT INTO SalesReserverDetail(TransNum, ProdCode, SeqNo, StoreCode, StockTypeCode, Qty, CreatedOn)
			  VALUES(@TxnNo, @ProdCode, @SeqNo, @StoreCode, 'R', @StoreOnhandQty, GETDATE())	
		  

			  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
			  VALUES (1, @WHStoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
					 @WHOnhandQty, - (@TotalQty - @StoreOnhandQty), @WHOnhandQty - (@TotalQty - @StoreOnhandQty), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby) 	   
			  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
			  VALUES (1, @WHStoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
					 @WHReserveQty, (@TotalQty - @StoreOnhandQty), @WHReserveQty + (@TotalQty - @StoreOnhandQty), @SerialNoType, @SerialNo, '', GETDATE(), @Createdby)
	  
			  INSERT INTO SalesReserverDetail(TransNum, ProdCode, SeqNo, StoreCode, StockTypeCode, Qty, CreatedOn)
			  VALUES(@TxnNo, @ProdCode, @SeqNo, 'WH', 'R', (@TotalQty - @StoreOnhandQty), GETDATE())			  		  	    
		  END ELSE
		  BEGIN
			  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
			  VALUES (1, @WHStoreID, 'G', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
					 @WHOnhandQty, - @TotalQty, @WHOnhandQty - @TotalQty, @SerialNoType, @SerialNo, '', GETDATE(), @Createdby)
	   
			  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
			  VALUES (1, @WHStoreID, 'R', @ProdCode, @TxnNo, '', @Busdate, @Txndate,
					 @WHReserveQty, @TotalQty, @WHReserveQty + @TotalQty, @SerialNoType, @SerialNo, '', GETDATE(), @Createdby)

			  INSERT INTO SalesReserverDetail(TransNum, ProdCode, SeqNo, StoreCode, StockTypeCode, Qty, CreatedOn)
			  VALUES(@TxnNo, @ProdCode, @SeqNo, 'WH', 'R', @TotalQty, GETDATE())	  
		  END  
		END
		FETCH FROM CUR_ReserveSalesStock INTO @StoreCode, @PickupLocation, @StockTypeCode, @ProdCode, @TotalQty, @StoreOnhandQty, @WHOnhandQty, @StoreID, 
											  @BusDate, @TxnDate,@SerialNoType,@SerialNo,@CreatedBy,@SeqNo
	  END
	  CLOSE CUR_ReserveSalesStock 
	  DEALLOCATE CUR_ReserveSalesStock

	  UPDATE SALES_D SET Collected = 1 WHERE TransNum = @TxnNo AND Collected = 0

  COMMIT TRAN

  RETURN 0
END

GO
