USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoGenCouponOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[AutoGenCouponOrder]
  @UserID               int,               -- 操作用户ID
  @CouponReplenishCode  varchar(1000) = '',  -- 自动补货规则表Code, 可能会给多个，用 ,  隔开
  @ReferenceNo          varchar(64) = '',  -- 相关单号。（如果有的话）
  @BusDate              datetime = null,   -- 交易日
  @TxnDate              datetime = null,    -- 当前交易时间
  @IsSchedule           int=0              -- 是否 日程安排 自动执行. 0:不是. 1: 是的
AS
/****************************************************************************
**  Name : AutoGenCouponOrder
**  Version: 1.0.0.17
**  Description : 自动产生产生Coupon的订单（包括给供应商和总部的）.（从规则表中读取设置。） 
**
**  Parameter :	 select * from card_movement
  declare @a int  
  exec @a = AutoGenCouponOrder 1,  'ddddd,ccccc,bbbbb,'
  print @a  
  
exec AutoGenCouponOrder @UserID=1,@CouponReplenishCode='ddddd,ccccc,bbbbb,',@ReferenceNo=NULL,@BusDate=NULL,@TxnDate=NULL
  
select * from CouponReplenishRule_H
**  Created by: Gavin @2014-03-21
**  Modified by: Gavin @2014-06-04 (ver 1.0.0.1) Ord_OrderToSupplier_H表结构变更，coupontypeid和qty放在头表，一个订单只允许一种Coupontype
**  Modified by: Gavin @2014-06-21 (ver 1.0.0.2) 增加参数@IsSchedule, 如果是自动执行，那么需要增加检查有效时间的设置。
                                                 店铺向总部的订单,直接产生时自动批核
**  Modified by: Gavin @2014-06-26 (ver 1.0.0.3) 修改了PackageQty的规则，直接取Replenish Rule中的RoundUpQty 捆绑数量 （箱数）   
**  Modified by: Gavin @2014-07-09 (ver 1.0.0.4) 店铺向总部的订单,直接产生时自动批核, 但是订单批核后,不再产生picking单  
**  Modified by: Gavin @2014-07-13 (ver 1.0.0.5) 修正Coupon_Onhand 取数逻辑，加上Group  
**  Modified by: Gavin @2014-07-17 (ver 1.0.0.6) 店铺向总部的订单, 取数逻辑:  max - damaged - issued-in location = order qty
**  Modified by: Gavin @2014-07-25 (ver 1.0.0.7) 使用Ord_OrderToSupplier_D中的CouponTypeID,一个订单允许多个CouponType
**  Modified by: Gavin @2014-08-12 (ver 1.0.0.8) 店铺向总部的订单，自动批核。
**  Modified by: Gavin @2014-08-13 (ver 1.0.0.9) 给供应商订单时，取出规则表中最大OrderRoundUpQty，填入H表的 PackageQty
**  Modified by: Gavin @2014-08-28 (ver 1.0.0.10) Ord_OrderToSupplier_D表增加字段：OrderRoundUpQty 
**  Modified by: Gavin @2014-08-29 (ver 1.0.0.11) test
**  Modified by: Gavin @2014-09-28 (ver 1.0.0.12) 规则表改名为:InventoryReplenishRule_H, InventoryReplenishRule_D
**  Modified by: Gavin @2014-11-17 (ver 1.0.0.13)  从group by 子句中去掉 brandid,Priority
**  Modified by: Gavin @2014-11-21 (ver 1.0.0.14) 写入orderdetail时, 数量判断条件 从 onhand<Min 改为onhand<=Min
**  Modified by: Gavin @2014-12-18 (ver 1.0.0.15) @OrderRoundUpQty 变量值需要根据coupontypeid 来读取
**  Modified by: Gavin @2015-04-07 (ver 1.0.0.16) 增加PurchaseType 条件
**  Modified by: Gavin @2015-05-06 (ver 1.0.0.17) 订单表中的数据太大,会导致执行时间过长, 采用预先读入临时表 的方式解决
**
****************************************************************************/
begin
  declare @SupplierID int, @NewPRNumber varchar(64), @OrderTargetID int, @OrderStoreTypeID int, @OrderStoreID int
  declare @StoreAddress varchar(512), @StoreContact varchar(512), @StoreContactPhone varchar(512),
          @StoreEmail varchar(512)
  declare @SourceAddress varchar(512), @SourceContact varchar(512), @SourceContactPhone varchar(512),
          @SourceEmail varchar(512)
  declare @BrandID int, @CouponTypeID int, @OrderQty int, @CompanyID int 
  declare @OnhandQty int, @MinStockQty int, @RecordCount int, @PackageQty int
  declare @ApprovalCode varchar(64), @OrderRoundUpQty int
  declare @StoreBrandID int
 
  -- ver 1.0.0.17
  declare @supplierorder table(CouponTypeID int, SupplierQty int, StoreID int)
  declare @StoreOrderPick table(CouponTypeID int, StoreOrderPickQty int, StoreID int)
  declare @StoreOrder table(CouponTypeID int, StoreOrderQty int, StoreID int)
  declare @StoreDelivery table(CouponTypeID int, StoreDeliveryQty int, StoreID int)
  
  insert into @supplierorder
  select D.CouponTypeID, sum(D.OrderQty) as SupplierQty, H.StoreID from Ord_OrderToSupplier_D D left join Ord_OrderToSupplier_H H on D.OrderSupplierNumber = H.OrderSupplierNumber where approvestatus = 'P' group by D.CouponTypeID, H.StoreID
  insert into @StoreOrderPick
  select D.CouponTypeID, sum(D.OrderQty) as StoreOrderPickQty, H.StoreID from ord_couponpicking_D D left join ord_couponpicking_H H on D.CouponPickingNumber = H.CouponPickingNumber where H.approvestatus in ('R','P') group by D.CouponTypeID, H.StoreID
  insert into @StoreOrder
  select D.CouponTypeID, sum(D.CouponQty) as StoreOrderQty, H.StoreID from ord_couponOrderForm_D D left join ord_couponOrderForm_H H on D.CouponOrderFormNumber = H.CouponOrderFormNumber where H.approvestatus in ('A') group by D.CouponTypeID, H.StoreID
  insert into @StoreDelivery
  select D.CouponTypeID, sum(D.OrderQty) as StoreDeliveryQty, H.StoreID from ord_couponDelivery_D D left join ord_couponDelivery_H H on D.CouponDeliveryNumber = H.CouponDeliveryNumber where H.approvestatus in ('P') group by D.CouponTypeID, H.StoreID
  -----------   
   
  set @RecordCount = 0 
  select Top 1 @CompanyID = CompanyID from Company Order by IsDefault desc
  set @CouponReplenishCode = isnull(@CouponReplenishCode, '')          
  if isnull(@BusDate, 0) = 0
  Begin
    select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
    set @BusDate=isnull(@BusDate,getdate())
  end
  if isnull(@TxnDate, 0) = 0
    set @TxnDate=getdate()
  									   
  DECLARE CUR_GenOrderToSupplier CURSOR fast_forward FOR
    select D.OrderTargetID, H.StoreTypeID, D.StoreID, max(S.StoreFullDetail),  max(S.Contact),   
       max(S.ContactPhone), max(S.Email)
     from InventoryReplenishRule_D D 
      left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
      left join (select CouponTypeID, StoreID, SUM(OnhandQty) as OnhandQty from Coupon_Onhand where CouponStockStatus in (1,2,6) group by CouponTypeID, StoreID) O on H.CouponTypeID = O.CouponTypeID and D.StoreID = O.StoreID
      left join Store S on D.StoreID = S.StoreID
      left join CouponType T on H.CouponTypeID = T.CouponTypeID  
      left join @supplierorder U on H.CouponTypeID = U.CouponTypeID and D.StoreID = U.StoreID
      left join @StoreOrderPick I on H.CouponTypeID = I.CouponTypeID and D.StoreID = I.StoreID 
      left join @StoreOrder J on H.CouponTypeID = J.CouponTypeID and D.StoreID = J.StoreID
      left join @StoreDelivery K on H.CouponTypeID = K.CouponTypeID and D.StoreID = K.StoreID                   
     where isnull(O.OnhandQty,0)+ (case H.StoreTypeID when 1 then isnull(U.SupplierQty,0) else isnull(I.StoreOrderPickQty,0) + isnull(J.StoreOrderQty,0)+ isnull(K.StoreDeliveryQty,0) end) <= isnull(D.MinStockQty,0) 
       and (@CouponReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @CouponReplenishCode) > 0)
       and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1
      and ( (@IsSchedule = 0) or (
             ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
         and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
         and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		    )        
      )
      and H.MediaType = 1 and H.PurchaseType = 1    
    group by D.OrderTargetID, H.StoreTypeID, D.StoreID--, H.BrandID, D.Priority
   -- order by D.Priority
  OPEN CUR_GenOrderToSupplier
  FETCH FROM CUR_GenOrderToSupplier INTO @OrderTargetID, @OrderStoreTypeID, @OrderStoreID, @StoreAddress, 
        @StoreContact, @StoreContactPhone, @StoreEmail
  WHILE @@FETCH_STATUS=0
  BEGIN
    set @RecordCount = @RecordCount + 1 
    if @OrderStoreTypeID = 2    -- 店铺向总部的订单.    
    begin
      select @StoreBrandID = BrandID from Store where StoreID = @OrderStoreID     
      exec GenApprovalCode @ApprovalCode output 
      exec GetRefNoString 'PR', @NewPRNumber output
      insert into Ord_CouponOrderForm_H
          (CouponOrderFormNumber, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod, SendAddress, FromAddress, 
          FromContactName, FromContactNumber, FromEmail, FromMobile, StoreContactName, StoreContactPhone,
          StoreContactEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, OrderType, ApprovalCode, ApproveOn)
      select @NewPRNumber, @StoreBrandID, @OrderTargetID, @OrderStoreID, 2, 0, 5, @StoreAddress, StoreFullDetail,
          Contact, ContactPhone, Email, '', @StoreContact, @StoreContactPhone,
          @StoreEmail, '', 'A', 'Auto Create order to HQ', 
          @BusDate, getdate(), @UserID, getdate(), @UserID, 1, @ApprovalCode, GETDATE()
       from Store where StoreID = @OrderTargetID  
             
      insert into Ord_CouponOrderForm_D (CouponOrderFormNumber, CouponTypeID, CouponQty) 
      select @NewPRNumber, A.CouponTypeID, 
        case (A.RunningStockQty-A.OnhandQty) % A.OrderRoundUpQty when 0 then (A.RunningStockQty-A.OnhandQty) else ((A.RunningStockQty-A.OnhandQty) / A.OrderRoundUpQty + 1) * A.OrderRoundUpQty end as OrderQty
      from 
      ( select H.CouponTypeID, (max(isnull(O.OnhandQty,0)) + max(isnull(I.StoreOrderPickQty,0)) + max(isnull(J.StoreOrderQty,0))+ max(isnull(K.StoreDeliveryQty,0))) as OnhandQty, max(isnull(D.MinStockQty,0)) as MinStockQty, 
              max(isnull(D.RunningStockQty,0)) as RunningStockQty, max(isnull(D.OrderRoundUpQty,0)) as OrderRoundUpQty --, D.Priority 
           from InventoryReplenishRule_D D 
           left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
           left join (select CouponTypeID, StoreID, SUM(OnhandQty) as OnhandQty from Coupon_Onhand where CouponStockStatus in (1,2,6) group by CouponTypeID, StoreID) O on H.CouponTypeID = O.CouponTypeID and D.StoreID = O.StoreID
           left join @StoreOrderPick I on H.CouponTypeID = I.CouponTypeID and D.StoreID = I.StoreID         
           left join @StoreOrder J on H.CouponTypeID = J.CouponTypeID and D.StoreID = J.StoreID                   
           left join @StoreDelivery K on H.CouponTypeID = K.CouponTypeID and D.StoreID = K.StoreID          
          where D.OrderTargetID = @OrderTargetID and D.StoreID = @OrderStoreID  
            and (@CouponReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @CouponReplenishCode) > 0)
            and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1            
            and ( (@IsSchedule = 0) or (
                   ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
               and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
               and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		           )        
              ) 
            and H.MediaType = 1 and H.PurchaseType = 1                  
         group by H.CouponTypeID--, D.Priority 
      ) A
      where OnhandQty <= MinStockQty 
      --order by A.Priority 
      
      -- 不在触发器中调用,直接手动调用
      exec ChangeCouponStockStatus 3, @NewPRNumber, 1
                     
    end 
    
    if @OrderStoreTypeID = 1   -- 总部向供应商的订单.
    begin         
      exec GetRefNoString 'PR', @NewPRNumber output
      insert into Ord_OrderToSupplier_H
          (OrderSupplierNumber, SupplierID, OrderSupplierDesc, ReferenceNo, SendMethod, SendAddress,SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreID, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark, IsProvideNumber, OrderType,  
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponTypeID, CouponQty, CompanyID, PackageQty)
      select @NewPRNumber, @OrderTargetID, 'Auto Create order to supplier', @ReferenceNo, 5, @StoreAddress, Supplieraddress,
		  Contact, ContactPhone, ContactEmail, ContactMobile, @OrderStoreID, @StoreContact, @StoreContactPhone, 
		  @StoreEmail, '', 'P', '', 0, 1,
		  @BusDate, getdate(), @UserID, getdate(), @UserID, 0, 0, @CompanyID, 0
        from Supplier where SupplierID = @OrderTargetID     

      -- 产生供应商订单的明细记录。其中没有具体的Number，需要批核后才会产生。批核后会删除此记录，扩展为一个Number一条记录。
      insert into Ord_OrderToSupplier_D (OrderSupplierNumber, CouponTypeID, OrderQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode, PackageQty, OrderRoundUpQty)
      select B.OrderSupplierNumber, B.CouponTypeID, B.OrderQty, '', '', '', B.OrderQty / B.OrderRoundUpQty, B.OrderRoundUpQty
      from
      (
        select @NewPRNumber as OrderSupplierNumber, A.CouponTypeID as CouponTypeID, A.OrderRoundUpQty,
          case (A.RunningStockQty-A.OnhandQty) % A.OrderRoundUpQty when 0 then (A.RunningStockQty-A.OnhandQty) else ((A.RunningStockQty-A.OnhandQty) / A.OrderRoundUpQty + 1) * A.OrderRoundUpQty end as OrderQty        
        from 
        ( select H.CouponTypeID, (max(isnull(O.OnhandQty,0)) + max(isnull(I.SupplierQty,0))) as OnhandQty, max(isnull(D.MinStockQty,0)) as MinStockQty, 
                max(isnull(D.RunningStockQty,0)) as RunningStockQty, max(isnull(D.OrderRoundUpQty,0)) as OrderRoundUpQty--, D.Priority 
             from InventoryReplenishRule_D D 
             left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
             left join (select CouponTypeID, StoreID, SUM(OnhandQty) as OnhandQty from Coupon_Onhand where CouponStockStatus in (1,2,6) group by CouponTypeID, StoreID) O on H.CouponTypeID = O.CouponTypeID and D.StoreID = O.StoreID
             left join @supplierorder I on H.CouponTypeID = I.CouponTypeID and D.StoreID = I.StoreID 
            where D.OrderTargetID = @OrderTargetID and D.StoreID = @OrderStoreID  
              and (@CouponReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @CouponReplenishCode) > 0)
              and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1            
              and ( (@IsSchedule = 0) or (
                     ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
                 and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
                 and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		             )        
                )     
            and H.MediaType = 1 and H.PurchaseType = 1             
           group by H.CouponTypeID--, D.Priority 
        ) A 
        where OnhandQty <= MinStockQty       
      ) B 
     -- order by B.Priority                    
    end  
    
    FETCH FROM CUR_GenOrderToSupplier INTO @OrderTargetID, @OrderStoreTypeID, @OrderStoreID, @StoreAddress, 
          @StoreContact, @StoreContactPhone, @StoreEmail
  END
  CLOSE CUR_GenOrderToSupplier 
  DEALLOCATE CUR_GenOrderToSupplier    
 
  return 0
end

GO
