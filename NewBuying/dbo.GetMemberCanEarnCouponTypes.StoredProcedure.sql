USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCanEarnCouponTypes]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberCanEarnCouponTypes]
  @MemberID				int,       -- 会员ID
  @CardNumber			varchar(512),      -- 如填写CardNumber，忽略MemberID	
  @StoreID			    int,	     -- 登录店铺
  @RegisterCode         varchar(512),       -- 终端号码
  @ServerCode           varchar(512)='',	   -- serverid 暂时用不到,预留
  @BrandID 			    int,       -- coupontype 的brandID\\
  @CampaignID			int,               -- Coupon 分类 (CampaignID) 
  @ExchangeType         int,               -- null/0: 不做限制。 1、金额兑换。2、积分兑换。3、金额+积分兑换. 4、coupon兑换
  @CouponTypeNatureID   int,       -- CouponTypeNature 的分类.
  @DeviceType			int=0,					-- NULL或0:全部  1:kiosk  2: mobile  3:PC   
  @ConditionStr			nvarchar(1000),     -- 查询条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=1 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)='',			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1
  @iBeaconGroupID   int = 0        -- 蓝牙定位使用的  
AS
/****************************************************************************
**  Name : GetMemberCanEarnCouponTypes  
**  Version: 1.0.0.20
**  Description : 获得此会员可以获得的coupontypes. 包括已获得过和未获得过的，只要是允许获得的（条件包括会员等级，登录卡品牌，登录店铺....）。
**  Parameter :
sp_helptext GetMemberCanEarnCouponTypes
select * from card where memberid = 66
  declare @MemberID int, @count int, @recordcount int, @a int  
  set @MemberID = 0
  exec @a = GetMemberCanEarnCouponTypes 0, '01001072', null, null, null, null, 0, 0, 0, 0, '',1, 30, @count output, @recordcount output, ''
  print @a  
  print @count
  print @recordcount
  select * from earncouponrule
  select * from CardGradeHoldCouponRule
  select * from card where memberid = 1074
  select * from card
  select * from coupontype where coupontypeid = 85
   select * from coupon where coupontypeid = 85 and status in (0,1)
  select * from coupon where cardnumber = '012300000012'
    select * from coupon where coupontypeid = 114 and status = 1
**  Created by: Gavin @2012-02-20
**  Modify by: Gavin @2012-07-20 （ver 1.0.0.1） 增加BrandCode输入， Brandcode + StoreCode，定位StoreID
**  Modify by: Gavin @2012-10-19 （ver 1.0.0.2） 增加返回可用coupon数量。（issued状态的coupon为可用，status=1）
**  Modify by: Gavin @2012-10-19 （ver 1.0.0.3） 修改storecode->StoreID, 修改brandCode->BrandID (此brandID为 CouponType的BrandID)
**  Modify by: Gavin @2013-01-15 （ver 1.0.0.4） 实现CardGradeHoldCouponRule的控制（只有输入CardNumber的情况下，此校验才生效）
**  Modify by: Gavin @2013-02-20 （ver 1.0.0.5） 修正bug,清除debug代码，CardGradeHoldCouponRule中如果没有记录的，则表示不做持有数量控制
**  Modify by: Gavin @2013-06-20 （ver 1.0.0.6） 去除memberid 和 cardnumber的校验, 允许不输入会员信息.
**  Modify by: Gavin @2013-07-04 （ver 1.0.0.7） 修正当访客登录(没有memberid，cardnumber)，读取的变量为NULL时，查询不到数据的问题
**  Modify by: Gavin @2013-09-09 （ver 1.0.0.8） CardGradeHoldCouponRule增加最大允许下载Coupon数量限制. 需要做相应的过滤
**  Modify by: Gavin @2013-09-23 （ver 1.0.0.9） Member表的MemberSex 是int 类型，所以@MemberSex 变量需要修改。(null或0：保密。1：男性。 2：女性)
**  Modify by: Gavin @2014-08-06 （ver 1.0.0.10）改造过程,使用 SelectDataInBatchs 的方法
**  Modify by: Gavin @2014-08-08 （ver 1.0.0.11）增加join MemberClause表,ClauseTypeCode hardcode为 'CouponType'
**  Modify by: Gavin @2014-08-11 （ver 1.0.0.12）增加输入参数@CouponTypeNatureID
**  Modify by: Gavin @2014-09-11 （ver 1.0.0.13）增加判断规则的status和 startdate,enddate
**  Modify by: Gavin @2014-11-13 （ver 1.0.0.14）取消group by coupon, CouponCountBalance始终返回9
**  Modify by: Gavin @2014-12-05 （ver 1.0.0.15）增加返回字段MemberClauseDesc
**  Modify by: Gavin @2015-01-14 （ver 1.0.0.16）增加输入参数iBeaconGroupID, 默认为0，可不填
**  Modify by: Gavin @2015-01-20 （ver 1.0.0.17）给出的SQL过长,调整一下SQL语句,去掉不必要的空格
**  Modify by: Gavin @2015-12-24 （ver 1.0.0.18）效率方面考虑,取消分页输出功能
**  Modify by: Gavin @2017-03-22  (Ver 1.0.0.19) 应调用方要求，MemberClauseDesc,CouponTypeNotes 返回字段类型改为 Text。   
**  Modify by: Gavin @2017-03-30  (Ver 1.0.0.20) Michael 的 JDBC，遇到，MemberClauseDesc,CouponTypeNotes是 NULL，转换为 TEXT时会出错。驱动不改的情况下只能加isnull。 
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @MemberSex int, @MemberYearOfBirth int, @MemberMonthOfBirth int, @MemberDateOfBirth datetime,
		  @MemberDayOfBirth int, @CardBrandID int, @StoreGroupID varchar(36), @CardGradeID int, @CardTypeID int,
          @StoreBrandID int, @StoreCountry nvarchar(20), @StoreProvince nvarchar(20), @StoreCity nvarchar(30)
  declare @MemberBirthdayLimit int, @MemberSexLimit int, @MemberAgeLimit int, @Language int
                
--  if (isnull(@MemberID, 0) = 0) and (isnull(@CardNumber, '') = '')  
--    return -7  
  
  set @BrandID = isnull(@BrandID, 0)
  set @CampaignID = isnull(@CampaignID, 0)
  set @ExchangeType = isnull(@ExchangeType, 0)
  set @DeviceType = isnull(@DeviceType, 0)

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
        
  -- 查询资料  
  if (isnull(@CardNumber, '') = '')   
  begin
    select @MemberSex = MemberSex, @MemberYearOfBirth = MemberYearOfBirth, @MemberMonthOfBirth = MemberMonthOfBirth, @MemberDayOfBirth = MemberDayOfBirth, @MemberDateOfBirth = MemberDateOfBirth,
      @CardGradeID = 0, @CardBrandID = 0, @CardTypeID = 0
	from member where memberID = @MemberID
  end else
  begin
    select @MemberSex = MemberSex, @MemberYearOfBirth = MemberYearOfBirth, @MemberMonthOfBirth = MemberMonthOfBirth, @MemberDayOfBirth = MemberDayOfBirth, @MemberDateOfBirth = MemberDateOfBirth,
        @CardGradeID = C.CardGradeID, @CardBrandID = T.BrandID, @CardTypeID = C.CardTypeID
      from Card C left join member M on C.MemberID = M.MemberID 
      left join CardType T on C.CardTypeID = T.CardTypeID 
     where C.CardNumber = @CardNumber
  end
  
  -- 转换查询条件
  set @MemberSex = isnull(@MemberSex, 0)
  set @MemberSexLimit = @MemberSex
  
  if isnull(@MemberDateOfBirth, '') <> ''
  begin
    if (DatePart(dd, getdate()) = DatePart(dd, @MemberDateOfBirth)) and (DatePart(mm, getdate()) = DatePart(mm, @MemberDateOfBirth))
      set @MemberBirthdayLimit = 3
    else if (DatePart(ww, getdate()) = DatePart(ww, @MemberDateOfBirth))
      set @MemberBirthdayLimit = 2
    else if (DatePart(mm, getdate()) = DatePart(mm, @MemberDateOfBirth))  
      set @MemberBirthdayLimit = 1
    else set @MemberBirthdayLimit = 0
  end else set @MemberBirthdayLimit = 0
  
  if isnull(@MemberDateOfBirth, '') <> ''
    set @MemberAgeLimit = DATEDIFF(yy, @MemberDateOfBirth, getdate())  + 1
  else set @MemberAgeLimit = 0
  
  select @StoreBrandID = BrandID from Store where StoreID = @StoreID
  set @CardGradeID = isnull(@CardGradeID, 0)
  set @CardBrandID = isnull(@CardBrandID, 0) 
  set @CardTypeID = isnull(@CardTypeID, 0)
  set @StoreBrandID = ISNULL(@StoreBrandID, 0)
  set @MemberBirthdayLimit = ISNULL(@MemberBirthdayLimit, 0)
  set @MemberAgeLimit = ISNULL(@MemberAgeLimit, 0)
  set @StoreID = ISNULL(@StoreID, 0)
  set @StoreGroupID = ISNULL(@StoreGroupID, 0)  
  set @CardNumber = isnull(@CardNumber, '')
  set @CouponTypeNatureID = isnull(@CouponTypeNatureID, 0)
            
  -- 查询
  set @SQLStr =        
  'select R.KeyID, C.CouponTypeStartDate, C.CouponTypeEndDate, C.CouponTypeName1, R.CouponTypeID, C.CouponTypeCode, case when ' + cast(@Language as varchar)+'= 2 then C.CouponTypeName2 when ' + cast(@Language as varchar)+' = 3 then C.CouponTypeName3 else C.CouponTypeName1 end as CouponName,'
+ 'C.CampaignID, cast(isnull(C.CouponTypeNotes,'''') as text) as CouponTypeNotes, C.CouponTypeLayoutFile, ExchangeType, ExchangeAmount, ExchangePoint, ExchangeCouponTypeID, E.CouponTypeCode as ExchangeCouponTypeCode, ExchangeCouponCount, C.CouponTypePicFile,'
+ '9 as CouponCountBalance, '
+ 'U.ClauseName, U.ClauseName2, U.ClauseName3, cast(isnull(U.MemberClauseDesc1,'''') as text) as MemberClauseDesc1, cast(isnull(U.MemberClauseDesc2,'''') as text) as MemberClauseDesc2, cast(isnull(U.MemberClauseDesc3,'''') as text) as MemberClauseDesc3 '
+ ',N.CouponTypeNatureID '
+ ',case when ' + cast(@Language as varchar)+'= 2 then cast(isnull(U.MemberClauseDesc2,'''') as text) when ' + cast(@Language as varchar)+' = 3 then cast(isnull(U.MemberClauseDesc3,'''') as text) else cast(isnull(U.MemberClauseDesc1,'''') as text) end as MemberClauseDesc '
+ 'from EarnCouponRule R left join CouponType C on R.CouponTypeID = C.CouponTypeID '  
+ ' left join CouponType E on R.ExchangeCouponTypeID = E.CouponTypeID'
--+ '     left join (select CouponTypeID, count(CouponNumber) as CouponCountBalance from Coupon WITH(READPAST) where Status = 1 and isnull(CardNumber,'''') = '''' group by CouponTypeID ) A on A.CouponTypeID = R.CouponTypeID'
+ ' left join (select * from MemberClause where ClauseTypeCode = ''CouponType'') U on U.ClauseSubCode = cast(R.CouponTypeID as varchar) '
+ ' left join CouponTypeNature N on C.CouponTypeNatureID = N.CouponTypeNatureID '
--     left join CouponTypeStoreCondition_List L on E.CouponTypeID = L.CouponTypeID and L.StoreConditionType = 1
+ ' where '
+ ' (R.Status = 1 and R.StartDate < getdate() and R.EndDate > (Getdate() - 1)) and '
+ ' (CardTypeIDLimit = '+cast(@CardTypeID as varchar)+' or isnull(CardTypeIDLimit, 0) = 0) and'
+ ' (CardGradeIDLimit = '+cast(@CardGradeID as varchar)+' or isnull(CardGradeIDLimit, 0) = 0) and'
+ ' (CardTypeBrandIDLimit = '+cast(@CardBrandID as varchar)+' or isnull(CardTypeBrandIDLimit, 0) = 0) and'          
+ ' (StoreBrandIDLimit = '+cast(@StoreBrandID as varchar)+' or (isnull(StoreBrandIDLimit, 0) = 0)) and'
+ ' (StoreGroupIDLimit = '+cast(@StoreGroupID as varchar)+' or isnull(StoreGroupIDLimit, 0) = 0) and'
+ ' (StoreIDLimit = '+cast(@StoreID as varchar)+' or isnull(StoreIDLimit, 0) = 0) and'
+ ' (MemberBirthdayLimit = '+cast(@MemberBirthdayLimit as varchar)+' or isnull(MemberBirthdayLimit, 0) = 0) and'
+ ' (MemberSexLimit = '+cast(@MemberSexLimit as varchar)+' or isnull(MemberSexLimit, 0) = 0) and'
+ ' (MemberAgeMinLimit <= '+cast(@MemberAgeLimit as varchar)+' or isnull(MemberAgeMinLimit, 0) = 0) and'
+ ' (MemberAgeMaxLimit >= '+cast(@MemberAgeLimit as varchar)+' or isnull(MemberAgeMaxLimit, 0) = 0) and'
+ ' (C.CampaignID = '+cast(@CampaignID as varchar)+' or '+cast(isnull(@CampaignID,0) as varchar)+' = 0) and'
+ ' (R.ExchangeType = '+cast(@ExchangeType as varchar)+' or '+cast(isnull(@ExchangeType,0) as varchar)+' = 0) and'
+ ' (C.BrandID = '+cast(@BrandID as varchar)+' or '+cast(isnull(@BrandID,0) as varchar)+' = 0) and'
+ ' (C.CouponTypeNatureID = '+cast(@CouponTypeNatureID as varchar)+' or '+cast(@CouponTypeNatureID as varchar)+' = 0) and'
+ ' (C.CouponTypeStartDate < getdate() and C.CouponTypeEndDate >= (getdate()-1)) and'  
+ ' (R.DeviceID = '+cast(@iBeaconGroupID as varchar)+' or '+cast(isnull(@iBeaconGroupID,0) as varchar)+' = 0) and '        
+ ' ('+cast(@CardGradeID as varchar)+' = 0 or (R.CouponTypeID in '
+ ' (select R1.CouponTypeID from CardGradeHoldCouponRule R1 left join'
+ ' (select CouponTypeID, count(CouponNumber) as CouponCount from'
+ ' (select CouponTypeID, CouponNumber from Coupon WITH(READPAST) ' 
+ ' where CardNumber = ''' + @CardNumber + ''' and Status = 2) A1 '
+ ' group by CouponTypeID) H1 on R1.CouponTypeID = H1.CouponTypeID'
+ ' where isnull(R1.HoldCount,0) > isnull(H1.CouponCount,0) and R1.CardGradeID = '+cast(@CardGradeID as varchar)+' and R1.RuleType = 0) '                                                                      
+ ' ) or R.CouponTypeID not in (select CouponTypeID from CardGradeHoldCouponRule where RuleType = 0)'
+ ' )  and'
+ ' ('+cast(@CardGradeID as varchar)+' = 0 or (R.CouponTypeID in '
+ ' (select R2.CouponTypeID from CardGradeHoldCouponRule R2 left join'
+ ' (select CouponTypeID, count(CouponNumber) as CouponCount from'
+ ' (select CouponTypeID, CouponNumber from Coupon WITH(READPAST) ' 
+ ' where InitCardNumber = ''' + @CardNumber + ''') A2 '
+ ' group by CouponTypeID) H2 on R2.CouponTypeID = H2.CouponTypeID'
+ ' where isnull(R2.HoldCount,0) > isnull(H2.CouponCount,0) and R2.CardGradeID = '+cast(@CardGradeID as varchar)+' and R2.RuleType = 2) '                                                                      
+ ' ) or R.CouponTypeID not in (select CouponTypeID from CardGradeHoldCouponRule where RuleType = 2)'
+ ' )'

  exec sp_executesql @SQLStr
  set @RecordCount = @@ROWCOUNT
  set @PageCount = 1

  --exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr 

  return 0
end

GO
