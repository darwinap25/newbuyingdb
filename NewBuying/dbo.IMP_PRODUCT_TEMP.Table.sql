USE [NewBuying]
GO
/****** Object:  Table [dbo].[IMP_PRODUCT_TEMP]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMP_PRODUCT_TEMP](
	[INTERNAL_PRODUCT_CODE] [varchar](64) NULL,
	[COMPANY_ID] [varchar](64) NULL,
	[BARCODE] [varchar](64) NOT NULL,
	[SUPPLIER_PRODUCT_CODE] [varchar](64) NULL,
	[BRAND] [varchar](64) NULL,
	[YEAR] [int] NULL,
	[SEASON] [varchar](64) NULL,
	[ARTICLE] [varchar](64) NULL,
	[SEX] [int] NULL,
	[MODEL] [varchar](64) NULL,
	[COLOR] [varchar](64) NULL,
	[PSIZE] [varchar](64) NULL,
	[CLASS] [varchar](64) NULL,
	[DESCRIPTION] [nvarchar](512) NULL,
	[REORDER_LEVEL] [varchar](64) NULL,
	[RETIRED] [varchar](64) NULL,
	[RETIRE_DATE] [datetime] NULL,
	[LAST_UPDATE_DATE] [datetime] NULL,
	[LAST_UPDATE_USER] [varchar](64) NULL,
	[DESCRIPTION2] [nvarchar](512) NULL,
	[SUPPLIER] [varchar](64) NULL,
	[STANDARD_COST] [money] NULL,
	[SIZE_RANGE] [varchar](64) NULL,
	[HS_CODE] [varchar](64) NULL,
	[EXPORT_COST] [money] NULL,
	[LOCAL_DESCRIPTION] [nvarchar](512) NULL,
	[COO] [varchar](64) NULL,
	[MATERIAL_1] [varchar](64) NULL,
	[MATERIAL_2] [varchar](64) NULL,
	[DESIGNER] [varchar](64) NULL,
	[BUYER] [varchar](64) NULL,
	[MERCHANDISER] [varchar](64) NULL,
	[SKU] [varchar](64) NULL,
	[AVG_COST] [money] NULL,
	[PRICE] [money] NULL,
	[FINAL_DISCOUNT_PRICE] [money] NULL,
	[ShortDescription1] [nvarchar](512) NULL,
	[ShortDescription2] [nvarchar](512) NULL,
	[ShortDescription3] [nvarchar](512) NULL,
	[Material1] [nvarchar](max) NULL,
	[Material2] [nvarchar](max) NULL,
	[Material3] [nvarchar](max) NULL,
	[Detailed1] [nvarchar](max) NULL,
	[Detailed2] [nvarchar](max) NULL,
	[Detailed3] [nvarchar](max) NULL,
	[Design1] [nvarchar](max) NULL,
	[Design2] [nvarchar](max) NULL,
	[Design3] [nvarchar](max) NULL,
	[ProductName1] [nvarchar](512) NULL,
	[ProductName2] [nvarchar](512) NULL,
	[ProductName3] [nvarchar](512) NULL,
	[IsOnlineSKU] [int] NULL,
	[OuterLayerMaterials] [varchar](512) NULL,
	[InnerLayerMaterials] [varchar](512) NULL,
	[SizeCategory] [varchar](512) NULL,
	[Video] [varchar](512) NULL,
	[360Photos] [varchar](512) NULL,
	[Photos1] [varchar](512) NULL,
	[Photos2] [varchar](512) NULL,
	[Photos4] [varchar](512) NULL,
	[Photos5] [varchar](512) NULL,
	[Photos6] [varchar](512) NULL,
	[Photos7] [varchar](512) NULL,
	[Photos10] [varchar](512) NULL,
	[Photos8] [varchar](512) NULL,
	[Photos3] [varchar](512) NULL,
	[Photos9] [varchar](512) NULL,
	[SizeM1] [varchar](64) NULL,
	[SizeM2] [varchar](64) NULL,
	[SizeM3] [varchar](64) NULL,
	[SizeM4] [varchar](64) NULL,
	[SizeM5] [varchar](64) NULL,
	[SizeM6] [varchar](64) NULL,
	[SizeM7] [varchar](64) NULL,
	[SizeM8] [varchar](64) NULL,
 CONSTRAINT [PK_IMP_PRODUCT_TEMP] PRIMARY KEY CLUSTERED 
(
	[BARCODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[IMP_PRODUCT_TEMP] ADD  DEFAULT ((0)) FOR [IsOnlineSKU]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'读入的product表。for Bauhaus。 
@2016-02-24   字段根据用户提供的excel文档。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IMP_PRODUCT_TEMP'
GO
