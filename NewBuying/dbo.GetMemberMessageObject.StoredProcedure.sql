USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberMessageObject]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberMessageObject]
  @MemberID				int,
  @MessageType          int,			  -- 消息类型. (未定)
  @IsReceiveMessage     int,              -- 0: 非接收到的消息（发送的消息）。 1：接收的消息。 默认1
  @IsRead	            int,              -- 0：未读消息。 1：已读消息。 2：所有消息。 默认0。
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=1 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr         varchar(20)=''
AS
/****************************************************************************
**  Name : GetMemberMessageObject
**  Version: 1.0.0.5
**  Description : 会员读取内部消息.
**
**  Parameter :  sp_helptext GetMemberMessageObject
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetMemberMessageObject 761, 0, 1, 0, 0, 2000, @count output, @recordcount output, 'en_CA'
  print @a
  print @recordcount 
  select * from MessageReceiveList where memberid = 13  
  select * from membermessageaccount where memberid = 13
     select * from messageobject
**  Created by: Gavin @2012-05-23
**  Modify by: Gavin @2012-09-26 （ver 1.0.0.1） 增加返回时间，并根据时间排序。
**  Modify by: Gavin @2012-10-16 （ver 1.0.0.2） 按照时间降序排序。（最近的在前面）
**  Modify by: Gavin @2014-07-08 （ver 1.0.0.3） 修正@IsReceiveMessage=0时的SQL语句错误
**  Modify by: Gavin @2014-10-28 （ver 1.0.0.4） 过滤已经删除的记录.(IsRead= -1 表示已经被删除)
**  Modify by: Gavin @2016-08-22 （ver 1.0.0.5） MessageBodyChar 兼容
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @SQLSelect nvarchar(1000), @SQLUpdate nvarchar(1000), @SQLWhere nvarchar(2000)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @MessageType = isnull(@MessageType, 0)
  set @IsRead = isnull(@IsRead, 2)
  if isnull(@IsReceiveMessage, 1) = 1
  begin
    set @SQLSelect = 'select O.MessageID, MessageType, MessageServiceTypeID, MessageCoding, MessageTitle, MessageBody, R.IsRead, cast(cast(MessageBody as nvarchar(max)) as text) as MessageBodyChar, O.CreatedOn, O.FromMemberID '
	set @SQLWhere = 
       ' from MessageReceiveList R left join MessageObject O on R.MessageID = O.MessageID '
       + ' where O.IsInternal = 1 and R.IsRead >= 0 '
       + ' and R.MemberID = ' + cast(@MemberID as varchar)
       + ' and (O.MessageType = ' + cast(@MessageType as varchar) + ' or ' + cast(@MessageType as varchar) + ' = 0) '
       + ' and (R.IsRead = ' + cast(@IsRead as varchar) + ' or ' + cast(@IsRead as varchar) + ' = 2) '   
  end else
  begin
    set @SQLSelect = ' select MessageID, MessageType, MessageServiceTypeID, MessageCoding, MessageTitle, MessageBody, 0 as IsRead,  cast(cast(MessageBody as nvarchar(max)) as text) as MessageBodyChar, CreatedOn, FromMemberID '
    set @SQLWhere = ' from MessageObject where FromMemberID = ' + cast(@MemberID as varchar)
  end

  set @SQLStr = @SQLSelect + @SQLWhere
  exec SelectDataInBatchs @SQLStr, 'MessageID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, ' order by CreatedOn desc '  

  return 0  
end

GO
