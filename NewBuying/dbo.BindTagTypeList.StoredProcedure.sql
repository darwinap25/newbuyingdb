USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BindTagTypeList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BindTagTypeList]
	-- Add the parameters for the stored procedure here
    @userid             int
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
        [c].[CouponTypeID],
        [c].[CouponTypeCode],
        [c].[CouponTypeName1] 
    FROM [dbo].[CouponType] c
        INNER JOIN [dbo].[Brand] b
            ON b.[BrandCode] = [c].[BrandCode]
        INNER JOIN [dbo].[UserAirlineMap] u
            ON u.[AirlineCode] = [c].[BrandCode]
     WHERE [u].[UserID] = @userid            
    ORDER BY CouponTypeName1
    
END

GO
