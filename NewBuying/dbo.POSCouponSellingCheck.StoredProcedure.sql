USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSCouponSellingCheck]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[POSCouponSellingCheck] 
  @CardNumber           varchar(64),            -- 会员卡号 (可不填. 填写则绑定到此卡) 
  @BindProdCode         varchar(64),            -- 销售绑定的prodcode   
  @CouponUID            varchar(512),           -- 需要激活的Coupon号码.如果多coupon则填写首号码      
  @StoreCode            varchar(64),            -- 店铺编号    
  @CouponExpiryDate     datetime output,        -- coupon有效期  
  @CouponAmount         money output,           -- coupon价值   
  @CouponStatus         int output              -- coupon状态      
AS    
/****************************************************************************    
**  Name : POSCouponSellingCheck    
**  Version: 1.0.0.0   
**  Description : 在MPOS端销售coupon时做检查.(销售激活)     
**    
**  Parameter :    
select * from coupon where status in (0,1)
select * from CouponTypeExchangeBinding
select * from buy_store
declare @A int ,@CouponExpiryDate datetime, @CouponStatus int, @CouponAmount money
exec @A = POSCouponSellingCheck '', 'PVCLTA', '000000001001', 'DEMO',
  @CouponExpiryDate output, @CouponAmount output, @CouponStatus output
print @A
print @CouponExpiryDate

**  Created by: Gavin @2017-10-11                    
**    
****************************************************************************/    
begin    
  DECLARE @BrandCode VARCHAR(64), @status INT, @CouponNumber VARCHAR(64), @BrandID INT, @CouponTypeID INT, @StoreID INT, @StoreCount INT,
          @LocateStoreID INT
  
  set @CardNumber = isnull(@CardNumber,'')
  set @BindProdCode = isnull(@BindProdCode,'')
  set @CouponUID = isnull(@CouponUID,'')
  set @StoreCode = isnull(@StoreCode,'')
       
  set @status=0    
  select @CouponNumber=CouponNumber from CouponUIDMap where CouponUID=@CouponUID    
  if isnull(@CouponNumber,'')=''     
  begin    
    set @status=-19    
    return @status    
  end
      
  if @Status=0    
  begin    
    select Top 1 @BrandID=BrandID from BUY_BRAND    
    select @CouponExpiryDate=CouponExpiryDate,@CouponStatus=[Status],@CouponTypeID=CouponTypeID, 
	  @LocateStoreID=LocateStoreID, @CouponAmount= CouponAmount 
    from Coupon where CouponNumber=@CouponNumber    
  end
                                          
  -- 验证传入的prodcode 是否SVA中设置的 销售指定coupontype 的Prodcode    
  if isnull(@BindProdCode, '') <> ''    
  begin    
    if not exists(select keyid from CouponTypeExchangeBinding 
	                where CouponTypeID = @CouponTypeID and BindingType = 1 
					      and ProdCode = @BindProdCode and BrandID = @BrandID)    
      return -64    
  end   
  
/*
  if @Status=0    
  begin    
    select @StoreID=StoreID from buy_Store where StoreBrandCode=@BrandCode and StoreCode=@StoreCode    
    if (isnull(@LocateStoreID,0)<>0) and (@StoreID<>isnull(@LocateStoreID,0))    
      set @status=-25    
    else    
    begin    
      select @StoreCount=count(StoreID) from CouponTypeStoreCondition_List    
      where CouponTypeID=@CouponTypeID and StoreID=@StoreID and StoreConditionType=1    
      if @StoreCount=0 set @status=-25    
    end      
    if @status <> 0    
      return @status     
  end         
*/
  return @Status      
end    

GO
