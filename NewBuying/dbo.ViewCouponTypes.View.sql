USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewCouponTypes]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewCouponTypes]
AS
 select T.BrandID, T.CouponTypeID, CouponTypeCode, CouponTypeName1, CouponTypeName2, CouponTypeName3, CouponTypeNotes, 
    T.CouponTypeNatureID, CouponTypeNatureName1, CouponTypeNatureName2, CouponTypeNatureName3, CouponTypeStartDate, CouponTypeEndDate, 
     T.CouponTypeAmount, T.CouponTypePoint, T.CouponTypeDiscount, '' as CouponTypeBindPLU, T.CouponTypeLayoutFile,
     T.CampaignID, P.CampaignCode, P.CampaignType, T.IsPhysicalCoupon, T.IsNeedVerifyNum, T.VerifyNumLifecycle
     ,T.[Status], T.CouponTypePicFile, A.MemberClauseDesc1, A.MemberClauseDesc2, A.MemberClauseDesc3
     , E.ExchangeAmount, E.ExchangePoint, T.TrainingMode, T.UnlimitedUsage
   from CouponType T left join CouponTypeNature N on T.CouponTypeNatureID = N.CouponTypeNatureID
     left join Campaign P on T.CampaignID = P.CampaignID
     left join (select * from MemberClause where ClauseTypeCode = 'CouponType') A on A.ClauseSubCode = cast(T.CouponTypeID as varchar) 
     left join (select CouponTypeID, max(isnull(ExchangeAmount,0)) as ExchangeAmount, 
                   max(isnull(ExchangePoint,0)) as ExchangePoint from EarnCouponRule 
                  group by CouponTypeID) E on T.CouponTypeID = E.CouponTypeID

GO
