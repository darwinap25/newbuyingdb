USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SignSalesShipOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[SignSalesShipOrder]                
  @UserID				         INT,	            -- 操作员ID
  @SalesShipOrderNumber          VARCHAR(64)        -- 单号码
AS
/****************************************************************************
**  Name : SignSalesShipOrder 
**  Version: 1.0.0.3
**  Description : 送货单签收,更新状态.  (这步是有门店做. 顾客不会签收不执行)
exec SignSalesShipOrder 1, 'SSO000000000001'
**  Created by: Gavin @2016-05-24  
**  Modify by: Thomas @2016-08-31 (ver 1.0.0.1) 倘若所有送貨單完成了,將狀態更改為完成status=2, ，未完成则return错误值：
**  Modify by: Thomas @2017-03-21 filter out home delivery order
**  Modify by: Thomas @2017-03-23 (ver 1.0.0.3) Shipment must be done before sales Order can be completed
****************************************************************************/

BEGIN

  DECLARE @TxnNo VARCHAR(64), @SalesPickOrderNumber VARCHAR(64), @OrderType INT, @OrderStatus INT=-1
  
  --SELECT @OrderStatus = ISNULL(Status,-1) FROM Ord_SalesShipOrder_H WHERE SalesShipOrderNumber = @SalesShipOrderNumber
  
  SELECT @OrderStatus = Status, @TxnNo = TxnNo, @SalesPickOrderNumber = ReferenceNo, @OrderType = OrderType FROM Ord_SalesShipOrder_H WHERE SalesShipOrderNumber = @SalesShipOrderNumber

  IF @OrderStatus = 0 --Version: 1.0.0.3
    RETURN -2720	
  
  IF @OrderType = 2 
    RETURN -2719

  IF @OrderStatus < 0
    RETURN -2710
	
  IF @OrderStatus = 2 
    RETURN -2711
  
  IF @OrderStatus = 3
    RETURN -2712

  IF @OrderType = 1 /* Store */
  BEGIN
     IF @OrderStatus = 1
     BEGIN
       UPDATE Ord_SalesShipOrder_H SET Status = 2, UpdatedOn = GETDATE(), UpdatedBy = @UserID 
        WHERE SalesShipOrderNumber = @SalesShipOrderNumber and status = 1
     END
  END ELSE IF @OrderType = 2 /* Home Delivery */
  BEGIN
     IF @OrderStatus = 1
     BEGIN
       UPDATE Ord_SalesShipOrder_H SET Status = 3, UpdatedOn = GETDATE(), UpdatedBy = @UserID 
        WHERE SalesShipOrderNumber = @SalesShipOrderNumber and status = 1
     END
	 
     IF NOT EXISTS(SELECT * FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNo AND ApproveStatus = 'P') AND
	    NOT EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE Status <> 2 AND TxnNo = @TxnNo)
     BEGIN
       UPDATE SALES_H SET Status = 5, UpdatedOn = GETDATE(), UpdatedBy = @UserID WHERE TransNum = @TxnNo
	 END
--	 END ELSE
--	   RETURN -206	 
  
  END

  --SELECT @TxnNo = TxnNo, @SalesPickOrderNumber = ReferenceNo, @OrderType = OrderType FROM Ord_SalesShipOrder_H WHERE SalesShipOrderNumber = @SalesShipOrderNumber
/*
  IF NOT EXISTS(SELECT * FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNo AND ApproveStatus = 'P')
  BEGIN
    IF NOT EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE Status <> 2 AND TxnNo = @TxnNo)
	BEGIN
	  IF @OrderType = 1 
	  BEGIN
	    UPDATE SALES_H SET Status = 13, UpdatedOn = GETDATE(), UpdatedBy = @UserID WHERE TransNum = @TxnNo
	  END ELSE IF @OrderType = 2 
	  BEGIN
	    UPDATE SALES_H SET Status = 5, UpdatedOn = GETDATE(), UpdatedBy = @UserID WHERE TransNum = @TxnNo
	  END
	END    
  END
*/


  RETURN 0

END

GO
