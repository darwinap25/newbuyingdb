USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCalcFreight]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DoCalcFreight]
  @Type                       int,             -- 计费类型。 1：YB , 2: bauhaus
  @TotalAmount                money,           -- 订单消费总金额。  （@Type=2 起效）
  @TotalWeight                decimal(16,6),   -- 订单总重量        （@Type=1 起效）
  @CountryCode                varchar(64),     -- 送达地址所在国家。
  @ProvinceCode               varchar(64),     -- 送达地址所在省份。
  @LogisticsProviderID        int,             -- 使用的供应商。
  @Freight                    money output     -- 计算获得的运费
AS
/****************************************************************************
**  Name : DoCalcFreight
**  Version: 1.0.0.1
**  Description : 计算运费。
    declare @Freight money
    exec DoCalcFreight 2,100,0,'','',0, @Freight output
    print @Freight
	select * from LogisticsPrice
**  Created by:  Gavin @2016-01-01
**  Modify by:  Gavin @2016-10-25 （ver 1.0.0.1) （bauhaus）在YB的基础上扩展. 
****************************************************************************/
begin    
  declare  @StartPrice money, @StartWeight decimal(16,6), @OverflowPricePerKG money, @FreeAmount money
  set @Freight = 0
  set @Type = isnull(@Type, 2)
  set @TotalAmount = isnull(@TotalAmount, 0)
  set @TotalWeight = isnull(@TotalWeight, 0)
  set @CountryCode = isnull(@CountryCode, '')
  set @ProvinceCode = isnull(@TotalAmount, '')
  set @LogisticsProviderID = isnull(@LogisticsProviderID, 0)
  
  if @Type = 1
  begin
	  select Top 1 @StartPrice = isnull(StartPrice,0), @StartWeight = isnull(StartWeight,0), 
	    @OverflowPricePerKG = isnull(OverflowPricePerKG,0)
	  from LogisticsPrice where ProvinceCode = @ProvinceCode and LogisticsProviderID = @LogisticsProviderID
	  if @@Rowcount > 0
	  begin
		set @Freight = @StartPrice
		if @TotalWeight > @StartWeight 
		begin
		  set @Freight = cast((@TotalWeight - @StartWeight) * @OverflowPricePerKG + @StartPrice as money)
		end
	  end
  end

  if @Type = 2
  begin
      select Top 1 @StartPrice = isnull(StartPrice,0), @StartWeight = isnull(StartWeight,0), 
	    @OverflowPricePerKG = isnull(OverflowPricePerKG,0), @FreeAmount = ISNULL(FreeAmount, 0)
	  from LogisticsPrice where CountryCode = @CountryCode and (LogisticsProviderID = @LogisticsProviderID or @LogisticsProviderID = 0)
	  if @@Rowcount > 0
	  begin		
		if @TotalAmount >= @FreeAmount 
		begin
		  set @Freight = @StartPrice
		end else
		  set @Freight = 0
	  end
  end
end

GO
