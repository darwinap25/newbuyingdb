USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCampaignList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetCampaignList]
  @CampaignID           int,               -- 活动ID
  @CampaignCode         varchar(64),       -- 活动code
  @CampaignType         int,               -- 活动类型
  @BrandID              int,               -- 品牌ID
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1  
AS
/****************************************************************************
**  Name : GetCampaignList
**  Version: 1.0.0.4
**  Description : 获取Campaign List
**
**  Parameter :
select * from Campaign
  exec GetCampaignList 0,'',0,0,''
select * from Campaign
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**  Modified by: Gavin @2014-08-04 (ver 1.0.0.1) 增加返回 ExchangeCount字段
**  Modified by: Gavin @2014-08-08 (ver 1.0.0.2) 使用CampaignCouponType 表的设置,绑定campaign和coupontype
**  Modified by: Gavin @2014-08-26 (ver 1.0.0.3) 增加Startdate和Enddate 判断.
**  Modified by: Gavin @2014-08-26 (ver 1.0.0.4) Campaign中的status没地方设置, 所以要求存储过程中 hardcode 返回 2
**
****************************************************************************/
begin 
  declare @Language int, @SQLStr nvarchar(2000)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select case @Language when 2 then CampaignName2 when 3 then CampaignName3 else CampaignName1 end as CampaignName, 
         case @Language when 2 then CampaignDetail2 when 3 then CampaignDetail3 else CampaignDetail1 end as CampaignDetail, 
         A.CampaignID, CampaignCode, CampaignType, CampaignPicFile, 
         case when isnull(A.Status,0) = 0 then 2 else A.Status end as [Status], 
         A.BrandID, StartDate, EndDate, 
         A.CreatedOn, A.CreatedBy, A.UpdatedOn, A.UpdatedBy, isnull(C.ExchangeCount, 0) as ExchangeCount
    from Campaign A 
      left join CampaignCouponType B on A.CampaignID = B.CampaignID
      left join (select MAX(ExchangeCouponCount) as ExchangeCount, ExchangeCouponTypeID from EarnCouponRule group by ExchangeCouponTypeID) C on B.CouponTypeID = C.ExchangeCouponTypeID 
  where (A.CampaignID = @CampaignID or (isnull(@CampaignID, 0) = 0))
        and (CampaignCode = @CampaignCode or (isnull(@CampaignCode, '') = ''))
        and (CampaignType = @CampaignType or (isnull(@CampaignType, 0) = 0))
        and (A.BrandID = @BrandID or (isnull(@BrandID, 0) = 0))	
        and A.StartDate	<= GETDATE() and A.EndDate >= (GETDATE() - 1)
  return 0 
end

GO
