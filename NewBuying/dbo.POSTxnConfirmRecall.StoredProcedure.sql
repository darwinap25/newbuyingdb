USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSTxnConfirmRecall]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[POSTxnConfirmRecall]
  @UserID             varchar(512),			--用户ＩＤ
  @TxnNo              varchar(512),			--交易号
  @ApprovalCode		  varchar(512) output	--返回的approvalCode
as
/****************************************************************************
**  Name : POSTxnConfirmRecall
**  Version: 1.0.0.0   (for RRG)
**  Description : POS提交的receivetxn 记录， 如果出现返回0，但获取Approval Code失败的情况提交完成。
**
**  Parameter :
declare @A int, @UserID varchar(512), @TxnNo varchar(512), @ApprovalCode varchar(512)
set @UserID = 
set TxnNo =
exec @A = POSTxnConfirmRecall @UserID, @TxnNo, @ApprovalCode output
print @A
**  Created by: Robin @2012-09-27
****************************************************************************/
begin
if isnull(@TxnNo,'')<>''
  select top 1 @ApprovalCode=isnull(ApprovalCode,'') from ReceiveTxn where TxnNo=@TxnNo
  
if @ApprovalCode='' return -1
else return 0
end

GO
