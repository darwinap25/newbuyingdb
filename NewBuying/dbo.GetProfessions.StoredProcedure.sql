USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProfessions]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetProfessions]
  @ProfessionID             int,       -- 主键, 为null或0, 则返回全部
  @LanguageAbbr             varchar(20)=''
AS
/****************************************************************************
**  Name : GetProfessions
**  Version: 1.0.0.0
**  Description :查找Profession数据
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetProfessions 0, ''
  print @a  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select ProfessionID, ProfessionCode,
     case when @Language = 2 then ProfessionName2 when @Language = 3 then ProfessionName3 else ProfessionName1 end as ProfessionName            
   from Profession where ProfessionID = @ProfessionID or isnull(@ProfessionID, 0) = 0
  return 0  
end

GO
