USE [NewBuying]
GO
/****** Object:  Table [dbo].[CheckDigitMode]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CheckDigitMode](
	[CheckDigitModeID] [int] IDENTITY(1,1) NOT NULL,
	[CheckDigitModeCode] [varchar](64) NOT NULL,
	[CheckDigitModeName] [nvarchar](512) NULL,
	[CheckDigitModeDesc] [nvarchar](512) NULL,
 CONSTRAINT [PK_CHECKDIGITMODE] PRIMARY KEY CLUSTERED 
(
	[CheckDigitModeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CheckDigitMode', @level2type=N'COLUMN',@level2name=N'CheckDigitModeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验位code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CheckDigitMode', @level2type=N'COLUMN',@level2name=N'CheckDigitModeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验位名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CheckDigitMode', @level2type=N'COLUMN',@level2name=N'CheckDigitModeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验位描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CheckDigitMode', @level2type=N'COLUMN',@level2name=N'CheckDigitModeDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验位计算方法表。
目前有两种算法： 
CheckDigitModeCode 值分别为EAN13 和 MOD10。 按照CheckDigitModeCode判断算法。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CheckDigitMode'
GO
