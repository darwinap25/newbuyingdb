USE [NewBuying]
GO
/****** Object:  Table [dbo].[Accounts_RolePermissions]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts_RolePermissions](
	[RoleID] [int] NOT NULL,
	[PermissionID] [int] NOT NULL
) ON [PRIMARY]

GO
