USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRecordByPageOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GetRecordByPageOrder] @tblName varchar(255),   -- 表名   
  @fldName varchar(512),   -- 显示字段名   
  @OrderfldName varchar(255),  -- 排序字段名   
  @StatfldName varchar(512),  -- 统计字段名   
  @PageSize int = 10,   -- 页尺寸   
  @PageIndex int = 0,   -- 页码   
  @IsReCount bit = 0,   -- 返回记录总数, 非 0 值则返回   
  @OrderType bit = 0,   -- 设置排序类型, 非 0 值则降序   
  @strWhere varchar(max) = ''  -- 查询条件 (注意: 不要加 where)   
  as  
begin   
declare @strSQL varchar(max)  -- 主语句   
set @strSQL = ''  
declare @CountstrSQL varchar(max)  -- 计算记录数   
set @CountstrSQL = ''  
declare @strOrder varchar(max)  -- 排序类型   
  
declare @begin bigint   
declare @end bigint   
  
set @begin = 0  
set @end = 0  
  
if isnull(@OrderfldName, '') = ''   
return -1  
  if @PageSize > 0  
  begin   
    
 if @PageIndex < 0 set @PageIndex = 0  
  
 set @begin = (@PageIndex * @PageSize) + 1  
 set @end = (@PageIndex + 1) * @PageSize  
   
 if @OrderType != 0 set @strOrder =  @OrderfldName + ' desc'  
 else set @strOrder = @OrderfldName + ' asc'  
   
 set @strSQL = @strSQL + ' select * from (select  row_number() OVER (Order BY ' + @strOrder +' ) AS rrow ,'   
 if @strWhere != '' set @strSQL =  @strSQL + @fldName + ' from ' + @tblName + ' where '+ @strWhere + ' ) A '  
 else set @strSQL =  @strSQL + @fldName + ' from ' + @tblName + ' ) A '  
 set @strSQL = @strSQL + 'where rrow between ' + cast(@begin as varchar(8))+ ' and ' + cast(@end as varchar(8))  
  
/*  
 if @IsReCount != 0   
 begin  
  if @strWhere != '' set @strSQL = @strSQL+' select count(1) as Total from [' + @tblName + ']' + ' where ' + @strWhere  
  else set @strSQL = @strSQL+' select count(1) as Total from [' + @tblName + ']'   
 end  
-- print @strSQL  
    exec (@strSQL)  
       
*/  
    exec (@strSQL)  
    
   if @IsReCount != 0   
   begin  
    if @strWhere != '' set @CountstrSQL = ' select count(1) as Total from [' + @tblName + ']' + ' where ' + @strWhere  
    else set @CountstrSQL = ' select count(1) as Total from [' + @tblName + ']'   
    exec (@CountstrSQL)  
   end  
  
  end   
  else  
  begin  
    return -1  
  end    
  return 0        
end  


GO
