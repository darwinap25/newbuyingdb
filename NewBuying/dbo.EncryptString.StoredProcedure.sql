USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[EncryptString]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EncryptString]
   @ClearText varchar(4000), 
   @SecurityCode varchar(4000) output 
AS
/****************************************************************************
**  Name : EncryptString
**  Version: 1.0.0.0
**  Description : 加密字符串 (转成二进制字符串)

declare @a int, @out varchar(4000)
exec @a = EncryptString 'MemberID=9,MemberAccountType=2,MemberAccount=abc@sina.com', @out output
print @a
print @out
**  Created by: Gavin @2013-08-20
**
****************************************************************************/
begin
  select @SecurityCode = dbo.ConvertHexToString(cast(@ClearText as varbinary(max)))
  return 0 
end

GO
