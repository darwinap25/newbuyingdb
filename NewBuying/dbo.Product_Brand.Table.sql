USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_Brand]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Brand](
	[ProductBrandID] [int] IDENTITY(1,1) NOT NULL,
	[ProductBrandCode] [varchar](64) NOT NULL,
	[ProductBrandName1] [nvarchar](512) NULL,
	[ProductBrandName2] [nvarchar](512) NULL,
	[ProductBrandName3] [nvarchar](512) NULL,
	[ProductBrandDesc1] [nvarchar](max) NULL,
	[ProductBrandDesc2] [nvarchar](max) NULL,
	[ProductBrandDesc3] [nvarchar](max) NULL,
	[ProductBrandPicSFile] [nvarchar](512) NULL,
	[ProductBrandPicMFile] [nvarchar](512) NULL,
	[ProductBrandPicGFile] [nvarchar](512) NULL,
	[CardIssuerID] [int] NULL,
	[IndustryID] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PRODUCT_BRAND] PRIMARY KEY CLUSTERED 
(
	[ProductBrandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品小图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandPicSFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品中图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandPicMFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'ProductBrandPicGFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品发行方ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'CardIssuerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand', @level2type=N'COLUMN',@level2name=N'IndustryID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Brand'
GO
