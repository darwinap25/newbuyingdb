USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_RPRICETYPE]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_RPRICETYPE](
	[RPriceTypeID] [int] IDENTITY(1,1) NOT NULL,
	[RPriceTypeCode] [varchar](64) NOT NULL,
	[RPriceTypeName1] [nvarchar](512) NULL,
	[RPriceTypeName2] [nvarchar](512) NULL,
	[RPriceTypeName3] [nvarchar](512) NULL,
	[AdditionalType] [int] NOT NULL,
	[Supervisor] [int] NOT NULL,
	[SerialNo] [int] NOT NULL,
	[Discount] [int] NOT NULL,
	[TypeLevel] [int] NOT NULL,
	[MemberShip] [int] NOT NULL,
	[StockTypeCode] [varchar](64) NOT NULL,
	[RANOnly] [int] NOT NULL,
	[DAMOnly] [int] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_RPRICETYPE] PRIMARY KEY CLUSTERED 
(
	[RPriceTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [AdditionalType]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [Supervisor]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [SerialNo]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [Discount]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [TypeLevel]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [MemberShip]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ('G') FOR [StockTypeCode]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [RANOnly]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT ((0)) FOR [DAMOnly]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_RPRICETYPE] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'RPriceTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价类型编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'RPriceTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'RPriceTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'RPriceTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'RPriceTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未定义' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'AdditionalType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要管理员登录。0：不需要。1需要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'Supervisor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要输入serialno。0：不需要。1需要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'SerialNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否折扣标志。0：不是。1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'Discount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型级别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'TypeLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否用于会员。0：不是。1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'MemberShip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型code。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否用于RAN货品。0：不是。1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'RANOnly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否用于损坏货品。0：不是。1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE', @level2type=N'COLUMN',@level2name=N'DAMOnly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价类型表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICETYPE'
GO
