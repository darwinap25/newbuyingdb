USE [NewBuying]
GO
/****** Object:  Table [dbo].[StockOnhandSync_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockOnhandSync_H](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CheckDate] [datetime] NOT NULL,
	[Note] [varchar](512) NULL,
	[ApproveStatus] [char](1) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_STOCKONHANDSYNC_H] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_StockOnhandSync]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_StockOnhandSync] ON [dbo].[StockOnhandSync_H]
For INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_StockOnhandSync
* Version: 1.0.0.1
* Description : onhand 数据同步触发器。
* Create By Gavin @2014-10-10
* Modify by Gavin @2014-11-19 (ver 1.0.0.1) 修正insert时的问题（left join 改为 right join）
*/
/*==============================================================*/
BEGIN
  DECLARE @KeyID int, @OldApproveStatus char(1), @ApproveStatus char(1)
  
  DECLARE CUR_StockOnhandSync CURSOR fast_forward FOR
    SELECT KeyID, ApproveStatus FROM INSERTED
  OPEN CUR_StockOnhandSync
  FETCH FROM CUR_StockOnhandSync INTO @KeyID, @ApproveStatus
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where KeyID = @KeyID
    
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      -- Coupon
      update Coupon_Onhand set OnhandQty = A.OnhandQty + B.AdjQty
      from Coupon_Onhand A left join 
      (
        select * from StockOnhandSync_D where HeadKeyID = @KeyID and SyncType = 2
      ) B on A.StoreID = B.StoreID and A.CouponTypeID = B.CouponTypeID and A.CouponStatus = B.Status 
              and A.CouponStockStatus = B.StockStatus
      -- 一般不会出现 Coupon_Onhand没有,Coupon有的情况.       
      insert into Coupon_Onhand(CouponTypeID,StoreTypeID,StoreID,CouponStatus,CouponStockStatus,OnhandQty,UpdatedBy)
      select B.CouponTypeID,B.StoreTypeID,B.StoreID,B.Status,B.StockStatus,AdjQty,1 from Coupon_Onhand A right join 
      (
        select * from StockOnhandSync_D where HeadKeyID = @KeyID and SyncType = 2
      ) B on A.StoreID = B.StoreID and A.CouponTypeID = B.CouponTypeID and A.CouponStatus = B.Status 
              and A.CouponStockStatus = B.StockStatus
      where A.CouponTypeID is null   
           
      -- Card
      update Card_Onhand  set OnhandQty = A.OnhandQty + B.AdjQty
      from Card_Onhand A left join 
      (
        select * from StockOnhandSync_D where HeadKeyID = @KeyID and SyncType = 1
      ) B on A.StoreID = B.StoreID and A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID and A.CardStatus = B.Status 
              and A.CardStockStatus = B.StockStatus
      -- 一般不会出现 Coupon_Onhand没有,Coupon有的情况.       
      insert into Card_Onhand(CardTypeID,CardGradeID,StoreTypeID,StoreID,CardStatus,CardStockStatus,OnhandQty,UpdatedBy)
      select B.CardTypeID,B.CardGradeID,B.StoreTypeID,B.StoreID,B.Status,B.StockStatus,AdjQty,1 from Card_Onhand A right join 
      (
        select * from StockOnhandSync_D where HeadKeyID = @KeyID and SyncType = 1
      ) B on A.StoreID = B.StoreID and A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID and A.CardStatus = B.Status 
              and A.CardStockStatus = B.StockStatus
      where A.CardTypeID is null  
            
    end
    FETCH FROM CUR_StockOnhandSync INTO @KeyID, @ApproveStatus
  END
  CLOSE CUR_StockOnhandSync 
  DEALLOCATE CUR_StockOnhandSync           
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_H', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'盘点日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_H', @level2type=N'COLUMN',@level2name=N'CheckDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： R：prepare。A:Approve 。
Approve时触发器启动，执行更新。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存onhand同步记录表。（主表）
根据 card 和 coupon的记录，同步更新card_onhand 和Coupon_onhand的数据。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockOnhandSync_H'
GO
