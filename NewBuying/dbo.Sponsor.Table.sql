USE [NewBuying]
GO
/****** Object:  Table [dbo].[Sponsor]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sponsor](
	[SponsorID] [int] IDENTITY(1,1) NOT NULL,
	[SponsorCode] [varchar](64) NOT NULL,
	[SponsorName1] [varchar](512) NULL,
	[SponsorName2] [varchar](512) NULL,
	[SponsorName3] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SPONSOR] PRIMARY KEY CLUSTERED 
(
	[SponsorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Sponsor] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Sponsor] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sponsor', @level2type=N'COLUMN',@level2name=N'SponsorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赞助商编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sponsor', @level2type=N'COLUMN',@level2name=N'SponsorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赞助商名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sponsor', @level2type=N'COLUMN',@level2name=N'SponsorName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赞助商名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sponsor', @level2type=N'COLUMN',@level2name=N'SponsorName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赞助商名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sponsor', @level2type=N'COLUMN',@level2name=N'SponsorName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赞助商' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sponsor'
GO
