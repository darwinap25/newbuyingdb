USE [NewBuying]
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertHexToString]    Script Date: 12/13/2017 2:45:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ConvertHexToString]
(
  @HexNumber varbinary(1023)   
)
RETURNS varchar(1023) 
AS
begin  
        declare   @charvalue   varchar(1023)   
        declare   @i   int   
        declare   @length   int   
        declare   @hexstring   char(16)   
    
        select   @charvalue   =   0x   
        select   @i   =   1   
        select   @length   =   datalength(@HexNumber)   
        select   @hexstring   =   '0123456789ABCDEF'
    
        while   (@i   <=   @length)   
        begin   
            declare   @tempint   int   
            declare   @firstint   int   
            declare   @secondint   int   
            select   @tempint   =   convert(int,   substring(@HexNumber,@i,1))   
            select   @firstint   =   floor(@tempint/16)   
            select   @secondint   =   @tempint   -   (@firstint*16)   
            select   @charvalue   =   @charvalue   +   
                substring(@hexstring,   @firstint+1,   1)   +   
                substring(@hexstring,   @secondint+1,   1)   
    
            select   @i   =   @i   +   1   
        end   
    
        RETURN  @charvalue  
end

GO
