USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetRSAPublicKey]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRSAPublicKey]
  @NowPublicKey varchar(512),
  @APPID int,
  @APPIP varchar(20),
  @NewPublicKey varchar(512) output,
  @StartDate date output,
  @EndDate date output
AS
/****************************************************************************
**  Name : GetRSAPublicKey
**  Version: 1.0.0.0
**  Description : 获得RSA公钥。
declare @NewPublicKey varchar(512), @StartDate date,  @EndDate date
exec GetRSAPublicKey '', 0, '', @NewPublicKey output, @StartDate output,  @EndDate output
print @NewPublicKey 
print @StartDate
print @EndDate 

**  Created by: Gavin @2012-08-21
**
****************************************************************************/
begin
  declare @BinaryValue varbinary(1024)    
  select Top 1 @BinaryValue = PublicKey, @StartDate = StartDate, @EndDate = EndDate from RSAKeys
  where StartDate <= GetDate() and EndDate >= GetDate()   
  order by StartDate  
					
  exec DecryptRSAKey @BinaryValue, @NewPublicKey output
end

GO
