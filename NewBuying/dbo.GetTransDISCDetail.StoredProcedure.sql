USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetTransDISCDetail]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTransDISCDetail]
  @TransNum              VARCHAR(64),              -- 交易号。 空表示不设条件
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN  
AS
/****************************************************************************
**  Name : GetTransDISCDetail
**  Version : 1.0.0.0
**  Description : 获得交易数据(Sales_Disc)
**
  declare  @a int, @PageCount int, @RecordCount int
  exec @a = GetTransDISCDetail '',  '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  select * from Sales_D
**  Created by Gavin @2015-03-06
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1  
  
  SET @TransNum = ISNULL(@TransNum, '')
  
  SET @WhereStr = ' WHERE 1=1 '
  IF @TransNum <> ''
    SET @WhereStr = @WhereStr + ' AND A.TransNum=''' + @TransNum + ''''

  SET @SQLStr = 'SELECT A.KeyID, A.TransNum, A.ItemSeqNo, A.ProdCode, A.TenderSeqNo, A.TenderID, A.TenderCode, '
    + ' A.SalesDiscCode, A.SalesDiscDesc, A.SalesDiscType, A.SalesDiscDataType, A.SalesDiscPrice, A.SalesPrice, '
    + ' A.SalesDiscOffAmount, A.SalesDiscQty, A.AffectNetPrice, A.SalesDiscLevel '
    + ' FROM SALES_DISC A '
    + ' LEFT JOIN SALES_H H ON A.TransNum = H.TransNum '    
  SET @SQLStr = @SQLStr + @WhereStr
  
  EXEC SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr  

  RETURN 0
END

GO
