USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberMessageAccount]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberMessageAccount]
  @MemberID				int,
  @MessageAccountID     int,
  @MessageServiceTypeID int,
  @IsPrefer             int=null,
  @CardNumber           varchar(64)=''
AS
/****************************************************************************
**  Name : GetMemberMessageAccount
**  Version: 1.0.0.4
**  Description : 返回用户消息账号
**
**  Parameter :
  exec GetMemberMessageAccount 599, 0,0, null, '000100086'
  select * from messageservicetype
  select * from card where memberid = 170
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-09-05
**  Modify by: Gavin @2012-11-21 (ver 1.0.0.1) 增加查询条件ServiceID，增加返回字段TokenUID，TokenStr，TokenUpdateDate
**  Modify by: Gavin @2013-01-24 (ver 1.0.0.2) 增加参数@IsPrefer, Null或者-1: 忽略此条件。 默认NULL
											   增加参数@CardNumber, Null或者-1: 忽略此条件。 默认NULL. 如果填写，则使用CardNumber查找MemberID，并替代传入参数@MemberID
**  Modify by: Gavin @2013-08-23 (ver 1.0.0.3) 增加返回新加字段VerifyFlag
**  Modify by: Gavin @2014-11-27 (ver 1.0.0.4) 增加返回字段PromotionFlag 
**MemberClause
****************************************************************************/
begin
  if isnull(@CardNumber, '') <> ''
    select @MemberID = MemberID from Card where CardNumber = @CardNumber
    
    select MessageAccountID, MemberID, MessageServiceTypeID, AccountNumber, [Status], Note,IsPrefer, TokenUID, TokenStr, TokenUpdateDate, VerifyFlag, PromotionFlag
      from MemberMessageAccount 
        where (MemberID = @MemberID or isnull(@MemberID, 0) = 0)
          and (MessageAccountID = @MessageAccountID or isnull(@MessageAccountID, 0) = 0)
          and (MessageServiceTypeID = @MessageServiceTypeID or isnull(@MessageServiceTypeID, 0) = 0)
          and (IsPrefer = @IsPrefer or isnull(@IsPrefer, -1) = -1)
   
  return 0 
end

GO
