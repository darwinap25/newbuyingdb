USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCouponTypes]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetCouponTypes]
  @MemberID				int,       -- 会员ID
  @CardNumber			varchar(512),      -- 如填写CardNumber，忽略MemberID	  
  @BrandID				int,			   -- Null或空表示全部。  
  @ConditionStr			nvarchar(1000),     -- 查询条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=1 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1
AS
/****************************************************************************
**  Name : GetCouponTypes  
**  Version: 1.0.0.3
**  Description : 获得所有CouponType数据, 不加过滤限制
**  Parameter :

  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetCouponTypes null, null, null, 'CouponTypeNatureID=0', 1, 30, @count output, @recordcount output, 1
  print @a  
  print @count
  print @recordcount
 
**  Created by: Gavin @2012-02-20
**  Modified by: Gavin @2013-06-21 (Ver 1.0.0.1) 使用1.0.0.6版本的SelectDataInBatchs	,输入的@ConditionStr直接传给SelectDataInBatchs 
**  Modified by: Gavin @2014-08-14 (Ver 1.0.0.2) 加上条件: 只取有效期内的记录. 没有设置有效期的（NULL），则认为长期有效。
**  Modified by: Gavin @2014-12-31 (Ver 1.0.0.3) 统一startdate和enddate的比较规则：如果填写的日期不带时间（时间为0），则和 getdate（）比较时，startdate可以直接判断 < getdate， enddate 需要 >= (getdate() - 1)
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
      
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
          
  if @Language = 2
    set @SQLStr = ' select CouponTypeName2 as CouponTypeName, * from ViewCouponTypes '
      + ' where (isnull(CouponTypeStartDate, convert(varchar(10), GETDATE(), 120)) < convert(varchar(10), GETDATE(), 120)) '
      + '      and (isnull(CouponTypeEndDate,convert(varchar(10), GETDATE(), 120)) >= convert(varchar(10), GETDATE()-1, 120)) '
  else if @Language = 3
    set @SQLStr = ' select CouponTypeName3 as CouponTypeName, * from ViewCouponTypes '
      + ' where (isnull(CouponTypeStartDate,convert(varchar(10), GETDATE(), 120)) < convert(varchar(10), GETDATE(), 120)) '
      + '      and (isnull(CouponTypeEndDate,convert(varchar(10), GETDATE(), 120)) >= convert(varchar(10), GETDATE()-1, 120)) '    
  else
    set @SQLStr = ' select CouponTypeName1 as CouponTypeName, * from ViewCouponTypes '  
      + ' where (isnull(CouponTypeStartDate,convert(varchar(10), GETDATE(), 120)) < convert(varchar(10), GETDATE(), 120)) '
      + '      and (isnull(CouponTypeEndDate, convert(varchar(10), GETDATE(), 120)) >= convert(varchar(10), GETDATE()-1, 120)) '    

  exec SelectDataInBatchs @SQLStr, 'CouponTypeID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr    
  
  return 0
end

GO
