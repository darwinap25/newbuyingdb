USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardOrderForm_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardOrderForm_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardOrderFormNumber] [varchar](64) NOT NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[CardQty] [int] NULL,
	[OrderAmount] [money] NULL,
	[OrderPoint] [int] NULL,
 CONSTRAINT [PK_ORD_CARDORDERFORM_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_D', @level2type=N'COLUMN',@level2name=N'CardOrderFormNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_D', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_D', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'需求数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_D', @level2type=N'COLUMN',@level2name=N'CardQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单收到金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_D', @level2type=N'COLUMN',@level2name=N'OrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单收到积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_D', @level2type=N'COLUMN',@level2name=N'OrderPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡订货单明细。
表中brandID，cardtypeid，cardgradeid 关系不做校验，由UI在创建单据时做校验。
存储过程实际操作时，只按照CardGradeID来做。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardOrderForm_D'
GO
