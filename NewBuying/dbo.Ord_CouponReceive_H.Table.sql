USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponReceive_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponReceive_H](
	[CouponReceiveNumber] [varchar](64) NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[StoreID] [int] NULL,
	[SupplierID] [int] NULL,
	[StorerAddress] [nvarchar](512) NULL,
	[SupplierAddress] [nvarchar](512) NULL,
	[SuppliertContactName] [varchar](512) NULL,
	[SupplierPhone] [varchar](512) NULL,
	[SupplierEmail] [varchar](512) NULL,
	[SupplierMobile] [varchar](512) NULL,
	[StoreContactName] [varchar](512) NULL,
	[StorePhone] [varchar](512) NULL,
	[StoreEmail] [varchar](512) NULL,
	[StoreMobile] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[OrderType] [int] NULL DEFAULT ((0)),
	[CouponTypeID] [int] NULL,
	[CouponQTY] [int] NULL,
	[ReceiveType] [int] NULL,
	[Remark1] [varchar](512) NULL,
	[Remark2] [varchar](512) NULL,
	[CompanyID] [int] NULL,
 CONSTRAINT [PK_ORD_COUPONRECEIVE_H] PRIMARY KEY CLUSTERED 
(
	[CouponReceiveNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CouponReceive_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CouponReceive_H] ON [dbo].[Ord_CouponReceive_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CouponReceive_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2014-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @CouponReceiveNumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @CouponReceiveNumber = CouponReceiveNumber FROM INSERTED  
  exec GenUserMessage @CreatedBy, 10, 0, @CouponReceiveNumber  
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponReceive_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponReceive_H] ON [dbo].[Ord_CouponReceive_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponReceive_H
* Version: 1.0.0.0
* Description : 供应商的Coupon收货单表的批核触发器

  select * from Ord_CouponReceive_H 
  select * from Ord_CouponReceive_D
  update Ord_CouponReceive_H set ReceiveType = 1 where CouponReceiveNumber = 'CREC00000000018'
  update Ord_CouponReceive_H set ApproveStatus = 'A' where CouponReceiveNumber = 'CREC00000000261'
  select * from Ord_CouponReceive_D where CouponReceiveNumber = 'CREC00000000267'
  select * from coupon_onhand where coupontypeid = 170
  select * from coupon_onhand
  select * from coupon where couponnumber = '123450000001'
** Create By Gavin @2014-06-04
*/
/*==============================================================*/
BEGIN  
  declare @CouponReceiveNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @FirstCoupon varchar(64), @EndCoupon varchar(64), @CouponTypeID int, @ReceiveType int, @VoidStockStatus int
  declare @CouponQty int, @StoreID int, @HCouponTypeID int
  
  DECLARE CUR_CouponReceive_H CURSOR fast_forward FOR
    SELECT CouponReceiveNumber, ApproveStatus, CreatedBy, ReceiveType, CouponQty, StoreID, CouponTypeID FROM INSERTED
  OPEN CUR_CouponReceive_H
  FETCH FROM CUR_CouponReceive_H INTO @CouponReceiveNumber, @ApproveStatus, @CreatedBy, @ReceiveType,@CouponQty, @StoreID, @HCouponTypeID
  WHILE @@FETCH_STATUS=0
  BEGIN
    if @ReceiveType = 1
      set @VoidStockStatus = 0
    if @ReceiveType = 2
      set @VoidStockStatus = 6
            
    select @OldApproveStatus = ApproveStatus from Deleted where CouponReceiveNumber = @CouponReceiveNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
      exec CouponOrderApprove 1, 1, @CouponReceiveNumber
      update Ord_CouponReceive_H set ApprovalCode = @ApprovalCode where CouponReceiveNumber = @CouponReceiveNumber
      
        -- 库存数量变动
      exec ChangeCouponStockStatus 4, @CouponReceiveNumber, 1   
      -- 产生提示消息.
      exec GenUserMessage @CreatedBy, 10, 1, @CouponReceiveNumber     
    end  

    if @ApproveStatus = 'V' and Update(ApproveStatus)
    begin
       if (@OldApproveStatus = 'P')  -- 如果原来是Approve状态，那么void时需要减去 Onhand中的数量。
         update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponQty  
           where StoreID = @StoreID and CouponTypeID = @HCouponTypeID and CouponStatus = 0 and CouponStockStatus = 1      
                   
        DECLARE CUR_CouponReceive_Detail CURSOR fast_forward for
          select FirstCouponNumber, EndCouponNumber, CouponTypeID from Ord_CouponReceive_D where CouponReceiveNumber = @CouponReceiveNumber
	      OPEN CUR_CouponReceive_Detail
        FETCH FROM CUR_CouponReceive_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
        WHILE @@FETCH_STATUS=0 
        BEGIN         
          update Coupon set StockStatus = @VoidStockStatus
            where CouponTypeID = @CouponTypeID and CouponNumber >= @FirstCoupon and CouponNumber <= @EndCoupon                                 
	      FETCH FROM CUR_CouponReceive_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
    	  END
        CLOSE CUR_CouponReceive_Detail 
        DEALLOCATE CUR_CouponReceive_Detail         
    end
        
    FETCH FROM CUR_CouponReceive_H INTO @CouponReceiveNumber, @ApproveStatus, @CreatedBy, @ReceiveType, @CouponQty, @StoreID,@HCouponTypeID
  END
  CLOSE CUR_CouponReceive_H 
  DEALLOCATE CUR_CouponReceive_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货确认单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'CouponReceiveNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。指coupon订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收到货的店铺（后台）主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发出或的供应商ID， 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址。（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'StorerAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'SuppliertContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'StorePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'StoreEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货的订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'CouponQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货来源。 1：正常的订单收货。2：退货单的收货（只有店铺的退货单）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'ReceiveType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'Remark2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货确认单。主表 （后台对供应商收货确认）
工作流程： 根据订货单或者手动输入。 根据订货单时，根据订货单的Coupon首号码和数量，需要展开为单个Coupon， 用户将根逐个确认Coupon是否完好。（修改Coupon的库存状态）。 
修改内容： @2014-06-05：
如同Ord_OrderToSupplier_H的修改，在头表中加上CouponTypeID和QTY
修改内容： @2014-06-05：
OrderQty 改成 CouponQty， CouponTypeID和CouponQty 放在最后
修改内容： @2014-06-08：
增加ReceiveType。 （为方便SVAWeb，作为最后一个字段）。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_H'
GO
