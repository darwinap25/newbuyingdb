USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_Associated]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Associated](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[SeqNo] [int] NULL DEFAULT ((0)),
	[AssociatedProdCode] [varchar](64) NOT NULL,
	[AssociatedProdName] [nvarchar](512) NULL,
	[AssociatedProdFile] [nvarchar](512) NULL,
	[Note] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PRODUCT_ASSOCIATED] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Associated', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此货品的关联货品序号。从0开始。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Associated', @level2type=N'COLUMN',@level2name=N'SeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'和此货品关联的货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Associated', @level2type=N'COLUMN',@level2name=N'AssociatedProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'和此货品关联的货品名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Associated', @level2type=N'COLUMN',@level2name=N'AssociatedProdName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'和此货品关联的货品图片文件路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Associated', @level2type=N'COLUMN',@level2name=N'AssociatedProdFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Associated', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联货品列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Associated'
GO
