USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBUYProductStyleInfo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetBUYProductStyleInfo]
  @StoreID              int,               -- 店铺ID
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr         varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1     
AS
/****************************************************************************
**  Name : GetBUYProductStyleInfo
**  Version: 1.0.0.1
**  Description : 获得BUY_ProductStyle数据	 （for bauhaus） 为了降低负载压力。 (BUYING DB)
**
  declare @a int, @PageCount int, @RecordCount int
  exec @a = GetBUYProductStyleInfo 0,'',7,50,@PageCount output,@RecordCount output,'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount  
  select * from product
  select * from Gender
   select * from buy_store
  select * from ProductStockOnhand
**  Created by: Gavin @2016-07-28
**  Modify by: Gavin @2017-06-09 (ver 1.0.0.1) DB改变，buy_product 的 BrandCode 改为 ProductBrandCode
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int

  set @StoreID = isnull(@StoreID, 0)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
     
  set @QueryCondition = isnull(@QueryCondition, '')

  set @SQLStr = 'select S.ProdCodeStyle as ProdCodeStyleCode, P.ProdCode, C.ColorCode, Q.TotQty as OnhandQty, '       
	  + ' E.SeasonCode as Season, G.GenderCode as sex, P.NewFlag, P.HotSaleFlag, P.Flag1, '	
	  + ' case ' + cast(@Language as varchar) + ' when 2 then Z.ProductSizeName2 when 3 then Z.ProductSizeName3 else Z.ProductSizeName1 end as ProductSizeName, '
	  + ' P.ProductBrandCode as ProductBrandID, P.DepartCode, R.RPriceTypeCode as ProdPriceType, R.NetPrice, R.DefaultPrice '	  
  set @SQLStr = @SQLStr + ' from buy_product P '
      + ' left join buy_Color C on P.ColorCode = C.ColorCode  '
  if @StoreID = 0
	 set @SQLStr = @SQLStr + ' left join (select ProdCode,sum(Isnull(OnHandQty, 0)) as TotQty from stk_StockOnhand where StockTypeCode = ''G'' group by ProdCode ) Q on P.ProdCode = Q.ProdCode '
  else
     set @SQLStr = @SQLStr + ' left join (select ProdCode,Isnull(OnHandQty, 0) as TotQty from stk_StockOnhand where StockTypeCode = ''G'' and storeid =' + CAST(@StoreID as varchar) + ') Q on P.ProdCode = Q.ProdCode '

  set @SQLStr = @SQLStr + ' left join (select ProdCodeStyle, ProdCode as PPCode from buy_Productstyle) S on P.ProdCode = S.PPCode ' 
	  + ' left join (select A.ProdCode, B.SeasonCode from (select * from buy_Product_Classify where ForeignTable = ''SEASON'') A left join SEASON B on A.ForeignKeyID = B.SeasonID) E on P.ProdCode = E.ProdCode '
      + ' left join (select A.ProdCode, B.GenderCode from (select * from buy_Product_Classify where ForeignTable = ''Gender'') A left join Gender B on A.ForeignKeyID = B.GenderID) G on P.ProdCode = G.ProdCode '
	  + ' left join Buy_ProductSize Z on P.ProductSizeCode = Z.ProductSizeCode ' 
      + ' left join (SELECT ProdCode, RPriceTypeCode, Price as NetPrice, RefPrice as DefaultPrice FROM BUY_RPRICE_M where KeyID in  (SELECT  MAX(KeyID) AS KeyID FROM BUY_RPRICE_M WHERE (StartDate <= GETDATE()) AND (EndDate+1 >= GETDATE()) group by ProdCode)) R on P.ProdCode = R.ProdCode'

--  set @SQLStr = @SQLStr + ' where (isnull(S.PPCode, '''') <> '''') '

  --exec SelectDataInBatchs_new @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @QueryCondition
  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @QueryCondition
  
  return 0
end

GO
