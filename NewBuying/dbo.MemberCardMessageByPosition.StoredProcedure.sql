USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardMessageByPosition]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[MemberCardMessageByPosition]
  @CardNumber varchar(64),    -- 卡号
  @PositionID varchar(64),    -- 地点编号
  @PositionType varchar(64),  -- 地点定位类型BlueTooth: 1; Mac:2; Defined Position: 3
  @LanguageAbbr varchar(20)=''
AS
/****************************************************************************
**  Name : MemberCardMessageByPosition
**  Version: 1.0.0.1
**  Description :  根据ibeacon位置,获得设定的MemberNotice和MessageTemplateDetail内容.
 
   declare @A int
   exec @A=MemberCardMessageByPosition '000300933', '1', '1'
   print @A

**  Created by: Gavin @2015-08-17
**
****************************************************************************/
BEGIN
  declare @MemberID int, @ReturnAmount money, @ReturnPoint int, @iBeaconID int, @AreaID int, @PromotionMsgKeyID int,
          @iBeaconGroupID int, @StoreID int, @StoreCode varchar(64), 
          @BrandID int, @BrandCode varchar(64), @count int, @recordcount int
  declare @CardTypeID int, @CardGradeID int, @Language int  
  Create Table #Temp (MessageTemplateID int, MessageTemplateCode varchar(64), MessageTemplateDesc nvarchar(512), 
         BrandID int, MessageTitle nvarchar(512), TemplateContent nvarchar(Max), Link varchar(512), URL varchar(512)) 
  Create Table #Return (Title varchar(512), Association_Type int, Association_ID varchar(64), Assiocation_Code varchar(64), 
    PicFile varchar(512), URL varchar(512), [Desc] varchar(512))
    
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1          
  select @MemberID = MemberID, @ReturnAmount = TotalAmount, @ReturnPoint = TotalPoints, 
      @CardTypeID = CardTypeID, @CardGradeID = CardGradeID 
    from Card where CardNumber = @CardNumber
--  select @BrandID = BrandID from CardType where CardTypeID = @CardTypeID  	
  select @AreaID = AreaID, @iBeaconID = iBeaconID from iBeacon where iBeaconCode = @PositionID
  select @iBeaconGroupID = iBeaconGroupID from iBeaconGroupMap where iBeaconID = @iBeaconID
  select @PromotionMsgKeyID = AssociationID from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID and AssociationType = 4

  insert into #Return
  select case @Language when 2 then D.MessageTitle2 when 3 then D.MessageTitle3 else D.MessageTitle1 end as MessageTitle,
     4, D.MessageTemplateID, H.MessageTemplateCode, '', '',
     case @Language when 2 then D.TemplateContent2 when 3 then D.TemplateContent3 else D.TemplateContent1 end as TemplateContent  
     from MessageTemplateDetail D left join MessageTemplate H on H.MessageTemplateID = D.MessageTemplateID 
     where D.MessageTemplateID = @PromotionMsgKeyID 
 
   select * from #Return 
END

GO
