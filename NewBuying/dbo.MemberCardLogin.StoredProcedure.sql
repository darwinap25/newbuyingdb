USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardLogin]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[MemberCardLogin]
  @UID                  varchar(512),			--刷卡login时, 实体卡固件ID，或者LaserID
  @CardNumber			varchar(512),			--SVA的Card Number （密文），如果LaserID时，无需填写此项
  @CardTypeCode			varchar(512),	        --SVA的CardTypeCode，如果LaserID时，无需填写此项
  @NeedPassword			int,					--0: 不需要要验证卡密码。 1：需要验证卡密码。 默认为1
  @CardPassword			varchar(512),			-- password（密文）
  @MemberID				int output,     -- 返回Member表主键
  @OutCardNumber		varchar(512) output, -- 返回Card Number （如果约定输入的CardNumber密文，则这里返回密文）
  @MemberCreatedOn      datetime output        -- 返回Member表记录的创建时间
AS
/****************************************************************************
**  Name : MemberCardLogin  会员卡登录
**  Version: 1.0.0.4
**  Description : 输入卡号,卡号密码（MD5加密，密文长度32）， 或者UID。 返回MemberID。
**  example:
  declare @MemberID varchar(30), @OutCardNumber nvarchar(30), @A int
  exec @A = MemberCardLogin '', '000100086', '',1, '1', @MemberID output, @OutCardNumber output
  print @A
  print @MemberID
  print @OutCardNumber
  select * from card where cardnumber = '000100086'
**  Created by: Gavin @2012-02-20
**  Modify by: Gavin @2012-08-29 (ver 1.0.0.1) 如果变量值为null， < , >, <> 逻辑判断时会不正确
**  Modify by: Gavin @2012-08-30 (ver 1.0.0.2) 如果输入的密码为空（或NULL），则不做MD5加密。（数据库中密码，若为空或NULL，同样不加MD5）
**  Modify by: Gavin @2013-04-03 (Ver 1.0.0.3) 增加返回参数@MemberCreatedOn
**  Modify by: Gavin @2013-10-11 (Ver 1.0.0.4) 记录操作到UserAction_Movement表
**
****************************************************************************/
begin
  declare @CardPWD varchar(512)
  if isnull(@CardPassword, '') <> ''
    set @CardPassword = dbo.EncryptMD5(@CardPassword)
  -- hardcode, 不读卡中内容，只读UID
  --set @CardNumber = ''
  --set @CardTypeCode = ''
  if isnull(@UID, '') <> ''
  begin
    --卡号解密--- 
    declare @CardNo nvarchar(30), @CardTypeID int, @GetSVACardNoResult int, @TUID varchar(20)  
    exec @GetSVACardNoResult = GetSVACardNo @UID, @CardNumber, @CardTypeCode, @TUID output, @CardNo output, @CardTypeID output
    if @GetSVACardNoResult <> 0
      return -99        
    select @MemberID = MemberID, @CardPWD = CardPassword, @OutCardNumber = CardNumber from Card where CardNumber = @CardNo and CardTypeID = @CardTypeID
  end else if isnull(@CardNumber, '') <> ''    -- 考虑虚拟卡的情况.(没有UID)
  begin
    select @MemberID = MemberID, @CardPWD = CardPassword, @OutCardNumber = CardNumber from Card where CardNumber = @CardNumber
  end
  
  if isnull(@OutCardNumber,'') = '' 
    return -2
 
  if @NeedPassword = 1 and isnull(@CardPassword, '') <> isnull(@CardPWD, '')  
    return -4
    
  if isnull(@CardNumber,'') <> '' 
    set @OutCardNumber = @CardNumber  

  select @MemberCreatedOn = CreatedOn from member where MemberID = @MemberID
  
  exec RecordUserAction @MemberID, 1, @CardNumber, '', 'Member Card Login', '', '', '', @MemberID      
  return 0      
end

GO
