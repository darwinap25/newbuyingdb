USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CloseSVASalesOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[CloseSVASalesOrder]
  @ASalesShipOrderNumber               VARCHAR(64)=''
AS
/****************************************************************************
**  Name : CloseSVASalesOrder   
**  Version: 1.0.0.0
**  Description :  用LinkServer的方式，在Buying DB 关闭SVA上的 sales单
select * from aftersales_H
Declare @a int
exec @a =CloseSVASalesOrder ''
print @a
**  Created by: Gavin @2016-11-18
****************************************************************************/
BEGIN

/*
  DECLARE @SalesShipOrderNumber varchar(64), @Status INT, @CreatedBy INT, @OldStatus INT, @ApprovalCode char(6)
  DECLARE @Busdate DATE, @StoreID INT, @StoreCode varchar(64)
  DECLARE @SalesPickOrderNumber VARCHAR(64), @TxnNO VARCHAR(64), @SalesStatus INT
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 ORDER BY BusDate DESC

  SET @ASalesShipOrderNumber = ISNULL(@ASalesShipOrderNumber, '')
  DECLARE CUR_CloseSVASalesOrder CURSOR fast_forward FOR
    SELECT SalesShipOrderNumber, Status, CreatedBy,ReferenceNo FROM Ord_SalesShipOrder_H 
	  WHERE Status =3 AND (SalesShipOrderNumber = @ASalesShipOrderNumber or @ASalesShipOrderNumber = '')
  OPEN CUR_CloseSVASalesOrder
  FETCH FROM CUR_CloseSVASalesOrder INTO @SalesShipOrderNumber, @Status, @CreatedBy,@SalesPickOrderNumber
  WHILE @@FETCH_STATUS=0
  BEGIN
	SELECT @TxnNO = H.ReferenceNo, @StoreCode = H.PickupLocation, @StoreID = S.StoreID FROM Ord_SalesPickOrder_H H 
	  LEFT JOIN BUY_STORE S ON H.PickupLocation = S.StoreCode
	WHERE SalesPickOrderNumber = @SalesPickOrderNumber

	IF NOT EXISTS(SELECT * FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNO AND ApproveStatus <> 'A')
	BEGIN
	  IF NOT EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE ReferenceNo IN (SELECT SalesPickOrderNumber FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNO) AND Status <> 3)
	  BEGIN
		--UPDATE A SET Status = @SalesStatus FROM OPENQUERY([LINKSVA], 'SELECT * FROM SVA_621.dbo.Sales_h') A  WHERE TxnNo = @TxnNO AND Status <> @SalesStatus 
	    UPDATE Sales_h SET Status = 5, UpdatedOn = GETDATE() WHERE TransNum = @TxnNO AND Status <> 5  -- sva 只需要complete就行。
	  END
	END	

    FETCH FROM CUR_CloseSVASalesOrder INTO @SalesShipOrderNumber, @Status, @CreatedBy, @SalesPickOrderNumber
  END
  CLOSE CUR_CloseSVASalesOrder 
  DEALLOCATE CUR_CloseSVASalesOrder  
*/
  return 0
END

GO
