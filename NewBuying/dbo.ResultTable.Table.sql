USE [NewBuying]
GO
/****** Object:  Table [dbo].[ResultTable]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ResultTable](
	[ResultID] [int] NOT NULL,
	[ResultDesc1] [varchar](512) NULL,
	[ResultDesc2] [varchar](512) NULL,
	[ResultDesc3] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
 CONSTRAINT [PK_RESULTTABLE] PRIMARY KEY CLUSTERED 
(
	[ResultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResultTable', @level2type=N'COLUMN',@level2name=N'ResultID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回值描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResultTable', @level2type=N'COLUMN',@level2name=N'ResultDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回值描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResultTable', @level2type=N'COLUMN',@level2name=N'ResultDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回值描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResultTable', @level2type=N'COLUMN',@level2name=N'ResultDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResultTable', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存储过程返回结果编码表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResultTable'
GO
