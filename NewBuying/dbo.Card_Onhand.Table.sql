USE [NewBuying]
GO
/****** Object:  Table [dbo].[Card_Onhand]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Card_Onhand](
	[CardTypeID] [int] NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[StoreTypeID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[CardStatus] [int] NOT NULL,
	[CardStockStatus] [int] NOT NULL,
	[OnhandQty] [int] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARD_ONHAND] PRIMARY KEY CLUSTERED 
(
	[CardTypeID] ASC,
	[CardGradeID] ASC,
	[StoreID] ASC,
	[CardStatus] ASC,
	[CardStockStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Card_Onhand] ADD  DEFAULT ((0)) FOR [CardStatus]
GO
ALTER TABLE [dbo].[Card_Onhand] ADD  DEFAULT ((0)) FOR [CardStockStatus]
GO
ALTER TABLE [dbo].[Card_Onhand] ADD  DEFAULT ((0)) FOR [OnhandQty]
GO
ALTER TABLE [dbo].[Card_Onhand] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型。（即店铺类型）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存所属店铺（总部）ID。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡状态，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand', @level2type=N'COLUMN',@level2name=N'CardStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡库存状态，默认0。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand', @level2type=N'COLUMN',@level2name=N'CardStockStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存数量，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand', @level2type=N'COLUMN',@level2name=N'OnhandQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡的库存数量。 （只有 CardType中标志为IsPhysicalCoupon=1的Card才会有库存数量。）
（卡数量的汇总表，表中数据应该和Card表的汇总数据相等）
（目前不使用流水记录表，直接修改onhand 数量）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Onhand'
GO
