USE [NewBuying]
GO
/****** Object:  Table [dbo].[AfterSales_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AfterSales_D](
	[SeqNo] [varchar](64) NOT NULL,
	[TxnNo] [varchar](64) NOT NULL,
	[TxnType] [int] NULL DEFAULT ((0)),
	[RefSeqNo] [varchar](64) NULL,
	[StoreCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[BusDate] [datetime] NULL,
	[TxnDate] [datetime] NULL DEFAULT (getdate()),
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[ProdDesc] [nvarchar](512) NULL,
	[DepartCode] [varchar](64) NULL,
	[Qty] [dbo].[Buy_Qty] NULL,
	[RetailPrice] [money] NULL,
	[NetPrice] [money] NULL,
	[NetAmount] [money] NULL,
	[POPrice] [money] NULL DEFAULT ((0)),
	[Description] [varchar](1000) NULL,
	[UploadPicFile1] [varchar](512) NULL,
	[UploadPicFile2] [varchar](512) NULL,
	[UploadPicFile3] [varchar](512) NULL,
	[UploadPicFile4] [varchar](512) NULL,
	[UploadPicFile5] [varchar](512) NULL,
	[Remark] [nvarchar](512) NULL,
	[Collected] [int] NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_AFTERSALES_D] PRIMARY KEY CLUSTERED 
(
	[TxnNo] ASC,
	[SeqNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单中货品序号。使用字符串是为了 预留 BOM功能时使用。 如果不使用BOM功能， 可以直接用 自然数。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'SeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'TxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型.
1：维修
2：退货
3：换货' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'TxnType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原单交易单中SeqNo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'RefSeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS机编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期时间。（system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'ProdDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品零售价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'RetailPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品实际销售价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'NetPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品金额（NetPrice*Qty）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'NetAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'改价价格（RetailPrice - POPrice = NetPrice）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'POPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'申请描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址1。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'UploadPicFile1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址2。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'UploadPicFile2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址3。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'UploadPicFile3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址4。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'UploadPicFile4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址5。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'UploadPicFile5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留。
设计整单统一快递状态。 所以只使用头表的 status。  如果同一单的货品出现不同送达情况， 可以启用此字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'Collected'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货物送达日期   （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'DeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'DeliveryBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'售后交易表。货品明细表
@2016-10-13 增加字段RetailPrice，NetPrice，NetAmount，POPrice （根据aftersales产生换货单时需要）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AfterSales_D'
GO
