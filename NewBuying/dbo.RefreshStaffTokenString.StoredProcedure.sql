USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[RefreshStaffTokenString]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[RefreshStaffTokenString]
  @StaffID			    int,      -- account_user 的 User_ID
  @ValidTime            int=1000,      -- 有效时间.  (时间间隔, 默认1000天. 单位:天)
  @GenNumber            int=10    -- 产生token 的数量。默认10
AS
/****************************************************************************
**  Name : RefreshStaffTokenString (for bauhaus)
**  Version: 1.0.0.2
**  Description : staff产生token,存放入 stafftoken表.
**  Example :
    exec RefreshStaffTokenString 1, 1, 10
	select * from stafftoken where userid = 26
	update stafftoken set status = 2 where keyid in (3,4,7)
**  Created by: Gavin @2016-11-03
**  Modify by: Gavin @2016-11-08 (ver 1.0.0.1) 增加字段VerifyType, VerifyTypeDesc, UpdatedOn
**  Modify by: Gavin @2016-11-08 (ver 1.0.0.2) @GenNumber 的内容含义改成 直接创建的 token数量,不需要再去考虑总数.
**
****************************************************************************/
begin
  declare @Token varchar(20), @A int, @i int, @UserName varchar(50), @ExpiryOn datetime,
          @validCount int
  set @i = 0
  set @ValidTime = isnull(@ValidTime, 0)
  set @ExpiryOn = DateAdd(dd, @ValidTime, Getdate())
    
  select @UserName = UserName from accounts_users where UserID = @StaffID
  select @validCount = count(*) from StaffToken where UserID = @StaffID and Status = 1
  
  --set @GenNumber = @GenNumber - @validCount

  while @i < @GenNumber
  begin
    exec @A = GenTokenString @StaffID, @Token output
    insert into StaffToken(UserID,UserName,TokenStr,Status,CreatedOn,ExpiryOn, VerifyType, VerifyTypeDesc, UpdatedOn)
	values(@StaffID, @UserName, @Token, 1, Getdate(), @ExpiryOn, 0, '', Getdate())
	set @i = @i + 1
  end
end

GO
