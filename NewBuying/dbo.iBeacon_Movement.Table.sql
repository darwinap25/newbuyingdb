USE [NewBuying]
GO
/****** Object:  Table [dbo].[iBeacon_Movement]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iBeacon_Movement](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[iBeaconID] [int] NOT NULL,
	[TokenID] [varchar](512) NULL,
	[AssociationType] [int] NULL DEFAULT ((0)),
	[AssociationID] [varchar](64) NULL,
	[Battery] [decimal](12, 2) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CardNumber] [varchar](64) NULL,
 CONSTRAINT [PK_IBEACON_MOVEMENT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeacon主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement', @level2type=N'COLUMN',@level2name=N'iBeaconID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'APP端传入的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement', @level2type=N'COLUMN',@level2name=N'TokenID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeaconGroup 相关联的类型 。  默认0.
1、Coupon （coupontypeid）。 
2、News （）
3、Product （Product表的 prodcode）
4、Message
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement', @level2type=N'COLUMN',@level2name=N'AssociationType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联对象的ID .根据AssociationType， 内容可能是coupontypeid， PromotionMsg的KeyID， Product表的 prodcode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement', @level2type=N'COLUMN',@level2name=N'AssociationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电池余量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement', @level2type=N'COLUMN',@level2name=N'Battery'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeacon的活动记录。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeacon_Movement'
GO
