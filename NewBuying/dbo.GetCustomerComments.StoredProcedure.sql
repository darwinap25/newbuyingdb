USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCustomerComments]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetCustomerComments]
  @ProdCode                        varchar(64),         -- 货品编码
  @Language                        varchar(20),         -- Chinese=zh_BIGCN, English=us en_CA CardPassword string M Card Password
  @OneStar                         int output,
  @TwoStar                         int output,
  @ThreeStar                       int output,
  @FourStar                        int output,
  @FiveStar                        int output,
  --@AVGStar                         decimal(10,1) output,
  @AVGStar                         money output,
  @ConditionStr			           nvarchar(1000)='',
  @OrderCondition	               nvarchar(1000)='',
  @PageCurrent			           int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				           int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			           int=0 output,	   -- 返回总页数。
  @RecordCount			           int=0 output 	   -- 返回总记录数。
AS
/****************************************************************************
**  Name : GetCustomerComments  (bauhaus)
**  Version: 1.0.0.4
**  Description :
  declare @OneStar int,@TwoStar int,@ThreeStar int,@FourStar int,@FiveStar int, @AVGStar decimal(10,1), @PageCount int, @RecordCount int
  exec GetCustomerComments '8805358581855','',@OneStar output,@TwoStar output,@ThreeStar output,@FourStar output,@FiveStar output, @AVGStar output,'','',1,0, @PageCount output, @RecordCount output
  print @OneStar
  print @TwoStar
  print  @ThreeStar
  print @FourStar
  print @FiveStar
  print @AVGStar
  select * from CustomerComments where prodcode = '101216A242860104'
**  Created by: Gavin @2016-02-15 
** Modify by Gavin @2016-03-24 (ver 1.0.0.1) 增加货品的统计值返回。 prodcode必须填 
** Modify by Gavin @2016-04-06 (ver 1.0.0.2) 增加字段VerifyFlag, 只返回VerifyFlag =1 的记录
** Modify by Gavin @2016-06-24 (ver 1.0.0.3) 返回时按照ProductStyle合并 
** Modify by Gavin @2016-09-05 (ver 1.0.0.4) Comments转换成text以符合调用方要求。 统计数据增加VerifyFlag = 1的条件, 增加CreatedOn,UpdatedOn字段的返回.(表中需要增加字段)
****************************************************************************/
BEGIN
  declare @SQLStr varchar(1000)
  set @ProdCode = isnull(@ProdCode, '')
  set @Language = isnull(@Language, '')
  select @OneStar = count(*) from CustomerComments where ProdCode = @ProdCode and CustomerRating = 1 and VerifyFlag = 1
  select @TwoStar = count(*) from CustomerComments where ProdCode = @ProdCode and CustomerRating = 2 and VerifyFlag = 1
  select @ThreeStar = count(*) from CustomerComments where ProdCode = @ProdCode and CustomerRating = 3 and VerifyFlag = 1
  select @FourStar = count(*) from CustomerComments where ProdCode = @ProdCode and CustomerRating = 4 and VerifyFlag = 1
  select @FiveStar = count(*) from CustomerComments where ProdCode = @ProdCode and CustomerRating = 5 and VerifyFlag = 1
  select @AVGStar =  Convert(decimal(10,1), AVG(cast(CustomerRating as decimal(10,1)))) from CustomerComments where ProdCode = @ProdCode and VerifyFlag = 1
  	  
  set @SQLStr = ' select KeyID, ProdCode, CustomerName, CustomerRating, cast(Comments as text) Comments, Language, '
     + ' Title,UploadPicFile1, UploadPicFile2, UploadPicFile3, UploadPicFile4, UploadPicFile5, CreatedOn,UpdatedOn '
     + ' from CustomerComments  '
	 + ' where VerifyFlag = 1 '  ---and (ProdCode = ''' + @ProdCode + ''')' 
	 + ' and ProdCode in (select ProdCode from Product_Style where ProdCodeStyle in (select ProdCodeStyle from Product_Style where ProdCode= ''' + @ProdCode + '''))'  
	 + ' and ([Language] = ''' + @Language + ''' or ''' + @Language + '''= '''') '
  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr	   
  return 0
END

GO
