USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductDepartments]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProductDepartments]
  @DepartCode            VARCHAR(64),			         -- 部门编码
  @IsIncludeChild        int,                      -- 0：不包括子部门。1：包括子部门。
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetProductDepartment
**  Version : 1.0.0.0
**  Description : 获得货品的部门数据
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = ''
  exec @a = GetProductDepartments @DepartCode, 1,'','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
  select * from buy_product
**  Created by Gavin @2015-03-06
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SET @DepartCode = ISNULL(@DepartCode, '')
  IF @DepartCode <> ''  
    SET @WhereStr = ' WHERE DepartCode=''' + @DepartCode + ''' '
  ELSE  
    SET @WhereStr = ''
    
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1
  
  SET @SQLStr = 'SELECT DepartCode, CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN DepartName2 WHEN 3 THEN DepartName3 ELSE DepartName1 END AS DepartName , '
    + ' CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN DepartPicFile2 WHEN 3 THEN DepartPicFile3 ELSE DepartPicFile END AS DepartPicFile, '
    + ' DepartNote, ReplenFormulaCode, FulfillmentHouseCode, NonOrder,NonSale,Consignment, WeightItem, DiscAllow, CouponAllow, '
    + ' VisuaItem, CouponSKU,BOM,MutexFlag,DiscountLimit,OnAccount, WarehouseCode, DefaultPickupStoreCode,Additional '
    + ' FROM BUY_DEPARTMENT '
    + @WhereStr

  EXEC SelectDataInBatchs @SQLStr, 'DepartCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr

  RETURN 0
END

GO
