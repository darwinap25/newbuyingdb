USE [NewBuying]
GO
/****** Object:  Table [dbo].[UserAirlineMap]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAirlineMap](
	[Id] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[AirlineCode] [nchar](10) NULL
) ON [PRIMARY]

GO
