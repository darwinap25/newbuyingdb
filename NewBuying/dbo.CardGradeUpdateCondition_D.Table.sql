USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGradeUpdateCondition_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardGradeUpdateCondition_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[UpdateConditionCode] [varchar](64) NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[ConditionType] [int] NOT NULL,
	[ConditionValue] [varchar](64) NOT NULL,
	[ConditionValidDuration] [int] NULL,
	[ConditionValidUnit] [int] NULL,
	[ConditionQty] [int] NULL,
	[ConditionAmount] [money] NULL,
 CONSTRAINT [PK_CARDGRADEUPDATECONDITION_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CardGradeUpdateCondition_D] ADD  DEFAULT ((1)) FOR [ConditionType]
GO
ALTER TABLE [dbo].[CardGradeUpdateCondition_D] ADD  DEFAULT ((0)) FOR [ConditionValidDuration]
GO
ALTER TABLE [dbo].[CardGradeUpdateCondition_D] ADD  DEFAULT ((0)) FOR [ConditionValidUnit]
GO
ALTER TABLE [dbo].[CardGradeUpdateCondition_D] ADD  DEFAULT ((1)) FOR [ConditionQty]
GO
ALTER TABLE [dbo].[CardGradeUpdateCondition_D] ADD  DEFAULT ((0)) FOR [ConditionAmount]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表主键主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'UpdateConditionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardGradeID（升级到此grade）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条件类型。 1：货品。  默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'ConditionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ConditionType=1时，此处填货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'ConditionValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表示此段时间内，此货品条件有效。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'ConditionValidDuration'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间类型：0：永久。 1：年。 2：月。 3：星期。 4：天。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'ConditionValidUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定条件的数量（未要求，预留）默认 1.  忽略' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'ConditionQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定条件的数量（未要求，预留）默认0 . 忽略' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D', @level2type=N'COLUMN',@level2name=N'ConditionAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'for Demo：2016-01-19
CardGrade升级时， 除了CardGrade中设置的积分要求， 还要考虑这个表中设置的货品要求：
会员必须在当日购买过所设置的货品，才允许升级。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeUpdateCondition_D'
GO
