USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetDepartment]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetDepartment]
  @DepartCode           varchar(64),       -- 部门编码，2位为1层。
  @BrandID              int,               -- 品牌ID
  @IsIncludeChild       int,               -- 0：不包括子部门。1：包括子部门。
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''     -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1    
AS
/****************************************************************************
**  Name : GetDepartment    
**  Version: 1.0.0.6
**  Description : 获得货品部门数据
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = null 
  exec @a = GetDepartment @DepartCode, null, 1,   1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
     
**  Created by: Gavin @2012-09-18
**  Modify by: Gavin @2012-09-26 (ver 1.0.0.1) @BrandCode 参数传入的是 BrandID, 参数名称暂时不变，修改处理过程
**  Modify by: Gavin @2012-10-10 (ver 1.0.0.2) 修改传入参数@BrandCode varchar(64) 为 @BrandID int
**  Modify by: Gavin @2013-05-23 (ver 1.0.0.3) department 增加图片字段,对应不同语言.
**  Modify by: Gavin @2014-01-09 (ver 1.0.0.4) 增加返回字段GenderFlag
**  Modify by: Gavin @2014-01-23 (ver 1.0.0.5) 增加返回字段departname1,departname2,departname3
**  Modify by: Gavin @2016-04-21 (ver 1.0.0.6) (for bauhaus) DepartPicFile, DepartPicFile2, DepartPicFile3 存放的是尺寸示意图，不分语言，全部返回。
**
****************************************************************************/
begin

  declare @SQLStr nvarchar(4000), @Language int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
     
  set @BrandID = isnull(@BrandID, 0)      
  
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))

  set @SQLStr = 'select DepartCode, case ' + cast(@Language as varchar) + ' when 2 then DepartName2 when 3 then DepartName3 else DepartName1 end as DepartName,'
        + ' case ' + cast(@Language as varchar) + ' when 2 then DepartPicFile2 when 3 then DepartPicFile3 else DepartPicFile end as DepartPicFile,'
 		+ 'GenderFlag, DepartName1, DepartName2, DepartName3, '
		+ ' DepartPicFile as DepartPicFile1, DepartPicFile2, DepartPicFile3 '
		+ ' from Department where '
    
  set @SQLStr = @SQLStr + ' (BrandID = ' + cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + ' = 0)' 
  if isnull(@IsIncludeChild, 0) = 0  
    set @SQLStr = @SQLStr + ' and (DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + '''='''') '
  else
    set @SQLStr = @SQLStr + ' and (DepartCode like ''' + @DepartCode + '%'' or ''' + @DepartCode + '''='''') '  
  
  exec SelectDataInBatchs @SQLStr, 'DepartCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', ''
 
  return 0
end

GO
