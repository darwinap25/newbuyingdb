USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckNumberMask]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[CheckNumberMask]
  @Type         int,               -- 0: CardTypre. 1: Cardgrade 2:CouponType
  @TypeID       int,               -- @TypeID内容根据@Type的设置. 新增时，可以填写0
  @NumMask      nvarchar(512),              -- 将要写入的Number Mask
  @NumPattern   nvarchar(512),               -- 将要写入的Number Pattern
  @ReturnTypeID int output         -- 正常返回0，有冲突返回冲突的ID，同样根据@Type区分
AS
/****************************************************************************
**  Name : CheckNumberMask
**  Version: 1.0.0.0
**  Description : 检查填入的Mask或者Pattern是否会导致号码重复
**
**  Parameter :
  declare @ReturnTypeID int, @a int
  exec @a = CheckNumberMask 2, 0, 'AAAA99999', '12255', @ReturnTypeID output
  print @a
  print @ReturnTypeID
select * from coupontype  
**  Return:  0: 允许。 -1：不允许
**  Created by: Gavin @2012-10-18
**
****************************************************************************/
begin
  declare @NumberMask  nvarchar(512), @NumberPattern  nvarchar(512), @nTypeID int
  Declare @MaskTable Table(TypeID int, NumberMask  nvarchar(512), NumberPattern  nvarchar(512))

  set @TypeID = isnull(@TypeID, 0)
  set @ReturnTypeID = 0
  
  if @Type = 0
  begin
    if @TypeID = 0
    begin
      insert into @MaskTable (TypeID, NumberMask, NumberPattern)
      select CardTypeID, RTrim(isnull(CardNumMask, '')), RTrim(isnull(CardNumPattern, '')) from CardType where isnull(CardNumMask, '') <> ''
    end else 
    begin
      insert into @MaskTable (TypeID, NumberMask, NumberPattern)
      select CardTypeID, RTrim(isnull(CardNumMask, '')), RTrim(isnull(CardNumPattern, '')) from CardType where CardTypeID <> @TypeID and isnull(CardNumMask, '') <> ''
    end  
  end else if @Type = 1
  begin
    if @TypeID = 0
    begin
      insert into @MaskTable (TypeID, NumberMask, NumberPattern)
      select CardGradeID, RTrim(isnull(CardNumMask, '')), RTrim(isnull(CardNumPattern, '')) from CardGrade where isnull(CardNumMask, '') <> ''
    end else 
    begin
      insert into @MaskTable (TypeID, NumberMask, NumberPattern)
      select CardGradeID, RTrim(isnull(CardNumMask, '')), RTrim(isnull(CardNumPattern, '')) from CardGrade where CardGradeID <> @TypeID and isnull(CardNumMask, '') <> ''
    end  
  end else if @Type = 2
  begin
    if @TypeID = 0
    begin
      insert into @MaskTable (TypeID, NumberMask, NumberPattern)
      select CouponTypeID, RTrim(isnull(CouponNumMask, '')), RTrim(isnull(CouponNumPattern, '')) from CouponType where isnull(CouponNumMask, '') <> ''
    end else 
    begin
      insert into @MaskTable (TypeID, NumberMask, NumberPattern)
      select CouponTypeID, RTrim(isnull(CouponNumMask, '')), RTrim(isnull(CouponNumPattern, '')) from CouponType where CouponTypeID <> @TypeID and isnull(CouponNumMask, '') <> ''
    end  
  end

--select * from @MaskTable
--print Len(@NumMask) 
--print @NumMask
--print @NumPattern
  select Top 1 @ReturnTypeID = TypeID from @MaskTable  
    where Len(NumberMask) = Len(@NumMask) and (NumberPattern like (@NumPattern + '%') or @NumPattern like (NumberPattern + '%'))
  if isnull(@ReturnTypeID, 0) = 0
    return 0 
  else
    return -1  
end

GO
