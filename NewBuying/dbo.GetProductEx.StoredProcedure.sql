USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductEx]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductEx]
  @MemberID             bigint,              -- MemberID
  @Barcode              varchar(64),       -- 条码
  @Prodcode             varchar(64),       -- 货品编码
  @DepartCode           varchar(64),       -- 部门编码
  @ProdCodeStyle	    varchar(64),       -- 货品系列编码。
  @IsIncludeChild       int,               -- 0：不包括部门子部门。1：包括部门子部门。  
  @BrandID              int,               -- 品牌ID
  @ProdType             int,               -- 货品类型
  @ProductColorID       int,               -- 货品颜色条件
  @ProductSizeID        int,			   -- 货品尺寸条件
  @ClassifyFilter       varchar(max),      -- 自定义货品分类过滤条件。格式：'school=1|aa=2|bb=3',多选时格式: 'school=1,2,3'
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @OrderCondition		nvarchar(1000),	   -- 自定义排序条件    
  @ReturnMode           int=0,             -- 0: 完整返回数据。 1：只返回部分字段。 默认0.  （目的：减少数据传输）
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''     -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1    
AS
/****************************************************************************
**  Name : GetProductEx
**  Version: 1.0.0.23
**  Description : 获得货品数据 (扩展)	   
**
  declare @a int, @PageCount int, @RecordCount int, @IsIncludeChild int
  declare @Barcode varchar(64), @Prodcode varchar(64), @DepartCode varchar(64),@ProdCodeStyle varchar(64), @BrandID int, @ProdType int
  set @Barcode = ''
  set @Prodcode = ''
  set @DepartCode = ''
  set @BrandID = 0
  set @ProdType = 0
  --set @ProdCodeStyle ='24286'
  --set @ProdCodeStyle ='LM04001'
  set @IsIncludeChild=1
  exec @a = GetProductEx 705, @Barcode,@Prodcode,@DepartCode,@ProdCodeStyle, @IsIncludeChild, @BrandID, @ProdType, 0, 0, 
  --N'<ROOT><FILTER TABLENAME= school  KEYID= schoolID  VALUEID= 1 /><FILTER TABLENAME= Region  KEYID= RegionID  VALUEID= 2 /></ROOT>', 
  N'',
  '', '', 0, 0, 100, @PageCount output, @RecordCount output, 'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount

**  Created by: Gavin @2013-07-29
**  Modified by: Gavin @2013-12-17 (ver 1.0.0.1) 要确保product_price中取出的记录，每个货品只有一个价格，否则join product 后记录会重复
**  Modified by: Gavin @2013-12-20 (ver 1.0.0.2) 增加输入参数MemberID，增加收藏和购物车的返回标志. 没有MemberID时，这两个标志返回0
**  Modified by: Gavin @2013-12-26 (ver 1.0.0.3) 修改收藏和购物车的返回标志的内容。 0 表示false， 非0时，表示对应表的KeyID值。
**  Modified by: Robin @2013-12-27 (ver 1.0.0.4) 修改取货品价格逻辑，取Product_Price表中，一个货品的在时间范围内的KeyID最大的价格作为有效价格
**  Modified by: Gavin @2014-01-09 (ver 1.0.0.5) 增加返回字段ProductPriority
**  Modified by: Gavin @2014-01-23 (ver 1.0.0.6) 修改@ClassifyFilter 多选情况的格式。
**  Modified by: Gavin @2014-01-27 (ver 1.0.0.7) 由于存在一个货品 多个barcode的情况, 放在过程返回一个货品多条记录的情况,取消join barcode表,不返回货品的barcode,只作为查询条件
**  Modified by: Gavin @2015-08-20 (ver 1.0.0.8) 因为MemberFavorite增加prodcode字段，关联字段要改成prodcode
**  Modified by: Gavin @2016-02-23 (ver 1.0.0.9) (for Bauhaus) 读取CustomerComments 中的 rate (取平均值)
**  Modified by: Gavin @2016-04-07 (ver 1.0.0.10) (for Bauhaus) 增加返回 Sex, season 
**  Modified by: Gavin @2016-04-21 (ver 1.0.0.11) (for Bauhaus) 平均数 CustomerRating ，需要变成 浮点数。 （avg函数，浮点数计算出来还是浮点数，整数计算出来还是整数）
**  Modified by: Gavin @2016-04-21 (ver 1.0.0.12) (for Bauhaus) 增加返回barcode。  barcode 和 prodcode 必须 1对1， 如果多个barcode对一个prodcode，数据将会重复
**  Modified by: Gavin @2016-04-28 (ver 1.0.0.13) (for Bauhaus) bugfix 
**  Modified by: Gavin @2016-06-24 (ver 1.0.0.14) (for Bauhaus) 增加返回 Material, overview, style, fabric_care,删除一些不需要的返回字段
**  Modified by: Gavin @2016-07-15 (ver 1.0.0.15) (for Bauhaus) 去掉OnhandQty
**  Modified by: Gavin @2016-08-08 (ver 1.0.0.16) 默认查询结果是 IsOnlineSKU=1
**  Modified by: Gavin @2016-08-16 (ver 1.0.0.17) 增加返回ProdCodeStyle, (要求一个product必须只有一个prodcodestyle)
**  Modified by: Gavin @2016-09-01 (ver 1.0.0.18) 增加返回ProductSizeType,  ProductSizeCode + ProductSizeType 才能定位唯一size.
**  Modified by: Gavin @2016-10-20 (ver 1.0.0.19) 增加返回ProductType. (判别这个货品是否需要扣库存.)
**  Modified by: Gavin @2016-11-21 (ver 1.0.0.20) 输入参数@MemberID从 INT 改为 BigInt
**  Modified by: Thomas @2017-04-06 (ver 1.0.0.21) java error found, fix by changing text type to ntext type, example :cast(A.Memo1 as ntext) 
**  Modified by: Gavin @2017-04-25 (ver 1.0.0.22) ProductBrandDesc1 要求转成 ntext
**  Modified by: Gavin @2017-05-19 (ver 1.0.0.23) SQL语句超长出错，缩减语句长度....
**
sp_helptext GetProductEx
select * from CustomerComments
****************************************************************************/
begin

  declare @SQLStr nvarchar(4000), @Language int, @ForeignTable varchar(64), @ForeignID varchar(100)
  DECLARE @idoc int  
  declare @FilterTable Table(TABLENAME varchar(512),VALUEID int)

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  set @BrandID = isnull(@BrandID, 0)    
  set @MemberID = isnull(@MemberID, 0)
  set @Barcode = isnull(@Barcode, '')
  set @Prodcode = isnull(@Prodcode, '')
  set @DepartCode = isnull(@DepartCode, '')
  set @ProdType = isnull(@ProdType, 0)
  set @IsIncludeChild = isnull(@IsIncludeChild, 0)
  set @ProdCodeStyle = isnull(@ProdCodeStyle, '')  
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))
  set @ProductColorID = isnull(@ProductColorID, 0)
  set @ProductSizeID = isnull(@ProductSizeID, 0)          
							   
  set @SQLStr = 'select T.Barcode,P.ProdCode,D.DepartCode,D.BrandID,P.ProdType,P.ProductBrandID,P.PackQty, P.OriginID,'
    + 'P.ColorID,P.NonSale,C.RGB ColorCode,C.ColorPicFile,P.NewFlag,P.HotSaleFlag,'	
	+ 'case ' + cast(@Language as varchar) + ' when 2 then ColorName2 when 3 then ColorName3 else ColorName1 end ColorName,'	     
	+ 'case ' + cast(@Language as varchar) + ' when 2 then ProductSizeName2 when 3 then ProductSizeName3 else ProductSizeName1 end ProductSizeName,'    
	+ 'R.ProdPriceType, R.NetPrice, R.DefaultPrice, Z.ProductSizeID, Z.ProductSizeCode, ' --Q.OnhandQty, '
	+ 'isnull(X.KeyID,0) IsShoppingCart, '
	+ 'isnull(Y.KeyID,0) IsFavorite, A.ProductPriority'	
  if @ReturnMode = 0 
  begin
	set @SQLStr = @SQLStr + ',' 
--	+ '  case ' + cast(@Language as varchar) + ' when 2 then NationName2 when 3 then NationName3 else NationName1 end as NationName, '
    + 'case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end ProdName ,'
	+ 'case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end DepartName,'
	+ 'Z.ProductSizeNote, P.ProdPicFile, P.ProdNote,'
	+ 'case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end ProductBrandName,'
	+ 'case ' + cast(@Language as varchar) + ' when 2 then cast(B.ProductBrandDesc2 as ntext)  when 3 then cast(B.ProductBrandDesc3 as ntext) else cast(B.ProductBrandDesc1 as ntext) end ProductBrandDesc,'
	+ 'ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile,'
--	+ '  A.ProdFunction, A.ProdIngredients, A.ProdInstructions, A.PackDesc, A.PackUnit, '
--	+ '  A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6, A.MemoTitle1, A.MemoTitle2, A.MemoTitle3, A.MemoTitle4, A.MemoTitle5, A.MemoTitle6, '	
	+ 'P.Flag1, P.Flag2,'
--	+ ' P.Flag3, P.Flag4, P.Flag5, N.NationFlagFile, '
	+ 'CR.CustomerRating, SE.Sex, SC.Season,'
--    + '  DD.Material, DD.overview, DD.style, DD.fabric_care, W.ProdCodeStyle, isnull(CC.CRating, 0) as CRating '
	+ 'cast(A.Memo1 as ntext) Material,cast(A.Memo2 as ntext) overview, cast(A.Memo3 as ntext) style,cast(A.Memo4 as ntext) fabric_care,W.ProdCodeStyle,isnull(CC.CRating, 0) CRating,'
	+ 'Z.ProductSizeType,P.ProdType as ProductType'
  end	
  set @SQLStr = @SQLStr + ' from Product P '
          + ' left join (select distinct Barcode, ProdCode from ProductBarcode) T on T.ProdCode=P.ProdCode '    
          --+ ' left join (select prodcode, max(ProdPriceType) as ProdPriceType, max(NetPrice) as NetPrice, max(DefaultPrice) as DefaultPrice from Product_Price where status = 1 group by prodcode) R on P.ProdCode = R.ProdCode '    
          +' left join (SELECT ProdCode, ProdPriceType, NetPrice, DefaultPrice FROM Product_Price where KeyID in (SELECT MAX(KeyID) KeyID FROM Product_Price WHERE (StartDate<=GETDATE()) AND (EndDate+1>=GETDATE()) group by ProdCode)) R on P.ProdCode=R.ProdCode'
          + ' left join (select * from Product_Particulars where LanguageID=' + cast(@Language as varchar) + ') A on P.ProdCode=A.Prodcode '
          + ' left join Product_Brand B on P.ProductBrandID=B.ProductBrandID '
          + ' left join Nation N on P.OriginID=N.NationID '         
          + ' left join Color C on P.ColorCode=C.ColorCode '
          + ' left join Product_Size Z on P.ProductSizeID=Z.ProductSizeID '
--		  + ' left join Department D on D.DepartCode = P.DepartCode '  
--		  + ' left join ProductStockOnhand Q on P.ProdCode = Q.ProdCode '

		  + ' left join (select KeyID, ProdCode from MemberShoppingCart where MemberID=' + cast(@MemberID as varchar) + ') X on P.ProdCode=X.ProdCode'
		  + ' left join Product_Style W on P.ProdCode = W.ProdCode '
		  + ' left join (select KeyID, ProdCodeStyle, ProdCode from MemberFavorite where MemberID=' + cast(@MemberID as varchar) + ') Y on P.ProdCode=Y.ProdCode'
		  + ' left join (select ProdCode, avg(cast(CustomerRating as float)) CustomerRating from CustomerComments Group By ProdCode) CR on P.ProdCode=CR.ProdCode' 
		  + ' left join (select case ' + cast(@Language as varchar) + ' when 2 then GenderDesc2 when 3 then GenderDesc3 else GenderDesc1 end as Sex, ProdCode from '
	                   + ' (select distinct ProdCode, ForeignKeyID from Product_Classify where ForeignTable=''Gender'') A left join Gender B on A.ForeignKeyID=B.GenderID) SE on SE.ProdCode=P.ProdCode' 	
		  + ' left join (select case ' + cast(@Language as varchar) + ' when 2 then SeasonName2 when 3 then SeasonName3 else SeasonName1 end Season, ProdCode from '
		               + ' (select distinct ProdCode, ForeignKeyID from Product_Classify where ForeignTable=''Season'') A left join Season B on A.ForeignKeyID=B.SeasonID) SC on SC.ProdCode=P.ProdCode'
--          + ' left join PRODUCT_ADD_BAU DD on DD.ProdCode = P.ProdCode '
		  + ' left join (select ProdCode, cast(avg(cast(CustomerRating as decimal(10,1))) as decimal(10,1)) CRating from CustomerComments group by ProdCode ) CC on CC.ProdCode=P.ProdCode' 		   
			   					
  if exists(select top 1 * from Product_Catalog) and @DepartCode <> ''         
    set @SQLStr = @SQLStr + ' left join Department D on D.DepartCode=''' + @DepartCode + ''''
  else  
    set @SQLStr = @SQLStr + ' left join Department D on D.DepartCode=P.DepartCode '  
    		    
--  set @SQLStr = @SQLStr + ' where (T.Barcode = ''' + @Barcode + ''' or ''' + @Barcode + ''' = '''')'
  set @SQLStr = @SQLStr + ' where P.IsOnlineSKU=1 and '
     + ' (P.Prodcode in (select ProdCode from ProductBarcode where Barcode=''' + @Barcode + ''') or ''' + @Barcode + '''='''')'
     + ' and (P.Prodcode=''' + @Prodcode + ''' or ''' + @Prodcode + '''='''')'
     + ' and (D.BrandID=' + cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + '=0)'
     + ' and (P.ProdType=' + cast(@ProdType as varchar) + ' or ' + cast(@ProdType as varchar) + '=0)'
     + ' and (P.ColorID=' + cast(@ProductColorID as varchar) + ' or ' + cast(@ProductColorID as varchar) + '=0)'
     + ' and (P.ProductSizeID=' + cast(@ProductSizeID as varchar) + ' or ' + cast(@ProductSizeID as varchar) + '=0)'   
	 + ' and (T.Barcode=''' + @Barcode + ''' or ''' + @Barcode + '''='''')'
     
  if exists(select top 1 * from Product_Catalog) and @DepartCode <> ''   
  begin
    if @IsIncludeChild = 0
	  set @SQLStr = @SQLStr + ' and (P.Prodcode in (select ProdCode from Product_Catalog where DepartCode=''' + @DepartCode + '''))'
	else  
	  set @SQLStr = @SQLStr + ' and (P.Prodcode in (select ProdCode from Product_Catalog where DepartCode like ''' + @DepartCode + '%''))'
  end else
  begin
    if @IsIncludeChild = 0
      set @SQLStr = @SQLStr + ' and (P.DepartCode=''' + @DepartCode + ''' or ''' + @DepartCode + '''='''')'  
    else  
	  set @SQLStr = @SQLStr + ' and (P.DepartCode like ''' + @DepartCode + '%'' or ''' + @DepartCode + '''='''') '
  end          

  if @ProdCodeStyle <> ''
	set @SQLStr = @SQLStr + ' and (P.Prodcode in (select Prodcode from Product_Style where ProdCodeStyle=''' + @ProdCodeStyle + '''))'

  if @ClassifyFilter <> ''
  begin
    declare @TempStr varchar(max), @POS int, @ConditionStr varchar(200)
    set @TempStr = RTrim(LTrim(@ClassifyFilter))
    while len(@TempStr) > 0
    begin
	  set @POS = CharIndex('|', @TempStr)
	  if @POS > 0 
	  begin
	    set @ConditionStr = substring(@TempStr, 1, @POS - 1)
		set @TempStr = substring(@TempStr, @POS + 1, len(@TempStr) - @POS)
	  end  else
	  begin
	    set @ConditionStr = @TempStr
		set @TempStr = ''
	  end
	  if len(@ConditionStr) > 0
	  begin
		set @POS = CharIndex('=', @ConditionStr)
		set @ForeignTable = substring(@ConditionStr, 1, @POS - 1)
		set @ForeignID = substring(@ConditionStr, @POS + 1, len(@ConditionStr) - @POS)
		set @SQLStr = @SQLStr + ' and (P.Prodcode in (select Prodcode from Product_Classify where ForeignTable=''' + @ForeignTable + ''' and ForeignkeyID in (' + @ForeignID + ')))'     
	  end
    end
  end  
  	
/*
  if @ClassifyFilter <> ''
  begin
    EXEC sp_xml_preparedocument @idoc OUTPUT, @ClassifyFilter 
    insert into @FilterTable
    SELECT TABLENAME, VALUEID FROM OPENXML (@idoc, '/ROOT/FILTER', 1) WITH (TABLENAME varchar(512), VALUEID int)
         
    declare CUR_Foreign cursor fast_forward local for 
      select TABLENAME, VALUEID from @FilterTable
    Open CUR_Foreign  
    Fetch From CUR_Foreign INTO @ForeignTable, @ForeignID
    WHILE @@FETCH_STATUS=0      
    BEGIN          
      set @SQLStr = @SQLStr + ' and (P.Prodcode in (select Prodcode from Product_Classify where ForeignTable = ''' + @ForeignTable + ''' and ForeignkeyID = ' + cast(@ForeignID as varchar) + '))'     
      Fetch From CUR_Foreign INTO @ForeignTable, @ForeignID
    end  
    CLOSE CUR_Foreign  
    Deallocate CUR_Foreign      
  end 
*/
print @SQLStr
  exec SelectDataInBatchs_New @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @QueryCondition

  return 0
end

GO
