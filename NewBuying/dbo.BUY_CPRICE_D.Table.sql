USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_CPRICE_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_CPRICE_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CPriceCode] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[CPriceGrossCost] [dbo].[Buy_Amt] NOT NULL,
	[CPriceNetCost] [dbo].[Buy_Amt] NOT NULL,
	[CPriceDisc1] [dbo].[Buy_Amt] NULL,
	[CPriceDisc2] [dbo].[Buy_Amt] NULL,
	[CPriceDisc3] [dbo].[Buy_Amt] NULL,
	[CPriceDisc4] [dbo].[Buy_Amt] NULL,
	[CPriceBuy] [dbo].[Buy_Qty] NULL,
	[CPriceGet] [dbo].[Buy_Qty] NULL,
 CONSTRAINT [PK_BUY_CPRICE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_CPRICE_D] ADD  DEFAULT ((0)) FOR [CPriceDisc1]
GO
ALTER TABLE [dbo].[BUY_CPRICE_D] ADD  DEFAULT ((0)) FOR [CPriceDisc2]
GO
ALTER TABLE [dbo].[BUY_CPRICE_D] ADD  DEFAULT ((0)) FOR [CPriceDisc3]
GO
ALTER TABLE [dbo].[BUY_CPRICE_D] ADD  DEFAULT ((0)) FOR [CPriceDisc4]
GO
ALTER TABLE [dbo].[BUY_CPRICE_D] ADD  DEFAULT ((0)) FOR [CPriceBuy]
GO
ALTER TABLE [dbo].[BUY_CPRICE_D] ADD  DEFAULT ((0)) FOR [CPriceGet]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'毛成本价。不包含所有折扣' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceGrossCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'净成本价。包含所有折扣' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceNetCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易折扣。（Transaction Discount）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceDisc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他折扣。（Other Discount）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceDisc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'特别折扣。（Special Discount）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceDisc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣4。（Discount 4）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceDisc4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How many to buy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceBuy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How many to get free' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D', @level2type=N'COLUMN',@level2name=N'CPriceGet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成本价子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_D'
GO
