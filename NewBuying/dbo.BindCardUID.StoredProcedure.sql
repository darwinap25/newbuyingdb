USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BindCardUID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BindCardUID]
AS
/****************************************************************************
**  Name : BindCardUID
**  Version: 1.0.0.0
**  Description :  绑定Card UID 和 CardNumber
**  Created by:  Gavin @2017-01-16
****************************************************************************/
BEGIN
  DECLARE @NewBatchCode VARCHAR(64), @QTY INT, @CardGradeID INT, @CardTypeID INT, @NewBatchID INT
  DECLARE @ImportCardNumber VARCHAR(64) 

  SET @ImportCardNumber = CONVERT(VARCHAR(10), GETDATE(), 112)

  DECLARE CUR_BINDCARD Cursor fast_forward FOR
    SELECT T.CardTypeID, G.CardGradeID, COUNT(*)
	FROM CardImport I 
	  LEFT JOIN CARDTYPE T ON I.CardTypeCode = T.CardTypeCode 
	  LEFT JOIN CARDGRADE G ON I.CardGradeCode = G.CardGradeCode 
	 WHERE I.Status = 0
	GROUP BY T.CardTypeID, G.CardGradeID
  OPEN CUR_BINDCARD
  FETCH FROM CUR_BINDCARD INTO @CardTypeID, @CardGradeID, @QTY
  WHILE @@FETCH_STATUS = 0
  BEGIN
    EXEC GetRefNoString 'BTHCAD', @NewBatchCode output
	INSERT INTO BatchCard
      (BatchCardCode, SeqFrom, SeqTo, Qty, CardGradeID, CreatedOn, UpdatedOn, CreatedBy, UpdatedBy)
    VALUES (@NewBatchCode, 1, 1, @QTY, @CardGradeID, getdate(), getdate(), 1, 1)  
    SET @NewBatchID = SCOPE_IDENTITY()    

    INSERT INTO Card (CardNumber, CardTypeID, CardIssueDate, CardExpiryDate, MemberID, Status, UsedCount, CardGradeID,
         BatchCardID, TotalPoints, TotalAmount, CardPassword, StockStatus)
    SELECT CardImportNumber, @CardTypeID, GETDATE(), '2100-01-01', NULL, Status, 1, @CardGradeID,
         @NewBatchID, 0, 0, '', 0
    FROM CardImport WHERE Status = 0
	  
    INSERT INTO Card_Movement (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, ApprovalCode, OrgStatus, NewStatus,
         OpenPoint, ClosePoint, StoreID)
    SELECT 0, CardImportNumber, null, null, '', 0, 0, 0, 0, GETDATE(), GETDATE(),NULL, 
	     null, null, '', '', '', 1, '', 0, 0,
         0, 0, null
    FROM CardImport WHERE Status = 0
		 
	INSERT INTO CardUIDMap(CardUID, ImportCardNumber, BatchCardID, CardGradeID, CardNumber, Status, CreatedOn, CreatedBy)	     
    SELECT CardBarcode, @ImportCardNumber, @NewBatchID, @CardGradeID, 1, CardImportNumber, GETDATE(), 1 FROM CardImport WHERE Status = 0

	UPDATE CardImport SET Status =  1 WHERE Status = 0
  END
END

GO
