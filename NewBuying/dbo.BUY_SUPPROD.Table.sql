USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_SUPPROD]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_SUPPROD](
	[VendorCode] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[SUPPLIER_PRODUCT_CODE] [varchar](64) NULL,
	[in_tax] [money] NULL,
	[Prefer] [int] NULL DEFAULT ((0)),
	[IsDefault] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_BUY_SUPPROD] PRIMARY KEY CLUSTERED 
(
	[VendorCode] ASC,
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SUPPROD', @level2type=N'COLUMN',@level2name=N'VendorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SUPPROD', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商的货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SUPPROD', @level2type=N'COLUMN',@level2name=N'SUPPLIER_PRODUCT_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'税' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SUPPROD', @level2type=N'COLUMN',@level2name=N'in_tax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先权。 0 ：最低。依次增高。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SUPPROD', @level2type=N'COLUMN',@level2name=N'Prefer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否默认供应商. 0：不是。1:是的。 默认 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SUPPROD', @level2type=N'COLUMN',@level2name=N'IsDefault'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商与货品的关联表
@2016-02-26' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SUPPROD'
GO
