USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCardRecharge_Purchase_HQ]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoCardRecharge_Purchase_HQ]
  @UserID                   int,             --操作员ID
  @CardReceiveNumber        varchar(64),     --单号 
  @IsVoid  int = 0
AS
/****************************************************************************
**  Name : DoCardRecharge_Purchase_HQ
**  Version: 1.0.0.3
**  Description : 按订单充值(总部向供应商) (for RRG  暂定)

    exec DoCardRechange
    exec DoCardRecharge_Purchase_HQ
    SELECT * FROM Ord_OrderToSupplier_Card_D
** 
**  Created by:  Gavin @2015-02-11
**  Modify by:  Gavin @2015-04-08 (ver 1.0.0.1) 部分收货
**  Modify by:  Gavin @2015-05-08 (ver 1.0.0.2) 修正部分收货bug
**  Modify by:  Gavin @2015-06-04 (ver 1.0.0.3) 增加校验,如果收货单中的卡不存在,则返回错误. trigger将会raise error
****************************************************************************/
begin
  declare @BusDate datetime, @TxnDate datetime
  declare @FirstNumber varchar(64), @EndNumber varchar(64), @CardTypeID int, @CardGradeID int, @CardCount int
  declare @IssuedDate datetime, @InitAmount money, @InitPoints int, @RandomPWD int, @InitPassword varchar(512), 
          @ExpiryDate datetime, @ApprovalCode varchar(512), @ReturnBatchID varchar(30), 
          @ReturnStartNumber varchar(64), @ReturnEndNumber varchar(64), @InitStatus int, @StoreID int
  declare @OrderAmount money, @OrderPoint int , @Sign int, @NewNumber varchar(64)
   
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()  
  if ISNULL(@IsVoid, 0) = 1
    set @Sign = -1  
  else
    set @Sign = 1
  
  -- ver 1.0.0.3  
  if exists(select * from Ord_CardReceive_D D left join Card C on D.FirstCardNumber = C.CardNumber 
              where D.CardReceiveNumber = @CardReceiveNumber and C.CardNumber is null)
    return -1
                  
    insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus, ApprovalCode)
	  select 51, C.CardNumber, null, 0, D.CardReceiveNumber, C.TotalAmount, @Sign * D.ActualAmount, C.TotalAmount + (@Sign * D.ActualAmount), 0, @BusDate, @Txndate, 
	      null, null, null, null, '', @UserID, H.StoreID, '', '',
	      C.TotalPoints, C.TotalPoints, C.CardExpiryDate, C.CardExpiryDate, C.Status, C.Status, ''
	    from Ord_CardReceive_D D
	      left join Ord_CardReceive_H H on D.CardReceiveNumber = H.CardReceiveNumber
	      left join Card C on D.FirstCardNumber = C.CardNumber
	   where H.PurchaseType = 2 and H.CardReceiveNumber = @CardReceiveNumber and D.ActualAmount > 0
	   
	   
  -- 产生收货单。
  if exists(select * from Ord_CardReceive_D D
	            left join Ord_CardReceive_H H on D.CardReceiveNumber = H.CardReceiveNumber
	            left join Card C on D.FirstCardNumber = C.CardNumber
	          where H.PurchaseType = 2 and H.CardReceiveNumber = @CardReceiveNumber and D.ActualAmount > 0 and D.ActualAmount <> isnull(D.OrderAmount,0))
	begin          
    exec GetRefNoString 'CRECCA', @NewNumber output     
    insert into Ord_CardReceive_H(CardReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ReceiveType, ordertype, PurchaseType)
    select @NewNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, 1, ordertype, 2
     from Ord_CardReceive_H where CardReceiveNumber = @CardReceiveNumber and PurchaseType = 2

    insert into Ord_CardReceive_D (CardReceiveNumber, CardTypeID, CardGradeID, Description, OrderQty, ActualQty, 
          FirstCardNumber, EndCardNumber, BatchCardCode, CardStockStatus, ReceiveDateTime, OrderAmount, ActualAmount, OrderPoint, ActualPoint)  
    select @NewNumber, CardTypeID, CardGradeID, '', 0, 0,  
         FirstCardNumber, EndCardNumber, BatchCardCode, 1, GETDATE(), isnull(D.OrderAmount,0) - isnull(D.ActualAmount,0), isnull(D.OrderAmount,0) - isnull(D.ActualAmount,0), 0, 0
        from Ord_CardReceive_D D
         left join Ord_CardReceive_H H on H.CardReceiveNumber = D.CardReceiveNumber
      where D.CardReceiveNumber = @CardReceiveNumber and H.PurchaseType = 2 and D.ActualAmount > 0 and D.ActualAmount <> isnull(D.OrderAmount,0)
  end 
  
  return 0   	   
end

GO
