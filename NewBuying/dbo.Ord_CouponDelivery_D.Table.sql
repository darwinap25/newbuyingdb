USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponDelivery_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponDelivery_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponDeliveryNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NULL,
	[Description] [nvarchar](512) NULL,
	[OrderQTY] [int] NULL,
	[PickQTY] [int] NULL,
	[ActualQTY] [int] NULL,
	[FirstCouponNumber] [varchar](64) NULL,
	[EndCouponNumber] [varchar](64) NULL,
	[BatchCouponCode] [varchar](64) NULL,
 CONSTRAINT [PK_ORD_COUPONDELIVERY_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'CouponDeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'OrderQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要求拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'PickQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'ActualQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的首coupon号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'FirstCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的末coupon号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'EndCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstCouponNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D', @level2type=N'COLUMN',@level2name=N'BatchCouponCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵送货单子表
根据 Ord_CouponPicking_D产生，过滤其中的实际拣货数量为0的记录。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponDelivery_D'
GO
