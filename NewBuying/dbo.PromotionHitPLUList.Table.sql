USE [NewBuying]
GO
/****** Object:  Table [dbo].[PromotionHitPLUList]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PromotionHitPLUList](
	[PromotionCode] [varchar](64) NOT NULL,
	[HitSeq] [int] NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[DepartCode] [varchar](64) NULL,
 CONSTRAINT [PK_PROMOTIONHITPLULIST] PRIMARY KEY CLUSTERED 
(
	[PromotionCode] ASC,
	[HitSeq] ASC,
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionHitPLUList', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promotion_Hit表序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionHitPLUList', @level2type=N'COLUMN',@level2name=N'HitSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionHitPLUList', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionHitPLUList', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销Hit 条件匹配的 货品列表。
根据Promotion_hit_Plu 来展开' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionHitPLUList'
GO
