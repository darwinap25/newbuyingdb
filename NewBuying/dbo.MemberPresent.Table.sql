USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberPresent]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberPresent](
	[MemberPresentID] [int] IDENTITY(1,1) NOT NULL,
	[CardNumber] [varchar](64) NULL,
	[ReceiveCardNumber] [varchar](64) NULL,
	[ActAmount] [money] NULL,
	[ActPoints] [int] NULL,
	[ActCouponNumbers] [varchar](max) NULL,
	[Status] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[StoreID] [int] NULL,
	[RegisterCode] [varchar](64) NULL,
	[ServerCode] [varchar](64) NULL,
	[ActionDate] [datetime] NULL,
	[RefTxnNo] [varchar](512) NULL,
	[BusDate] [datetime] NULL,
	[Txndate] [datetime] NULL,
	[ReceiveMobileNumber] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [varchar](512) NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [varchar](512) NULL,
 CONSTRAINT [PK_MEMBERPRESENT] PRIMARY KEY CLUSTERED 
(
	[MemberPresentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Insert_MemberPresent]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_MemberPresent] ON [dbo].[MemberPresent]
For INSERT, Update
AS
/*================================================================================*/
/*                
* Name: Insert_MemberPresent
* Version: 1.0.0.0
* Description :
*        触发器操作，在写入数据前校验， 触发器中不做校验
* Create By Gavin @2012-05-10
*/
/*================================================================================*/
begin
  declare @OldSTATUS int, @NewStatus int, @OprID int, @OprID_1 int, @SQLStr nvarchar(4000),  @SQLStr1 nvarchar(4000)
  declare @UserID varchar(512), @CardNumber varchar(512), @ReceiveCardNumber	varchar(512), @ActAmount money, @ActPoints int, @ActCouponNumbers varchar(max),
          @ActionDate datetime, @Remark nvarchar(512), @StoreID int, @RegisterCode varchar(512), @ServerCode varchar(512)
  declare @CardTypeID int
  declare @SourceTotalAmt money, @SourceTotalPoints money, @ReceiveTotalAmt money, @ReceiveTotalPoints int, @MaxAmt money
  declare @CouponNumber varchar(512), @CouponTypeID int, @TxnNo varchar(30), @BusDate datetime, @Txndate datetime
  declare @CouponStauts int, @CouponExpiryDate datetime
          
  select @UserID = CreatedBy, @CardNumber = CardNumber, @ReceiveCardNumber = ReceiveCardNumber, @ActAmount = ActAmount, @ActPoints = ActPoints, @ActCouponNumbers = ActCouponNumbers,
         @ActionDate = ActionDate, @NewStatus = Status, @Remark = Remark, @StoreID = StoreID, @RegisterCode = RegisterCode, @ServerCode = ServerCode, --@TxnNo = convert(varchar(30), memberpresentid),
         @TxnNo = RefTxnNo, @Busdate = Busdate, @TxnDate = TxnDate         
   from inserted
  select @OldSTATUS = Status from Deleted
 
  select @SourceTotalAmt = TotalAmount, @SourceTotalPoints = TotalPoints, @CardTypeID = CardTypeID from Card where CardNumber = @CardNumber
  select @ReceiveTotalAmt = TotalAmount, @ReceiveTotalPoints = TotalPoints from Card where CardNumber = @ReceiveCardNumber
  
  --UPDATE(Status)
  
  -- 调用方只能选择转赠金额或者积分或者Coupon中任意一个，不能同时填写。
  if isnull(@ActCouponNumbers, '') <> ''
    set @OprID = 36
  else set @OprID = 6  
  set @OprID_1 = @OprID + 1
    
  -- 转出操作
  IF ((@NewStatus = 1) AND (isnull(@OldSTATUS,0) = 0)) or ((@NewStatus = 2) AND (isnull(@OldSTATUS,0) = 0))
  begin
    -- 扣除原卡
    if @OprID in (6,16)
    begin
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode)
      values
        (@OprID, @CardNumber, null, 0, @TxnNo, @SourceTotalAmt, -abs(@ActAmount), @SourceTotalAmt - abs(@ActAmount), -abs(@ActPoints), @BusDate, @TxnDate, 
         null, null, null, @Remark, '', @UserID, @StoreID, @RegisterCode, @ServerCode)
    end 
    if @OprID = 36
    begin
      if object_id('TempDB..##CouponNumberList') is not null
        drop table ##CouponNumberList   
      set @SQLStr = N'select CouponNumber, CouponTypeID, CouponExpiryDate, status into ##CouponNumberList from coupon where CouponNumber in ( ' + @ActCouponNumbers + ')'
      set @SQLStr1 = @SQLStr    
      exec sp_executesql @SQLStr1    
            
      DECLARE CUR_HolidayTransferCoupon CURSOR fast_forward local FOR
        select CouponNumber, CouponTypeID, CouponExpiryDate, status from ##CouponNumberList      
      OPEN CUR_HolidayTransferCoupon
      FETCH FROM CUR_HolidayTransferCoupon INTO @CouponNumber, @CouponTypeID, @CouponExpiryDate, @CouponStauts
      WHILE @@FETCH_STATUS=0
      BEGIN
        -- 解除指定的Coupon的绑定
        insert into Coupon_Movement
          (OprID, CardNumber,CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
           BusDate, Txndate, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
        values
          (@OprID, @CardNumber, @CouponNumber, @CouponTypeID, '', 0, @TxnNo, 0, 0, 0,
           @BusDate, @TxnDate, @Remark,'', @UserID, @StoreID, @RegisterCode, @ServerCode, @CouponExpiryDate, @CouponExpiryDate, @CouponStauts, @CouponStauts)
        FETCH FROM CUR_HolidayTransferCoupon INTO @CouponNumber, @CouponTypeID, @CouponExpiryDate, @CouponStauts     
      END
      CLOSE CUR_HolidayTransferCoupon 
      DEALLOCATE CUR_HolidayTransferCoupon        
      DROP table ##CouponNumberList    
    end         
  end
  -- 转入操作
  IF (@NewStatus = 2) AND (isnull(@OldSTATUS,0) = 0 or @OldSTATUS = 1)
  begin
    if @OprID in (6,16)
    begin
      -- 加入目的卡
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode)
      values
        (@OprID_1, @ReceiveCardNumber, null, 0, @TxnNo, @ReceiveTotalAmt, abs(@ActAmount), @ReceiveTotalAmt + abs(@ActAmount), abs(@ActPoints), getdate(), getdate(), 
         null, null, null, @Remark, '', @UserID, @StoreID, @RegisterCode, @ServerCode)      
    end  
    if @OprID = 36
    begin
      if object_id('TempDB..##CouponNumberList') is not null
        drop table ##CouponNumberList   
      set @SQLStr = N'select CouponNumber, CouponTypeID, CouponExpiryDate, status into ##CouponNumberList from coupon where CouponNumber in ( ' + @ActCouponNumbers + ')'
      set @SQLStr1 = @SQLStr    
      exec sp_executesql @SQLStr1    
            
      DECLARE CUR_HolidayTransferCoupon CURSOR fast_forward local FOR
        select CouponNumber, CouponTypeID, CouponExpiryDate, status from ##CouponNumberList      
      OPEN CUR_HolidayTransferCoupon
      FETCH FROM CUR_HolidayTransferCoupon INTO @CouponNumber, @CouponTypeID, @CouponExpiryDate, @CouponStauts
      WHILE @@FETCH_STATUS=0
      BEGIN
        -- 绑定coupon      
        insert into Coupon_Movement
          (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
           BusDate, Txndate, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
        values
          (@OprID_1, @ReceiveCardNumber, @CouponNumber, @CouponTypeID, '', 0, @TxnNo, 0, 0, 0,
           @BusDate, @TxnDate, @Remark,'', @UserID, @StoreID, @RegisterCode, @ServerCode, @CouponExpiryDate, @CouponExpiryDate, @CouponStauts, @CouponStauts)    
        FETCH FROM CUR_HolidayTransferCoupon INTO @CouponNumber, @CouponTypeID, @CouponExpiryDate, @CouponStauts     
      END
      CLOSE CUR_HolidayTransferCoupon 
      DEALLOCATE CUR_HolidayTransferCoupon        
      DROP table ##CouponNumberList        
    end        
  end  
end

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'MemberPresentID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赠与方卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'获赠方卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'ReceiveCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'ActAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'ActPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作coupon号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'ActCouponNumbers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态.0：未执行。1：已转出。2：完成' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'ActionDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联的交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'RefTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'Txndate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收方的手机号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent', @level2type=N'COLUMN',@level2name=N'ReceiveMobileNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员转赠表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPresent'
GO
