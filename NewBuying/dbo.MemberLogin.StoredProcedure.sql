USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberLogin]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[MemberLogin]  
  @MemberRegisterMobile	varchar(512),			--会员注册手机号
  @MemberPassword		varchar(512),			--会员密码（MD5加密，密文长度32）  
  @BrandCode            varchar(64),			--品牌code，如果为空，则不返回CardNumber
  @MemberID				int output,             -- 返回Member表主键
  @CardNumber			varchar(64) output,     -- 返回Card表主键
  @MemberCreatedOn      datetime output,        -- 返回Member表记录的创建时间
  @CheckResult          varchar(64) output,     -- 返回账号校验结果：90241- email not verified，90242- mobile number not verified，90244- Invalid status - Frozen
  @EmailValidation      int output,             -- 邮箱验证标志。（只取一个邮箱）                                        
  @SMSValidation        int output,             -- 手机验证标志。（只取一个手机）                                      
  @ReadReguFlag         int output,             -- 条例已读标志。                                         
  @MemberDefLanguage    varchar(64) output,     -- 会员默认语言
  @NoPassword			int=0,					--0：需要验证密码. 1: 不需要要验证密码。  默认为0
  @OSType               int,                     -- 操作系统类型：  1： Apple的OS。 2：Google的 Android
  @DeviceID             varchar(512),            -- 和操作系统有关的设备ID。
  @CountryCode          varchar(64) output,      -- (只返回)国家码
  @MemberMobilePhone    varchar(64) output,       -- (只返回)手机号码
  @PWDFailRetryCount    int output                -- (只返回)密码错误导致的 剩余重试次数. -1：表示不做出错次数限制。  
AS
/****************************************************************************
**  Name : MemberLogin 会员登录
**  Version: 1.0.0.14
**  Description : 会员登录，返回MemberID，调用者保存。
**  Example :
  declare @MemberID int, @A int, @CardNumber varchar(64),  @MemberCreatedOn datetime, @CheckResult varchar(64),
       @CountryCode  varchar(64), @MemberMobilePhone  varchar(64),@PWDFailRetryCount int,      
  @EmailValidation int, @SMSValidation int, @ReadReguFlag int, @MemberDefLanguage varchar(64)
  exec @A = MemberLogin '08613916423422', '', '', @MemberID output, @CardNumber output, @MemberCreatedOn output, 
   @CheckResult output,
  @EmailValidation output, @SMSValidation output, @ReadReguFlag output, @MemberDefLanguage output, 1, 1, '23423424',
    @CountryCode output, @MemberMobilePhone output, @PWDFailRetryCount output
  print @A
  print @MemberID
  print @CardNumber
  select * from card where memberid  >0
  select EmailValidation, SMSValidation, ReadReguFlag, MemberDefLanguage, * from member
    select * from useraction_movement

**  Created by: Gavin @2012-02-20
**  Modify by: Gavin @2012-08-09 (Ver 1.0.0.1) 增加输入参数@NoPassword. 控制是否要校验passw。
**  Modify by: Gavin @2012-08-10 (Ver 1.0.0.2) 修改输入参数，如果输入BrandCode，则返回此会员指定brandcode的 cardnumber
**  Modify by: Gavin @2013-04-03 (Ver 1.0.0.4) 增加返回参数@MemberCreatedOn
**  Modify by: Gavin @2013-06-21 (Ver 1.0.0.5) @MemberRegisterMobile 内容可以手机号,也可以是邮箱. 根据内容中是否有 @ 来判断
**  Modify by: Gavin @2013-10-11 (Ver 1.0.0.6) 记录操作到UserAction_Movement表
**  Modify by: Gavin @2014-06-19 (Ver 1.0.0.7) 因为存在新的要求：原样Password存入。 所以交易密码时要做 二次尝试：先MD5加密后检查，如果不成功，再做原样的校验
**  Modify by: Gavin @2014-07-03 (Ver 1.0.0.8) 增加输出参数：@EmailValidation，@SMSValidation，@ReadReguFlag，@MemberDefLanguage
                                               修改Member表中MemberDefLanguage存放的值,确定为languageMap表的KeyID
**  Modify by: Gavin @2014-07-08 (Ver 1.0.0.9) 兼容Email作为registermobile的情况。  
**  Modify by: Gavin @2014-07-21 (Ver 1.0.0.10) 增加返回字段:@CountryCode, @MemberMobilePhone 
**  Modify by: Gavin @2014-07-24 (Ver 1.0.0.11) 增加返回字段:@PWDFailRetryCount 。 
**  Modify by: Gavin @2014-10-23 (Ver 1.0.0.12) 优化 
**  Modify by: Gavin @2014-11-17 (Ver 1.0.0.13) 从这个版本起，移除711的特殊（非通用）的要求。 (取消update memberMessageaccount 中 servicetypeid =9,10 的更新)   
**  Modify by: Gavin @2015-05-05 (Ver 1.0.0.14) 加回 update memberMessageaccount 中 servicetypeid =9,10 的更新                        
**
****************************************************************************/
begin
  declare @MemberPasswordStr varchar(512), @MD5PasswordStr varchar(512), @CardStatus int
  declare @EMailFlag int, @SMSFlag int, @PWDFailCount int,  @LoginFailureCount int
  declare @AppleNotification varchar(512), @GoogleNotification varchar(512)
  set @AppleNotification = ''
  set @GoogleNotification = ''
  if isnull(@OSType, 0) = 1
    set @AppleNotification = @DeviceID
  if isnull(@OSType, 0) = 2
    set @GoogleNotification = @DeviceID    

  set @EMailFlag = 0 
  set @SMSFlag = 0
  set @CheckResult = '0'
  set @CardStatus = 0
  set @LoginFailureCount = 0
  set @PWDFailRetryCount = -1
  
  if isnull(@MemberPassword, '') <> ''
    set @MD5PasswordStr = dbo.EncryptMD5(@MemberPassword)
  if CharIndex('@', @MemberRegisterMobile) > 0
  begin 
    select @MemberID = MemberID, @MemberPasswordStr = MemberPassword, @MemberCreatedOn = CreatedOn, 
        @ReadReguFlag = ReadReguFlag, @MemberDefLanguage = L.LanguageAbbr, @CountryCode = CountryCode,
        @MemberMobilePhone = MemberMobilePhone, @PWDFailCount = isnull(PWDFailCount, 0)
      from member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
     where MemberEmail = @MemberRegisterMobile
    if @@ROWCOUNT = 0
    begin
      select @MemberID = MemberID, @MemberPasswordStr = MemberPassword, @MemberCreatedOn = CreatedOn, 
          @ReadReguFlag = ReadReguFlag, @MemberDefLanguage = L.LanguageAbbr, @CountryCode = CountryCode,
          @MemberMobilePhone = MemberMobilePhone, @PWDFailCount = isnull(PWDFailCount, 0)
        from member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
       where MemberRegisterMobile = @MemberRegisterMobile
      print  @MemberRegisterMobile   
    end 
  end else
  begin
    select @MemberID = MemberID, @MemberPasswordStr = MemberPassword, @MemberCreatedOn = CreatedOn,
        @ReadReguFlag = ReadReguFlag,@MemberDefLanguage = L.LanguageAbbr, @CountryCode = CountryCode,
        @MemberMobilePhone = MemberMobilePhone, @PWDFailCount = isnull(PWDFailCount, 0)
      from member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
      where MemberRegisterMobile = @MemberRegisterMobile
  end
      
  if isnull(@MemberID, 0) <> 0
  begin
    select Top 1 @EMailFlag = VerifyFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 1
    select Top 1 @SMSFlag = VerifyFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2

    update MemberMessageAccount set AccountNumber = @AppleNotification where MemberID = @MemberID and MessageServiceTypeID = 9
    update MemberMessageAccount set AccountNumber = @GoogleNotification where MemberID = @MemberID and MessageServiceTypeID = 10   
   
    set @EmailValidation = @EMailFlag
    set @SMSValidation = @SMSFlag
    
	if isnull(@BrandCode, '') <> ''
	begin
	  select top 1 @CardNumber = CardNumber, @CardStatus = C.Status, @LoginFailureCount = isnull(G.LoginFailureCount, 0)
	    from card C 
	    left join CardType T on C.CardTypeID = T.CardTypeID
	    left join CardGrade G on C.CardGradeID = G.CardTypeID
		  left join Brand B on T.BrandID = B.StoreBrandID
		where B.StoreBrandCode = @BrandCode and C.MemberID = @MemberID
	end else
	begin
	  select top 1 @CardNumber = CardNumber, @CardStatus = C.Status, @LoginFailureCount = isnull(G.LoginFailureCount, 0)
	    from card C 
	    left join CardType T on C.CardTypeID = T.CardTypeID
	    left join CardGrade G on C.CardGradeID = G.CardTypeID		 
		where C.MemberID = @MemberID	
	end
	
	if isnull(@LoginFailureCount, 0) > 0
	begin
	  set @PWDFailRetryCount = isnull(@LoginFailureCount, 0) - ISNULL(@PWDFailCount, 0)
	  if @PWDFailRetryCount <= 0
	    return -203  -- 密码错误超过指定次数。 
	end
	
    if @CardStatus <> 2 
      set @CheckResult = '90244'
    else if ISNULL(@EMailFlag, 0) <> 1
      set @CheckResult = '90241'
    else if ISNULL(@SMSFlag, 0) <> 1
      set @CheckResult = '90242'   
     	
    if @NoPassword = 0
    begin
      if isnull(@MD5PasswordStr,'') = isnull(@MemberPasswordStr, '') or isnull(@MemberPassword,'') = isnull(@MemberPasswordStr, '') 
      begin
        --exec RecordUserAction @MemberID, 1, @CardNumber, '', 'Member Login', '', '', '', @MemberID     
        update Member set PWDFailCount = 0, UpdatedOn = GETDATE() where MemberID = @MemberID              
        return 0
      end else 
      begin
        update Member set PWDFailCount = PWDFailCount + 1, UpdatedOn = GETDATE() where MemberID = @MemberID
        return -3
      end                
    end else
    begin
     -- exec RecordUserAction @MemberID, 1, @CardNumber, '', 'Member Login', '', '', '', @MemberID 
      update Member set PWDFailCount = 0, UpdatedOn = GETDATE() where MemberID = @MemberID 
      return 0 
    end
       
  end else
    return -1
end

GO
