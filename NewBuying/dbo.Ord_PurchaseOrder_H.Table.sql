USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_PurchaseOrder_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_PurchaseOrder_H](
	[PurchaseOrderNumber] [varchar](64) NOT NULL,
	[SupplierDesc] [varchar](512) NULL,
	[SupplierID] [int] NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[OrderType] [int] NULL,
	[SuppliertContactName] [varchar](512) NULL,
	[SupplierPhone] [varchar](512) NULL,
	[SupplierEmail] [varchar](512) NULL,
	[SupplierAddress] [varchar](512) NULL,
	[SupplierMobile] [varchar](512) NULL,
	[StoreID] [int] NOT NULL,
	[StoreName] [varchar](512) NULL,
	[StoreAddress] [varchar](512) NULL,
	[StoreContactName] [varchar](512) NULL,
	[StorePhone] [varchar](512) NULL,
	[StoreEmail] [varchar](512) NULL,
	[StoreMobile] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_PURCHASEORDER_H] PRIMARY KEY CLUSTERED 
(
	[PurchaseOrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_PurchaseOrder_H] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[Ord_PurchaseOrder_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_PurchaseOrder_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'PurchaseOrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单供应商描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'SupplierDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商主键，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'SupplierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。如果是由其他单据产生的，就填此单据号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'SuppliertContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'SupplierPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'SupplierEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'SupplierAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'SupplierMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID （总部）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'StoreName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'StoreAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'StorePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'StoreEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提交给供应商的采购单 。 主表
RRG： 电信卡给供应商的订单

（不使用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_PurchaseOrder_H'
GO
