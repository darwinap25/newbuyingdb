USE [NewBuying]
GO
/****** Object:  Table [dbo].[TelcoCardTopUpRule_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TelcoCardTopUpRule_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardTopUpCode] [varchar](64) NOT NULL,
	[TopUpType] [int] NULL,
	[StoreID] [int] NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[OrderTargetID] [int] NOT NULL,
	[OrderTargetCardNumber] [varchar](64) NULL,
	[MinBalance] [money] NULL,
	[RunningBalance] [money] NULL,
	[Priority] [int] NULL,
	[Column_11] [char](10) NULL,
 CONSTRAINT [PK_TELCOCARDTOPUPRULE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TelcoCardTopUpRule_D] ADD  DEFAULT ((1)) FOR [TopUpType]
GO
ALTER TABLE [dbo].[TelcoCardTopUpRule_D] ADD  DEFAULT ((0)) FOR [Priority]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，规则代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'CardTopUpCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'补货类型。 1：金额。 2：积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'TopUpType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货目标ID。（如果是店铺订货，则填写总部的ID。 如果总部订货，则填写供应商ID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'OrderTargetID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货目标卡。（如果是店铺订货，则填写总部的Card。 如果总部订货，则填空））' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'OrderTargetCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小允许余额。（低于此将可能触发自动充值）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'MinBalance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'正常余额。（补货金额为： 正常余额 - 当前余额）
注：如果有补货的倍数要求，按照倍数要求计算。
比如： 正常库存数量 - 当前库存数量 = 53.   供应商要求按50倍数订货，所以需要订货数量为 100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'RunningBalance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级。 （库存‘金额不足情况下。按 从小到大 优先分配。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电信卡自动充值设置表。子表。 支持的CardGrade列表。 总部的卡号和余额设置。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_D'
GO
