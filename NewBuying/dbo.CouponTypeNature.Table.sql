USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponTypeNature]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponTypeNature](
	[CouponTypeNatureID] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeNatureName1] [nvarchar](512) NULL,
	[CouponTypeNatureName2] [nvarchar](512) NULL,
	[CouponTypeNatureName3] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_COUPONTYPENATURE] PRIMARY KEY CLUSTERED 
(
	[CouponTypeNatureID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CouponTypeNature] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CouponTypeNature] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeNature', @level2type=N'COLUMN',@level2name=N'CouponTypeNatureID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型种类名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeNature', @level2type=N'COLUMN',@level2name=N'CouponTypeNatureName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型种类名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeNature', @level2type=N'COLUMN',@level2name=N'CouponTypeNatureName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型种类名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeNature', @level2type=N'COLUMN',@level2name=N'CouponTypeNatureName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵大类表。
1：现金劵。2：折扣优惠劵。3：货品兑换卷。4：印花' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeNature'
GO
