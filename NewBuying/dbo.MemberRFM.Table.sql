USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberRFM]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberRFM](
	[CardNumber] [varchar](64) NOT NULL,
	[Recency_Score] [int] NULL,
	[Frequency_Score] [int] NULL,
	[MemberID] [int] NOT NULL,
	[Monetary_Score] [int] NULL,
	[RFM_Score] [int] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_MEMBERRFM] PRIMARY KEY CLUSTERED 
(
	[CardNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberRFM] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号。 作为唯一主键。 查询时，只输入卡号就可以唯一定位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberRFM', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Regency评分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberRFM', @level2type=N'COLUMN',@level2name=N'Recency_Score'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Frequency评分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberRFM', @level2type=N'COLUMN',@level2name=N'Frequency_Score'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberRFM', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monetary评分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberRFM', @level2type=N'COLUMN',@level2name=N'Monetary_Score'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RFM评分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberRFM', @level2type=N'COLUMN',@level2name=N'RFM_Score'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员的RFM分数。
Add by Gavin @2016-08-29' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberRFM'
GO
