USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewCardTypes]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewCardTypes]
AS
 select BrandID, T.CardTypeID, CardTypeCode, CardTypeName1, CardTypeName2, CardTypeName3, CardTypeNotes, 
    T.CardTypeNatureID, CardTypeNatureName1, CardTypeNatureName2, CardTypeNatureName3, CardTypeStartDate, CardTypeEndDate,
    A.CardGradeLayoutFile
   from CardType T left join CardTypeNature N on T.CardTypeNatureID = N.CardTypeNatureID
   left join (select min(CardGradeLayoutFile) as CardGradeLayoutFile, CardTypeID from CardGrade group by CardTypeID) A on T.CardTypeID = A.CardTypeID

GO
