USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberLogin_New]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[MemberLogin_New]  
  @LoginAccountNumber      varchar(64),        -- 会员登录号码
  @LoginAccountType        int,                -- 会员登录号码类型。 0：MemberRegisterMobile；1：MemberEmail；2：MemberMobileNumber； 3：3个字段中只要有一个符合即可；	
  @NoPassword			         int=0,              -- 0：需要验证密码.  1: 不需要要验证密码。  默认为0
  @MemberPassword          varchar(512),       -- 会员密码（MD5加密，密文长度32）  
  @BrandID                 int,                -- Brand表 ID，根据brandID选择CardType, 返回此品牌的CardNumber。 如果为0/NULL，则返回第一张卡的数据
  @OSType                  int,                -- 操作系统类型：  1： Apple的OS。 2：Google的 Android. 0 : 不指定
  @DeviceID                varchar(512),       -- 和操作系统有关的设备ID。  
  @MemberID                int output,         -- (只返回)返回Member表主键
  @CardNumber              varchar(64) output, -- (只返回)返回CardNumber
  @CardAmountBalance       money output,       -- (只返回)返回Card的 金额 余额
  @CardPointBalance        int output,         -- (只返回)返回Card的 积分 余额
  @MemberCreatedOn         datetime output,    -- (只返回)返回Member表记录的创建时间
  @CheckResult             varchar(64) output, -- (只返回)返回账号校验结果：90241- email not verified，90242- mobile number not verified，90244- Invalid status - Frozen
  @EmailValidation         int output,         -- (只返回)邮箱验证标志。（只取一个邮箱）                                        
  @SMSValidation           int output,         -- (只返回)手机验证标志。（只取一个手机）                                      
  @ReadReguFlag            int output,         -- (只返回)条例已读标志。                                         
  @MemberDefLanguage       varchar(64) output,    -- (只返回)会员默认语言
  @CountryCode             varchar(64) output,    -- (只返回)国家码
  @MemberMobilePhone       varchar(64) output,    -- (只返回)手机号码
  @PWDFailRetryCount       int output,            -- (只返回)密码错误导致的 剩余重试次数. -1：表示不做出错次数限制。
  @HoldCouponCount         int output,            -- (只返回)当前属于此卡的有效Coupon数量（status=2）  
  @MemberNickName          varchar(64) output,    -- (只返回)会员昵称
  @MemberFamilyName        varchar(512) output ,  -- (只返回)会员姓
  @MemberGivenName         varchar(512) output    -- (只返回)会员名  
AS
/****************************************************************************
**  Name : MemberLogin_New 会员登录
**  Version: 1.0.1.1
**  Description : 会员登录，返回MemberID，以及一些用户指定的信息,比如 coupon数量,积分余额,金额余额.(只支持返回此用户的第一张卡的数据)
**  Example :
  declare @MemberID int, @A int, @CardNumber varchar(64),  @MemberCreatedOn datetime, @CheckResult varchar(64),
          @CountryCode  varchar(64), @MemberMobilePhone  varchar(64),@PWDFailRetryCount int,  @EmailValidation int,      
          @SMSValidation int, @ReadReguFlag int, @MemberDefLanguage varchar(64), @CardAmountBalance money,
          @CardPointBalance int, @HoldCouponCount int, @MemberNickName varchar(64), @MemberFamilyName varchar(512),
          @MemberGivenName varchar(64) 
  exec @A = MemberLogin_New '08613916423422', 0, 1, '', 0, 0, '', @MemberID output, @CardNumber output, @CardAmountBalance output,
    @CardPointBalance output, @MemberCreatedOn output,@CheckResult output, @EmailValidation output, @SMSValidation output, 
    @ReadReguFlag output, @MemberDefLanguage output, @CountryCode output, @MemberMobilePhone output, @PWDFailRetryCount output,
    @HoldCouponCount output, @MemberNickName output, @MemberFamilyName output, @MemberGivenName output
  print @A
  print @MemberID  
  print @CardNumber print @CardAmountBalance print  @CardPointBalance print @MemberCreatedOn  print @CheckResult
select * from member
**  Created by: Gavin @2015-01-30 (从 @2014-11-17 (Ver 1.0.0.13) 版本修改,需要恢复update memberMessageaccount 中 servicetypeid =9,10)
**  Modify by: Gavin @2015-11-19 (从 @2015-11-19 (Ver 1.0.0.13) 修正bug, (left join CardGrade G on C.CardGradeID = G.CardGradeID join字段不对)
**
****************************************************************************/
begin
  declare @MemberPasswordStr varchar(512), @MD5PasswordStr varchar(512), @CardStatus int
  declare @EMailFlag int, @SMSFlag int, @PWDFailCount int,  @LoginFailureCount int
  declare @AppleNotification varchar(512), @GoogleNotification varchar(512)
  
  set @BrandID = ISNULL(@BrandID, 0)
  set @LoginAccountType = ISNULL(@LoginAccountType, 0)
  set @AppleNotification = ''
  set @GoogleNotification = ''
  if isnull(@OSType, 0) = 1
    set @AppleNotification = @DeviceID
  if isnull(@OSType, 0) = 2
    set @GoogleNotification = @DeviceID    

  set @EMailFlag = 0 
  set @SMSFlag = 0
  set @CheckResult = '0'
  set @CardStatus = 0
  set @LoginFailureCount = 0
  set @PWDFailRetryCount = -1
  
  if isnull(@MemberPassword, '') <> ''
    set @MD5PasswordStr = dbo.EncryptMD5(@MemberPassword)
  
  -- 查询获得MemberID   
  if @LoginAccountType = 0 
  begin
    select @MemberID = MemberID, @MemberPasswordStr = MemberPassword, @MemberCreatedOn = CreatedOn, 
        @ReadReguFlag = ReadReguFlag, @MemberDefLanguage = L.LanguageAbbr, @CountryCode = CountryCode,
        @MemberMobilePhone = MemberMobilePhone, @PWDFailCount = isnull(PWDFailCount, 0),
        @MemberNickName = NickName, @MemberFamilyName = MemberEngFamilyName, @MemberGivenName = MemberEngGivenName
      from member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
     where MemberRegisterMobile = @LoginAccountNumber  
  end else if @LoginAccountType = 1 
  begin
    select @MemberID = MemberID, @MemberPasswordStr = MemberPassword, @MemberCreatedOn = CreatedOn, 
        @ReadReguFlag = ReadReguFlag, @MemberDefLanguage = L.LanguageAbbr, @CountryCode = CountryCode,
        @MemberMobilePhone = MemberMobilePhone, @PWDFailCount = isnull(PWDFailCount, 0),
        @MemberNickName = NickName, @MemberFamilyName = MemberEngFamilyName, @MemberGivenName = MemberEngGivenName
      from member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
     where MemberEmail = @LoginAccountNumber    
  end else if @LoginAccountType = 2
  begin
    select @MemberID = MemberID, @MemberPasswordStr = MemberPassword, @MemberCreatedOn = CreatedOn, 
        @ReadReguFlag = ReadReguFlag, @MemberDefLanguage = L.LanguageAbbr, @CountryCode = CountryCode,
        @MemberMobilePhone = MemberMobilePhone, @PWDFailCount = isnull(PWDFailCount, 0),
        @MemberNickName = NickName, @MemberFamilyName = MemberEngFamilyName, @MemberGivenName = MemberEngGivenName
      from member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
     where MemberMobilePhone = @LoginAccountNumber       
  end else
  begin 
    select @MemberID = MemberID, @MemberPasswordStr = MemberPassword, @MemberCreatedOn = CreatedOn, 
        @ReadReguFlag = ReadReguFlag, @MemberDefLanguage = L.LanguageAbbr, @CountryCode = CountryCode,
        @MemberMobilePhone = MemberMobilePhone, @PWDFailCount = isnull(PWDFailCount, 0),
        @MemberNickName = NickName, @MemberFamilyName = MemberEngFamilyName, @MemberGivenName = MemberEngGivenName
      from member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
     where MemberRegisterMobile = @LoginAccountNumber or MemberEmail = @LoginAccountNumber or MemberMobilePhone = @LoginAccountNumber
  end
  
  -- 查到MemberID后，继续获取相关信息 （Card）    
  if isnull(@MemberID, 0) <> 0
  begin
    select Top 1 @EMailFlag = VerifyFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 1
    select Top 1 @SMSFlag = VerifyFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2   
    set @EmailValidation = @EMailFlag
    set @SMSValidation = @SMSFlag  
    select Top 1 @CardNumber = CardNumber, @CardStatus = C.Status, @LoginFailureCount = isnull(G.LoginFailureCount, 0),
        @CardAmountBalance = C.TotalAmount, @CardPointBalance = C.TotalPoints
	    from card C 
	      left join CardType T on C.CardTypeID = T.CardTypeID
	      left join CardGrade G on C.CardGradeID = G.CardGradeID
       where C.MemberID = @MemberID and (T.BrandID = @BrandID or (@BrandID = 0))
     order by C.CardNumber
	
	  select @HoldCouponCount = count(CouponNumber) from Coupon where CardNumber = @CardNumber and Status = 2
	   
    update MemberMessageAccount set AccountNumber = @AppleNotification where MemberID = @MemberID and MessageServiceTypeID = 9
    update MemberMessageAccount set AccountNumber = @GoogleNotification where MemberID = @MemberID and MessageServiceTypeID = 10   
	
	  if isnull(@LoginFailureCount, 0) > 0
	  begin
	    set @PWDFailRetryCount = isnull(@LoginFailureCount, 0) - ISNULL(@PWDFailCount, 0)
	    if @PWDFailRetryCount <= 0
	      return -203  -- 密码错误超过指定次数。 
	  end
    if @CardStatus <> 2 
      set @CheckResult = '90244'
    else if ISNULL(@EMailFlag, 0) <> 1
      set @CheckResult = '90241'
    else if ISNULL(@SMSFlag, 0) <> 1
      set @CheckResult = '90242'   
     	
    if @NoPassword = 0
    begin
      if isnull(@MD5PasswordStr,'') = isnull(@MemberPasswordStr, '') or isnull(@MemberPassword,'') = isnull(@MemberPasswordStr, '') 
      begin
        --exec RecordUserAction @MemberID, 1, @CardNumber, '', 'Member Login', '', '', '', @MemberID     
        update Member set PWDFailCount = 0, UpdatedOn = GETDATE() where MemberID = @MemberID              
        return 0
      end else 
      begin
        update Member set PWDFailCount = PWDFailCount + 1, UpdatedOn = GETDATE() where MemberID = @MemberID
        return -3
      end                
    end else
    begin
      --exec RecordUserAction @MemberID, 1, @CardNumber, '', 'Member Login', '', '', '', @MemberID 
      update Member set PWDFailCount = 0, UpdatedOn = GETDATE() where MemberID = @MemberID 
      return 0 
    end       
  end else
    return -1
end

GO
