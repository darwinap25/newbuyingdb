USE [NewBuying]
GO
/****** Object:  Table [dbo].[SVARewardRules]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SVARewardRules](
	[SVARewardRulesID] [int] IDENTITY(1,1) NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[SVARewardType] [int] NULL DEFAULT ((0)),
	[SVARewardRulesCode] [varchar](64) NOT NULL,
	[RewardAmount] [money] NULL,
	[RewardPoint] [int] NULL,
	[RewardCouponTypeID] [int] NULL,
	[RewardCouponCount] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NOT NULL DEFAULT ((1)),
	[Note] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SVAREWARDRULES] PRIMARY KEY CLUSTERED 
(
	[SVARewardRulesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'SVARewardRulesID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励类型，默认0：
0： 一般
1：推荐新会员奖励。（新会员阅读初始条约时，给予推荐人的奖励）
2：新会员奖励
3：会员share to facebook奖励
4：会员生日奖励
5：会员第一笔交易奖励。（给会员本人）
6：会员第一笔交易奖励。（给会员的推荐人）
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'SVARewardType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'SVARewardRulesCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'RewardAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'RewardPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励优惠劵类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'RewardCouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励优惠劵数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'RewardCouponCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态： 0：无效。 1：有效。默认1.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员推荐新会员奖励规则。（这里设定的奖励，都是一次性的奖励）
@2014-10-27 增加类型 5,6
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SVARewardRules'
GO
