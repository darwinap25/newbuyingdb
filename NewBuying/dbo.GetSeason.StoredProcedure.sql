USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSeason]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetSeason]
  @SeasonID			    int,      -- 指定的SeasonID
  @LanguageAbbr			varchar(20)=''	   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1  
AS
/****************************************************************************
**  Name : GetSeason
**  Version: 1.0.0.0
**  Description : 返回Season表数据
**  Example :
    exec GetSeason 0, ''
**  Created by: Gavin @2016-07-27
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @SeasonID = isnull(@SeasonID, 0)
  select SeasonID, SeasonCode, 
      case when @Language = 2 then SeasonName2 when @Language = 3 then SeasonName3 else SeasonName1 end as SeasonName    
   from Season
   where  SeasonID = @SeasonID or @SeasonID = 0

  return 0
end

GO
