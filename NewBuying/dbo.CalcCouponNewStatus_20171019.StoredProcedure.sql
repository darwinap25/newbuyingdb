USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcCouponNewStatus_20171019]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[CalcCouponNewStatus_20171019]
  @CouponNumber varchar(64),
  @CouponTypeID int,
  @OprID  int,
  @OldStatus int,			-- 当前coupon的状态, 如果是手动调节，则填写将要改成的状态。
  @NewStatus int out        -- 新状态
AS
/****************************************************************************
**  Name : CalcCouponNewStatus
**  Version: 1.0.0.3
**  Description : 计算coupon在此操作后，应该变更成的新状态
**
**  Created by: Gavin @2012-09-07
**  Modify by: Gavin @2013-03-07 (ver 1.0.0.1) 增加OprID=56的处理
**  Modify by: Darwin @2017-09-25 (ver 1.0.0.2) Added OprID = 38
**  Modify by: Darwin @2017-09-28 (ver 1.0.0.3) Added OprID = 57
**
****************************************************************************/
begin
   set @NewStatus = @OldStatus            
      
    if @OprID IN (31, 32, 33, 34, 35, 38, 40, 41,42, 53, 54, 56, 57)
    begin       
      if @OprID = 40
        set @NewStatus = 1    
      if @OprID in (31, 32, 33, 53)
         set @NewStatus = 2
      if @OprID in (34, 54)
        set @NewStatus = 3
      if @OprID IN (38, 42)
        set @NewStatus = 4
      if @OprID = 35
        set @NewStatus = 5
	  if @OprID = 57 and @OldStatus = 1
        set @NewStatus = 2
	  if @OprID = 57 and @OldStatus = 2
        set @NewStatus = 0		
      if @OprID = 41 or @OprID = 56
      begin
        if @OldStatus = 0
          set @NewStatus = 0
        if @OldStatus = 1
          set @NewStatus = 0
        if @OldStatus = 2
          set @NewStatus = 1
        if @OldStatus = 3
          set @NewStatus = 2
        if @OldStatus in (4,5)  -- 当前为4或者5的，需要回到之前的状态
        begin
		  select Top 1 @NewStatus = OrgStatus from Coupon_movement 
			where CouponNumber = @CouponNumber and OrgStatus <> NewStatus 
		  order by KeyID desc
        end  
      end 
    end 
end





GO
