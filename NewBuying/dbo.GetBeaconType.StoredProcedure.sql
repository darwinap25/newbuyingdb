USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBeaconType]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetBeaconType]
  @PositionID      varchar(64),      -- iBeacon的ID 必填
  @CardNumber     varchar(64),      -- 卡号 如果不填，获取的Coupon不绑定card。
  @Additional     varchar(512),     -- 预留
  @Battery        varchar(64),      -- iBeacon电池余量
  @TokenID        varchar(64),      -- 手机的TokenID
  @LanguageAbbr   varchar(20)=''	 
AS
/****************************************************************************
**  Name : GetBeaconType
**  Version: 1.0.0.2
**  Description : 由iBeacon触发提交的请求。
  (根据ibeaconid在iBeaconGroup中找到所能提供的功能)
 
   declare @A int 
   exec @A=GetBeaconType '1501-110', '', '', '89','RAYTEST123',''
   print @A
select * from ibeacon
**  Created by: Gavin @2015-07-09
**  Modify by: Gavin @2015-10-02 (ver 1.0.0.1) 防止一天内多次收到消息  (For HKIA)
**  Modify by: Ray @2015-10-05 (ver 1.0.0.2) 修改防止一天内多次收到消息, 並更新Beacon?量 
****************************************************************************/
BEGIN
  declare @Language int, @MemberID int, @CardTotalAmount money, @CardTotalPoints int, @count int, @recordcount int
  declare @AreaID int, @iBeaconGroupID int, @AssociationType int, @AssociationID int, @iBeaconID int
  declare @EachCoupon varchar(64), @MaxKeyID int, @StoreID int, @StoreCode varchar(64), @PromotionMsgKeyID int,
          @MessageID int, @BrandID int, @BrandCode varchar(64), @URL varchar(500)
  
  set @URL = 'http://103.9.245.11:83/html/Message/%#.html'    

  Create Table #ReturnTable (Title nvarchar(512), Association_Type int, Association_ID varchar(64), Assiocation_Code varchar(64), 
    PicFile varchar(512), URL varchar(512), [Desc] nvarchar(max))

  declare @ReturnAmount money ,@ReturnPoint int ,@ReturnMessageStr varchar(max), @ReturnCouponNumber varchar(64)

  declare @SC varchar(64)
  declare @CouponTypeCode varchar(64), @CouponTypeID int
 
  Create Table #Return (Title varchar(512), Association_Type int, Association_ID varchar(64), Assiocation_Code varchar(64), 
    PicFile varchar(512), URL varchar(512), [Desc] varchar(512))
     
  Declare @CanEarnTypes Table (KeyID int, CouponTypeStartDate datetime, CouponTypeEndDate datetime, CouponTypeName1 varchar(512), 
    CouponTypeID int, CouponTypeCode varchar(64), CouponName varchar(512), CampaignID int, CouponTypeNotes varchar(512), 
    CouponTypeLayoutFile varchar(512), ExchangeType int, ExchangeAmount money, ExchangePoint int, ExchangeCouponTypeID int, 
    ExchangeCouponTypeCode varchar(64), ExchangeCouponCount int, CouponTypePicFile varchar(512),CouponCountBalance int, 
    ClauseName varchar(512), ClauseName2 varchar(512), ClauseName3 varchar(512), MemberClauseDesc1 varchar(512), 
    MemberClauseDesc2 varchar(512), MemberClauseDesc3 varchar(512), CouponTypeNatureID int, MemberClauseDesc varchar(512), 
    StoreCode varchar(64))  
  DECLARE @CouponStoreList table(CouponTypeID int, StoreCode varchar(64))
  DECLARE @CouponNumberList table(KeyID int, CouponTypeStartDate datetime, CouponTypeEndDate datetime, CouponTypeName1 varchar(512), 
    CouponTypeID int, CouponTypeCode varchar(64), CouponName varchar(512), CampaignID int, CouponTypeNotes varchar(512), 
    CouponTypeLayoutFile varchar(512), ExchangeType int, ExchangeAmount money, ExchangePoint int, ExchangeCouponTypeID int, 
    ExchangeCouponTypeCode varchar(64), ExchangeCouponCount int, CouponTypePicFile varchar(512),CouponCountBalance int, 
    ClauseName varchar(512), ClauseName2 varchar(512), ClauseName3 varchar(512), MemberClauseDesc1 varchar(512), 
    MemberClauseDesc2 varchar(512), MemberClauseDesc3 varchar(512), CouponTypeNatureID int, MemberClauseDesc varchar(512), CouponNumber varchar(64))  
  DECLARE @icoupontype int
  Create Table #Temp (KeyID int, PromotionDesc nvarchar(max), PromotionTitle nvarchar(512), PromotionPicFile varchar(512), 
      PromotionRemark nvarchar(2000), CreatedOn datetime, StartDate datetime, EndDate datetime, PromotionMsgCode varchar(64), 
      UpdatedOn datetime, PromotionMsgTypeID int, PromotionPicFile2 varchar(512), PromotionPicFile3 varchar(512),
      IsRead int, Link varchar(512), URL varchar(512))
  
  if ISNULL(@Battery, '') = ''     
  set @Battery = '0'  
          
  set @PositionID = ISNULL(@PositionID, '')
  set @CardNumber = ISNULL(@CardNumber, '')
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1	
  
  select @MemberID = MemberID, @CardTotalAmount = TotalAmount, @CardTotalPoints = TotalPoints 
    from Card where CardNumber = @CardNumber	
  select @AreaID = AreaID, @iBeaconID = iBeaconID from iBeacon where iBeaconCode = @PositionID
  select @iBeaconGroupID = iBeaconGroupID from iBeaconGroupMap where iBeaconID = @iBeaconID
  select @AssociationID = AssociationID, @AssociationType = AssociationType from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID 
  select @PromotionMsgKeyID = AssociationID from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID and AssociationType = 2
  set @MemberID = ISNULL(@MemberID, 0)
  set @AreaID = ISNULL(@AreaID, 0)
  set @iBeaconGroupID = ISNULL(@iBeaconGroupID, 0)
  set @AssociationID = ISNULL(@AssociationID, 0)
  set @AssociationType = ISNULL(@AssociationType, 0)

  select @MemberID = MemberID, @ReturnAmount = TotalAmount, @ReturnPoint = TotalPoints 
    from Card where CardNumber = @CardNumber	
  select @AreaID = AreaID, @iBeaconID = iBeaconID from iBeacon where iBeaconCode = @PositionID
  select @iBeaconGroupID = iBeaconGroupID from iBeaconGroupMap where iBeaconID = @iBeaconID
  select @icoupontype = AssociationID from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID and AssociationType = 1
  select @PromotionMsgKeyID = AssociationID from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID and AssociationType = 2
  select @MessageID = AssociationID from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID and AssociationType = 4


  DECLARE CUR_EarchStore CURSOR fast_forward FOR
    select StoreID from AreaStoreMap where AreaID = @AreaID 
  OPEN CUR_EarchStore
  FETCH FROM CUR_EarchStore INTO @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN 
    select @StoreCode = StoreCode from Store where StoreID = @StoreID
    insert into @CanEarnTypes
      (KeyID, CouponTypeStartDate, CouponTypeEndDate, CouponTypeName1, CouponTypeID, CouponTypeCode, CouponName, CampaignID, CouponTypeNotes, 
       CouponTypeLayoutFile, ExchangeType, ExchangeAmount, ExchangePoint, ExchangeCouponTypeID, 
       ExchangeCouponTypeCode, ExchangeCouponCount, CouponTypePicFile,CouponCountBalance, 
       ClauseName, ClauseName2, ClauseName3, MemberClauseDesc1, MemberClauseDesc2, MemberClauseDesc3, CouponTypeNatureID, MemberClauseDesc)
    exec GetMemberCanEarnCouponTypes 0, @CardNumber, @StoreID, '', '', 0, 
      0, 0, 0, 1, '', 1, 0, @count output,
      @recordcount output, '', 0        -- @iBeaconGroupID 取消. 总是0, 在iBeaconGroup表中设置.
    update @CanEarnTypes set StoreCode = @StoreCode
     
    FETCH FROM CUR_EarchStore INTO @StoreID  
  END
  CLOSE CUR_EarchStore 
  DEALLOCATE CUR_EarchStore 
  
  DECLARE CUR_EarchStore_News CURSOR fast_forward FOR
    select StoreID from AreaStoreMap where AreaID = @AreaID 
  OPEN CUR_EarchStore_News  
  FETCH FROM CUR_EarchStore_News INTO @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN 
    select @StoreCode = StoreCode, @BrandID = BrandID from Store where StoreID = @StoreID
    select @BrandCode = StoreBrandCode from Brand where StoreBrandID = @BrandID
    insert into #Temp
      (KeyID, PromotionDesc, PromotionTitle, PromotionPicFile, PromotionRemark, CreatedOn, StartDate, 
      EndDate, PromotionMsgCode, UpdatedOn, PromotionMsgTypeID, PromotionPicFile2, PromotionPicFile3, IsRead, Link, URL)
    exec GetMemberPromotions @MemberID, @CardNumber, @StoreCode, '', '', @BrandCode, '',  0, 0, 1, null,null,1, 0, @count output, @recordcount output, @LanguageAbbr 
     
    FETCH FROM CUR_EarchStore_News INTO @StoreID  
  END
  CLOSE CUR_EarchStore_News 
  DEALLOCATE CUR_EarchStore_News 

  
   -- ver 1.0.0.1 ==============
 if exists(select * from iBeacon_Movement where TokenID = @TokenID and AssociationType = @AssociationType and @AssociationType  = 4 and AssociationID = @AssociationID 
			 and Convert(varchar(10), CreatedOn, 120) = Convert(varchar(10), GetDate(), 120) )
 begin
   -- ver 1.0.0.2
   update iBeacon set Battery = @Battery, UpdatedOn = GetDate() where iBeaconID = @iBeaconID
   select * from #ReturnTable
   return 0 
 end	
  -- ver 1.0.0.1=================


  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  if @Language = 1
    set @URL = REPLACE(@URL, '#', '_en-us')  
  else if @Language = 2
    set @URL = REPLACE(@URL, '#', '_zh-cn')  
  else if @Language = 3
    set @URL = REPLACE(@URL, '#', '_zh-hk')   

	set @URL = REPLACE(@URL, '%', @MessageID) 

  insert into #ReturnTable 
  select CouponName as Title, 1 as Association_Type, cast(CouponTypeID as varchar(64)) as Association_ID, 
    CouponTypeCode as Assiocation_Code, CouponTypePicFile as PicFile, @URL as URL, MemberClauseDesc as [Desc] 
  from @CanEarnTypes where CouponTypeID = @icoupontype
  insert into #ReturnTable 
  select case @Language when 2 then D.MessageTitle2 when 3 then D.MessageTitle3 else D.MessageTitle1 end as MessageTitle,
     4, D.MessageTemplateID, H.MessageTemplateCode, '', @URL as URL,
     case @Language when 2 then '' when 3 then '' else '' end as TemplateContent  
     from MessageTemplateDetail D left join MessageTemplate H on H.MessageTemplateID = D.MessageTemplateID 
     where D.MessageTemplateID = @MessageID 


  insert into #ReturnTable 
  select PromotionTitle, 2, KeyID, PromotionMsgCode, PromotionPicFile, '', PromotionDesc      
  from #Temp       
  where KeyID = @PromotionMsgKeyID
     
  exec UpdateiBeacon @iBeaconID, @CardNumber, @TokenID, @AssociationType, @AssociationID, @Battery

  select * from #ReturnTable  
 
END

GO
