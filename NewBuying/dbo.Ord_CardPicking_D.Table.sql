USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardPicking_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardPicking_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardPickingNumber] [varchar](64) NOT NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[Description] [nvarchar](512) NULL,
	[OrderQTY] [int] NULL,
	[PickQTY] [int] NULL,
	[ActualQTY] [int] NULL,
	[FirstCardNumber] [varchar](64) NULL,
	[EndCardNumber] [varchar](64) NULL,
	[BatchCardCode] [varchar](64) NULL,
	[PickupDateTime] [datetime] NULL,
	[OrderAmount] [money] NULL,
	[PickAmount] [money] NULL,
	[ActualAmount] [money] NULL,
	[OrderPoint] [int] NULL,
	[PickPoint] [int] NULL,
	[ActualPoint] [int] NULL,
 CONSTRAINT [PK_ORD_CARDPICKING_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CardPicking_D] ADD  DEFAULT (getdate()) FOR [PickupDateTime]
GO
/****** Object:  Trigger [dbo].[Delete_Ord_CardPicking_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Delete_Ord_CardPicking_D] ON [dbo].[Ord_CardPicking_D]
FOR DELETE
AS
/*==============================================================*/
/*                
* Name: Delete_Ord_CardPicking_D
* Version: 1.0.0.1
* Description : Card订单表的批核触发器 (根据Delete_Ord_CouponPicking_D （1.0.1.0))
select * from Ord_CardPicking_D
* Create By Gavin @2014-10-09
* Modify by Gavin @2015-02-11 (ver 1.0.0.1) 增加PurchaseType的区分。
*/
/*==============================================================*/
BEGIN  
  declare @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @FirstCard varchar(64), @EndCard varchar(64), @CardTypeID int, @CardGradeID int, @PurchaseType int
  
  DECLARE CUR_PickupCard_Detail CURSOR fast_forward for
      select FirstCardNumber, EndCardNumber, CardTypeID, CardGradeID, isnull(H.PurchaseType, 1) from Deleted D
        left join Ord_CardPicking_H H on D.CardPickingNumber = H.CardPickingNumber
	OPEN CUR_PickupCard_Detail
  FETCH FROM CUR_PickupCard_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID, @PurchaseType
  WHILE @@FETCH_STATUS=0 
  BEGIN 
    if @PurchaseType = 1
    begin
      update card set PickupFlag = 0, StockStatus = 2 
        where CardNumber >= @FirstCard and  CardNumber <= @EndCard and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
    end  
	  FETCH FROM CUR_PickupCard_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID, @PurchaseType	  
	END
  CLOSE CUR_PickupCard_Detail 
  DEALLOCATE CUR_PickupCard_Detail 
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'CardPickingNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'OrderQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要求拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'PickQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'ActualQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的首卡号。 （Telco 时， 此处填 总部的 issued 卡号。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'FirstCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的末卡号。（Telco 时， 此处填 店铺的 issued 卡号。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'EndCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstCardNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'BatchCardCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建此明细的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'PickupDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单收到金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'OrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货单金额。为了和PickQty统一。 存储过程将读取这个值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'PickAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收到金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'ActualAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单收到积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'OrderPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货单积分。为了和PickQty统一。 存储过程将读取这个值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'PickPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收到积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D', @level2type=N'COLUMN',@level2name=N'ActualPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡拣货单子表
表中brandID，cardtypeid，cardgradeid 关系不做校验，由UI在创建单据时做校验。
存储过程实际操作时，只按照CardGradeID来做。
（实际拣货首号码必须填，尾号码可以根据数量来计算，具体根据业务需求来做）
@2015-03-02 actamount 改为 ActualAmount ， ActPoint 改为ActualPoint 
  增加PickAMount， PickPoint' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_D'
GO
