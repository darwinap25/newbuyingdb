USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SendMessageToAccount]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SendMessageToAccount]
  @UserID               int,
  @SendMemberID			int,  
  @MessageServiceTypeID int,
  @MessageAccount       nvarchar(512),  
  @MessageType          int,
  @MessagePriority      int,
  @MessageCoding        int,
  @MessageTitle         nvarchar(512),
  @MessageBody          varbinary(Max),   
  @IsInternal           int,      
  @ReceiveList			varchar(max),  -- 接收者的MemberID 列表. 格式：12,14,16,103
  @MessageID            int output
AS
/****************************************************************************
**  Name : SendMessageToAccount
**  Version: 1.0.0.3
**  Description : 发送消息到指定的账号
**
**  Parameter :
  declare @a int, @MessageBody  varbinary(Max), @MessageID int, @SendMemberID int, 
    @MessageServiceTypeID int, @MessageType int, @MessagePriority int, @MessageCoding int,  @MessageTitle nvarchar(512),
    @IsInternal int, @ReceiveList varchar(max),  @MessageBodyChar  nvarchar(Max), @MessageAccount nvarchar(512)
  
  set @SendMemberID = 1
  set @MessageServiceTypeID = 1
  set @MessageAccount = ''
  set @MessageType  = 1
  set @MessagePriority = 1
  set @MessageCoding = 0
  set @MessageTitle = 'test message'
  set @MessageBodyChar = '发送给memberID： 2 和 170， 之间用 , 隔开'
  set @MessageBody = cast(@MessageBodyChar as varbinary(max))
  set @IsInternal = 1
  set @ReceiveList = '2'  -- 发送给memberID： 2 和 170， 之间用 , 隔开
  exec @a = SendMessageToAccount 1, @SendMemberID, @MessageServiceTypeID, '', @MessageType, 
      @MessagePriority, @MessageCoding, @MessageTitle,  @MessageBody, @IsInternal, @ReceiveList, @MessageID output
  print @a  
  print @MessageID
  print cast(@MessageBody as nvarchar(max))
  
  select top 10 cast(messagebody as nvarchar(max)), * from MessageObject order by createdon desc 
  select * from MessageReceiveList   where messageid = 30263
  select * from MessageObject
  delete from MessageObject
  delete from MessageReceiveList
  delete from MessageAttachment
**  Created by: Gavin @2012-05-23
**  Modify by: Gavin @2013-06-24 (ver 1.0.0.1) 修正@ReceiveList没有输入数据的情况下会出错的问题。
**  Modify by: Gavin @2013-07-02 (ver 1.0.0.2) 如果@MessageAccount为空，则使用@ReceiveList。否则使用@MessageAccount
**  Modify by: Gavin @2013-07-31 (ver 1.0.0.3) 修正只发内部消息时，没有accountno 的问题。
**
****************************************************************************/
begin
  declare @TempList varchar(max), @ReceiveMemberID int, @XMLStr xml, @MemberID int, @AccountNumber nvarchar(512)
  declare @MessageStatus int, @ResponseCode int
   
  set @MessageAccount = isnull(@MessageAccount, '') 
  if isnull(@ReceiveList, '') = '' 
    set @ReceiveList = '0'
    
  -- ReceiveList转换成XML格式
  set @XMLStr = convert(xml,'<root><v>'+replace(RTrim(LTrim(@ReceiveList)),',','</v><v>')+'</v></root>')    
  -- 内部消息默认就是成功发送的。 
  if @MessageServiceTypeID = 0
  begin
    set @MessageStatus = 2
    set @ResponseCode = 0
  end else set @MessageStatus = 0
  
  -- 插入MessageObject
  insert into MessageObject(MessageServiceTypeID, MessageType, MessagePriority, MessageCoding, MessageTitle, 
      MessageBody, FromMemberID, IsInternal, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ResponseCode) 
  values (@MessageServiceTypeID, @MessageType, @MessagePriority, @MessageCoding, @MessageTitle, 
      @MessageBody, @SendMemberID, @IsInternal, @MessageStatus, Getdate(), @UserID, Getdate(), @UserID, @ResponseCode)    
  set @MessageID = SCOPE_IDENTITY()
 
  if @MessageAccount <> '' 
  begin
    insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy)
    values ( @MessageID, 0, @MessageAccount, 0, 0, getdate(), @UserID )    
  end else
  begin
    -- 根据@ReceiveList插入MessageReceiveList
    if CharIndex(',', @ReceiveList) > 0
    begin
      insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy)
      select @MessageID, A.MemberID, M.AccountNumber, 0, 0, getdate(), @UserID 
        from (select T.v.value('.','int') as MemberID from @XMLStr.nodes('/root/v') T(v)) A
          left join (select MemberID, MessageServiceTypeID, AccountNumber from MemberMessageAccount 
                       where MessageServiceTypeID = @MessageServiceTypeID) M on A.MemberID = M.MemberID 
      where A.MemberID <> 0 and isnull(M.AccountNumber, '') <> ''        
    end else
    begin
      set @MemberID = cast(@ReceiveList as int)    
      select @AccountNumber = AccountNumber from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = @MessageServiceTypeID
      set @AccountNumber = isnull(@AccountNumber, '')
--print @AccountNumber      
      insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy)
      values ( @MessageID, @MemberID, @AccountNumber, 0, 0, getdate(), @UserID )     
    end
  end
    
  return 0
end

GO
