USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductStyle_Bauhaus]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductStyle_Bauhaus]
  @ColorCodeList        varchar(1000),     -- ÑÕÉ«¹ýÂËÌõ¼þ,ÓÃ,¸ô¿ª, ÀýÈç: red,green,blue
  @MinPrice             money,             -- ¼Û¸ñ¹ýÂËÌõ¼þ
  @MaxPrice             money,             -- ¼Û¸ñ¹ýÂËÌõ¼þ
  @QueryCondition       nvarchar(1000),    -- ×Ô¶¨Òå²éÑ¯Ìõ¼þ
  @OrderCondition	    nvarchar(1000)='',
  @PageCurrent			int=1,			   -- È¡µÚ¼¸Ò³¼ÇÂ¼£¬Ä¬ÈÏ1£¬Ð¡ÓÚµÈÓÚ0Ê±Ò²ÈÏÎª1
  @PageSize				int=0,			   -- Ã¿Ò³¼ÇÂ¼Êý£¬ Îª0Ê±£¬²»·ÖÒ³£¬Ä¬ÈÏ0
  @PageCount			int=0 output,	   -- ·µ»Ø×ÜÒ³Êý¡£
  @RecordCount			int=0 output,	   -- ·µ»Ø×Ü¼ÇÂ¼Êý¡£   
  @LanguageAbbr			varchar(20)='',
  @OrderBy              int=0,             -- ÅÅÐò²ÎÊý¡£0£º²»¼ÓÅÅÐòÌõ¼þ¡£ 1£º order by MinPrice¡£ 2£º order by MINPrice desc. 3: order by ProductBrandName. 4: order by ProductBrandName desc
  @ProductSearchStr     varchar(200)='',   -- ¿ÉÄÜÊÇprodcode, Ò²¿ÉÄÜÊÇprodname
  @PromotionCode        varchar(64)=''     -- °´ÕÕÊäÈëµÄpromotioncode À´¹ýÂË»õÆ·¡£ Îª¿ÕÔò²»×ö¹ýÂË¡£
AS
/****************************************************************************
**  Name : GetProductStyle_Bauhaus  »ñµÃ»õÆ·×éÁÐ±í. Ã¿×é»õÆ·ÏÂÊôµÄ»õÆ·,Ö»ÐèÁÐ³öÎåÖÖÑÕÉ«.
**  Version: 1.0.0.6
**  Description : ÐÂ°æ±¾£¬Ê¹ÓÃÖÐ¼ä±íProductStyle_ListÀ´·µ»Ø»õÆ·styleÁÐ±í¡£ ÎªÊ¹µ÷ÓÃ¶Ë²»×öÐÞ¸Ä£¬ËùÒÔ´æ´¢¹ý³ÌÃû×ÖºÍ²ÎÊý²»×öÐÞ¸Ä¡£
**  Parameter :
 declare @PageCount int, @RecordCount int
 exec GetProductStyle_Bauhaus  '', 0, 0, '', ' ', 1,18, @PageCount output, @RecordCount output, 'zh_BigCN',2, '',''
 print @PageCount
 print @RecordCount
 select * from product where 
**  Created by: Gavin @2016-12-02
**  Modify by: Gavin @2016-12-05 (ver 1.0.0.1) ÐÞ¸Ä@ProductSearchStrÌõ¼þµÄ²éÑ¯¡£
**  Modify by: Gavin @2016-12-07 (ver 1.0.0.2) ÑÕÉ«µÄ·µ»Ø×Ö¶ÎÃû¸ü¸Ä£¬colorcode1 ¸Ä³Écolor1 ¡£¡£¡£¡£
**  Modify by: Gavin @2017-02-24 (ver 1.0.0.3) ·µ»ØCreatedOn, UpdatedOn,  @OrderBy Îª0 Ê±£¬ °´ÕÕ UpdatedOn ÅÅÐò¡£
**  Modify by: Gavin @2017-03-07 (ver 1.0.0.4) ProductStyle_List ±íÐÂÔö×Ö¶ÎInStockFlag. Ôö¼Ó·µ»Ø.
**  Modify by: Gavin @2017-04-06 (ver 1.0.0.5) Ôö¼Ó·µ»ØFlag2, Flag3
**  Modify by: Gavin @2017-06-01 (ver 1.0.0.6) Ôö¼Ó·µ»Ø×Ö¶Î SeasonName1, Ìá¹©¸øpos ¶Ë×÷Îª²éÑ¯Ìõ¼þ.
*
****************************************************************************/
begin
  DECLARE @SQLStr nvarchar(4000), @Language int, @ForeignTable varchar(64), @ForeignID varchar(100)
  DECLARE @idoc int, @ColorStr varchar(1000), @ColorStrTemp varchar(1000), @WhereStr varchar(1000), @ProdCodeWhereStr varchar(1000)
  DECLARE @CD Table(ProdCodeStyle varchar(64), ProdCode varchar(64))
  DECLARE @PL Table(ProdCodeStyle varchar(64), ProdCode varchar(64),ProdName1 nvarchar(400),ProdName2 nvarchar(400),ProdName3 nvarchar(400), 
    ProdPicFile nvarchar(512), ProdType int,  ProdNote nvarchar(512), NonSale int, NewFlag int, Flag1 int, HotSaleFlag int, DepartCode varchar(64), 
	ProdName nvarchar(400), DepartName nvarchar(512), ProductBrandName nvarchar(512), ProductBrandDesc nvarchar(max),
	DepartName1 nvarchar(512), DepartName2 nvarchar(512), DepartName3 nvarchar(512),ProductBrandID int,ProductBrandCode varchar(64), 
	ProductBrandName1 nvarchar(512), ProductBrandName2 nvarchar(512), ProductBrandName3 nvarchar(512),ProductBrandDesc1 nvarchar(max),ProductBrandDesc2 nvarchar(max),
	ProductBrandDesc3 nvarchar(max), ProductBrandPicSFile nvarchar(512),ProductBrandPicMFile nvarchar(512),ProductBrandPicGFile nvarchar(512),
	Color1 varchar(64),Color2 varchar(64),Color3 varchar(64),Color4 varchar(64),Color5 varchar(64),MinPrice money,MaxPrice money,
	MinDefaultPrice money,MaxDefaultPrice money, ColorCodeList varchar(500),ProductSizeIDList  varchar(500),SeasonID int,GenderID int,SeasonCode varchar(64),GenderCode varchar(64))
		  
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  
  set @QueryCondition = isnull(@QueryCondition, '')
  set @OrderCondition = isnull(@OrderCondition, '')
  set @MaxPrice = isnull(@MaxPrice, 0)
  set @MinPrice = isnull(@MinPrice, 0)
  set @WhereStr = ' where 1=1 '
  set @ProdCodeWhereStr = ' where 1=1 '
  set @OrderBy = isnull(@OrderBy, 0)
  set @ProductSearchStr = isnull(@ProductSearchStr, '')
  set @ProdCodeWhereStr = ''
  set @PromotionCode = isnull(@PromotionCode, '')

  if @OrderBy = 0  
    set @OrderCondition = ' order by CreatedOn desc ' 
  else if @OrderBy = 1  
    set @OrderCondition = ' order by MinPrice '
  else if @OrderBy = 2
    set @OrderCondition = ' order by MinPrice desc '
  else if @OrderBy = 3
    set @OrderCondition = ' order by ProductBrandName '
  else if @OrderBy = 4
    set @OrderCondition = ' order by ProductBrandName desc '

  if @MinPrice > 0 
    set @WhereStr = @WhereStr + ' and MinPrice >= ' + cast(@MinPrice as varchar)
  if @MaxPrice > 0 
    set @WhereStr = @WhereStr + ' and MaxPrice <= ' + cast(@MaxPrice as varchar)

  -- for color
  set @ColorCodeList = isnull(@ColorCodeList, '')
  set @ColorStrTemp = @ColorCodeList
  set @ColorStr = ''
  while @ColorStrTemp <> ''
  begin
    if CharIndex(',', @ColorStrTemp) > 0
	begin
	  set @ColorStr = @ColorStr + '''' + SUBSTRING(@ColorStrTemp,1,CharIndex(',', @ColorStrTemp)-1) + ''',' 
	  set @ColorStrTemp = SUBSTRING(@ColorStrTemp,CharIndex(',', @ColorStrTemp)+1, LEN(@ColorStrTemp) - CharIndex(',', @ColorStrTemp))
	end else
	begin
      set @ColorStr = @ColorStr + '''' + @ColorStrTemp + ''''
	  set @ColorStrTemp = '' 	  
	end
  end

--  if @ColorStr <> ''
--  begin
--    set @WhereStr = @WhereStr + '  and C.ColorCode in (' + @ColorStr + ')'
--  end

  if @ProductSearchStr <> '' 
  begin
     set @WhereStr = @WhereStr + ' and ( CHARINDEX(''' + @ProductSearchStr + ''', ProdCodeList) > 0 or CHARINDEX(''' + @ProductSearchStr + ''', ProdName1List) > 0 or CHARINDEX(''' + @ProductSearchStr + ''', ProdName2List) > 0 or CHARINDEX(''' + @ProductSearchStr + ''', ProdName3List) > 0 ) '
/*
    insert into @CD(ProdCodeStyle, ProdCode)
    select B.ProdCodeStyle, A.ProdCode 
	  from Product A left join product_style B on A.ProdCode = B.ProdCode
	 where ((A.ProdCode like @ProductSearchStr + '%') or (A.ProdName1 like '%' + @ProductSearchStr + '%') 
	         or (A.ProdName2 like '%' + @ProductSearchStr + '%') or (A.ProdName3 like '%' + @ProductSearchStr + '%'))	
*/			     
  end
  if @PromotionCode <> ''
  begin
    set @WhereStr = @WhereStr +  ' and ( CHARINDEX(''' + @PromotionCode + ''', PromotionCodeList) > 0 ) '
/*
    insert into @CD(ProdCodeStyle, ProdCode)
    select B.ProdCodeStyle, A.ProdCode 
	  from Product A left join product_style B on A.ProdCode = B.ProdCode
	 where (A.ProdCode in (select EntityNum from BUY_Promotion_Hit_PLU Where EntityType = 1 and PromotionCode =  @PromotionCode) 
	         or A.departcode in (select EntityNum from BUY_Promotion_Hit_PLU Where EntityType = 2 and PromotionCode = @PromotionCode) )	 
*/
  end
  
  set @SQLStr = 'select ProdCodeStyle, ProdCode, ProdName1,ProdName2,ProdName3,ProdPicFile,ProdType,ProdNote,NonSale,NewFlag,Flag1,HotSaleFlag,DepartCode,'
     + 'case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName, '
	 + 'case ' + cast(@Language as varchar) + ' when 2 then DepartName2 when 3 then DepartName3 else DepartName1 end as DepartName,'
     + 'case ' + cast(@Language as varchar) + ' when 2 then ProductBrandName2 when 3 then ProductBrandName3 else ProductBrandName1 end as ProductBrandName, '
	 + 'case ' + cast(@Language as varchar) + ' when 2 then ProductBrandDesc2 when 3 then ProductBrandDesc3 else ProductBrandDesc1 end as ProductBrandDesc, '
     + 'DepartName1,DepartName2,DepartName3,ProductBrandID,ProductBrandCode,ProductBrandName1,ProductBrandName2,ProductBrandName3,ProductBrandDesc1,ProductBrandDesc2,'
	 + 'ProductBrandDesc3,ProductBrandPicSFile,ProductBrandPicMFile,ProductBrandPicGFile,ColorCode1 Color1,ColorCode2 Color2,ColorCode3 Color3,ColorCode4 Color4,ColorCode5 Color5,'
	 + 'MinPrice,MaxPrice,MinDefaultPrice,MaxDefaultPrice,ColorCodeList,ProductSizeIDList,'
	 + 'A.SeasonID,GenderID,A.SeasonCode,A.GenderCode,CreatedOn, UpdatedOn, InStockFlag, Flag2, Flag3, B.SeasonName1 '
	 + ' from ProductStyle_List A '
 	 + ' left join Season B on A.SeasonID = B.SeasonID '
 
	 + @WhereStr 

 -- insert into @PL
  exec SelectDataInBatchs_New @SQLStr, 'ProdCodeStyle', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @QueryCondition

/*
  if @ProductSearchStr <> '' or @PromotionCode <> ''
  begin
    select * from @PL where ProdCodeStyle in (select ProdCodeStyle from @CD)
	set @RecordCount = @@ROWCOUNT
	set @PageCount = 1	
  end else
  begin
    select * from @PL
  end
*/
end

GO
