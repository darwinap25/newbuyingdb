USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_RPRICE_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_RPRICE_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[RPriceCode] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[RPriceTypeCode] [varchar](64) NOT NULL,
	[Price] [money] NOT NULL,
	[RefPrice] [money] NOT NULL,
	[PromotionPrice] [money] NULL,
	[MemberPrice] [money] NULL,
 CONSTRAINT [PK_BUY_RPRICE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BUY_RPRICE_H表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'RPriceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价类型编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'RPriceTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售单价' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'Price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指导价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'RefPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销价格（取消此字段）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'PromotionPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D', @level2type=N'COLUMN',@level2name=N'MemberPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价格明细表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_D'
GO
