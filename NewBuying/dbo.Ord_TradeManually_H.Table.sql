USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_TradeManually_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_TradeManually_H](
	[TradeManuallyCode] [varchar](64) NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[BrandID] [int] NULL,
	[Busdate] [datetime] NULL,
	[TenderID] [int] NULL,
	[Remark] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
 CONSTRAINT [PK_ORD_TRADEMANUALLY_H] PRIMARY KEY CLUSTERED 
(
	[TradeManuallyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_TradeManually_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_TradeManually_H] ON [dbo].[Ord_TradeManually_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_TradeManually_H
* Version: 1.0.0.0
* Description : 手动修改积分的批核触发器
*  
* Create By Gavin @2013-12-02
*/
/*==============================================================*/
BEGIN  
  declare @TradeManuallyCode varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @CardNumber varchar(64), @Busdate datetime, @TraderAmount money, @EarnPoint int, @ReferenceNo varchar(64), @StoreID int
  declare @TxnDate datetime
  			  
  DECLARE CUR_Ord_TradeManually_H CURSOR fast_forward FOR
    --SELECT TradeManuallyCode, CardNumber, Busdate, TraderAmount, EarnPoint, ReferenceNo, StoreID, ApproveStatus, CreatedBy FROM INSERTED
    SELECT TradeManuallyCode, CardNumber, Busdate, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_Ord_TradeManually_H
  --FETCH FROM CUR_Ord_TradeManually_H INTO @TradeManuallyCode, @CardNumber, @Busdate, @TraderAmount, @EarnPoint, @ReferenceNo, @StoreID, @ApproveStatus, @CreatedBy
  FETCH FROM CUR_Ord_TradeManually_H INTO @TradeManuallyCode, @CardNumber, @Busdate, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    set @TxnDate = getdate()
    select @OldApproveStatus = ApproveStatus from Deleted where TradeManuallyCode = @TradeManuallyCode
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec DoTradeManually @TradeManuallyCode, @CreatedBy, @CardNumber, @Busdate, @TxnDate, @ApprovalCode output       
      update Ord_TradeManually_H set ApprovalCode = @ApprovalCode  where TradeManuallyCode = @TradeManuallyCode
    end
    FETCH FROM CUR_Ord_TradeManually_H INTO @TradeManuallyCode, @CardNumber, @Busdate, @ApproveStatus, @CreatedBy
  END
  CLOSE CUR_Ord_TradeManually_H 
  DEALLOCATE CUR_Ord_TradeManually_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'TradeManuallyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'Busdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手工交易单。 头表。 为指定会员添加交易， 实现的目的，通过订单金额实现为目标卡添加积分。（通过预设的规则，根据金额计算获得积分） ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_H'
GO
