USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_StockAdjust_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_StockAdjust_H](
	[StockAdjustNumber] [varchar](64) NOT NULL,
	[OrderType] [int] NULL DEFAULT ((0)),
	[ReferenceNo] [varchar](64) NULL,
	[StoreID] [int] NOT NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_STOCKADJUST_H] PRIMARY KEY CLUSTERED 
(
	[StockAdjustNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_StockAdjust_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_StockAdjust_H] ON [dbo].[Ord_StockAdjust_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_StockAdjust_H
* Version: 1.0.0.1
* Description :
     select * from Ord_StockAdjust_H
     update Ord_StockAdjust_H set approvestatus = 'A' where StoreOrderNumber = 'COPO00000000012'
* Create By Gavin @2015-03-10
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
*/
/*==============================================================*/
BEGIN  
  DECLARE @StockAdjustNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  DECLARE @Busdate DATE, @TxnDate DATETIME, @StoreID int, @StoreCode varchar(64)
  SET @TxnDate = GETDATE()
      
  DECLARE CUR_Ord_StockAdjust_H CURSOR fast_forward FOR
    SELECT StockAdjustNumber, ApproveStatus, CreatedBy, StoreID FROM INSERTED
  OPEN CUR_Ord_StockAdjust_H
  FETCH FROM CUR_Ord_StockAdjust_H INTO @StockAdjustNumber, @ApproveStatus, @CreatedBy, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where StockAdjustNumber = @StockAdjustNumber
    if (@OldApproveStatus = 'P' or ISNULL(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
	  select @StoreCode = StoreCode from BUY_STORE WHERE StoreID = @StoreID
	  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC
      SET @Busdate = ISNULL(@Busdate, GETDATE())
      exec GenApprovalCode @ApprovalCode output
      
      -- 插入movement表
      INSERT INTO STK_StockMovement    -- 店铺出库
        (OprID, StoreID, StockTypeCode, ProdCode, ReferenceNo, ReferenceNo_Other, BusDate, TxnDate, OpenQty, ActQty, CloseQty, 
          SerialNoType, SerialNo, ApprovalCode, CreatedOn, CreatedBy)
      SELECT 15, H.StoreID, D.StockTypeCode, D.ProdCode, D.StockAdjustNumber, '', @Busdate, @TxnDate, ISNULL(O.OnhandQty, 0), ISNULL(D.AdjustQty,0), ISNULL(O.OnhandQty,0) + ISNULL(D.AdjustQty,0),
          0, '', @ApprovalCode, GETDATE(), H.CreatedBy
      FROM Ord_StockAdjust_D D LEFT JOIN Ord_StockAdjust_H H ON D.StockAdjustNumber = H.StockAdjustNumber
        LEFT JOIN STK_StockOnhand O ON D.ProdCode = O.ProdCode AND D.StockTypeCode = O.StockTypeCode AND H.StoreID = O.StoreID
      WHERE D.StockAdjustNumber = @StockAdjustNumber AND ISNULL(D.AdjustQty,0) <> 0
      ---------------------
            
      update Ord_StockAdjust_H set ApprovalCode = @ApprovalCode, ApproveOn = GETDATE(), ApproveBusDate = @Busdate  --, ApproveBy = @CreatedBy
        where StockAdjustNumber = @StockAdjustNumber
    end

    FETCH FROM CUR_Ord_StockAdjust_H INTO @StockAdjustNumber, @ApproveStatus, @CreatedBy, @StoreID    
  END
  CLOSE CUR_Ord_StockAdjust_H 
  DEALLOCATE CUR_Ord_StockAdjust_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'StockAdjustNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存调整订单。@2015-03-10
buying 订单取消 brandid 字段 @2015-03-31' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StockAdjust_H'
GO
