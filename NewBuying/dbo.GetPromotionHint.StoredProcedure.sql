USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetPromotionHint]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPromotionHint]
  @ProdCodes               VARCHAR(4000),			   -- 货品编码. e.g. :  PCode1,PCode2,PCode3  (一个交易单中,多个prodcode 用, 隔开)
  @LanguageAbbr            VARCHAR(20)=''              -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1 
AS
/****************************************************************************
**  Name : GetPromotionHint
**  Version : 1.0.0.0
**  Description : 销售货品时，获得相关的可能失效的Promotion数据 (简单)
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  exec @a = GetPromotionHint '01110001B03',  'zh_CN'
  print @a
  select * from Promotion_H  
**  Created by Gavin @2016-08-29
****************************************************************************/
BEGIN
  DECLARE @TEMPSTR VARCHAR(4000), @PList VARCHAR(4000), @P INT, @SQLStr NVARCHAR(4000), @Language INT
  DECLARE @PL TABLE(ProdCode VARCHAR(64), DepartCode VARCHAR(64))
  SET @TEMPSTR = @ProdCodes
  SET @PList = ''

  IF @TEMPSTR = '' 
    RETURN 0
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1

  WHILE @TEMPSTR <> ''
  BEGIN
    IF @PList <> ''
	  SET @PList = @PList + ','
    SET @P = CHARINDEX(',', @TEMPSTR)
    IF @P > 0 
	BEGIN
	  SET @PList = @PList + '''' +SUBSTRING(@TEMPSTR, 1, @P - 1) + ''''
	  SET @TEMPSTR = SUBSTRING(@TEMPSTR, @P + 1, LEN(@TEMPSTR) - @P)
	END ELSE
	BEGIN
	  SET @PList = @PList + '''' + @TEMPSTR + '''' 
	  SET @TEMPSTR = ''
	END
  END
  
  SET @SQLStr = 'SELECT ProdCode, DepartCode FROM BUY_PRODUCT WHERE PRODCODE IN (' + @PList + ')'

  INSERT INTO @PL
  exec sp_executesql @SQLStr
  
  SELECT PromotionCode, 
      CASE @Language WHEN 2 THEN PromotionDesc2 WHEN 3 THEN PromotionDesc3 ELSE PromotionDesc1 END AS PromotionDesc
    FROM Promotion_H WHERE ApproveStatus = 'A' AND PromotionCode IN
      (SELECT PromotionCode FROM Promotion_Hit_PLU A LEFT JOIN @PL B ON (A.EntityType = 1 AND A.EntityNum = B.ProdCode) OR (A.EntityType = 2 AND A.EntityNum = B.DepartCode)
	     WHERE B.ProdCode IS NOT NULL)
	  
  RETURN 0
END

GO
