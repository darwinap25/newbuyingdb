USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenSalesShipOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[GenSalesShipOrder]
  @UserID				         INT,	            -- 操作员ID
  @SalesPickOrderNumber          VARCHAR(64)        -- 单号码
AS
/****************************************************************************
**  Name : GenSalesShipOrder 
**  Version: 1.0.0.0
**  Description : 由SalesPickUp单产生送货单. 
select * from Ord_SalesPickOrder_H
select * from Ord_SalesShipOrder_D
**  Created by: Gavin @2016-05-24  
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @TxnDate DATETIME, @StoreCode varchar(64)

  IF EXISTS(SELECT * FROM Ord_SalesPickOrder_D WHERE SalesPickOrderNumber = @SalesPickOrderNumber AND ISNULL(ActualQty, 0) > 0) 
  BEGIN
	  EXEC GetRefNoString 'SSO', @NewNumber OUTPUT

	  INSERT INTO Ord_SalesShipOrder_D(SalesShipOrderNumber,ProdCode,OrderQty,Remark)
	  SELECT @NewNumber, ProdCode, ISNULL(ActualQty, 0), '' FROM Ord_SalesPickOrder_D 
	      WHERE SalesPickOrderNumber = @SalesPickOrderNumber AND ISNULL(ActualQty, 0) > 0
	  IF @@ROWCOUNT <> 0
	  BEGIN

		  INSERT INTO Ord_SalesShipOrder_H(SalesShipOrderNumber,OrderType,MemberID,CardNumber,ReferenceNo,TxnNo,
			  DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,
			  DeliveryFullAddress,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,
			  DeliveryDate,DeliveryBy,Remark,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		  SELECT @NewNumber, OrderType, MemberID,CardNumber,@SalesPickOrderNumber,ReferenceNo,
			  DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,
			  DeliveryFullAddress,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,
			  DeliveryDate,DeliveryBy,Remark,0,GETDATE(),@UserID,GETDATE(),@UserID 
		  FROM Ord_SalesPickOrder_H WHERE SalesPickOrderNumber = @SalesPickOrderNumber
	  END
  END
  RETURN 0
END

GO
