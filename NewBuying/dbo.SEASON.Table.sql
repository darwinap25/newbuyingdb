USE [NewBuying]
GO
/****** Object:  Table [dbo].[SEASON]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SEASON](
	[SeasonID] [int] IDENTITY(1,1) NOT NULL,
	[SeasonCode] [varchar](64) NULL,
	[SeasonName1] [varchar](512) NULL,
	[SeasonName2] [varchar](512) NULL,
	[SeasonName3] [varchar](512) NULL,
 CONSTRAINT [PK_SEASON] PRIMARY KEY CLUSTERED 
(
	[SeasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SEASON', @level2type=N'COLUMN',@level2name=N'SeasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'季节编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SEASON', @level2type=N'COLUMN',@level2name=N'SeasonCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SEASON', @level2type=N'COLUMN',@level2name=N'SeasonName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SEASON', @level2type=N'COLUMN',@level2name=N'SeasonName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SEASON', @level2type=N'COLUMN',@level2name=N'SeasonName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'季节。  for Bauhaus
@2016-02-24
@2016-07-19 seasonid 改成自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SEASON'
GO
