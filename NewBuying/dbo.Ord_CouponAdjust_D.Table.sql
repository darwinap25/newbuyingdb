USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponAdjust_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponAdjust_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponAdjustNumber] [varchar](64) NOT NULL,
	[CouponNumber] [varchar](64) NOT NULL,
	[CouponAmount] [money] NULL,
	[ExchangeStatus] [char](1) NULL,
 CONSTRAINT [PK_Ord_CouponAdjust_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_D', @level2type=N'COLUMN',@level2name=N'CouponAdjustNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_D', @level2type=N'COLUMN',@level2name=N'CouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际操作金额。都是正数， 根据实际的操作，来判断增减。
33、激活指定coupon  （加金额）
34、使用指定coupon  （减金额）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_D', @level2type=N'COLUMN',@level2name=N'CouponAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵调整子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponAdjust_D'
GO
