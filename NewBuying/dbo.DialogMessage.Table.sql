USE [NewBuying]
GO
/****** Object:  Table [dbo].[DialogMessage]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DialogMessage](
	[TAPCode] [nvarchar](50) NOT NULL,
	[OriginalCode] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[Project] [nvarchar](512) NULL,
	[Product] [nvarchar](512) NULL,
	[ModifiedBy] [nvarchar](512) NULL,
	[AddDate] [datetime] NULL,
	[MessageIconDisplay] [bit] NULL,
	[ButtonDisplay] [bit] NULL,
 CONSTRAINT [PK_DIALOGMESSAGE] PRIMARY KEY CLUSTERED 
(
	[TAPCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
