USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberClause]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberClause](
	[MemberClauseID] [int] IDENTITY(1,1) NOT NULL,
	[BrandID] [int] NULL,
	[ClauseTypeCode] [varchar](64) NOT NULL,
	[ClauseSubCode] [varchar](64) NULL,
	[ClauseName] [nvarchar](512) NULL,
	[ClauseName2] [nvarchar](512) NULL,
	[ClauseName3] [nvarchar](512) NULL,
	[MemberClauseDesc1] [nvarchar](max) NULL,
	[MemberClauseDesc2] [nvarchar](max) NULL,
	[MemberClauseDesc3] [nvarchar](max) NULL,
 CONSTRAINT [PK_MEMBERCLAUSE] PRIMARY KEY CLUSTERED 
(
	[MemberClauseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'MemberClauseID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条款类型编号。区分全局的，或者局部功能的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'ClauseTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子项编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'ClauseSubCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条款描述1
（2,3是添加的字段，为保证之前程序部出错，不使用ClauseName1，还是用ClauseName）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'ClauseName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条款描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'ClauseName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条款描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'ClauseName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条款内容1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'MemberClauseDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条款内容2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'MemberClauseDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条款内容3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause', @level2type=N'COLUMN',@level2name=N'MemberClauseDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员条款' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberClause'
GO
