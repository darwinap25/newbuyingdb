USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SplitSalesOrderByStock_BUY]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[SplitSalesOrderByStock_BUY]
  @UserID                           INT,                   -- 操作员ID
  @TxnNo                            VARCHAR(64)            -- 交易号码
AS
/****************************************************************************
**  Name : SplitSalesOrderByDept_BUY  （BUYING DB）
**  Version: 1.0.0.0
**  Description : 拆分交易单 （根据库存数量）. ( FOR Bauhaus)
    exec SplitSalesOrderByStock_BUY 666,'20151010watsons214R01000006'
**  Created by: Gavin @2016-05-05   
****************************************************************************/
BEGIN
  DECLARE @DeptCode VARCHAR(64), @NewTxnNo VARCHAR(64), @NewTotal MONEY, @StoreID INT, @Count INT,
          @Status INT, @RETURN INT, @ApprovalCode VARCHAR(6)
  DECLARE @BusDate DATETIME, @TxnDate DATETIME, @StoreCode VARCHAR(64), @stocktypecode VARCHAR(64)
  DECLARE @DeptList TABLE(DepartCode VARCHAR(64), NewTotal MONEY)

  SELECT * INTO #Sales_D_Temp FROM Sales_D WHERE TransNum = @TxnNo
  SELECT * INTO #Sales_H_Temp FROM Sales_H WHERE TransNum = @TxnNo
  SELECT @StoreID = B.StoreID, @Status = A.Status, @StoreCode = A.StoreCode,  @BusDate = A.BusDate, @TxnDate = A.TxnDate 
    FROM #Sales_H_Temp A LEFT JOIN buy_store B ON A.StoreCode = B.StoreCode
  SET @RETURN = 0
  SET @stocktypecode = 'G'
  SET @StoreID = ISNULL(@StoreID, 0)

  IF @Status = 4
  BEGIN
	  IF EXISTS(SELECT * FROM #Sales_D_Temp A LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') B ON A.ProdCode = B.ProdCode
				WHERE B.OnhandQty < TotalQty )
	  BEGIN
	   -- 够库存的货品
		exec GetRefNoString 'SPTXNNO', @NewTxnNo output 

		INSERT INTO Sales_D(TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, DepartCode,
			ProdCode, ProdDesc, Serialno, Collected, OrgPrice, OrgAmount, UnitAmount, TotalQty, DiscountPrice, DiscountAmount, POPrice, 
			ExtraPrice, POReasonCode, RPriceTypeCode, SerialNoType, IMEI, StockTypeCode, PickupLocation, PickupStaff,
			PickupDate, DeliveryDate, DeliveryBy, OrgTransNum, OrgSeqNo, Remark, RefGUID,
			UnitPrice, NetPrice, Qty, NetAmount, Additional1,Additional2,Additional3, CreatedBy, UpdatedBy, ReservedDate)
		select @NewTxnNo, SeqNo, TransType, StoreCode, RegisterCode, BusDate, DepartCode,
			A.ProdCode, ProdDesc, Serialno, Collected, OrgPrice, CASE WHEN B.OnhandQty < A.TotalQty AND A.Collected =2 THEN OrgPrice * B.OnhandQty ELSE OrgPrice * A.TotalQty END, 
			 CASE WHEN B.OnhandQty < A.TotalQty AND A.Collected =2 THEN UnitPrice * B.OnhandQty ELSE UnitPrice * A.TotalQty END, 
			 CASE WHEN B.OnhandQty < A.TotalQty AND A.Collected =2 THEN B.OnhandQty ELSE A.TotalQty END, DiscountPrice, DiscountAmount, POPrice, 
			ExtraPrice, POReasonCode, RPriceTypeCode, SerialNoType, IMEI, A.StockTypeCode, PickupLocation, PickupStaff,
			PickupDate, DeliveryDate, DeliveryBy, OrgTransNum, OrgSeqNo, Remark, RefGUID,
			UnitPrice, NetPrice, CASE WHEN B.OnhandQty < A.TotalQty AND A.Collected =2 THEN B.OnhandQty ELSE A.TotalQty END, 
			CASE WHEN B.OnhandQty < A.TotalQty AND A.Collected =2 THEN NetPrice * B.OnhandQty ELSE NetPrice * A.TotalQty END, 
			Additional1,Additional2,Additional3, @UserID, @UserID, ReservedDate 
		  from #Sales_D_Temp A LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') B ON A.ProdCode = B.ProdCode
		WHERE (B.OnhandQty > 0 AND B.ProdCode IS NOT NULL) OR A.Collected <> 2
    
		SELECT @NewTotal = SUM(ISNULL(NetAmount,0)), @Count = Count(*) FROM Sales_D WHERE TransNum = @NewTxnNo
		IF ISNULL(@Count, 0) > 0
		BEGIN
		  INSERT INTO Sales_H (TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,TotalAmount,Status,
			TransDiscount,TransDiscountType,TransReason,RefTransNum,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
			DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
			SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		  SELECT @NewTxnNo,20,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,@NewTotal,5,
			TransDiscount,TransDiscountType,TransReason,@TxnNo,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
			DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
			SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy
		  FROM #Sales_H_Temp
		END

		 -- 够库存货品，需要扣库存。
		  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
			  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo,ApprovalCode,CreatedOn,CreatedBy)
		  SELECT 4, @StoreID, @StockTypeCode, A.ProdCode, A.TransNum, '', @Busdate, @Txndate,
			  ISNULL(B.OnhandQty, 0), -ISNULL(A.Qty,0), ISNULL(B.OnhandQty,0) - ISNULL(A.Qty,0), 1, A.Serialno, @ApprovalCode, GETDATE(), @UserID  
		  from Sales_D A left join (select * from STK_StockOnhand where StoreID = @StoreID and StockTypeCode = @StockTypeCode) B 
							on A.ProdCode = B.ProdCode  
		  WHERE A.TransNum = @NewTxnNo AND A.Collected = 2
		  UPDATE Sales_D SET Collected = 4 WHERE TransNum = @NewTxnNo AND Collected = 2

	   -- 库存不够的货品
		exec GetRefNoString 'SPTXNNO', @NewTxnNo output 
		INSERT INTO Sales_D(TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, DepartCode,
			ProdCode, ProdDesc, Serialno, Collected, OrgPrice, OrgAmount, UnitAmount, TotalQty, DiscountPrice, DiscountAmount, POPrice, 
			ExtraPrice, POReasonCode, RPriceTypeCode, SerialNoType, IMEI, StockTypeCode, PickupLocation, PickupStaff,
			PickupDate, DeliveryDate, DeliveryBy, OrgTransNum, OrgSeqNo, Remark, RefGUID,
			UnitPrice, NetPrice, Qty, NetAmount, Additional1,Additional2,Additional3, CreatedBy, UpdatedBy, ReservedDate)
		select @NewTxnNo, SeqNo, TransType, StoreCode, RegisterCode, BusDate, DepartCode,
			A.ProdCode, ProdDesc, Serialno, 0, OrgPrice, OrgPrice * ABS(ISNULL(B.OnhandQty,0) - A.TotalQty), UnitPrice * B.OnhandQty,B.OnhandQty, DiscountPrice, DiscountAmount, POPrice, 
			ExtraPrice, POReasonCode, RPriceTypeCode, SerialNoType, IMEI, A.StockTypeCode, PickupLocation, PickupStaff,
			PickupDate, DeliveryDate, DeliveryBy, OrgTransNum, OrgSeqNo, Remark, RefGUID,
			UnitPrice, NetPrice, ABS(ISNULL(B.OnhandQty,0) - A.TotalQty), NetPrice * ABS(ISNULL(B.OnhandQty,0) - A.TotalQty), Additional1,Additional2,Additional3, @UserID, @UserID, ReservedDate 
		  from #Sales_D_Temp A LEFT JOIN (SELECT ProdCode, CASE WHEN ISNULL(OnhandQty,0) < 0 THEN 0 ELSE OnhandQty END AS OnhandQty FROM STK_StockOnhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') B ON A.ProdCode = B.ProdCode
		WHERE (ISNULL(B.OnhandQty,0) < TotalQty OR B.ProdCode IS NULL) AND A.Collected = 2
    
		SELECT @NewTotal = SUM(ISNULL(NetAmount,0)), @Count = Count(*) FROM Sales_D WHERE TransNum = @NewTxnNo
		IF ISNULL(@Count, 0) > 0
		BEGIN
		  INSERT INTO Sales_H (TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,TotalAmount,Status,
			TransDiscount,TransDiscountType,TransReason,RefTransNum,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
			DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
			SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		  SELECT @NewTxnNo,20,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,@NewTotal,4,
			TransDiscount,TransDiscountType,TransReason,@TxnNo,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
			DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
			SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy
		  FROM #Sales_H_Temp
		END

		UPDATE Sales_H SET Status = 10, UpdatedOn = GETDATE(), UpdatedBy = @UserID  WHERE TransNum = @TxnNo
	  END ELSE
	    SET @RETURN = -1
  END ELSE
    SET @RETURN = -2

  DROP TABLE #Sales_D_Temp
  DROP TABLE #Sales_H_Temp

  RETURN @RETURN
END

GO
