USE [NewBuying]
GO
/****** Object:  Table [dbo].[Card]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Card](
	[CardNumber] [varchar](64) NOT NULL,
	[CardTypeID] [int] NULL,
	[CardIssueDate] [datetime] NOT NULL,
	[CardExpiryDate] [datetime] NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[MemberID] [int] NULL,
	[BatchCardID] [int] NULL,
	[Status] [int] NOT NULL DEFAULT ((0)),
	[UsedCount] [int] NOT NULL DEFAULT ((0)),
	[CardPassword] [varchar](512) NULL,
	[TotalPoints] [int] NULL DEFAULT ((0)),
	[TotalAmount] [money] NULL DEFAULT ((0)),
	[ParentCardNumber] [varchar](64) NULL,
	[CardForfeitPoints] [int] NULL DEFAULT ((0)),
	[CardForfeitAmount] [money] NULL DEFAULT ((0)),
	[PasswordExpiryDate] [datetime] NULL,
	[PWDExpiryPromptDays] [int] NULL,
	[ResetPassword] [int] NULL DEFAULT ((0)),
	[CardAmountExpiryDate] [datetime] NULL,
	[CardPointExpiryDate] [datetime] NULL,
	[CumulativeEarnPoints] [int] NULL,
	[CumulativeConsumptionAmt] [money] NULL,
	[StockStatus] [int] NULL DEFAULT ((0)),
	[IssueStoreID] [int] NULL,
	[ActiveStoreID] [int] NULL,
	[PickupFlag] [int] NULL DEFAULT ((0)),
	[ActiveDate] [datetime] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [Card_PK] PRIMARY KEY CLUSTERED 
(
	[CardNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Card]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Card] ON [dbo].[Card]
For INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Card
* Version: 1.0.0.0
* Description : 卡级别升级触发器
* Create By Gavin @2012-03-19
*/
/*==============================================================*/
BEGIN
  declare @OldPoints int, @NewPoints int, @CardNumber varchar(512), @CardTypeID int
  select @CardNumber = CardNumber, @CardTypeID = CardTypeID, @NewPoints = TotalPoints FROM INSERTED 
  select @OldPoints = TotalPoints FROM DELETED
  if @NewPoints <> @OldPoints
    exec UpdateCardGrade @CardNumber, @CardTypeID            
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号。 作为唯一主键。 查询时，只输入卡号就可以唯一定位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡发行日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardIssueDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡过期日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别（CardGrad表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BatchCard表自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'BatchCardID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡状态：
0:	Dormant (need active)
1:	issued （need active）	 		                
2:	Active (can be used) 		               
3:	already redeem  (can be used)                   		                
4:	already expiry
5:	already void	
6:             Recycled
7:             inactive （未验证）
8:             Deactive （会员主动注销（记录保留））
9:             frozen（和8一样效果，我方发起冻结用户）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'UsedCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡密码。 存放的密码是 MD5的密文。
初始密码为 123456 （MD5 加密后存放）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'积分余额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'TotalPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金额余额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'TotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主卡号。 主卡必须相同cardtype' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'ParentCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效的积分余额。（累计值）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardForfeitPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效的金额余额。（累计值）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardForfeitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡密码失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'PasswordExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡密码失效提示预警天数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'PWDExpiryPromptDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要重置密码。默认 0
 0：不需要。  1：需要重置。2：密码过期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'ResetPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡中金额有效期。 如果是多有效期， 这里取最大有效期。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardAmountExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡中积分有效期。 如果是多有效期， 这里取最大有效期。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CardPointExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累计获得的积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CumulativeEarnPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累计消费金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'CumulativeConsumptionAmt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实体Card的库存状态。电子卡为0
1. For Printing (總部未確認收貨)
2. Good For Release (總部確認收貨)
3. Damaged (總部發現有優惠劵損壞)
4. Picked (總部收到了店舖的訂單)
5. In Transit (總部收到了店舖的訂單, 並發貨)
6. In Location (店舖確認收貨)
7. Returned (店舖退貨給總部)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'StockStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行店铺。（Card送货，送达的店铺）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'IssueStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'激活的店铺。（销售Card的店铺）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'ActiveStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'捡取标志，0：未被捡取。1：已被捡取。默认为0。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'PickupFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡的激活日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card', @level2type=N'COLUMN',@level2name=N'ActiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card'
GO
