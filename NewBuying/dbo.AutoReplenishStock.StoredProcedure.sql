USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoReplenishStock]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[AutoReplenishStock]
AS
/****************************************************************************
**  Name : AutoReplenishStock  (bauhaus)
**  Version: 1.0.0.0
**  Description : 自动补货
declare @a int
  exec @a = AutoReplenishStock 
print @a
select* from Ord_StoreOrder_H
select* from BUY_REPLENFORMULA
**  Created by: Gavin @2016-07-14   
****************************************************************************/
BEGIN
  DECLARE @ReplenCode VARCHAR(64), @UseReplenFormula INT, @StoreID INT, @TargetType INT, @OrderTargetID INT, 
          @NewNumber VARCHAR(64), @ReplenFormulaID INT, @MinStockQty INT, @RunningStockQty INT, @OrderRoundUpQty INT,
		  @StoreCode VARCHAR(64), @BusDate DATETIME
  DECLARE @ProdCode VARCHAR(64), @Standard_Cost MONEY, @AVG_Cost MONEY, @Export_Cost MONEY, @OnhandQty INT
  DECLARE @CalcProductDaliyQty DECIMAL(12,4), @PreDefine INT, @RunningStock INT, @RoundUpQty INT

  DECLARE CUR_AutoReplenishStocke CURSOR fast_forward FOR
    SELECT A.ReplenCode, A.UseReplenFormula, A.ReplenFormulaID, A.StoreID, A.TargetType, A.OrderTargetID, B.StoreCode,
	       C.PreDefineDOC, C.RunningStockDOC, C.OrderRoundUpQty
	    FROM BUY_REPLEN_H A
	    LEFT JOIN BUY_STORE B ON A.StoreID = B.StoreID
		LEFT JOIN BUY_REPLENFORMULA C ON A.ReplenFormulaID = C.ReplenFormulaID
	  where A.Status = 1 AND DATEDIFF(DD, A.StartDate, GETDATE()) >= 0 AND DATEDIFF(DD, GETDATE(), A.EndDate) >= 0
    ORDER BY [Priority]
  OPEN CUR_AutoReplenishStocke
  FETCH FROM CUR_AutoReplenishStocke INTO @ReplenCode, @UseReplenFormula, @ReplenFormulaID, @StoreID, 
      @TargetType, @OrderTargetID, @StoreCode, @PreDefine, @RunningStock, @RoundUpQty
  WHILE @@FETCH_STATUS=0
  BEGIN 
    SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC

    IF @UseReplenFormula = 0   -- 不使用公式， 则使用BUY_REPLEN_D中的设置。
	BEGIN		
		IF @TargetType = 0   -- 向供应商订货
		BEGIN
		  EXEC GetRefNoString 'PO', @NewNumber OUTPUT

		  SELECT TOP 1 @MinStockQty = MinStockQty, @RunningStockQty = RunningStockQty, @OrderRoundUpQty = OrderRoundUpQty 
		    FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 0
		  IF (ISNULL(@RunningStockQty, 0) > 0)
		  BEGIN		    
			INSERT INTO BUY_PO_D(POCode, ProdCode, Cost,AverageCost,UnitCost,Qty,FreeQty,GRNQty)
			SELECT @NewNumber, P.ProdCode, U.Standard_Cost, U.AVG_Cost, U.Export_Cost, 
			    (CEILING(CAST((@RunningStockQty - O.OnhandQty) AS float) / @OrderRoundUpQty) * @OrderRoundUpQty) AS ReplenQty, 0, 0
			  FROM buy_product P 
			  LEFT JOIN BUY_PRODUCT_ADD_BAU U ON P.ProdCode = U.ProdCode
			  LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
            WHERE O.OnhandQty < @MinStockQty  

			INSERT INTO BUY_PO_H(POCode,OrderType,VendorID,StoreID,CurrencyCode,Note1,Note2,Note3,ExpectDate,TotalAmount,
			  VATAmount,DiscountAmount,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
			  ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
			VALUES(@NewNumber, 1, @OrderTargetID, @StoreID, '', 'Auto Replenish Stock','','',GETDATE()+100, 0,0,0,@BusDate,null,'', 'P',null,null,GETDATE(),1,GETDATE(),1)
		  END ELSE
		  BEGIN
		    INSERT INTO BUY_PO_D(POCode, ProdCode, Cost,AverageCost,UnitCost,Qty,FreeQty,GRNQty)
			SELECT @NewNumber, P.ProdCode, U.Standard_Cost, U.AVG_Cost, U.Export_Cost, 
			    (CEILING(CAST((@RunningStockQty - O.OnhandQty) AS float) / @OrderRoundUpQty) * @OrderRoundUpQty) AS ReplenQty, 0, 0
			  FROM buy_product P 
			  LEFT JOIN BUY_PRODUCT_ADD_BAU U ON P.ProdCode = U.ProdCode
			  LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
			  LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 1) A ON A.EntityCode = P.ProdCode
			  LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 2) B ON B.EntityCode = P.DepartCode
            WHERE O.OnhandQty < @MinStockQty AND (A.EntityCode IS NOT NULL OR B.EntityCode IS NOT NULL)

			INSERT INTO BUY_PO_H(POCode,OrderType,VendorID,StoreID,CurrencyCode,Note1,Note2,Note3,ExpectDate,TotalAmount,
			  VATAmount,DiscountAmount,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
			  ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
			VALUES(@NewNumber, 1, @OrderTargetID, @StoreID, '', 'Auto Replenish Stock','','',GETDATE()+100, 0,0,0,@BusDate,null,'', 'P',null,null,GETDATE(),1,GETDATE(),1)
		  END
		END
		IF @TargetType = 1   -- 向总部订货（或者是其他店铺）
		BEGIN
		  EXEC GetRefNoString 'SP', @NewNumber OUTPUT
		  
		  SELECT TOP 1 @MinStockQty = MinStockQty, @RunningStockQty = RunningStockQty, @OrderRoundUpQty = OrderRoundUpQty 
		    FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 0

		  IF (ISNULL(@RunningStockQty, 0) > 0)
		  BEGIN		    
			INSERT INTO Ord_StoreOrder_D(StoreOrderNumber, ProdCode, OrderQty)
			SELECT @NewNumber, P.ProdCode, (CEILING(CAST((@RunningStockQty - O.OnhandQty) AS float) / @OrderRoundUpQty) * @OrderRoundUpQty) AS ReplenQty  
			  FROM buy_product P 
			  LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
            WHERE O.OnhandQty < @MinStockQty 			
			 
			INSERT INTO Ord_StoreOrder_H(StoreOrderNumber,OrderType,ReferenceNo,FromStoreID,FromContactName,
			  FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
			  StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
			  ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
            SELECT @NewNumber, 1, @ReplenCode, A.OrderTargetID,C.Contact,C.StoreTel,C.ContactPhone,C.StoreEMail,C.StoreFullDetail1,
			     A.StoreID,B.Contact,B.StoreTel,B.ContactPhone,B.StoreEMail,B.StoreFullDetail1,'Auto Replenish Stock',
				 @BusDate,@BusDate,'', 'P',null,null,GETDATE(),1,GETDATE(),1
			   FROM BUY_REPLEN_H A LEFT JOIN BUY_STORE B ON A.StoreID = B.StoreID
			   LEFT JOIN BUY_STORE C ON A.OrderTargetID = C.StoreID
			WHERE ReplenCode = @ReplenCode

		  END ELSE
		  BEGIN
			INSERT INTO Ord_StoreOrder_D(StoreOrderNumber, ProdCode, OrderQty)
			SELECT @NewNumber, P.ProdCode, (CEILING(CAST((@RunningStockQty - O.OnhandQty) AS float) / @OrderRoundUpQty) * @OrderRoundUpQty) AS ReplenQty  			  
			  FROM buy_product P 
			  LEFT JOIN BUY_PRODUCT_ADD_BAU U ON P.ProdCode = U.ProdCode
			  LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
			  LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 1) A ON A.EntityCode = P.ProdCode
			  LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 2) B ON B.EntityCode = P.DepartCode
            WHERE O.OnhandQty < @MinStockQty AND (A.EntityCode IS NOT NULL OR B.EntityCode IS NOT NULL)

			INSERT INTO Ord_StoreOrder_H(StoreOrderNumber,OrderType,ReferenceNo,FromStoreID,FromContactName,
			  FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
			  StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
			  ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
            SELECT @NewNumber, 1, @ReplenCode, A.OrderTargetID,C.Contact,C.StoreTel,C.ContactPhone,C.StoreEMail,C.StoreFullDetail1,
			     A.StoreID,B.Contact,B.StoreTel,B.ContactPhone,B.StoreEMail,B.StoreFullDetail1,'Auto Replenish Stock',
				 @BusDate,@BusDate,'', 'P',null,null,GETDATE(),1,GETDATE(),1
			   FROM BUY_REPLEN_H A LEFT JOIN BUY_STORE B ON A.StoreID = B.StoreID
			   LEFT JOIN BUY_STORE C ON A.OrderTargetID = C.StoreID
			WHERE ReplenCode = @ReplenCode
		  END
		END
    END
	ELSE             ---- 使用公式
	BEGIN        	  
	  IF @TargetType = 0   -- 向供应商订货
	  BEGIN
	      EXEC GetRefNoString 'PO', @NewNumber OUTPUT
		  IF EXISTS(SELECT * FROM  BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 0) 
			OR NOT EXISTS(SELECT * FROM  BUY_REPLEN_D WHERE ReplenCode = @ReplenCode)
			  DECLARE CUR_AutoReplenishStocke_Detail CURSOR fast_forward FOR
				  SELECT P.ProdCode, U.Standard_Cost, U.AVG_Cost, U.Export_Cost, O.OnhandQty,DBO.CalcProductDaliySales(@ReplenFormulaID,P.ProdCode,0)
					FROM buy_product P 
					LEFT JOIN BUY_PRODUCT_ADD_BAU U ON P.ProdCode = U.ProdCode
					LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
		  ELSE
			  DECLARE CUR_AutoReplenishStocke_Detail CURSOR fast_forward FOR
				  SELECT P.ProdCode, U.Standard_Cost, U.AVG_Cost, U.Export_Cost, O.OnhandQty,DBO.CalcProductDaliySales(@ReplenFormulaID,P.ProdCode,0)
					FROM buy_product P 
					LEFT JOIN BUY_PRODUCT_ADD_BAU U ON P.ProdCode = U.ProdCode
					LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
					LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 1) A ON A.EntityCode = P.ProdCode
					LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 2) B ON B.EntityCode = P.DepartCode
				  WHERE (A.EntityCode IS NOT NULL OR B.EntityCode IS NOT NULL)	    
		  OPEN CUR_AutoReplenishStocke_Detail
		  FETCH FROM CUR_AutoReplenishStocke_Detail INTO @ProdCode, @Standard_Cost, @AVG_Cost, @Export_Cost, @OnhandQty,@CalcProductDaliyQty
		  WHILE @@FETCH_STATUS=0
		  BEGIN 
		      IF @OnhandQty < (@CalcProductDaliyQty * @PreDefine)
			  BEGIN 
				INSERT INTO BUY_PO_D(POCode, ProdCode, Cost,AverageCost,UnitCost,Qty,FreeQty,GRNQty)
				VALUES(@NewNumber, @ProdCode, @Standard_Cost, @AVG_Cost, @Export_Cost, (CEILING(CAST((@RunningStock * @CalcProductDaliyQty - @OnhandQty) AS float) / @RoundUpQty) * @RoundUpQty), 0, 0)
              END
			  FETCH FROM CUR_AutoReplenishStocke_Detail INTO @ProdCode, @Standard_Cost, @AVG_Cost, @Export_Cost, @OnhandQty, @CalcProductDaliyQty
		  END
		  CLOSE CUR_AutoReplenishStocke_Detail 
		  DEALLOCATE CUR_AutoReplenishStocke_Detai

	      INSERT INTO BUY_PO_H(POCode,OrderType,VendorID,StoreID,CurrencyCode,Note1,Note2,Note3,ExpectDate,TotalAmount,
				  VATAmount,DiscountAmount,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
				  ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
          VALUES(@NewNumber, 1, @OrderTargetID, @StoreID, '', 'Auto Replenish Stock','','',GETDATE()+100, 0,0,0,@BusDate,null,'', 'P',null,null,GETDATE(),1,GETDATE(),1)
	  END
      ELSE      -- 向店铺订货
	  BEGIN
	      EXEC GetRefNoString 'SP', @NewNumber OUTPUT
		  IF EXISTS(SELECT * FROM  BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 0) 
			OR NOT EXISTS(SELECT * FROM  BUY_REPLEN_D WHERE ReplenCode = @ReplenCode)
			  DECLARE CUR_AutoReplenishStocke_Detail CURSOR fast_forward FOR
				  SELECT P.ProdCode, U.Standard_Cost, U.AVG_Cost, U.Export_Cost, O.OnhandQty,DBO.CalcProductDaliySales(@ReplenFormulaID,P.ProdCode,0)
					FROM buy_product P 
					LEFT JOIN BUY_PRODUCT_ADD_BAU U ON P.ProdCode = U.ProdCode
					LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
		  ELSE
			  DECLARE CUR_AutoReplenishStocke_Detail CURSOR fast_forward FOR
				  SELECT P.ProdCode, U.Standard_Cost, U.AVG_Cost, U.Export_Cost, O.OnhandQty,DBO.CalcProductDaliySales(@ReplenFormulaID,P.ProdCode,0)
					FROM buy_product P 
					LEFT JOIN BUY_PRODUCT_ADD_BAU U ON P.ProdCode = U.ProdCode
					LEFT JOIN (SELECT * FROM stk_stockonhand WHERE StoreID = @StoreID AND StockTypeCode = 'G') O ON P.ProdCode = O.ProdCode
					LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 1) A ON A.EntityCode = P.ProdCode
					LEFT JOIN (SELECT EntityCode FROM BUY_REPLEN_D WHERE ReplenCode = @ReplenCode AND EntityType = 2) B ON B.EntityCode = P.DepartCode
				  WHERE (A.EntityCode IS NOT NULL OR B.EntityCode IS NOT NULL)	    
		  OPEN CUR_AutoReplenishStocke_Detail
		  FETCH FROM CUR_AutoReplenishStocke_Detail INTO @ProdCode, @Standard_Cost, @AVG_Cost, @Export_Cost, @OnhandQty,@CalcProductDaliyQty
		  WHILE @@FETCH_STATUS=0
		  BEGIN 
		      IF @OnhandQty < (@CalcProductDaliyQty * @PreDefine)
			  BEGIN 
				INSERT INTO Ord_StoreOrder_D(StoreOrderNumber, ProdCode, OrderQty)
				VALUES(@NewNumber, @ProdCode, (CEILING(CAST((@RunningStock * @CalcProductDaliyQty - @OnhandQty) AS float) / @RoundUpQty) * @RoundUpQty))
              END
			  FETCH FROM CUR_AutoReplenishStocke_Detail INTO @ProdCode, @Standard_Cost, @AVG_Cost, @Export_Cost, @OnhandQty, @CalcProductDaliyQty
		  END
		  CLOSE CUR_AutoReplenishStocke_Detail 
		  DEALLOCATE CUR_AutoReplenishStocke_Detai

		  INSERT INTO Ord_StoreOrder_H(StoreOrderNumber,OrderType,ReferenceNo,FromStoreID,FromContactName,
			  FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
			  StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
			  ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
          SELECT @NewNumber, 1, @ReplenCode, A.OrderTargetID,C.Contact,C.StoreTel,C.ContactPhone,C.StoreEMail,C.StoreFullDetail1,
			     A.StoreID,B.Contact,B.StoreTel,B.ContactPhone,B.StoreEMail,B.StoreFullDetail1,'Auto Replenish Stock',
				 @BusDate,@BusDate,'', 'P',null,null,GETDATE(),1,GETDATE(),1
			   FROM BUY_REPLEN_H A LEFT JOIN BUY_STORE B ON A.StoreID = B.StoreID
			   LEFT JOIN BUY_STORE C ON A.OrderTargetID = C.StoreID
			WHERE ReplenCode = @ReplenCode      
	  END
	END

    FETCH FROM CUR_AutoReplenishStocke INTO @ReplenCode, @UseReplenFormula, @ReplenFormulaID, @StoreID, 
	    @TargetType, @OrderTargetID, @StoreCode, @PreDefine, @RunningStock, @RoundUpQty
  END
  CLOSE CUR_AutoReplenishStocke 
  DEALLOCATE CUR_AutoReplenishStocke
END

GO
