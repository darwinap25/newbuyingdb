USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenStockADJOrderBySTKVAR]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenStockADJOrderBySTKVAR]
  @StockTakeNumber  VARCHAR(64)
AS
/****************************************************************************
**  Name : GenStockADJOrderBySTKVAR
**  Version: 1.0.0.2
**  Description : 根据盘点差异表来产生库存调整单
**  Parameter :
select * from STK_STAKEVAR
exec GenStockADJOrderBySTKVAR 'SRKNUM000000032'
select* from Ord_StockAdjust_D
select * from STK_STAKE02
**  Created by: Gavin @2015-06-25
**  MOdified by：Gavin @2015-09-24 (ver 1.0.0.1) 数量为的记录不写入调整单.
**  Modified by：Gavin @2015-11-30 (ver 1.0.0.2) STK_STAKEBOOK表是当前库存,可能是空数据,所以storeid应该从STK_STAKE_H读取.
**
****************************************************************************/
BEGIN
  DECLARE @StockAdjustNumber VARCHAR(64), @BusDate DATETIME, @UserID INT, @StoreID INT, @StoreCode VARCHAR(64)       
  
  EXEC GetRefNoString 'STKADJ', @StockAdjustNumber output  
 
  BEGIN TRAN
    
    select @UserID = CreatedBy, @StoreID = StoreID from STK_STAKE_H WHERE StockTakeNumber = @StockTakeNumber
	select @StoreCode = StoreCode from BUY_STORE WHERE StoreID = @StoreID
    SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC
    SET @BusDate = ISNULL(@BusDate, GETDATE())

   -- SELECT  @StoreID = StoreID FROM STK_STAKEBOOK WHERE StockTakeNumber = @StockTakeNumber
    INSERT INTO Ord_StockAdjust_H (StockAdjustNumber, OrderType, ReferenceNo, StoreID, Remark, CreatedBusDate, ApproveStatus, 
      CreatedBy, UpdatedBy)
    VALUES(@StockAdjustNumber, 1, @StockTakeNumber, @StoreID, 'Stock Take VAR Adjust Order', @BusDate, 'P', @UserID, @UserID)  
    
    INSERT INTO Ord_StockAdjust_D (StockAdjustNumber,ProdCode,AdjustQty,StockTypeCode,ReasonID,Remark)
    select @StockAdjustNumber, ProdCode, qty, StockType, 0, 'STK ADJ' from
    (  
    SELECT ProdCode, SUM(ISNULL(VARQTY,0)) as qty, StockType FROM STK_STAKEVAR 
      WHERE StockTakeNumber = @StockTakeNumber AND StoreID = @StoreID
    GROUP BY StockType, ProdCode 
    ) A where A.qty <> 0 
  
    INSERT INTO Ord_StockAdjust_SerialNo (StockAdjustNumber,ProdCode,StockTypeCode,SerialNo)
    SELECT @StockAdjustNumber, ProdCode, StockType, SerialNo FROM STK_STAKEVAR 
      WHERE StockTakeNumber = @StockTakeNumber AND StoreID = @StoreID AND ISNULL(SerialNo, '') <> ''
  
  COMMIT TRAN
  
  RETURN 0   
END

GO
