USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[RecordUserAction]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[RecordUserAction] 
  @MemberID                 int,
  @ActionTypeID             int,
  @CardNumber               varchar(64),
  @RefNumber                varchar(64),
  @Description              varchar(512),
  @Remark                   varchar(512),
  @ApprovalCode             varchar(64),
  @SecurityCode             varchar(512),
  @UserID                   int    
AS
/****************************************************************************
**  Name : RecordUserAction  
**  Version: 1.0.0.0
**  Description : 记录用户操作的流水表。
**  Parameter :

**  Created by: Gavin @2013-02-20
**
****************************************************************************/
begin
  if exists(select * from sysobjects where name = 'UserAction_movement' and type = 'U')
  begin
    insert into UserAction_movement(MemberID, ActionTypeID, CardNumber, RefNumber, Description, 
      Remark, ApprovalCode, SecurityCode, CreatedBy)
    values(@MemberID, @ActionTypeID, @CardNumber, @RefNumber, @Description, 
      @Remark, @ApprovalCode, @SecurityCode, @UserID)
  end  
  return 0   
end

GO
