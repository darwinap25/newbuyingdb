USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCouponNatureList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\darwin.pasco
 * Created At: 2017/09/09 14:05:27
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetCouponNatureList] 
  @coupontypeid       int
  
AS
SET NOCOUNT ON;

BEGIN
    
    SELECT 
        [c].[CouponNatureID],
        [c].[CouponNatureCode],
        [c].[CouponNatureName1]
    FROM [dbo].[CouponNature] c
        INNER JOIN [dbo].[CouponNature_List] cnl
            ON [cnl].[CouponNatureID] = [c].[CouponNatureID]
    WHERE [cnl].[CouponTypeID] = @coupontypeid

    
END



GO
