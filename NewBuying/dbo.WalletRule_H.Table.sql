USE [NewBuying]
GO
/****** Object:  Table [dbo].[WalletRule_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WalletRule_H](
	[WalletRuleCode] [varchar](64) NOT NULL,
	[Description] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_WALLETRULE_H] PRIMARY KEY CLUSTERED 
(
	[WalletRuleCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[WalletRule_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[WalletRule_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_WalletRule_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_WalletRule_H] ON [dbo].[WalletRule_H]
FOR UPDATE
AS
/*==============================================================*/
/*                
* Name: Insert_WalletRule_H
* Version: 1.0.0.0
* Description : 
** Create By Gavin @2015-04-24
*/
/*==============================================================*/
BEGIN  
  declare @WalletRuleCode varchar(64)
  
  DECLARE CUR_WalletRule_H CURSOR fast_forward FOR
    SELECT WalletRuleCode FROM INSERTED
  OPEN CUR_WalletRule_H
  FETCH FROM CUR_WalletRule_H INTO @WalletRuleCode
  WHILE @@FETCH_STATUS=0
  BEGIN
    exec DoWalletRule @WalletRuleCode     
    FETCH FROM CUR_WalletRule_H INTO @WalletRuleCode
  END
  CLOSE CUR_WalletRule_H 
  DEALLOCATE CUR_WalletRule_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WalletRule_H', @level2type=N'COLUMN',@level2name=N'WalletRuleCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WalletRule_H', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RRG的要求。 绑定店铺和Telco卡。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WalletRule_H'
GO
