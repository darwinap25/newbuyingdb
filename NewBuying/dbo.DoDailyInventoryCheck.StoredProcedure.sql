USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoDailyInventoryCheck]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DoDailyInventoryCheck]
AS
/****************************************************************************
**  Name : DoDailyInventoryCheck 
**  Version: 1.0.0.2
**  Description : Coupon和的日常库存校对 (一般在EOD中调用)
**  Parameter : 日常 根据Coupon表的数据，对 Coupon_Onhand， Card_Onhand的数据进行校正。
** select * from StockOnhandSync_H
**  Created by: Gavin @2014-10-10
**  Modified by: Gavin @2014-10-10 (ver 1.0.0.1) 加上Card部分
**  Modified by: Gavin @2014-11-19 (ver 1.0.0.2) 修正Onhand中没有，但是card或者coupon中没有的情况。
**
****************************************************************************/
begin
  declare @HeadKeyID int
  if exists (select 1 from  sysobjects where id = object_id('StockOnhandSync_H') and type = 'U') 
  begin
    insert into StockOnhandSync_H (CheckDate, Note, ApproveStatus, CreatedOn, CreatedBy)
    values(convert(varchar(10), GETDATE(), 120), 'Auto Sync Onhand', 'P', GETDATE(), 1)
    set @HeadKeyID = SCOPE_IDENTITY()
    -- Coupon
      -- 总部
        -- 更新Onhand中有的记录
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 2, A.StoreTypeID,A.StoreID, A.CouponTypeID, null, null, A.CouponStatus, A.CouponStockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - A.OnhandQty from Coupon_Onhand A 
    left join (select CouponTypeID, LocateStoreID, StockStatus, COUNT(*) as Qty from Coupon 
                 where  Status < 2 
               group by CouponTypeID, LocateStoreID, StockStatus) B
            on A.CouponTypeID = B.CouponTypeID and A.CouponStockStatus = B.StockStatus and (A.StoreID = B.LocateStoreID or B.LocateStoreID is null)
    where A.StoreTypeID = 1  
       -- 写入Onhand中没有的记录
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 2, B.StoreTypeID,B.LocateStoreID, B.CouponTypeID, null, null, 0 as CouponStatus, B.StockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - isnull(A.OnhandQty,0) from Coupon_Onhand A 
    right join (select max(StoreTypeID) as StoreTypeID, CouponTypeID, LocateStoreID, StockStatus, COUNT(*) as Qty from Coupon C
                   left join Store S on C.LocateStoreID = S.StoreID 
                 where  C.Status < 2 
               group by CouponTypeID, LocateStoreID, StockStatus) B
            on A.CouponTypeID = B.CouponTypeID and A.CouponStockStatus = B.StockStatus and (A.StoreID = B.LocateStoreID or B.LocateStoreID is null)
    where B.StoreTypeID = 1 and A.CouponTypeID is null         
      -- 店铺
        -- 更新Onhand中有的记录      
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 2, A.StoreTypeID,A.StoreID, A.CouponTypeID, null, null, A.CouponStatus, A.CouponStockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - A.OnhandQty from Coupon_Onhand A 
    left join (select CouponTypeID, LocateStoreID, StockStatus, COUNT(*) as Qty from Coupon 
                 where  Status < 2 and isnull(LocateStoreID, 0) <> 0
               group by CouponTypeID, LocateStoreID, StockStatus) B
            on A.CouponTypeID = B.CouponTypeID and A.CouponStockStatus = B.StockStatus and A.StoreID = B.LocateStoreID
    where A.StoreTypeID = 2
       -- 写入Onhand中没有的记录    
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 2, B.StoreTypeID,B.LocateStoreID, B.CouponTypeID, null, null, 0 as CouponStatus, B.StockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - isnull(A.OnhandQty,0) from Coupon_Onhand A 
    right join (select max(StoreTypeID) as StoreTypeID, CouponTypeID, LocateStoreID, StockStatus, COUNT(*) as Qty from Coupon C
                   left join Store S on C.LocateStoreID = S.StoreID 
                 where  C.Status < 2 
               group by CouponTypeID, LocateStoreID, StockStatus) B
            on A.CouponTypeID = B.CouponTypeID and A.CouponStockStatus = B.StockStatus and A.StoreID = B.LocateStoreID
    where B.StoreTypeID = 2 and A.CouponTypeID is null   
                 
    -- Card
      -- 总部
        -- 更新Onhand中有的记录
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 1, A.StoreTypeID, A.StoreID, null, A.CardTypeID, A.CardGradeID, A.CardStatus, A.CardStockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - A.OnhandQty from Card_Onhand A 
    left join (select CardTypeID, CardGradeID, IssueStoreID, StockStatus, COUNT(*) as Qty from Card 
                 where  Status < 2 
               group by CardTypeID, CardGradeID, IssueStoreID, StockStatus) B
            on A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID and A.CardStockStatus = B.StockStatus and (A.StoreID = B.IssueStoreID or B.IssueStoreID is null)
    where A.StoreTypeID = 1  
       -- 写入Onhand中没有的记录
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 1, B.StoreTypeID, B.IssueStoreID, null, B.CardTypeID, B.CardGradeID, 0 as CardStatus, B.StockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - isnull(A.OnhandQty,0) from Card_Onhand A 
    right join (select max(StoreTypeID) as StoreTypeID, CardTypeID, CardGradeID, IssueStoreID, StockStatus, COUNT(*) as Qty from Card C
                   left join Store S on C.IssueStoreID = S.StoreID 
                 where C.Status < 2                  
               group by CardTypeID, CardGradeID, IssueStoreID, StockStatus) B
            on A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID and A.CardStockStatus = B.StockStatus and (A.StoreID = B.IssueStoreID or B.IssueStoreID is null)
    where B.StoreTypeID = 1 and A.CardTypeID is null         
      -- 店铺
        -- 更新Onhand中有的记录      
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 1, A.StoreTypeID, A.StoreID, null, A.CardTypeID, A.CardGradeID, A.CardStatus, A.CardStockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - A.OnhandQty from Card_Onhand A 
    left join (select CardTypeID, CardGradeID, IssueStoreID, StockStatus, COUNT(*) as Qty from Card
                 where  Status < 2 and isnull(IssueStoreID, 0) <> 0
               group by  CardTypeID, CardGradeID, IssueStoreID, StockStatus) B
            on A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID and A.CardStockStatus = B.StockStatus and A.StoreID = B.IssueStoreID
    where A.StoreTypeID = 2
       -- 写入Onhand中没有的记录    
    insert into StockOnhandSync_D (HeadKeyID,SyncType,StoreTypeID,StoreID,CouponTypeID,CardTypeID,CardGradeID,Status,StockStatus,ActualQty,OnhandQty,AdjQty)  
    select @HeadKeyID, 1, B.StoreTypeID, B.IssueStoreID, null, B.CardTypeID, B.CardGradeID, 0 as CardStatus, B.StockStatus, B.Qty, A.OnhandQty, isnull(B.Qty,0) - isnull(A.OnhandQty,0) from Card_Onhand A 
    right join (select max(StoreTypeID) as StoreTypeID, CardTypeID, CardGradeID, IssueStoreID, StockStatus, COUNT(*) as Qty from Card C
                   left join Store S on C.IssueStoreID = S.StoreID 
                 where C.Status < 2                  
               group by CardTypeID, CardGradeID, IssueStoreID, StockStatus) B
            on A.CardTypeID = B.CardTypeID and A.CardGradeID = B.CardGradeID and A.CardStockStatus = B.StockStatus and A.StoreID = B.IssueStoreID
    where B.StoreTypeID = 2 and A.CardTypeID is null       
    
    -- 执行更新
    update StockOnhandSync_H set ApproveStatus = 'A' where KeyID = @HeadKeyID
  end
end

GO
