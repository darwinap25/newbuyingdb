USE [NewBuying]
GO
/****** Object:  Table [dbo].[GrossMargin_MobileNo]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GrossMargin_MobileNo](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[GrossMarginCode] [varchar](64) NOT NULL,
	[MobileNoPattern] [varchar](64) NOT NULL,
	[PatternStart] [int] NOT NULL,
	[PatternLen] [int] NOT NULL,
 CONSTRAINT [PK_GROSSMARGIN_MOBILENO] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[GrossMargin_MobileNo] ADD  DEFAULT ((1)) FOR [PatternStart]
GO
ALTER TABLE [dbo].[GrossMargin_MobileNo] ADD  DEFAULT ((0)) FOR [PatternLen]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin_MobileNo', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GrossMargin表code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin_MobileNo', @level2type=N'COLUMN',@level2name=N'GrossMarginCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码标识段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin_MobileNo', @level2type=N'COLUMN',@level2name=N'MobileNoPattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标识段起始位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin_MobileNo', @level2type=N'COLUMN',@level2name=N'PatternStart'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标识段长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin_MobileNo', @level2type=N'COLUMN',@level2name=N'PatternLen'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GrossMargin 子表.  适用的手机号码规则。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrossMargin_MobileNo'
GO
