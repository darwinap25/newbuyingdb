USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetMemberPosition]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetMemberPosition]
  @UserID    int,
  @MemberID  int,  
  @Longitude varchar(64),
  @latitude	 varchar(64)
AS
/****************************************************************************
**  Name : SetMemberPosition  
**  Version: 1.0.0.0
**  Description : 获得City数据
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = SetMemberPosition 'anqing', 'anqingxian', 1, 30, @count output, @recordcount output, 'zh_CN'
  print @a  
  print @count
  print @recordcount  
**  Created by: Gavin @2013-02-01
**
****************************************************************************/
begin
  set @MemberID = isnull(@MemberID, 0)
  if not exists(select * from MemberPosition where MemberID = @MemberID)
  begin
    insert into MemberPosition(MemberID, Longitude, latitude, CreatedBy, UpdatedBy, CreatedOn, UpdatedOn)
    values(@MemberID, @Longitude, @latitude, @UserID, @UserID, getdate(), getdate())
  end else      
  begin
	update MemberPosition set Longitude = @Longitude, latitude = @latitude,UpdatedBy = @UserID, UpdatedOn = getdate()  where MemberID = @MemberID
  end
  
  return 0
end

GO
