USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckPrepareOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckPrepareOrder]
  @StoreID            INT           -- 店铺ID ,必填
AS
/****************************************************************************
**  Name : CheckPrepareOrder
**  Version: 1.0.0.0
**  Description : 注册盘点之前检查是否有为批核的订单. 有的话返回订单名称, 前台会报错,禁止注册.
**  Parameter :
exec CheckPrepareOrder 1
select * from buy_store
**  Created by: Gavin @2016-04-13
**
****************************************************************************/
BEGIN
  DECLARE @table table(TableName VARCHAR(64), OrderNumber VARCHAR(64), CreatedOn DATETIME, CreatedBy INT) 
  
  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_HQReceiveOrder_H', HQReceiveOrderNumber, CreatedOn, CreatedBy FROM Ord_HQReceiveOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID
  
  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_PickingOrder_H', PickingOrderNumber, CreatedOn, CreatedBy FROM Ord_PickingOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_ReceiveOrder_H', ReceiveOrderNumber, CreatedOn, CreatedBy FROM Ord_ReceiveOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_ReturnOrder_H', ReturnOrderNumber, CreatedOn, CreatedBy FROM Ord_ReturnOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_ShipmentOrder_H', ShipmentOrderNumber, CreatedOn, CreatedBy FROM Ord_ShipmentOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_StockAdjust_H', StockAdjustNumber, CreatedOn, CreatedBy FROM Ord_StockAdjust_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_StoreOrder_H', StoreOrderNumber, CreatedOn, CreatedBy FROM Ord_StoreOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_TransInOrder_H', TransInOrderNumber, CreatedOn, CreatedBy FROM Ord_TransInOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'Ord_TransOutOrder_H', TransOutOrderNumber, CreatedOn, CreatedBy FROM Ord_TransOutOrder_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  INSERT INTO @table(TableName, OrderNumber, CreatedOn, CreatedBy) 
  SELECT  'BUY_PO_H', POCode, CreatedOn, CreatedBy FROM BUY_PO_H WHERE ApproveStatus = 'P' AND StoreID = @StoreID

  SELECT  * FROM @table
  
  RETURN 0   
END

GO
