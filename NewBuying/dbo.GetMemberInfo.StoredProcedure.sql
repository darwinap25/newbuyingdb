USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberInfo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberInfo]
  @MemberID				int,			   --会员ID
  @LanguageAbbr			varchar(20)=''	  
AS
/****************************************************************************
**  Name : GetMemberInfo
**  Version: 1.0.0.8
**  Description :查找Member数据
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetMemberInfo 170, ''
  print @a  
**  Created by: Gavin @2012-05-23
**  Modify by: Gavin @2012-11-13 (ver 1.0.0.1) 增加返回ReadReguFlag
**  Modify by: Gavin @2013-04-03 (ver 1.0.0.2) 增加返回CreatedOn
**  Modify by: Gavin @2013-06-04 (ver 1.0.0.3) 增加返回ReceiveAllAdvertising，TransPersonalInfo
**  Modify by: Gavin @2013-07-09 (ver 1.0.0.4) 增加返回CompanyDesc(学校名称), OfficeAddress（学校位置）, ViewHistoryTranDays （查看历史记录的天数）
**  Modify by: Gavin @2013-07-09 (ver 1.0.0.5) 增加返回字段EmailValidation，SMSValidation，AcceptPhoneAdvertising， RecEmailAdv
**  Modify by: Gavin @2013-07-11 (ver 1.0.0.6) 增加返回字段AddressCounttry, AddressProvince, AddressCity, AddressDistrict, AddressDetail
**  Modify by: Gavin @2014-11-18 (ver 1.0.0.7) 增加返回字段MemberAppellation
**  Modify by: Gavin @2015-02-16 (ver 1.0.0.8) 增加返回字段AgeBracklet
**
****************************************************************************/
begin
  declare @EMailFlag int, @SMSFlag int, @EmailPromotionFlag int, @SMSPromotionFlag int, @Language int
  declare @AddressCountry varchar(512), @AddressProvince varchar(512), @AddressCity varchar(512), @AddressDistrict varchar(512), @AddressDetail varchar(512)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select Top 1 @EMailFlag = VerifyFlag, @EmailPromotionFlag = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 1
  select Top 1 @SMSFlag = VerifyFlag, @SMSPromotionFlag = PromotionFlag from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2
 
  select top 1 @AddressCountry = case @Language when 2 then C.CountryName2 when 3 then C.CountryName3 else C.CountryName1 end 
      ,@AddressProvince = case @Language when 2 then P.ProvinceName2 when 3 then P.ProvinceName3 else P.ProvinceName1 end 
      ,@AddressCity = case @Language when 2 then T.CityName2 when 3 then T.CityName3 else T.CityName1 end 
      ,@AddressDistrict = case @Language when 2 then D.DistrictName2 when 3 then D.DistrictName3 else D.DistrictName1 end 
      ,@AddressDetail= A.AddressDetail
    from Memberaddress A 
    left join Country C on A.AddressCountry = C.CountryCode
    left join Province P on A.AddressProvince = P.ProvinceCode
    left join City T on A.AddressCity = T.CityCode
    left join District D on A.AddressDistrict = D.DistrictCode
  where MemberID = @MemberID

  select MemberID, MemberRegisterMobile, MemberMobilePhone, CountryCode, MemberEmail, NickName, 
	MemberEngFamilyName,MemberEngGivenName, MemberChiFamilyName,MemberChiGivenName, MemberSex, MemberDateOfBirth,
	MemberMarital, MemberIdentityType, MemberIdentityRef, EducationID, ProfessionID, MemberPosition, NationID, HomeTelNum,
	HomeAddress, OtherContact, Hobbies, SpRemark, MemberPictureFile, ReadReguFlag, CreatedOn,
	ReceiveAllAdvertising, TransPersonalInfo, CompanyDesc, OfficeAddress, ViewHistoryTranDays, 
	isnull(@EMailFlag,0) as EmailValidation, isnull(@SMSFlag,0) as SMSValidation, AcceptPhoneAdvertising, 
	isnull(@EmailPromotionFlag,0) as RecEmailAdv, @SMSPromotionFlag as RecSMSAdv,
	@AddressCountry as AddressCountry, @AddressProvince as AddressProvince, @AddressCity as AddressCity, 
	@AddressDistrict as AddressDistrict, @AddressDetail as AddressDetail, L.LanguageAbbr as MemberDefaultLanguage,
	MemberAppellation, AgeBracklet
   from Member M left join LanguageMap L on M.MemberDefLanguage = L.KeyID
   where MemberID = @MemberID

  return 0  
end

GO
