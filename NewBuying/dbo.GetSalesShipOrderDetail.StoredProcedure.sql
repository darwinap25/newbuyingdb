USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSalesShipOrderDetail]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[GetSalesShipOrderDetail]
  @CardNumber				     VARCHAR(64),	      -- 顾客卡号
  @SalesShipOrderNumber          VARCHAR(64)          -- 单号码
AS
/****************************************************************************
**  Name : GetSalesShipOrderDetail 
**  Version: 1.0.0.0
**  Description : 查询送货单的detail
exec GetSalesShipOrderDetail '',''
**  Created by: Gavin @2016-05-24  
2395
****************************************************************************/
BEGIN
  SET @CardNumber = ISNULL(@CardNumber, '')
  SET @SalesShipOrderNumber = ISNULL(@SalesShipOrderNumber, '')
  SELECT H.SalesShipOrderNumber,H.OrderType,H.MemberID,H.CardNumber,H.ReferenceNo,H.Contact,H.ContactPhone,H.RequestDeliveryDate,H.DeliveryDate,H.DeliveryBy,H.Remark,H.Status,H.CreatedOn,H.CreatedBy,H.UpdatedOn,H.UpdatedBy,D.ProdCode,D.OrderQty
  FROM Ord_SalesShipOrder_D D LEFT JOIN Ord_SalesShipOrder_H H ON H.SalesShipOrderNumber = D.SalesShipOrderNumber
  WHERE (H.CardNumber = @CardNumber OR @CardNumber = '')
    AND (D.SalesShipOrderNumber = @SalesShipOrderNumber OR @SalesShipOrderNumber = '')
  RETURN 0
END

GO
