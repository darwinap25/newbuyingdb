USE [NewBuying]
GO
/****** Object:  Table [dbo].[Province]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Province](
	[ProvinceCode] [varchar](64) NOT NULL,
	[CountryCode] [varchar](64) NULL,
	[ProvinceName1] [nvarchar](512) NULL,
	[ProvinceName2] [nvarchar](512) NULL,
	[ProvinceName3] [nvarchar](512) NULL,
	[DeliveryCharge] [money] NULL,
 CONSTRAINT [PK_PROVINCE] PRIMARY KEY CLUSTERED 
(
	[ProvinceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'省编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Province', @level2type=N'COLUMN',@level2name=N'ProvinceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Province', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'省名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Province', @level2type=N'COLUMN',@level2name=N'ProvinceName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'省名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Province', @level2type=N'COLUMN',@level2name=N'ProvinceName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'省名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Province', @level2type=N'COLUMN',@level2name=N'ProvinceName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送收费' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Province', @level2type=N'COLUMN',@level2name=N'DeliveryCharge'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'省资料表。（中国一级行政区，用于填写地址）
@2015-10-12 增加DeliveryCharge。
@2015-12-30 使用新的表LogisticsPrice， DeliveryCharge 字段失效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Province'
GO
