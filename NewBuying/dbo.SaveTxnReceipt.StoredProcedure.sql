USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SaveTxnReceipt]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SaveTxnReceipt]   
  @UserID				varchar(512),		 -- 操作员ID
  @MemberID				int,                 -- 会员ID
  @CardUID				varchar(512),		 -- 会员卡的UID
  @CardNumber			varchar(512),        -- 会员卡号。
  @StoreCode            varchar(512),        -- 店铺编码   
  @RegisterCode         varchar(512),        -- 终端编码
  @ServerCode			varchar(512),        -- server编码
  @TxnNo				varchar(512),        -- 交易号码
  @Busdate				datetime,            -- 交易busdate
  @Txndate				datetime,			 -- 交易系统时间
  @SalesType			int,				 -- 交易类型
  @CashierID			varchar(512),		 -- 收银员编码
  @SalesManID			varchar(512),		 -- 销售员编码
  @TotalAmount			money,				 -- 交易总额
  @TxnStatus			int,				 -- 交易状态
  @Contact				varchar(512),		 -- 联系人
  @ContactPhone			varchar(512),        -- 联系人电话
  @Courier				varchar(512),		 -- 送货员
  @DeliveryaAddr		nvarchar(512),		 -- 送货地址
  @DeliveryDate			datetime,			 -- 送货时间
  @DeliveryCompleteDate datetime,			 -- 送货完成时间
  @SalesReceipt         nvarchar(max),		 -- 交易单小票
  @BrandCode			varchar(64)=''		 -- 店铺brandcode
AS
/****************************************************************************
**  Name : SaveTxnReceipt
**  Version: 1.0.0.3
**  Description : 保存前台的交易信息。（目前只传receipt）
**  Parameter :
**
**  Created by: Gavin @2012-05-24
**  Modify by Gavin @2012-08-13 (Ver 1.0.0.1) 交易上传时，根据交易总金额计算获得积分。（上传是会员交易，参数必须有cardnumber）
											  增加输入参数:brandcode
**  Modify by Gavin @2012-10-31 (Ver 1.0.0.2) Jacky FS当前使用此存储过程，需要更新。 
**  Modify by Gavin @2013-01-07 (Ver 1.0.0.3) 使用addressFullDetail字段,保存到sales_H
******************************************************************************/
begin
  
  declare  @OldTxnStatus int, @Longitude varchar(512), @latitude varchar(512), @MemberName varchar(512), @MemberMobilePhone varchar(512), @MemberAddress varchar(512),
			@EarnPoints int, @CardTotalAmount money, @StoreID int, @CalcResult int, @CardTotalPoints int,
			@ExpiryDate datetime, @CardStatus int
  --卡号解密--- 
  if isnull(@CardNumber, '') = '' and isnull(@CardUID, '') <> ''
    select @CardNumber = CardNumber from VenCardIDMap where VendorCardNumber = @CardUID
  if isnull(@CardNumber, '') <> ''
    select @MemberID = MemberID, @CardTotalAmount = TotalAmount, @CardTotalPoints = isnull(TotalPoints,0),
           @ExpiryDate = CardExpiryDate, @CardStatus = Status
     from Card where CardNumber = @CardNumber  

  select @MemberAddress = AddressFullDetail from memberaddress where MemberFirstAddr = 1 and MemberID = @MemberID  
  select @MemberName = RTrim(LTrim(MemberChiFamilyName)) + RTrim(LTrim(MemberChiGivenName)), 
     @MemberMobilePhone = MemberRegisterMobile from Member where MemberID = @MemberID
  if @@rowcount = 0
    return -1      
  -------------- 
  if isnull(@TxnStatus, 0) = 0
    set @TxnStatus = 5

  select @OldTxnStatus = Status from sales_H where TransNum = @TxnNo
  if @@rowcount > 0
  begin      
    begin
      select  @Longitude = Longitude, @latitude = latitude from CourierPosition where CourierCode = @Courier   
      update sales_H set DeliveryBy = substring(@Courier, 1, 10),  RequestDeliveryDate = @DeliveryDate, 
            DeliveryDate = @DeliveryCompleteDate, DeliveryFullAddress = substring(@DeliveryaAddr, 1, 200),
            [Status] = @TxnStatus, SalesReceipt = @SalesReceipt, DeliveryLongitude = @Longitude, Deliverylatitude = @latitude,
            UpdatedOn = getdate(), UpdatedBy = @UserID 
       where TransNum = @TxnNo
    end   
  end else
  begin      
    insert into sales_H
            (StoreCode, RegisterCode, TransNum, BusDate, TxnDate, TransType, CashierID, SalesManID, TotalAmount, 
			 Status, DeliveryBy, DeliveryFullAddress, RequestDeliveryDate, DeliveryDate, SalesReceipt, Contact, ContactPhone,
			 MemberID, MemberName, MemberMobilePhone, CardNumber, MemberAddress,
             CreatedOn, CreatedBy, UpdatedOn, UpdatedBy
             )
    values
            (@StoreCode, @RegisterCode, @TxnNo, @BusDate, @TxnDate, @SalesType, @CashierID, @SalesManID, @TotalAmount,
             @TxnStatus, @Courier, @DeliveryaAddr, @DeliveryDate, @DeliveryCompleteDate, @SalesReceipt, @Contact, @ContactPhone,
			 @MemberID, @MemberName, @MemberMobilePhone, @CardNumber, @MemberAddress,             
             getdate(), @UserID, getdate(), @UserID) 
  end

  /* to do 以后考虑计算 */    
  exec @CalcResult = CalcEarnPoints @CardNumber, @TotalAmount, @EarnPoints output   
  select top 1 @StoreID = StoreID from store S left join Brand B on S.BrandID = B.StoreBrandID 
	where StoreCode = @StoreCode and (StoreBrandCode = @BrandCode or isnull(@BrandCode, '') = '')

  insert into Card_Movement
     (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
      CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	  OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
  values
     (25, @CardNumber, null, 0, @TxnNo, @CardTotalAmount, 0, @CardTotalAmount, @EarnPoints, @BusDate, @TxnDate, 
     null, null, null, null, '', @UserID, @StoreID, @RegisterCode, @ServerCode,
     @CardTotalPoints, @CardTotalPoints + @EarnPoints,  @ExpiryDate, @ExpiryDate, @CardStatus, @CardStatus)  

  return 0  
end

GO
