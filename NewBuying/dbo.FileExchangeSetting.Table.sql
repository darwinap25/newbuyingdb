USE [NewBuying]
GO
/****** Object:  Table [dbo].[FileExchangeSetting]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileExchangeSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileExchangeIntervalMin] [int] NULL,
	[ExecutionTime] [datetime] NULL
) ON [PRIMARY]

GO
