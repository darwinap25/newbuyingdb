USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenCardReceiveOrder_Purchase]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenCardReceiveOrder_Purchase]
  @UserID                   int,             --操作员ID
  @OrderNumber              varchar(64),     --单号
  @Type                     int              --类型。 1：总部给供应商订单产生的。 2：店铺给总部的退货单 产生的  
AS
/****************************************************************************
**  Name : GenCardReceiveOrder_Purchase
**  Version: 1.0.0.3
**  Description : 产生充值收货单(总部向供应商) (for RRG) (写入Ord_Cardreceive_D时，FirstCardNumber是金额出的卡， EndCardNumber是金额入的卡)
    exec DoCardRechange
    SELECT PurchaseType, * FROM Ord_CardReceive_H
** 
**  Created by:  Gavin @2015-02-11
**  Modify by:  Gavin @2015-04-07 (ver 1.0.0.1) 卡的状态必须是2.  stockstatus 必须是 6
**  Modify by:  Gavin @2015-06-15 (ver 1.0.0.2) 写入@CardList时，同时检查Ord_OrderToSupplier_Card_D 中的CardGradeID。 但是如果一个单子中有 两个 cardgradeid， 一个有卡，一个没卡， 怎么处理呢？ 
**  Modify by:  Gavin @2015-12-21 (ver 1.0.0.3)		移除 StockStatus = 6 的条件.(因为在总部,这个卡可能还是 stockstatus =2 的)
****************************************************************************/
BEGIN
  declare @StoreID int, @CardReceiveNumber varchar(64), @FromStoreID int
  declare @CardList table(CardNumber varchar(64), CardTypeID int, CardGradeID int)
  declare @CardList2 table(CardNumber varchar(64), CardTypeID int, CardGradeID int)
  
  -- 产生收货单。
  if @Type = 1
  begin
    select @StoreID = StoreID from Ord_OrderToSupplier_Card_H where OrderSupplierNumber = @OrderNumber and PurchaseType = 2
    if @@ROWCOUNT = 0
      return -1 
    insert into @CardList(CardNumber, CardTypeID, CardGradeID)
    select CardNumber, C.CardTypeID, C.CardGradeID 
      from Card C left join (select * from Ord_OrderToSupplier_Card_D 
                              where OrderSupplierNumber = @OrderNumber) D on C.CardTypeID = D.CardTypeID and C.CardGradeID = D.CardGradeID
       where C.IssueStoreID = @StoreID and C.Status = 2  --and C.StockStatus = 6  -- ver 1.0.0.1
             and D.CardGradeID is not null
    if @@ROWCOUNT = 0
      return -1 
       
    -- 此时需要通过总部的storeid, 选择的cardtypeid， cardgradeid， 来确认cardnumber，填入Ord_CardReceive_D
    exec GetRefNoString 'CRECAP', @CardReceiveNumber output     
    insert into Ord_CardReceive_H(CardReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ReceiveType, ordertype, PurchaseType)
    select @CardReceiveNumber, OrderSupplierNumber, StoreID, SupplierID, SendAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, 1, ordertype, PurchaseType
     from Ord_OrderToSupplier_Card_H where OrderSupplierNumber = @OrderNumber and PurchaseType = 2

    insert into Ord_CardReceive_D (CardReceiveNumber, CardTypeID, CardGradeID, Description, OrderQty, ActualQty, 
          FirstCardNumber, EndCardNumber, BatchCardCode, CardStockStatus, ReceiveDateTime, OrderAmount, ActualAmount, OrderPoint, ActualPoint)  
    select @CardReceiveNumber, D.CardTypeID, D.CardGradeID, '',  OrderQty, OrderQty,  
         C.CardNumber, C.CardNumber, BatchCardCode, 1, GETDATE(), OrderAmount, isnull(OrderAmount,0), OrderPoint, isnull(OrderPoint,0)
        from Ord_OrderToSupplier_Card_D D
         left join Ord_OrderToSupplier_Card_H H on H.OrderSupplierNumber = D.OrderSupplierNumber
         left join (select Max(CardNumber) as CardNumber, CardTypeID, CardGradeID 
                      from @CardList Group By CardTypeID, CardGradeID) C 
                 on D.CardTypeID = C.CardTypeID and D.CardGradeID = C.CardGradeID
      where D.OrderSupplierNumber = @OrderNumber and H.PurchaseType = 2 and C.CardNumber is not null
  end    
  else if @Type = 2
  begin
    -- fromstore 是总部的，金额进入的店铺。  storeid是 发起退回的。
    select @FromStoreID = FromStoreID, @StoreID = StoreID from Ord_CardReturn_H where CardReturnNumber = @OrderNumber and PurchaseType = 2
    insert into @CardList(CardNumber, CardTypeID, CardGradeID)    --  store card
    select CardNumber, CardTypeID, CardGradeID from Card where IssueStoreID = @StoreID  
      and Status = 2 --and StockStatus = 6  -- ver 1.0.0.1
    insert into @CardList2(CardNumber, CardTypeID, CardGradeID)   --  HQ card
    select CardNumber, CardTypeID, CardGradeID from Card where IssueStoreID = @FromStoreID    
      and Status = 2 --and StockStatus = 6  -- ver 1.0.0.1 
    if @@ROWCOUNT = 0
      return -1 
        
    exec GetRefNoString 'CRECAP', @CardReceiveNumber output     
    insert into Ord_CardReceive_H(CardReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ReceiveType, ordertype, PurchaseType)
    select @CardReceiveNumber, CardReturnNumber, StoreID, FromStoreID, SendAddress, FromAddress, 
          FromContactName, FromContactNumber, FromEmail, FromMobile, StoreContactName, StoreContactPhone,
          StoreContactEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, 2, 0, PurchaseType
     from Ord_CardReturn_H where CardReturnNumber = @OrderNumber and PurchaseType = 2

    insert into Ord_CardReceive_D (CardReceiveNumber, CardTypeID, CardGradeID, Description, OrderQty, ActualQty, 
          FirstCardNumber, EndCardNumber, BatchCardCode, CardStockStatus, ReceiveDateTime, OrderAmount, ActualAmount, OrderPoint, ActualPoint)  
    select @CardReceiveNumber, D.CardTypeID, D.CardGradeID, '',  OrderQty, OrderQty,  
         C.CardNumber, CC.CardNumber, BatchCardCode, 1, GETDATE(), OrderAmount, isnull(OrderAmount,0), OrderPoint, isnull(OrderPoint,0)
        from Ord_CardReturn_D D
         left join Ord_CardReturn_H H on H.CardReturnNumber = D.CardReturnNumber
         left join (select Max(CardNumber) as CardNumber, CardTypeID, CardGradeID 
                      from @CardList Group By CardTypeID, CardGradeID) C 
                 on D.CardTypeID = C.CardTypeID and D.CardGradeID = C.CardGradeID
         left join (select Max(CardNumber) as CardNumber, CardTypeID, CardGradeID 
                      from @CardList2 Group By CardTypeID, CardGradeID) CC 
                 on D.CardTypeID = C.CardTypeID and D.CardGradeID = C.CardGradeID                 
      where D.CardReturnNumber = @OrderNumber and H.PurchaseType = 2 and C.CardNumber is not null and CC.CardNumber is not null 
  end
END

GO
