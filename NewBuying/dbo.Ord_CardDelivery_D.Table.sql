USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardDelivery_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardDelivery_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardDeliveryNumber] [varchar](64) NOT NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[Description] [nvarchar](512) NULL,
	[OrderQTY] [int] NULL,
	[PickQTY] [int] NULL,
	[ActualQTY] [int] NULL,
	[FirstCardNumber] [varchar](64) NULL,
	[EndCardNumber] [varchar](64) NULL,
	[BatchCardCode] [varchar](64) NULL,
 CONSTRAINT [PK_ORD_CARDDELIVERY_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'CardDeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'OrderQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要求拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'PickQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'ActualQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的首卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'FirstCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的末卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'EndCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstCouponNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D', @level2type=N'COLUMN',@level2name=N'BatchCardCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡送货单子表
根据 Ord_CardPicking_D产生，过滤其中的实际拣货数量为0的记录。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_D'
GO
