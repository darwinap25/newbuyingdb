USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductBrands]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetProductBrands]   
  @ProductBrandID       int, 
  @ConditionStr         nvarchar(1000)='',
  @OrderCondition       nvarchar(1000)='',
  @PageCurrent          int=1,             -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize             int=0,             -- 每页记录数， 为0时，不分页，默认0
  @PageCount            int=0 output,      -- 返回总页数。
  @RecordCount          int=0 output,      -- 返回总记录数。
  @LanguageAbbr         varchar(20)=''   
AS
/****************************************************************************
**  Name : GetProductBrands    
**  Version: 1.0.0.0
**  Description : 返回货品品牌表数据
**
  declare @a int, @PageCount int, @RecordCount int
  exec @a = GetProductBrands 0, '', '', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  select * from product_brand
  
**  Created by: Gavin @2013-10-24
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @LanguageStr varchar(10)

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @LanguageStr = cast(@Language as int) 
  set @ProductBrandID = isnull(@ProductBrandID, 0)
    
  set @SQLStr = ' Select ProductBrandID, ProductBrandCode, case ' + @LanguageStr + ' when 2 then ProductBrandName2 when 3 then ProductBrandName3 else ProductBrandName1 end as ProductBrandName, '
    + ' case ' + @LanguageStr + ' when 2 then ProductBrandDesc2 when 3 then ProductBrandDesc3 else ProductBrandDesc1 end as ProductBrandDesc, '
    + ' ProductBrandName1, ProductBrandName2, ProductBrandName3, ProductBrandDesc1, ProductBrandDesc2, ProductBrandDesc3, '
    + ' ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, CardIssuerID, IndustryID '
    + ' from product_brand '
    + ' where ProductBrandID = ' + cast(@ProductBrandID as varchar) + ' or ' + cast(@ProductBrandID as varchar) + ' = 0'
  exec SelectDataInBatchs @SQLStr, 'ProductBrandCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr     
  return 0
end

GO
