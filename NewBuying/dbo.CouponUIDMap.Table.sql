USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponUIDMap]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponUIDMap](
	[CouponUID] [varchar](512) NOT NULL,
	[ImportCouponNumber] [varchar](64) NULL,
	[BatchCouponID] [int] NULL,
	[CouponTypeID] [int] NULL,
	[CouponNumber] [varchar](64) NULL,
	[Status] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_COUPONUIDMAP] PRIMARY KEY CLUSTERED 
(
	[CouponUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵导入号码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponUIDMap', @level2type=N'COLUMN',@level2name=N'CouponUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponUIDMap', @level2type=N'COLUMN',@level2name=N'ImportCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'coupon 的 batchID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponUIDMap', @level2type=N'COLUMN',@level2name=N'BatchCouponID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定的CouponTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponUIDMap', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绑定的CouponNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponUIDMap', @level2type=N'COLUMN',@level2name=N'CouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。0：无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponUIDMap', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号码与UID对照表。
（UID：客户提供的优惠劵号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponUIDMap'
GO
