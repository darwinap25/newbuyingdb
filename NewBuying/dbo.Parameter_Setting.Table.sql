USE [NewBuying]
GO
/****** Object:  Table [dbo].[Parameter_Setting]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parameter_Setting](
	[EstimatedFareFunction] [int] NULL,
	[MainListLPNDownload] [int] NULL,
	[LPNIdleCriteria] [int] NULL,
	[IdleLPNTime] [int] NULL,
	[GreenActiveTime] [int] NULL,
	[IdleGreenTime] [int] NULL,
	[GreenRetryTimeOut] [int] NULL,
	[GreenRetryCount] [int] NULL,
	[RequestLPNListsEveryNPrint] [int] NULL,
	[UpdateLPNListsInterval] [int] NULL,
	[VoidedGreenDisplayTime] [int] NULL,
	[IdleDeviceRequestLPNTime] [int] NULL,
	[LPNListRequestTimeOut] [int] NULL,
	[OffLineCheckTime] [int] NULL,
	[OffLineNextUploadTime] [int] NULL,
	[OffLineDataExpireTime] [int] NULL,
	[AlertMessageRequestLPNTime] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((0)) FOR [EstimatedFareFunction]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((3)) FOR [LPNIdleCriteria]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((30)) FOR [IdleLPNTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((40)) FOR [GreenActiveTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((30)) FOR [IdleGreenTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((30)) FOR [GreenRetryTimeOut]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((3)) FOR [GreenRetryCount]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((1)) FOR [VoidedGreenDisplayTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((3)) FOR [OffLineCheckTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((10)) FOR [OffLineNextUploadTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((86400)) FOR [OffLineDataExpireTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT ((5)) FOR [AlertMessageRequestLPNTime]
GO
ALTER TABLE [dbo].[Parameter_Setting] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estimated Fare 的功能，0就是OFF, 1就是ON。 OFF的時候，User不用選擇目的地，直接就能打印' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'EstimatedFareFunction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of LPN downloaded to devices’ Main List and changes status to “Active”
Allowable range is 20-30
Default: 20
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'MainListLPNDownload'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of LPN passed before LPN is idle。 Allowable range is 1-100 。Default: 3
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'LPNIdleCriteria'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duration in minutes of idle LPN before expiry   Allowable range is 1-90  Default: 30
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'IdleLPNTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duration in minutes of green LPN before idle.  Allowable range is 1-90.  Default: 40
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'GreenActiveTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duration in minutes of idle green LPN before expiry. Allowable range is 1-90. Default: 30
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'IdleGreenTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Retry duration before time-out for green card printing
Allowable range is 1-10
Default: 3
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'GreenRetryTimeOut'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GreenRetryCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'GreenRetryCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Request updated LPN lists every N print, where N is the number specified by this field.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'RequestLPNListsEveryNPrint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How often are Main LPN list, Idle and Green list updated on server
Allowable range is 30-3600
Default: 60
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'UpdateLPNListsInterval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allowable range is 12-48
Default: 24
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'VoidedGreenDisplayTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allowable range is 10-1800
Default: 60
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'IdleDeviceRequestLPNTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allowable range is 1-10
Default: 3
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'LPNListRequestTimeOut'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'离线时，每次检查需要上传数据的间隔  单位秒  默认3秒' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'OffLineCheckTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'离线时，如果一条数据上传失败，下次再次上传的间隔  单位秒  默认10秒' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'OffLineNextUploadTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'离线时，如果一条数据存在了多少时间，还未上传成功，则手机本地自动删除该离线记录  单位秒 默认 86400=24小时' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'OffLineDataExpireTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'检查提示用户的消息间隔，每间隔多少秒，进行一次信息读取' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting', @level2type=N'COLUMN',@level2name=N'AlertMessageRequestLPNTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数设定，仅一条记录。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter_Setting'
GO
