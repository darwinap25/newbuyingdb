USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSCompleteSalesOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[POSCompleteSalesOrder]
  @UserID				         int,		        -- 操作员ID
  @TxnNo                         VARCHAR(64),       -- 交易号码
  @ApprovalCode                  VARCHAR(64) output --返回的approvalCode
AS
/****************************************************************************
**  Name : POSCompleteSalesOrder 
**  Version: 1.0.0.2
**  Description : 完成交易单。

**  Created by: Gavin @2016-05-05   
**  Modified by: Gavin @2016-07-21 （ver 1.0.0.1） 如果   @TxnStatus 不正确，不update sales_H, 则return错误值：
**  Modified by: Gavin @2016-12-09 （ver 1.0.0.2） 更新交易单时，增加考虑
****************************************************************************/
BEGIN
  DECLARE  @StockTypeCode VARCHAR(64), @StoreID INT, @StoreCode VARCHAR(64), @A INT, @TxnStatus INT
  DECLARE @Busdate DATETIME, @Txndate DATETIME

  SELECT @BusDate = BusDate, @TxnDate = TxnDate, @StoreCode = StoreCode, @TxnStatus = Status  FROM Sales_H WHERE TransNum = @TxnNo
  SET @stocktypecode = 'G'
  SELECT @StoreID = StoreID FROM BUY_STORE WHERE StoreCode = @StoreCode
  SET @StoreID = ISNULL(@StoreID, 0)

  -- 非购物车, 首次完成支付或者交易完成.
  IF @TxnStatus = 13  --IN (4, 10, 11, 12, 13)
  BEGIN
    UPDATE SALES_H SET Status = 5, UpdatedOn = GETDATE(), UpdatedBy = @UserID WHERE TransNum = @TxnNo
  END ELSE
    RETURN -521

  RETURN 0
END

GO
