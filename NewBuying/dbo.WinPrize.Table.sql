USE [NewBuying]
GO
/****** Object:  Table [dbo].[WinPrize]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WinPrize](
	[WinPrizeID] [int] IDENTITY(1,1) NOT NULL,
	[GameCampaignPrizeID] [int] NOT NULL,
	[EarnCouponNumber] [varchar](64) NOT NULL,
	[ConsumeCouponNumber] [varchar](64) NULL,
	[Status] [int] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_WINPRIZE] PRIMARY KEY CLUSTERED 
(
	[WinPrizeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[WinPrize] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[WinPrize] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[WinPrize] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WinPrize', @level2type=N'COLUMN',@level2name=N'WinPrizeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏活动奖品ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WinPrize', @level2type=N'COLUMN',@level2name=N'GameCampaignPrizeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'获得的couponnumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WinPrize', @level2type=N'COLUMN',@level2name=N'EarnCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费的coupon number（用金额或积分的话，这里为空）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WinPrize', @level2type=N'COLUMN',@level2name=N'ConsumeCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态： 0：无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WinPrize', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赢取的奖励记录表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WinPrize'
GO
