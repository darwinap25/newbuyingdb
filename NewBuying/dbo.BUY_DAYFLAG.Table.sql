USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_DAYFLAG]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_DAYFLAG](
	[DayFlagID] [int] IDENTITY(1,1) NOT NULL,
	[DayFlagCode] [varchar](64) NULL,
	[Note] [nvarchar](512) NULL,
	[Day1Flag] [int] NULL,
	[Day2Flag] [int] NULL,
	[Day3Flag] [int] NULL,
	[Day4Flag] [int] NULL,
	[Day5Flag] [int] NULL,
	[Day6Flag] [int] NULL,
	[Day7Flag] [int] NULL,
	[Day8Flag] [int] NULL,
	[Day9Flag] [int] NULL,
	[Day10Flag] [int] NULL,
	[Day11Flag] [int] NULL,
	[Day12Flag] [int] NULL,
	[Day13Flag] [int] NULL,
	[Day14Flag] [int] NULL,
	[Day15Flag] [int] NULL,
	[Day16Flag] [int] NULL,
	[Day17Flag] [int] NULL,
	[Day18Flag] [int] NULL,
	[Day19Flag] [int] NULL,
	[Day20Flag] [int] NULL,
	[Day21Flag] [int] NULL,
	[Day22Flag] [int] NULL,
	[Day23Flag] [int] NULL,
	[Day24Flag] [int] NULL,
	[Day25Flag] [int] NULL,
	[Day26Flag] [int] NULL,
	[Day27Flag] [int] NULL,
	[Day28Flag] [int] NULL,
	[Day29Flag] [int] NULL,
	[Day30Flag] [int] NULL,
	[Day31Flag] [int] NULL,
 CONSTRAINT [PK_BUY_DAYFLAG] PRIMARY KEY CLUSTERED 
(
	[DayFlagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day1Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day2Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day3Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day4Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day5Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day6Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day7Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day8Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day9Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day10Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day11Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day12Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day13Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day14Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day15Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day16Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day17Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day18Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day19Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day20Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day21Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day22Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day23Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day24Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day25Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day26Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day27Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day28Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day29Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day30Flag]
GO
ALTER TABLE [dbo].[BUY_DAYFLAG] ADD  DEFAULT ((0)) FOR [Day31Flag]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'DayFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'DayFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day1Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day2Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day3Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day4Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day5Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day6Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day7Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day8Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day9Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day10Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day11Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day12Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day13Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day14Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day15Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day16Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day17Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day18Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day19Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day20Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day21Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day22Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day23Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day24Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day25Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day26Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day27Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day28Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day29Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day30Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day31Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'天标志表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DAYFLAG'
GO
