USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewCouponMovement]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewCouponMovement]
AS
select OprID, C.CardNumber, C.CouponNumber, C.CouponTypeID, RefTxnNo, Amount as ActAmount, 
  C.Status, C.CouponExpiryDate, C.CouponIssueDate, C.CouponActiveDate, C.StoreID, C.BatchCouponID, C.CouponAmount, C.RedeemStoreID
 from coupon_movement M left join Coupon C on C.CouponNumber = M.CouponNumber

GO
