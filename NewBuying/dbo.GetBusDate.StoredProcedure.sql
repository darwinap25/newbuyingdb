USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBusDate]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBusDate]
  @StoreCode             varchar(64),      -- 店铺编号
  @BusDate               DATE OUTPUT       -- 交易的Business date
AS
/****************************************************************************
**  Name : GetBusDate
**  Version : 1.0.0.1
**  Description : 获得当前的Busdate
**
  declare  @a int, @BusDate date, @storeCode VARCHAR(64)
  exec @a = GetBusDate, 'DEMO'
  print @a
  print @BusDate

**  Created by Gavin @2015-03-23
**  Modify by Gavin @2016-01-07 (1.0.0.1) 增加参数@StoreCode
****************************************************************************/
BEGIN
  SELECT TOP 1 @BusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 and StoreCode = @StoreCode
   ORDER BY BusDate desc
  IF @BusDate IS NULL
    RETURN -510
  ELSE  
    RETURN 0
END

GO
