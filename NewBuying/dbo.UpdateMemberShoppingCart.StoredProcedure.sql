USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberShoppingCart]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[UpdateMemberShoppingCart]
  @MemberID             bigint,               -- 会员ID
  @Action               char(1),           -- 操作： 'A': 增加。'U': 更新（需要KeyID）。'D':删除（需要KeyID）。 'C': 清空购物车
  @KeyID                int,               -- 主键 
  @ProdCodeStyle	    varchar(64),       -- 货品style编码
  @ProdCode             varchar(64),       -- 货品编码
  @ProdDesc             varchar(512),      -- 货品描述
  @ProdPIC              varchar(512),      -- 货品图片
  @Qty                  int,               -- 货品数量
  @RetailPrice          money,             -- 货品零售价
  @RetailAmount         money,             -- 货品零售金额 （@RetailPrice * @Qty）
  @Memo                 varchar(Max),       -- 附加信息
  @ProdTitle			varchar(512),	   -- 标题
  @RetentionDays        int=7			   -- 保留天数，默认7天
AS
/****************************************************************************
**  Name : UpdateMemberShoppingCart  
**  Version: 1.0.0.6
**  Description : 更新会员的购物车内信息 (根据KeyID定位，由调用方判断同一个member，product唯一)
**  Parameter :
  declare @a int  
  exec @a = UpdateMemberShoppingCart 1, 'D', 1, '243243', '随碟附送该死的', 'sadfasf', 3, 103, 309, '隔阂和edewew额无法和违法'
  print @a  
  
**  Created by: Gavin @2013-12-20
**  Modified by: Gavin @2013-12-26  (ver 1.0.0.1) @Action只使用A，D，C。 KeyID不再传入数据，根据传入的@ProdCode 来判断update或者insert。 存储过程来判断一个memberID，product必须唯一
**  Modified by: Robin @2013-12-27  (ver 1.0.0.2) @Action加入U，原来的A执行后，为累加购物车中同货号商品数量
**  Modified by: Gavin @2014-01-06  (ver 1.0.0.3) @Action=A 时，如果是增加数量，则retailamount需要重新计算。
**  Modified by: Gavin @2014-01-07  (ver 1.0.0.4) 更新UpdatedOn， 删除过期记录
**  Modified by: Gavin @2016-06-27  (ver 1.0.0.5) UpdatedOn 增加时间.
**  Modified by: Gavin @2016-10-17 (ver 1.0.0.6) @MemberID 类型由int改为 bigint
**
****************************************************************************/
begin
  if isnull(@MemberID, 0) = 0
    return -7

  set @ProdCode = isnull(@ProdCode, '')
  set @ProdCodeStyle = isnull(@ProdCodeStyle, '')
  set @ProdDesc = isnull(@ProdDesc, '')
  set @ProdPIC = isnull(@ProdPIC, '')
  set @Qty = isnull(@Qty, 0)
  set @RetailPrice = isnull(@RetailPrice, 0)
  set @RetailAmount = isnull(@RetailAmount, 0)
  set @Memo = isnull(@Memo, '')
  if @Memo = '' 
    set @Memo = ' '	  -- 否则出现[Microsoft][SQLServer 2000 Driver for JDBC]Underlying input stream returned zero bytes 的异常
    
  if @Action = 'A'
  begin											
    if exists(select keyid from MemberShoppingCart where MemberID = @MemberID and ProdCode = @ProdCode)
    begin
      update MemberShoppingCart set MemberID = @MemberID, ProdCodeStyle = @ProdCodeStyle, ProdCode = @ProdCode, ProdDesc = @ProdDesc, 
        ProdPIC = @ProdPIC, Qty = Qty+@Qty, RetailPrice = @RetailPrice, RetailAmount = @RetailPrice * (Qty+@Qty), Memo = @Memo, 
        ProdTitle = @ProdTitle, UpdatedOn = Getdate() --UpdatedOn = convert(varchar(10), Getdate(), 120)
      where MemberID = @MemberID and ProdCode = @ProdCode
    end else
    begin  
      insert into MemberShoppingCart 
        (MemberID, ProdCodeStyle, ProdCode, ProdDesc, ProdPIC, Qty, RetailPrice, RetailAmount, Memo, ProdTitle, UpdatedOn)
       values (@MemberID, @ProdCodeStyle, @ProdCode, @ProdDesc, @ProdPIC, @Qty, @RetailPrice, @RetailAmount, @Memo, @ProdTitle, Getdate())  -- convert(varchar(10), Getdate(), 120))
    end
  end else if @Action = 'U'
  begin
    if exists(select keyid from MemberShoppingCart where MemberID = @MemberID and ProdCode = @ProdCode)
    begin
      update MemberShoppingCart set MemberID = @MemberID, ProdCodeStyle = @ProdCodeStyle, ProdCode = @ProdCode, ProdDesc = @ProdDesc, 
        ProdPIC = @ProdPIC, Qty = @Qty, RetailPrice = @RetailPrice, RetailAmount = @RetailAmount, Memo = @Memo, 
        ProdTitle = @ProdTitle, UpdatedOn = Getdate() --UpdatedOn = convert(varchar(10), Getdate(), 120)
      where MemberID = @MemberID and ProdCode = @ProdCode
    end else
    begin  
      insert into MemberShoppingCart 
        (MemberID, ProdCodeStyle, ProdCode, ProdDesc, ProdPIC, Qty, RetailPrice, RetailAmount, Memo, ProdTitle, UpdatedOn)
       values (@MemberID, @ProdCodeStyle, @ProdCode, @ProdDesc, @ProdPIC, @Qty, @RetailPrice, @RetailAmount, @Memo, @ProdTitle, Getdate()) --convert(varchar(10), Getdate(), 120))
    end          
  end else if @Action = 'D'
  begin
	  Delete from MemberShoppingCart where MemberID = @MemberID and ProdCode = @ProdCode
  end else if @Action = 'C'
  begin
	Delete from MemberShoppingCart where MemberID = @MemberID
  end else 
    return -97    	  
  
  -- 删除超过@RetentionDays 的记录
  Delete from MemberShoppingCart where MemberID = @MemberID and (UpdatedOn + @RetentionDays + 1) < getdate()  
  return 0
end

GO
