USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberPosition]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberPosition]
  @MemberID       int,	   -- 会员ID
  @PointLongitude varchar(64)='',  --可以不指定会员按指定的坐标点作为原点查询。
  @PointLatitude  varchar(64)='',
  @RangeMeter     int=0	   -- 范围内的所有会员，单位米，默认0  
AS
/****************************************************************************
**  Name : GetMemberPosition  
**  Version: 1.0.0.3
**  Description : 获得City数据
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = GetMemberPosition 13,'','',100
  print @a  
  print @count
  print @recordcount 
  
  declare @a decimal(30,20), @b decimal(30,20), @c decimal(30,20)
  set @b =0.0004390566
  set @c = 0.01448379
  select @b * @c
  
**  Created by: Gavin @2013-01-22
**  Modify By Gavin @2013-02-19 (ver 1.0.0.1) 修正纬度计算。
**  Modify By Gavin @2013-02-22 (ver 1.0.0.2) 增加返回字段：M.MemberPictureFile, M.MemberRegisterMobile, M.MemberSex, M.MemberEmail, M.MemberDateOfBirth
**  Modify By Gavin @2013-03-05 (ver 1.0.0.3) 增加返回字段：M.MemberEngFamilyName, M.MemberEngGivenName,  M.MemberChgFamilyName, M.MemberChgGivenName
**
****************************************************************************/
begin
  declare @PerUnitLength decimal(30,20), @TempLongitude	decimal(30,20), @TempLatitude decimal(30,20), @TempCalc decimal(30,20)
  declare @LeftLongitude decimal(30,20), @LeftLatitude decimal(30,20), @RightLongitude decimal(30,20), @RightLatitude decimal(30,20)
  set @PerUnitLength = 111111.111111      -- hardcode. 每度纬度的单位长度（单位米），也是在赤道上每度经度度的单位长度（单位米）
  
  set @RangeMeter = isnull(@RangeMeter, 0)
  set @MemberID = isnull(@MemberID, 0)
  set @PointLongitude = isnull(@PointLongitude, '')
  set @PointLatitude = isnull(@PointLatitude, '')
  
  if @RangeMeter = 0 
  begin
    if @MemberID <> 0
	  select P.MemberID, P.Longitude, P.Latitude,  M.MemberPictureFile, 
	    M.MemberRegisterMobile, M.MemberSex, M.MemberEmail, M.MemberDateOfBirth,
	    M.MemberEngFamilyName, M.MemberEngGivenName,  M.MemberChiFamilyName, M.MemberChiGivenName
	  from MemberPosition P left join Member M on P.MemberID = M.MemberID where P.MemberID = @MemberID    
	else if (@PointLongitude <> '' and @PointLatitude <> '')
	  select P.MemberID, P.Longitude, P.Latitude,  M.MemberPictureFile, 
	    M.MemberRegisterMobile, M.MemberSex, M.MemberEmail, M.MemberDateOfBirth
	  from MemberPosition P left join Member M on P.MemberID = M.MemberID where Longitude = @PointLongitude and latitude = @PointLatitude
	else
	  return -1  
  end  else
  begin
	if @MemberID <> 0
	begin
	  select @TempLongitude = cast(Longitude as decimal(30,20)), @TempLatitude = cast(latitude as decimal(30,20)) from MemberPosition where MemberID = @MemberID    
	end else if (@PointLongitude <> '' and @PointLatitude <> '')
	begin
	  set @TempLongitude = cast(@PointLongitude as decimal(30,20))	  
	  set @TempLatitude = cast(@PointLatitude as decimal(30,20))	  
	end else
	  return -1

    --计算纬度
	set @TempCalc =	@RangeMeter  / @PerUnitLength	
	set @LeftLatitude = @TempLatitude - @TempCalc 
	set @RightLatitude = @TempLatitude + @TempCalc
	
    --计算经度
    if @TempLatitude = 90
	  set @TempCalc = 0
	else  
	  set @TempCalc = @RangeMeter / (@PerUnitLength *  COS((@TempLatitude * PI())/180))
	set @LeftLongitude = @TempLongitude	- @TempCalc
	set @RightLongitude = @TempLongitude + @TempCalc

	select P.MemberID, P.Longitude, P.Latitude,  M.MemberPictureFile, 
	    M.MemberRegisterMobile, M.MemberSex, M.MemberEmail, M.MemberDateOfBirth, 
	    M.MemberEngFamilyName, M.MemberEngGivenName,  M.MemberChiFamilyName, M.MemberChiGivenName
	  from MemberPosition P left join Member M on P.MemberID = M.MemberID
	   where (Longitude >= @LeftLongitude and Longitude <= @RightLongitude)
	     and (latitude >= @LeftLatitude and latitude <= @RightLatitude)
  end
  return 0
end

GO
