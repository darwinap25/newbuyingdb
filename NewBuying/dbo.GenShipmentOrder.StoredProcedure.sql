USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenShipmentOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenShipmentOrder] 
  @PickingOrderNumber VARCHAR(64)
AS
/****************************************************************************
**  Name : GenShipmentOrder  
**  Version: 1.0.0.2
**  Description : 店铺订单批核后，产生送货单
**  Parameter :
select *from ord_shipmentorder_d
update ord_shipmentorder_d set actualqty = 230 where keyid = 19
**  Created by: Gavin @2015-03-24
**  Modify by: Gavin @2015-04-13 (ver 1.0.0.1) 流程更改。shipment单的数据来源改为pickingorder
**  Modify by Gavin @2016-01-07 (1.0.0.2) 获取busdate时增加StoreCode条件
**
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @StoreCode varchar(64)
  EXEC GetRefNoString 'SHP', @NewNumber OUTPUT

  select @StoreCode = StoreCode FROM Ord_PickingOrder_H A left join Buy_STORE B on A.StoreID = B.StoreID
    WHERE PickingOrderNumber = @PickingOrderNumber
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC
  
  INSERT INTO Ord_ShipmentOrder_H (ShipmentOrderNumber,OrderType,ReferenceNo,FromStoreID,FromContactName,
      FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
      StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
      ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
  SELECT @NewNumber, 1, PickingOrderNumber, FromStoreID, FromContactName,
      FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
      StoreMobile,StoreAddress,Remark,@BusDate,NULL,'','P',
      NULL,ApproveBy,GETDATE(),CreatedBy,GETDATE(),UpdatedBy
    FROM Ord_PickingOrder_H 
    WHERE PickingOrderNumber = @PickingOrderNumber
    
  INSERT INTO Ord_ShipmentOrder_D (ShipmentOrderNumber,ProdCode,OrderQty,ActualQty,Remark)
  SELECT @NewNumber, ProdCode, OrderQty, ActualQty, 'Auto Gen Shipment Order'  
    FROM Ord_PickingOrder_D D left join Ord_PickingOrder_H H on H.PickingOrderNumber = D.PickingOrderNumber
    WHERE H.PickingOrderNumber = @PickingOrderNumber
  RETURN 0   
END

GO
