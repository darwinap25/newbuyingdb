USE [NewBuying]
GO
/****** Object:  Table [dbo].[PromotionMsg]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PromotionMsg](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PromotionMsgCode] [varchar](64) NOT NULL,
	[PromotionMsgTypeID] [int] NOT NULL,
	[PromotionTitle1] [nvarchar](512) NULL,
	[PromotionTitle2] [nvarchar](512) NULL,
	[PromotionTitle3] [nvarchar](512) NULL,
	[PromotionMsgStr1] [nvarchar](max) NULL,
	[PromotionMsgStr2] [nvarchar](max) NULL,
	[PromotionMsgStr3] [nvarchar](max) NULL,
	[PromotionPicFile] [varchar](512) NULL,
	[BirthPromotion] [int] NULL,
	[DeviceType] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL DEFAULT ((0)),
	[CampaignID] [int] NULL,
	[PromptScheduleID] [int] NULL,
	[PromotionRemark] [nvarchar](2000) NULL,
	[AttachmentFilePath] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[Priority] [int] NULL,
	[Link] [varchar](512) NULL,
	[PromotionPicFile2] [varchar](512) NULL,
	[PromotionPicFile3] [varchar](512) NULL,
	[AttachFile1] [nvarchar](512) NULL,
	[AttachFile2] [nvarchar](512) NULL,
	[AttachFile3] [nvarchar](512) NULL,
 CONSTRAINT [PK_PromotionMsg] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionMsgCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息类型ID。 （暂定）。默认1
1：广告消息
2：店铺促销. 
3：生日促销  
4：VIP促销.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionMsgTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息标题1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionTitle1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息标题2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionTitle2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息标题3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionTitle3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息正文1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionMsgStr1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息正文2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionMsgStr2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息正文2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionMsgStr3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销图片索引' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日优惠显示：
0：无生日限制。 1： 生日当月. 2:生日当周。 3：生日当日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'BirthPromotion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定设备类型：
NULL或0:全部  1:Kiosk. 2:mobile 3:PC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'DeviceType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态
0：无效。 1：生效。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'CampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠有效日程设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromptScheduleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠备注信息。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息附件文件路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'AttachmentFilePath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级。 从小到大 递增 排序。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'保存Link的内容。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'Link'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销图片索引2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionPicFile2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销图片索引3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg', @level2type=N'COLUMN',@level2name=N'PromotionPicFile3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠信息表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg'
GO
