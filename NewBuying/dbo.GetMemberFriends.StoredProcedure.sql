USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberFriends]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberFriends]
  @MemberID				int
AS
/****************************************************************************
**  Name : 
**  Version: 1.0.0.3
**  Description : 返回用户好友
**
  exec GetMemberFriends 1
  select * from member
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**  Modify by: Gavin @2013-02-18 (ver 1.0.0.1) 增加返回好友的坐标: Longitude, Latitude
**  Modify by: Gavin @2013-02-19 (ver 1.0.0.2) 修正MemberFriend和member表的关联字段。（应该是用FriendMemberID）
**  Modify by: Gavin @2013-02-21 (ver 1.0.0.3) 增加返回字段：MemberPictureFile,M.MemberRegisterMobile, M.MemberSex, M.MemberEmail, M.MemberDateOfBirth。 
**MemberClause
****************************************************************************/
begin 
  select MemberFriendID, F.MemberID, FriendMemberID, FriendFamilyName, FriendGivenName, FriendSex, MobileNumber, F.CountryCode, EMail, F.Status,
    P.Longitude, P.Latitude, M.MemberPictureFile, M.MemberRegisterMobile, M.MemberSex, M.MemberEmail, M.MemberDateOfBirth
   from MemberFriend F left join MemberPosition P on F.FriendMemberID = P.MemberID
     left join Member M on F.FriendMemberID = M.MemberID
  where F.MemberID = @MemberID
  return 0 
end

GO
