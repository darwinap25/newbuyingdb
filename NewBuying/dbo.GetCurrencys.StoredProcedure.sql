USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCurrencys]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCurrencys]
  @CurrencyCode				   VARCHAR(64),			         -- 支付货币编码
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetCurrencys
**  Version : 1.0.0.2
**  Description : 获得支付货币的数据
**
sp_helptext GetCurrencys
select* from BUY_CURRENCY
select * from buy_product
  declare  @a int, @PageCount int, @RecordCount int
  exec @a = GetCurrencys null,   '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
**  Created by Gavin @2015-03-06
**  Modified by Gavin @2015-04-16 (ver 1.0.0.1) BUY_CURRENCY表增加字段CurrencyPicFile， SP增加返回此字段
**  Modify by Gavin @2015-12-25 (ver 1.0.0.2)  仅显示允许使用于POS端的Tender 
**
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SET @CurrencyCode = ISNULL(@CurrencyCode, '')
  SET @WhereStr = ' WHERE Status = 1 and (isnull(TerminalFlag,'''') = '''' or substring(TerminalFlag, 1, 1)=''Y'') '
  IF @CurrencyCode <> ''  
    SET @WhereStr = ' AND CurrencyCode=''' + @CurrencyCode + ''' '
    
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1
  
  SET @SQLStr = 'SELECT CurrencyID, CurrencyCode, CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN CurrencyName2 WHEN 3 THEN CurrencyName3 ELSE CurrencyName1 END AS CurrencyName, '
    + ' Rate,TenderType, CashSale, CouponValue, Base, MinAmt, MaxAmt, CardType, CardBegin, CardEnd, CardLen, AccountCode, ContraCode, '
    + ' PayTransfer, Refund_Type, CurrencyPicFile '
    + ' FROM BUY_CURRENCY '
    + @WhereStr

  EXEC SelectDataInBatchs @SQLStr, 'CurrencyCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr

  RETURN 0
END

GO
