USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessageServiceType]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageServiceType](
	[MessageServiceTypeID] [int] NOT NULL,
	[ServiceTypeClass] [int] NULL,
	[MessageServiceTypeName1] [nvarchar](512) NULL,
	[MessageServiceTypeName2] [nvarchar](512) NULL,
	[MessageServiceTypeName3] [nvarchar](512) NULL,
	[MessageServiceTypeURL] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MESSAGESERVICETYPE] PRIMARY KEY CLUSTERED 
(
	[MessageServiceTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageServiceType', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息服务大类。 SNS，SMS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageServiceType', @level2type=N'COLUMN',@level2name=N'ServiceTypeClass'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageServiceType', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageServiceType', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageServiceType', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'链接地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageServiceType', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeURL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息服务类型表
0、内部消息
1、邮件
2、短信
3、彩信
4、MSN
5、QQ
6、what''s APP
7、facebook
8、新浪微博
9, Apple Notification   ( IOS )
10, Google Notification ( Android )
99、Other' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageServiceType'
GO
