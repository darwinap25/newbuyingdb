USE [NewBuying]
GO
/****** Object:  Table [dbo].[ReceiveMessage]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceiveMessage](
	[ReceiveMessageID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[SendMessageID] [int] NULL,
 CONSTRAINT [PK_RECEIVEMESSAGE] PRIMARY KEY CLUSTERED 
(
	[ReceiveMessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveMessage', @level2type=N'COLUMN',@level2name=N'ReceiveMessageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveMessage', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息发送表主键。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReceiveMessage', @level2type=N'COLUMN',@level2name=N'SendMessageID'
GO
