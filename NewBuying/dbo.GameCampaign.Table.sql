USE [NewBuying]
GO
/****** Object:  Table [dbo].[GameCampaign]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameCampaign](
	[GameCampaignID] [int] IDENTITY(1,1) NOT NULL,
	[GameCampaignName1] [nvarchar](512) NULL,
	[GameCampaignName2] [nvarchar](512) NULL,
	[GameCampaignName3] [nvarchar](512) NULL,
	[GameID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[StoreID] [int] NULL,
	[StoreGroupID] [int] NULL,
	[AmountPerScore] [money] NULL,
	[PointsPerScore] [int] NULL,
	[CouponCountPerScore] [int] NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[BrandID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_GAMECAMPAIGN] PRIMARY KEY CLUSTERED 
(
	[GameCampaignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[GameCampaign] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[GameCampaign] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[GameCampaign] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏活动主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'GameCampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏活动名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'GameCampaignName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏活动名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'GameCampaignName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏活动名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'GameCampaignName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'GameID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态
0：无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'StoreGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每次游戏消费的金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'AmountPerScore'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每次游戏消费的积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'PointsPerScore'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每次游戏消费的优惠劵数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'CouponCountPerScore'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许参加游戏的CardTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许参加游戏的CardGradeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'游戏活动表
@2016-11-24 增加字段 BrandID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaign'
GO
