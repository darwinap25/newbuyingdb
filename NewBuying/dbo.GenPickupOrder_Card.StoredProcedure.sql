USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenPickupOrder_Card]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenPickupOrder_Card]
  @UserID                 int,             --操作员ID
  @CardOrderFormNumber    varchar(64),     --卡创建单的号码
  @IsEarchCard            int=0            -- 1: 输入每个Card，即一个Card一行。 0：连续Card合并。 默认0  
AS
/****************************************************************************
**  Name : GenPickupOrder_Card
**  Version: 1.0.0.3
**  Description : 自动根据卡订单产生拣货单
**  example :
  declare @a int  , @CardOrderFormNumber varchar(64)
  set @CardOrderFormNumber = ''
  exec @a = GenPickupOrder_Card 1, @CardOrderFormNumber  
  print @a  
  select * from Ord_CardOrderForm_H
  select * from Ord_CardPicking_H
  select * from Ord_CardPicking_D
**  Created by: Gavin @2012-09-20
**  Modify by: Gavin @2012-11-13 (ver 1.0.0.1) 修改CalcCheckDigit_EAN13 为CalcCheckDigit。
**  Modify by: Gavin @2014-10-08 (ver 1.0.0.2) 一系列表结构更改(和Coupon类似)
**  Modify by: Gavin @2014-11-05 (ver 1.0.0.3) 修正insert pick detail时读取的Card条件。
**
****************************************************************************/
begin
  declare @CardPickingNumber varchar(64), @BusDate date
  declare @KeyID int, @BrandID int, @CardQty int, @i int, @CardNumber varchar(64)
  declare @CardTypeID int, @CardGradeID int, @CardNumMask nvarchar(30), @CardNumPattern nvarchar(30), @PatternLen int, @Checkdigit int
  declare @StartCardNumber varchar(64), @EndCardNumber varchar(64), @PrevCardNumber varchar(64), @CardSeqNo bigint, @PickQty int
  declare @FirstCard varchar(64), @EndCard varchar(64)
  declare @IsImportCardNumber int, @IsConsecutiveUID int, @BatchCardCode varchar(64), @StartBatchCardCode varchar(64)
  declare @ModeID int, @ModeCode varchar(64)
  
  -- 获取busdate
  select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1    

  -- 获取pickup单的单号
  exec GetRefNoString 'COPOC', @CardPickingNumber output 
  -- 插入pickup单的头表数据 
  insert into Ord_CardPicking_H(CardPickingNumber, ReferenceNo, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod,
     SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, StoreMobile, FromContactName, FromContactNumber, 
     FromEmail, FromMobile, Remark, CreatedBusDate, ApproveStatus, CreatedBy, UpdatedBy, OrderType, Remark1) 
  select @CardPickingNumber, CardOrderFormNumber, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod,
     SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, StoreMobile, FromContactName, FromContactNumber, 
     FromEmail, FromMobile, Remark, @BusDate,'R', @UserID, @UserID, OrderType, Remark1 
    from Ord_CardOrderForm_H
   where CardOrderFormNumber = @CardOrderFormNumber
  
  -- 插入pickup单的子表数据
  DECLARE CUR_CardOrderForm_D CURSOR fast_forward for
    select max(BrandID) as BrandID, CardTypeID, CardGradeID, sum(isnull(D.CardQty,0))  
       from Ord_CardOrderForm_D D Left join Ord_CardOrderForm_H H on H.CardOrderFormNumber = D.CardOrderFormNumber 
      where D.CardOrderFormNumber = @CardOrderFormNumber
     group by CardTypeID, CardGradeID   
  OPEN CUR_CardOrderForm_D
  FETCH FROM CUR_CardOrderForm_D INTO @BrandID, @CardTypeID, @CardGradeID, @CardQty  
  WHILE @@FETCH_STATUS=0
  BEGIN
    if @IsEarchCard = 1
    begin
       insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, 
           FirstCardNumber, EndCardNumber, BatchCardCode)
       select CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, 
           FirstCardNumber, EndCardNumber, BatchCardCode
         from (           
          select ROW_NUMBER() OVER(order by C.CardNumber) as IID, @CardPickingNumber as CardPickingNumber , @CardTypeID as CardTypeID, @CardGradeID as CardGradeID, '' as [Description], 
                1 as OrderQty, 1 as PickQty, C.CardNumber as FirstCardNumber, C.CardNumber as EndCardNumber, B.BatchCardCode 
            from Card C left join BatchCard B on C.BatchCardID = B.BatchCardID
            where C.status = 0  and StockStatus = 2 and C.PickupFlag = 0 and C.CardTypeID = @CardTypeID and C.CardGradeID = @CardGradeID
          ) A
        where A.IID <= @CardQty 
       order by IID            
    end
    else  
    BEGIN
      -- 获得CardType的设置
      select @CardNumMask = case when isnull(G.CardNumMask, '') = '' then T.CardNumMask else G.CardNumMask end, 
         @CardNumPattern = case when isnull(G.CardNumPattern, '') = '' then T.CardNumPattern else G.CardNumPattern end,
         @Checkdigit =  case when isnull(G.CardCheckdigit, -1) = -1 then T.CardCheckdigit else G.CardCheckdigit end,
         @ModeID = case when isnull(G.CheckDigitModeID, -1) = -1 then T.CheckDigitModeID else G.CheckDigitModeID end
        from Card C 
        Left join CardType T on C.CardTypeID = T.CardTypeID 
        left join CardGrade G on C.CardGradeID = G.CardGradeID          
      select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
      set @ModeCode = isnull(@ModeCode, 'EAN13')
          
      set @PatternLen = 0
      while @PatternLen < Len(@CardNumMask)
      begin      
        if substring(@CardNumMask, @PatternLen + 1, 1) = 'A'
          set @PatternLen = @PatternLen + 1
        else  
          break  
      end
    
      -- 自动获取 Pickup Detail 的 Card号码 （循环） 
      set @i = 1
      set @PickQty = 0
      set @CardNumber = ''
      set @StartCardNumber = ''
      set @EndCardNumber = ''
      set @PrevCardNumber = ''
      DECLARE CUR_Card CURSOR fast_forward for
        select C.CardNumber, B.BatchCardCode from Card C left join BatchCard B on C.BatchCardID = B.BatchCardID
          where C.status = 0 and C.PickupFlag = 0 and C.StockStatus = 2 and C.CardTypeID = @CardTypeID and C.CardGradeID = @CardGradeID 
         order by C.CardNumber
	    OPEN CUR_Card
      FETCH FROM CUR_Card INTO @CardNumber, @BatchCardCode
      WHILE @@FETCH_STATUS=0 and @i <= @CardQty
      BEGIN    
        if @StartCardNumber = ''
        begin
          set @StartCardNumber = @CardNumber
          set @StartBatchCardCode = @BatchCardCode
          set @EndCardNumber = @CardNumber
        end
        if isnull(@PrevCardNumber, '') <> ''
        begin  
          if isnull(@IsImportCardNumber, 0) = 0
          begin
              -- 获得上一个cardnumber的序号。
              if @Checkdigit = 1 
                set @CardSeqNo = substring(@PrevCardNumber, @PatternLen + 1,  Len(@CardNumMask) - @PatternLen - 1) 
              else  
                set @CardSeqNo = substring(@PrevCardNumber, @PatternLen + 1,  Len(@CardNumMask) - @PatternLen)      
              -- 计算从上一个序号开始的连续序号和卡号
              set @CardSeqNo = @CardSeqNo + 1
              if @Checkdigit = 1 
              begin
                set @EndCardNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CardSeqNo), Len(@CardNumMask) - @PatternLen - 1)        
                select @EndCardNumber = dbo.CalcCheckDigit(@EndCardNumber, @ModeCode)
              end else        
                set @EndCardNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @CardSeqNo), Len(@CardNumMask) - @PatternLen)
          end else  
          begin
              if isnull(@IsConsecutiveUID, 1) = 1
              begin
                if @Checkdigit = 1
                begin 
                  set @EndCardNumber =  cast((cast(substring(@PrevCardNumber,1,len(@PrevCardNumber)-1) as bigint) + 1) as varchar)
                  select @EndCardNumber = dbo.CalcCheckDigit(@EndCardNumber, @ModeCode)
                end else  
                  set @EndCardNumber =  cast((cast(@PrevCardNumber as bigint) + 1) as varchar)
              end else
                set @EndCardNumber =  ''  
          end          
          -- 比较根据上一个序号计算出的号码是否和当前号码相同，如果相同则表示连续，如果不同，则表示不连续，需要另起一条记录。（上一条为空不包括）   
          if (@EndCardNumber <> @CardNumber) and (isnull(@PrevCardNumber,'') <> '')
          begin                      
	          insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, FirstCardNumber, EndCardNumber, BatchCardCode)
	          values(@CardPickingNumber, @CardTypeID, @CardGradeID, '', @CardQty, @PickQty, @StartCardNumber, @PrevCardNumber, @StartBatchCardCode) 
	          set @PickQty = 0 
	          set @StartCardNumber = @CardNumber
	          set @StartBatchCardCode = @BatchCardCode
          end
          set @EndCardNumber = @CardNumber
	      end
	      set @PickQty = @PickQty + 1	  
	      set @i = @i + 1
	      set @PrevCardNumber = @CardNumber	  
	      FETCH FROM CUR_Card INTO @CardNumber, @BatchCardCode  
      END
      CLOSE CUR_Card 
      DEALLOCATE CUR_Card 	
    -- END （循环） 自动获取 Pickup Detail 的 Coupon号码 
    -- 自动获取 Pickup Detail 的 Coupon号码 （循环外）
      if (@PickQty > 0) 
      begin
        insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, FirstCardNumber, EndCardNumber, BatchCardCode)
	      values(@CardPickingNumber, @CardTypeID, @CardGradeID, '', @CardQty, @PickQty, @StartCardNumber, @EndCardNumber, @StartBatchCardCode) 	
	      set @PickQty = 0   
	    end 
	
	  -- 没有库存的Card， 要根据Order的 detail记录， 加入pickup=0 的记录
	    if not exists(select * from Ord_CardPicking_D where CardPickingNumber = @CardPickingNumber and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID) 
	    begin
        insert into Ord_CardPicking_D(CardPickingNumber, CardTypeID, CardGradeID, [Description], OrderQty, PickQty, FirstCardNumber, EndCardNumber, BatchCardCode)
	      values(@CardPickingNumber, @CardTypeID, @CardGradeID, '', @CardQty, 0, '', '', '') 	
	      set @PickQty = 0  	
	    end
    END 	    
    FETCH FROM CUR_CardOrderForm_D INTO @BrandID, @CardTypeID, @CardGradeID, @CardQty  
  END
  CLOSE CUR_CardOrderForm_D 
  DEALLOCATE CUR_CardOrderForm_D 


  -- 更新Ord_CardOrderForm_H状态到  Ｃ
  update Ord_CardOrderForm_H set ApproveStatus = 'C' where CardOrderFormNumber = @CardOrderFormNumber 
  
  -- update Card的 PickupFlag
    DECLARE CUR_PickupCard_Detail CURSOR fast_forward for
      select FirstCardNumber, EndCardNumber, CardTypeID, CardGradeID from Ord_CardPicking_D 
      where CardPickingNumber = @CardPickingNumber
	  OPEN CUR_PickupCard_Detail
    FETCH FROM CUR_PickupCard_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
    WHILE @@FETCH_STATUS=0 
    BEGIN 
      update card set PickupFlag = 1, StockStatus = 4 
        where CardNumber >= @FirstCard and  CardNumber <= @EndCard and CardTypeID = @CardTypeID
	    FETCH FROM CUR_PickupCard_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
	  END
    CLOSE CUR_PickupCard_Detail 
    DEALLOCATE CUR_PickupCard_Detail 
              
  return 0
end

GO
