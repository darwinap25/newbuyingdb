USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberShoppingCart]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberShoppingCart](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [bigint] NOT NULL,
	[ProdCodeStyle] [varchar](64) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[ProdDesc] [varchar](512) NULL,
	[ProdTitle] [varchar](512) NULL,
	[ProdPIC] [varchar](512) NULL,
	[Qty] [int] NULL,
	[RetailPrice] [money] NULL,
	[RetailAmount] [money] NULL,
	[Memo] [varchar](max) NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[Status] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_MEMBERSHOPPINGCART] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品stylecode，如果系统不使用style，则同样填写prodcode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'ProdCodeStyle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员收藏货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品描述。（会员在浏览器中看到的货品描述）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'ProdDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'title显示内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'ProdTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'ProdPIC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'加入到购物车中的货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品建议零售价' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'RetailPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品销售金额。（RetailPrice *  Qty）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'RetailAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'Memo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。0：没有被选中。 1：已经选中。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员的购物车。（会员在浏览器中添加到购物车的货品）
@2016-10-17  因为Nick需要使用自己的ID作为MemberID，所以MemberID需从Int类型扩展到 BigInt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberShoppingCart'
GO
