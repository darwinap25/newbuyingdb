USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoGenSalesPickupOrderByBrand]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[AutoGenSalesPickupOrderByBrand]
  @UserID				         INT,	            -- 操作员ID
  @TxnNo                         VARCHAR(64)        -- 交易号码
AS
/****************************************************************************
**  Name : AutoGenSalesPickupOrderByBrand (for 机场)
**  Version: 1.0.0.1
**  Description : 必须状态是>=4的交易单(PS). 单子中所有需要扣库存的货品.  Collected状态目前不太确定,暂定只要是 2和4的都产生pick单
                  自动产生Picking单，根据交易单中的货品的运营商brand，来拆分picking单。
				  (for HK AirPort)
select * from Ord_SalesPickOrder_H
select * from Ord_SalesPickOrder_D
sp_Helptext AutoGenSalesPickupOrderByBrand
update Ord_SalesPickOrder_D set actualqty = 4
select * from sales_H where status = 4
exec GenSalesPickupOrder 1,'KTXNNO000001028' 
**  Created by: Gavin @2016-08-19  
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @TxnDate DATETIME, @StoreCode varchar(64),
          @StoreBrandCode VARCHAR(64)

  SELECT @BusDate = BusDate, @TxnDate = TxnDate, @StoreCode = StoreCode FROM Sales_H WHERE TransNum = @TxnNo AND Status >= 4
  IF  @@ROWCOUNT = 0
  BEGIN 
    RETURN 0
  END ELSE
  BEGIN 
    DECLARE CUR_AutoGenSalesPickup CURSOR fast_forward FOR
      SELECT distinct B.StoreBrandCode FROM (SELECT ProdCode FROM Sales_D WHERE TransNum = @TxnNo AND Collected IN (2,4)) A 
	    LEFT JOIN BUY_PRODUCT B ON A.ProdCode = B.ProdCode
    OPEN CUR_AutoGenSalesPickup
    FETCH FROM CUR_AutoGenSalesPickup INTO @StoreBrandCode
    WHILE @@FETCH_STATUS=0
    BEGIN
	  EXEC GetRefNoString 'SPUO', @NewNumber OUTPUT
	  INSERT INTO Ord_SalesPickOrder_D(SalesPickOrderNumber,ProdCode,OrderQty,ActualQty,StockTypeCode,Remark)
	  SELECT @NewNumber, A.ProdCode, TotalQty, 0, 'G', '' FROM Sales_D A
	    LEFT JOIN BUY_PRODUCT B ON A.ProdCode = B.ProdCode
	  WHERE TransNum = @TxnNo AND Collected IN (2,4) AND B.StoreBrandCode = @StoreBrandCode
	  IF @@ROWCOUNT <> 0
	  BEGIN
		  INSERT INTO Ord_SalesPickOrder_H(SalesPickOrderNumber,OrderType,MemberID,CardNumber,ReferenceNo,PickupLocation,PickupStaff,PickupDate,
			  DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,CreatedBusDate,ApproveBusDate,
			  ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		  SELECT @NewNumber, PickupType, MemberID,CardNumber,TransNum,PickupStoreCode,@UserID,GETDATE(),
			  PickupType,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
			  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,@BusDate,null,
			  '','P',null,@UserID,GETDATE(),@UserID,GETDATE(),@UserID 
		  FROM Sales_H WHERE TransNum = @TxnNo
	  END
      FETCH FROM CUR_AutoGenSalesPickup INTO @StoreBrandCode
    END
    CLOSE CUR_AutoGenSalesPickup 
    DEALLOCATE CUR_AutoGenSalesPickup 
  END

  RETURN 0
END

GO
