USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSTenderReport]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[POSTenderReport]
  @StoreCode          VARCHAR(64),     -- StoreCode
  @RegisterCode       VARCHAR(64),     -- RegisterCode
  @BusDate            DATETIME,        -- 交易日
  @CashierID          INT              -- 收银员
AS
/****************************************************************************
**  Name : POSTenderReport
**  Version : 1.0.0.1
**  Description : POS付款报表
**
declare @a int
exec @a=POSTenderReport '1','R01','2015-03-24',1
print @a
select * from posstaff
select * from sales_h

**  Created by Gavin @2015-03-26
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
****************************************************************************/
BEGIN
  SET NOCOUNT ON
  DECLARE @TenderDesc VARCHAR(64), @TenderType INT, @COUNT INT, @LocalAmount MONEY, @LINESUM MONEY
  DECLARE @LineLen INT, @LineStr VARCHAR(100), @SPACESTR VARCHAR(50)
  DECLARE @Report TABLE(KeyID INT IDENTITY(1,1), LineStr VARCHAR(100))  
  SET @LineLen = 100
  SET @SPACESTR = '                                                                 '
  DECLARE @TotalQty INT, @TotalAmt MONEY, @BankTotalQty INT, @BankTotalAmt MONEY
  SET @TotalAmt = 0
  SET @TotalQty = 0
  SET @BankTotalAmt = 0
  SET @BankTotalQty = 0  
  SET @CashierID = ISNULL(@CashierID, 0)
  SET @BusDate = ISNULL(@BusDate, 0)
  IF @BusDate = 0
    SELECT TOP 1 @BusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode  ORDER BY BusDate desc
      
  SET @LineStr = '******************************************'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '               CONSOLIDATED'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '                 付款报表'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '******************************************'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = LEFT(RTRIM('店舖號:' + @StoreCode) + @SPACESTR, 24)
  SET @LineStr = @LineStr + RTRIM('交易日:' + CONVERT(VARCHAR(10), @BusDate, 120))
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)  
  
  -- 数据开始
  DECLARE CUR_POSTenderReport CURSOR fast_forward FOR
    SELECT MAX(A.TenderDesc), MAX(A.TenderType), COUNT(*), SUM(ISNULL(A.LocalAmount, 0))   
      FROM sales_T A LEFT JOIN sales_H B ON A.TransNum = B.TransNum
      WHERE B.StoreCode = @StoreCode and B.RegisterCode = @RegisterCode and B.BusDate = @BusDate
         AND B.InvalidateFlag = 0 AND B.Status >= 4 AND B.TransType NOT IN (4,5)
         AND (B.CashierID = @CashierID or @CashierID = 0) 
    GROUP BY A.TenderID
    ORDER BY A.TenderID
  OPEN CUR_POSTenderReport
  FETCH FROM CUR_POSTenderReport INTO @TenderDesc, @TenderType, @COUNT, @LocalAmount
  WHILE @@FETCH_STATUS=0
  BEGIN
    SET @LineStr = LEFT(@TenderDesc + @SPACESTR, 16) + RIGHT(@SPACESTR + CAST(@COUNT AS VARCHAR) + '/', 11) + RIGHT(@SPACESTR + 'RMB ' + CAST(ISNULL(@LocalAmount,0) AS VARCHAR), 15)
    INSERT INTO @Report  VALUES(@LineStr)  
         
    SET @TotalQty = @TotalQty + ISNULL(@COUNT, 0)
    SET @TotalAmt = @TotalAmt + ISNULL(@LocalAmount, 0)
    IF @TenderType IN (4,5,6)
    BEGIN
      SET @BankTotalQty = @BankTotalQty + ISNULL(@COUNT, 0)
      SET @BankTotalAmt = @BankTotalAmt + ISNULL(@LocalAmount, 0)    
    END
    FETCH FROM CUR_POSTenderReport INTO @TenderDesc, @TenderType, @COUNT, @LocalAmount
  END
  CLOSE CUR_POSTenderReport 
  DEALLOCATE CUR_POSTenderReport     
  -- 数据结束      
  
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = LEFT('银行卡' + @SPACESTR, 16) + RIGHT(@SPACESTR + CAST(ISNULL(@BankTotalQty,0) AS VARCHAR) + '/', 11) + RIGHT(@SPACESTR + 'RMB ' + CAST(ISNULL(@BankTotalAmt,0) AS VARCHAR), 15)
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '=========================================='
  SET @LineStr = LEFT('支付总计' + @SPACESTR, 16) + RIGHT(@SPACESTR + 'RMB ' + CAST(ISNULL(@TotalAmt,0) AS VARCHAR), 26)
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = LEFT('定金' + @SPACESTR, 16) + RIGHT(@SPACESTR + 'RMB 0.00', 26)
  INSERT INTO @Report  VALUES(@LineStr)  
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)     
  
    SELECT @LINESUM = SUM(ISNULL(A.LocalAmount, 0))   
      FROM sales_T A LEFT JOIN sales_H B ON A.TransNum = B.TransNum
      WHERE B.StoreCode = @StoreCode and B.RegisterCode = @RegisterCode and B.BusDate = @BusDate
         AND B.InvalidateFlag = 0 AND B.Status = 5 AND B.TransType NOT IN (4,5)
         AND (B.CashierID = @CashierID or @CashierID = 0)   
  SET @LineStr = LEFT('净销售' + @SPACESTR, 16) + RIGHT(@SPACESTR + 'RMB ' + CAST(ISNULL(@LINESUM,0) AS VARCHAR), 26)
  INSERT INTO @Report  VALUES(@LineStr)           
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)       
  
  SET @LineStr = CONVERT(VARCHAR(19), GETDATE(), 120) + RIGHT(@SPACESTR + @StoreCode, 8) + RIGHT(@SPACESTR + @RegisterCode, 4)+ RIGHT(@SPACESTR + CAST(@CashierID as VARCHAR), 11)
  INSERT INTO @Report  VALUES(@LineStr) 
  SET @LineStr = @SPACESTR
  INSERT INTO @Report  VALUES(@LineStr)   
  SET @LineStr = '                  報告結束'
  INSERT INTO @Report  VALUES(@LineStr)                 
                    
  SELECT * FROM  @Report     
  SET NOCOUNT OFF  
  RETURN 0
END

GO
