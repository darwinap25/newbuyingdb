USE [NewBuying]
GO
/****** Object:  Table [dbo].[Promotion_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion_H](
	[PromotionCode] [varchar](64) NOT NULL,
	[PromotionDesc1] [varchar](512) NULL,
	[PromotionDesc2] [varchar](512) NULL,
	[PromotionDesc3] [varchar](512) NULL,
	[PromotionNote] [varchar](512) NULL,
	[StoreID] [int] NULL,
	[StoreGroupID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartTime] [datetime] NULL DEFAULT ('0'),
	[EndTime] [datetime] NULL DEFAULT ('0'),
	[DayFlagID] [int] NULL,
	[WeekFlagID] [int] NULL,
	[MonthFlagID] [int] NULL,
	[LoyaltyFlag] [int] NULL DEFAULT ((0)),
	[Priority] [int] NULL,
	[TopLimit] [int] NULL DEFAULT ((1)),
	[MutexFlag] [int] NULL,
	[HitRelation] [int] NULL DEFAULT ((1)),
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[SegmentFlag] [int] NULL,
 CONSTRAINT [PK_PROMOTION_H] PRIMARY KEY CLUSTERED 
(
	[PromotionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Delete_Promotion_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Delete_Promotion_H] ON [dbo].[Promotion_H]
after DELETE
AS
/*==============================================================*/
begin
  declare @PromotionCode varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)

  DECLARE CUR_Delete_Promotion_H CURSOR fast_forward local FOR
    SELECT PromotionCode, ApproveStatus, CreatedBy  FROM Deleted
  OPEN CUR_Delete_Promotion_H
  FETCH FROM CUR_Delete_Promotion_H INTO @PromotionCode, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    exec ChangePromotionHitPLUList  @PromotionCode, 0  
                  
    FETCH FROM CUR_Delete_Promotion_H INTO @PromotionCode, @ApproveStatus, @CreatedBy    
  END
  CLOSE CUR_Delete_Promotion_H
  DEALLOCATE CUR_Delete_Promotion_H
end

GO
/****** Object:  Trigger [dbo].[Update_Promotion_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Promotion_H] ON [dbo].[Promotion_H]
after INSERT, UPDATE
AS
--update Promotion_H set approvestatus = 'A' where PromotionCode = 'PROM00000000027'
/*==============================================================*/
begin
  declare @PromotionCode varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)

  DECLARE CUR_Update_Promotion_H CURSOR fast_forward local FOR
    SELECT PromotionCode, ApproveStatus, CreatedBy  FROM INSERTED
  OPEN CUR_Update_Promotion_H
  FETCH FROM CUR_Update_Promotion_H INTO @PromotionCode, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where PromotionCode = @PromotionCode
  
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output
      exec ChangePromotionHitPLUList  @PromotionCode, 0               
      update Promotion_H set ApprovalCode = @ApprovalCode  where PromotionCode = @PromotionCode
    end
     
    FETCH FROM CUR_Update_Promotion_H INTO @PromotionCode, @ApproveStatus, @CreatedBy    
  END
  CLOSE CUR_Update_Promotion_H
  DEALLOCATE CUR_Update_Promotion_H
end

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'适用的店铺ID。0/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。0/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'StoreGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效时间（例如 上午9:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'StartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销失效时间（例如 下午20:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'EndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一月中促销生效日 （Buy_DayFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'DayFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一周中促销生效日 （Buy_WeekFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'WeekFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效月 （Buy_MonthFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'MonthFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否仅会员促销。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'LoyaltyFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上限数量。  一单中此促销允许重复的上限。默认1.  只允许出现一次。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'TopLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'互斥标志。 0 : 可以和其他促销共同出现。 1：只允许这一条出现。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'MutexFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'hit条件之间的逻辑关系。 1： and 。 2：or   默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'HitRelation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否有Segment条件。 0：没有。 1： 有。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H', @level2type=N'COLUMN',@level2name=N'SegmentFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销头表。 （包含所有被动促销设置.  促销会根据具体的销售单，顾客情况，自动匹配合适的促销方案）
@2016-01-21： 增加字段SegmentFlag， 以配合Promotion_Segment表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_H'
GO
