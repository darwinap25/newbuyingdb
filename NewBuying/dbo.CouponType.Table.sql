USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponType]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponType](
	[CouponTypeID] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeNatureID] [int] NOT NULL DEFAULT ((1)),
	[CouponTypeCode] [varchar](64) NOT NULL,
	[CouponTypeName1] [nvarchar](512) NULL,
	[CouponTypeName2] [nvarchar](512) NULL,
	[CouponTypeName3] [nvarchar](512) NULL,
	[BrandID] [int] NOT NULL,
	[CouponTypeRank] [int] NULL,
	[CouponTypeNotes] [nvarchar](max) NULL,
	[CouponNumMask] [nvarchar](512) NOT NULL,
	[CouponNumPattern] [nvarchar](512) NOT NULL,
	[CouponCheckdigit] [int] NULL DEFAULT ((0)),
	[CheckDigitModeID] [int] NULL,
	[CouponNumberToUID] [int] NULL DEFAULT ((0)),
	[IsImportCouponNumber] [int] NULL DEFAULT ((0)),
	[IsConsecutiveUID] [int] NULL DEFAULT ((1)),
	[UIDCheckDigit] [int] NULL DEFAULT ((0)),
	[UIDToCouponNumber] [int] NULL DEFAULT ((0)),
	[CouponValidityDuration] [int] NOT NULL,
	[CouponValidityUnit] [int] NOT NULL DEFAULT ((0)),
	[CouponSpecifyExpiryDate] [datetime] NULL,
	[EffectScheduleID] [int] NULL,
	[CouponVerifyMethod] [int] NULL DEFAULT ((0)),
	[CurrencyID] [int] NULL,
	[CouponTypeStartDate] [datetime] NULL,
	[CouponTypeEndDate] [datetime] NULL,
	[CoupontypeFixedAmount] [int] NULL DEFAULT ((0)),
	[CouponTypeAmount] [money] NOT NULL,
	[CouponTypePoint] [int] NOT NULL,
	[CouponTypeDiscount] [decimal](16, 6) NOT NULL DEFAULT ((1)),
	[Status] [int] NOT NULL DEFAULT ((0)),
	[CouponTypeLayoutFile] [nvarchar](512) NULL,
	[CouponTypePicFile] [nvarchar](512) NULL,
	[CampaignID] [int] NULL,
	[LocateStore] [int] NULL DEFAULT ((0)),
	[IsMemberBind] [int] NULL DEFAULT ((1)),
	[ActiveResetExpiryDate] [int] NULL DEFAULT ((0)),
	[CouponTypeTransfer] [int] NULL,
	[PasswordRuleID] [int] NULL,
	[CouponforfeitControl] [int] NULL,
	[AutoReplenish] [int] NULL DEFAULT ((0)),
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[CouponTypeRedeemCount] [int] NULL DEFAULT ((0)),
	[SponsorID] [int] NULL,
	[SponsoredValue] [decimal](16, 6) NULL,
	[IsPhysicalCoupon] [int] NULL DEFAULT ((0)),
	[IsNeedVerifyNum] [int] NULL DEFAULT ((0)),
	[VerifyNumLifecycle] [int] NULL DEFAULT ((10)),
	[SupplierID] [int] NULL,
	[AllowDeleteCoupon] [int] NULL DEFAULT ((1)),
	[AllowShareCoupon] [int] NULL DEFAULT ((1)),
	[QRCodePrefix] [varchar](64) NULL,
	[MaxDownloadCoupons] [int] NULL,
	[CouponReturnValue] [int] NULL,
	[TrainingMode] [int] NULL DEFAULT ((0)),
	[UnlimitedUsage] [int] NULL DEFAULT ((0)),
	[StaffCouponFlag] [int] NULL DEFAULT ((0)),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[LastSequenceGenerated] [bigint] NULL,
	[BrandCode] [varchar](64) NULL,
 CONSTRAINT [CouponType_PK] PRIMARY KEY CLUSTERED 
(
	[CouponTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型种类ID，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeNatureID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠券编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联的CouponNature中的品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'優惠券等級的級別，數字越大，級別越高' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeRank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeNotes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号码规则' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponNumMask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号码规则的初始值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponNumPattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号是否包含校验位。 0：没有。 1：有。 默认0。  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponCheckdigit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验位产生逻辑表的外键（预留）。 CouponCheckdigit设置为1时生效。 
CheckDigitMode 表ID。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CheckDigitModeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建CouponNumber时，是否同步产生UID。 默认：0。 
0：不产生UID。
1：原样复制CouponNumber 到 UID
2：复制CouponNumber 到 UID，并删除最后一位。    （不考虑CouponNumber 本身是否已经有了checkdigit）
3：复制CouponNumber 到 UID，并增加checkdigit，加在最后一位。 （不考虑CouponNumber 本身没有checkdigit的情况）
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponNumberToUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'coupon号码是否需要导入。0：SVA系统根据coupontype来产生。1：号码导入，不是自动产生。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'IsImportCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UID号码是否为连续的。 0：不连续。1：连续，默认1   
（注：IsImportCouponNumber=1是有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'IsConsecutiveUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UID是否含有checkdigit。0：没有。1：有。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'UIDCheckDigit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据UID号码产生couponnumber 控制。  默认0： 不根据UID产生。
0：UID  binding  CouponNumber
1：原样复制UID到CouponNumber
2：复制UID到CouponNumber，并删除最后一位。    （不考虑UID本身是否已经有了checkdigit）
3：复制UID到CouponNumber，并增加checkdigit，加在最后一位。 （不考虑UID本身没有checkdigit的情况）
注： CD：checkdigit位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'UIDToCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵有效期值。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponValidityDuration'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵有效期持续时间长度 的单位：默认0，不需要检查CouponValidityDuration值
0：永久。 
1：年。 
2：月。 
3：星期。 
4：天。 
5: 有效期不变更。 
6: 指定失效日期。（失效日期在CouponSpecifyExpiryDate设置）
7：指定生效日程（在EffectSchedule中设置ID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponValidityUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建的coupon，指定的失效日期。（当CouponValidityUnit=6时生效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponSpecifyExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon生效日程ID。（外键，还未实现）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'EffectScheduleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'验证方式。
NULL或0：不验证。 1：Visual Verify。 2：Online Verify。3：Visual Verify & Negative file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponVerifyMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'币种ID，外键
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CurrencyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型开始生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeStartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeEndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'定额优惠劵。0：不定值。1：普通优惠劵。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CoupontypeFixedAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵价值，金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵价值，积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypePoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵价值，现金折扣值（7折，记录为: 0.3）默认1  （sales off）
针对整单' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵种类状态。
0：失效。 1：生效。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可存放优惠劵封面的打印模板文件 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeLayoutFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可存放优惠劵封面的图片相对路径名称。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypePicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类似store group 的 附加字段。 作为coupontype分类使用。
(由于Campaign表中加了brandID， 所以要求前台输入时保证campaign中的brandid和 coupontype中brandid一致)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否领取店铺本店使用。0：不是。1：是。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'LocateStore'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否要绑定会员。0：不是，1：是的。 默认1。  如果要绑定会员，必须绑定会员后激活。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'IsMemberBind'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'激活时是否要重置有效期. 0：不重置。 1：重置，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'ActiveResetExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵转赠：0：不允许。1：同品牌转赠。2：同CardGrade。3：同CardType。 4：任意转赠' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码规则表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'PasswordRuleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'coupon forfeit控制。 （使用的coupon金额（只对金额coupon有效），小于coupon金额， 多余的金额是否被 没收）
0：直接forfeit。（默认）。  1：当日forfeit（当日可继续使用。EOD时统一执行）。 2：不forfeit。（金额未使用完，则状态不改变，可以继续使用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponforfeitControl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自动补货。1：补货。0: 不补货' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'AutoReplenish'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未使用。（SVAWeb框架中加了，但实际未使用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'StartDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未使用。（SVAWeb框架中加了，但实际未使用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'EndDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'coupon的使用总数限制。默认0 表示不限制。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponTypeRedeemCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键。赞助商' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'SponsorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'赞助百分比。
（Coupon可以有赞助商（只能一个），可以设置coupon价值的百分比为供应商提供）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'SponsoredValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否实体优惠劵。0：不是。1：是的。 默认0
= 1 时，属于实体Coupon，不只是SVA的coupon数据，还有实体Coupon的库存数，两者要绑定。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'IsPhysicalCoupon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否在使用时需要验证。0：不需要。1：需要。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'IsNeedVerifyNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果IsNeedVerifyNum=1，这里填写验证码的有效期。 单位：分钟。 默认10。
IsNeedVerifyNum=0时无效。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'VerifyNumLifecycle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'默认供应商ID。（补货时需要使用）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'SupplierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许删除Coupon。0：不允许。1：允许。默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'AllowDeleteCoupon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许分享Coupon。0：不允许。1：允许。默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'AllowShareCoupon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QRCode的 Prefix。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'QRCodePrefix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许下载的最大Coupon数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'MaxDownloadCoupons'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当被转赠的用户没有首次登录，超过指定天数，则归还给赠送方。  单位： 天' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'CouponReturnValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'training mode. 默认0.  1：yes. 0:No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'TrainingMode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：Coupon可无限制使用，不会变成Redeem状态，Coupon中的金额不会变化。  0：正常Coupon .  默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'UnlimitedUsage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工Coupon标志，默认0。 客户可以对此标志定义' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType', @level2type=N'COLUMN',@level2name=N'StaffCouponFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型表
@2014-12-05 删除字段：CouponNatureID。因为创建了CouponNature_List表，支持CouponType和CouponNature的多对多关系。
@2014-12-05 增加字段：StaffCouponFlag， 可以自定义
   Bauhaus： 使用此标志的coupon， 整单交易不积分，交易金额不累计。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponType'
GO
