USE [NewBuying]
GO
/****** Object:  Table [dbo].[PromotionMsg_Pic]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PromotionMsg_Pic](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PromotionMsgCode] [varchar](64) NULL,
	[ExtraPic] [varchar](512) NULL,
	[PicRemark] [varchar](512) NULL,
 CONSTRAINT [PK_PROMOTIONMSG_PIC] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Delete_PromotionMsg_PIC]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Delete_PromotionMsg_PIC] ON [dbo].[PromotionMsg_Pic]
FOR DELETE
AS
/*==============================================================*/
/*                
* Name: Delete_PromotionMsg_PIC
* Version: 1.0.0.0
* Description : 删除记录时触发 
*  
** Create By Gavin @2014-09-22
*/
/*==============================================================*/
BEGIN 
  -- 触发一条，读一批的，所以不用游标，只管一个code
  declare @PromotionMsgCode varchar(64), @PIC1 varchar(512), @PIC2 varchar(512), @PIC3 varchar(512)
  declare @delPIC varchar(512)
  SELECT @PromotionMsgCode = PromotionMsgCode, @delPIC = ExtraPic FROM DELETED   
  
  update PromotionMsg set PromotionPicFile = case when @delPIC = PromotionPicFile then '' else PromotionPicFile end,
    PromotionPicFile2 = case when @delPIC = PromotionPicFile2 then '' else PromotionPicFile2 end,
    PromotionPicFile3 = case when @delPIC = PromotionPicFile3 then '' else PromotionPicFile3 end
  where PromotionMsgCode = @PromotionMsgCode
  
END

GO
/****** Object:  Trigger [dbo].[Update_PromotionMsg_PIC]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_PromotionMsg_PIC] ON [dbo].[PromotionMsg_Pic]
FOR INSERT,UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_PromotionMsg_PIC
* Version: 1.0.0.0
* Description : 更改记录时触发 
*  
** Create By Gavin @2014-09-19
*/
/*==============================================================*/
BEGIN 
  -- 触发一条，读一批的，所以不用游标，只管一个code
  declare @PromotionMsgCode varchar(64), @PIC1 varchar(512), @PIC2 varchar(512), @PIC3 varchar(512)
  SELECT @PromotionMsgCode = PromotionMsgCode FROM inserted 
  
  select @PIC1 = max(P1), @PIC2 = max(P2), @PIC3 = max(P3)
  from (
    select  
      case when iid = 1 then ExtraPic else '' end as P1,
      case when iid = 2 then ExtraPic else '' end as P2,
      case when iid = 3 then ExtraPic else '' end as P3
    from (
      select ROW_NUMBER() OVER(order by KeyID) as iid, ExtraPic 
      from PromotionMsg_PIC where PromotionMsgCode = @PromotionMsgCode
    ) A
  ) B  
  
  update PromotionMsg set PromotionPicFile = @PIC1, PromotionPicFile2 = @PIC2, PromotionPicFile3 = @PIC3 
  where PromotionMsgCode = @PromotionMsgCode
  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg_Pic', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg_Pic', @level2type=N'COLUMN',@level2name=N'PromotionMsgCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg_Pic', @level2type=N'COLUMN',@level2name=N'ExtraPic'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg_Pic', @level2type=N'COLUMN',@level2name=N'PicRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销消息图片表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsg_Pic'
GO
