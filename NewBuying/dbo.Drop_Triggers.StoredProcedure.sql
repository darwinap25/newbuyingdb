USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[Drop_Triggers]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Drop_Triggers]   
AS  
/*================================================================================  
=================================================================================*/  
begin  
declare @SQLString varchar(8000),@TableName varchar(100)  
declare TableList cursor for select  Name from sysobjects where xtype='U'  
and Name in (  
'Accounts_PermissionCategories',  
'Accounts_Permissions',  
'Accounts_RolePermissions',  
'Accounts_Roles',  
'Accounts_UserRoles',  
'Accounts_Users',  
'Address',  
'Bank',  
'Brand',  
'Campaign',  
'CardAutoAddValueRule',  
'CardExtensionRule',  
'CardGrade',  
'CardGradeStoreCondition',  
'CardGradeStoreCondition_List',  
'CardIssuer',  
'CardType',  
'CardTypeNature',  
'Company',  
'CouponType',  
'CouponTypeExchangeBinding',  
'CouponTypeNature',  
'CouponTypeStoreCondition',  
'CouponTypeStoreCondition_List',  
'CourierPosition',  
'Currency',  
'Customer',  
'Department',  
'DialogMessage',  
'DialogMessage_Lan',  
'DistributeTemplate',  
'EarnCouponRule',  
'Education',  
'Game',  
'GameCampaign',  
'GameCampaignPrize',  
'Industry',  
'InitialOptions',  
'Lan_Accounts_PermissionCategories',  
'Lan_Accounts_Permissions',  
'Lan_Accounts_Roles',  
'Lan_List',  
'Lan_S_Tree',  
'LanguageMap',  
'Location',  
'MiscellaneousMapping',  
'Nation',  
'OperationTable',  
'Organization',  
'PasswordRule',  
'PointRule',  
'Position',  
'Product',  
'Product_Price',  
'Profession',  
'Promotion_G',  
'Promotion_H',  
'Promotion_I',  
'PromotionMsg',  
'Reason',  
'RelationRoleBrand',  
'RelationRoleIssuer',  
'ResultTable',  
'RSAKeys',  
'RSAKeyTable',  
'Schedule',  
'ScheduleWeek',  
'SNSType',  
'Store',  
'StoreCondition_D',  
'StoreGroup',  
'StoreType',  
'TENDER',  
'ThirdParty',  
'ThirdPartyExchange',  
'USEFULEXPRESSIONS',  
'VENDOR')  
open TableList  
fetch next from TableList into @TableName  
if @@fetch_status=0  
begin  
  while @@fetch_status=0  
  begin  
  
    set @SQLString='  
DROP TRIGGER '+@TableName+'_AuditTrail_Insert  '  
    Execute(@SQLString)  
  
    set @SQLString='  
DROP TRIGGER '+@TableName+'_AuditTrail_Update  '  
    Execute(@SQLString)  
  
    set @SQLString='  
DROP TRIGGER '+@TableName+'_AuditTrail_Delete  '  
    Execute(@SQLString)  
    fetch next from TableList into @TableName  
  end  
end  
close TableList  
deallocate TableList  
end  

GO
