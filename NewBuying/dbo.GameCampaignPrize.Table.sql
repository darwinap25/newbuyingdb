USE [NewBuying]
GO
/****** Object:  Table [dbo].[GameCampaignPrize]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameCampaignPrize](
	[GameCampaignPrizeID] [int] IDENTITY(1,1) NOT NULL,
	[PrizeCode] [varchar](64) NULL,
	[GameCampaignID] [int] NOT NULL,
	[PrizeType] [int] NOT NULL,
	[CouponTypeID] [nvarchar](64) NOT NULL,
	[PrizeDesc1] [nvarchar](512) NULL,
	[PrizeDesc2] [nvarchar](512) NULL,
	[PrizeDesc3] [nvarchar](512) NULL,
	[ResetMethod] [int] NULL,
	[MustWin] [int] NULL,
	[MaxQTY] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[LastWinDate] [datetime] NULL,
	[Probability] [int] NULL,
	[ProbabilityMax] [int] NULL,
	[LayOut] [nvarchar](512) NULL,
	[PrizeHours] [int] NULL,
	[PrizeDay] [int] NULL,
	[PrizeWeek] [int] NULL,
	[PrizeMon] [int] NULL,
	[PrizeYear] [int] NULL,
	[PrizeUserOnce] [int] NULL,
	[PrizeUserNextTime] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_GAMECAMPAIGNPRIZE] PRIMARY KEY CLUSTERED 
(
	[GameCampaignPrizeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [ResetMethod]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [MustWin]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [ProbabilityMax]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [PrizeHours]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [PrizeDay]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [PrizeWeek]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [PrizeMon]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [PrizeYear]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [PrizeUserOnce]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [PrizeUserNextTime]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[GameCampaignPrize] ADD  DEFAULT ((0)) FOR [UpdatedBy]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'GameCampaignPrizeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键code。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'GameCampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励类别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖品类型ID（优惠劵）.（改成填入coupontypecode）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖励描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重置方法。
0：不做重置。 1：每日重置奖品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'ResetMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'必须赢  0：不是。 1：是的  默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'MustWin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大赢取数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'MaxQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后赢的日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'LastWinDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'中奖率。  记录中奖率的分子。 例如：70%中奖率， 这里就记录7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'Probability'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'中奖率。  记录中奖率的分母。 例如：70%中奖率， 这里就记录10.  0 表示必须中奖' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'ProbabilityMax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'奖项图片存放目录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'LayOut'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每小时允许送出的奖数  -- 0:不限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeHours'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每天允许送出的奖数  -- 0:不限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeDay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每星期允许送出的奖数  -- 0:不限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeWeek'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每月允许送出的奖数  -- 0:不限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeMon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每年允许送出的奖数  -- 0:不限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeYear'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一个用户只能获取一次该奖  -- 0:不限制   1:每用户只能获取一次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeUserOnce'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此用户中奖后，下次再次中奖的间隔  -- 0:不限制   x:分钟' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize', @level2type=N'COLUMN',@level2name=N'PrizeUserNextTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动奖励表
Nick @2016-11-23，为GameCampaignPrize奖项表，增加新的字段 
@2016-12-01， 按Nick要求，修改Probability含义：中奖率。  记录中奖率的分子。 例如：70%中奖率， 这里就记录7
   增加新字段：ProbabilityMax：中奖率。  记录中奖率的分母。 例如：70%中奖率， 这里就记录10.  0 表示必须中奖
@2016-12-XX， CouponTypeID改成 nvarchar（64）， 填入 coupontypecode 内容。
@2016-12-20， 增加 PrizeCode varchar(64)。  用于外部导入code。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameCampaignPrize'
GO
