USE [NewBuying]
GO
/****** Object:  Table [dbo].[StaffStatus]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StaffStatus](
	[UserID] [int] NOT NULL,
	[UserName] [varchar](50) NULL,
	[StaffTabletNo] [varchar](64) NULL,
	[TaxiBayType] [int] NULL,
	[TaxiBayCode] [varchar](64) NULL,
	[Status] [int] NULL,
	[LastUpdatedOn] [datetimeoffset](0) NULL,
 CONSTRAINT [PK_STAFFSTATUS] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[StaffStatus] ADD  DEFAULT ((0)) FOR [Status]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tablet/Device number of user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffStatus', @level2type=N'COLUMN',@level2name=N'StaffTabletNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What taxi bay did staff login to?
1: Urban
2: New Territories
3: Lantau Island
null: not involved' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffStatus', @level2type=N'COLUMN',@level2name=N'TaxiBayType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taxi bay ID staff checked into, 001-014' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffStatus', @level2type=N'COLUMN',@level2name=N'TaxiBayCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：还未登录过。  1： login。  2：logout。 3：锁定。 9：idle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffStatus', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后更新时间。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffStatus', @level2type=N'COLUMN',@level2name=N'LastUpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LPN Staff status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffStatus'
GO
