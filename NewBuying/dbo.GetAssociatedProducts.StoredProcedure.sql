USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetAssociatedProducts]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetAssociatedProducts]
  @Prodcode             varchar(64),       -- 货品编码  
  @LanguageAbbr         varchar(20)=''  
AS
/****************************************************************************
**  Name : GetAssociatedProducts
**  Version: 1.0.0.4
**  Description : 获得指定货品的相关货品信息
**
**  Parameter :	
  select * from Product_Style	
  select * from Product_Associated			
  declare @a int, @MessageID int
  exec @a = GetAssociatedProducts '8806358581800', 'zh_CN'
  print @a									  
**  Created by: Gavin @2012-05-23
**  Modify by: Gavin @2014-01-15 (var 1.0.0.1) 增加语言输入，增加返回关联货品的品牌	
**  Modify by: Gavin @2014-02-18 (var 1.0.0.2) 修改返回字段AssociatedProdName的内容， 改成product的 productname，根据语言返回
**  Modify by: Gavin @2016-04-21 (var 1.0.0.3) (for Bauhaus) 增加返回价格
**  Modify by: Gavin @2016-05-16 (var 1.0.0.4) (for Bauhaus) 增加返回ProdCodeStyle
**			
****************************************************************************/
begin
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1

   select A.KeyID, A.ProdCode, A.SeqNo, A.AssociatedProdCode,  
     case @Language when 2 then P.ProdName2 when 3 then P.ProdName3 else P.ProdName1 end as AssociatedProdName, 
     A.AssociatedProdFile, A.Note,
     case @Language when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName,
	 R.DefaultPrice, R.NetPrice, R.ProdPriceType, C.ps as ProdCodeStyle
    from Product_Associated A 
     left join Product P on A.AssociatedProdCode = P.ProdCode
     left join product_brand B on P.ProductBrandID = B.ProductBrandID
	 left join (select max(prodcodestyle) as ps, prodcode from Product_Style group by prodcode) C on A.AssociatedProdCode = C.ProdCode
	 left join (SELECT ProdCode, ProdPriceType, NetPrice, DefaultPrice FROM Product_Price 
	              where KeyID in  (SELECT  MAX(KeyID) AS KeyID FROM Product_Price WHERE (StartDate <= GETDATE()) AND (EndDate+1 >= GETDATE()) group by ProdCode)) R on P.ProdCode = R.ProdCode    	  
    where A.ProdCode = @Prodcode
  return 0  
end

GO
