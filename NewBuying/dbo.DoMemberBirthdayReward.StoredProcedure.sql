USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoMemberBirthdayReward]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoMemberBirthdayReward]
AS
/****************************************************************************
**  Name : DoMemberBirthdayReward
**  Version: 1.0.0.0
**  Description : 会员生日奖励 (EOD时执行)
    exec DoMemberBirthdayReward
** 
**  Created by:  Gavin @2014-08-25
****************************************************************************/
begin
  declare @CardNumber varchar(64), @BirthDay varchar(64)
  set @BirthDay = CONVERT(varchar(10), GETDATE(), 120)
  
  DECLARE CUR_DoMemberBirthdayReward CURSOR fast_forward FOR
    select CardNumber from Card C left join Member M on C.MemberID = M.MemberID
      where DATEPART(MM, M.MemberDateOfBirth) = DATEPART(MM, getdate()) and DATEPART(DD, M.MemberDateOfBirth) = DATEPART(DD, getdate())   
  OPEN CUR_DoMemberBirthdayReward
  FETCH FROM CUR_DoMemberBirthdayReward INTO @CardNumber 
  WHILE @@FETCH_STATUS=0
  BEGIN
    exec DoSVAReward @CardNumber, @BirthDay, 4
   
    FETCH FROM CUR_DoMemberBirthdayReward INTO @CardNumber 
  END
  CLOSE CUR_DoMemberBirthdayReward 
  DEALLOCATE CUR_DoMemberBirthdayReward      
end

GO
