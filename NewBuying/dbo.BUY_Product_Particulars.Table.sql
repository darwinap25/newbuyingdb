USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_Product_Particulars]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_Product_Particulars](
	[ProdCode] [varchar](64) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[ProdFunction] [nvarchar](max) NULL,
	[ProdIngredients] [nvarchar](max) NULL,
	[ProdInstructions] [nvarchar](max) NULL,
	[PackDesc] [nvarchar](512) NULL,
	[PackUnit] [nvarchar](512) NULL,
	[Memo1] [nvarchar](max) NULL,
	[Memo2] [nvarchar](max) NULL,
	[Memo3] [nvarchar](max) NULL,
	[Memo4] [nvarchar](max) NULL,
	[Memo5] [nvarchar](max) NULL,
	[Memo6] [nvarchar](max) NULL,
	[MemoTitle1] [nvarchar](512) NULL,
	[MemoTitle2] [nvarchar](512) NULL,
	[MemoTitle3] [nvarchar](512) NULL,
	[MemoTitle4] [nvarchar](512) NULL,
	[MemoTitle5] [nvarchar](512) NULL,
	[MemoTitle6] [nvarchar](512) NULL,
	[ProductPriority] [int] NULL,
 CONSTRAINT [PK_BUY_PRODUCT_PARTICULARS] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC,
	[LanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_Product_Particulars] ADD  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [dbo].[BUY_Product_Particulars] ADD  DEFAULT ((0)) FOR [ProductPriority]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LanguageMap主键ID. 默认1，LanguageMap中第一条记录，母语' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'LanguageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产品功能描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'ProdFunction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产品成分描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'ProdIngredients'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产品使用说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'ProdInstructions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售包装描述  （eg. 100ml/瓶）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'PackDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售包装单位 （eg. ml）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'PackUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'Memo1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'Memo2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'Memo3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'Memo4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'Memo5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'Memo6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性标题1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'MemoTitle1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性标题2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'MemoTitle2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性标题3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'MemoTitle3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性标题4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'MemoTitle4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性标题5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'MemoTitle5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性标题6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'MemoTitle6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品显示排序优先级。（默认0， 从0开始依次升高）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars', @level2type=N'COLUMN',@level2name=N'ProductPriority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品细节描述。（分语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Product_Particulars'
GO
