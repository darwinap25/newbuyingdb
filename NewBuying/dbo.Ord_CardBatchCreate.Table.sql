USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardBatchCreate]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardBatchCreate](
	[CardCreateNumber] [varchar](64) NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[CardCount] [int] NOT NULL,
	[Note] [nvarchar](512) NULL,
	[IssuedDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[InitAmount] [money] NULL DEFAULT ((0)),
	[InitPoints] [int] NULL DEFAULT ((0)),
	[RandomPWD] [int] NULL DEFAULT ((0)),
	[InitPassword] [varchar](512) NULL,
	[BatchCardID] [int] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NOT NULL DEFAULT ('P'),
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
 CONSTRAINT [PK_Ord_CardBatchCreate] PRIMARY KEY CLUSTERED 
(
	[CardCreateNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_CardBatchCreate]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CardBatchCreate] ON [dbo].[Ord_CardBatchCreate]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CardBatchCreate
* Version: 1.0.0.0
* Description : 批量创建卡
*     
*  
* Create By Gavin @2012-05-18
*/
/*==============================================================*/
BEGIN
  declare @CardCreateNumber varchar(512), @CardGradeID int, @CardCount int, @IssuedDate datetime, @ExpiryDate datetime, @InitAmount money, @InitPoints int, @ApproveStatus char(1), @CreatedBy int, @RandomPWD int, @InitPassword varchar(512)
  declare @OldApproveStatus char(1), @ReturnBatchID int, @ReturnStartNumber varchar(512), @ReturnEndNumber varchar(512)
  declare @ApprovalCode char(6)
  
  DECLARE CUR_CardBatchCreate CURSOR fast_forward local FOR
    SELECT CardCreateNumber, CardGradeID, CardCount, IssuedDate, ExpiryDate, InitAmount, InitPoints, ApproveStatus, ApproveBy, RandomPWD, InitPassword
     FROM INSERTED
  OPEN CUR_CardBatchCreate
  FETCH FROM CUR_CardBatchCreate INTO @CardCreateNumber, @CardGradeID, @CardCount, @IssuedDate, @ExpiryDate, @InitAmount, @InitPoints, @ApproveStatus, @CreatedBy, @RandomPWD, @InitPassword
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CardCreateNumber = @CardCreateNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus) 
    begin
      exec GenApprovalCode @ApprovalCode output
      exec BatchGenerateNumber @CreatedBy, 0, null, @CardGradeID, null, @CardCount, @IssuedDate, @InitAmount, @InitPoints, @RandomPWD, @InitPassword, @ExpiryDate,@CardCreateNumber,@ApprovalCode, @Createdby, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output
      update Ord_CardBatchCreate set BatchCardID = @ReturnBatchID, ApprovalCode = @ApprovalCode where CardCreateNumber = @CardCreateNumber 
    end  
    FETCH FROM CUR_CardBatchCreate INTO @CardCreateNumber, @CardGradeID, @CardCount, @IssuedDate, @ExpiryDate, @InitAmount, @InitPoints, @ApproveStatus, @CreatedBy, @RandomPWD, @InitPassword
  END
  CLOSE CUR_CardBatchCreate 
  DEALLOCATE CUR_CardBatchCreate   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建卡单号码， 主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'CardCreateNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'CardCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'IssuedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'ExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'InitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'InitPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否初始随机密码。0：不是。1:是的。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'RandomPWD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始密码。RandomPWD=1时，不需要填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'InitPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'BatchCardID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡批量创建表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardBatchCreate'
GO
