USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckGenerateNumber]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[CheckGenerateNumber]
  @Type                 int,             --类型：0：Card。 1：coupon
  @TypeID               int,             --类型ID。 如果是card，则是CardgradeID，如果是coupon，则是coupontypeid
  @GenQty    			int,			 --创建的数量。
  @ReturnStartNumber    varchar(64) output, --创建的号码的起始号码
  @ReturnEndNumber      varchar(64) output,  --创建的号码的结束号码
  @ReturnDuplicateNumber varchar(64) output,  --重复的首号码。（如果有重复号码。没有的话返回空）
  @RemainQty			bigint output,			 -- 按照NumberMask设置，剩余可创建数量。
  @LastNumber			varchar(64) output   -- 当前数据库中最大序号的number
AS
/****************************************************************************
**  Name : CheckGenerateNumber
**  Version: 1.0.0.4
**  Description : 根据产生号码的逻辑， 计算将要产生的号码， 检查是否会产生号码重复。（只按照当前的数据情况，未批核的创建单不考虑）
**  Return: 0: OK.   -1:已经存在号码。 -2：数量超出Mask范围
**  Parameter :
  declare @a int  , @ReturnStartNumber varchar(64), @ReturnEndNumber varchar(64), @ReturnDuplicateNumber varchar(64)
  ,@RemainQty bigint,   @LastNumber			varchar(64)
  exec @a = CheckGenerateNumber 1, 36, 0, @ReturnStartNumber output, @ReturnEndNumber output, @ReturnDuplicateNumber output
  ,@RemainQty output,   @LastNumber	output
  print @a  
  print @ReturnStartNumber
  print @ReturnEndNumber
  print @ReturnDuplicateNumber
  print @RemainQty
  print @LastNumber
   select * from cardgrade
  select * from coupontype
  select * from coupon where coupontypeid = 22
   select * from card where cardgradeID = 8
   select * from card where cardnumber >= '1001' and cardnumber <= '1012' and len(cardnumber) = 4
**  Created by: Gavin @2012-08-16
**  Modify by Gavin @2012-08-29 (ver 1.0.0.1) 修正没有产生过号码的情况下，取剩余数不正确的问题。
**  Modify by Gavin @2012-09-18 (ver 1.0.0.2) 返回参数@RemainQty由 int 改成 bigint，扩大数值范围到（-2^63 ~ 2^63-1）
**  Modify by Gavin @2012-11-13 (ver 1.0.0.3) 修改CalcCheckDigit_EAN13 为CalcCheckDigit。
**  Modify by: Gavin @2012-11-29 (ver 1.0.0.4) Cardtype，CardGrade的卡号产生规则变动： 如果填写了CardType中的卡号规则，下属CardGrade中的卡号规则将是相同的，不得变动。如果没有填写CardType中的卡号规则，下属CardGrade中的卡号规则必须填写，而且不能是相同的。
**
****************************************************************************/
begin
  declare @NewBatchID int, @NewBatchCode varchar(30), @MaxID bigint, @TempSeqNo bigint, @i int
  declare @NumberHead varchar(100), @NumberSeqNo varchar(100), @NumberSeqNoLength int
  declare @CardNumMask nvarchar(30), @CardNumPattern nvarchar(30), @PatternLen int, @MaxSeqNo bigint
  declare @CouponNumMask nvarchar(30), @CouponNumPattern nvarchar(30), @CouponPatternLen int, @MaxCouponSeqNo bigint       
  declare @TranCount int, @MaxCouponNumber varchar(64), @IsImportCouponNumber int, @Checkdigit int
  declare @BusDate date --Business Date
  declare @TxnDate datetime --Transaction Date
  declare @NewNumber varchar(64)
  declare @IsPhysicalCard int, @CouponUID varchar(512), @CardTypeID int, @CardGradeID int, @CouponTypeID int
  declare @NumberToUID int, @EndSeqNoLen int, @MaxSeqNoStr varchar(30)
  declare @ModeID int, @ModeCode varchar(64), @IsCardTypeNumMask int
  
  set @ReturnDuplicateNumber = ''
  if @Type = 0
  begin    
    select @CardGradeID = CardGradeID, @CardTypeID = CardTypeID, @CardNumMask = CardNumMask, @CardNumPattern = CardNumPattern, @Checkdigit = CardCheckdigit, @NumberToUID = CardNumberToUID, @ModeID = CheckDigitModeID from CardGrade where CardGradeID = @TypeID     
--    if isnull(@CardNumMask, '') = ''
--      select @CardNumMask = CardNumMask, @CardNumPattern = CardNumPattern, @CardTypeID = CardTypeID, @Checkdigit = CardCheckdigit, @NumberToUID = CardNumberToUID, @ModeID = CheckDigitModeID from CardType where CardTypeID = @CardTypeID    
      
    select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
    set @ModeCode = isnull(@ModeCode, 'EAN13')
            
    set @PatternLen = 0
    while @PatternLen < Len(@CardNumMask)
    begin      
      if substring(@CardNumMask, @PatternLen + 1, 1) = 'A'
        set @PatternLen = @PatternLen + 1
      else  
        break  
    end 
    
    select @LastNumber = isnull(max(CardNumber),'0000000000000000000000000000000000000000000000000000000000000000') from Card where len(CardNumber) = len(@CardNumMask) and (CardNumber like (@CardNumPattern + '%') or isnull(@CardNumPattern, '') = '')
    
    if @Checkdigit = 1 
      set @MaxSeqNoStr = substring(@LastNumber, @PatternLen + 1,  Len(@CardNumMask) - @PatternLen - 1) 
    else  
      set @MaxSeqNoStr = substring(@LastNumber, @PatternLen + 1,  Len(@CardNumMask) - @PatternLen)
    set @MaxSeqNo=Convert(bigint, @MaxSeqNoStr)       
    set @MaxSeqNo = isnull(@MaxSeqNo, 0) + 1
    set @TempSeqNo = @MaxSeqNo
    set @RemainQty = cast(Left('9999999999999999999999999999999999999999999999999999999999999999', len(@MaxSeqNoStr)) as bigint) - @MaxSeqNo + 1

    if @Checkdigit = 1
    begin
	  select @ReturnStartNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo), Len(@CardNumMask) - @PatternLen - 1)
      select @ReturnEndNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo + @GenQty - 1), Len(@CardNumMask) - @PatternLen - 1)        
      select @ReturnStartNumber = dbo.CalcCheckDigit(@ReturnStartNumber, @ModeCode)
      select @ReturnEndNumber = dbo.CalcCheckDigit(@ReturnEndNumber, @ModeCode)
    end else
    begin
	  select @ReturnStartNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo), Len(@CardNumMask) - @PatternLen)
      select @ReturnEndNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo + @GenQty - 1), Len(@CardNumMask) - @PatternLen)    
    end 
    
    --因为包含校验位，所以产生的序号长度要加上1 位（校验位）， 然后比较number长度。
    if @Checkdigit = 1
      set @EndSeqNoLen = len(Rtrim(cast((@MaxSeqNo + @GenQty - 1) as varchar(64)))) + 1
    else  set @EndSeqNoLen = len(Rtrim(cast((@MaxSeqNo + @GenQty - 1) as varchar(64))))

    if @LastNumber='0000000000000000000000000000000000000000000000000000000000000000' Set @LastNumber=''        
    if (@PatternLen + @EndSeqNoLen) <= len(@CardNumMask)
    begin
      select top 1 @ReturnDuplicateNumber = CardNumber from Card where CardNumber >= @ReturnStartNumber and CardNumber <= @ReturnEndNumber and len(CardNumber) = Len(@CardNumMask) order by CardNumber
      if isnull(@ReturnDuplicateNumber, '') <> ''
        return -1
      else
        return 0         
    end else
      return -2    
  end else 
  begin
    select @CouponNumMask = CouponNumMask, @CouponNumPattern = CouponNumPattern, @CouponTypeID = CouponTypeID, @IsImportCouponNumber = IsImportCouponNumber,
        @Checkdigit = CouponCheckdigit, @NumberToUID = CouponNumberToUID, @ModeID = CheckDigitModeID 
      from CouponType where CouponTypeID = @TypeID
    select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
    set @ModeCode = isnull(@ModeCode, 'EAN13')
          
    set @CouponPatternLen = 0
    while @CouponPatternLen < Len(@CouponNumMask)
    begin      
        if substring(@CouponNumMask, @CouponPatternLen + 1, 1) = 'A'
          set @CouponPatternLen = @CouponPatternLen + 1
        else  
          break  
    end

    -- 考虑到导入的CouponNumber问题, 自增长的当前最大号码需要根据CouponNumPattern获得    
    select @LastNumber = isNull(max(CouponNumber),'0000000000000000000000000000000000000000000000000000000000000000') from Coupon where CouponTypeID = @CouponTypeID and len(CouponNumber) = len(@CouponNumMask) and (CouponNumber like (@CouponNumPattern + '%') or isnull(@CouponNumPattern,'') = '') 
    if @Checkdigit = 1 
      set @MaxSeqNoStr = substring(@LastNumber, @CouponPatternLen + 1,  Len(@CouponNumMask) - @CouponPatternLen - 1) 
    else  
      set @MaxSeqNoStr = substring(@LastNumber, @CouponPatternLen + 1,  Len(@CouponNumMask) - @CouponPatternLen) 
    set @MaxCouponSeqNo = Convert(bigint, @MaxSeqNoStr)   
    select @MaxCouponSeqNo = isnull(@MaxCouponSeqNo, 0) + 1
    set @TempSeqNo = @MaxCouponSeqNo
    set @RemainQty = cast(Left('9999999999999999999999999999999999999999999999999999999999999999', len(@MaxSeqNoStr)) as bigint) - @MaxCouponSeqNo + 1

    if @Checkdigit = 1
    begin
      select @ReturnStartNumber = substring(@CouponNumPattern, 1, @CouponPatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @MaxCouponSeqNo), Len(@CouponNumMask) - @CouponPatternLen - 1)      
      select @ReturnStartNumber = dbo.CalcCheckDigit(@ReturnStartNumber, @ModeCode)
      select @ReturnEndNumber = dbo.CalcCheckDigit(@ReturnEndNumber, @ModeCode)
    end else
    begin
      select @ReturnStartNumber = substring(@CouponNumPattern, 1, @CouponPatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @MaxCouponSeqNo), Len(@CouponNumMask) - @CouponPatternLen)
      select @ReturnEndNumber = substring(@CouponNumPattern, 1, @CouponPatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + Convert(varchar(30), @MaxCouponSeqNo + @GenQty - 1), Len(@CouponNumMask) - @CouponPatternLen)        
    end
    
    --因为包含校验位，所以产生的序号长度要加上1 位（校验位）， 然后比较number长度。
    if @Checkdigit = 1
      set @EndSeqNoLen = len(Rtrim(cast((@MaxCouponSeqNo + @GenQty - 1) as varchar(64)))) + 1
    else  set @EndSeqNoLen = len(Rtrim(cast((@MaxCouponSeqNo + @GenQty - 1) as varchar(64))))
    
    
    if @LastNumber='0000000000000000000000000000000000000000000000000000000000000000' Set @LastNumber=''
    if (@CouponPatternLen + @EndSeqNoLen) <= len(@CouponNumMask)
    begin
      select top 1 @ReturnDuplicateNumber = CouponNumber from Coupon where CouponNumber >= @ReturnStartNumber and CouponNumber <= @ReturnEndNumber and len(CouponNumber) = Len(@CouponNumMask) order by CouponNumber
      if isnull(@ReturnDuplicateNumber, '') <> ''
        return -1
      else
        return 0         
    end else
      return -2
  end    
end

GO
