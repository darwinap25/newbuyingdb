USE [NewBuying]
GO
/****** Object:  Table [dbo].[BrandDeliveryAddressMap]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BrandDeliveryAddressMap](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[BrandID] [int] NOT NULL,
	[CountryCode] [varchar](64) NOT NULL,
 CONSTRAINT [PK_BRANDDELIVERYADDRESSMAP] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BrandDeliveryAddressMap', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BrandDeliveryAddressMap', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌和送货地址的绑定表 （for Bauhaus）
@2016-02-23   销售的货品品牌 和 送货地址 相关。 某些品牌 的货品 不能 送到某些地址。 
这里绑定 BrandID 和 CountryCode，  触发器产生 此品牌下的 货品 和 countrycode 的对应。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BrandDeliveryAddressMap'
GO
