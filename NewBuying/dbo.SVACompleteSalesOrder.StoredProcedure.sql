USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SVACompleteSalesOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SVACompleteSalesOrder]
  @UserID				         int,		        -- 操作员ID
  @TxnNo                         VARCHAR(64),       -- 交易号码
  @ApprovalCode                  VARCHAR(64) output --返回的approvalCode
AS
/****************************************************************************
**  Name : SVACompleteSalesOrder 
**  Version: 1.0.0.1
**  Description : 完成交易单。  （此版本用于bauhaus，使用的LINKSERVER DB是LINKBUY.Buying_621.Test）
**  Created by: Gavin @2016-05-05   
**  Modify by: Gavin @2017-03-22 (ver 1.0.0.1) 完成 SVA Sales 交易时，增加判断Buying Sales 的状态。 如果还没有出库完成，则不能完成。   
****************************************************************************/
BEGIN
	  RETURN 0
END

GO
