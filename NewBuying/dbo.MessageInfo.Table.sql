USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessageInfo]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageInfo](
	[MessageInfoID] [bigint] IDENTITY(1,1) NOT NULL,
	[MemssageSubject] [nvarchar](512) NULL,
	[MemssageContent] [nvarchar](max) NULL,
 CONSTRAINT [PK_MESSAGEINFO] PRIMARY KEY CLUSTERED 
(
	[MessageInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageInfo', @level2type=N'COLUMN',@level2name=N'MessageInfoID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息主题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageInfo', @level2type=N'COLUMN',@level2name=N'MemssageSubject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageInfo', @level2type=N'COLUMN',@level2name=N'MemssageContent'
GO
