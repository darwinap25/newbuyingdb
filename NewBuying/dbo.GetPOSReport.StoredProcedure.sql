USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetPOSReport]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPOSReport]
  @StoreCode          VARCHAR(64),     -- StoreCode
  @RegisterCode       VARCHAR(64),     -- RegisterCode
  @BusDate            DATETIME,        -- 交易日
  @ReportID           INT              -- 报表ID
AS
/****************************************************************************
**  Name : GetPOSReport
**  Version : 1.0.0.0
**  Description : 获取报表内容
**
declare @a int
exec @a=GetPOSReport '1','R01','2015-03-25',1
print @a

**  Created by Gavin @2015-03-26
****************************************************************************/
BEGIN
  DECLARE @TenderDesc VARCHAR(64), @TenderType INT, @COUNT INT, @LocalAmount MONEY, @LINESUM MONEY
  DECLARE @LineLen INT, @LineStr VARCHAR(100), @SPACESTR VARCHAR(50)
  DECLARE @Report TABLE(KeyID INT IDENTITY(1,1), LineStr VARCHAR(100))  
  
  SELECT * FROM POSDAYREPORT 
    WHERE StoreCode = @StoreCode AND RegisterCode = @RegisterCode AND Busdate = @BusDate AND ReportID = @ReportID
  
  RETURN 0
END

GO
