USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewProductSize_IsOnlineSKU]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewProductSize_IsOnlineSKU]
AS
/*
*/
	select * from Product_Size 
	where ProductSizeCode in	(select distinct ProductSizeCode from product where IsOnlineSKU = 1)

GO
