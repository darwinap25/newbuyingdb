USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGrade]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardGrade](
	[CardGradeID] [int] IDENTITY(1,1) NOT NULL,
	[CardGradeCode] [varchar](64) NOT NULL,
	[CardTypeID] [int] NOT NULL,
	[CardGradeName1] [nvarchar](512) NULL,
	[CardGradeName2] [nvarchar](512) NULL,
	[CardGradeName3] [nvarchar](512) NULL,
	[CardNumMask] [nvarchar](512) NOT NULL,
	[CardNumPattern] [nvarchar](512) NOT NULL,
	[CardCheckdigit] [int] NULL DEFAULT ((0)),
	[CheckDigitModeID] [int] NULL,
	[CardNumberToUID] [int] NULL DEFAULT ((0)),
	[IsImportUIDNumber] [int] NULL DEFAULT ((0)),
	[IsConsecutiveUID] [int] NULL DEFAULT ((1)),
	[UIDCheckDigit] [int] NULL DEFAULT ((0)),
	[UIDToCardNumber] [int] NULL DEFAULT ((0)),
	[CardGradeRank] [int] NOT NULL DEFAULT ((0)),
	[CardGradeUpdMethod] [int] NULL DEFAULT ((0)),
	[CardGradeUpdThreshold] [int] NULL,
	[CardGradeUpdCouponTypeID] [int] NULL,
	[CardGradeDiscCeiling] [decimal](16, 6) NULL,
	[CardGradeMaxAmount] [money] NULL DEFAULT ((0)),
	[CardGradeMaxPoint] [int] NULL DEFAULT ((0)),
	[CardTypeInitPoints] [int] NULL DEFAULT ((0)),
	[CardTypeInitAmount] [money] NULL DEFAULT ((0)),
	[CardConsumeBasePoint] [int] NULL DEFAULT ((0)),
	[CardPointToAmountRate] [decimal](16, 6) NULL DEFAULT ((0)),
	[CardAmountToPointRate] [decimal](16, 6) NULL DEFAULT ((0)),
	[ForfeitAmountAfterExpired] [int] NULL DEFAULT ((0)),
	[ForfeitPointAfterExpired] [int] NULL DEFAULT ((0)),
	[IsAllowStoreValue] [int] NULL DEFAULT ((1)),
	[IsAllowConsumptionPoint] [int] NULL DEFAULT ((1)),
	[CardPointTransfer] [int] NOT NULL DEFAULT ((0)),
	[CardAmountTransfer] [int] NULL DEFAULT ((0)),
	[CardValidityDuration] [int] NULL,
	[CardValidityUnit] [int] NULL DEFAULT ((0)),
	[CardGradeLayoutFile] [nvarchar](512) NULL,
	[CardGradePicFile] [nvarchar](512) NULL,
	[CardGradeStatementFile] [nvarchar](512) NULL,
	[CardGradeNotes] [nvarchar](max) NULL,
	[MinAmountPreAdd] [money] NULL,
	[MaxAmountPreAdd] [money] NULL,
	[MinPointPreAdd] [int] NULL DEFAULT ((0)),
	[MaxPointPreAdd] [int] NULL DEFAULT ((0)),
	[ActiveResetExpiryDate] [int] NULL DEFAULT ((0)),
	[MinAmountPreTransfer] [money] NULL,
	[MaxAmountPreTransfer] [money] NULL,
	[MinPointPreTransfer] [int] NULL,
	[MaxPointPreTransfer] [int] NULL,
	[DayMaxAmountTransfer] [money] NULL,
	[DayMaxPointTransfer] [int] NULL,
	[PasswordRuleID] [int] NULL,
	[CampaignID] [int] NULL,
	[MinBalanceAmount] [money] NULL,
	[MinBalancePoint] [int] NULL,
	[MinConsumeAmount] [money] NULL,
	[GracePeriodValue] [int] NULL,
	[GracePeriodUnit] [int] NULL,
	[CardGradeUpdHitPLU] [varchar](64) NULL,
	[HoldCouponCount] [int] NULL,
	[ServiceCharge] [decimal](16, 6) NULL DEFAULT ((0)),
	[NumberOfCouponDisplay] [int] NULL,
	[NumberOfNewsDisplay] [int] NULL,
	[NumberOfTransDisplay] [int] NULL,
	[MobileProtectPeriodValue] [int] NULL,
	[EmailValidatedPeriodValue] [int] NULL,
	[NonValidatedPeriodValue] [int] NULL,
	[SessionTimeoutValue] [int] NULL,
	[LoginFailureCount] [int] NULL,
	[QRCodePeriodValue] [int] NULL,
	[AllowOfflineQRCode] [int] NULL DEFAULT ((1)),
	[QRCodePrefix] [varchar](64) NULL,
	[TrainingMode] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CardGrade] PRIMARY KEY CLUSTERED 
(
	[CardGradeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级所属卡种类' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号码规则' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardNumMask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'按照卡号码规则的初始值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardNumPattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号是否包含校验位。默认0。 0：没有。1：有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardCheckdigit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验位产生逻辑表的外键（预留）。 CardCheckdigit设置为1时生效。 
CheckDigitMode 表 ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CheckDigitModeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建CardNumber时，是否同步产生UID。 默认：0。 
0：不产生UID。
1：原样复制CardNumber到 UID
2：复制CardNumber到 UID，并删除最后一位。    （不考虑CardNumber本身是否已经有了checkdigit）
3：复制CardNumber到 UID，并增加checkdigit，加在最后一位。 （不考虑CardNumber本身没有checkdigit的情况）
注： CD：checkdigit位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardNumberToUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardNumber号码是否需要导入。0：SVA系统根据CardNumMask设置来产生。1：号码导入，不是自动产生。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'IsImportUIDNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardUID号码是否为连续的。 0：不连续。1：连续，默认1   
（注：IsImportUIDNumber=1时有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'IsConsecutiveUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UID是否含有checkdigit。0：没有。1：有。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'UIDCheckDigit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据UID号码产生Cardnumber 控制。默认0： 不根据UID产生。
0：UID  binding  Cardnumber
1：原样复制UID到Cardnumber
2：复制UID到Cardnumber，并删除最后一位。    （不考虑UID本身是否已经有了checkdigit）
3：复制UID到Cardnumber，并增加checkdigit，加在最后一位。 （不考虑UID本身没有checkdigit的情况）
注： CD：checkdigit位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'UIDToCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'等级次序。（升序）
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeRank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级升级规则：
Null/0:   表示这个Grade不参与升级， 即升级过程不会升级到此级别。
1：每年消费金额到指定值。 
2：每年获取积分到指定值。
3：单笔消费金额到达指定值。
4：获得CardGradeUpdCouponTypeID指定的Coupon数量达到指定值。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeUpdMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'升级界限值，根据CardGradeUpdMethod决定值含义
达到这个指定值后，进入这个grade。 （即，这个值是进入此级别的最低值）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeUpdThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡升级条件：指定的CouponTypeID。 数量看CardGradeUpdThreshold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeUpdCouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'等级折扣上限值。记录为扣减掉的折扣。
例如：上限8折，记录为 0.2。  Null/0：没有折扣。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeDiscCeiling'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大存储金额。 默认0
Null/0： 不设上限。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeMaxAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存储的最大积分值。 默认0， 表示不设限制。（如果超过此值，将不再增加积分，客户端自动产生和购买积分时可能提出警告，但不会阻止交易）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeMaxPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡创建时的初始积分。默认0。 
（卡创建时，默认按照此设置，但可在创建时更改）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardTypeInitPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡创建时的初始金额。默认0。 
（卡创建时，默认按照此设置，但可在创建时更改）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardTypeInitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费积分的最低值。默认0
Null/0: 不限最低值。  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardConsumeBasePoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡积分兑换金额设置值（例如，设置为10，表示10个积分兑换1元，10为最低兑换值）。默认0 
Null/0: 不允许兑换。 
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardPointToAmountRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡金额兑换为积分（例如：设置为10， 表示10元兑换1个积分，10元为最低兑换值）。默认0
Null/0：不允许兑换。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardAmountToPointRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡过期时金额是否清0。 默认0. （取代以前的ForfeitAfterExpired）
0： 不清零。 1：清零' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'ForfeitAmountAfterExpired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡过期时积分是否清0。 默认0. （取代以前的ForfeitAfterExpired）
0： 不清零。 1：清零' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'ForfeitPointAfterExpired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许储值。
0: 不允许储值（即不允许增值）。 1：允许。  默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'IsAllowStoreValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许消费积分。0：不允许。1：允许。 如果允许，则按照积分规则计算积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'IsAllowConsumptionPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许转移积分，默认0
0：不允許；
1：允許同卡级别
2：同卡類型轉贈；
3：允許同品牌轉贈；
4：允許同发行商轉贈；
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardPointTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许转赠金额。默认0.
0：不允許；
1：允許同卡级别
2：同卡類型轉贈；
3：允許同品牌轉贈；
4：允許同发行商轉贈；' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardAmountTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡有效期持续时间长度。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardValidityDuration'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡有效期持续时间长度 的单位：默认0，不需要检查CardValidityDuration值
0：永久。 1：年。 2：月。 3：星期。 4：天。5: 有效期不变更。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardValidityUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可存放卡封面的设计模板，图片文件的相对路径名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeLayoutFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可存放卡的图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradePicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别结算单模板文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeStatementFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条例说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeNotes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最小增值金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MinAmountPreAdd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最大增值金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MaxAmountPreAdd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小购买积分。 默认0， 表示不设限制。 只对购买积分生效，达到下限则取消操作，需要用户重新输入。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MinPointPreAdd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大购买积分。  默认0， 表示不设限制。只对购买积分生效，达到上限则取消操作，需要用户重新输入。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MaxPointPreAdd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'激活时，是否重置有效期。
0：不重置。 1：重置。  默认0；' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'ActiveResetExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最小转赠金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MinAmountPreTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最大转赠金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MaxAmountPreTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最小转赠积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MinPointPreTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔最大转赠积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MaxPointPreTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每天最大转赠金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'DayMaxAmountTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每天最大转赠积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'DayMaxPointTransfer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码规则表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'PasswordRuleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类似store group 的 附加字段。 作为coupontype分类使用。
(由于Campaign表中加了brandID， 所以要求前台输入时保证campaign中的brandid和 coupontype中brandid一致)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小剩余金额。
例如：如果 最小剩余金额 为 10，  当前卡余额为 100.   用户查询余额发现还有 100，  但是去 消费 95元 时， 就报告余额不足' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MinBalanceAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小剩余积分。 同最小剩余金额逻辑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MinBalancePoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小消费金额（每次）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MinConsumeAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡有效期的宽限期。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'GracePeriodValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡有效期的宽限期的 单位值。
0：永久。 1：年。 2：月。 3：星期。 4：天。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'GracePeriodUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户购买此货品后，卡可以升到此级别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'CardGradeUpdHitPLU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许持有的coupon总数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'HoldCouponCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务费。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'ServiceCharge'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'显示拥有的Coupon的最大数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'NumberOfCouponDisplay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'显示新闻的最大数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'NumberOfNewsDisplay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'显示交易的最大数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'NumberOfTransDisplay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码保护时间。 单位：月。 （超过时间后，其他用户可以使用这个手机号码，使用后，其他使用此号码的Member，手机号码变成未验证）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'MobileProtectPeriodValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email的未验证期限时间。 单位：小时。（超过时间后，UI会弹出要求用户重新输入 Email账号的窗口） ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'EmailValidatedPeriodValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户首次登录前保留时间。单位：小时。（超过时间会员记录删除）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'NonValidatedPeriodValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Session的超时时间。单位：分钟。（' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'SessionTimeoutValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许的连续登录失败次数。（密码错误造成的失败）。 null 或 0 表示不加限制。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'LoginFailureCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QRCode的有效时间长度。单位：分钟。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'QRCodePeriodValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许Offline的QRCode登录。0：不允许。1：允许.默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'AllowOfflineQRCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QRCode的 Prefix。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'QRCodePrefix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'training mode. 默认0.  1：yes. 0:No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade', @level2type=N'COLUMN',@level2name=N'TrainingMode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级表
按照L2要求，有关卡号规则的设置字段，CardType和CardGrade中设置双份。字段包括（CardNumMask，CardNumPattern，CardCheckdigit，CheckDigitModeID，IsImportUIDNumber，IsConsecutiveUID，UIDToCardNumber）
@2015-10-29：  CardGradeUpdMethod 为0 时， 表示这个Grade不参与升级， 即升级过程不会升级到此级别。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGrade'
GO
