USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSalesDByTxnNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[GetSalesDByTxnNo]
  @TxnNo              varchar(512)           -- 交易号
AS
/****************************************************************************
**  Name : GetSalesDByTxnNo
**  Version: 1.0.0.4
**  Description : 根据交易号获得此交易的sales_D 数据。
**
**  Parameter :
  exec GetSalesDByTxnNo 'KTXNNO000001010'
**  Created by: Gavin @2012-10-11
**  Modified by: Gavin @2012-10-22 (ver 1.0.0.1) 增加返回additional 字段
**  Modified by: Gavin @2013-02-06 (ver 1.0.0.2) 增加返回ReservedDate, PickupDate 字段
**  Modified by: Gavin @2016-01-06 (ver 1.0.0.3) 增加返回字段 ProdType 
**  Modified by: Gavin @2017-08-24 (ver 1.0.0.4) 为了kiosk使用，把TransNum 返回字段名字改为TxnNo
**
****************************************************************************/
begin
  select D.TransNum as TxnNo, D.SeqNo, D.ProdCode, D.ProdDesc, D.SerialNo, D.Collected, D.UnitPrice, D.NetPrice, 
    D.Qty, D.NetAmount, D.POPrice, D.POReasonCode, D.DiscountPrice, D.DiscountAmount, 
	D.CreatedOn, D.CreatedBy, D.UpdatedOn, D.UpdatedBy, D.Additional1,
    D.ReservedDate, D.PickupDate, isnull(P.ProdType, 0) as ProdType
  from sales_D D left join Product P on D.ProdCode = P.ProdCode
  where TransNum = @TxnNo 
  order by SeqNo  
  return 0
end  

GO
