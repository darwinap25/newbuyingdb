USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSDepartmentReport]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[POSDepartmentReport]
  @StoreCode          VARCHAR(64),     -- StoreCode
  @RegisterCode       VARCHAR(64),     -- RegisterCode
  @BusDate            DATETIME,        -- 交易日
  @CashierID          INT              -- 收银员
AS
/****************************************************************************
**  Name : POSDepartmentReport
**  Version : 1.0.0.1
**  Description : POS 部门日报表
**
declare @a int
exec @a=POSDepartmentReport '1','R01','',1
print @a

select * from posstaff
select * from sales_h

**  Created by Gavin @2015-03-26
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
****************************************************************************/
BEGIN
  SET NOCOUNT ON
  DECLARE @Qty INT, @Amount MONEY, @DepartCode VARCHAR(64), @DepartDesc VARCHAR(512), @TransType INT
  DECLARE @LineLen INT, @LineStr VARCHAR(100), @SPACESTR VARCHAR(50)
  DECLARE @Report TABLE(KeyID INT IDENTITY(1,1), LineStr VARCHAR(100))  
  SET @LineLen = 100
  SET @SPACESTR = '                                                                 '
  DECLARE @TotalQty INT, @TotalAmt MONEY
  SET @TotalAmt = 0
  SET @TotalQty = 0
  SET @CashierID = ISNULL(@CashierID, 0)
  SET @BusDate = ISNULL(@BusDate, 0)
  IF @BusDate = 0
    SELECT TOP 1 @BusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode  ORDER BY BusDate desc
  
  SET @LineStr = '******************************************'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '               CONSOLIDATED'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '                 部门报表'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '******************************************'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = LEFT(RTRIM('店舖號:' + @StoreCode) + @SPACESTR, 24)
  SET @LineStr = @LineStr + RTRIM('交易日:' + CONVERT(VARCHAR(10), @BusDate, 120))
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr) 
   
  -- 数据开始
  SET @LineStr = LEFT('DEPT#' + @SPACESTR, 7) + LEFT('NAME' + @SPACESTR, 15) + LEFT('ITMES' + @SPACESTR, 5) + RIGHT(@SPACESTR + 'NET SALES', 15)
  INSERT INTO @Report  VALUES(@LineStr)   
  
  DECLARE CUR_POSDepartmentReport CURSOR fast_forward FOR
    SELECT A.DepartCode, MAX(B.DepartName1), SUM(TotalQty), SUM(NetAmount), A.TransType 
      FROM sales_D A LEFT JOIN BUY_DEPARTMENT B ON A.DepartCode = B.DepartCode
        LEFT JOIN Sales_H H ON A.TransNum = H.TransNum
      WHERE A.StoreCode = @StoreCode and A.RegisterCode = @RegisterCode and A.BusDate = @BusDate 
           AND (H.CashierID = @CashierID or @CashierID = 0)
    GROUP BY A.DepartCode, A.TransType
    ORDER BY A.DepartCode, A.TransType
  OPEN CUR_POSDepartmentReport
  FETCH FROM CUR_POSDepartmentReport INTO @DepartCode, @DepartDesc, @Qty, @Amount, @TransType
  WHILE @@FETCH_STATUS=0
  BEGIN
    IF @TransType = 0
    BEGIN
      SET @LineStr = LEFT(@DepartCode + @SPACESTR, 7) + LEFT(@DepartDesc + @SPACESTR, 15) + RIGHT(@SPACESTR + CAST(@Qty AS VARCHAR) + '/', 5) + RIGHT(@SPACESTR + 'RMB ' + CAST(@Amount AS VARCHAR), 15)
      INSERT INTO @Report  VALUES(@LineStr)  
    END
    IF @TransType = 4
    BEGIN
      SET @LineStr = ' ' + LEFT('VOIDS' + @SPACESTR, 21) + RIGHT(@SPACESTR + CAST(@Qty AS VARCHAR) + '/', 5) + RIGHT(@SPACESTR + 'RMB ' + CAST(@Amount AS VARCHAR), 15)
      INSERT INTO @Report  VALUES(@LineStr)  
    END
    IF @TransType = 5
    BEGIN
      SET @LineStr = ' ' + LEFT('REFUNDS' + @SPACESTR, 21) + RIGHT(@SPACESTR + CAST(@Qty AS VARCHAR) + '/', 5) + RIGHT(@SPACESTR + 'RMB ' + CAST(@Amount AS VARCHAR), 15)
      INSERT INTO @Report  VALUES(@LineStr)  
    END            
    SET @TotalQty = @TotalQty + ISNULL(@Qty, 0)
    SET @TotalAmt = @TotalAmt + ISNULL(@Amount, 0)
    FETCH FROM CUR_POSDepartmentReport INTO @DepartCode, @DepartDesc, @Qty, @Amount, @TransType  
  END
  CLOSE CUR_POSDepartmentReport 
  DEALLOCATE CUR_POSDepartmentReport   
  -- 数据结束
  
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '总计'
  SET @LineStr = @LineStr + RIGHT(@SPACESTR + CAST(@TotalQty AS VARCHAR), 22)
  SET @LineStr = @LineStr + '/' + RIGHT(@SPACESTR + 'RMB ' + CAST(@TotalAmt AS VARCHAR), 15)
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)    
  SET @LineStr = @SPACESTR
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = CONVERT(VARCHAR(19), GETDATE(), 120) + RIGHT(@SPACESTR + @StoreCode, 8) + RIGHT(@SPACESTR + @RegisterCode, 4)+ RIGHT(@SPACESTR + CAST(@CashierID as VARCHAR), 11)
  INSERT INTO @Report  VALUES(@LineStr) 
  SET @LineStr = @SPACESTR
  INSERT INTO @Report  VALUES(@LineStr)   
  SET @LineStr = '                  報告結束'
  INSERT INTO @Report  VALUES(@LineStr)                 
                    
  SELECT * FROM  @Report 
  
  SET NOCOUNT OFF      
  RETURN 0
END

GO
