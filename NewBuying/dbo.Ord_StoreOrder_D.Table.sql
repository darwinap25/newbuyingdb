USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_StoreOrder_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_StoreOrder_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[StoreOrderNumber] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[OrderQty] [int] NOT NULL,
	[StockTypeCode] [varchar](64) NULL,
 CONSTRAINT [PK_ORD_STOREORDER_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_StoreOrder_D] ADD  DEFAULT ('G') FOR [StockTypeCode]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StoreOrder_D', @level2type=N'COLUMN',@level2name=N'StoreOrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StoreOrder_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StoreOrder_D', @level2type=N'COLUMN',@level2name=N'OrderQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BUY_STOCKTYPE 表的 StockTypeCode。 （类似prodcode， 特殊意义表，使用code，不使用ID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StoreOrder_D', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺订货单。 子表
@20150925 增加stocktypecode，相关SP和UI都还没有修改。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_StoreOrder_D'
GO
