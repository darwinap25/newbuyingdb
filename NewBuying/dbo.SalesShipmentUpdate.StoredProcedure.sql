USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SalesShipmentUpdate]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SalesShipmentUpdate]
  @UserID				         INT,		        -- 操作员ID
  @TxnNo                         VARCHAR(64),       -- 交易号码
  @DeliveryNumber                VARCHAR(512),
  @LogisticsProviderID           INT
AS
/****************************************************************************
**  Name : SalesShipmentUpdate 
**  Version: 1.0.0.2
**  Description : 发货单发货后调用，更新SVA的Sales状态，以及发送通知消息  （此版本用于bauhaus，使用的LINKSERVER DB是LINKBUY.Buying_621.Test）
**  Created by: Gavin @2016-05-05   
**  Modify by: Gavin @2017-03-22 (ver 1.0.0.1) 完成 SVA Sales 交易时，增加判断Buying Sales 的状态。 如果还没有出库完成，则不能完成。
**  Modify by: Gavin @2017-04-18 (ver 1.0.0.2) 由于调用方提供的是 shipment单号,所以需要转换成 交易号.  
****************************************************************************/
BEGIN
  DECLARE @a INT, @SalesTxnNo VARCHAR(64)
  
  -- ver 1.0.0.2
  SELECT @SalesTxnNo = TXNNO FROM Ord_SalesShipOrder_H WHERE SalesShipOrderNumber = @TxnNo

  EXEC @a = LINKSVA.SVA_621_Test.DBO.ShoppingConfirm @SalesTxnNo, 2, @DeliveryNumber,@LogisticsProviderID

  RETURN @a

END

GO
