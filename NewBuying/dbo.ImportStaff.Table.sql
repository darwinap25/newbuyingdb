USE [NewBuying]
GO
/****** Object:  Table [dbo].[ImportStaff]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImportStaff](
	[Company] [varchar](64) NULL,
	[StaffNo] [varchar](64) NOT NULL,
	[EngName] [varchar](64) NULL,
	[Alias] [varchar](64) NULL,
	[Joined] [datetime] NULL,
	[Sex] [char](1) NULL,
	[DOB] [datetime] NULL,
	[EmailAddr] [varchar](100) NULL,
	[EmailAddr2] [varchar](100) NULL,
	[CountryCode] [varchar](64) NULL,
	[MobilePhone] [varchar](64) NULL,
	[TerminationDate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_IMPORTSTAFF] PRIMARY KEY CLUSTERED 
(
	[StaffNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ImportStaff] ADD  DEFAULT ((1)) FOR [Status]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'Company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'StaffNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'员工英文名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'EngName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'昵称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'Alias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'入职日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'Joined'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别。  M：男。  F：女' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'Sex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'DOB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'EmailAddr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'EmailAddr2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'MobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'离职日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'TerminationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1： 在职。  -1：离职。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入的staff数据， （for Bauhaus ）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportStaff'
GO
