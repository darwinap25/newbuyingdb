USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenRandNumber]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenRandNumber]
  @Length     int,                  -- 随机数长度 (最大 10 位) 
  @RandNumber varchar(64) output    -- 返回的随机数
AS
/****************************************************************************
**  Name : GenRandNumber
**  Version: 1.0.0.0
**  Description : 产生随机数
**  Example :
  declare @ApprovalCode varchar(64), @A int
  exec @A = GenRandNumber 9, @ApprovalCode output
  print @A
  print @ApprovalCode   005014, 005023  005256
**  Created by: Gavin @2014-05-22
**
****************************************************************************/
begin
  declare @Seq int, @ApprovalCodeSeq varchar(20), @i int, @sum int, @CheckDigit int, @AppSeq int
  declare @seed int, @Multiple int, @PerStr varchar(64)
  set @Length = isnull(@Length, 4)
  set @i = 1
  set @PerStr = '0'
  set @Multiple = 1

  if @Length > 10
     set @Length = 10
  while @i <= @Length
  begin
    set @Multiple = @Multiple * 10
    set @PerStr = @PerStr + '0'
    set @i = @i + 1
  end   
  set @seed = round(abs(cast(getdate() as float) * 10000000 - round(cast(getdate() as float) * 10000000, 0)) * 100000, 0)
    
  set @RandNumber = Right(@PerStr + LTRIM(str(round(rand(@seed) * @Multiple, 0))), @Length)

  return 0    
end

GO
