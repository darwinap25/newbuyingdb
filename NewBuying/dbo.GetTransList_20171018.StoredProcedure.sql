USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetTransList_20171018]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTransList_20171018]
  @TransNum              VARCHAR(64),              -- 交易号。 空表示不设条件
  @TransType             INT,                      -- 交易类型。 0 表示不设条件
  @TransStatus           INT,                      -- 交易状态。 0 表示不设条件
  @StoreCode             VARCHAR(64),              -- POS 的店铺编号
  @RegisterCode          VARCHAR(64),              -- POS 的注册编号
  @BusDate               DATE,                     -- 交易的Business date
  @CashierID             INT,                      -- 收银员
  @CouponNumber          VARCHAR(64),              -- 销售的CouponNumber
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN  
AS
/****************************************************************************
**  Name : GetTransList
**  Version : 1.0.0.2
**  Description : 获得交易数据
**
  declare  @a int, @PageCount int, @RecordCount int
  exec @a = GetTransList '',0,0, '','',null,0,   '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  sp_helptext GetTransList
  select * from TXNNO
**  Created by Gavin @2015-03-06
**  Modify by Gavin @2017-10-18 (ver 1.0.0.1) 增加返回会员姓名
**  Modify by Gavin @2017-10-18 (ver 1.0.0.2) 增加查询输入参数CouponNumber
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1  
  
  SET @TransNum = ISNULL(@TransNum, '')
  SET @TransType = ISNULL(@TransType, 0)
  SET @TransStatus = ISNULL(@TransStatus, 0)
  SET @CashierID = ISNULL(@CashierID, 0)
  SET @StoreCode = ISNULL(@StoreCode, '')
  SET @RegisterCode = ISNULL(@RegisterCode, '')
  SET @BusDate = ISNULL(@BusDate, '1900-01-01')  
  SET @CouponNumber = ISNULL(@CouponNumber, '')

  SET @WhereStr = ' WHERE 1=1 '
  IF @TransNum <> ''
    SET @WhereStr = @WhereStr + ' AND A.TransNum=''' + @TransNum + ''''
  IF @TransType <> 0
    SET @WhereStr = @WhereStr + ' AND A.TransType=' + CAST(@TransType as VARCHAR)
  IF @TransStatus <> 0
    SET @WhereStr = @WhereStr + ' AND A.Status=' + CAST(@TransStatus as VARCHAR)   
  IF @CashierID <> 0
    SET @WhereStr = @WhereStr + ' AND A.CashierID=' + CAST(@CashierID as VARCHAR)       
  IF @StoreCode <> ''
    SET @WhereStr = @WhereStr + ' AND A.StoreCode=''' + @StoreCode + ''''
  IF @RegisterCode <> ''
    SET @WhereStr = @WhereStr + ' AND A.RegisterCode=''' + @RegisterCode + ''''
  IF DATEDIFF(dd, @BusDate, '1900-01-01') < 0
    SET @WhereStr = @WhereStr + ' AND A.BusDate=''' + CONVERT(VARCHAR(10), @BusDate,120) + ''''
  IF @CouponNumber <> ''
    SET @WhereStr = @WhereStr + ' AND TransNum IN (SELECT TransNum FROM Sales_D WHERE Additional1=''' + @CouponNumber + ''') '

  SET @SQLStr = 'SELECT A.TransNum,A.TransType,A.StoreCode,A.RegisterCode,A.BusDate,A.TxnDate,A.CashierID,A.SalesManID,A.TotalAmount,A.Status, '
    + ' A.TransDiscount,A.TransDiscountType,A.TransReason,A.RefTransNum,A.InvalidateFlag,A.MemberSalesFlag,A.MemberID,A.CardNumber,A.DeliveryFlag,'
    + ' A.DeliveryCountry,A.DeliveryProvince,A.DeliveryCity,A.DeliveryDistrict,A.DeliveryAddressDetail,A.DeliveryFullAddress,'
    + ' A.RequestDeliveryDate,A.DeliveryDate,A.DeliveryBy,A.Contact,A.ContactPhone,A.PickupType,A.PickupStoreCode,A.CODFlag,A.Remark,A.SettlementDate,'
    + ' A.SettlementStaffID,A.PaySettleDate,A.CompleteDate,A.DeliveryNumber, '
	+ ' RTRIM(B.MemberChiFamilyName) + LTRIM(B.MemberChiGivenName) AS CustomerNameChn, RTRIM(B.MemberEngGivenName) + LTRIM(B.MemberEngFamilyName) AS CustomerNameEng  '
    + ' FROM SALES_H A '
	+ ' LEFT JOIN Member B ON A.MemberID = B.MemberID '
  SET @SQLStr = @SQLStr + @WhereStr
  
  EXEC SelectDataInBatchs @SQLStr, 'TransNum', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr  

  RETURN 0
END


GO
