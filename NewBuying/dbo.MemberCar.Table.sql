USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberCar]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberCar](
	[CarLicense] [varchar](64) NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[VIN] [varchar](64) NULL,
	[ContactPerson] [varchar](64) NULL,
	[ContactNumber] [varchar](64) NULL,
	[AttachmentFile] [varchar](512) NULL,
	[HKID] [varchar](64) NULL,
	[BR] [varchar](64) NULL,
	[Status] [int] NULL,
	[TargetMagic] [varchar](64) NULL,
	[RegistrationNumber] [varchar](64) NULL,
	[VehicleNumber] [varchar](64) NULL,
	[CINumber] [varchar](64) NULL,
	[CarOwnerName] [varchar](64) NULL,
	[FirstRegistrationDate] [datetime] NULL,
	[ReasonID] [int] NULL,
	[ModelCode] [varchar](64) NULL,
	[UpdatedBy] [int] NULL,
	[Remark] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[ModelVariant] [varchar](64) NULL,
 CONSTRAINT [PK_MEMBERCAR] PRIMARY KEY CLUSTERED 
(
	[CarLicense] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberCar] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[MemberCar] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[MemberCar] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Update_MemberCar]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_MemberCar] ON [dbo].[MemberCar]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_MemberCar
* Version: 1.0.0.0
* Description : MemberCar的批核触发器
** Create By Gavin @2015-09-30
*/
/*==============================================================*/
BEGIN  
  declare @OrderNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @PurchaseType int
  
  DECLARE CUR_MemberCar CURSOR fast_forward FOR
    SELECT CarLicense, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_MemberCar
  FETCH FROM CUR_MemberCar INTO @OrderNumber, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CarLicense = @OrderNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
      update MemberCar set ApprovalCode = @ApprovalCode,ApproveOn = Getdate(), ApproveBy = @CreatedBy  where CarLicense = @OrderNumber
    end      
    FETCH FROM CUR_MemberCar INTO @OrderNumber, @ApproveStatus, @CreatedBy
  END
  CLOSE CUR_MemberCar 
  DEALLOCATE CUR_MemberCar   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车牌号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'CarLicense'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属会员的卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车辆识别码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'VIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车辆联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'ContactPerson'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车辆联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'ContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'AttachmentFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'香港身份证' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'HKID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'BR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可能车辆上的号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'TargetMagic'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可能车辆注册号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'RegistrationNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可能就是VIN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'VehicleNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户系统的会员号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'CINumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车辆所有人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'CarOwnerName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可能车辆第一次注册时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'FirstRegistrationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原因ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'ReasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户要求添加字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'ModelCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态： P：prepare。  A:Approve 。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户要求添加字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar', @level2type=N'COLUMN',@level2name=N'ModelVariant'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员汽车记录
for InChacape
@2015-10-26 添加字段ModelVariant，ModelCode,remark.  之前添加了ReasonID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberCar'
GO
