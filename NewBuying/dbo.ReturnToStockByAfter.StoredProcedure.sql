USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ReturnToStockByAfter]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ReturnToStockByAfter]
  @AfterTxnNo               VARCHAR(64)
AS
/****************************************************************************
**  Name : ReturnToStockByAfter   
**  Version: 1.0.0.7
**  Description :  售后单批核后退货到库存。 
sp_helptext ReturnToStockByAfter
select * from aftersales_H
Declare @a int
exec @a =ReturnToStockByAfter '601216C002972100'
print @a
**  Created by: Gavin @2016-10-24
**  Modify by: Gavin @2016-12-29 (ver 1.0.0.1) 根据Picking 单判断
**  Modify by: Gavin @2017-01-18 (ver 1.0.0.2) 如果已经出库（shipping单状态 > 0），则产生一张收货单，等待用户收货批核
**  Modify by: Gavin @2017-01-25 (ver 1.0.0.3) pick单的orderqty不能直接清0, 需要根据 aftersales的 数量减去. 
**  Modify by: Gavin @2017-02-08 (ver 1.0.0.4) 收货单 hardcode为 指定 ECO 仓库收货。
**  Modify by: Gavin @2017-03-22 (ver 1.0.0.5) (IF @ApproveStatus = 'A' AND ISNULL(@Status, -1) > 0 ) 条件时，插入detail时，取aftersales数据时，需要检查storecode。
**  Modify by: Gavin @2017-03-23 (ver 1.0.0.6) 改写过程.退货要按照aftersales中的数量来退,先退未出库的,再退已经出库的. （实际上因为未完成的交易都全部退了不执行此过程。 所以，这里只有全部都出库的情况）
**  Modify by: Gavin @2017-04-07 (ver 1.0.0.7) 增加判断，如果拣货单的OrderQty数量都为 0， 那么这个拣货单状态改为 V。 
****************************************************************************/
BEGIN
  DECLARE @StoreCode VARCHAR(64), @StoreID INT, @TxnType INT, @OriginalTxnNo VARCHAR(64), @SalesPickOrderNumber VARCHAR(64),
          @ApproveStatus CHAR(1), @PickupLocation VARCHAR(64), @Status INT, @BusDate DATETIME, @SalesShipOrderNumber VARCHAR(64)
  DECLARE @NewNumber VARCHAR(64), @ReturnQty INT, @NOOutPickQty INT, @ProdCode VARCHAR(64), @OutPickQty INT, @TempQTY INT
  DECLARE @PPCODE VARCHAR(64), @PQTY INT, @OQTY INT, @PKeyID INT, @RemainingQty INT

  SET @AfterTxnNo = ISNULL(@AfterTxnNo, '')

  SELECT @StoreCode = CASE WHEN ISNULL(PickupStoreCode,'') <> '' THEN PickupStoreCode ELSE StoreCode END,
      @TxnType = TxnType,  @OriginalTxnNo = RefTransNum
    FROM AfterSales_H H WHERE TxnNo = @AfterTxnNo
  SELECT @StoreID = StoreID FROM BUY_Store WHERE StoreCode = @StoreCode
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC

  -- 只是为了获取出库单的送货地址等信息
  DECLARE @SalesShipOrderNumber1 VARCHAR(64)
  SELECT TOP 1 @SalesShipOrderNumber1  = SalesShipOrderNumber FROM Ord_SalesShipOrder_H WHERE txnno = @OriginalTxnNo
  ---------------

  IF ISNULL(@StoreID, 0) = 0
    RETURN -1
 
  DECLARE CUR_ReturnToStockByAfter CURSOR FAST_FORWARD FOR
    SELECT SalesPickOrderNumber, ApproveStatus, PickupLocation, B.StoreID FROM Ord_SalesPickOrder_H A
	   LEFT JOIN BUY_STORE B ON A.PickupLocation = B.StoreCode
	  WHERE ReferenceNo = @OriginalTxnNo
  OPEN CUR_ReturnToStockByAfter 
  FETCH FROM CUR_RETURNTOSTOCKBYAFTER INTO @SalesPickOrderNumber, @ApproveStatus, @PickupLocation, @StoreID
  WHILE @@FETCH_STATUS = 0
  BEGIN
      IF @ApproveStatus = 'A' 
        SELECT TOP 1 @Status = Status, @SalesShipOrderNumber = SalesShipOrderNumber FROM Ord_SalesShipOrder_H WHERE ReferenceNo = @SalesPickOrderNumber
	  IF ISNULL(@Status, -1) = 0
	  BEGIN
	    UPDATE  Ord_SalesShipOrder_H SET Status = 4 WHERE ReferenceNo = @SalesPickOrderNumber
	    UPDATE Ord_SalesPickOrder_H SET ApproveStatus = 'P', ApprovalCode = '', UpdatedOn = GETDATE() 
		WHERE SalesPickOrderNumber = @SalesPickOrderNumber AND ApproveStatus = 'A'
      END
    FETCH FROM CUR_RETURNTOSTOCKBYAFTER INTO @SalesPickOrderNumber, @ApproveStatus, @PickupLocation, @StoreID
  END
  CLOSE CUR_ReturnToStockByAfter
  DEALLOCATE CUR_ReturnToStockByAfter


  DECLARE CUR_ReturnTAfterSales CURSOR FAST_FORWARD FOR
    SELECT ProdCode,ABS(ISNULL(D.Qty,0)) FROM AfterSales_D D LEFT JOIN AfterSales_H H ON D.TxnNo = H.TxnNo 
	  WHERE D.TxnNo=@AfterTxnNo AND (H.TxnType=2 OR (H.TxnType=3 AND D.Qty < 0))
	ORDER BY ProdCode,D.Qty   
  OPEN CUR_ReturnTAfterSales 
  FETCH FROM CUR_ReturnTAfterSales INTO @ProdCode, @ReturnQty
  WHILE @@FETCH_STATUS = 0
  BEGIN
    SELECT @NOOutPickQty = SUM(ISNULL(ActualQty,0)) FROM Ord_SalesPickOrder_D D 
	  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber
	WHERE H.ApproveStatus = 'P' AND H.ReferenceNo = @OriginalTxnNo AND D.ProdCode = @ProdCode

    SELECT @OutPickQty = SUM(ISNULL(ActualQty,0)) FROM Ord_SalesPickOrder_D D 
	  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber
	WHERE H.ApproveStatus = 'A' AND H.ReferenceNo = @OriginalTxnNo AND D.ProdCode = @ProdCode
	
    SET @ReturnQty = ISNULL(@ReturnQty, 0)
    SET @NOOutPickQty = ISNULL(@NOOutPickQty, 0)
    SET @OutPickQty = ISNULL(@OutPickQty, 0)

	-- 优先归还未出库的
	IF @NOOutPickQty >= @ReturnQty
	BEGIN
	  SET @TempQTY = @ReturnQty
	  SET @RemainingQty = 0
	END ELSE
	BEGIN
	  SET @TempQTY = @NOOutPickQty
	  SET @RemainingQty = @ReturnQty - @NOOutPickQty 
    END

	IF @NOOutPickQty > 0
	BEGIN
			  DECLARE CUR_DeductPickQty_UPDATE CURSOR FAST_FORWARD FOR
				SELECT KeyID, OrderQty, B.StoreID FROM Ord_SalesPickOrder_D D 
				  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber
				  LEFT JOIN BUY_STORE B ON H.PickupLocation = B.StoreCode
				 WHERE H.ApproveStatus = 'P' AND H.ReferenceNo = @OriginalTxnNo AND D.ProdCode = @ProdCode
				ORDER BY KeyID           
			  OPEN CUR_DeductPickQty_UPDATE 
			  FETCH FROM CUR_DeductPickQty_UPDATE INTO @PKeyID, @OQTY, @StoreID
			  WHILE @@FETCH_STATUS = 0
			  BEGIN
			    IF abs(@TempQTY) > 0
				BEGIN
					IF abs(@TempQTY) <= abs(@OQTY)
					BEGIN
					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'R', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), -ABS(@TempQTY), ISNULL(T.OnhandQty,0) - ABS(@TempQTY), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'R' ) T ON D.ProdCode = T.ProdCode 
					  WHERE D.KeyID = @PKeyID

					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'G', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), ABS(@TempQTY), ISNULL(T.OnhandQty,0) + ABS(@TempQTY), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D 
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'G' ) T ON D.ProdCode = T.ProdCode  	   
					  WHERE D.KeyID = @PKeyID

					  UPDATE Ord_SalesPickOrder_D SET OrderQty = OrderQty - abs(@TempQTY), ActualQty = 0 WHERE KeyID = @PKeyID
					  SET @TempQTY = 0
					END ELSE
					BEGIN
					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'R', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), -ABS(D.OrderQty), ISNULL(T.OnhandQty,0) - ABS(D.OrderQty), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'R' ) T ON D.ProdCode = T.ProdCode 
					  WHERE D.KeyID = @PKeyID

					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'G', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), ABS(D.OrderQty), ISNULL(T.OnhandQty,0) + ABS(D.OrderQty), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D 
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'G' ) T ON D.ProdCode = T.ProdCode  	   
					  WHERE D.KeyID = @PKeyID

					  UPDATE Ord_SalesPickOrder_D SET OrderQty = 0, ActualQty = 0 WHERE KeyID = @PKeyID
					  SET @TempQTY = abs(@TempQTY) - abs(@OQTY)				
					END 
                END
				FETCH FROM CUR_DeductPickQty_UPDATE INTO @PKeyID, @OQTY, @StoreID
			  END
			  CLOSE CUR_DeductPickQty_UPDATE
			  DEALLOCATE CUR_DeductPickQty_UPDATE    	

			  UPDATE Ord_SalesPickOrder_H SET ApproveStatus = 'V', UpdatedOn = GETDATE()
			  WHERE SalesPickOrderNumber IN (
				  SELECT SalesPickOrderNumber FROM
				  (
					  SELECT D.SalesPickOrderNumber, SUM(ISNULL(D.OrderQty, 0)) AS ORDQTY FROM Ord_SalesPickOrder_D D 
						LEFT JOIN Ord_SalesPickOrder_H H ON H.SalesPickOrderNumber = D.SalesPickOrderNumber
					  WHERE H.ReferenceNo = @OriginalTxnNo AND H.ApproveStatus = 'P'
					  Group By D.SalesPickOrderNumber
				  ) A WHERE A.ORDQTY = 0
			  )
    END

	IF @RemainingQty > 0    
	BEGIN
        -- ver 1.0.0.2 不是直接影响库存。 而是产生收货单，等待批核。  170 是hardcode， 以后再定。
		EXEC GetRefNoString 'RECIO', @NewNumber OUTPUT

		INSERT INTO Ord_ReceiveOrder_D (ReceiveOrderNumber, ProdCode, OrderQty, ReceiveQty, StockTypeCode, Remark)
		VALUES (@NewNumber, @ProdCode, ABS(@RemainingQty), ABS(@RemainingQty), 'G', 'Retutn Sales Auto Gen Receive Order')

		INSERT INTO Ord_ReceiveOrder_H (ReceiveOrderNumber,OrderType,ReceiveType,ReferenceNo,FromStoreID,FromContactName,
			FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
			StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
			ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		SELECT @NewNumber, 1, 2, A.SalesShipOrderNumber, 170, A.Contact,
			A.ContactPhone,A.ContactPhone,'','',C.StoreID,C.Contact,C.ContactPhone,C.StoreEmail,
			C.StoreTel,C.StoreFullDetail1,'Customer return order',@BUSDATE,NULL,'','P',
			NULL,NULL,GETDATE(),A.CreatedBy,GETDATE(),A.UpdatedBy
		FROM Ord_SalesShipOrder_H A  
			LEFT JOIN Ord_SalesPickOrder_H B ON A.ReferenceNo = B.SalesPickOrderNumber
			LEFT JOIN BUY_STORE C ON  C.StoreCode = 'ECO' -- B.PickupLocation = C.StoreCode  
		WHERE A.SalesShipOrderNumber = @SalesShipOrderNumber1

	END
    FETCH FROM CUR_ReturnTAfterSales INTO @ProdCode, @ReturnQty
  END
  CLOSE CUR_ReturnTAfterSales
  DEALLOCATE CUR_ReturnTAfterSales

  SET NOCOUNT OFF  
END

GO
