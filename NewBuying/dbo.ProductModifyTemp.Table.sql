USE [NewBuying]
GO
/****** Object:  Table [dbo].[ProductModifyTemp]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductModifyTemp](
	[ProdCode] [varchar](64) NOT NULL,
	[IsOnlineSKU] [tinyint] NULL,
	[Feature] [tinyint] NULL,
	[HotSale] [tinyint] NULL,
	[Price] [money] NULL,
	[RefPrice] [money] NULL,
	[Status] [tinyint] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_PRODUCTMODIFYTEMP] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProductModifyTemp] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:  下架。  1：上架' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp', @level2type=N'COLUMN',@level2name=N'IsOnlineSKU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'推荐标志 （Flag1）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp', @level2type=N'COLUMN',@level2name=N'Feature'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'热卖标志' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp', @level2type=N'COLUMN',@level2name=N'HotSale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际销售价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp', @level2type=N'COLUMN',@level2name=N'Price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指导价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp', @level2type=N'COLUMN',@level2name=N'RefPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：未审核。1：已审核。2：已经更新。-1：作废。  此字段预留，未使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'@2016-12-14 @Gavin
用户product变更表。  每次操作后清空。
status暂时没用。  如果需要批核的话就启用。
设置的字段，如果是null，则不更新。 如果有值则更新。 （同理，读入的文件中没有填写则不更新）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductModifyTemp'
GO
