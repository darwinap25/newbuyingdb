USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardPromotionMsgByPosition]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[MemberCardPromotionMsgByPosition]
  @CardNumber varchar(64),    -- 卡号
  @PositionID varchar(64),    -- 地点编号
  @PositionType varchar(64),  -- 地点定位类型BlueTooth: 1; Mac:2; Defined Position: 3
  @LanguageAbbr varchar(20)=''
AS
/****************************************************************************
**  Name : MemberCardPromotionMsgByPosition
**  Version: 1.0.0.0
**  Description : 根据ibeacon位置,获得指定的promotionmsg
 
   declare @A int
   exec @A=MemberCardPromotionMsgByPosition '000300933', '1', '1'
   print @A

**  Created by: Gavin @2015-08-20
**
****************************************************************************/
BEGIN
  declare @MemberID int, @ReturnAmount money, @ReturnPoint int, @iBeaconID int, @AreaID int, @PromotionMsgKeyID int,
          @iBeaconGroupID int, @StoreID int, @StoreCode varchar(64), 
          @BrandID int, @BrandCode varchar(64), @count int, @recordcount int
  Create Table #Temp (iid int, KeyID int, PromotionDesc nvarchar(max), PromotionTitle nvarchar(512), PromotionPicFile varchar(512), 
      PromotionRemark nvarchar(2000), CreatedOn datetime, StartDate datetime, EndDate datetime, PromotionMsgCode varchar(64), 
      UpdatedOn datetime, PromotionMsgTypeID int, PromotionPicFile2 varchar(512), PromotionPicFile3 varchar(512),
      IsRead int, Link varchar(512), URL varchar(512))
  Create Table #Return (Title varchar(512), Association_Type int, Association_ID varchar(64), Assiocation_Code varchar(64), 
    PicFile varchar(512), URL varchar(512), [Desc] varchar(512))
          
  select @MemberID = MemberID, @ReturnAmount = TotalAmount, @ReturnPoint = TotalPoints 
    from Card where CardNumber = @CardNumber	
  select @AreaID = AreaID, @iBeaconID = iBeaconID from iBeacon where iBeaconCode = @PositionID
  select @iBeaconGroupID = iBeaconGroupID from iBeaconGroupMap where iBeaconID = @iBeaconID
  select @PromotionMsgKeyID = AssociationID from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID and AssociationType = 2

  DECLARE CUR_EarchStore CURSOR fast_forward FOR
    select StoreID from AreaStoreMap where AreaID = @AreaID 
  OPEN CUR_EarchStore  
  FETCH FROM CUR_EarchStore INTO @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN 
    select @StoreCode = StoreCode, @BrandID = BrandID from Store where StoreID = @StoreID
    select @BrandCode = StoreBrandCode from Brand where StoreBrandID = @BrandID
    insert into #Temp
      (iid, KeyID, PromotionDesc, PromotionTitle, PromotionPicFile, PromotionRemark, CreatedOn, StartDate, 
      EndDate, PromotionMsgCode, UpdatedOn, PromotionMsgTypeID, PromotionPicFile2, PromotionPicFile3, IsRead, Link, URL)
    exec GetMemberPromotions @MemberID, @CardNumber, @StoreCode, '', '', @BrandCode, '',  0, 0, 1, null,null,1, 0, @count output, @recordcount output, @LanguageAbbr 
     
    FETCH FROM CUR_EarchStore INTO @StoreID  
  END
  CLOSE CUR_EarchStore 
  DEALLOCATE CUR_EarchStore 

  insert into #Return
  select PromotionTitle, 2, KeyID, PromotionMsgCode, PromotionPicFile, '', PromotionDesc
  from #Temp 
 
 select * from #Return  
  
END

GO
