USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetStoreAttribute]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetStoreAttribute]
  @StoreID				int,			   --店铺ID
  @ConditionStr			nvarchar(1000)='',
  @OrderCondition	    nvarchar(1000)='',
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''
AS
/****************************************************************************
**  Name : GetStoreAttribute  
**  Version: 1.0.0.0
**  Description : 获取店铺的附加属性.
**  Parameter :
      declare @A int, @PageCount int, @RecordCount int
      exec GetStoreAttribute 1, '', '', 1, 0, @PageCount output, @RecordCount output, ''
      print @A
      print @PageCount
      print @RecordCount
      select * from store
**  return: 
     
**  Created by: Gavin @2014-07-04
**
****************************************************************************/
begin
  declare @SQLStr varchar(1000)
  set @StoreID = isnull(@StoreID, 0)
  set @SQLStr = ' select StoreID, L.StoreAttributeID, A.StoreAttributeCode, A.Description1, A.Description2, A.Description3, '
     + ' A.StoreAttributePic from StoreAttributeList L left join Store_Attribute A on L.StoreAttributeID = A.StoreAttributeID '
     + ' where L.StoreID = ' + CAST(@StoreID as varchar) + ' or ' + CAST(@StoreID as varchar) + ' = 0 '
  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr
  return 0
end

GO
