USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardPicking_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardPicking_H](
	[CardPickingNumber] [varchar](64) NOT NULL,
	[PurchaseType] [int] NULL,
	[ReferenceNo] [varchar](64) NULL,
	[BrandID] [int] NULL,
	[OrderType] [int] NULL,
	[FromStoreID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[CustomerType] [int] NULL,
	[CustomerID] [int] NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[FromAddress] [nvarchar](512) NULL,
	[StoreContactName] [nvarchar](512) NULL,
	[StoreContactPhone] [nvarchar](512) NULL,
	[StoreContactEmail] [nvarchar](512) NULL,
	[StoreMobile] [nvarchar](512) NULL,
	[FromContactName] [nvarchar](512) NULL,
	[FromContactNumber] [nvarchar](512) NULL,
	[FromEmail] [nvarchar](512) NULL,
	[FromMobile] [nvarchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[Remark1] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_CARDPICKING_H] PRIMARY KEY CLUSTERED 
(
	[CardPickingNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CardPicking_H] ADD  DEFAULT ((1)) FOR [PurchaseType]
GO
ALTER TABLE [dbo].[Ord_CardPicking_H] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[Ord_CardPicking_H] ADD  DEFAULT ((2)) FOR [CustomerType]
GO
ALTER TABLE [dbo].[Ord_CardPicking_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CardPicking_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CardPicking_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CardPicking_H] ON [dbo].[Ord_CardPicking_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CardPicking_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2014-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @PurchaseType int, @ordernumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @PurchaseType = PurchaseType, @ordernumber = CardPickingNumber FROM INSERTED  
  if ISNULL(@PurchaseType, 1) = 1
    exec GenUserMessage @CreatedBy, 3, 0, @ordernumber
  else exec GenUserMessage @CreatedBy, 22, 0, @ordernumber 
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CardPicking_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CardPicking_H] ON [dbo].[Ord_CardPicking_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CardPicking_H
* Version: 1.0.0.4
* Description : Pickup表的批核触发器 (根据Update_Ord_CouponPicking_H （1.0.1.0))
* Create By Gavin @2014-10-09
** Modify by Gavin @2015-02-11 (ver 1.0.0.1) 增加PurchaseType的区分。 PurchaseType=1 是原有的card库存订单。 PurchaseType=2是成card购买充值订单
** Modify by Gavin @2015-04-30 (ver 1.0.0.2) PurchaseType = 2时检查是否有填卡号,没填的话填上.
** Modify by Gavin @2015-06-08 (ver 1.0.0.3)  ROLLBACK 不能中止后面的操作, 必须加上return
** Modify by Gavin @2015-10-09 (ver 1.0.0.4) 增加消息发送
*/
/*==============================================================*/
BEGIN  
  declare @CardPickingNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @FirstCard varchar(64), @EndCard varchar(64), @CardTypeID int, @CardGradeID int, @PurchaseType int
  declare @FromStoreID int, @StoreID int, @KeyID int, @FirstCardNumber varchar(64), @EndCardNumber varchar(64)
  declare @card1 varchar(64), @card2 varchar(64), @a int
  
  DECLARE CUR_Ord_CardPicking_H CURSOR fast_forward FOR
    SELECT CardPickingNumber, ApproveStatus, CreatedBy, isnull(PurchaseType,1), FromStoreID, StoreID FROM INSERTED
  OPEN CUR_Ord_CardPicking_H
  FETCH FROM CUR_Ord_CardPicking_H INTO @CardPickingNumber, @ApproveStatus, @CreatedBy, @PurchaseType, @FromStoreID, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CardPickingNumber = @CardPickingNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      if @PurchaseType = 1
      begin
        if not exists(select * from Ord_CardPicking_D where CardPickingNumber = @CardPickingNumber)
        begin
          RAISERROR ('No detail record.', 16, 1)
          if (@@Trancount > 0)
            ROLLBACK TRANSACTION 
          return     
        end
        exec GenApprovalCode @ApprovalCode output    
        exec GenDeliveryOrder_Card @CreatedBy, @CardPickingNumber, @ApprovalCode      
        update Ord_CardPicking_H set ApprovalCode = @ApprovalCode , UpdatedOn = GETDATE() where CardPickingNumber = @CardPickingNumber
        
        exec ChangeCardStockStatus 5, @CardPickingNumber, 1
      end else if @PurchaseType = 2
      begin
        DECLARE CUR_Ord_CardPicking_D CURSOR fast_forward FOR
          SELECT KeyID, CardTypeID, CardGradeID, FirstCardNumber, EndCardNumber FROM Ord_CardPicking_D  
            where CardPickingNumber = @CardPickingNumber and (isnull(FirstCardNumber,'') = '' or isnull(EndCardNumber,'') = '') 
        OPEN CUR_Ord_CardPicking_D
        FETCH FROM CUR_Ord_CardPicking_D INTO @KeyID, @CardTypeID, @CardGradeID, @FirstCardNumber, @EndCardNumber
        WHILE @@FETCH_STATUS=0
        BEGIN   
          if isnull(@FirstCardNumber,'') = ''
            select top 1 @card1 =CardNumber from Card 
              where IssueStoreID = @FromStoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
          if isnull(@EndCardNumber,'') = ''
            select top 1 @card2 =CardNumber from Card 
              where IssueStoreID = @StoreID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
          
          update Ord_CardPicking_D set FirstCardNumber = case when isnull(@card1,'') = '' then FirstCardNumber else @card1 end,
              EndCardNumber = case when isnull(@card2,'') = '' then EndCardNumber else @card2 end
            where KeyID = @KeyID                    
          FETCH FROM CUR_Ord_CardPicking_D INTO @KeyID, @CardTypeID, @CardGradeID, @FirstCardNumber, @EndCardNumber
        END
        CLOSE CUR_Ord_CardPicking_D 
        DEALLOCATE CUR_Ord_CardPicking_D  
            
        exec GenApprovalCode @ApprovalCode output    
        exec @a = DoTelcoTransfer @CreatedBy, @CardPickingNumber
        if @a <> 0
        begin
          RAISERROR ('Store Transfer is fail. ', 16, 1)
          if (@@Trancount > 0)
            ROLLBACK TRANSACTION 
          return  
        end           
        update Ord_CardPicking_H set ApprovalCode = @ApprovalCode, UpdatedOn = GETDATE() where CardPickingNumber = @CardPickingNumber
      end 
      
      if ISNULL(@PurchaseType, 1) = 1
        exec GenUserMessage @CreatedBy, 3, 1, @CardPickingNumber
      else exec GenUserMessage @CreatedBy, 22, 1, @CardPickingNumber        
    end
    
    if @ApproveStatus = 'V' and Update(ApproveStatus)
    begin
      exec ChangeCardStockStatus 5, @CardPickingNumber, 2    
    end
        
    FETCH FROM CUR_Ord_CardPicking_H INTO @CardPickingNumber, @ApproveStatus, @CreatedBy, @PurchaseType, @FromStoreID, @StoreID   
  END
  CLOSE CUR_Ord_CardPicking_H 
  DEALLOCATE CUR_Ord_CardPicking_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'CardPickingNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货种类。1：实体卡订货。 2： 指定卡购买金额或者积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'PurchaseType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。指coupon订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源方店铺主键，一般可能是总部' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货方店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户类型（备用，由大客户引起订单时填写）。1：客戶訂貨。 2：店鋪訂貨
默认2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'CustomerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'customer表 主键， 外键.（有大客户时填写）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network，5：快递送货（实体Coupon）。
1到4 是虚拟Coupon。  5 是实体Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址（店铺地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'FromContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void
状态流程：  R -> P -> A' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡拣货单主表
@2014-09-17 改成和Ord_CouponPicking_H一样的结构
@2015-02-11 统一增加PurchaseType 字段, 用于区分 金额/积分类的 订单 或者是 实体卡的 订单' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardPicking_H'
GO
