USE [NewBuying]
GO
/****** Object:  Table [dbo].[School]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[School](
	[SchoolID] [int] IDENTITY(1,1) NOT NULL,
	[SchoolCode] [varchar](64) NULL,
	[SchoolName1] [nvarchar](512) NULL,
	[SchoolName2] [nvarchar](512) NULL,
	[SchoolName3] [nvarchar](512) NULL,
	[SchoolArea1] [nvarchar](512) NULL,
	[SchoolArea2] [nvarchar](512) NULL,
	[SchoolArea3] [nvarchar](512) NULL,
	[SchoolDistrict1] [nvarchar](512) NULL,
	[SchoolDistrict2] [nvarchar](512) NULL,
	[SchoolDistrict3] [nvarchar](512) NULL,
	[SchoolNote] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SCHOOL] PRIMARY KEY CLUSTERED 
(
	[SchoolID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[School] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[School] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校所属地区1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolArea1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校所属地区2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolArea2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校所属地区3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolArea3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校所属区域1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolDistrict1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校所属区域2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolDistrict2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校所属区域3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolDistrict3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School', @level2type=N'COLUMN',@level2name=N'SchoolNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学校表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'School'
GO
