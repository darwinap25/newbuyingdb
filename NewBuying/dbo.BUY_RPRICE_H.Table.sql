USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_RPRICE_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_RPRICE_H](
	[RPriceCode] [varchar](64) NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[StoreGroupCode] [varchar](64) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CurrencyCode] [varchar](64) NULL,
	[Note] [nvarchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_RPRICE_H] PRIMARY KEY NONCLUSTERED 
(
	[RPriceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_RPRICE_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_RPRICE_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Update_BUY_RPRICE_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_BUY_RPRICE_H] ON [dbo].[BUY_RPRICE_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_BUY_RPRICE_H
* Version: 1.0.0.0
* Description : BUY_RPRICE_H的批核触发器

  select * from BUY_RPRICE_H
** Create By Gavin @2017-06-01
*/
/*==============================================================*/
BEGIN  
  DECLARE @RPriceCode VARCHAR(64), @ApproveStatus CHAR(1), @CreatedBy INT, @OldApproveStatus CHAR(1), @ApprovalCode CHAR(6)
  
  DECLARE CUR_BUY_RPRICE_H CURSOR fast_forward FOR
    SELECT RPriceCode, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_BUY_RPRICE_H
  FETCH FROM CUR_BUY_RPRICE_H INTO @RPriceCode, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    SELECT @OldApproveStatus = ApproveStatus FROM Deleted WHERE RPriceCode = @RPriceCode
    IF (@OldApproveStatus = 'P' OR ISNULL(@OldApproveStatus, '') = '') AND @ApproveStatus = 'A' AND UPDATE(ApproveStatus)
    BEGIN
      EXEC GenApprovalCode @ApprovalCode OUTPUT  
	    
	  INSERT INTO BUY_RPRICE_M(RPriceCode,ProdCode,RPriceTypeCode,RefPrice,Price,PromotionPrice,MemberPrice,
	    StoreCode,StoreGroupCode,StartDate,EndDate,Status,ApproveOn,ApproveBy,UpdatedOn,UpdatedBy) 
      SELECT D.RPriceCode,D.ProdCode,D.RPriceTypeCode,D.RefPrice,D.Price,D.PromotionPrice,D.MemberPrice,
	    H.StoreCode,H.StoreGroupCode,H.StartDate,H.EndDate,1,GETDATE(),@CreatedBy,GETDATE(),@CreatedBy
	    FROM BUY_RPRICE_D D LEFT JOIN BUY_RPRICE_H H ON D.RPriceCode = H.RPriceCode
	  WHERE D.RPriceCode = @RPriceCode

      UPDATE BUY_RPRICE_H SET ApprovalCode = @ApprovalCode, ApproveOn = GETDATE(), ApproveBy = @CreatedBy,
	      UpdatedOn = GETDATE(), UpdatedBy = @CreatedBy
	  WHERE RPriceCode = @RPriceCode
    END      
    FETCH FROM CUR_BUY_RPRICE_H INTO @RPriceCode, @ApproveStatus, @CreatedBy  
  END
  CLOSE CUR_BUY_RPRICE_H 
  DEALLOCATE CUR_BUY_RPRICE_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'RPriceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺Code。空/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。空/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'StoreGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码。一般应该是本币代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'CurrencyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价格表，主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_RPRICE_H'
GO
