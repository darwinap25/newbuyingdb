USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberMessage]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberMessage](
	[MemberMessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageType] [int] NULL,
	[MessageTitle] [nvarchar](512) NULL,
	[MessageContent] [nvarchar](max) NULL,
	[SenderMemberID] [int] NULL,
	[MemberID] [int] NULL,
	[MessageReceiveMode] [int] NULL,
	[SNSTypeID] [int] NULL,
	[Status] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[USEFULEXPRESSIONSID] [int] NULL,
	[MemberPresentID] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [varchar](512) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [varchar](512) NULL,
 CONSTRAINT [PK_MEMBERMESSAGE] PRIMARY KEY CLUSTERED 
(
	[MemberMessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberMessage] ADD  DEFAULT ((0)) FOR [MessageReceiveMode]
GO
ALTER TABLE [dbo].[MemberMessage] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[MemberMessage] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[MemberMessage] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'MemberMessageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息类型：1：通知类。2：交互消息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'MessageType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'MessageTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息正文' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'MessageContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送人MemberID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'SenderMemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收方的MemberID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息接收方式：0：所有。1：kiosk。2：手机短信。3：SNS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'MessageReceiveMode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SNS类型，外键。 MessageReceiveMode为3时有效，指定SNSType接收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'SNSTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息状态：0：未读。1：已读。2：已过期。3：已删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短语ID，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'USEFULEXPRESSIONSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转赠表，外键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage', @level2type=N'COLUMN',@level2name=N'MemberPresentID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员消息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessage'
GO
