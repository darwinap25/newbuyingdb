USE [NewBuying]
GO
/****** Object:  UserDefinedFunction [dbo].[DecryptCardNumber]    Script Date: 12/13/2017 2:45:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DecryptCardNumber]
(
  @EnCryptCardNumber varchar(40), @KeyIDea varchar(30)
)
RETURNS varchar(40) 
AS
BEGIN
  declare @CardNumber varchar(40)
  DECLARE @position int, @string char(15), @Total int, @CharNum int, @TempNum1 int, @TempNum2 int
  set @CardNumber = ''
  
  SET @position = 1
  set @Total = 0
  WHILE @position <= DATALENGTH(@KeyIDea)
  BEGIN
    SELECT @Total = @Total + ASCII(SUBSTRING(@KeyIDea, @position, 1))
    SET @position = @position + 1
  END
  Set @Total = @Total % 10
  
  SET @position = 1
  WHILE @position <= DATALENGTH(@EnCryptCardNumber)
  BEGIN
    set @TempNum1 = ASCII(SUBSTRING(@EnCryptCardNumber, @position, 1))
    set @TempNum2 = ASCII(SUBSTRING(@EnCryptCardNumber, @position + 1, 1))
    if @TempNum1 = 40
      set @TempNum1 = 60
    if @TempNum1 = 41
      set @TempNum1 = 62
    if @TempNum1 = 37 
      set @TempNum1 = 64    
            
    if @TempNum2 = 40
      set @TempNum2 = 60
    if @TempNum2 = 41
      set @TempNum2 = 62
    if @TempNum2 = 37 
      set @TempNum2 = 64          
      
    SELECT @CharNum = (@TempNum1 - 48) * 75 + (@TempNum2 - 48)
    SELECT @CharNum = @CharNum / ((@position + 1) / 2 + @Total)
     
    --SELECT @CharNum = @CharNum ^ @Total
    
    set @CardNumber = @CardNumber + CHAR(@CharNum)
    SET @position = @position + 2
  END  
  return @CardNumber
END

GO
