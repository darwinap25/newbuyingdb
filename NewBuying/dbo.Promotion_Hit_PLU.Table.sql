USE [NewBuying]
GO
/****** Object:  Table [dbo].[Promotion_Hit_PLU]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion_Hit_PLU](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PromotionCode] [varchar](64) NOT NULL,
	[HitSeq] [int] NOT NULL,
	[HitPLUSeq] [int] NULL,
	[EntityNum] [varchar](64) NULL,
	[EntityType] [int] NULL,
	[HitSign] [int] NULL,
	[PLUHitType] [int] NULL,
 CONSTRAINT [PK_PROMOTION_HIT_PLU] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Promotion_Hit_PLU] ADD  DEFAULT ((0)) FOR [HitPLUSeq]
GO
ALTER TABLE [dbo].[Promotion_Hit_PLU] ADD  DEFAULT ((0)) FOR [EntityType]
GO
ALTER TABLE [dbo].[Promotion_Hit_PLU] ADD  DEFAULT ((1)) FOR [HitSign]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户自己填，会按照这个来排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promotion_Hit表序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'HitSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号。将根据序号排序。默认0.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'HitPLUSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'EntityNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'决定EntityNum中内容的含义。默认0
0：所有货品。EntityNum中为空
1：EntityNum内容为 prodcode。 销售EntityNum中指定货品时，可以生效
2：EntityNum内容为 DepartCode。。 销售EntityNum中指定部门的货品时，可以生效
3：EntityNum内容为 TenderCode。 使用EntityNum中指定货币支付时，可以生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'EntityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中的符号。默认1
1：此记录为命中条件。 
0：此记录为非命中条件。（即指定的货品或部门或tender等不参与促销，除此都参与）
不参加促销时，不考虑hit条件
@2014-08-30 修改： 因为UI的原因，修改内容含义： 1：命中。 2：不命中
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'HitSign'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列表中的货品或者部门的属性。
1： 命中条件。 （和Promotion_Hit 中的条件有关。比如hit条件是命中数量，那么就是指这里填写的货品的数量，其他货品不计算）
2： 过滤条件。（此时HitSign 属性有效，指参与的货品是否参与促销或排除在外。会产生PromotionHitPLUList表数据）
3： 命中条件。 （和1 的区别在于，和Promotion_Hit 中的条件无关。 只有销售清单中包含这些货品，促销才成立。）

同类型货品列表，互相之间的关系
1：根据 Pormotion_Hit的设置，决定or， and的关系
2：根据列表的输入次序，过滤条件逐条累加，后面的可以覆盖前面的。
3：or 的关系。 销售清单中包含任何列表中的货品即可能命中。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU', @level2type=N'COLUMN',@level2name=N'PLUHitType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销命中表的指定货品列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Hit_PLU'
GO
