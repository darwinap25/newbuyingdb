USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponStatusOnhand]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponStatusOnhand](
	[CouponTypeID] [int] NOT NULL,
	[TotalQty] [int] NULL,
	[Dormant] [int] NULL,
	[Issue] [int] NULL,
	[Active] [int] NULL,
	[Redeemed] [int] NULL,
	[Expired] [int] NULL,
	[Void] [int] NULL,
	[Recycle] [int] NULL,
	[Hide] [int] NULL,
	[Deleted] [int] NULL,
 CONSTRAINT [PK_COUPONSTATUSONHAND] PRIMARY KEY CLUSTERED 
(
	[CouponTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [TotalQty]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Dormant]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Issue]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Active]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Redeemed]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Expired]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Void]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Recycle]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Hide]
GO
ALTER TABLE [dbo].[CouponStatusOnhand] ADD  DEFAULT ((0)) FOR [Deleted]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponStatusOnhand', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponStatusOnhand', @level2type=N'COLUMN',@level2name=N'TotalQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon的各种状态的统计数量。
注： for 711使用。  这个表只是记录各类Coupon的各种状态的数量， Coupon_movement的触发器负责更新这个表。
这个表的数量只是用于 Kiosk上显示， 不能保证数量的准确性。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponStatusOnhand'
GO
