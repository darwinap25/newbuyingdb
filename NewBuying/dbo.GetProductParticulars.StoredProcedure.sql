USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductParticulars]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductParticulars]
  @Prodcode             varchar(64),       -- 货品编码
  @LanguageAbbr			varchar(20)=''     -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1    
AS
/****************************************************************************
**  Name : GetProductParticulars    
**  Version: 1.0.0.0
**  Description : 获得单个货品的属性细节
**
  declare @a int, @PageCount int, @RecordCount int, @Barcode varchar(64), @Prodcode varchar(64), @DepartCode varchar(64), @BrandCode varchar(64), @ProdType int
  set @Barcode = ''
  set @Prodcode = ''
  set @DepartCode = ''
  set @BrandCode = ''
  set @ProdType = 0
  exec @a = GetProductParticulars @Prodcode, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
**  Created by: Gavin @2013-05-03
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select P.ProdCode, 
         case @Language when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName , 
		 case @Language when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, 
		 P.ProdPicFile, P.DepartCode, D.BrandID, P.ProdType, P.ProdNote, P.ProductBrandID, 
         case @Language when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName,
		 case @Language when 2 then B.ProductBrandDesc2 when 3 then B.ProductBrandDesc3 else B.ProductBrandDesc1 end as ProductBrandDesc,
		 ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, 
		 A.ProdFunction, A.ProdIngredients, A.ProdInstructions, A.PackDesc, A.PackUnit, 
		 A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6, A.MemoTitle1, A.MemoTitle2, A.MemoTitle3, A.MemoTitle4, A.MemoTitle5, A.MemoTitle6,
		 P.NewFlag, P.HotSaleFlag, P.PackQty, P.OriginID, P.ColorID, P.NonSale, C.ColorCode, C.ColorPicFile,
		 P.Flag1, P.Flag2, P.Flag3, P.Flag4, P.Flag5, N.NationFlagFile,
		 case @Language when 2 then NationName2 when 3 then NationName3 else NationName1 end as NationName, 
		 case @Language when 2 then ColorName2 when 3 then ColorName3 else ColorName1 end as ColorName
    from product P left join (select * from Product_Particulars where LanguageID = @Language) A on P.ProdCode = A.Prodcode
      left join Product_Brand B on P.ProductBrandID = B.ProductBrandID 
      left join Department D on D.DepartCode = P.DepartCode       
      left join Nation N on P.OriginID = N.NationID          
      left join Color C on P.ColorID = C.ColorID       
    where P.ProdCode = @Prodcode

  return 0
end

GO
