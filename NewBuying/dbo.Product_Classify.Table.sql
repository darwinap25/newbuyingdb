USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_Classify]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Classify](
	[ProdCode] [varchar](64) NOT NULL,
	[ForeignkeyID] [int] NOT NULL,
	[ForeignTable] [varchar](64) NOT NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PRODUCT_CLASSIFY] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC,
	[ForeignkeyID] ASC,
	[ForeignTable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Classify', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键ID。（ForeignTable指定表的外键）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Classify', @level2type=N'COLUMN',@level2name=N'ForeignkeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键表。（表名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Classify', @level2type=N'COLUMN',@level2name=N'ForeignTable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品划分。（辅助表。 货品和其他表配合，多对多关系）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Classify'
GO
