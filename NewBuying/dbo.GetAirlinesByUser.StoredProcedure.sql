USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetAirlinesByUser]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/04 12:37:31
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetAirlinesByUser]
   @userid              int
AS
SET NOCOUNT ON;

BEGIN

        SELECT 
            [b].[BrandCode] AS AirlineCode,
            [b].[BrandName1] AS Description,
            [b].[BrandCode] +'-'+[b].[BrandName1] AS Description2,
            (SELECT COUNT(1) FROM [dbo].[Brand] b
                INNER JOIN [dbo].[UserAirlineMap] e
                    ON [e].[AirlineCode] = [b].[BrandCode] AND e.[UserID] = @userid) AS count
        FROM [dbo].[Brand] b
        INNER JOIN [dbo].[UserAirlineMap] e
            ON [e].[AirlineCode] = [b].[BrandCode] AND e.[UserID] = @userid
        ORDER BY [Description2]

END




GO
