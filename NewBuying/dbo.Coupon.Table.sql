USE [NewBuying]
GO
/****** Object:  Table [dbo].[Coupon]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupon](
	[CouponNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[CouponNatureID] [int] NULL,
	[CouponIssueDate] [datetime] NOT NULL,
	[CouponExpiryDate] [datetime] NOT NULL,
	[CouponActiveDate] [datetime] NULL,
	[StoreID] [int] NULL,
	[RedeemStoreID] [int] NULL,
	[BatchCouponID] [int] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF__Coupon__Status__566E88C8]  DEFAULT ((0)),
	[CouponPassword] [varchar](512) NULL,
	[CardNumber] [varchar](64) NULL,
	[InitCardNumber] [varchar](64) NULL,
	[CouponAmount] [money] NULL,
	[PickupFlag] [int] NULL CONSTRAINT [DF__Coupon__PickupFl__5762AD01]  DEFAULT ((0)),
	[LocateStoreID] [int] NULL,
	[VerifyNum] [varchar](64) NULL,
	[VerifyNumUpdatedOn] [datetime] NULL,
	[StockStatus] [int] NULL,
	[CouponRedeemDate] [datetime] NULL,
	[IsRead] [int] NULL CONSTRAINT [DF__Coupon__IsRead__5856D13A]  DEFAULT ((0)),
	[IsFavorite] [int] NULL CONSTRAINT [DF__Coupon__IsFavori__594AF573]  DEFAULT ((0)),
	[CreatedOn] [datetime] NULL CONSTRAINT [DF__Coupon__CreatedO__5A3F19AC]  DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL CONSTRAINT [DF__Coupon__UpdatedO__5B333DE5]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[Exported] [int] NULL DEFAULT ((0)),
 CONSTRAINT [Coupon_PK] PRIMARY KEY CLUSTERED 
(
	[CouponNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠券发行日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponIssueDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠券过期日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵激活日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponActiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'领取优惠劵的店铺代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用coupon的店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'RedeemStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时的批次号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'BatchCouponID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵状态。
0:	Dormant (need active)
1:	issued （need active）	 		                
2:	Active (can be used) 		               
3:	already redeem  (can be used)                   		                
4:	already expiry
5:	already void	
6:             Recycled
7:             hid (隐藏)
8:             deleted (被删除)
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵密码。 存储内容为 MD5加密的密文。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主卡卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'首次获得者的CardNumber。（以区分转赠者，用于判断Coupon允许的下载数量限制）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'InitCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存放coupon的金额。即CouponType中的CouponTypeAmount值。 当CouponType为不定值类型时， 此金额将和CouponTypeAmount值不同。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'捡取标志，0：未被捡取。1：已被捡取。默认为0。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'PickupFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实体coupon发送到的店铺。发送单签收时填写' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'LocateStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'验证码。一般是4位随机数字。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'VerifyNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'验证码产生时间。用于检查验证码是否已经过期。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'VerifyNumUpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon的库存状态。
1. For Printing (總部未確認收貨)
2. Good For Release (總部確認收貨)
3. Damaged (總部發現有優惠劵損壞)
4. Picked (總部收到了店舖的訂單)
5. In Transit (總部收到了店舖的訂單, 並發貨)
6. In Location (店舖確認收貨)
7. Returned (店舖退貨給總部)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'StockStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵使用日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'CouponRedeemDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已读标志。0：没有。 1：已读' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'IsRead'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否喜爱。0：喜爱的。 1：不是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon', @level2type=N'COLUMN',@level2name=N'IsFavorite'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon'
GO
