USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardPointDetail]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardPointDetail](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[AddPoint] [int] NOT NULL,
	[BalancePoint] [int] NOT NULL,
	[ForfeitPoint] [int] NULL,
	[ExpiryDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL DEFAULT ((1)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [varchar](512) NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [varchar](512) NULL,
 CONSTRAINT [PK_CardPointDetail] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'增加的积分值。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail', @level2type=N'COLUMN',@level2name=N'AddPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'积分余额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail', @level2type=N'COLUMN',@level2name=N'BalancePoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'被丢弃的积分。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail', @level2type=N'COLUMN',@level2name=N'ForfeitPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔积分有效期。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail', @level2type=N'COLUMN',@level2name=N'ExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。
0：积分无效。 1：有效。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡积分记录明细表。（记录每一笔增加的记录，以及每一笔的余额）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardPointDetail'
GO
