USE [NewBuying]
GO
/****** Object:  Table [dbo].[temp_EligibleAirlineMap]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_EligibleAirlineMap](
	[Id] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[EligibleAirlineCode] [varchar](64) NULL,
	[Flag] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
