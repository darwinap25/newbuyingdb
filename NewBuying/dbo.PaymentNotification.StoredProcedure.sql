USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[PaymentNotification]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentNotification]

  @TxnNo			  varchar(50),

  @MemberID			  int			output,

  @DateAdded          varchar(100)	output,

  @MemberEmail		  varchar(50)	output,

  @CardNumber		  varchar(50)	output,

  @PayMethod          varchar(50)	output,

  @PickupType         varchar(50)	output,

  @BillAddress        varchar(200)	output,

  @BillContact        varchar(200)	output,

  @BillContactPhone   varchar(200)	output,

  @DeliveryAddress    varchar(200)	output,

  @Contact            varchar(200)	output,

  @ContactPhone       varchar(200)	output, 

  @SubTotal           money         output,

  @TotalAmount		  money			output,    

  @CouponNo           varchar(50)	output,

  @TenderAmount       money			output,    

  @QueryCondition     nvarchar(1000)='',

  @OrderCondition	  nvarchar(1000)='',

  @PageCurrent		  int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1

  @PageSize			  int=0,			   -- 每页记录数， 为0时，不分页，默认0

  @PageCount	      int=0 output,	       -- 返回总页数。

  @RecordCount		  int=0 output,	       -- 返回总记录数。	

  @Language		       varchar(50)='' output

AS

/****************************************************************************

**  Name : PaymentNotification

**  Version: 1.0.0.2

**  Description : （for bauhaus）。 (SVA DB)

**

  declare @a int, @PageCount int, @RecordCount int

  exec @a = PaymentNotification  ,'KTXNNO000000154','1043',,@PageCount output,@RecordCount output,'zh_BigCN'

  print @a  

  print @PageCount

  print @RecordCount  

  select * from product

  select * from color

  select * from Product_size

**  Created by: Thomas @2016-10-07

** Modified by: Thomas @2016-11-09

** Modified by: Gavin @2017-08-25 (ver 1.0.0.2) 合并版本，把txnno 改为transnum

****************************************************************************/

BEGIN



  set nocount on;

  declare @SQLStr nvarchar(4000), @LanguageID int

  declare @ReturnDateAdded varchar(100), @ReturnCardNumber varchar(50);



  --select @LanguageID = DescFieldNo from LanguageMap where LanguageAbbr = @Language



  --select @Language = LanguageAbbr , @LanguageID = MemberDefLanguage from member where MemberID = @MemberID



/* Comment by 1.0.0.1 

  select @DateAdded = convert(nvarchar(100), H.CreatedOn, 120), 

         @PickupType = CASE When PickupType = 1 Then 

		                 Case When M.MemberDefLanguage = 2  Then '门店自取' When M.MemberDefLanguage = 3 Then '門店自取' Else 'Store Pickup' End 

					   ELSE 

					     Case When M.MemberDefLanguage = 3 Then '送货上门' When M.MemberDefLanguage = 3 Then '送貨上門' Else 'Home Delivery' End									   

                       END, 					   

	     @BillAddress = BillAddress, @BillContact =  BillContact, @BillContactPhone = BillContactPhone, @DeliveryAddress = DeliveryAddress,

	     @TotalAmount = TotalAmount,

		 @CardNumber = CardNumber,

		 @LanguageID = M.MemberDefLanguage,

		 @Language = L.LanguageAbbr,

		 @MemberID = H.MemberID,

		 @Contact = H.Contact,

		 @ContactPhone = H.ContactPhone

  from sales_h H

  left join Member M on H.MemberID = M.MemberID

  left join LanguageMap L on M.MemberDefLanguage = L.KeyID  

  where TxnNo = @TxnNo;

*/

  select @DateAdded = convert(nvarchar(100), H.CreatedOn, 120), 

         @PickupType = CASE When PickupType = 1 Then 

		                 Case When M.MemberDefLanguage = 2  Then '门店自取' When M.MemberDefLanguage = 3 Then '門店自取' Else 'Store Pickup' End 

					   ELSE 

					     Case When M.MemberDefLanguage = 3 Then '送货上门' When M.MemberDefLanguage = 3 Then '送貨上門' Else 'Home Delivery' End									   

                       END, 					   

	     @BillAddress = BillAddress, @BillContact =  BillContact, @BillContactPhone = BillContactPhone, 

		 @DeliveryAddress = CASE When PickupType = 1 Then 

		                      CASE M.MemberDefLanguage When 2 Then S.StoreName2 +'-'+ S.StoreCode + '   '+ StoreAddressDetail

							                           When 3 Then S.StoreName3 +'-'+ S.StoreCode + '   '+ StoreAddressDetail

						                               Else S.StoreName1 +'-'+ S.StoreCode + '   '+ StoreAddressDetail END

	

				        ELSE 

					          DeliveryFullAddress  -- DeliveryAddress

                            END,		 

	     @TotalAmount = TotalAmount,

		 @CardNumber = CardNumber,

		 @LanguageID = M.MemberDefLanguage,

		 @Language = L.LanguageAbbr,

		 @MemberID = H.MemberID,

		 @Contact = H.Contact,

		 @ContactPhone = H.ContactPhone

  from sales_h H

  left join Member M on H.MemberID = M.MemberID

  left join LanguageMap L on M.MemberDefLanguage = L.KeyID  

  left join store S on H.PickupStoreCode = S.StoreCode

  where TransNum = @TxnNo;  

  

    

  if isnull(@LanguageID, 0) = 0

     set @LanguageID = 1       



	 set @QueryCondition = isnull(@QueryCondition, '')



 -- select @CardNumber = CardNumber from Card where MemberID = @MemberID;



  select @MemberEmail = MemberRegisterMobile from Member where MemberID = @MemberID;



  select @PayMethod = Case @LanguageID When 2 Then b.TenderName2 When 3 Then b.TenderName3 Else b.TenderName1 End,

         @CouponNo = CASE TenderDesc When 'Coupon' Then a.Additional Else '' End,

		 @TenderAmount = TenderAmount 

    from sales_t a

	left join tender b on a.tenderCode = b.TenderCode

	where TransNum = @TxnNo;	

	

		

  select @SubTotal = sum(NetAmount) from sales_d where TransNum = @TxnNo; 

	

  SET @SQLStr = ' select p.ProdCode, ps.ProductSizeID, ps.ProductSizeCode, '

      + ' Case ' + cast(@LanguageID as Varchar) + ' When 2 Then ProdName2 When 3 Then ProdName3 Else ProdName1 End as ProdName, '

      + ' Case ' + cast(@LanguageID as Varchar) + ' When 2 Then ColorName2 When 3 Then ColorName3 Else ColorName1 End as ColorName, '

      + ' Case ' + cast(@LanguageID as Varchar) + ' When 2 Then ProductSizeName2 When 3 Then ProductSizeName3 Else ProductSizeName1 End as ProductSizeName, '

      + ' d.Qty, d.NetPrice, d.NetAmount '

      + ' from sales_d d '         

      + ' left join product p on d.ProdCode = p.ProdCode '

      + ' left join color c on p.colorCode = c.colorCode '

      + ' left join Product_Size ps on p.ProductSizeID = ps.ProductSizeID '

      + ' where p.ProductSizeCode = ps.ProductSizeCode '
	  + ' and (d.TransNum = ''' + @TxnNo + ''' or ''' + @TxnNo + ''' = '''') '

	
      

  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @QueryCondition



  

  return 0





END

/*================================================================================*/

GO
