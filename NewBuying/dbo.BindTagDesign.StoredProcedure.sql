USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BindTagDesign]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BindTagDesign]
	-- Add the parameters for the stored procedure here
	@tagtypeid			int		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT t.TagDesignName1,
	t.TagDesignCode
	FROM TagDesign t
		INNER JOIN TagDesignType td
			on t.TagDesignTypeCode = td.TagDesignTypeCode
		INNER JOIN Brand b
			on td.BrandCode = b.BrandCode
		INNER JOIN CouponType c
			on b.BrandCode = c.BrandCode
    WHERE c.CouponTypeID = @tagtypeid
END



GO
