USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardCashDetail]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardCashDetail](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[TenderCode] [varchar](64) NULL,
	[TenderRate] [decimal](16, 6) NULL DEFAULT ((1)),
	[AddAmount] [money] NOT NULL,
	[BalanceAmount] [money] NOT NULL,
	[ForfeitAmount] [money] NULL,
	[ExpiryDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL DEFAULT ((1)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [varchar](512) NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [varchar](512) NULL,
 CONSTRAINT [PK_CardCashDetail] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'充值支付用的Tender（预留字段）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'TenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'充值支付用的Tender的汇率（预留字段）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'TenderRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'充值金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'AddAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'余额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'BalanceAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'丢弃金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'ForfeitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'ExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。
0：无效。1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡金额记录明细表。（记录每一笔增加的记录，以及每一笔的余额）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardCashDetail'
GO
