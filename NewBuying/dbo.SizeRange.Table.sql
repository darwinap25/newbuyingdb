USE [NewBuying]
GO
/****** Object:  Table [dbo].[SizeRange]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SizeRange](
	[SizeRangeID] [int] NOT NULL,
	[SizeRangeCode] [varchar](64) NULL,
	[SizeRangeName1] [varchar](512) NULL,
	[SizeRangeName2] [varchar](512) NULL,
	[SizeRangeName3] [varchar](512) NULL,
 CONSTRAINT [PK_SIZERANGE] PRIMARY KEY CLUSTERED 
(
	[SizeRangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SizeRange', @level2type=N'COLUMN',@level2name=N'SizeRangeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SizeRange', @level2type=N'COLUMN',@level2name=N'SizeRangeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SizeRange', @level2type=N'COLUMN',@level2name=N'SizeRangeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SizeRange', @level2type=N'COLUMN',@level2name=N'SizeRangeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SizeRange', @level2type=N'COLUMN',@level2name=N'SizeRangeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'尺寸范围
@2016-04-06 for bauhaus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SizeRange'
GO
