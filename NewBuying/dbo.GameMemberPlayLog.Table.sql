USE [NewBuying]
GO
/****** Object:  Table [dbo].[GameMemberPlayLog]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameMemberPlayLog](
	[MemberID] [int] NOT NULL,
	[GameCampaignID] [int] NOT NULL,
	[GameCampaignPrizeID] [int] NOT NULL,
	[Date] [char](10) NOT NULL,
	[GSource] [nchar](100) NOT NULL,
	[PlayCount] [int] NOT NULL,
	[MemberLastDate] [datetime] NOT NULL,
 CONSTRAINT [PK_GAMEMEMBERPLAYLOG] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC,
	[GameCampaignID] ASC,
	[GameCampaignPrizeID] ASC,
	[Date] ASC,
	[GSource] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'抽奖重置表。 Create by Lisa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameMemberPlayLog'
GO
