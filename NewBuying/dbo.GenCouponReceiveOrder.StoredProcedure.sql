USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenCouponReceiveOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[GenCouponReceiveOrder]
  @UserID                   int,             --操作员ID
  @OrderSupplierNumber      varchar(64)     --单号
AS
/****************************************************************************
**  Name : GenCouponReceiveOrder
**  Version: 1.0.0.7
**  Description : 根据提交给供应商的订单来产生收货单 （供应商订单 批核时调用）
**  example :
  declare @a int  , @CouponOrderFormNumber varchar(64)
  set @CouponOrderFormNumber = 'BCC0000000502'
  exec @a = GenCouponReceiveOrder 1, @CouponOrderFormNumber
  print @a  
  select * from Ord_OrderToSupplier_D
  select * from Ord_CouponReceive_H where ReferenceNo = 'BCC0000000502'
  select * from Ord_CouponReceive_D where CouponReceiveNumber = 'CREC00000000476'
delete from  Ord_CouponReceive_H where CouponReceiveNumber >= 'CREC00000000473' 
**  Created by: Gavin @2014-05-20 
**  Modify by: Gavin @2014-06-11 (ver 1.0.0.1) 产生Receive单时，同时产生Ord_OrderToSupplier_D数据。（Ord_OrderToSupplier 创建单时只有H表，没有D表数据，批核时自动产生）
**  Modify by: Gavin @2014-06-12 (ver 1.0.0.2) 需要重新创建一批Coupon来使用.
**  Modify by: Gavin @2014-07-24 (ver 1.0.0.3) 使用Detail中的CouponTypeID,一个订单允许多个CouponType
**  Modify by: Gavin @2014-08-28 (ver 1.0.0.4) Ord_OrderToSupplier_D表增加字段：OrderRoundUpQty 
**  Modify by: Gavin @2014-09-22 (ver 1.0.0.5) 由于使用了批量创建Coupon, 所以insert into Ord_OrderToSupplier_D 时取Coupon的逻辑要改变。（可能有多个batchcouponid）
**  Modify by: Gavin @2014-09-26 (ver 1.0.0.6) 产生receive单时，根据数量，分割成多个收货单。
**  Modify by: Gavin @2014-10-13 (ver 1.0.0.7) 取消拆分多个Ord_CouponReceive_H单
****************************************************************************/
begin
  declare @BusDate datetime, @TxnDate datetime, @CouponReceiveNumber varchar(64)
  declare @FirstCouponNumber varchar(64), @EndCouponNumber varchar(64), @CouponTypeID int, @CouponCount int
  declare @IssuedDate datetime, @InitAmount money, @InitPoints int, @RandomPWD int, @InitPassword varchar(512), 
          @ExpiryDate datetime, @ApprovalCode varchar(512), @ReturnBatchID varchar(30), 
          @ReturnStartNumber varchar(64), @ReturnEndNumber varchar(64), @InitStatus int, @StoreID int
  declare @PackageQty int, @OrderRoundUpQty int            
   
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()
  
  -- 批核供应商订单批核时产生CouponNumber，并扩展订单明细
  DECLARE CUR_GenCouponReceiveOrder CURSOR fast_forward for
    SELECT CouponTypeID, OrderQty, PackageQty, OrderRoundUpQty, FirstCouponNumber
      --from Ord_OrderToSupplier_D where OrderSupplierNumber = @OrderSupplierNumber and isnull(FirstCouponNumber, '') = ''
      from Ord_OrderToSupplier_D where OrderSupplierNumber = @OrderSupplierNumber and isnull([BatchCouponCode], '') = ''
  OPEN CUR_GenCouponReceiveOrder
  FETCH FROM CUR_GenCouponReceiveOrder INTO @CouponTypeID, @CouponCount, @PackageQty, @OrderRoundUpQty, @FirstCouponNumber
  WHILE @@FETCH_STATUS=0 
  BEGIN         
    set @IssuedDate = GETDATE()
    select @InitAmount = CouponTypeAmount, @InitPoints = CouponTypePoint from coupontype where CouponTypeID = @CouponTypeID
    set @RandomPWD = 0
    set @InitPassword = '' 
    set @ExpiryDate = null
    set @InitStatus = 0
    set @ApprovalCode = ''
    --exec BatchGenerateNumber @UserID, 1, @CouponTypeID, null, null, @CouponCount, @IssuedDate, @InitAmount, @InitPoints, @RandomPWD, @InitPassword, @ExpiryDate, @OrderSupplierNumber, @ApprovalCode, @UserID, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, '', @InitStatus     
    exec BatchGenerateNumber @UserID, 1, @CouponTypeID, null, @FirstCouponNumber, @CouponCount, @IssuedDate, @InitAmount, @InitPoints, @RandomPWD, @InitPassword, @ExpiryDate, @OrderSupplierNumber, @ApprovalCode, @UserID, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, '', @InitStatus     
 
    -- @ReturnBatchID是自增长KeyID，认为当前过程批量产生时，大于@ReturnBatchID的都是属于这个订单的。
    insert into Ord_OrderToSupplier_D (OrderSupplierNumber, CouponTypeID, OrderQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode, PackageQty, OrderRoundUpQty)
    select @OrderSupplierNumber, @CouponTypeID, 1, CouponNumber, CouponNumber, BatchCouponCode, @PackageQty, @OrderRoundUpQty
    from
    (
      select ROW_NUMBER() OVER(order by CouponNumber) as iid, CouponNumber, B.BatchCouponCode
       from Coupon C left join BatchCoupon B on C.BatchCouponID = B.BatchCouponID
       where C.BatchCouponID >= @ReturnBatchID and C.CouponTypeID = @CouponTypeID
    ) A 
    where iid <= @CouponCount   
           
  FETCH FROM CUR_GenCouponReceiveOrder INTO @CouponTypeID, @CouponCount, @PackageQty, @OrderRoundUpQty, @FirstCouponNumber
  END
  CLOSE CUR_GenCouponReceiveOrder 
  DEALLOCATE CUR_GenCouponReceiveOrder 
  
  --delete from Ord_OrderToSupplier_D where OrderSupplierNumber = @OrderSupplierNumber and isnull(FirstCouponNumber, '') = ''  
  delete from Ord_OrderToSupplier_D where OrderSupplierNumber = @OrderSupplierNumber and isnull([BatchCouponCode], '') = ''  

  -- 
   
  -- 产生收货单。(拆分多个单据)
/*  
  declare @OrderCount int, @TempCount int, @UnitCount int, @DoCount int, @SumCount int
  set @UnitCount = 10000
  set @SumCount = 0  
    
  select @OrderCount = COUNT(*) from Ord_OrderToSupplier_D where OrderSupplierNumber = @OrderSupplierNumber
  set @TempCount = @OrderCount

  while @TempCount > 0
  begin
    if @TempCount > @UnitCount
    begin
      set @DoCount = @UnitCount 
      set @TempCount = @TempCount - @UnitCount
    end else
    begin
      set @DoCount = @TempCount 
      set @TempCount = 0
    end    

    exec GetRefNoString 'CREC', @CouponReceiveNumber output     
    insert into Ord_CouponReceive_H(CouponReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponTypeID, CouponQty, ReceiveType, ordertype)
    select @CouponReceiveNumber, OrderSupplierNumber, StoreID, SupplierID, SendAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponTypeID, CouponQty, 1, ordertype
     from Ord_OrderToSupplier_H where OrderSupplierNumber = @OrderSupplierNumber

    insert into Ord_CouponReceive_D (CouponReceiveNumber, CouponTypeID, Description, OrderQty, ActualQty, 
          FirstCouponNumber, EndCouponNumber, BatchCouponCode, CouponStockStatus, ReceiveDateTime)  
    select @CouponReceiveNumber,CouponTypeID, '', @DoCount, @DoCount, 
         FirstCouponNumber, EndCouponNumber, BatchCouponCode, 1, GETDATE()
     from
    (       
      select ROW_NUMBER() OVER(order by FirstCouponNumber) as iid, CouponTypeID, OrderQty,  
         FirstCouponNumber, EndCouponNumber, BatchCouponCode
        from Ord_OrderToSupplier_D where OrderSupplierNumber = @OrderSupplierNumber
    ) A
    where A.iid > @SumCount and A.iid <= @SumCount + @DoCount
    set @SumCount = @SumCount + @DoCount       
  end
*/  
  -- 产生收货单
    exec GetRefNoString 'CREC', @CouponReceiveNumber output     
    insert into Ord_CouponReceive_H(CouponReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponTypeID, CouponQty, ReceiveType, ordertype)
    select @CouponReceiveNumber, OrderSupplierNumber, StoreID, SupplierID, SendAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponTypeID, CouponQty, 1, ordertype
     from Ord_OrderToSupplier_H where OrderSupplierNumber = @OrderSupplierNumber

    insert into Ord_CouponReceive_D (CouponReceiveNumber, CouponTypeID, Description, OrderQty, ActualQty, 
          FirstCouponNumber, EndCouponNumber, BatchCouponCode, CouponStockStatus, ReceiveDateTime)
    select @CouponReceiveNumber, CouponTypeID, '', OrderQty, OrderQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode, 1, GETDATE()
          from Ord_OrderToSupplier_D where  OrderSupplierNumber = @OrderSupplierNumber         
      
  -- 库存数量变动
  exec ChangeCouponStockStatus 2, @OrderSupplierNumber, 1   
   
 /*  
  update Coupon set StockStatus = 1  from Coupon C left join Ord_OrderToSupplier_D D on C.CouponNumber = D.FirstCouponNumber
  where D.OrderSupplierNumber = @OrderSupplierNumber
 
  -- 改变Onhand
  exec UpdateCouponOnhand_CouponOrder 70, null, @StoreID, @CouponTypeID, @CouponCount   
 */         
  return 0
end




GO
