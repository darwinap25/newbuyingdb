USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SendMemberNoticeMessage]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SendMemberNoticeMessage]
  @UserID               int,
  @ReceiveMemberID      int,
  @MessageType          int,
  @MessagePriority      int,
  @MessageCoding        int,
  @MessageTitle         nvarchar(512),
  @MessageBody          varbinary(Max)        
AS
/****************************************************************************
**  Name : SendMemberNoticeMessage
**  Version: 1.0.0.0
**  Description : 系统向指定用户发布消息，调用SendMemberMessageObject
**
**  Parameter :
  declare @a int, @MessageBody  varbinary(Max), @MessageID int, @ReceiveMemberID int, 
    @MessageServiceTypeID int, @MessageType int, @MessagePriority int, @MessageCoding int,  @MessageTitle nvarchar(512),
    @IsInternal int, @ReceiveList varchar(max)
  
  set @ReceiveMemberID = 2
  set @MessageServiceTypeID = 1
  set @MessageType  = 1
  set @MessagePriority = 1
  set @MessageCoding = 0
  set @MessageTitle = 'test message center'
  set @MessageBody = cast('test message center' as varbinary)
  set @IsInternal = 1
  set @ReceiveList = '2'  -- 发送给memberID： 2 和 170， 之间用 , 隔开
  exec @a = SendMemberNoticeMessage 1,@ReceiveMemberID, @MessageType, 
      @MessagePriority, @MessageCoding, @MessageTitle,  @MessageBody
  print @a  
  select * from MessageObject
  select * from MessageReceiveList order by keyid
  
  delete from MessageObject
  delete from MessageReceiveList  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin
  declare @MessageServiceTypeID int, @ReceiveList varchar(512), @MessageID int
  set @ReceiveList = cast(@ReceiveMemberID as varchar)
  DECLARE CUR_NoticeMessage CURSOR fast_forward local FOR
    select  MessageServiceTypeID from membermessageaccount 
     where memberid = @ReceiveMemberID and IsPrefer = 1 
  OPEN CUR_NoticeMessage
  FETCH FROM CUR_NoticeMessage INTO @MessageServiceTypeID
  WHILE @@FETCH_STATUS=0
  BEGIN  
    exec SendMemberMessageObject @UserID, 0, @MessageServiceTypeID, @MessageType, 
        @MessagePriority, @MessageCoding, @MessageTitle,  @MessageBody, 1, @ReceiveList, @MessageID output
    FETCH FROM CUR_NoticeMessage INTO @MessageServiceTypeID
  END
  CLOSE CUR_NoticeMessage
  DEALLOCATE CUR_NoticeMessage  
  return 0
end

GO
