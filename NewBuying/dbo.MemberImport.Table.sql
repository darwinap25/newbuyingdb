USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberImport]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberImport](
	[MemberID] [int] IDENTITY(1,1) NOT NULL,
	[MemberPassword] [varchar](512) NULL,
	[MemberIdentityType] [int] NULL DEFAULT ((1)),
	[MemberIdentityRef] [nvarchar](512) NULL,
	[MemberRegisterMobile] [varchar](64) NOT NULL,
	[MemberMobilePhone] [varchar](64) NULL,
	[MemberEmail] [nvarchar](512) NULL,
	[NickName] [nvarchar](512) NULL,
	[MemberAppellation] [nvarchar](512) NULL,
	[MemberEngFamilyName] [nvarchar](512) NULL,
	[MemberEngGivenName] [nvarchar](512) NULL,
	[MemberChiFamilyName] [nvarchar](512) NULL,
	[MemberChiGivenName] [nvarchar](512) NULL,
	[MemberSex] [int] NULL DEFAULT ((0)),
	[MemberDateOfBirth] [datetime] NULL,
	[MemberDayOfBirth] [int] NULL,
	[MemberMonthOfBirth] [int] NULL,
	[MemberYearOfBirth] [int] NULL,
	[MemberMarital] [int] NULL DEFAULT ((0)),
	[MemberPosition] [nvarchar](512) NULL,
	[CompanyDesc] [nvarchar](512) NULL,
	[SpRemark] [nvarchar](512) NULL,
	[Status] [int] NULL DEFAULT ((1)),
	[MemberDefLanguage] [int] NULL,
	[NoVerifyEMail] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MEMBERIMPORT] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员密码。（MD5加密）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员证件类型
1：身份证号码。2：军官证。 3：回乡证' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberIdentityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员证件号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberIdentityRef'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员注册用手机号（加上国家代码的，例如0861392572219）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberRegisterMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员手机号，不加国家代码（例如，1394592381）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberMobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员昵称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'NickName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员称呼：
Mr.  Miss.  Mrs.     或者 先生   小姐   夫人。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberAppellation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员英文名（姓）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberEngFamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员英文名（名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberEngGivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员中文名（姓）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberChiFamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员中文名（名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberChiGivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员性别
null或0：保密。1：男性。 2：女性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberSex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（出生日期）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberDateOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（天）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberDayOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（月）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberMonthOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（年）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberYearOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'婚姻情况。默认 0 . 0: 保密  1：未婚. 2：已婚。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberMarital'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员职位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberPosition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'CompanyDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'SpRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键，LanguageMap表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'MemberDefLanguage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未验证邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport', @level2type=N'COLUMN',@level2name=N'NoVerifyEMail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bauhaus的Member导入表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberImport'
GO
