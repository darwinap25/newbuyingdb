USE [NewBuying]
GO
/****** Object:  UserDefinedFunction [dbo].[EncryptMD5]    Script Date: 12/13/2017 2:45:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[EncryptMD5]
(
   @Code varchar(512)  
)
RETURNS varchar(512) 
AS
/****************************************************************************
**  Name : EncryptMD5
**  Version: 1.0.0.0
**  Description : MD5加密. (必须要有MD5的系列函数)
  select dbo.EncryptMD5('0')
**  Created by: Gavin @2012-08-21
**
****************************************************************************/
begin
  return dbo.MD5(@Code, 32)  
end

GO
