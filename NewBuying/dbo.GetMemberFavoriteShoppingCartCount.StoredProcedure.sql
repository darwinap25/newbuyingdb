USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberFavoriteShoppingCartCount]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetMemberFavoriteShoppingCartCount]
  @MemberID             bigint,               -- 会员ID
  @FavoriteType         int = 0,           -- 收藏信息的类型。 默认0, 1:指收藏货品。输入0 表示忽略此条件
  @FavoriteCount        int output,        -- 返回favorite 的货品总数
  @ShoppingCartCount    int output         -- 返回shoppingcart的货品总数
AS
/****************************************************************************
**  Name : GetMemberFavoriteShoppingCartCount  
**  Version: 1.0.0.2
**  Description : 获取会员的收藏的货品数量和购物车的货品数量
**  Parameter :
  declare @a int , @FavoriteCount int, @ShoppingCartCount int  
  exec @a = GetMemberFavoriteShoppingCartCount 837, 1, @FavoriteCount output, @ShoppingCartCount output
  print @a
  print @FavoriteCount 
  print @ShoppingCartCount  
  select * from MemberShoppingCart
**  Created by: Gavin @2015-12-03
**  Modified by: Gavin @2016-10-17 (ver 1.0.0.1) @MemberID 类型由int改为 bigint
**  Modified by: Gavin @2017-01-03 (ver 1.0.0.2) @ShoppingCartCount 返回的内容， 改为所有货品的数量。  
**
****************************************************************************/
begin
  set @MemberID = isnull(@MemberID, 0)
  set @FavoriteType = isnull(@FavoriteType, 0)
  select @FavoriteCount = count(KeyID) from MemberFavorite where MemberID = @MemberID and (FavoriteType = @FavoriteType or @FavoriteType = 0)
  -- ver 1.0.0.2
  --select @ShoppingCartCount = count(KeyID) from MemberShoppingCart where MemberID = @MemberID
  select @ShoppingCartCount = sum(Qty) from MemberShoppingCart where MemberID = @MemberID

  set @FavoriteCount = isnull(@FavoriteCount, 0)
  set @ShoppingCartCount = isnull(@ShoppingCartCount, 0)
  return 0
end

GO
