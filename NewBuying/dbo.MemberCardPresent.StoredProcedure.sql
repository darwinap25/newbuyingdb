USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardPresent]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[MemberCardPresent]
  @UserID		        varchar(512),			-- 登录用户
  @CardNumber			varchar(512),			-- 会员卡号
  @TargetMemberMobileNumber	nvarchar(512),       -- 目标用户的手机号。（如果没有用户，则需要新增用户）
  @TargetCardNumber		varchar(512),			-- 目标会员卡号 （调用方需要拿到卡号，如果是新用户，调用方需要创建新卡）   
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),            -- 终端号码
  @ServerCode           varchar(512)='',			-- serverid 暂时用不到,预留
  @BrandCode			varchar(512),			-- 品牌编码
  @TxnNoKeyID           varchar(512),			-- ReceiveTxn 表主键
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间
  @OprID		        int,					-- 操作类型  (6,16,36)
  @ActAmount		    money,					-- 操作金额
  @ActPoint		        int,					-- 操作积分  
  @ActCouponNumbers		nvarchar(max),           -- 操作coupon
  @SecurityCode         varchar(512),			-- 签名
  @Additional           varchar(200),			-- 附加信息
  @Remark				nvarchar(1000),			-- 注释。（或者transfer原因）
  @SourceCardAmount     money output,			-- 转出卡的余额
  @SourceCardPoints     int output,			    -- 转出卡的积分余额
  @TargetCardAmount     money output,			-- 转入卡的余额
  @TargetCardPoints     int output			    -- 转入卡的积分余额  
as
/****************************************************************************
**  Name : MemberCardPresent  
**  Version: 1.0.0.1
**  Description : 转赠是一个卡中的金额或者积分，或coupon转给另外一个卡。业务逻辑和校验写在MemberCardTransfer中, 比如是否必须同一种卡，转入卡不存在，是否要自动新增。
                 
**  Parameter :
  declare @Result int	, @SourceCardAmount money,  @TargetCardAmount money, @SourceCardPoints int, @TargetCardPoints int
  exec @Result = MemberCardPresent '1', '000400001', '0004', '13916423422', '000400002', '0004', 'store1', '001', '01', '201205010100001', '2012050101', '2012-04-26', '2012-04-26', 36, 0, 0, N'''CN0100002'', ''CN0100004'',''CN0100005''',   '', '','',
     @SourceCardAmount output, @TargetCardAmount output, @SourceCardPoints output, @TargetCardPoints output
  print @Result    
  print @SourceCardAmount
  print @SourceCardPoints
  print @TargetCardAmount
  print @TargetCardPoints
 
**  Created by: Gavin @2012-04-01
**  Modify by: Gavin @2012-10-22 （ver 1.0.0.1）不传入Busdate，由存储过程自己获取
**
****************************************************************************/
begin
  declare @CardTypeID int, @TargetCardTypeID int, @ActCouponTypeID int, @MemberID int, @CardGradeID int, @TargetCardGradeID int 

  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
    
  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()       
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120) + cast(@OprID as varchar) + cast(cast(floor(@ActAmount) as int) as varchar)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
    return @CheckResult
  -- end    
  select @CardTypeID = CardTypeID, @CardGradeID = CardGradeID from Card where CardNumber = @CardNumber
  select @TargetCardTypeID = CardTypeID, @TargetCardGradeID = CardGradeID from Card where CardNumber = @TargetCardNumber
  if isnull(@TargetCardNumber, '') = '' and isnull(@TargetMemberMobileNumber, '') <> '' 
  begin
    select @MemberID = MemberID from member where MemberRegisterMobile = @TargetMemberMobileNumber    
    select @TargetCardNumber = CardNumber, @TargetCardTypeID = CardTypeID from Card where MemberID = @MemberID and CardTypeID = @CardTypeID
  end
  if isnull(@CardNumber, '') = ''
    return -2
  if isnull(@TargetCardNumber, '') = ''
    return -2    
  -- 调用方只能选择转赠金额或者积分或者Coupon中任意一个，不能同时填写。
  if isnull(@ActCouponNumbers, '') <> ''
    set @OprID = 36
  else set @OprID = 6  

  exec MemberCardTransfer   @UserID, @CardNumber, @TargetCardNumber, @StoreCode, 
    @RegisterCode, @ServerCode, @BrandCode, @TxnNoKeyID, @TxnNo, @BusDate, @TxnDate, @OprID, @ActAmount, @ActPoint, @ActCouponNumbers, @SecurityCode,
    @Additional, @Remark,  @SourceCardAmount output, @SourceCardPoints output, @TargetCardAmount output, @TargetCardPoints output
  return 0    
end

GO
