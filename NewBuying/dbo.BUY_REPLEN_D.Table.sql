USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_REPLEN_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_REPLEN_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ReplenCode] [varchar](64) NOT NULL,
	[EntityType] [int] NOT NULL,
	[EntityCode] [varchar](64) NOT NULL,
	[MinStockQty] [int] NULL,
	[RunningStockQty] [int] NULL,
	[OrderRoundUpQty] [int] NULL,
 CONSTRAINT [PK_BUY_REPLEN_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_REPLEN_D] ADD  DEFAULT ((0)) FOR [EntityType]
GO
ALTER TABLE [dbo].[BUY_REPLEN_D] ADD  DEFAULT ((1)) FOR [OrderRoundUpQty]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D', @level2type=N'COLUMN',@level2name=N'ReplenCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'决定EntityCode中内容的含义。默认0。如果是0，则只需一条记录即可。
0：所有货品。EntityNum中为空
1：EntityCode内容为 prodcode。 
2：EntityCode内容为 DepartCode。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D', @level2type=N'COLUMN',@level2name=N'EntityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D', @level2type=N'COLUMN',@level2name=N'EntityCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小允许库存。（低于此将可能触发自动补货）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D', @level2type=N'COLUMN',@level2name=N'MinStockQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'正常库存数量。（补货数量为： 正常库存数量 - 当前库存数量）
注：如果有补货的倍数要求，按照倍数要求计算。
比如： 正常库存数量 - 当前库存数量 = 53.   供应商要求按50倍数订货，所以需要订货数量为 100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D', @level2type=N'COLUMN',@level2name=N'RunningStockQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货数量的倍数（包装数量）：
Sample:
max=300 
current=97
max-current=203
203 will be rounded up to 250 since it is the next number divisible by 50.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D', @level2type=N'COLUMN',@level2name=N'OrderRoundUpQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺补货设置。从表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_D'
GO
