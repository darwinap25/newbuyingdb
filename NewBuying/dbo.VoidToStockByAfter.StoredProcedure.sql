USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[VoidToStockByAfter]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VoidToStockByAfter]
  @AfterTxnNo               VARCHAR(64)
AS
/****************************************************************************
**  Name : VoidToStockByAfter   
**  Version: 1.0.0.1
**  Description :  售后单批核后退货到库存。(整单全部void。) 
sp_helptext ReturnToStockByAfter
select * from aftersales_H
Declare @a int
exec @a =ReturnToStockByAfter '601216C002972100'
print @a
**  Created by: Gavin @2017-03-16
**  Modify by: Gavin @2017-04-07 (ver 1.0.0.1) 增加判断，如果拣货单的OrderQty数量都为 0， 那么这个拣货单状态改为 V。 
****************************************************************************/
BEGIN
  DECLARE @StoreCode VARCHAR(64), @StoreID INT, @TxnType INT, @OriginalTxnNo VARCHAR(64), @SalesPickOrderNumber VARCHAR(64),
          @ApproveStatus CHAR(1), @PickupLocation VARCHAR(64), @Status INT, @BusDate DATETIME, @SalesShipOrderNumber VARCHAR(64)
  DECLARE @NewNumber VARCHAR(64)
  
  SET @AfterTxnNo = ISNULL(@AfterTxnNo, '')

  SELECT @StoreCode = CASE WHEN ISNULL(PickupStoreCode,'') <> '' THEN PickupStoreCode ELSE StoreCode END,
      @TxnType = TxnType,  @OriginalTxnNo = RefTransNum
    FROM AfterSales_H H WHERE TxnNo = @AfterTxnNo
  SELECT @StoreID = StoreID FROM BUY_Store WHERE StoreCode = @StoreCode
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC

  IF ISNULL(@StoreID, 0) = 0
    RETURN -1
 
  DECLARE CUR_ReturnToStockByAfter CURSOR FAST_FORWARD FOR
    SELECT SalesPickOrderNumber, ApproveStatus, PickupLocation, B.StoreID FROM Ord_SalesPickOrder_H A
	   LEFT JOIN BUY_STORE B ON A.PickupLocation = B.StoreCode
	  WHERE ReferenceNo = @OriginalTxnNo
  OPEN CUR_ReturnToStockByAfter 
  FETCH FROM CUR_RETURNTOSTOCKBYAFTER INTO @SalesPickOrderNumber, @ApproveStatus, @PickupLocation, @StoreID
  WHILE @@FETCH_STATUS = 0
  BEGIN
      IF @ApproveStatus = 'A' 
        SELECT TOP 1 @Status = Status, @SalesShipOrderNumber = SalesShipOrderNumber FROM Ord_SalesShipOrder_H WHERE ReferenceNo = @SalesPickOrderNumber
	  IF ISNULL(@Status, -1) = 0
	  BEGIN
	    UPDATE  Ord_SalesShipOrder_H SET Status = 4 WHERE ReferenceNo = @SalesPickOrderNumber
	    UPDATE Ord_SalesPickOrder_H SET ApproveStatus = 'P', ApprovalCode = '', UpdatedOn = GETDATE() 
		WHERE SalesPickOrderNumber = @SalesPickOrderNumber
      END
	  
	  IF (@ApproveStatus = 'P') OR  (@ApproveStatus = 'A' AND ISNULL(@Status, 0) = 0)
	  BEGIN
		  DECLARE @PPCODE VARCHAR(64), @PQTY INT, @OQTY INT, @PKeyID INT, @TempQTY INT
		  DECLARE CUR_DeductPickQty CURSOR FAST_FORWARD FOR
			SELECT ProdCode,D.Qty FROM Sales_D D LEFT JOIN Sales_H H ON D.TransNum = H.TransNum 
			WHERE D.TransNum=@OriginalTxnNo
			ORDER BY ProdCode,D.Qty             
		  OPEN CUR_DeductPickQty 
		  FETCH FROM CUR_DeductPickQty INTO @PPCODE, @PQTY
		  WHILE @@FETCH_STATUS = 0
		  BEGIN
		      SET @TempQTY = @PQTY
			  DECLARE CUR_DeductPickQty_UPDATE CURSOR FAST_FORWARD FOR
				SELECT KeyID, OrderQty FROM Ord_SalesPickOrder_D WHERE ProdCode = @PPCODE AND SalesPickOrderNumber = @SalesPickOrderNumber  
				ORDER BY KeyID           
			  OPEN CUR_DeductPickQty_UPDATE 
			  FETCH FROM CUR_DeductPickQty_UPDATE INTO @PKeyID, @OQTY
			  WHILE @@FETCH_STATUS = 0
			  BEGIN
			    IF abs(@TempQTY) > 0
				BEGIN
					IF abs(@TempQTY) <= abs(@OQTY)
					BEGIN
					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'R', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), -ABS(@TempQTY), ISNULL(T.OnhandQty,0) - ABS(@TempQTY), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'R' ) T ON D.ProdCode = T.ProdCode 
					  WHERE D.KeyID = @PKeyID

					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'G', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), ABS(@TempQTY), ISNULL(T.OnhandQty,0) + ABS(@TempQTY), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D 
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'G' ) T ON D.ProdCode = T.ProdCode  	   
					  WHERE D.KeyID = @PKeyID

					  UPDATE Ord_SalesPickOrder_D SET OrderQty = OrderQty - abs(@TempQTY), ActualQty = 0 WHERE KeyID = @PKeyID
					  SET @TempQTY = 0
					END ELSE
					BEGIN

					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'R', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), -ABS(D.OrderQty), ISNULL(T.OnhandQty,0) - ABS(D.OrderQty), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'R' ) T ON D.ProdCode = T.ProdCode 
					  WHERE D.KeyID = @PKeyID

					  INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
						  OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
					  SELECT 5, @StoreID, 'G', D.ProdCode, @OriginalTxnNo, '', H.CreatedBusDate, H.CreatedOn,
							 ISNULL(T.OnhandQty,0), ABS(D.OrderQty), ISNULL(T.OnhandQty,0) + ABS(D.OrderQty), null, '', GETDATE(), H.CreatedBy 
					  FROM Ord_SalesPickOrder_D D 
					  LEFT JOIN Ord_SalesPickOrder_H H ON D.SalesPickOrderNumber = H.SalesPickOrderNumber 
					  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @StoreID AND StockTypeCode = 'G' ) T ON D.ProdCode = T.ProdCode  	   
					  WHERE D.KeyID = @PKeyID

					  UPDATE Ord_SalesPickOrder_D SET OrderQty = 0, ActualQty = 0 WHERE KeyID = @PKeyID
					  SET @TempQTY = abs(@TempQTY) - abs(@OQTY)				
					END 
                END
				FETCH FROM CUR_DeductPickQty_UPDATE INTO @PKeyID, @OQTY
			  END
			  CLOSE CUR_DeductPickQty_UPDATE
			  DEALLOCATE CUR_DeductPickQty_UPDATE

            FETCH FROM CUR_DeductPickQty INTO @PPCODE, @PQTY
		  END
		  CLOSE CUR_DeductPickQty
		  DEALLOCATE CUR_DeductPickQty	
		

	    IF NOT EXISTS(SELECT * FROM Ord_SalesPickOrder_D WHERE SalesPickOrderNumber = @SalesPickOrderNumber AND ISNULL(OrderQty,0) > 0)
		  UPDATE Ord_SalesPickOrder_H SET ApproveStatus = 'V', UpdatedOn = GETDATE()
		    WHERE SalesPickOrderNumber  = @SalesPickOrderNumber
	  	    
	  END 
	  
	  IF @ApproveStatus = 'A' AND ISNULL(@Status, -1) > 0 
      BEGIN   
        -- ver 1.0.0.2 不是直接影响库存。 而是产生收货单，等待批核。  170 是hardcode， 以后再定。
		EXEC GetRefNoString 'RECIO', @NewNumber OUTPUT
		INSERT INTO Ord_ReceiveOrder_H (ReceiveOrderNumber,OrderType,ReceiveType,ReferenceNo,FromStoreID,FromContactName,
			FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
			StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
			ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		SELECT @NewNumber, 1, 2, A.SalesShipOrderNumber, 170, A.Contact,
			A.ContactPhone,A.ContactPhone,'','',C.StoreID,C.Contact,C.ContactPhone,C.StoreEmail,
			C.StoreTel,C.StoreFullDetail1,'Customer return order',@BUSDATE,NULL,'','P',
			NULL,NULL,GETDATE(),A.CreatedBy,GETDATE(),A.UpdatedBy
		FROM Ord_SalesShipOrder_H A  
		  LEFT JOIN Ord_SalesPickOrder_H B ON A.ReferenceNo = B.SalesPickOrderNumber
		  LEFT JOIN BUY_STORE C ON  C.StoreCode = 'ECO' -- B.PickupLocation = C.StoreCode
		WHERE A.SalesShipOrderNumber = @SalesShipOrderNumber
		

		INSERT INTO Ord_ReceiveOrder_D (ReceiveOrderNumber, ProdCode, OrderQty, ReceiveQty, StockTypeCode, Remark)
		SELECT @NewNumber, ProdCode, ABS(OrderQty), ABS(OrderQty), 'G', 'Retutn Sales Auto Gen Receive Order'
		FROM Ord_SalesShipOrder_D 
		where SalesShipOrderNumber = @SalesShipOrderNumber
/*
		INSERT INTO Ord_ReceiveOrder_D (ReceiveOrderNumber, ProdCode, OrderQty, ReceiveQty, StockTypeCode, Remark)
		SELECT @NewNumber, ProdCode, ABS(D.Qty), ABS(D.Qty), 'G', 'Retutn Sales Auto Gen Receive Order'
		FROM Sales_D D LEFT JOIN Sales_H H ON D.TransNum = H.TransNum 
		WHERE D.TransNum=@OriginalTxnNo 
		   and D.prodcode in (select prodcode from Ord_SalesShipOrder_D where SalesShipOrderNumber = @SalesShipOrderNumber)
*/
	  END
	  
	      
      FETCH FROM CUR_RETURNTOSTOCKBYAFTER INTO @SalesPickOrderNumber, @ApproveStatus, @PickupLocation, @StoreID
  END
  CLOSE CUR_ReturnToStockByAfter
  DEALLOCATE CUR_ReturnToStockByAfter

  SET NOCOUNT OFF  
END

GO
