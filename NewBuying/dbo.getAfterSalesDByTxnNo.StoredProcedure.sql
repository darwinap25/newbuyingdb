USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[getAfterSalesDByTxnNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAfterSalesDByTxnNo] 
	@TxnNo  varchar(64)     -- 交易号
AS
/****************************************************************************
**  Name : getAfterSalesDByTxnNo
**  Version: 1.0.0.1   
**  Description : 根据交易号获得此交易的AfterSalesD 数据。
**  Created by: Lisa @2016-01-07
****************************************************************************/
begin
 declare @MemberID int,  @Language int

  select @MemberID=MemberID from  AfterSales_H  where TxnNo = @TxnNo 
  select @Language = MemberDefLanguage from member where MemberID = @MemberID
  set @Language = ISNULL(@Language, 1)

  select D.SeqNo,D.TxnNo,D.TxnType,case D.TxnType when 1 then '维修' when 2 then '退货' else '换货' end TxnTypeName, 
  D.RefSeqNo, D.StoreCode,case CAST(@Language as varchar) when 2 then S.StoreName2  when 3 then S.StoreName3 else S.StoreName1 end as StoreName, 
  D.RegisterCode, D.ProdCode, D.ProdDesc, D.DepartCode, D.Qty,D.UploadPicFile1,D.UploadPicFile2,
  D.UploadPicFile3,D.UploadPicFile4,D.UploadPicFile5
  from AfterSales_D D left join Product P on D.ProdCode = P.ProdCode
  left join Store S on S.StoreCode=D.StoreCode
  where TxnNo = @TxnNo 
  order by SeqNo  
  return 0
end 

GO
