USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BatchGenerateNumber]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchGenerateNumber]
  @UserID               int,
  @Type                 int,				-- 0:卡。 1:Coupon 默认0
  @NumberTypeID		    int,	    -- CardTypeID或者CouponTypeID. CardTypeID时，可以不填
  @CardGradeID	        int,       -- 创建卡的等级， coupon 没有 cardgrade，此处置空
  @StartingNumber		bigint,             -- 批量创建Coupon的初始号码。（当CouponType的 IsImportCouponNumber=1时，@StartingNumber有效，如果此时不输入@StartingNumber，则使用hardcode号码。输入的@StartingNumber必须是整数。）
  @GenQty				int,                -- 创建数量。
  @IssueDate			datetime,           -- 此批次的发行日期
  @InitAmount			money,              -- 此批次的初始金额。 默认0
  @InitPoints           int,                -- 此批次的初始积分   默认0
  @RandomPWD			int,				-- 1:随机密码。 0: 不是。
  @InitPWD				varchar(512),        -- 初始密码
  @ExpiryDate			datetime,           -- 此批次的有效日期   默认0. (如果是0或者Null，则按照CardType或者CouponType的设置)   
  @RefTxnNo varchar(512),			    -- 参考编号，一般填入原始单号
  @ApprovalCode varchar(512), 			    -- Approval号
  @Createdby int, 				    -- 创建人ID
  @ReturnBatchID        varchar(30) output, -- 返回的批次号
  @ReturnStartNumber    nvarchar(30) output,-- 返回的起始号码
  @ReturnEndNumber      nvarchar(30) output, -- 返回的结束号码
  @ImportBatchCode		varchar(60) = '',     -- 传入的batchcardcode或者batchcouponcode
  @InitStatus           int = 0,			    -- 此批次的初始状态。
  @InitStockStatus      int = 0           -- 此批次的初始库存状态,
  --@CouponNatureID       int                --Coupon.CouponNatureID 
AS
/****************************************************************************
**  Name : BatchGenerateNumber
**  Version: 1.0.0.5
**  Description : 大数据量创建Coupon/card
  declare @A int, @ReturnBatchID        varchar(30), @ReturnStartNumber    nvarchar(30), @ReturnEndNumber      nvarchar(30)
  exec @A = BatchGenerateNumber 1, 0, 8, 3, null, 500000, '2014-08-21', 10000, 0, 0, '', 0, '','', 1, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output,
    '', 1, 0
  print @A
  print @ReturnBatchID
  print @ReturnStartNumber
  print  @ReturnEndNumber 
  select * from card where createdon > '2014-12-30'
  select * from cardgrade
  update cardtype set cardnummask = '', cardnumpattern = '' where cardtypeid = 8
  select* from cardtype where cardgradeid = 3
**  Created by:  Gavin @2014-08-30
**  Modify by:  Gavin @2014-09-22 (ver 1.0.0.2) @ReturnBatchID 返回第一个batchid，@ReturnStartNumber 返回第一个card号， @ReturnEndNumber 返回最后一个卡号 
**  Modify by:  Gavin @2014-10-16 (ver 1.0.0.3) 增加输入参数@InitStockStatus
**  Modify by:  Gavin @2015-01-05 (ver 1.0.0.4) 在调用过程中 分割多次执行. 可以通过设置@MaxQty数量来调节。(大数据量时，会延长执行速度，但降低服务器负载) 
**  Modify by:  Darwin @2017-09-09 (ver 1.0.0.5) @CouponNatureID for SP BatchGenerateNumber_Base
****************************************************************************/
begin
  declare @A int, @RBatchID varchar(30), @RStartNumber nvarchar(30), @REndNumber nvarchar(30)
  declare @MaxQty int, @TempQty int, @DoQty int, @TempStartingNumber varchar(64)
  set @ReturnBatchID = ''
  set @ReturnStartNumber = ''
  set @ReturnEndNumber = ''
  set nocount on
  
  set @TempStartingNumber = isnull(@StartingNumber,'')
  set @MaxQty = 1000000
  set @TempQty = @GenQty
  while @TempQty > 0 
  begin
    if @TempQty > @MaxQty
    begin
      set @DoQty = @MaxQty
      set @TempQty = @TempQty - @MaxQty
    end else
    begin
      set @DoQty = @TempQty
      set @TempQty = 0  
    end  
--    begin tran     
      exec @A = BatchGenerateNumber_Base @UserID,@Type,@NumberTypeID,@CardGradeID,@TempStartingNumber, @DoQty,
      @IssueDate,@InitAmount,@InitPoints,@RandomPWD,@InitPWD,@ExpiryDate,@RefTxnNo,@ApprovalCode,@Createdby,
      @RBatchID output,@RStartNumber output,@REndNumber OUTPUT,@ImportBatchCode,@InitStatus, @InitStockStatus,0--, @CouponNatureID
      if @ReturnBatchID = ''
        set @ReturnBatchID = @RBatchID
      if @ReturnStartNumber = '' 
        set @ReturnStartNumber = @RStartNumber
      set @ReturnEndNumber = @REndNumber      
--    commit tran
    set @TempStartingNumber = ''
  end
end





GO
