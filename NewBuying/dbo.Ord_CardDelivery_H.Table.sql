USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardDelivery_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardDelivery_H](
	[CardDeliveryNumber] [varchar](64) NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[BrandID] [int] NULL,
	[FromStoreID] [int] NULL,
	[StoreID] [int] NULL,
	[CustomerType] [int] NULL,
	[CustomerID] [int] NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[FromAddress] [nvarchar](512) NULL,
	[StoreContactName] [nvarchar](512) NULL,
	[StoreContactPhone] [nvarchar](512) NULL,
	[StoreContactEmail] [nvarchar](512) NULL,
	[StoreMobile] [nvarchar](512) NULL,
	[FromContactName] [nvarchar](512) NULL,
	[FromContactNumber] [nvarchar](512) NULL,
	[FromEmail] [nvarchar](512) NULL,
	[FromMobile] [nvarchar](512) NULL,
	[NeedActive] [int] NULL,
	[Remark] [varchar](512) NULL,
	[Remark1] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_CARDDELIVERY_H] PRIMARY KEY CLUSTERED 
(
	[CardDeliveryNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CardDelivery_H] ADD  DEFAULT ((0)) FOR [NeedActive]
GO
ALTER TABLE [dbo].[Ord_CardDelivery_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CardDelivery_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CardDelivery_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CardDelivery_H] ON [dbo].[Ord_CardDelivery_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CardDelivery_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2015-09-25
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @ordernumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @ordernumber = CardDeliveryNumber FROM INSERTED  
  exec GenUserMessage @CreatedBy, 23, 0, @ordernumber  
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CardDelivery_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CardDelivery_H] ON [dbo].[Ord_CardDelivery_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CardDelivery_H
* Version: 1.0.0.2
* Description : delivery order 的触发器, 目的为了填写approvalcode (根据Update_Ord_CouponDelivery_H （1.0.0.2))
* Create By Gavin @2014-10-09
* Modify By Gavin @2014-11-13 (ver 1.0.0.1) void时，货品退回到总部， dormant- good for release 状态
* Modify By Gavin @2014-11-13 (ver 1.0.0.2) void时，修正bug
*/
/*==============================================================*/
BEGIN  
  declare @CardDeliveryNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6),
          @NeedActive int, @FromStoreID int
  declare @FirstCard varchar(64), @EndCard varchar(64), @CardTypeID int, @ActExpireDate datetime, @NewCardExpiryDate datetime
  declare @BusDate datetime, @TxnDate datetime, @StoreID int, @CardGradeID int
  declare @NewAmountExpiryDate datetime, @NewPointExpiryDate datetime, @IsMultiExpiry int
   
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()
    
  DECLARE Update_Ord_CardDelivery_H CURSOR fast_forward FOR
    SELECT FromStoreID, CardDeliveryNumber, ApproveStatus, CreatedBy, NeedActive, StoreID FROM INSERTED
  OPEN Update_Ord_CardDelivery_H
  FETCH FROM Update_Ord_CardDelivery_H INTO @FromStoreID, @CardDeliveryNumber, @ApproveStatus, @CreatedBy, @NeedActive, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CardDeliveryNumber = @CardDeliveryNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
    
      -- active 发送的card 
      DECLARE CUR_CardDelivery_Detail CURSOR fast_forward for
          select FirstCardNumber, EndCardNumber, CardTypeID, CardGradeID from Ord_CardDelivery_D 
            where CardDeliveryNumber = @CardDeliveryNumber
	    OPEN CUR_CardDelivery_Detail
      FETCH FROM CUR_CardDelivery_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
      WHILE @@FETCH_STATUS=0 
      BEGIN
        update Card set StockStatus = 6, IssueStoreID = @StoreID
            where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardNumber >= @FirstCard and CardNumber <= @EndCard 
          if @NeedActive = 1
          begin        
            select top 1 @ActExpireDate = cardexpirydate from card where CardNumber = @FirstCard
            exec CalcExpiryDate 21, @FirstCard, 0, @NewCardExpiryDate output, 
                      @NewAmountExpiryDate output, @NewPointExpiryDate output, @IsMultiExpiry output            
            insert into Card_Movement
              (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
               CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	             OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
            select 21, CardNumber, null, null, @CardDeliveryNumber, TotalAmount, 0, TotalAmount, 0, 
               @BusDate, @TxnDate, null, null, '', '', '', @CreatedBy, @StoreID, '', '', 
               TotalPoints, TotalPoints, @ActExpireDate, @NewCardExpiryDate, status, 2
              from Card where CardTypeID = @CardTypeID and CardNumber >= @FirstCard and CardNumber <= @EndCard 
          end      
	      FETCH FROM CUR_CardDelivery_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
    	END
      CLOSE CUR_CardDelivery_Detail 
      DEALLOCATE CUR_CardDelivery_Detail 

      exec ChangeCardStockStatus 7, @CardDeliveryNumber, 1    
      update Ord_CardDelivery_H set ApprovalCode = @ApprovalCode where CardDeliveryNumber = @CardDeliveryNumber      
    end
    
    if @ApproveStatus = 'V' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output        
      -- rollback 发送的card 
      DECLARE CUR_CardDelivery_Detail CURSOR fast_forward for
          select FirstCardNumber, EndCardNumber, CardTypeID, CardGradeID 
            from Ord_CardDelivery_D where CardDeliveryNumber = @CardDeliveryNumber
	    OPEN CUR_CardDelivery_Detail
      FETCH FROM CUR_CardDelivery_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
      WHILE @@FETCH_STATUS=0 
      BEGIN 
        update Card set Status= 0, StockStatus = 2, IssueStoreID = @FromStoreID
            where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and CardNumber >= @FirstCard and CardNumber <= @EndCard       
        if @NeedActive = 1
        begin       
          select top 1 @ActExpireDate = cardexpirydate from card where cardtypeid = @CardTypeID and CardNumber = @FirstCard
            exec CalcExpiryDate 29, @FirstCard, 0, @ActExpireDate output, 
                      @NewAmountExpiryDate output, @NewPointExpiryDate output, @IsMultiExpiry output            
            insert into Card_Movement
              (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
               CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	             OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
            select 29, CardNumber, null, null, @CardDeliveryNumber, TotalAmount, 0, TotalAmount, 0, 
               @BusDate, @TxnDate, null, null, '', '', '', @CreatedBy, @StoreID, '', '', 
               TotalPoints, TotalPoints, @ActExpireDate, @NewCardExpiryDate, status, 0
              from Card where CardTypeID = @CardTypeID and CardNumber >= @FirstCard and CardNumber <= @EndCard 
        end      
	      FETCH FROM CUR_CardDelivery_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
    	END
      CLOSE CUR_CardDelivery_Detail 
      DEALLOCATE CUR_CardDelivery_Detail
                 
      update Ord_CardDelivery_H set ApprovalCode = @ApprovalCode where CardDeliveryNumber = @CardDeliveryNumber      
    end
    exec ChangeCardStockStatus 7, @CardDeliveryNumber, 2    
    FETCH FROM Update_Ord_CardDelivery_H INTO @FromStoreID, @CardDeliveryNumber, @ApproveStatus, @CreatedBy, @NeedActive, @StoreID  
  END
  CLOSE Update_Ord_CardDelivery_H
  DEALLOCATE Update_Ord_CardDelivery_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'CardDeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。指coupon拣货单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源店铺，一般可能是总部' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送达店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户类型。1：客戶訂貨。 2：店鋪訂貨' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'CustomerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'customer表 主键， 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'FromContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户收货确认后是否激活。0：不激活。1：激活。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'NeedActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare（送货中）。  A:Approve（送货完成，顾客签收） 。 V：Void（顾客拒收，card回到dormant状态）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡送货单主表。
（从批核后的拣货单复制过来，过滤了拣货数量为0的记录。 头表状态略有不同）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardDelivery_H'
GO
