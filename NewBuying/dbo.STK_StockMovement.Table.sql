USE [NewBuying]
GO
/****** Object:  Table [dbo].[STK_StockMovement]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STK_StockMovement](
	[KeyID] [bigint] IDENTITY(1,1) NOT NULL,
	[OprID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[StockTypeCode] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[ReferenceNo_Other] [varchar](64) NULL,
	[BusDate] [datetime] NULL,
	[TxnDate] [datetime] NULL DEFAULT (getdate()),
	[OpenQty] [int] NOT NULL DEFAULT ((0)),
	[ActQty] [int] NOT NULL DEFAULT ((0)),
	[CloseQty] [int] NOT NULL DEFAULT ((0)),
	[SerialNoType] [int] NULL,
	[SerialNo] [varchar](64) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_STK_STOCKMOVEMENT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_STK_StockMovement]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_STK_StockMovement] ON [dbo].[STK_StockMovement]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Update_STK_StockMovement
* Version: 1.0.0.0
* Description :
     select * from STK_StockMovement
     select * from BUY_STOCKTYPE
     update Ord_StockAdjust_H set approvestatus = 'A' where StoreOrderNumber = 'COPO00000000012'
* Create By Gavin @2015-03-10
*/
/*==============================================================*/
BEGIN  
  DECLARE @OprID INT,@StoreID INT,@StockTypeCode VARCHAR(64),@ProdCode VARCHAR(64),@ReferenceNo VARCHAR(64),@ReferenceNo_Other VARCHAR(64),
       @BusDate DATETIME,@TxnDate DATETIME,@OpenQty INT,@ActQty INT,@CloseQty INT,@SerialNo VARCHAR(64),@ApprovalCode VARCHAR(64),
       @CreatedOn DATETIME,@CreatedBy INT,@SerialNoType INT
  
  DECLARE CUR_STK_StockMovement CURSOR fast_forward FOR
    SELECT OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty,ActQty,CloseQty,SerialNoType,SerialNo,ApprovalCode,CreatedOn,CreatedBy 
      FROM INSERTED WHERE ActQty <> 0          
  OPEN CUR_STK_StockMovement
  FETCH FROM CUR_STK_StockMovement INTO @OprID,@StoreID,@StockTypeCode,@ProdCode,@ReferenceNo,@ReferenceNo_Other,
       @BusDate,@TxnDate,@OpenQty,@ActQty,@CloseQty,@SerialNoType,@SerialNo,@ApprovalCode,@CreatedOn,@CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    IF @SerialNo <> ''
    BEGIN
      IF @ActQty < 0
        DELETE FROM STK_StocktOnhand_SN WHERE StoreID = @StoreID AND StockTypeCode = @StockTypeCode AND ProdCode = @ProdCode
      ELSE IF @ActQty > 0
      BEGIN
        IF NOT EXISTS(SELECT *  FROM STK_StocktOnhand_SN WHERE StoreID = @StoreID AND StockTypeCode = @StockTypeCode AND ProdCode = @ProdCode)
          INSERT INTO STK_StocktOnhand_SN(SerialNo, SerialNoType, ProdCode, StoreID, StockTypeCode)
          VALUES(@SerialNo, @SerialNoType,@ProdCode,@StoreID,@StockTypeCode)
      END        
    END
    
    IF EXISTS(SELECT * FROM STK_StockOnhand WHERE StoreID = @StoreID AND StockTypeCode = @StockTypeCode AND ProdCode = @ProdCode)
    BEGIN
      UPDATE STK_StockOnhand SET OnhandQty = OnhandQty + @ActQty, UpdatedBy = @CreatedBy, UpdatedOn = GETDATE()
        WHERE StoreID = @StoreID AND StockTypeCode = @StockTypeCode AND ProdCode = @ProdCode
    END
    ELSE
    BEGIN
      INSERT INTO STK_StockOnhand (StoreID, StockTypeCode, ProdCode, OnhandQty, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn)
      VALUES(@StoreID, @StockTypeCode, @ProdCode, @ActQty, @CreatedBy, GETDATE(), @CreatedBy, GETDATE())
    END
    FETCH FROM CUR_STK_StockMovement INTO @OprID,@StoreID,@StockTypeCode,@ProdCode,@ReferenceNo,@ReferenceNo_Other,
         @BusDate,@TxnDate,@OpenQty,@ActQty,@CloseQty,@SerialNoType,@SerialNo,@ApprovalCode,@CreatedOn,@CreatedBy  
  END
  CLOSE CUR_STK_StockMovement 
  DEALLOCATE CUR_STK_StockMovement  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_Store表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_StockType表 Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关交易号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他相关交易号。（如果有的话）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'ReferenceNo_Other'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期时间。（system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作前，期初在库数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'OpenQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作数量。（有正负）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'ActQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行此操作后，同类货品的剩余在库数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'CloseQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SerialNo的类型
0： 没有
1： IMSI
2： MSISDN
3： SIM' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'SerialNoType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品的serialno。如果有serialno， actqty应该为1.  大于1时需要拆开为多条记录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'SerialNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品库存变动表。（流水表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_StockMovement'
GO
