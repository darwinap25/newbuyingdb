USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCreateMember]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DoCreateMember]
  @CreateMemberNumber               varchar(64),
  @MemberRandomPWD			        int=0,				    -- 会员密码.   1:随机密码(6位数字)。 0: 不是。默认0
  @MemberInitPWD				    varchar(512)='',        -- 会员密码的初始密码.	默认空
  @ReturnApprovalCode				varchar(6) output,
  @ErrorCode                        int output
AS
/****************************************************************************
**  Name : DoCreateMember 
**  Version: 1.0.0.2
**  Description : Ord_CreateMember_H单据批核, 创建member记录
**  Parameter :
**
  declare @MemberID varchar(36), @A int,   @ReturnApprovalCode varchar(6)  ,
  @ErrorCode int
  exec @A = DoCreateMember 'MEI000000000025', 0, '', @ReturnApprovalCode output,   @ErrorCode output
  print @A
  print @MemberID
  select * from Ord_CreateMember_D
**  Created by: Gavin @2013-09-03
**  Modify by: Gavin @2013-09-11 (ver 1.0.0.1) 增加返回Errorcode（为方便UI接收）。需要绑定卡时，先检查是否有足够的空白卡数
**  Modify by: Gavin @2013-09-16 (ver 1.0.0.2) 修正membermessageaccount 中手机账号没有国家码的问题
**
****************************************************************************/
begin
  declare @ApproveStatus char(1), @CreatedBy int, @ApprovalCode char(6), @MemberID int
  declare @CountryCode varchar(64), @MobileNumber varchar(64), @EngFamilyName varchar(64), @EngGivenName varchar(64), 
          @ChiFamilyName varchar(64), @ChiGivenName varchar(64), @Birthday datetime, @Gender int, @HomeAddress varchar(512), 
          @Email varchar(64), @FaceBook varchar(64), @QQ varchar(64), @MSN varchar(64), @Weibo varchar(64), @OtherContact varchar(64)
  declare @MemberRegisterMobile varchar(64), @BindCardFlag int, @CardTypeID int, @CardGradeID int,
          @BusDate datetime, @TxnDate datetime
  declare @MemberCount int, @CardCount int
  declare @MemberPWD varchar(512)
  declare @MemberMsgTable table(MemberID int, MemberRegisterMobile varchar(64), MemberPassword varchar(512)) 
  
  set @ErrorCode = 0
  set @MemberRandomPWD = isnull(@MemberRandomPWD, 0)
  set @MemberInitPWD = isnull(@MemberInitPWD, '')

  select @BindCardFlag = isnull(BindCardFlag, 0), @ApproveStatus = ApproveStatus, @CreatedBy = CreatedBy,
         @CardTypeID = CardTypeID, @CardGradeID = CardGradeID 
    from Ord_CreateMember_H where CreateMemberNumber = @CreateMemberNumber

  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@BusDate, 0) = 0
    set @BusDate = getdate()    
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
  
  -- 如果绑定卡，先检查是否有足够的空卡
  if @BindCardFlag = 1 
  begin
    select @MemberCount = count(*) from Ord_CreateMember_D 
      where CreateMemberNumber = @CreateMemberNumber 
    select @CardCount = count(*) from Card 
      where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and Status in (0,1) and isnull(MemberID, 0) = 0  
    if @MemberCount > @CardCount 
    begin
	  set @ErrorCode = -102
	  return @ErrorCode
    end                   
  end
        
  if isnull(@ApproveStatus, '') = 'P'
  begin
  begin tran
    DECLARE CUR_DoCreateMember CURSOR fast_forward FOR
      	select  RTrim(CountryCode), RTrim(MobileNumber), EngFamilyName,EngGivenName, ChiFamilyName, 
	            ChiGivenName, Birthday, Gender, HomeAddress, Email, FaceBook, QQ, MSN, Weibo, OtherContact	    
	       from Ord_CreateMember_D where CreateMemberNumber = @CreateMemberNumber
    OPEN CUR_DoCreateMember
    FETCH FROM CUR_DoCreateMember INTO @CountryCode, @MobileNumber, @EngFamilyName, @EngGivenName, @ChiFamilyName, 
	            @ChiGivenName, @Birthday, @Gender, @HomeAddress, @Email, @FaceBook, @QQ, @MSN, @Weibo, @OtherContact
    WHILE @@FETCH_STATUS=0
    BEGIN
      set @MemberRegisterMobile = RTrim(@CountryCode) + RTrim(@MobileNumber)
      set @MemberPWD = ''
      if @MemberRandomPWD = 1
      begin
        exec GenRandomPassword '', '', 6, 0, @MemberPWD output, @MemberInitPWD output
      end  else
      begin
		if @MemberInitPWD <> ''
		begin
		  set @MemberPWD = @MemberInitPWD
		  set @MemberInitPWD = dbo.EncryptMD5(@MemberInitPWD)
		end  
      end
      
	  insert into Member
	    (MemberRegisterMobile, CountryCode, MemberMobilePhone, MemberEngFamilyName,MemberEngGivenName, MemberChiFamilyName, 
	     MemberChiGivenName, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth, 
	     MemberSex, HomeAddress, MemberEmail, MemberPassword)      
	  values
	    (@MemberRegisterMobile, @CountryCode, @MobileNumber, @EngFamilyName, @EngGivenName, @ChiFamilyName, 
	     @ChiGivenName, @Birthday, datepart(dd, @Birthday), datepart(mm, @Birthday), datepart(yy, @Birthday), 
	     @Gender, @HomeAddress, @Email, @MemberInitPWD)
	  set @MemberID = SCOPE_IDENTITY() 
	  
	  if isnull(@MemberID, 0) = 0
	  begin
        CLOSE CUR_DoCreateMember 
        DEALLOCATE CUR_DoCreateMember  	
	    set @ErrorCode = -1
	    rollback tran
	    return @ErrorCode          
	  end
	  
      insert into @MemberMsgTable(MemberID, MemberRegisterMobile, MemberPassword)
      values(@MemberID, @MemberRegisterMobile, @MemberPWD)
        	  
	  if isnull(@Email, '') <> ''
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 2, @MemberRegisterMobile, 0, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy)
	  else
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 2, @MemberRegisterMobile, 1, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy)
        
      if isnull(@Email, '') <> ''
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 1, @Email, 1, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy)
      if isnull(@FaceBook, '') <> ''
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 7, @FaceBook, 0, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy)
      if isnull(@QQ, '') <> ''
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 5, @QQ, 0, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy)
      if isnull(@MSN, '') <> ''
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 4, @MSN, 0, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy)
      if isnull(@Weibo, '') <> ''
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 8, @Weibo, 0, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy) 
      if isnull(@OtherContact, '') <> ''
        insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        values (@MemberID, 99, @OtherContact, 0, 1, '', Getdate(), @CreatedBy, Getdate(), @CreatedBy)         
                                   
      FETCH FROM CUR_DoCreateMember INTO @CountryCode, @MobileNumber, @EngFamilyName, @EngGivenName, @ChiFamilyName, 
	            @ChiGivenName, @Birthday, @Gender, @HomeAddress, @Email, @FaceBook, @QQ, @MSN, @Weibo, @OtherContact

    END
    CLOSE CUR_DoCreateMember 
    DEALLOCATE CUR_DoCreateMember  
    
    -- 更新单据状态
    exec GenApprovalCode @ApprovalCode output 
    update Ord_CreateMember_H set ApprovalCode = @ApprovalCode, ApproveStatus = 'A', ApproveOn = GetDate(),  
         ApproveBy = @CreatedBy, ApproveBusDate =  @BusDate
      where CreateMemberNumber = @CreateMemberNumber 
    set @ReturnApprovalCode = @ApprovalCode
    
    -- 绑定卡
    if @BindCardFlag = 1 
    begin   
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      select 26, B.CardNumber, null, 0, @CreateMemberNumber, 0, 0, 0, 0, @BusDate, @TxnDate, 
             null, null, C.MemberID, null, '', C.MemberID, null, null, null,
             0, 0,  B.CardExpiryDate, B.CardExpiryDate, B.Status, 2   
        from (select ROW_NUMBER() OVER(order by KeyID) as IID, RTrim(CountryCode) + RTrim(MobileNumber) as RegisterNum, CountryCode, MobileNumber from Ord_CreateMember_D 
                where CreateMemberNumber = @CreateMemberNumber ) A
        left join (select ROW_NUMBER() OVER(order by CardNumber) as IID, CardNumber, CardExpiryDate, Status 
                     from Card where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID 
                                     and Status in (0,1) and isnull(MemberID, 0) = 0 ) B  on A.IID = B.IID
        left join Member C on A.RegisterNum = C.MemberRegisterMobile             
    end
  commit tran  										
  end
  
  select * from @MemberMsgTable      
  return @ErrorCode
end

GO
