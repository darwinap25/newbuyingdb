USE [NewBuying]
GO
/****** Object:  Table [dbo].[StoreCondition_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreCondition_H](
	[StoreConditionID] [int] IDENTITY(1,1) NOT NULL,
	[StoreConditionDesc1] [nvarchar](512) NULL,
	[StoreConditionDesc2] [nvarchar](512) NULL,
	[StoreConditionDesc3] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_STORECONDITION_H] PRIMARY KEY CLUSTERED 
(
	[StoreConditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[StoreCondition_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[StoreCondition_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreCondition_H', @level2type=N'COLUMN',@level2name=N'StoreConditionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreCondition_H', @level2type=N'COLUMN',@level2name=N'StoreConditionDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreCondition_H', @level2type=N'COLUMN',@level2name=N'StoreConditionDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreCondition_H', @level2type=N'COLUMN',@level2name=N'StoreConditionDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺限制条件表。（通用表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreCondition_H'
GO
