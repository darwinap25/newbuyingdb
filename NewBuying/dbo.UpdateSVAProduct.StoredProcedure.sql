USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateSVAProduct]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[UpdateSVAProduct]
AS
/****************************************************************************
**  Name : CloseSVASalesOrder   
**  Version: 1.0.0.0
**  Description :  用LinkServer的方式，在Buying DB 更新 SVA DB 的Product数据
select * from aftersales_H
Declare @a int
exec @a =UpdateSVAProduct 
print @a
**  Created by: Gavin @2016-12-14
****************************************************************************/
BEGIN
  UPDATE Product SET IsOnlineSKU = CASE WHEN B.IsOnlineSKU IS NULL THEN A.IsOnlineSKU ELSE B.IsOnlineSKU END,
    HotSaleFlag = CASE WHEN B.HotSale IS NULL THEN A.HotSaleFlag ELSE B.HotSale END,
	Flag1 = CASE WHEN B.Feature IS NULL THEN A.Flag1 ELSE B.Feature END
  FROM Product A LEFT JOIN ProductModifyTemp B ON A.ProdCode = B.ProdCode
  WHERE B.ProdCode IS NOT NULL

  UPDATE BUY_RPRICE_M SET Price = CASE WHEN B.Price IS NULL THEN A.Price ELSE B.Price END,
    RefPrice = CASE WHEN B.RefPrice IS NULL THEN A.RefPrice ELSE B.RefPrice END
  FROM BUY_RPRICE_M A LEFT JOIN ProductModifyTemp B ON A.ProdCode = B.ProdCode
  WHERE B.ProdCode IS NOT NULL

  truncate table ProductModifyTemp
  -- 如果InitProductStyle_List 耗时影响比较大,可以删除, 放在空闲时间运行.
  exec InitProductStyle_List

  return 0
END

GO
