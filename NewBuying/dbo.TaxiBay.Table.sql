USE [NewBuying]
GO
/****** Object:  Table [dbo].[TaxiBay]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaxiBay](
	[BayCode] [varchar](64) NOT NULL,
	[BayName] [varchar](64) NULL,
	[BayType] [int] NULL,
 CONSTRAINT [PK_TAXIBAY] PRIMARY KEY CLUSTERED 
(
	[BayCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'站牌code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TaxiBay', @level2type=N'COLUMN',@level2name=N'BayCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'站牌名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TaxiBay', @level2type=N'COLUMN',@level2name=N'BayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'站牌类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TaxiBay', @level2type=N'COLUMN',@level2name=N'BayType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车站牌' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TaxiBay'
GO
