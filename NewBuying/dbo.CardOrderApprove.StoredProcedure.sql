USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CardOrderApprove]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[CardOrderApprove]
  @UserID              int,            -- 批核人员
  @OrderType           int,            --订单类型。 1： Ord_CardReceive_H（供应商订单的收货） 2:Ord_CardReturn_H (店铺订单和退单的收货)
  @OrderNumber         varchar(64)     --单号
AS
/****************************************************************************
**  Name : CardOrderApprove
**  Version: 1.0.0.4
**  Description : 批核实体Card的订单。根据 CouponOrderApprove (1.0.0.4) 改写
**  example :

**  Created by: Gavin @2014-10-09
**
****************************************************************************/
begin
  declare @FirstNumber varchar(64), @EndNumber varchar(64), @Qty int, @CardTypeID int, @CardGradeID int, @StoreID int
  declare @ReceiveType int, @StockStatus int, @CardReceiveNumber varchar(64), @ReferenceNo varchar(64)
  declare @NewReceiveNumber varchar(64), @CardStockStatus int, @CardNoPickQty int, @CardGoodQty int, @CardBadQty int
  
  if @OrderType = 1      -- 收货单批核 
  begin
    select @ReceiveType = ReceiveType, @ReferenceNo = ReferenceNo, @StoreID = StoreID 
      from Ord_CardReceive_H where CardReceiveNumber = @OrderNumber
    
    if @ReceiveType = 1   -- 来自供应商的订单收货
    begin                   
      -- 如果有未定货(CardStockStatus = 0)，则重新并且是供应商订单，则重新产生收货单。
      if exists(select * from Ord_CardReceive_D where CardReceiveNumber = @OrderNumber and CardStockStatus = 1)
      begin
        if exists(select * from Ord_OrderToSupplier_Card_H where OrderSupplierNumber = @ReferenceNo)
        begin
          exec GetRefNoString 'CRECCA', @NewReceiveNumber output
          insert into Ord_CardReceive_H(CardReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
               SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
               StoreEmail, StoreMobile, ApproveStatus, Remark,   
               CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ReceiveType, ordertype)
          select @NewReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
              SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
              StoreEmail, StoreMobile, 'P', Remark,   
              CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, 1, ordertype
            from Ord_CardReceive_H where CardReceiveNumber = @OrderNumber
  
          insert into Ord_CardReceive_D (CardReceiveNumber, CardTypeID, CardGradeID, Description, OrderQty, ActualQty, FirstCardNumber, EndCardNumber, BatchCardCode, CardStockStatus, ReceiveDateTime)   
          select @NewReceiveNumber, CardTypeID, CardGradeID, '', OrderQty, OrderQty, FirstCardNumber, EndCardNumber, BatchCardCode, 1, GETDATE()
          from Ord_CardReceive_D where  CardReceiveNumber = @OrderNumber and CardStockStatus = 1
          
          -- ver 1.0.0.4
          delete from Ord_CardReceive_D where  CardReceiveNumber = @OrderNumber and CardStockStatus = 1
          
        end
      end
      ----------------------------------                    
    end         
  end else if @OrderType = 2
  begin
    exec GetRefNoString 'CRECCA', @CardReceiveNumber output
    select @StoreID = FromStoreID from Ord_CardReturn_H where CardReturnNumber = @OrderNumber
    select @CardGoodQty = count(*) from Ord_CardReturn_D where CardReturnNumber = @OrderNumber
     
    select top 1 @CardTypeID = CardTypeID From Ord_CardReturn_D  where CardReturnNumber = @OrderNumber 
    insert into Ord_CardReceive_H(CardReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ReceiveType, ordertype)
      select @CardReceiveNumber, CardReturnNumber, StoreID, FromStoreID, SendAddress, FromAddress, 
          FromContactName, FromContactNumber, FromEmail, FromMobile, StoreContactName, StoreContactPhone,
          StoreContactEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, 2, 0
     from Ord_CardReturn_H where CardReturnNumber = @OrderNumber
  
    insert into Ord_CardReceive_D (CardReceiveNumber, CardTypeID, CardGradeID, Description, OrderQty, ActualQty, FirstCardNumber, EndCardNumber, BatchCardCode, CardStockStatus, ReceiveDateTime)   
    select @CardReceiveNumber, CardTypeID, CardGradeID, '', OrderQty, OrderQty, FirstCardNumber, EndCardNumber, BatchCardCode, 2, GETDATE()
     from Ord_CardReturn_D where CardReturnNumber = @OrderNumber 
  end    
  
end

GO
