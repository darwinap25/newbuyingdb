USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_STORE]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_STORE](
	[StoreID] [int] IDENTITY(1,1) NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[Status] [int] NULL DEFAULT ((1)),
	[StoreName1] [nvarchar](512) NULL,
	[StoreName2] [nvarchar](512) NULL,
	[StoreName3] [nvarchar](512) NULL,
	[StoreTypeID] [int] NULL,
	[BankCode] [varchar](64) NULL,
	[StoreBrandCode] [varchar](64) NULL,
	[StoreCountry] [varchar](64) NULL,
	[StoreProvince] [varchar](64) NULL,
	[StoreCity] [varchar](64) NULL,
	[StoreDistrict] [varchar](64) NULL,
	[StoreAddressDetail1] [varchar](512) NULL,
	[StoreAddressDetail2] [varchar](512) NULL,
	[StoreAddressDetail3] [varchar](512) NULL,
	[StoreFullDetail1] [nvarchar](512) NULL,
	[StoreFullDetail2] [nvarchar](512) NULL,
	[StoreFullDetail3] [nvarchar](512) NULL,
	[StoreTel] [varchar](512) NULL,
	[StoreFax] [varchar](512) NULL,
	[StoreEmail] [nvarchar](512) NULL,
	[Contact] [nvarchar](512) NULL,
	[ContactPhone] [nvarchar](512) NULL,
	[StoreLongitude] [varchar](512) NULL,
	[StoreLatitude] [varchar](512) NULL,
	[StorePicFile] [nvarchar](512) NULL,
	[MapsPicFile] [nvarchar](512) NULL,
	[MapsPicShadowFile] [nvarchar](512) NULL,
	[LocationCode] [int] NULL,
	[StoreNote] [nvarchar](512) NULL,
	[StoreOpenTime] [varchar](512) NULL,
	[StoreCloseTime] [varchar](512) NULL,
	[Comparable] [int] NULL DEFAULT ((0)),
	[GLCode] [varchar](64) NULL,
	[RegionCode] [varchar](64) NULL,
	[OrgCode] [varchar](64) NULL,
	[StoreIP] [varchar](64) NULL,
	[SubInvCode] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_STORE] PRIMARY KEY CLUSTERED 
(
	[StoreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺状态。0：关闭。1：开放' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型ID：
1: Retail
2: Road show
3: CC
4: CC support
5: RAN
6: Commercial BU

增加： @2015-03-31
0、总部。（虚拟。 店铺类型为总部时，表示适用于所有店铺）
9、仓库' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'BankCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺的运营商品牌code（Brand->StoreBrandCode）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreBrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺所在国家（存放Country表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreCountry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺所在省 （存放Province表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreProvince'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺所在城市 （存放City表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreCity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺地址所在区县 （存放District表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreDistrict'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺详细地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreAddressDetail1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺详细地址（第二种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreAddressDetail2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺详细地址（第三种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreAddressDetail3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺完整地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreFullDetail1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺完整地址（第二种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreFullDetail2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺完整地址（第三种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreFullDetail3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreTel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreFax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺坐标，经度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreLongitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺坐标，纬度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreLatitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺图片地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StorePicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺图标文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'MapsPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'阴影图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'MapsPicShadowFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区域Code，外键。一个店铺 只能属于一个 区域。（  最下层的那个区域）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'LocationCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开店时间，例如 09:00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreOpenTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关店时间，例如 20:00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreCloseTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否是基准店铺. 0：不是。1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'Comparable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总账代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'GLCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区域代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'RegionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'OrgCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺连接IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'StoreIP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子库存代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE', @level2type=N'COLUMN',@level2name=N'SubInvCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺表。
@2015-03-09 修改StoreTypeID含义。
@2016-08-09 删除BrandCode（货品品牌）， 加上StoreBrandCode（店铺品牌）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STORE'
GO
