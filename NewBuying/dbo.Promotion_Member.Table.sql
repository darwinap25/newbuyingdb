USE [NewBuying]
GO
/****** Object:  Table [dbo].[Promotion_Member]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion_Member](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PromotionCode] [varchar](64) NOT NULL,
	[LoyaltyType] [int] NULL,
	[LoyaltyValue] [int] NULL DEFAULT ((0)),
	[LoyaltyThreshold] [int] NULL DEFAULT ((0)),
	[LoyaltyBirthdayFlag] [int] NULL DEFAULT ((0)),
	[LoyaltyPromoScope] [int] NULL,
 CONSTRAINT [PK_PROMOTION_MEMBER] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销指定的会员限制类型。1：brand， 2：CardType， 3：CardGrade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member', @level2type=N'COLUMN',@level2name=N'LoyaltyType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据LoyaltyType设置，Value为指定类型的 ID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member', @level2type=N'COLUMN',@level2name=N'LoyaltyValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销指定的会员忠诚度阀值。(一般指会员积分数 )。 默认0，表示不设置阀值。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member', @level2type=N'COLUMN',@level2name=N'LoyaltyThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否销售当日生日促销。默认0.
0：不是生日促销。
1：会员生日当日
2：会员生日当周
3：会员生日当月' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member', @level2type=N'COLUMN',@level2name=N'LoyaltyBirthdayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销制定的会员范围 1 新注册会员 2朋友推荐朋友' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member', @level2type=N'COLUMN',@level2name=N'LoyaltyPromoScope'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promotion_H的子表，促销针对人群表。
（记录之间是 or 的关系）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Member'
GO
