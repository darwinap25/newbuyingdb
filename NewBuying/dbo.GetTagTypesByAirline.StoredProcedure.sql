USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetTagTypesByAirline]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTagTypesByAirline]
	-- Add the parameters for the stored procedure here
	@airlinecode			varchar(64)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select
		c.CouponTypeName1,
		c.CouponTypeCode
	FROM CouponType c
	WHERE c.BrandCode = @airlinecode		


END

GO
