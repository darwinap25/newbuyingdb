USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberAddress]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberAddress](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[MemberFirstAddr] [int] NULL DEFAULT ((0)),
	[AddressCountry] [varchar](64) NULL,
	[AddressProvince] [varchar](64) NULL,
	[AddressCity] [varchar](64) NULL,
	[AddressDistrict] [varchar](64) NULL,
	[AddressDetail] [nvarchar](512) NULL,
	[AddressFullDetail] [nvarchar](512) NULL,
	[AddressZipCode] [varchar](512) NULL,
	[Contact] [nvarchar](512) NULL,
	[ContactPhone] [nvarchar](512) NULL,
	[AddressType] [int] NULL DEFAULT ((0)),
	[AddressTypeDesc] [varchar](512) NULL,
	[IsBillAddress] [int] NULL,
	[Column_20] [char](10) NULL DEFAULT ('0'),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MemberAddress] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员表ID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否会员首地址。（默认地址）
0： 不是。 1： 是的。  默认为0。 都为0 时，随机排序。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'MemberFirstAddr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressCountry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'省（州）编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressProvince'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'城市编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressCity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区县编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressDistrict'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'详细地址。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'完整地址。（AddressCountry，AddressProvince，AddressCity， AddressDistrict内填写是Code。这些字段对应的Name合并起来，加上AddressDetail内容，就是FullDetail）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressFullDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮编' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressZipCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址类型。 0：默认未定义。1：家里。2：公司 。3：自定义' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址类型描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'AddressTypeDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否账单地址。 0：不是。1：是。  默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress', @level2type=N'COLUMN',@level2name=N'IsBillAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员地址表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberAddress'
GO
