USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[NewCalcEarnPoints]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[NewCalcEarnPoints]
   @CardNumber          varchar(512),  --卡号
   @ConsumeAMT          money,
   @SalesDetail			nvarchar(max),  -- 交易单货品明细(交易货品, XML格式字符串)	@SalesDetail格式同svasalessubmit
   @EarnPoint           int output,     --获得的积分
   @PointRuleType       int = 0
AS
/****************************************************************************
**  Name : NewCalcEarnPoints
**  Version: 1.0.0.1
**  Description : 获得积分的新计算方式，销售的各个货品可以不同积分规则。
**				  分别以部门, 品牌,SKU 合计后, 分开计算积分, 如果为0,则以普通货品方式计算。
                  计算优先级依次：sku，deptcode, brand.  一旦能计算出积分，则此货品不再参与下面的积分计算。 
**  Parameter :
**         @CardNumber			varchar(512),	-卡号 (CardNumber 是唯一号码, Card表主键)
**         @ConsumeAMT float   - 消费的金额
**         @EarnPoint     int output  - 获得的积分
   @ProdCode            varchar(64),   --参与计算的货品
   @DeptCode            varchar(64),   --此货品的部门
   @ProductBrandCode	varchar(64)
**         return:  0 成功， -1 失败
  declare @EarnPoint int
  exec CalcEarnPoints '1111002', 200, @EarnPoint output, 0
  print @EarnPoint
**                                         -- PointRuleOper: 0 = @, 1 = >=, 2 = <=
**  Created by: Gavin @2013-11-20
**  Modify by: Gavin @2013-12-19 (ver 1.0.0.1)  单独为每个货品计算积分时，要考虑Product->AddPointFlag.
**
****************************************************************************/
begin 
  declare @SalesDData xml, @PartEarnPoint int, @TotalEarnPoint int
  declare @SalesD table (SeqNo int, ProdCode varchar(64), DeptCode varchar(64), ProductBrandCode varchar(64), ProdDesc nvarchar(512), Serialno varchar(512), 
        Collected int, RetailPrice money, NetPrice money, Qty int, NetAmount money, Additional varchar(512),ReservedDate datetime, PickupDate datetime,
        AddPointFlag int, AddPointValue int)
  declare @ProdCode varchar(64), @DeptCode varchar(64), @ProductBrandCode varchar(64), @NetAmount money
  declare @SpeTotalAmt money, @TotalAmt money, @SeqNo int, @AddPointValue int, @AddPointFlag int 
   		 
  set @ConsumeAMT = abs(isnull(@ConsumeAMT,0))
  set @CardNumber = isnull(@CardNumber, '')
  set @EarnPoint = 0

  --PLU detail数据写入临时表，判断货品库存数量
  if isnull(@SalesDetail, '') <> ''
  begin
	set @SalesDData = @SalesDetail
	Insert into @SalesD(SeqNo, ProdCode, DeptCode, ProductBrandCode, ProdDesc, Serialno, Collected, RetailPrice, NetPrice, Qty, NetAmount, Additional, ReservedDate, PickupDate, AddPointFlag, AddPointValue)
	select T.v.value('@SeqNo','int') as SeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode,
	    P.DepartCode, B.ProductBrandCode,   
	    T.v.value('@ProdDesc','nvarchar(512)') as ProdDesc, T.v.value('@Serialno','varchar(512)') as Serialno, T.v.value('@Collected','int') as Collected, T.v.value('@RetailPrice','money') as RetailPrice, 
	    T.v.value('@NetPrice','money') as NetPrice, T.v.value('@QTY','int') as QTY, T.v.value('@NetAmount','money') as NetAmount, T.v.value('@Additional','varchar(512)') as Additional,
	    T.v.value('@ReservedDate','datetime') as ReservedDate, T.v.value('@PickupDate','datetime') as PickupDate,
	    P.AddPointFlag, P.AddPointValue	    
	  from @SalesDData.nodes('/ROOT/PLU') T(v) left join Product P on T.v.value('@ProdCode','varchar(64)') = P.ProdCode 
	    left join Product_Brand	B on P.ProductBrandID = B.ProductBrandID
	
	-- 获得指定货品的积分
	set @PartEarnPoint = 0
	set @TotalEarnPoint = 0
	set @SpeTotalAmt = 0 
	set @TotalAmt = 0
    DECLARE CUR_PointRuleSalesD_PLU CURSOR fast_forward local FOR  
      select ProdCode, sum(isnull(NetAmount,0)), max(isnull(AddPointFlag,1)), max(isnull(AddPointValue,0)) from @SalesD
       where isnull(AddPointFlag, 1) <> 0
      group by ProdCode
    OPEN CUR_PointRuleSalesD_PLU  
    FETCH FROM CUR_PointRuleSalesD_PLU INTO @ProdCode, @NetAmount, @AddPointFlag, @AddPointValue
    WHILE @@FETCH_STATUS=0  
    BEGIN
	  exec CalcEarnPoints_Base @CardNumber, @NetAmount, @ProdCode, '', '', @PartEarnPoint output, @AddPointFlag, @AddPointValue, 0
	  if @PartEarnPoint > 0 
	  begin
		set @TotalEarnPoint = @PartEarnPoint + @TotalEarnPoint
		delete from @SalesD where ProdCode = @ProdCode
	  end
      FETCH FROM CUR_PointRuleSalesD_PLU INTO @ProdCode, @NetAmount, @AddPointFlag, @AddPointValue
    END  
    CLOSE CUR_PointRuleSalesD_PLU   
    DEALLOCATE CUR_PointRuleSalesD_PLU

    -- 获得指定部门货品的积分
    DECLARE CUR_PointRuleSalesD_DEPT CURSOR fast_forward local FOR  
      select DeptCode, sum(isnull(NetAmount,0)), max(isnull(AddPointFlag,1)), max(isnull(AddPointValue,0)) from @SalesD
        where isnull(AddPointFlag, 1) <> 0
      group by DeptCode     
    OPEN CUR_PointRuleSalesD_DEPT  
    FETCH FROM CUR_PointRuleSalesD_DEPT INTO @DeptCode, @NetAmount, @AddPointFlag, @AddPointValue
    WHILE @@FETCH_STATUS=0  
    BEGIN
	  exec CalcEarnPoints_Base @CardNumber, @NetAmount, '', @DeptCode, '', @PartEarnPoint output, 1, 0, 0
	  if @PartEarnPoint > 0 
	  begin
		set @TotalEarnPoint = @PartEarnPoint + @TotalEarnPoint
		delete from @SalesD where DeptCode = @DeptCode
	  end    
      FETCH FROM CUR_PointRuleSalesD_DEPT INTO @DeptCode, @NetAmount, @AddPointFlag, @AddPointValue
    END  
    CLOSE CUR_PointRuleSalesD_DEPT 
    DEALLOCATE CUR_PointRuleSalesD_DEPT  
    
    -- 获得指定品牌货品的积分
    DECLARE CUR_PointRuleSalesD_BRD CURSOR fast_forward local FOR  
      select ProductBrandCode, sum(isnull(NetAmount,0)), max(isnull(AddPointFlag,1)), max(isnull(AddPointValue,0)) from @SalesD
        where isnull(AddPointFlag, 1) <> 0
      group by ProductBrandCode     
    OPEN CUR_PointRuleSalesD_BRD  
    FETCH FROM CUR_PointRuleSalesD_BRD INTO @ProductBrandCode, @NetAmount, @AddPointFlag, @AddPointValue
    WHILE @@FETCH_STATUS=0  
    BEGIN
	  exec CalcEarnPoints_Base @CardNumber, @NetAmount, '', '', @ProductBrandCode, @PartEarnPoint output, 1, 0, 0
	  if @PartEarnPoint > 0 
	  begin
		set @TotalEarnPoint = @PartEarnPoint + @TotalEarnPoint
		delete from @SalesD where ProductBrandCode = @ProductBrandCode
	  end      
      FETCH FROM CUR_PointRuleSalesD_BRD INTO @ProductBrandCode, @NetAmount, @AddPointFlag, @AddPointValue
    END  
    CLOSE CUR_PointRuleSalesD_BRD 
    DEALLOCATE CUR_PointRuleSalesD_BRD 
    
    select @TotalAmt = sum(isnull(NetAmount, 0)) from @SalesD where isnull(AddPointFlag, 1) <> 0
    exec CalcEarnPoints_Base @CardNumber, @TotalAmt, '', '', '', @PartEarnPoint output, 0
    if @PartEarnPoint > 0 
      set @TotalEarnPoint = @PartEarnPoint + @TotalEarnPoint
                 	       
  end else
  begin
    exec CalcEarnPoints_Base @CardNumber, @ConsumeAMT, '', '', '', @EarnPoint output, 1, 0, 0
    set @TotalEarnPoint = @EarnPoint
  end
  
  set @EarnPoint = isnull(@TotalEarnPoint, 0)         
  return 0
end

GO
