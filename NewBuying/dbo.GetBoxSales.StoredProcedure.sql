USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBoxSales]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBoxSales]
  @BOMCode               VARCHAR(64),			         -- BOM货品的Code, 即ProdCode
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetBoxSales
**  Version : 1.0.0.1
**  Description : 获得BOM货品的子货品。（）
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = '501217D332981304'
  exec @a = GetBoxSales @DepartCode,  '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
  select * from BUY_BOXSALE
select * from buy_product
**  Created by Gavin @2015-03-06
**  Modify by Gavin @2017-09-29 (ver 1.0.0.1) 使用buy_product 的startdate，enddate来判断
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000), @NowDate DATETIME

  SET @NowDate = GETDATE()
  SET @BOMCode = ISNULL(@BOMCode, '')
  IF @BOMCode <> ''  
    SET @WhereStr = ' WHERE A.BOMCode=''' + @BOMCode + ''' AND ( DATEDIFF(DD, B.StartDate, ''' + CONVERT(VARCHAR(10), @NowDate, 120) + ''') >= 0 AND DATEDIFF(DD, ''' + CONVERT(VARCHAR(10), @NowDate, 120) + ''', B.EndDate) >= 0 ) '
  ELSE  
    SET @WhereStr = ' WHERE ( DATEDIFF(DD, B.StartDate, ''' + CONVERT(VARCHAR(10), @NowDate, 120) + ''') >= 0 AND DATEDIFF(DD, ''' + CONVERT(VARCHAR(10), @NowDate, 120) + ''', B.EndDate) >= 0 )'

  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1
  
  SET @SQLStr = 'SELECT A.BOMID as KeyID, A.BOMCode, A.ProdCode, A.Qty, A.MinQty, A.MaxQty, A.DefaultQty, A.Price, A.PartValue, A.ValueType, '
    + ' A.ActPrice, B.StartDate, B.EndDate, A.MutexTag'
    + ' FROM BUY_BOXSALE A LEFT JOIN BUY_Product B ON A.BOMCode = B.ProdCode '
    + @WhereStr

  EXEC SelectDataInBatchs @SQLStr, 'BOMCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr

  RETURN 0
END

GO
