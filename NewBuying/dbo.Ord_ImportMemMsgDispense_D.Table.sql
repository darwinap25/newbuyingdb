USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportMemMsgDispense_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportMemMsgDispense_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MemberMsgDispenseNumber] [varchar](64) NOT NULL,
	[MessageTemplateCode] [varchar](64) NULL,
	[MemberRegisterMobile] [varchar](64) NULL,
	[MemberPhoneNumber] [varchar](64) NULL,
	[MemberEmail] [varchar](64) NULL,
 CONSTRAINT [PK_ORD_IMPORTMEMMSGDISPENSE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_D', @level2type=N'COLUMN',@level2name=N'MemberMsgDispenseNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息模板Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_D', @level2type=N'COLUMN',@level2name=N'MessageTemplateCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员注册用手机号（加上国家代码的，例如0861392572219）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_D', @level2type=N'COLUMN',@level2name=N'MemberRegisterMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员电话号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_D', @level2type=N'COLUMN',@level2name=N'MemberPhoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡号，分发的coupon，绑定在此卡上' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_D', @level2type=N'COLUMN',@level2name=N'MemberEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'@2016-05-12 分发消息给指定会员的单据表。（子表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMemMsgDispense_D'
GO
