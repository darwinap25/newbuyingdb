USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DataConvert_BUYTOSVA_PLU]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DataConvert_BUYTOSVA_PLU]
  @OnlyUpdateTempSKU      int = 0
AS
/****************************************************************************
**  Name : DataConvert_BUYTOSVA_PLU
  exec DataConvert_BUYTOSVA_PLU
  sp_helptext DataConvert_BUYTOSVA_PLU
**  Version: 1.0.0.13 (for Bauhaus)
**  Description :  从Buying DB 获得 PLU 相关数据写入 SVA。 此过程在SVA上执行。
                   脚本中buying DB 名字为： Buying_bauhaus， 可以根据情况修改。  
**  Created by: Gavin @2013-11-21
**  Modify by: Gavin @2014-01-16 先屏蔽外键约束，执行完后才恢复。
**  Modify by: Gavin @2015-09-22 (ver 1.0.0.2) BUY_PRODUCT表增加字段isOnlineSKU，根据此字段判断货品是否要同步到SVA。
**  Modify by: Gavin @2015-09-24 (ver 1.0.0.3) 由于BUY_PRODUCT_STYLE没有数据, 所以需要自动添加. 以后有数据后去掉。 
**  Modify by: Gavin @2015-10-10 (ver 1.0.0.4) 修正插入 PRODUCT_STYLE 数据查询的问题
**  Modify by: Gavin @2015-10-16 (ver 1.0.0.5) 加上Tender的同步 
**  Modify by: Gavin @2015-11-26 (ver 1.0.0.6) 加上Product_Catalog的同步 
**  Modify by: Gavin @2016-04-08 (ver 1.0.0.7) (for Bauhaus) 为Bauhaus修改.
**  Modify by: Gavin @2016-08-08 (ver 1.0.0.8) (for Bauhaus) 加上IsOnlineSKU. product 等数据，不再删除，只是做更新。只有IsOnlineSKU的货品才做更新。
**  Modify by: Gavin @2016-09-27 (ver 1.0.0.9) (for Bauhaus) BUY_PRODUCT_ADD_BAU 增加 SizeM1,SizeM2,SizeM3,SizeM4,SizeM5,SizeM6,SizeM7,SizeM8
**  Modify by: Gavin @2016-10-12 (ver 1.0.0.10) (for Bauhaus) 增加Product_Style表字段ProdStyleSizeFile 
**  Modify by: Gavin @2016-10-24 (ver 1.0.0.11) (for Bauhaus) 增加buying 下promotion 表的同步。
**  Modify by: Gavin @2017-03-15 (ver 1.0.0.12) (for Bauhaus) 同步 BUY_RPRICE_M 表时, 加上判断有效日期. 有效开始日期在后面的优先.
**  Modify by: Gavin @2017-04-25 (ver 1.0.0.13) (for Bauhaus) 增加参数@OnlyUpdateTempSKU，0：原来的做法。 1：只更新IMP_PRODUCT_TEMP表中存在的货品数据。 默认为0。
**
****************************************************************************/
begin 

alter table Product_Size nocheck constraint all
alter table ProductBarcode nocheck constraint all
alter table Product nocheck constraint all
alter table Department nocheck constraint all
alter table Product_Style nocheck constraint all
alter table Product_Brand nocheck constraint all
alter table Product_PIC nocheck constraint all
alter table Product_Price nocheck constraint all
alter table Product_PROMOPrice nocheck constraint all
alter table Product_Associated nocheck constraint all
alter table Product_Particulars nocheck constraint all
alter table Product_Classify nocheck constraint all
alter table Product_Catalog nocheck constraint all

alter table BUY_CURRENCY nocheck constraint all
alter table MATERIAL_OUT nocheck constraint all
alter table MATERIAL_IN nocheck constraint all
alter table SEASON nocheck constraint all
alter table SizeRange nocheck constraint all
alter table PRODUCT_ADD_BAU nocheck constraint all
alter table PRODUCT_size nocheck constraint all
  
   set @OnlyUpdateTempSKU = isnull(@OnlyUpdateTempSKU, 0)

   insert into Currency(CurrencyCode,CurrencyName1,CurrencyName2,CurrencyName3, CurrencyRate, IsLocalCurrency, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
   select CurrencyCode, CurrencyName1,CurrencyName2,CurrencyName3,Rate, 0, GetDate(), CreatedBy, GetDate(), UpdatedBy 
     from buy_currency    
   where CurrencyCode not in (select CurrencyCode from Currency)

   insert into buy_currency(CurrencyCode,CurrencyName1,CurrencyName2,CurrencyName3,Rate,TenderType,Status,
       CashSale,CouponValue,Base,MinAmt,MaxAmt,CardType,CardBegin,CardEnd,CardLen,AccountCode,ContraCode,
	   PayTransfer,Refund_Type,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CurrencyPicFile,TerminalFlag)
   select CurrencyCode,CurrencyName1,CurrencyName2,CurrencyName3,Rate,TenderType,Status,
       CashSale,CouponValue,Base,MinAmt,MaxAmt,CardType,CardBegin,CardEnd,CardLen,AccountCode,ContraCode,
	   PayTransfer,Refund_Type,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,CurrencyPicFile,TerminalFlag 
	 from buy_currency    
   where CurrencyCode not in (select CurrencyCode from buy_currency)

   insert into tender(TenderCode, TenderType, TenderName1, TenderName2, TenderName3, CashSale, Status, Base, Rate, 
       MinAmount, MaxAmount, CardBegin, CardEnd, CardLen, Additional, BankID, TenderPicFile, BrandID, 
       CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
   select CurrencyCode, TenderType, CurrencyName1,CurrencyName2, CurrencyName3, CashSale, Status, Base, Rate,
       MinAmt, MaxAmt, CardBegin, CardEnd, CardLen, AccountCode, 0, CurrencyPicFile, 0,
       GetDate(), CreatedBy, GetDate(), UpdatedBy from buy_currency    
   where CurrencyCode not in (select TenderCode from tender)

  truncate table SEASON
  insert into SEASON (SeasonID,SeasonCode,SeasonName1,SeasonName2,SeasonName3)
  select SeasonID,SeasonCode,SeasonName1,SeasonName2,SeasonName3
    from SEASON
  truncate table Gender
  insert into Gender (GenderID, GenderCode, GenderDesc1, GenderDesc2, GenderDesc3)
  select GenderID, GenderCode, GenderDesc1, GenderDesc2, GenderDesc3
    from Gender

  truncate table SizeRange
  insert into SizeRange (SizeRangeID,SizeRangeCode, SizeRangeName1, SizeRangeName2, SizeRangeName3)
  select SizeRangeID,SizeRangeCode, SizeRangeName1, SizeRangeName2, SizeRangeName3
    from SizeRange
   
  truncate table Color
  insert into Color (ColorCode, ColorName1, ColorName2, ColorName3, ColorPicFile, RGB)
  select ColorCode, ColorName1, ColorName2, ColorName3, ColorPicFile, RGB
    from BUY_COLOR
    
  truncate table Product_Size
  insert into Product_Size (ProductSizeID, ProductSizeCode, ProductSizeName1, ProductSizeName2, ProductSizeName3, ProductSizeNote, ProductSizeType)
  select ProductSizeID, ProductSizeCode, ProductSizeName1, ProductSizeName2, ProductSizeName3, ProductSizeNote, ProductSizeType
    from BUY_PRODUCTSIZE 
  
--  delete from ProductBarcode
--  delete from Product   
--  delete from Department
  
  insert into Department (DepartCode, DepartName1, DepartName2, DepartName3, DepartPicFile, 
     DepartPicFile2, DepartPicFile3, DepartNote, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  select DepartCode, DepartName1, DepartName2, DepartName3, DepartPicFile, 
     DepartPicFile2, DepartPicFile3, DepartNote, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy
    from BUY_DEPARTMENT A
  where A.DepartCode not in (select DepartCode from Department)
    
--  truncate table Product_Style
  insert into Product_Style (ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile)
  select ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile
    from BUY_PRODUCTSTYLE A 
  where A.ProdCodeStyle not in (select ProdCodeStyle from Product_Style)
    
--  delete from Product_Brand
  insert into Product_Brand (ProductBrandCode, ProductBrandName1, ProductBrandName2, ProductBrandName3, 
     ProductBrandDesc1, ProductBrandDesc2, ProductBrandDesc3, ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, 
     CardIssuerID, IndustryID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  select BrandCode, BrandName1, BrandName2, BrandName3, 
     BrandDesc1, BrandDesc2, BrandDesc3, BrandPicSFile, BrandPicMFile, BrandPicGFile, 
     CardIssuerID, IndustryID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy
    from BUY_BRAND A 
  where A.BrandCode not in (select ProductBrandCode from Product_Brand)
 
  --delete from Product where IsOnlineSKU = 1
  insert into Product (ProdCode, ProdName1, ProdName2, ProdName3, DepartCode, ProductBrandID, NonSale, 
    ProdPicFile, ProdType, ProdNote, PackQty, NewFlag, HotSaleFlag, Flag1, Flag2, Flag3, Flag4, Flag5, 
    OriginID, ColorID, ProductSizeID, AddPointFlag, AddPointValue, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy,ColorCode,ProductSizeCode, IsOnlineSKU)
  select P.ProdCode, ProdDesc1, ProdDesc2, ProdDesc3, D.DepartCode, B.ProductBrandID, NonSale, 
    ProdPicFile, ProductType, ProdSpec, G.PackageSizeQty, NewFlag, HotSaleFlag, Flag1, Flag2, Flag3, Flag4, Flag5, 
    N.NationID, C.ColorID, S.ProductSizeID, AddPointFlag, AddPointValue,     
    P.CreatedOn, P.CreatedBy, P.UpdatedOn, P.UpdatedBy,P.ColorCode,S.ProductSizeCode,P.IsOnlineSKU
   from BUY_PRODUCT P left join Product_Brand B on P.ProductBrandCode = B.ProductBrandCode 
     left join Department D on D.DepartCode = P.DepartCode 
     left join BUY_PRODUCTPACKAGE G on P.PackageSizeCode = G.PackageSizeCode
     left join Nation N on P.OriginCountryCode = N.CountryCode
     left join Color C on P.ColorCode = C.ColorCode 
     left join Product_Size S on P.ProductSizeCode = S.ProductSizeID    
   where P.ProdCode not in (select ProdCode from Product)   

--  truncate table Product_PIC
  insert into Product_PIC (ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1, ProductPicNote2, ProductPicNote3)  
  select ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1, ProductPicNote2, ProductPicNote3
    from BUY_PRODUCT_PIC A
  where A.ProdCode not in (select ProdCode from Product_PIC)
    
  truncate table Product_Price
  insert into Product_Price (ProdCode, ProdPriceType, DefaultPrice, NetPrice, StoreCode, StoreGroupCode,   -- MemberPrice, PromotionPrice, 
     Status, StartDate, EndDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  select M.ProdCode, 1, RefPrice, Price, StoreCode, StoreGroupCode,                         -- MemberPrice, PromotionPrice, 
     M.Status, M.StartDate, M.EndDate, Getdate(), M.UpdatedBy, M.UpdatedOn, M.UpdatedBy
    from BUY_RPRICE_M M 
      left join (select ProdCode, max(StartDate) as StartDate from BUY_RPRICE_M M
                  where DateDiff(dd, M.startdate, getdate()) >= 0 and DateDiff(dd, getdate(), M.enddate) >= 0 and M.status = 1 
                 Group by ProdCode) M1 on M.ProdCode = M1.ProdCode and M.StartDate = M1.StartDate 
	  left join Product P on M.ProdCode = P.ProdCode
   where M.status = 1 and P.ProdCode is not null and M1.ProdCode is not null
 
  truncate table Product_PROMOPrice  
  insert into Product_PROMOPrice (PromotionCode, Description1, Description2, Description3, StoreCode, 
     StoreGroupCode, StartDate, EndDate, StartTime, EndTime, EntityNum , EntityType, HitAmount, HitOP, 
     DiscPrice, DiscType, DayFlagCode, WeekFlagCode, MonthFlagCode, LoyaltyFlag, LoyaltyCardTypeCode, 
     LoyaltyCardGradeCode, BirthdayFlag, Status, ApproveOn, ApproveBy, UpdatedOn, UpdatedBy)
  select PromotionCode, Description1, Description2, Description3, StoreCode, 
     StoreGroupCode, StartDate, EndDate, StartTime, EndTime, EntityNum , EntityType, HitAmount, HitOP, 
     DiscPrice, DiscType, DayFlagCode, WeekFlagCode, MonthFlagCode, LoyaltyFlag, LoyaltyCardTypeCode, 
     LoyaltyCardGradeCode, BirthdayFlag, Status, ApproveOn, ApproveBy, UpdatedOn, UpdatedBy
    from BUY_PROMO_M  
       
  insert into ProductBarcode 
    (Barcode, ProdCode)
  select Barcode, B.ProdCode 
    from BUY_BARCODE	B left join Product P on B.ProdCode = P.ProdCode 
   where P.ProdCode is not null
         and B.Barcode not in (select Barcode from ProductBarcode)

 -- truncate table Product_Associated
  insert into Product_Associated (ProdCode, SeqNo, AssociatedProdCode, AssociatedProdName, AssociatedProdFile, Note,
     CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  select ProdCode, SeqNo, AssociatedProdCode, AssociatedProdName, AssociatedProdFile, Note,
     CreatedOn, CreatedBy, UpdatedOn, UpdatedBy
   from BUY_ProductAssociated A
   where A.ProdCode not in (select ProdCode from Product_Associated)
 	 
--  truncate table Product_Particulars
  insert into Product_Particulars (ProdCode, LanguageID, ProdFunction, ProdIngredients, ProdInstructions, PackDesc,
   PackUnit, Memo1, Memo2, Memo3, Memo4, Memo5, Memo6, MemoTitle1, MemoTitle2, MemoTitle3, MemoTitle4, 
   MemoTitle5, MemoTitle6)
  select ProdCode, LanguageID, ProdFunction, ProdIngredients, ProdInstructions, PackDesc,
   PackUnit, Memo1, Memo2, Memo3, Memo4, Memo5, Memo6, MemoTitle1, MemoTitle2, MemoTitle3, MemoTitle4, 
   MemoTitle5, MemoTitle6 
  from BUY_Product_Particulars A	
  where A.ProdCode not in (select ProdCode from Product_Particulars) 
	 
--  truncate table Product_Classify
  insert into Product_Classify (ProdCode, ForeignkeyID, ForeignTable,
   CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  select ProdCode, ForeignkeyID, ForeignTable,
    CreatedOn, CreatedBy, UpdatedOn, UpdatedBy 
   from BUY_PRODUCT_CLASSIFY A
  where A.ProdCode not in (select ProdCode from Product_Classify) 

  insert into Product_Style (ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile)
  select ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile
     from BUY_PRODUCTSTYLE
  where ProdCode not in (select ProdCode from Product_Style)

--  truncate table Product_Catalog
  insert into Product_Catalog (ProdCode, DepartCode, 
   CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  select ProdCode, DepartCode, 
    CreatedOn, CreatedBy, UpdatedOn, UpdatedBy 
   from BUY_PRODUCT
   where ProdCode not in (select ProdCode from Product_Catalog)

--  truncate table PRODUCT_ADD_BAU
  insert into PRODUCT_ADD_BAU (ProdCode,Standard_Cost,Export_Cost,AVG_Cost,MODEL,SKU,[YEAR],REORDER_LEVEL, 
      HS_CODE,COO,SIZE_RANGE,DESIGNER,BUYER,MERCHANDISER,RETIRE_DATE,CompanyCode, SizeM1,SizeM2,SizeM3,SizeM4,SizeM5,SizeM6,SizeM7,SizeM8)
  select ProdCode,Standard_Cost,Export_Cost,AVG_Cost,MODEL,SKU,[YEAR],REORDER_LEVEL, 
      HS_CODE,COO,SIZE_RANGE,DESIGNER,BUYER,MERCHANDISER,RETIRE_DATE,CompanyCode, SizeM1,SizeM2,SizeM3,SizeM4,SizeM5,SizeM6,SizeM7,SizeM8
   from BUY_PRODUCT_ADD_BAU
   where ProdCode not in (select ProdCode from PRODUCT_ADD_BAU)


-- promotion Promotion_Hit_PLU
  if exists (select 1 from  sysobjects where  id = object_id('BUY_Promotion_H') and type = 'U')
  begin
    drop table BUY_Promotion_H	
  end
  select * into BUY_Promotion_H from Promotion_H

  if exists (select 1 from  sysobjects where  id = object_id('BUY_Promotion_Hit_PLU') and type = 'U')
  begin
    drop table BUY_Promotion_Hit_PLU
  end
  select *  into BUY_Promotion_Hit_PLU from Promotion_Hit_PLU

   --select * from  BUY_PRODUCT 
alter table Product_Size check constraint all
alter table ProductBarcode check constraint all
alter table Product check constraint all
alter table Department check constraint all
alter table Product_Style check constraint all
alter table Product_Brand check constraint all
alter table Product_PIC check constraint all
alter table Product_Price check constraint all
alter table Product_PROMOPrice check constraint all
alter table Product_Associated check constraint all
alter table Product_Particulars check constraint all
alter table Product_Classify check constraint all
alter table Product_Catalog check constraint all

alter table BUY_CURRENCY check constraint all
alter table MATERIAL_OUT check constraint all
alter table MATERIAL_IN check constraint all
alter table SEASON check constraint all
alter table SizeRange check constraint all
alter table PRODUCT_ADD_BAU check constraint all
               
end

GO
