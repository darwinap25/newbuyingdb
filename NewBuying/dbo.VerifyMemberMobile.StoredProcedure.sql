USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[VerifyMemberMobile]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[VerifyMemberMobile]  
  @MemberID             int,                    --会员ID  
  @CountryCode          varchar(64),            --手机号码的国家码  
  @MobileNo             varchar(64),            --手机号码。
  @ReturnCode           varchar(64) output,     --验证码 (4位)   
  @LanguageAbbr         varchar(20)='' 
as  
/****************************************************************************
**  Name : 用户手机校验过程。
**  Version: 1.0.0.2
**  Description : 
**  Example :
    select * from useraction_movement	
**  Created by: Gavin @2014-05-14
**  Modified by: Gavin @2014-07-16 (ver 1.0.0.1) 判断输入的账号是否已经验证,如果已经验证,则返回 -107
**  Modified by: Gavin @2014-08-23 (ver 1.0.0.2) 产生的号码保存在member表中
**  Modified by: Gavin @2014-08-28 (ver 1.0.0.3) 加发送消息。
**
****************************************************************************/
begin
  declare @VerifyFlag int
  select @VerifyFlag = VerifyFlag from MemberMessageAccount 
    where MemberID = @MemberID and MessageServiceTypeID = 2 and AccountNumber = isnull(@MobileNo,'')
  if @VerifyFlag = 1
    return -107
  else
  begin 
    exec GenRandNumber 4, @ReturnCode output
    --ver 1.0.0.2
    update Member set MobileVerifyCode = @ReturnCode, MobileVerifyCodeStamp = GETDATE() where MemberID = @MemberID 
    
    --ver 1.0.0.3
/*    
    declare @CardGradeID int,  @CardTypeID int, @CardBrandID int, @CardNumber varchar(64), @CouponNumber varchar(64), 
            @CouponTypeID int
    select Top 1 @CardGradeID = C.CardGradeID, @CardTypeID = C.CardTypeID, @MemberID = MemberID, @CardBrandID = T.BrandID
      from Card C
      left join CardType T on C.CardTypeID = T.CardTypeID
     where C.MemberID = @MemberID 
    select top 1 @CouponNumber = CouponNumber, @CouponTypeID = CouponTypeID from Coupon 
      where CouponNumber in (select CardNumber from Card where MemberID = @MemberID)
    set @CouponNumber = isnull(@CouponNumber, '')
    set @CouponTypeID = isnull(@CouponTypeID, 0)  
    exec GenBusinessMessage 507, @MemberID, @CardNumber, @CouponNumber, @CardTypeID, @CardGradeID, @CardBrandID,  @CouponTypeID
    exec GenBusinessMessage 26, @MemberID, @CardNumber, @CouponNumber, @CardTypeID, @CardGradeID, @CardBrandID,  @CouponTypeID      
*/    
    return 0
  end  
end

GO
