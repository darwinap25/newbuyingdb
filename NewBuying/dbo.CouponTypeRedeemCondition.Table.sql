USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponTypeRedeemCondition]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponTypeRedeemCondition](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[RedeemLimitType] [int] NULL,
	[LimitValue] [int] NULL,
	[BirthdayFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_COUPONTYPEREDEEMCONDITION] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CouponTypeRedeemCondition] ADD  DEFAULT ((0)) FOR [CardTypeID]
GO
ALTER TABLE [dbo].[CouponTypeRedeemCondition] ADD  DEFAULT ((0)) FOR [CardGradeID]
GO
ALTER TABLE [dbo].[CouponTypeRedeemCondition] ADD  DEFAULT ((1)) FOR [RedeemLimitType]
GO
ALTER TABLE [dbo].[CouponTypeRedeemCondition] ADD  DEFAULT ((0)) FOR [BirthdayFlag]
GO
ALTER TABLE [dbo].[CouponTypeRedeemCondition] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CouponTypeRedeemCondition] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型D' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵使用指定的会员卡类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵使用指定的会员卡等级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用Coupon的条件类型。
1、整单交易总金额大于等于LimitValue
2、会员卡累计消费金额大于等于LimitValue
3、会员卡积分大于等于LimitValue
4、交易单中，在CouponTypeExchangeBinding中指定的消费货品，总金额大于等于LimitValue
5、交易单中，在CouponTypeExchangeBinding中指定的消费货品，总数量大于等于LimitValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition', @level2type=N'COLUMN',@level2name=N'RedeemLimitType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据RedeemLimitType的不同，值为不同含义 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition', @level2type=N'COLUMN',@level2name=N'LimitValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否销售当日生日促销。默认0.
0：不是生日促销。
1：会员生日当日
2：会员生日当周
3：会员生日当月' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition', @level2type=N'COLUMN',@level2name=N'BirthdayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵使用条件设置表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeRedeemCondition'
GO
