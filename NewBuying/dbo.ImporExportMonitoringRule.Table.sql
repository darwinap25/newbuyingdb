USE [NewBuying]
GO
/****** Object:  Table [dbo].[ImporExportMonitoringRule]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImporExportMonitoringRule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RuleName] [varchar](64) NOT NULL,
	[Description] [varchar](512) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL,
	[Func] [varchar](64) NULL,
	[DownloadPath] [varchar](512) NULL,
	[DayFlagID] [int] NULL,
	[MonthFlagID] [int] NULL,
	[WeekFlagID] [int] NULL,
	[ActiveTime] [datetime] NULL,
	[CreatedOn] [datetime] NULL CONSTRAINT [DF__ImporExpo__Creat__242E1EDF]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL CONSTRAINT [DF__ImporExpo__Updat__25224318]  DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ImporExportMonitoringRule] PRIMARY KEY CLUSTERED 
(
	[RuleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
