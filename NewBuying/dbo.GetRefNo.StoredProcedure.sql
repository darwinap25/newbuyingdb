USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetRefNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetRefNo]
  @Code varchar(6), 
  @Seq int output,
  @Head char(6) output,
  @Len int output,
  @Auto char(1) output,
  @Active int output
AS
/****************************************************************************
**  Name : GetRefNo
**  Version: 1.0.0.1
**  Description : Get CODE No. for new price, order, allocation...
**
**  Parameters: @Code - Code for get Number
**              @Seq - Seq Number for return
**              @Head - Head code char for return
**              @Len - Code Max Length
**              @Auto - Auto get number Y-Enable, N-Disable
**  Return: 0 - Success
**          -1 - Error
**  Created by: Robot
**  Modify by: Gavin @2012-06-12 RefNo表增加Active字段。0表示不产生号码。1：表示产生号码
**  Modify by: Gavin @2013-08-29 ‘(ver 1.0.0.1) 如果传入的Code不存在，则自动创建记录
**
****************************************************************************/
begin
  set @Code = isnull(@Code, '')
  if not exists(select * from REFNO where code = @Code)
  begin
	insert into REFNO(Code, RefDesc, seq, header, length, Auto, Active)
	values(@Code, @Code, 1, @Code, 15, 'Y', 1)
  end
  
  DECLARE cursor_refno CURSOR SCROLL_LOCKS
  for
    select seq, header, length, Auto, Active from REFNO
    where code = @Code

  Select @Seq = 0
  Select @Head = ''

  Open cursor_refno

  fetch next from cursor_refno into @Seq, @Head, @Len, @Auto, @Active
  if @@fetch_status = 0 and @Auto = 'Y' and @Active = 1
  begin
    update REFNO set seq = @Seq + 1 where current of cursor_refno
  end

  close cursor_refno
  deallocate cursor_refno
end

GO
