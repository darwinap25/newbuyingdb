USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberAddress]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberAddress]
  @MemberID				int
AS
/****************************************************************************
**  Name : GetMemberAddress
**  Version: 1.0.0.3
**  Description : 返回用户提交的地址。
**
**  Parameter :
  exec GetMemberAddress 1
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**  Modify by: Gavin @2013-01-07 (ver 1.0.0.1) 增加返回 AddressDistrict,  AddressFullDetail
**  Modify by: Gavin @2015-08-25 (ver 1.0.0.2) MemberAddress表增加字段, 此过程做相应修改   
**  Modify by: Gavin @2015-12-18 (ver 1.0.0.3) 增加 order by AddressID desc     
**
****************************************************************************/
begin 
  if isnull(@MemberID, 0) = 0
    return -1  
      
  select AddressID, MemberID, MemberFirstAddr, AddressCountry, AddressProvince, AddressCity, AddressDetail, AddressZipCode, Contact, ContactPhone,
      AddressDistrict,  AddressFullDetail, AddressType, AddressTypeDesc, IsBillAddress 
    from MemberAddress
   where MemberID = @MemberID
   order by AddressID desc 

  return 0 
end

GO
