USE [NewBuying]
GO
/****** Object:  Table [dbo].[InventoryReplenishRule_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InventoryReplenishRule_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryReplenishCode] [varchar](64) NOT NULL,
	[ReplenishType] [int] NULL,
	[StoreID] [int] NOT NULL,
	[OrderTargetID] [int] NULL,
	[MinStockQty] [int] NULL,
	[RunningStockQty] [int] NULL,
	[OrderRoundUpQty] [int] NOT NULL,
	[MinAmtBalance] [money] NULL,
	[RunningAmtBalance] [money] NULL,
	[MinPointBalance] [int] NULL,
	[RunningPointBalance] [int] NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_INVENTORYREPLENISHRULE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[InventoryReplenishRule_D] ADD  DEFAULT ((1)) FOR [ReplenishType]
GO
ALTER TABLE [dbo].[InventoryReplenishRule_D] ADD  DEFAULT ((1)) FOR [OrderRoundUpQty]
GO
ALTER TABLE [dbo].[InventoryReplenishRule_D] ADD  DEFAULT ((0)) FOR [MinAmtBalance]
GO
ALTER TABLE [dbo].[InventoryReplenishRule_D] ADD  DEFAULT ((0)) FOR [RunningAmtBalance]
GO
ALTER TABLE [dbo].[InventoryReplenishRule_D] ADD  DEFAULT ((0)) FOR [MinPointBalance]
GO
ALTER TABLE [dbo].[InventoryReplenishRule_D] ADD  DEFAULT ((0)) FOR [RunningPointBalance]
GO
ALTER TABLE [dbo].[InventoryReplenishRule_D] ADD  DEFAULT ((0)) FOR [Priority]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'InventoryReplenishCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'补货类型。 1：实体数量。  2：金额。 3：积分
（主表的PurchaseType=1时，ReplenishType只能为1（实际上此字段忽略，不做判断。）.  PurchaseType=2时， 可以是2， 或者3）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'ReplenishType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据StoreTypeID选择店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货目标ID。（如果是店铺订货，则填写总部的ID。 如果总部订货，则填写供应商ID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'OrderTargetID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小允许库存。（低于此将可能触发自动补货）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'MinStockQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'正常库存数量。（补货数量为： 正常库存数量 - 当前库存数量）
注：如果有补货的倍数要求，按照倍数要求计算。
比如： 正常库存数量 - 当前库存数量 = 53.   供应商要求按50倍数订货，所以需要订货数量为 100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'RunningStockQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货数量的倍数（包装数量）：
Sample:
max=300 
current=97
max-current=203
203 will be rounded up to 250 since it is the next number divisible by 50.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'OrderRoundUpQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小允许余额。（低于此将可能触发自动充值）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'MinAmtBalance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'正常余额。（补货金额为： 正常余额 - 当前余额）
注：如果有补货的倍数要求，按照倍数要求计算。
比如： 正常库存数量 - 当前库存数量 = 53.   供应商要求按50倍数订货，所以需要订货数量为 100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'RunningAmtBalance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小允许余额。（低于此将可能触发自动充值）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'MinPointBalance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'正常余额。（补货金额为： 正常余额 - 当前余额）
注：如果有补货的倍数要求，按照倍数要求计算。
比如： 正常库存数量 - 当前库存数量 = 53.   供应商要求按50倍数订货，所以需要订货数量为 100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'RunningPointBalance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级。 （库存不足情况下。按 从小到大 优先分配。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon，Card自动补货设置子表。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InventoryReplenishRule_D'
GO
