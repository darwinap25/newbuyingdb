USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetStoreList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetStoreList]
  @StoreID				int,			   --店铺ID
  @BrandID              int,               --品牌ID，
  @CountryCode          varchar(64),       --国家Code，
  @ProvinceCode         varchar(64),       --省Code，
  @CityCode             varchar(64),       --城市Code，
  @DistrictCode         varchar(64),       --区县Code，
  @StoreTypeID          int,               --店铺类型ID
  @ConditionStr			nvarchar(1000),    --自定义条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。   
  @LanguageAbbr			varchar(20)='',
  @Attribute            varchar(512)=''    -- 店铺属性条件。内容为Store_Attribute表的StoreAttributeCode，多个内容用'|'隔开，每个code用两层单引号括起来，例如： ' ''SA01'',''SA02'' '
AS
/****************************************************************************
**  Name : GetStoreList
**  Version: 1.0.0.14
**  Description :查找SVA中的receiveTxn 和 Card_Movement
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetStoreList null, null, '','','HK','',null,'', 1, 30, @count output, @recordcount output, 'zh_BigCN', ''
  print @a  
  print @count
  print @recordcount
  select * from Store 
  sp_helptext SelectDataInBatchs 
**  Created by: Gavin @2012-05-23
**  Modify by: Gavin @2012-09-21(ver 1.0.0.1) 增加输入参数brandcode，可以根据brandcode筛选记录
**  Modify by: Gavin @2012-09-26 (ver 1.0.0.2) @BrandCode 参数传入的是 BrandID, 参数名称暂时不变，修改处理过程
**  Modify by: Gavin @2012-10-10 (ver 1.0.0.3) 修改传入参数@BrandCode varchar(64) 为 @BrandID int   
**  Modify by: Gavin @2013-01-23 (ver 1.0.0.4) 增加输入的过滤条件  @CountryCode,@ProvinceCode,@CityCode,@DistrictCode
**  Modify by: Gavin @2013-01-29 (ver 1.0.0.5) 增加输入的过滤条件 @StoreTypeID, 更新ViewStores,添加storetypecode
**  Modify by: Gavin @2013-05-17 (ver 1.0.0.6) for SASA, 从Lan_Store 中读取数据。
**  Modify by: Gavin @2013-05-23 (ver 1.0.0.7) for SASA, ViewStores恢复原版本, store增加多语言地址字段,取消从Lan_store读取
**  Modify by: Gavin @2013-05-28 (ver 1.0.0.8) 修正languageid = 2 时的bug
**  Modify by: Gavin @2013-06-21 (Ver 1.0.0.9) 使用1.0.0.6版本的SelectDataInBatchs	,输入的@ConditionStr直接传给SelectDataInBatchs 
**  Modify by: Gavin @2013-07-24 (Ver 1.0.0.10) 增加返回字段: email. (ViewStore同步修改,增加email)
**  Modify by: Gavin @2013-12-19 (Ver 1.0.0.11) 增加返回字段PickupStoreFlag，前台可以不获取，只需要@ConditionStr 中作为条件即可
**  Modify by: Gavin @2014-07-04 (Ver 1.0.0.12) 增加输入参数@Attribute，增加返回字段StoreAttribute1,StoreAttributePic1 ....
**  Modify by: Gavin @2014-07-29 (Ver 1.0.0.13) 修正不限制返回行数时出现的bug 
**  Modify by: Gavin @2016-01-14 (Ver 1.0.0.14) 增加返回字段: ReturnStoreFlag int (ViewStore同步修改,增加ReturnStoreFlag)
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @StoreAttributeCode varchar(64)   
  declare @SA01 varchar(64), @SA02 varchar(64), @SA03 varchar(64), @SA04 varchar(64), @SA05 varchar(64)
          , @SA06 varchar(64), @SA07 varchar(64), @SA08 varchar(64), @SA09 varchar(64), @SA10 varchar(64)
  declare @@AttributeTemp varchar(64)          
  
  set @StoreID = isnull(@StoreID, 0)
  if ISNULL(@Attribute, '') = ''
  begin
    set @Attribute = '''''' 
    set @@AttributeTemp = ''''''
  end  
  if @Attribute <> ''
    set @Attribute = replace(@Attribute, '|', ',')
 
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1

  set @BrandID = isnull(@BrandID, 0)
  --select @BrandID = BrandID from brand where BrandCode = @BrandCode
  --set @BrandID = cast(isnull(@BrandCode, 0) as int)

  DECLARE CUR_Store_Attribute CURSOR fast_forward for
    select top 10 SACode from Store_Attribute
  OPEN CUR_Store_Attribute
  FETCH FROM CUR_Store_Attribute INTO @StoreAttributeCode
  WHILE @@FETCH_STATUS=0 
  BEGIN 
    set @StoreAttributeCode = isnull(@StoreAttributeCode, '')
    if ISNULL(@SA01, '') = ''
      set @SA01 = @StoreAttributeCode
    else  if ISNULL(@SA02, '') = ''
      set @SA02 = @StoreAttributeCode 
    else  if ISNULL(@SA03, '') = ''
      set @SA03 = @StoreAttributeCode  
    else  if ISNULL(@SA04, '') = ''
      set @SA04 = @StoreAttributeCode  
    else  if ISNULL(@SA05, '') = ''
      set @SA05 = @StoreAttributeCode  
    else  if ISNULL(@SA06, '') = ''
      set @SA06 = @StoreAttributeCode  
    else  if ISNULL(@SA07, '') = ''
      set @SA07 = @StoreAttributeCode  
    else  if ISNULL(@SA08, '') = ''
      set @SA08 = @StoreAttributeCode  
    else  if ISNULL(@SA09, '') = ''
      set @SA09 = @StoreAttributeCode  
    else  if ISNULL(@SA10, '') = ''
      set @SA10 = @StoreAttributeCode                                                                
  	FETCH FROM CUR_Store_Attribute INTO @StoreAttributeCode
  END
  CLOSE CUR_Store_Attribute 
  DEALLOCATE CUR_Store_Attribute 

  if @Language = 2
    set @SQLStr = ' select StoreName2 as StoreName, StoreTypeName2 as StoreTypeName, StoreGroupName2 as StoreGroupName, LocationName2 as LocationName, BrandName2 as BrandName, '
         + ' V.StoreID, StoreCode, StoreName1, StoreName2, StoreName3, StoreTypeID, StoreGroupID, BankID, BrandID, StoreCountry, StoreProvince, StoreCity, StoreAddressDetail2 as StoreAddressDetail, '
         + ' StoreTel, StoreFax, StoreLongitude, StoreLatitude, LocationID, StoreNote, StoreOpenTime, StoreCloseTime, MapsPicFile, MapsPicShadowFile, '
         + ' StoreTypeName1, StoreTypeName2, StoreTypeName3, StoreGroupName1, StoreGroupName2, StoreGroupName3, BrandName1, BrandName2, BrandName3, CardIssuerID, IndustryID, '
         + ' ParentLoactionID, LocationName1, LocationName2, LocationName3, LocationType, Lotitude, Longitude, Status, LocationFullPath, StorePicFile, StoreDistrict, StoreFullDetail2 as StoreFullDetail, '
         + ' StoreTypeCode, Email, PickupStoreFlag, ReturnStoreFlag '         
  else if @Language = 3	
    set @SQLStr = ' select StoreName3 as StoreName, StoreTypeName3 as StoreTypeName, StoreGroupName3 as StoreGroupName, LocationName3 as LocationName, BrandName3 as BrandName, '
         + ' V.StoreID, StoreCode, StoreName1, StoreName2, StoreName3, StoreTypeID, StoreGroupID, BankID, BrandID, StoreCountry, StoreProvince, StoreCity, StoreAddressDetail3 as StoreAddressDetail, '
         + ' StoreTel, StoreFax, StoreLongitude, StoreLatitude, LocationID, StoreNote, StoreOpenTime, StoreCloseTime, MapsPicFile, MapsPicShadowFile, '
         + ' StoreTypeName1, StoreTypeName2, StoreTypeName3, StoreGroupName1, StoreGroupName2, StoreGroupName3, BrandName1, BrandName2, BrandName3, CardIssuerID, IndustryID, '
         + ' ParentLoactionID, LocationName1, LocationName2, LocationName3, LocationType, Lotitude, Longitude, Status, LocationFullPath, StorePicFile, StoreDistrict, StoreFullDetail3 as StoreFullDetail, '
         + ' StoreTypeCode, Email, PickupStoreFlag, ReturnStoreFlag '
  else
    set @SQLStr = ' select StoreName1 as StoreName, StoreTypeName1 as StoreTypeName, StoreGroupName1 as StoreGroupName, LocationName1 as LocationName, BrandName1 as BrandName, '
         + ' V.StoreID, StoreCode, StoreName1, StoreName2, StoreName3, StoreTypeID, StoreGroupID, BankID, BrandID, StoreCountry, StoreProvince, StoreCity, StoreAddressDetail, '
         + ' StoreTel, StoreFax, StoreLongitude, StoreLatitude, LocationID, StoreNote, StoreOpenTime, StoreCloseTime, MapsPicFile, MapsPicShadowFile, '
         + ' StoreTypeName1, StoreTypeName2, StoreTypeName3, StoreGroupName1, StoreGroupName2, StoreGroupName3, BrandName1, BrandName2, BrandName3, CardIssuerID, IndustryID, '
         + ' ParentLoactionID, LocationName1, LocationName2, LocationName3, LocationType, Lotitude, Longitude, Status, LocationFullPath, StorePicFile, StoreDistrict, StoreFullDetail, '
         + ' StoreTypeCode, Email, PickupStoreFlag, ReturnStoreFlag  '    
  set @SQLStr = @SQLStr + ',T.StoreAttribute1,T.StoreAttributePic1,T.StoreAttribute2,T.StoreAttributePic2,'
         + ' T.StoreAttribute3,T.StoreAttributePic3,T.StoreAttribute4,T.StoreAttributePic4,'
         + ' T.StoreAttribute5,T.StoreAttributePic5,T.StoreAttribute6,T.StoreAttributePic6,'
         + ' T.StoreAttribute7,T.StoreAttributePic7,T.StoreAttribute8,T.StoreAttributePic8,'
         + ' T.StoreAttribute9,T.StoreAttributePic9,T.StoreAttribute10,T.StoreAttributePic10'
         + ' from ViewStores V left join (select A.StoreID, '
         + ' MAX(case when SACode='''+isnull(@SA01,'')+''' then SACode else '''' end) as StoreAttribute1,'
         + ' MAX(case when SACode='''+isnull(@SA01,'')+''' then SAPIC else '''' end) as StoreAttributePic1, '
         + ' MAX(case when SACode='''+isnull(@SA02,'')+''' then SACode else '''' end) as StoreAttribute2,'
         + ' MAX(case when SACode='''+isnull(@SA02,'')+''' then SAPIC else '''' end) as StoreAttributePic2,'
         + ' MAX(case when SACode='''+isnull(@SA03,'')+''' then SACode else '''' end) as StoreAttribute3,'
         + ' MAX(case when SACode='''+isnull(@SA03,'')+''' then SAPIC else '''' end) as StoreAttributePic3,'
         + ' MAX(case when SACode='''+isnull(@SA04,'')+''' then SACode else '''' end) as StoreAttribute4, '
         + ' MAX(case when SACode='''+isnull(@SA04,'')+''' then SAPIC else '''' end) as StoreAttributePic4,'
         + ' MAX(case when SACode='''+isnull(@SA05,'')+''' then SACode else '''' end) as StoreAttribute5,'
         + ' MAX(case when SACode='''+isnull(@SA05,'')+''' then SAPIC else '''' end) as StoreAttributePic5,'
         + ' MAX(case when SACode='''+isnull(@SA06,'')+''' then SACode else '''' end) as StoreAttribute6,'
         + ' MAX(case when SACode='''+isnull(@SA06,'')+''' then SAPIC else '''' end) as StoreAttributePic6,'
         + ' MAX(case when SACode='''+isnull(@SA07,'')+''' then SACode else '''' end) as StoreAttribute7,'
         + ' MAX(case when SACode='''+isnull(@SA07,'')+''' then SAPIC else '''' end) as StoreAttributePic7,'
         + ' MAX(case when SACode='''+isnull(@SA08,'')+''' then SACode else '''' end) as StoreAttribute8,'
         + ' MAX(case when SACode='''+isnull(@SA08,'')+''' then SAPIC else '''' end) as StoreAttributePic8,'
         + ' MAX(case when SACode='''+isnull(@SA09,'')+''' then SACode else '''' end) as StoreAttribute9, '
         + ' MAX(case when SACode='''+isnull(@SA09,'')+''' then SAPIC else '''' end) as StoreAttributePic9,'
         + ' MAX(case when SACode='''+isnull(@SA10,'')+''' then SACode else '''' end) as StoreAttribute10,'
         + ' MAX(case when SACode='''+isnull(@SA10,'')+''' then SAPIC else '''' end) as StoreAttributePic10'                                  
         + ' from ('
         + 'select A.StoreID, B.SACode, B.SAPic, B.SADesc1, B.SADesc2, B.SADesc3 '
         + ' from StoreAttributeList A left join Store_Attribute B on A.SAID = B.SAID '
         + ') A '
         + ' group by A.StoreID '
         + ') T on V.StoreID = T.StoreID'

  set @SQLStr = @SQLStr + ' where (V.StoreID = ' + convert(varchar(10), @StoreID) + ' or ' + convert(varchar(10), @StoreID) + ' = 0)'  
  set @SQLStr = @SQLStr + ' and (BrandID = ' + cast(isnull(@BrandID,0) as varchar) + ' or ''' + cast(isnull(@BrandID,0) as varchar) + ''' = 0)'
  set @SQLStr = @SQLStr + ' and (StoreCountry = ''' + @CountryCode + ''' or ''' + isnull(@CountryCode,'') + ''' = '''')'
  set @SQLStr = @SQLStr + ' and (StoreProvince = ''' + @ProvinceCode + ''' or ''' + isnull(@ProvinceCode,'') + ''' = '''')'
  set @SQLStr = @SQLStr + ' and (StoreCity = ''' + @CityCode + ''' or ''' + isnull(@CityCode,'') + ''' = '''')'
  set @SQLStr = @SQLStr + ' and (StoreDistrict = ''' + @DistrictCode + ''' or ''' + isnull(@DistrictCode,'') + ''' = '''')'
  set @SQLStr = @SQLStr + ' and (StoreTypeID = ' + cast(isnull(@StoreTypeID,0) as varchar) + ' or ''' + cast(isnull(@StoreTypeID,0) as varchar) + ''' = 0)'
  set @SQLStr = @SQLStr + ' and ((' + @@AttributeTemp + ' = '''') or(V.StoreID in (select A.StoreID from StoreAttributeList A left join Store_Attribute B on A.SAID = B.SAID where B.SACode in (' + @Attribute + ')))) '


--exec sp_executesql @SQLStr

  exec SelectDataInBatchs @SQLStr, 'StoreID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr

  return 0
end

GO
