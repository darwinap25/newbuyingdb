USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoTradeManually]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoTradeManually]
  @TradeManuallyCode    varchar(64),
  @UserID               int,
  @CardNumber           varchar(64),
  @BusDate              datetime = null,
  @TxnDate              datetime = null,  
  @ApprovalCode         char(6) output
AS
/****************************************************************************
**  Name : DoTradeManually
**  Version: 1.0.0.2
**  Description : Ord_TradeManually_H 触发器中调用，用于实现添加会员积分
**
**  Parameter :	 select * from card_movement
  declare @a int  
  exec @a = DoPushCoupon '', 0
  print @a  
**  Created by: Gavin @2013-12-02
**  Modify by: Gavin @2014-10-27 (ver 1.0.0.2) (for 伊盈化妆品) 上传交易号码, 获取积分。判断第一笔交易时，需要奖励推荐者
**
****************************************************************************/
begin
  declare @AddPoint int, @TxnNo varchar(64), @StoreID int, @RegisterCode varchar(64), @ServerCode varchar(64)
  declare @ReferCardNumber varchar(64), @MemberID int
   
  set @AddPoint = isnull(@AddPoint, 0)
  exec GenApprovalCode @ApprovalCode output
  
  DECLARE CUR_Ord_TradeManually_D CURSOR fast_forward FOR
    SELECT TradeManuallyCode, StoreID, EarnPoint, ReferenceNo FROM Ord_TradeManually_D
     where TradeManuallyCode = @TradeManuallyCode
  OPEN CUR_Ord_TradeManually_D
  FETCH FROM CUR_Ord_TradeManually_D INTO @TradeManuallyCode, @StoreID, @AddPoint, @TxnNo
  WHILE @@FETCH_STATUS=0
  BEGIN
    -- ver 1.0.0.2
    if not exists(select KeyID from Card_Movement where CardNumber = @CardNumber and OprID = 25)
    begin
      select @MemberID = MemberID from Card where CardNumber = @CardNumber  
      select @ReferCardNumber = ReferCardNumber from Member where MemberID = @MemberID 
      set @ReferCardNumber = isnull(@ReferCardNumber, '')
      if @ReferCardNumber <> ''
        exec DoSVAReward @ReferCardNumber, TradeManuallyCode, 6
    end
    
    insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus, ApprovalCode)
	  select 25, @CardNumber, null, 0, @TradeManuallyCode, TotalAmount, 0, TotalAmount, @AddPoint, @BusDate, @Txndate, 
	      null, null, null, null, '', @UserID, @StoreID, @RegisterCode, @ServerCode,
	      TotalPoints, isnull(TotalPoints, 0) + @AddPoint, CardExpiryDate, CardExpiryDate, Status, Status, @ApprovalCode
	    from card where CardNumber = @CardNumber   
    FETCH FROM CUR_Ord_TradeManually_D INTO @TradeManuallyCode, @StoreID, @AddPoint, @TxnNo
  END
  CLOSE CUR_Ord_TradeManually_D 
  DEALLOCATE CUR_Ord_TradeManually_D    
 
end

GO
