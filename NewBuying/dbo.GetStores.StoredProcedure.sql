USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetStores]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStores]
  @StoreCode				     VARCHAR(64),			         --店铺Code
  @CountryCode           VARCHAR(64),              --国家Code，
  @ProvinceCode          VARCHAR(64),              --省Code，
  @CityCode              VARCHAR(64),              --城市Code，
  @DistrictCode          VARCHAR(64),              --区县Code，
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetStores
**  Version : 1.0.0.1
**  Description : 获得店铺数据
**
  declare @a int, @PageCount int, @RecordCount int
  exec @a = GetStores 'Bauhaus214', '','','','',   '','',1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  select * from buy_store

** Created by Gavin @2015-03-06 
** Modify By Gavin @2016-11-25 (ver 1.0.0.1) BUY_STORE结构修改,做同步修改 (@2016-08-09 删除BrandCode（货品品牌）， 加上StoreBrandCode（店铺品牌）)
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)    
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1
  SET @StoreCode = ISNULL(@StoreCode, '')
  SET @CountryCode = ISNULL(@CountryCode, '')
  SET @ProvinceCode = ISNULL(@ProvinceCode, '')
  SET @CityCode = ISNULL(@CityCode, '')
  SET @DistrictCode = ISNULL(@DistrictCode, '')
  SET @WhereStr = ' WHERE Status = 1 '

  IF @StoreCode <> ''  
    SET @WhereStr = @WhereStr + ' AND StoreCode=''' + @StoreCode + ''' '   
  IF @CountryCode <> ''  
    SET @WhereStr = @WhereStr + ' AND StoreCountry=''' + @CountryCode + ''' '   
  IF @ProvinceCode <> ''  
    SET @WhereStr = @WhereStr + ' AND StoreProvince=''' + @ProvinceCode + ''' '   
  IF @CityCode <> ''  
    SET @WhereStr = @WhereStr + ' AND StoreCity=''' + @CityCode + ''' '   
  IF @DistrictCode <> ''  
    SET @WhereStr = @WhereStr + ' AND StoreDistrict=''' + @DistrictCode + ''' '    
                    
  SET @SQLStr = 'SELECT StoreID, StoreCode, CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN StoreName2 WHEN 3 THEN StoreName3 ELSE StoreName1 END AS StoreName, '
    + ' StoreTypeID,BankCode, StoreBrandCode as BrandCode, StoreCountry, StoreProvince, StoreCity, StoreDistrict, '
    + ' CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN StoreAddressDetail2 WHEN 3 THEN StoreAddressDetail3 ELSE StoreAddressDetail1 END AS StoreAddressDetail, '
    + ' CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN StoreFullDetail2 WHEN 3 THEN StoreFullDetail3 ELSE StoreFullDetail1 END AS StoreFullDetail, '
    + ' StoreTel, StoreFax, StoreEmail, Contact, ContactPhone, StoreLongitude, StoreLatitude, StorePicFile, MapsPicFile, MapsPicShadowFile, '
    + ' LocationCode, StoreNote, StoreOpenTime, StoreCloseTime, Comparable, GLCode, RegionCode, OrgCode, StoreIP, SubInvCode '
    + ' FROM BUY_STORE '
    + @WhereStr

  EXEC SelectDataInBatchs @SQLStr, 'StoreCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr

  RETURN 0
END

GO
