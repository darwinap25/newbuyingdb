USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenSalesTag]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenSalesTag]
  @UserID   int,                -- 操作员ID
  @TxnNo    varchar(512),
  @SalesTag varchar(512) output
AS
/****************************************************************************
**  Name : GenSalesTag  
**  Version: 1.0.0.0
**  Description : 产生Sales交易的Tag数据，更新到Sales表。
**  Parameter :
  declare  @SalesTag varchar(512), @a int  
  exec @a = GenSalesTag 2, 'KTXNNO000000147', @SalesTag output 
  print @a  
  print  @SalesTag
select * from sales_h where txnno = 'KTXNNO000000147'		KTXNNO000000147,45.00
**  Created by: Gavin @2013-02-04
**
****************************************************************************/
begin
  declare @TotalAmount money, @ProdCode varchar(64)
  
  set @SalesTag = ''
  select @TotalAmount = TotalAmount from sales_h where TransNum = @TxnNo
  if @@ROWCOUNT = 0
    return -91
  set @TotalAmount = isnull(@TotalAmount, 0)
  set @SalesTag = isnull(@TxnNo, '') + ',' + cast(@TotalAmount as varchar)

  DECLARE CUR_SalesD CURSOR  fast_forward local FOR
      select ProdCode from sales_D where TransNum = @TxnNo 
  OPEN CUR_SalesD
  FETCH FROM CUR_SalesD INTO @ProdCode
  WHILE @@FETCH_STATUS=0
  BEGIN
    set @SalesTag = @SalesTag + ',' + @ProdCode
    FETCH FROM CUR_SalesD INTO @ProdCode
  END
  CLOSE CUR_SalesD 
  DEALLOCATE CUR_SalesD   
  
  if @SalesTag <> ''
    update Sales_H set SalesTag = @SalesTag, UpdatedOn = getdate(), UpdatedBy = @UserID  where TransNum = @TxnNo

  return 0
end

GO
