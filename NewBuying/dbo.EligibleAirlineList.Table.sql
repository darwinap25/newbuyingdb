USE [NewBuying]
GO
/****** Object:  Table [dbo].[EligibleAirlineList]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EligibleAirlineList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EligibleAirlineCode] [varchar](64) NOT NULL,
	[EligibleAirlineName] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
