USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SyncSalesStatusToBUY]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SyncSalesStatusToBUY]
  @UserID				         int,		        -- 操作员ID
  @TxnNo                         VARCHAR(64)       -- 交易号码
AS
/*==============================================================*/
/*                
* Name: SyncSalesStatusToBUY 
* Version: 1.0.0.0
* Description : (for bauhaus) 使用linkserver, 同步Buying DB 的是sales_H ->status 到 SVA DB 的 sales_H ->status
                SVA的Sales_H的 Status 将和 buying 的一样。
select * from sales_H
exec SyncSalesStatusToBUY 1,'01SOP023220151217091205'

* Create By Gavin @2017-03-06
*/
/*==============================================================*/ 
BEGIN
  DECLARE @TXNSTATUS INT 
  SELECT @TXNSTATUS = Status FROM SALES_H WHERE TransNum = @TxnNo
  UPDATE LINKSVA.SVA_Bauhaus.dbo.Sales_H SET STATUS = @TXNSTATUS WHERE TxnNo = @TxnNo
END

GO
