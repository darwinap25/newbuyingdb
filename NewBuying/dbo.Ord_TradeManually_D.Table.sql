USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_TradeManually_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_TradeManually_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[TradeManuallyCode] [varchar](64) NOT NULL,
	[StoreID] [int] NULL,
	[TraderAmount] [money] NULL,
	[EarnPoint] [int] NULL,
	[ReferenceNo] [varchar](64) NULL,
	[Receipt] [varchar](512) NULL,
 CONSTRAINT [PK_ORD_TRADEMANUALLY_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D', @level2type=N'COLUMN',@level2name=N'TradeManuallyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D', @level2type=N'COLUMN',@level2name=N'TraderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'获得的积分。（根据积分规则计算获得）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D', @level2type=N'COLUMN',@level2name=N'EarnPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上传的小票文件地址.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D', @level2type=N'COLUMN',@level2name=N'Receipt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手工交易单。 子表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_TradeManually_D'
GO
