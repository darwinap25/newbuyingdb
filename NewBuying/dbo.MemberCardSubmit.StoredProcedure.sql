USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardSubmit]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[MemberCardSubmit]
  @UserID		        varchar(10),			-- 登录用户
  @CardNumber			varchar(512),			-- 会员卡号
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),           -- 终端号码
  @ServerCode           varchar(512)='',		-- serverid 暂时用不到,预留
  @BrandCode			varchar(512),			-- 品牌编码
  @TxnNoKeyID           varchar(512),			-- ReceiveTxn 表主键
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间
  @OprID		        int,					-- 操作类型
  @Amount		        money,					-- 操作金额
  @Point		        int,					-- 操作积分
  @VoidTxnNo            varchar(512),			-- 原单交易单号
  @SecurityCode         varchar(512),			-- 签名
  @TenderCode			varchar(512),			-- 使用的Tender code。（如果是银行卡做支付，）
  @Additional           varchar(512),			-- 附加信息（如果是银行卡做支付，此处记录mask card number）
  @ApprovedCode         varchar(512),			-- 如果是银行卡做支付，此处记录批核号
  @ReturnAmount         money output,			-- 操作完成后卡金额余额
  @ReturnPoint          int output,				-- 操作完成后卡积分余额
  @CardExpiryDate       datetime output,		-- 操作完成后卡的有效期（金额最高有效期）。
  @ReturnMessageStr		varchar(max) output,	-- 返回信息。（json格式）
  @NeedPassword         int=0,		            -- 是否验证密码。0：不需要。1：需要
  @CardPassword         varchar(512)='',		-- 卡密码
  @NeedCallConfirm      int=0                   -- 是否需要调用POSTxnConfirm。0：不需要。 1：需要。  默认0
as
/****************************************************************************
**  Name : MemberCardSubmit  
**  Version: 1.0.0.13
**  Description : 对于会员卡操作请求入口（充值，消费，积分消费，购买积分，void操作，作废卡，return操作）
         在此函数中计算消费增加的积分, 兑换积分时需要的金额， 写入receivetxn 的 amount 和point 中
**  Parameter :
  declare @ReturnAmount money, @ReturnPoint int, @CardExpiryDate datetime, @Result int,	 @ReturnMessageStr varchar(max)    
  exec @Result = MemberCardSubmit '1', '222222200000003', '0001', 'store1', '001', '01', '201203260010002', '20120326002', '2012-03-26', '2012-08-22 12:12:12', 2, 100, 0, '', '', '', '', '', @ReturnAmount output, @ReturnPoint output, @CardExpiryDate output, @ReturnMessageStr output
  print @Result    
  print @ReturnAmount
  print @ReturnPoint
  print @CardExpiryDate
  print @ReturnMessageStr
select * from card where cardnumber = '000300025'
select top 10 * from card_movement order by keyid desc
update receivetxn set approvestatus = 'A' where keyid = 391
select top 10 * from receivetxn order by keyid desc
sp_helptext MemberCardSubmit
**  Created by: Gavin @2012-03-06
**  Modify by: Gavin @2012-07-20 （ver 1.0.0.1）增加BrandCode输入
**  Modify by: Gavin @2012-08-22 （ver 1.0.0.2）增加单笔购买积分范围判断，增加积分存储上限判断
**  Modify by: Gavin @2012-10-22 （ver 1.0.0.3）不传入Busdate，由存储过程自己获取
**  Modify by: Gavin @2013-04-27 （ver 1.0.0.4）增加password校验,增加参数NeedPassword, CardPassword.
**  Modify by: Gavin @2013-08-07 （ver 1.0.0.5）卡消费金额时,不再增加积分.
**  Modify by: Gavin @2013-08-23 （ver 1.0.0.6）增加返回操作信息字符串.
**  Modify by: Gavin @2014-05-28 （ver 1.0.0.7) 增加参数@NeedCallConfirm， 0：不需要call confirm， 1：需要confirm。 默认0
**  Modify by: Gavin @2014-07-08 （ver 1.0.0.8) 输入的@Amount可能会存在负数.(当做无原单取消前操作操作时). 如果是负数,判断OpriD,如果是2,或者3, 则是作废原单,OprID自动转为8
                                                即：充值和消费操作，输入的金额不得为负数。 一旦为负数，则判断为return操作。 自动转换成 OprID=8
**  Modify by: Gavin @2014-07-08 （ver 1.0.0.9) 根据card status 状态的更改，调整过程中的状态判断
**  Modify by: Gavin @2014-08-07 （ver 1.0.0.10)修正充值时(oprid=2),卡上限校验没有实现的问题.
**  Modify by: Gavin @2014-08-14 （ver 1.0.0.11)检查交易数据.(比如充值时,这一单交易总值是否超过上限等.)(未批核,同一单)
**  Modify by: Gavin @2014-08-26 （ver 1.0.0.12)return操作时（OprID=8），检查是否余额为负数， 是的话返回 -10
**  Modify by: Gavin @2014-09-22 （ver 1.0.0.13) 因为有其他OprID的调用,所以@Status默认填写0
** ver 1.0.0.14 是RRG Telco的修改, 通用版本不需要. 版本号跳过.
**  Modify by: Gavin @2015-08-05 （ver 1.0.0.15) 消费积分(OprID=13), 写入receivetxn 的point 改为负数
**
****************************************************************************/
begin
  declare @OldCardExpiryDate datetime, @MemberID varchar(36), @CardGradeID nvarchar(10), @CardTypeID int,
          @BatchID varchar(30), @TotalPoints int, @TotalAmount money, @CardStatus int, @Status int,
          @NewAmt money, @MaxAmt money, @OpenBal money, @CloseBal money, @NewPoints int, @OpenPoints int, @ClosePoints int,
          @NewCardExpiryDate datetime, @NewAmountExpiryDate datetime, @NewPointExpiryDate datetime, @IsMultiExpiry int,
          @CalcAddType int,  @CalcAddValue money, @VoidKeyID varchar(30), @MaxAmtReturn money, @CalcResult int,
          @IsAllowStoreValue int, @TenderID int, @CardGradeMaxAmount money   
  declare @MinAmountPreAdd money, @MaxAmountPreAdd money, @MinBalanceAmount money, @MinBalancePoint int, @MinConsumeAmount money, 
		  @CardConsumeBasePoint int, @IsAllowConsumptionPoint int, @MinPointPreAdd int, @MaxPointPreAdd int, @CardGradeMaxPoint int
  declare @MD5CardPWD varchar(512), @CardPWD varchar(512)
  declare @TxnAmt money   -- ver 1.0.0.11 
  set @Status = 0
    		            
  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120) + cast(@OprID as varchar) + cast(cast(floor(@Amount) as int) as varchar)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
    return @CheckResult
  -- end
  
  -- 取busdate
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 
  if isnull(@TxnDate, 0) = 0
    set @TxnDate = getdate()
   
  -- ver 1.0.0.8
  if @Amount < 0 and @OprID in (2,3)
  begin
    if @OprID = 3
      set @Amount = -@Amount
    set @OprID = 8
  end
      
  -- 取卡数据  
  select top 1 @MemberID = MemberID, @CardNumber = CardNumber, @CardTypeID = C.CardTypeID,  @CardStatus = C.Status, @OldCardExpiryDate = CardExpiryDate, 
       @CardGradeID = C.CardGradeID, @TotalAmount = isnull(TotalAmount, 0), @TotalPoints = isnull(TotalPoints, 0),
       @IsAllowStoreValue = isnull(G.IsAllowStoreValue, 1), 
       @MinAmountPreAdd = isnull(MinAmountPreAdd, 0), @MaxAmountPreAdd = isnull(MaxAmountPreAdd, 0), 
       @MinBalanceAmount = isnull(MinBalanceAmount, 0), @MinBalancePoint = isnull(MinBalancePoint, 0), 
       @MinConsumeAmount = isnull(MinConsumeAmount, 0), @CardGradeMaxAmount = isnull(CardGradeMaxAmount, 0),
       @CardConsumeBasePoint = isnull(CardConsumeBasePoint, 0), @IsAllowConsumptionPoint = isnull(IsAllowConsumptionPoint, 0),
       @MinPointPreAdd = isnull(MinPointPreAdd, 0), @MaxPointPreAdd = isnull(MaxPointPreAdd, 0),
       @CardGradeMaxPoint = isnull(CardGradeMaxPoint, 0), @CardPWD = CardPassword
    from Card C left join CardType T on C.CardTypeID = T.CardTypeID
      left join CardGrade G on C.CardGradeID = G.CardGradeID
    where CardNumber = @CardNumber    
  if @@ROWCOUNT = 0
    return -2
  if isnull(@TenderCode, '') <> ''
  begin
    select @TenderID = TenderID from tender where TenderCode = @TenderCode    
  end    

  if isnull(@NeedPassword, 0) = 1
  begin
    set @MD5CardPWD = dbo.EncryptMD5(@CardPassword)
    if @MD5CardPWD <> @CardPWD
      return -4
  end  
  
  -- ver 1.0.0.11  获得本单交易中未批核的金额合计值  
  select @TxnAmt = SUM(case when OprID in (2,8) then isnull(Amount,0) when OprID = 3 then -isnull(Amount,0) else 0 end ) from ReceiveTxn 
    where TxnNo = @TxnNo and CardNumber = @CardNumber and [Status] = 0 and ApproveStatus = 'P'
  set @TxnAmt = ISNULL(@TxnAmt, 0)  
  --------------------------------
  
  exec CalcExpiryDate @OprID, @CardNumber, @Amount, @CardExpiryDate output, 
                      @NewAmountExpiryDate output, @NewPointExpiryDate output, @IsMultiExpiry output
    
  set @NewAmt = @TotalAmount
  set @NewPoints = @TotalPoints
  set @OpenPoints = 0
  set @ClosePoints = 0
  
  -- 充值金额 （业务逻辑：如已过期则自动激活，并根据设置，自动延长expiry。 如果已经void，则返回错。）
  if @OprID = 2 or @OprID = 12
  begin 
    if @IsAllowStoreValue = 1
    begin           
      if (@CardStatus in (2, 3, 4))
      begin
        set @Status = 0        
        set @NewAmt = @TotalAmount + @Amount
        -- 判断是否超过卡金额上限
        if @CardGradeMaxAmount > 0    -- 如果没有设置，或者是0，则表示没有上限
        begin
          if @NewAmt + @TxnAmt > @CardGradeMaxAmount 
            set @Status = -14
        end
        -- 判断是否超过最大单笔充值金额上限, 如果没有设置，或者是0，则表示没有上限
        if @MaxAmountPreAdd > 0 and @Amount > @MaxAmountPreAdd    
          set @Status = -26        
        -- 判断是否低于最小单笔充值金额, 如果没有设置，或者是0，则表示没有现在
        if @MinAmountPreAdd > 0 and @Amount < @MinAmountPreAdd        
          set @Status = -39
        set @OpenBal = @TotalAmount
        set @CloseBal = @NewAmt       
        set @ReturnAmount = @NewAmt   
        set @ReturnPoint = @TotalPoints          
      end else
        set @Status = -6  
    end else
      set @Status = -22    
  end  
  
  -- 消费金额
  if @OprID = 3    
  begin
      if (@CardStatus in (2, 3))
      begin
        set @NewAmt = @TotalAmount - @Amount
        if @Amount < @MinConsumeAmount
        begin  
          set @ReturnAmount = @TotalAmount 
          set @Status = -55             -- 不能低于最低消费金额
        end else if (@NewAmt + @TxnAmt) < @MinBalanceAmount and @NewAmt >= 0
        begin
          set @ReturnAmount = @TotalAmount 
          set @Status = -56             -- 余额将小于最低剩余金额
        end else if @NewAmt < 0 
        begin
          set @ReturnAmount = @TotalAmount 
          set @Status = -10             -- 金额不足
        end else
        begin 
          set @Point = 0
          set @NewPoints = @TotalPoints + isnull(@Point, 0)          
          set @OpenPoints = @TotalPoints
          set @ClosePoints = @NewPoints 
          set @OpenBal = @TotalAmount
          set @CloseBal = @NewAmt
          set @Amount = -@Amount
          set @VoidKeyID = ''
          set @ReturnAmount = @NewAmt
          set @ReturnPoint = @NewPoints
          set @Status = 0
        end 
        set @ReturnPoint = @TotalPoints   
      end else
        set @Status = -6
  end
  
  -- void 根据@VoidTxnNo 查找原单。 金额返回到原卡。（输入参数的CardNumber必须和原单中的卡号相同）
  if @OprID = 4    
  begin
      if (@CardStatus in (2, 3)) 
      begin
        declare @VoidAmt float, @VoidRecStatus int, @VoidOprID int, @RefCardNo nvarchar(30), @VoidPoints int        
        select top 1 @VoidAmt = abs(isnull(Amount, 0)), @VoidKeyID = KeyID, @VoidRecStatus = [Status], @VoidOprID = OprID,
            @RefCardNo = CardNumber, @VoidPoints = abs(isnull(Points, 0)) 
          from ReceiveTxn where TxnNo = @VoidTxnNo and [Status] = 0
        if (@@ROWCOUNT = 0) or (isnull(@VoidTxnNo, '') = '')
          set @Status = -11  -- 没有找到原单
        else
        begin
          if @VoidRecStatus = 1 
            set @Status = -12  -- 原单已经void
          else
          begin 
            if (@VoidOprID = 1 or @VoidOprID = 4) 
              set @Status = -13  -- 原单是查询或void单，不能再void
            else
            begin
              if (@CardNumber = @RefCardNo)
              begin                
                -- 从原单中取操作金额，积分
                set @NewAmt = @TotalAmount - @VoidAmt
                set @NewPoints = @NewPoints - @VoidPoints                              
                if @NewAmt < 0 or @NewPoints < 0
                begin
				          set @ReturnAmount = @TotalAmount   
                  set @ReturnPoint = @TotalPoints                    
                  set @Status = -10             -- 金额不足 (充值100，消费50，void充值100的交易，这将不允许)
                end else
                begin
                  set @OpenBal = @TotalAmount
                  set @CloseBal = @NewAmt
                  set @OpenPoints = @TotalPoints
                  set @ClosePoints = @NewPoints                  
                  set @Amount = -@VoidAmt
				          set @ReturnAmount = @NewAmt   
                  set @ReturnPoint = @NewPoints 
                  set @Point = -@VoidPoints                   
                  set @Status = 0                   
                  -- 判断是否超过卡金额上限
                  if isnull(@CardGradeID, '') <> ''        
                  begin          
                    select @MaxAmtReturn = isnull(CardGradeMaxAmount,0) from CardGrade where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
                    if @MaxAmtReturn > 0    -- 如果没有设置，或者是0，则表示没有上限
                    begin
                      if @NewAmt > @MaxAmtReturn 
                        set @Status = -14
                    end
                  end                  
                end
              end
              else
              begin
                set @Status = -16   -- 输入的卡号和原单中使用的卡号必须相同。
              end
            end
          end
        end
      end
      else
        set @Status = -6      
  end

  -- 作废 ()
  if @OprID = 5   
  begin
      if (@CardStatus in (2, 3)) --and (@CardExpiryDate >= GETDATE())  
      begin                                          
          set @OpenBal = @TotalAmount
          set @CloseBal = @TotalAmount
          set @OpenPoints = @TotalPoints
          set @ClosePoints = @TotalPoints           
          set @Amount = 0
          set @VoidKeyID = ''                  
	      set @ReturnAmount = @TotalAmount   
          set @ReturnPoint = @TotalPoints           
          set @Status = 0       
      end else
        set @Status = -6
  end
  
  -- Return
  if @OprID = 8
  begin        
      if (@CardStatus in (2, 3)) 
      begin        
        set @NewAmt = @TotalAmount + @Amount
        set @OpenBal = @TotalAmount
        set @CloseBal = @NewAmt
        set @OpenPoints = @TotalPoints
        set @ClosePoints = @TotalPoints           
        set @Amount = @Amount
        set @VoidKeyID = ''                  
	      set @ReturnAmount = @NewAmt   
        set @ReturnPoint = @TotalPoints           
        set @Status = 0         
        -- 判断是否超过卡金额上限
        if isnull(@CardGradeID, '') <> ''        
        begin          
          select @MaxAmtReturn = isnull(CardGradeMaxAmount,0) from CardGrade where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
          if @MaxAmtReturn > 0    -- 如果没有设置，或者是0，则表示没有上限
          begin
            if (@NewAmt + @TxnAmt)> @MaxAmtReturn 
              set @Status = -14
          end
        end
        if @NewAmt < 0      
          set @Status = -10             -- 余额不足              
      end else
        set @Status = -6  
  end
      
  -- 积分消费 
  if @OprID = 13    
  begin
    if @IsAllowConsumptionPoint = 1
    begin
      if (@CardStatus in (2, 3))
      begin 
        set @NewPoints = @TotalPoints - abs(@Point)
        if @NewPoints < @MinBalancePoint and @NewPoints >= 0
        begin 
          set @ReturnPoint = @TotalPoints 
          set @Status = -58            -- 卡积分将小于最低剩余积分       
        end else if abs(@Point) < @CardConsumeBasePoint 
        begin
          set @ReturnPoint = @TotalPoints 
          set @Status = -59             --单笔消费积分不能低于最低消费积分
        end else if @NewPoints < 0 
        begin
          set @ReturnPoint = @TotalPoints 
          set @Status = -10             -- 余额不足
        end else
        begin
          set @Point = -abs(@Point)    -- ver 1.0.0.15
          set @OpenBal = @TotalAmount
          set @CloseBal = @TotalAmount
          set @OpenPoints = @TotalPoints
          set @ClosePoints = @NewPoints           
          set @Amount = 0
          set @VoidKeyID = ''                  
	        set @ReturnAmount = @TotalAmount   
          set @ReturnPoint = @NewPoints           
          set @Status = 0           
        end          
      end else
        set @Status = -6
    end else
     set @Status = -57      -- 此卡不允许积分消费
  end  
    
  -- 直接购买积分
  if @OprID = 14
  begin
    if (@CardStatus in (2, 3))
    begin
      set @NewPoints = @TotalPoints + @Point
      set @OpenBal = @TotalAmount
      set @CloseBal = @TotalAmount
      set @OpenPoints = @TotalPoints
      set @ClosePoints = @NewPoints           
      set @Amount = 0
      set @VoidKeyID = ''                  
	  set @ReturnAmount = @TotalAmount
      set @ReturnPoint = @NewPoints           
      set @Status = 0             
      if (@Point < @MinPointPreAdd and @MinPointPreAdd > 0) or (@Point > @MaxPointPreAdd and @MaxPointPreAdd > 0)
        set @Status = -62
      if @NewPoints > @CardGradeMaxPoint and (@CardGradeMaxPoint > 0)
        set @Status = -63      
    end else
      set @Status = -6    
  end
    
  -- 使用卡金额购买积分 
  if @OprID = 15
  begin
    if (@CardStatus in (2, 3))
    begin
      declare @CardAmountToPointRate float
      select @CardAmountToPointRate = isnull(CardAmountToPointRate, 0) from CardGrade where CardGradeID = @CardGradeID
      set @NewAmt = Convert(money, @Point * @CardAmountToPointRate)
      set @NewPoints = @TotalPoints + @Point
      set @OpenBal = @TotalAmount
      set @CloseBal = @TotalAmount - @NewAmt
      set @OpenPoints = @TotalPoints
      set @ClosePoints = @NewPoints           
      set @Amount =  - @NewAmt
      set @VoidKeyID = ''                  
	  set @ReturnAmount = @TotalAmount - @NewAmt
      set @ReturnPoint = @NewPoints           
      set @Status = 0       
      if @TotalAmount - @NewAmt < 0
      begin      
        set @Status = -10             -- 余额不足    
      end else
      begin
        if (@Point < @MinPointPreAdd and (@MinPointPreAdd > 0)) or (@Point > @MaxPointPreAdd and (@MaxPointPreAdd > 0))
          set @Status = -62
        if @NewPoints > @CardGradeMaxPoint and (@CardGradeMaxPoint > 0)
          set @Status = -63       
      end
    end else
      set @Status = -6  
  end
  insert into ReceiveTxn
      (BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber, OprID, Amount, Points, TenderID,
      [Status], VoidKeyID, VoidTxnNo, Additional, ApproveStatus, Remark, SecurityCode, CreatedBy, UpdatedBy,Approvedby, ApprovalCode)
  values
      (@BrandCode, @StoreCode, @ServerCode, @RegisterCode, @TxnNoKeyID, @TxnNo, @BusDate, @TxnDate, @CardNumber, @OprID, @Amount, @Point, @TenderID,
       @Status, @VoidKeyID, @VoidTxnNo, @Additional, case isnull(@NeedCallConfirm,0) when 0 then 'A' else 'P' end, '', @SecurityCode, @UserID, @UserID, @UserID, @ApprovedCode) 

  declare @InputString varchar(max), @MessageServiceTypeID int, @AccountNumber nvarchar(512)
  set @InputString = ''

  DECLARE CUR_MemberCardSubmit CURSOR fast_forward FOR
    select MessageServiceTypeID, AccountNumber from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1  
  OPEN CUR_MemberCardSubmit
  FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber
  WHILE @@FETCH_STATUS=0
  BEGIN
	if isnull(@AccountNumber, '') <> '' 
	begin
	  if @InputString <> ''
	    set @InputString = @InputString + '|' 
 
      set @InputString = @InputString + 'OPRID=' + cast(@OprID as varchar) + ';AMOUNT=' + cast(@Amount as varchar)	+ ';AMOUNTBALANCE=' + cast(@CloseBal as varchar)
     	   + ';POINT=' + cast(@Point as varchar) + ';POINTBALANCE=' + cast(@ClosePoints as varchar) + ';EXPDATE=' + convert(varchar(10), @CardExpiryDate, 120)
     	   + ';TXNNO=' + @TxnNo + ';MEMBERID=' + cast(@MemberID as varchar)	
     	   + ';MSGACCOUNT=' + @AccountNumber + ';MSGACCOUNTTYPE=' + cast(@MessageServiceTypeID as varchar)	
   
	end
    FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber   
  END
  CLOSE CUR_MemberCardSubmit 
  DEALLOCATE CUR_MemberCardSubmit        	
	  
  exec GenMessageJSONStr 'Transaction', @InputString, @ReturnMessageStr  output
  return @Status
end

GO
