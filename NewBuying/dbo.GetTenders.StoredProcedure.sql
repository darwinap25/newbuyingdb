USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetTenders]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetTenders]  
  @TenderCode			varchar(512),   --  卡号  
  @ConditionStr			nvarchar(1000)='',
  @OrderCondition	    nvarchar(1000)='',
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''
AS
/****************************************************************************
**  Name : GetTenders  
**  Version: 1.0.0.2
**  Description : 获得tender表记录
**  Parameter :
**
  declare @PageCount int, @RecordCount int
  exec GetTenders  '', '', '', 1,0, @PageCount output, @RecordCount output, ''
**  Created by: Gavin @2012-05-21
**  Modify by Gavin @2013-09-17 (ver 1.0.0.1)  增加分页和自定义查询条件
**  Modify by Gavin @2015-12-25 (ver 1.0.0.2)  过滤仅使用于POS端的Tender 
**
****************************************************************************/
begin
  declare @CardGradeID int, @Language int, @SQLStr varchar(2000) 
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @TenderCode = isnull(@TenderCode, '')
  set @SQLStr = 'select TenderID, TenderCode, TenderType, case ' + cast(@Language as varchar) + ' when 2 then TenderName2 when 3 then TenderName3 else TenderName1 end as TenderName, '
     + ' CashSale, Status, Base, Rate, MinAmount, MaxAmount, CardBegin, CardEnd, CardLen, Additional, BankID, TenderPicFile	'
     + ' from tender where (TenderCode = ''' + @TenderCode + ''' or ''' + @TenderCode + ''' = '''')'
	 + ' and (isnull(TerminalFlag,'''') = '''' or substring(TerminalFlag, 2, 1)=''Y'' or substring(TerminalFlag, 3, 1)=''Y'' or substring(TerminalFlag, 4, 1)=''Y''  or substring(TerminalFlag, 5, 1)=''Y'')'

  exec SelectDataInBatchs @SQLStr, 'TenderID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr

  return 0
end

GO
