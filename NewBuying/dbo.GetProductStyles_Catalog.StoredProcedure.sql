USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductStyles_Catalog]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductStyles_Catalog]
  @DepartCode           varchar(64),       -- 部门编码
  @BrandID              int,               -- 品牌ID
  @ProdType             int,               -- 货品类型
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr         varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1     
AS
/****************************************************************************
**  Name : GetProductStyles_Catalog
**  Version: 1.0.0.6
**  Description : 获得ProductStyles数据	 （根据Product_Catalog 来查询，SASA使用此版本,）
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = '01' 
  exec @a = GetProductStyles_Catalog @DepartCode, '', '', null,0,1000,@PageCount output,@RecordCount output,'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount  
  
**  Created by: Gavin @2013-05-07
**  Modify by: Gavin @2013-05-15 （ver 1.0.0.1）只取Product_style表中ProdCodeStyle记录的货品
**  Modify by: Gavin @2013-05-17 （ver 1.0.0.2）修正bug	. 增加分页
**  Modify by: Gavin @2013-06-08  (ver 1.0.0.3) (for SASA) 类似GetProduct,根据传入的@DepartCode，到Product_Catalog表中查询
**  Modify by: Gavin @2013-07-17  (ver 1.0.0.4) 传入的@DepartCode到Product_Catalog表中查询。（支持product和department 多对多）
**  Modify by: Gavin @2013-07-30  (ver 1.0.0.5) 修正bug
**  Modify by: Gavin @2013-08-01  (ver 1.0.0.6) 增加返回NonSale, 目的：用户需要根据Nonsale来过滤。
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
     
  set @BrandID = isnull(@BrandID, 0)        
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))
  set @ProdType = isnull(@ProdType, 0)
  set @QueryCondition = isnull(@QueryCondition, '')
  
  set @SQLStr = 'select P.ProdCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName , '	  
	  + '	 P.ProdPicFile, P.DepartCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '	  
	  + ' D.BrandID, P.ProdType, P.ProdNote, P.ProductBrandID, '	  
      + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName, '
	  + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandDesc2 when 3 then B.ProductBrandDesc3 else B.ProductBrandDesc1 end as ProductBrandDesc, '
	  + '   ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, S.ProdCodeStyle, '
	  + '   A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6, P.NonSale '
  set @SQLStr = @SQLStr + ' from product P '
      + ' left join (select * from Product_Particulars where LanguageID =  ' + cast(@Language as varchar) + ' ) A on P.ProdCode = A.Prodcode '
      + ' left join Product_Brand B on P.ProductBrandID = B.ProductBrandID '
      + ' left join Nation N on P.OriginID = N.NationID '
      + ' left join Color C on P.ColorID = C.ColorID '
      + ' left join (select ProdCodeStyle, max(ProdCode) as PPCode from Product_style group by ProdCodeStyle) S on P.ProdCode = S.PPCode ' 

  if exists(select top 1 * from Product_Catalog) and @DepartCode <> ''         
    set @SQLStr = @SQLStr + ' left join Department D on D.DepartCode = ''' + @DepartCode + ''''
  else  
    set @SQLStr = @SQLStr + ' left join Department D on D.DepartCode = P.DepartCode '     
  
  set @SQLStr = @SQLStr + ' where (isnull(S.PPCode, '''') <> '''') '
  if exists(select top 1 * from Product_Catalog) and @DepartCode <> ''                    
     set @SQLStr = @SQLStr + ' and (P.Prodcode in (select ProdCode from Product_Catalog where DepartCode = ''' + @DepartCode + '''))'   
  else   
     set @SQLStr = @SQLStr + ' and (P.DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + '''= '''')'      

  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @QueryCondition
  
  return 0
end

GO
