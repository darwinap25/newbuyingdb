USE [NewBuying]
GO
/****** Object:  Table [dbo].[T_DailyMediaInfo]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_DailyMediaInfo](
	[Media] [varchar](64) NULL,
	[MediaTypeID] [int] NULL,
	[BusDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[Brand] [int] NULL,
	[Store] [int] NULL,
	[MediaStatus] [varchar](64) NULL,
	[MediaStatusID] [int] NULL,
	[MediaCount] [int] NULL,
	[MediaAmount] [money] NULL,
	[StockStatus] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaInfo', @level2type=N'COLUMN',@level2name=N'Media'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据Media， 存放coupontypeid或者cardtypeid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaInfo', @level2type=N'COLUMN',@level2name=N'MediaTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'brandid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaInfo', @level2type=N'COLUMN',@level2name=N'Brand'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BI 报表使用， 存储内容改为 ID， name 用另外的表保存。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaInfo'
GO
