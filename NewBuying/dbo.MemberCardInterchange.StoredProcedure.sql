USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardInterchange]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[MemberCardInterchange]
  @UserID             varchar(512),  --操作人
  @StoreID            int,		    -- 店铺ID
  @RegisterCode       varchar(512),           -- 终端号码
  @ServerCode         varchar(512)='',		-- serverid 暂时用不到,预留  
  @CardNumber    varchar(64),       -- 操作的cardnumber
  @OprID         int,               -- 61:金额->积分。 62：积分->金额
  @Amount        money,             -- @OprID=61时，用于转换积分的金额，@OprID=62时，期望获得的金额
  @Points        int,               -- @OprID=61时，期望获得的积分，@OprID=62时，用于转换金额的积分
  @SecurityCode         varchar(512),			-- 签名
  @BalanceAmount money output,      -- 操作后，金额余额
  @BalancePoints int output         -- 操作后，积分余额
AS
/****************************************************************************
**  Name : MemberCardInterchange
**  Version: 1.0.0.0
**  Description : 会员卡内部积分和金额的呼唤. (OprID=61时, 优先读取Point, 62时优先读取Amount)
**
**  Parameter :
  declare @BalanceAmount money, @BalancePoints int, @a int
  exec @a = MemberCardInterchange '1','1','99', '88', '000100086', 61, 100, 0,'', @BalanceAmount output, @BalancePoints output
  print @a			
  print @BalanceAmount
  print @BalancePoints 
  select * from card where cardnumber = '000100086'
  update card set cardgradeid = 2 where cardnumber = '000100086'
  select * from cardgrade
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-10-19
**
****************************************************************************/
begin 
  declare @CardGradeID int, @CardPointToAmountRate decimal(16,6), @CardAmountToPointRate decimal(16,6), @TotalAmount money, @TotalPoints int
  declare @TempValue float,  @ActAmount money, @ActPoints int, @TxnNo varchar(512), @CardExpiryDate datetime, @CardStatus int
  declare @BusDate datetime, @TxnDate datetime
  
  set @Amount = abs(isnull(@Amount, 0))
  set @Points = abs(isnull(@Points, 0))
  select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 order by BusDate desc
  set @Txndate = getdate()
  select @CardGradeID = CardGradeID, @TotalAmount = isnull(TotalAmount,0), @TotalPoints = isnull(TotalPoints,0), @CardExpiryDate = CardExpiryDate, @CardStatus = status
    from Card where CardNumber = @CardNumber  
  select @CardPointToAmountRate = CardPointToAmountRate, @CardAmountToPointRate = CardAmountToPointRate 
    from CardGrade where CardGradeID = @CardGradeID
  
  --计算要增加和扣除的金额积分。  
  if @OprID = 61
  begin
    if isnull(@CardAmountToPointRate, 0) <> 0
    begin      
      if @Points <> 0
      begin
        set @ActPoints = @Points
        set @ActAmount = - @Points * @CardAmountToPointRate 
      end else if @Amount <> 0 
      begin
        set @ActAmount = -@Amount
        set @ActPoints = @Amount / @CardAmountToPointRate
      end  
    end else
      return -65  
  end    
  if @OprID = 62  
  begin
    if isnull(@CardPointToAmountRate, 0) <> 0
    begin          
      if @Amount <> 0
      begin
        set @ActAmount = @Amount
        set @ActPoints = - (@Amount * @CardPointToAmountRate)
      end else if @Points <> 0 
      begin
        set @ActPoints = -@Points
        set @ActAmount = @Points / @CardPointToAmountRate    
      end        
    end else
      return -65  
  end
  
  if @TotalAmount + @ActAmount < 0 
    return -37
  if @TotalPoints + @ActPoints < 0 
    return -38          
  exec GetRefNoString 'API', @TxnNo output
        insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
           CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
        values
          (@OprID, @CardNumber, '', 0, @TxnNo, @TotalAmount, @ActAmount, @TotalAmount + @ActAmount, @ActPoints, @BusDate, @TxnDate, 
          null, null, '', @SecurityCode, @UserID, @StoreID, @RegisterCode, @ServerCode,
          @TotalPoints, @TotalPoints + @ActPoints, @CardExpiryDate, @CardExpiryDate, @CardStatus, @CardStatus)
 
  set @BalanceAmount = @TotalAmount + @ActAmount
  set @BalancePoints = @TotalPoints + @ActPoints            
  return 0 
end

GO
