USE [NewBuying]
GO
/****** Object:  Table [dbo].[DialogMessage_Lan]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DialogMessage_Lan](
	[TAPCode] [nvarchar](50) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Lan] [nvarchar](512) NULL,
 CONSTRAINT [PK_DIALOGMESSAGE_LAN] PRIMARY KEY CLUSTERED 
(
	[TAPCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
