USE [NewBuying]
GO
/****** Object:  Table [dbo].[Accounts_PermissionCategories]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts_PermissionCategories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ParentID] [int] NOT NULL,
	[OrderID] [int] NULL
) ON [PRIMARY]

GO
