USE [NewBuying]
GO
/****** Object:  Table [dbo].[RFMSetting]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RFMSetting](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[BrandCode] [varchar](64) NULL,
	[RFMName] [varchar](64) NULL,
	[Recency] [int] NULL,
	[Frequency] [int] NULL,
	[Monetary] [money] NULL,
	[Status] [int] NULL,
	[LocalCurrency] [varchar](64) NULL,
 CONSTRAINT [PK_RFMSETTING] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'BrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'RFMName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最近一次的限制。 单位：天。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'Recency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费频率。  单位：次数。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'Frequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费金额。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'Monetary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。0：无效。 只能有一个生效。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'本币的Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting', @level2type=N'COLUMN',@level2name=N'LocalCurrency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费者消费模型设置表。
Add by Gavin @2016-08-29' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RFMSetting'
GO
