USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenNewTxnNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenNewTxnNo]
  @StoreCode             VARCHAR(64),          -- POS 的店铺编号
  @RegisterCode          VARCHAR(64),          -- POS 的注册编号
  @BusDate               DATE,                 -- 交易的Business date
  @TransNum              VARCHAR(64) OUTPUT    -- 返回的交易号
AS
/****************************************************************************
**  Name : GenNewTxnNo
**  Version : 1.0.0.2
**  Description : 获得支付货币的数据
**
  declare  @a int, @TransNum varchar(64)
  exec @a = GenNewTxnNo '1', 'R01', '2015-03-26', @TransNum output
  print @a
  print @TransNum
  delete from txnno
  select * from TXNNO
  select * from SALES_H
**  Created by Gavin @2015-03-06
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
**  Modify by Gavin @2017-09-21 (1.0.0.2) 日期部分固定长度
****************************************************************************/
BEGIN
  DECLARE @BusDateStr char(8), @LastNo int, @SODEODBusDate DATE
  SET @StoreCode = ISNULL(@StoreCode, '')
  SET @RegisterCode = ISNULL(@RegisterCode, '')  
  SET @TransNum = ''
  SELECT TOP 1 @SODEODBusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate desc
  IF @SODEODBusDate is null
    RETURN -510
        
  IF @BusDate IS NOT NULL
  BEGIN
    IF DATEDIFF(DD, @BusDate,@SODEODBusDate) <> 0
      RETURN -511
  END
    
  SET @BusDateStr = CAST(DATEPART(yy, @SODEODBusDate) AS VARCHAR) + RIGHT('00' + CAST(DATEPART(MM, @SODEODBusDate) AS VARCHAR), 2) +  RIGHT('00' + CAST(DATEPART(DD, @SODEODBusDate) AS VARCHAR), 2)
  
  SELECT @LastNo = ISNULL(LastNo, 1) FROM TXNNO WHERE StoreCode = @StoreCode and RegisterCode = @RegisterCode and Busdate = @SODEODBusDate
  IF ISNULL(@LastNo, 0) = 0 
  BEGIN
    INSERT INTO TXNNO(StoreCode, RegisterCode, BusDate, LastNo)
    VALUES(@StoreCode, @RegisterCode, @SODEODBusDate, 1)
    SET @LastNo = 1
  END ELSE
  BEGIN    
    UPDATE TXNNO SET LastNo = @LastNo + 1 WHERE StoreCode = @StoreCode and RegisterCode = @RegisterCode and Busdate = @SODEODBusDate and LastNo = @LastNo
    IF @@ROWCOUNT = 0
      RETURN -501
    ELSE
      SET @LastNo = @LastNo + 1      
  END
  
  SET @TransNum = RTRIM(@BusDateStr) + RTRIM(@StoreCode) + RTRIM(@RegisterCode) + RIGHT('000000' + CAST(@LastNo as VARCHAR), 6)
  
  RETURN 0
END

GO
