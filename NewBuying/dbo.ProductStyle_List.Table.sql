USE [NewBuying]
GO
/****** Object:  Table [dbo].[ProductStyle_List]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductStyle_List](
	[ProdCodeStyle] [varchar](64) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[ProdName1] [nvarchar](512) NULL,
	[ProdName2] [nvarchar](512) NULL,
	[ProdName3] [nvarchar](512) NULL,
	[ProdPicFile] [nvarchar](512) NULL,
	[ProdType] [int] NULL DEFAULT ((0)),
	[ProdNote] [nvarchar](512) NULL,
	[NonSale] [int] NULL DEFAULT ((0)),
	[NewFlag] [int] NULL DEFAULT ((0)),
	[Flag1] [int] NULL DEFAULT ((0)),
	[Flag2] [int] NULL DEFAULT ((0)),
	[Flag3] [int] NULL DEFAULT ((0)),
	[HotSaleFlag] [int] NULL DEFAULT ((0)),
	[DepartCode] [varchar](64) NULL,
	[DepartName1] [nvarchar](512) NULL,
	[DepartName2] [nvarchar](512) NULL,
	[DepartName3] [nvarchar](512) NULL,
	[ProductBrandID] [int] NULL,
	[ProductBrandCode] [varchar](64) NULL,
	[ProductBrandName1] [nvarchar](512) NULL,
	[ProductBrandName2] [nvarchar](512) NULL,
	[ProductBrandName3] [nvarchar](512) NULL,
	[ProductBrandDesc1] [nvarchar](max) NULL,
	[ProductBrandDesc2] [nvarchar](max) NULL,
	[ProductBrandDesc3] [nvarchar](max) NULL,
	[ProductBrandPicSFile] [nvarchar](512) NULL,
	[ProductBrandPicMFile] [nvarchar](512) NULL,
	[ProductBrandPicGFile] [nvarchar](512) NULL,
	[ColorCode1] [varchar](64) NULL,
	[ColorCode2] [varchar](64) NULL,
	[ColorCode3] [varchar](64) NULL,
	[ColorCode4] [varchar](64) NULL,
	[ColorCode5] [varchar](64) NULL,
	[MinPrice] [money] NULL,
	[MaxPrice] [money] NULL,
	[MaxDefaultPrice] [money] NULL,
	[MinDefaultPrice] [money] NULL,
	[SeasonID] [int] NULL,
	[GenderID] [int] NULL,
	[SeasonCode] [varchar](64) NULL,
	[GenderCode] [varchar](64) NULL,
	[ColorCodeList] [varchar](max) NULL,
	[ProductSizeIDList] [varchar](max) NULL,
	[PromotionCodeList] [varchar](max) NULL,
	[ProdCodeList] [varchar](max) NULL,
	[ProdName1List] [varchar](max) NULL,
	[ProdName2List] [varchar](max) NULL,
	[ProdName3List] [varchar](max) NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[InStockFlag] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_PRODUCTSTYLE_LIST] PRIMARY KEY CLUSTERED 
(
	[ProdCodeStyle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品系列编码，联合主键。（以prodcode作为主键，例如，同一系列不同颜色的口红，以第一个颜色的货品作为 productline 的主键，其他货品作为子货品。 主键货品也需要在子货品列表中）
货品A系列： A1，A2，A3
StyleCode         ProdCode
A1                     A1
A1                     A2
A1                     A3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdCodeStyle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这里的货品，只是首货品。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品类型。
0： 销售商品
1： 费用类型 （非商品）（价格不定，比如运费，维修费，服务费）
2： msvc Card  （销售卡或者充值）
3： msvc Coupon （销售Coupon）
4： msvc Card  （购买积分）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProdNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非销售货品。 0： 不是不能销售货品。 1：是不能销售货品。 2： 这个style中两种prodcode都有  默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'NonSale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新品标志。 0：非新品。1：新品。 2： 这个style中两种prodcode都有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'NewFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'Flag1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'Flag2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'Flag3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'热销标志。0：非热销。1：热销。 2： 这个style中两种prodcode都有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'HotSaleFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'DepartName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'DepartName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'DepartName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品小图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandPicSFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品中图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandPicMFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大图文件路径名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductBrandPicGFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限制：一个style，必须同一个season' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'SeasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限制：一个style，必须同一个Gender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'GenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限制：一个style，必须同一个season' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'SeasonCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限制：一个style，必须同一个Gender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'GenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色code的 列表， 之间用 "," 隔开。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ColorCodeList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ProductSizeID的 列表， 之间用 "," 隔开。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'ProductSizeIDList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ProductSizeID的 列表， 之间用 "," 隔开。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'PromotionCodeList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否有库存标志： 0：没有。 1：有库存' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List', @level2type=N'COLUMN',@level2name=N'InStockFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'productstyle列表 for Bauhaus。 目的：预先准备productstyle数据（增加所属颜色，价格，季节等字段， 只是为了查询）
@2016-12-01 
@2017-02-24 增加Createdon 和 UpdatedOn
@2017-03-07 增加InStockFlag。
@2017-04-06 增加Flag2, Flag3。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductStyle_List'
GO
