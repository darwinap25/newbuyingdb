USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenDeliveryOrder_Card]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenDeliveryOrder_Card]
  @UserID                   int,             --操作员ID
  @CardPickingNumber      varchar(64),     --单号
  @PickupApprovalcode		varchar(6)       -- pickup的 approvalcode
AS
/****************************************************************************
**  Name : GenDeliveryOrder_Card
**  Version: 1.0.0.0
**  Description : 根据GenDeliveryOrder_Coupon(1.0.1.0)改写
**  example :

**  Created by: Gavin @2014-10-09
**
****************************************************************************/
begin
  declare @CardOrderFormNumber varchar(64), @CardDeliveryNumber varchar(64), @OrderQty int, @PickupQty int, 
          @CardNumber varchar(64), @BrandID int
  declare @i int, @EndCardNumber varchar(64), @PrevCardNumber varchar(64), @A int
  declare @FirstCard varchar(64), @EndCard varchar(64), @OrderStatus char(1)
  declare @BusDate datetime, @TxnDate datetime, @StoreID int, @CardTypeID int, @CardGradeID int
   
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()
  
  -- 获取pickup单的单号
  select @CardOrderFormNumber = ReferenceNo, @OrderStatus = ApproveStatus from Ord_CardPicking_H where CardPickingNumber = @CardPickingNumber
  if @OrderStatus <> 'A' 
    return -1
  exec GetRefNoString 'CODOCA', @CardDeliveryNumber output   
  select @StoreID = StoreID from Ord_CardOrderForm_H where CardOrderFormNumber = @CardOrderFormNumber
     
  insert into Ord_CardDelivery_H(CardDeliveryNumber, ReferenceNo, BrandID,  FromStoreID, StoreID, CustomerType, 
          CustomerID,SendMethod, SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, 
          StoreMobile, FromContactName, FromContactNumber, FromEmail, FromMobile,            
          NeedActive, Remark, CreatedBusdate, ApproveStatus, CreatedBy, UpdatedBy, Remark1)
  select @CardDeliveryNumber, @CardPickingNumber, BrandID,  FromStoreID, StoreID, CustomerType, 
          CustomerID,SendMethod, SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, 
          StoreMobile, FromContactName, FromContactNumber, FromEmail, FromMobile, 
          case when CustomerType = 1 then 1 else 0 end, Remark, CreatedBusdate, 'P', @UserID, @UserID, Remark1
     from Ord_CardOrderForm_H where CardOrderFormNumber = @CardOrderFormNumber
  
  insert into Ord_CardDelivery_D (CardDeliveryNumber, CardTypeID, CardGradeID, Description, OrderQty, PickQty, ActualQty, FirstCardNumber, EndCardNumber, BatchCardCode)   
  select @CardDeliveryNumber, CardTypeID, CardGradeID, Description, OrderQty, PickQty, ActualQty, FirstCardNumber, EndCardNumber, BatchCardCode
    from Ord_CardPicking_D where CardPickingNumber = @CardPickingNumber   
 
  return 0
end

GO
