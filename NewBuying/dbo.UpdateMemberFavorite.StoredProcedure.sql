USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberFavorite]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[UpdateMemberFavorite]
  @MemberID             bigint,               -- 会员ID
  @ActionKey			char(1),           -- 操作： 'A': 增加。'U': 更新（需要KeyID）。'D':删除（需要KeyID）。 'C': 清空购物车
  @KeyID                int, 
  @FavoriteType         int,               -- 类型
  @ProdCodeStyle        varchar(64),
  @ProdStyleDesc        varchar(512),
  @ProdStylePIC         varchar(512),
  @Memo                 varchar(Max),
  @ProdStyleTitle       varchar(512),
  @RetentionDays        int=7,			   -- 保留天数，默认7天
  @ProdCode             varchar(64)        -- 增加输入参数Prodcode 
AS
/****************************************************************************
**  Name : UpdateMemberFavorite  
**  Version: 1.0.0.7
**  Description : 更新会员的收藏信息  (根据KeyID定位，由调用方判断同一个member，product唯一)
**  Parameter :
  declare @a int  
  exec @a = UpdateMemberFavorite 1, 'D', 1, '243243', '随碟附送该死的', 'sadfasf', '隔阂和edewew额无法和违法'
  print @a  
  declare @p1 int
set @p1=4
exec sp_prepexec @p1 output,N'@P1 int,@P2 varchar(80),@P3 int,@P4 int,@P5 varchar(1),@P6 varchar(1),@P7 varchar(1),@P8 varchar(80),@P9 varchar(1),@P10 int,@P11 varchar(1)',N'exec UpdateMemberFavorite @P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11',705,'D',1127,0,'','','','{"IsFavorite":"1127"}','',999,''
select @p1

**  Created by: Gavin @2013-12-20
**  Modified by: Gavin @2013-12-26  (ver 1.0.0.1) @Action只使用A，D，C。 KeyID不再传入数据，根据传入的@ProdCode 来判断update或者insert。 存储过程来判断一个memberID，product必须唯一
**  Modified by: Gavin @2014-01-07  (ver 1.0.0.2) 更新UpdatedOn, 删除过期记录
**  Modified by: Gavin @2015-08-19  (ver 1.0.0.3) MemberFavorite增加prodcode, 增加输入参数Prodcode, 更新以Prodcode为条件
**  Modified by: Gavin @2015-12-21  (ver 1.0.0.4) Action 是FS 的 关键字， 所以改为 @ActionKey
**  Modified by: Gavin @2016-06-27  (ver 1.0.0.5) UpdatedOn 记录恢复 全日期时间。 
**  Modified by: Gavin @2016-09-21  (ver 1.0.0.6) 增加判断KeyID，如果传入了KeyID，则删除和更新时，使用KeyID，否则按照原来逻辑。 （Nick前台传入KeyID）
**  Modified by: Gavin @2016-10-17 (ver 1.0.0.7) @MemberID 类型由int改为 bigint
**
****************************************************************************/
begin
  if isnull(@MemberID, 0) = 0
    return -7

  set @ProdCodeStyle = isnull(@ProdCodeStyle, '')
  set @FavoriteType = isnull(@FavoriteType, 0)
  set @ProdStyleDesc = isnull(@ProdStyleDesc, '')
  set @ProdStylePIC = isnull(@ProdStylePIC, '')
  set @ProdCode = isnull(@ProdCode, '')
  set @Memo = isnull(@Memo, '')
  set @KeyID = isnull(@KeyID, 0)
  if @Memo = '' 
    set @Memo = ' '	  -- 否则出现[Microsoft][SQLServer 2000 Driver for JDBC]Underlying input stream returned zero bytes 的异常
    
  if @ActionKey = 'A'
  begin
    if exists(select keyid from MemberFavorite where (MemberID = @MemberID and ProdCode = @ProdCode and FavoriteType = @FavoriteType) or (KeyID = @KeyID))
    begin
	  if @KeyID > 0
		  update MemberFavorite set MemberID = @MemberID, FavoriteType = @FavoriteType, ProdCodeStyle = @ProdCodeStyle, 
			ProdStyleDesc = @ProdStyleDesc, ProdStylePIC = @ProdStylePIC, Memo = @Memo, ProdStyleTitle = @ProdStyleTitle,
			--UpdatedOn =  convert(varchar(10), Getdate(), 120), ProdCode = @ProdCode
			UpdatedOn =  Getdate(), ProdCode = @ProdCode
		  where KeyID = @KeyID
	  else	    
		  update MemberFavorite set MemberID = @MemberID, FavoriteType = @FavoriteType, ProdCodeStyle = @ProdCodeStyle, 
			ProdStyleDesc = @ProdStyleDesc, ProdStylePIC = @ProdStylePIC, Memo = @Memo, ProdStyleTitle = @ProdStyleTitle,
			--UpdatedOn =  convert(varchar(10), Getdate(), 120), ProdCode = @ProdCode
			UpdatedOn =  Getdate(), ProdCode = @ProdCode
		  where MemberID = @MemberID and ProdCode = @ProdCode and FavoriteType = @FavoriteType
    end else
    begin  
      insert into MemberFavorite 
        (MemberID, FavoriteType, ProdCodeStyle, ProdStyleDesc, ProdStylePIC, Memo, ProdStyleTitle, UpdatedOn, ProdCode)
      values (@MemberID, @FavoriteType, @ProdCodeStyle, @ProdStyleDesc, @ProdStylePIC, @Memo, @ProdStyleTitle, Getdate(), @ProdCode)
    end  
  end else if @ActionKey = 'D'
  begin
	  --Delete from MemberFavorite where MemberID = @MemberID and ProdCodeStyle = @ProdCodeStyle and (FavoriteType = @FavoriteType or @FavoriteType = 0)
    if @KeyID > 0
	  Delete from MemberFavorite where KeyID = @KeyID
    else
	  Delete from MemberFavorite where MemberID = @MemberID and ProdCode = @ProdCode and (FavoriteType = @FavoriteType or @FavoriteType = 0)
  end else if @ActionKey = 'C'
  begin
	  Delete from MemberFavorite where MemberID = @MemberID  and (FavoriteType = @FavoriteType or @FavoriteType = 0)
  end else 
    return -97    	  
 
    -- 删除超过@RetentionDays 的记录
  Delete from MemberFavorite where MemberID = @MemberID and (UpdatedOn + @RetentionDays + 1) < getdate()    
  return 0
end

GO
