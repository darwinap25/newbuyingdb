USE [NewBuying]
GO
/****** Object:  Table [dbo].[StaffToken]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StaffToken](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[UserName] [varchar](50) NULL,
	[TokenStr] [varchar](20) NULL,
	[VerifyType] [int] NULL,
	[VerifyTypeDesc] [varchar](100) NULL,
	[Status] [int] NULL DEFAULT ((1)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[ExpiryOn] [datetime] NULL,
	[MemberID] [int] NULL,
	[StoreCode] [varchar](64) NULL,
 CONSTRAINT [PK_STAFFTOKEN] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts_Users 的 userid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'UserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts_Users 的 user name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'token值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'TokenStr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'验证的类型：
1為註冊碼被用來跳過電郵＋電話步，而2為只跳過電話步呢' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'VerifyType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VerifyType的描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'VerifyTypeDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。1： 有效状态。 -1：无效。 2：已经使用。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'ExpiryOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'验证的memberid。 （记录为哪个member做的验证）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bauhaus 要求的，每个staff可以有 10 个 token，使用token后，允许顾客注册时跳过 手机的验证。
@2016-11-03 create。
@2017-01-10 增加字段 StoreCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StaffToken'
GO
