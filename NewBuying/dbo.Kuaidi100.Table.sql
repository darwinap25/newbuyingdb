USE [NewBuying]
GO
/****** Object:  Table [dbo].[Kuaidi100]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Kuaidi100](
	[message] [varchar](100) NULL,
	[nu] [varchar](64) NULL,
	[ischeck] [int] NULL,
	[com] [varchar](64) NULL,
	[updatetime] [datetime] NULL,
	[status] [int] NULL,
	[state] [int] NULL,
	[condition] [varchar](64) NULL,
	[time] [datetime] NULL,
	[location] [varchar](64) NULL,
	[context] [nvarchar](512) NULL,
	[ftime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'查询结果状态： 
0：物流单暂无结果， 
1：查询成功， 
2：接口出现异常，' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Kuaidi100', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快递单当前的状态 ：　 
0：在途，即货物处于运输过程中；
1：揽件，货物已由快递公司揽收并且产生了第一条跟踪信息；
2：疑难，货物寄送过程出了问题；
3：签收，收件人已签收；
4：退签，即货物由于用户拒签、超区等原因退回，而且发件人已经签收；
5：派件，即快递正在进行同城派件；
6：退回，货物正处于退回发件人的途中；
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Kuaidi100', @level2type=N'COLUMN',@level2name=N'state'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'查询Kuaidi100 获得的原始资料。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Kuaidi100'
GO
