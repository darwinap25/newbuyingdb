USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[InitProductStyle_List]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InitProductStyle_List]
as
/****************************************************************************  
**  Name : InitProductStyle_List  
**  Version: 1.0.0.8
**  Description : ³õÊ¼»¯InitProductStyle_List±í
**  Example :  
  exec InitProductStyle_List
  select * from ProductStyle_List

**  Created by: Gavin @2016-12-01  
**  Modify by Gavin @2016-12-05 (ver 1.0.0.1) ProductStyle_List±í add ProdCodeList µÈ×Ö¶Î
**  Modify by Gavin @2016-12-06 (ver 1.0.0.2) ¹ØÁªproduct±íÊ±£¬¼ÓÉÏIsOnlineskuÌõ¼þ¡£ promotionlist ÐèÒª»Øµ½ÉÏÒ»²ãÑ­»·ÔÙ´ÎÀÛ¼Æ 
**  Modify by Gavin @2016-12-12 (ver 1.0.0.3) ÐÞ¸Äflag1²¿·Ö¡£µ±Ò»¸östyleÖÐ´æÔÚflag1=trueµÄ»õÆ·Ê±£¬Õû¸östyle flag1±íÏÖÎªtrue
**  Modify by Gavin @2017-02-12 (ver 1.0.0.4) Ôö¼ÓCreatedon£¬ updatedon 
**  Modify by Gavin @2017-03-07 (ver 1.0.0.5) Ôö¼ÓInStockFlag¡£´ËÊý¾ÝÐèÒª´ÓBuying DB¶ÁÈ¡£¨Í¨¹ýLinkServer£© 
**  Modify by Gavin @2017-04-06 (ver 1.0.0.6) Ôö¼Óflag2 (ÊÇ·ñÊÇ ÊÖ´ü¼°ï—Îï). Í¬flag1£¬µ±Ò»¸östyleÖÐ´æÔÚflag2=trueµÄ»õÆ·Ê±£¬Õû¸östyle flag2±íÏÖÎªtrue
                                              Ôö¼Óflag3 (ÊÇ·ñÊÇ ÏÞÁ¿ÉÌÆ·). Í¬flag1£¬µ±Ò»¸östyleÖÐ´æÔÚflag3=trueµÄ»õÆ·Ê±£¬Õû¸östyle flag3±íÏÖÎªtrue
**  Modify by Gavin @2017-06-14 (ver 1.0.0.7) ¹ýÂË¼Û¸ñÎª0µÄ»õÆ·. @2017-06-16 ÓÅ»¯ËÙ¶È
**  Modify by Gavin @2017-08-16 (ver 1.0.0.8) DBºÏ²¢°æ±¾ 
**  
****************************************************************************/  
begin
  declare @ProdCodeStyle varchar(64)
  declare @ProdCode varchar(64), @ProdName1 nvarchar(512), @ProdName2 nvarchar(512), @ProdName3 nvarchar(512), @DepartCode varchar(64), @PromotionCode varchar(64)
  declare @ProdCodeList varchar(max), @ProdName1List varchar(max), @ProdName2List varchar(max), @ProdName3List varchar(max), @PromotionCodeList varchar(max), @PMList varchar(max)
  declare @a table(ProdCodeStyle varchar(64), ProdCode varchar(64), flag1 int, flag2 int, flag3 int, CreatedOn Datetime, UpdatedOn datetime)  
  
  declare @SQLStr varchar(max)
  declare @stockdata table(ProdCodeStyle varchar(64), InStockFlag int)

  declare @tempprice money

  truncate table ProductStyle_List

    insert into @a (ProdCodeStyle, ProdCode, flag1, flag2, CreatedOn, UpdatedOn)
	select B.ProdCodeStyle, min(A.ProdCode) as ProdCode, max(isnull(A.flag1,0)) as flag1, max(isnull(A.flag2,0)) as flag2, max(CreatedOn) as CreatedOn, max(updatedon) as updatedon
	  from (select * from product where IsOnlineSKU = 1) A 
	  left join Product_style B on A.ProdCode = B.ProdCode
	where isnull(B.ProdCodeStyle,'') <> '' 
	group by B.ProdCodeStyle 
  
   -- ver 1.0.0.5
   insert into @stockdata (ProdCodeStyle, InStockFlag)
   select ProdCodeStyle, case when sum(isnull(B.StockQty,0)) > 0 then 1 else 0 end as StockQty 
     from BUY_PRODUCTSTYLE A
     left join (select ProdCode, isnull(sum(OnhandQty),0) as StockQty from Stk_Stockonhand 
	             where StockTypeCode = 'G' 
				 Group By ProdCode) B on A.ProdCode = B.ProdCode 
   group by A.ProdCodeStyle

  insert into ProductStyle_List(ProdCodeStyle, ProdCode, ProdName1,ProdName2,ProdName3,ProdPicFile,ProdType,ProdNote,NonSale,NewFlag,Flag1,Flag2,HotSaleFlag,DepartCode,
    DepartName1,DepartName2,DepartName3,ProductBrandID,ProductBrandCode,ProductBrandName1,ProductBrandName2,ProductBrandName3,ProductBrandDesc1,ProductBrandDesc2,
	ProductBrandDesc3,ProductBrandPicSFile,ProductBrandPicMFile,ProductBrandPicGFile,--ColorCode1,ColorCode2,ColorCode3,ColorCode4,ColorCode5,MinPrice,MaxPrice,MinDefaultPrice,MaxDefaultPrice,ColorCodeList,ProductSizeIDList,
	SeasonID,GenderID,SeasonCode,GenderCode, CreatedOn, UpdatedOn, InStockFlag) 
  select A.ProdCodeStyle, A.ProdCode, P.ProdName1,P.ProdName2,P.ProdName3,P.ProdPicFile,P.ProdType,P.ProdNote,P.NonSale,P.NewFlag,A.Flag1,A.Flag2,P.HotSaleFlag,P.DepartCode,
    D.DepartName1,D.DepartName2,D.DepartName3,C.ProductBrandID,C.ProductBrandCode,C.ProductBrandName1,C.ProductBrandName2,C.ProductBrandName3,C.ProductBrandDesc1,C.ProductBrandDesc2,
	C.ProductBrandDesc3,C.ProductBrandPicSFile,C.ProductBrandPicMFile,C.ProductBrandPicGFile,--ColorCode1,ColorCode2,ColorCode3,ColorCode4,ColorCode5,MinPrice,MaxPrice,MinDefaultPrice,MaxDefaultPrice,ColorCodeList,ProductSizeIDList,
	E.SeasonID,G.GenderID,E.SeasonCode,G.GenderCode, A.CreatedOn, A.UpdatedOn, isnull(S.InStockFlag, 0)
    from @a A
    left join Product P on A.ProdCode = P.ProdCode
    left join Product_Brand C on P.ProductBrandID = C.ProductBrandID
    left join Department D on D.DepartCode = P.DepartCode
    left join (select * from Product_Classify where ForeignTable = 'SEASON') SE on SE.ProdCode = P.ProdCode
    left join SEASON E on SE.ForeignKeyID = E.SeasonID 
	left join (select * from Product_Classify where ForeignTable = 'Gender') SG on SG.ProdCode = P.ProdCode
    left join Gender G on SG.ForeignKeyID = G.GenderID 
    left join @stockdata S on A.ProdCodeStyle = S.ProdCodeStyle

    update ProductStyle_List 
	  set ColorCode1=Color1, ColorCode2=Color2, ColorCode3=Color3, ColorCode4=Color4, ColorCode5=Color5,
	     MinPrice = B.MinPrice, MaxPrice = B.MaxPrice, MinDefaultPrice=B.MinDefaultPrice, MaxDefaultPrice=B.MaxDefaultPrice
    from ProductStyle_List A 
	 left join (
		 select A.ProdCodeStyle, max(CO.Color1) Color1, max(CO.Color2) Color2, max(CO.Color3) Color3, max(CO.Color4) Color4, max(CO.Color5) Color5, 
			max(S.MinPrice) MinPrice, max(S.MaxPrice) MaxPrice, max(S.MinDefaultPrice) MinDefaultPrice, max(S.MaxDefaultPrice) MaxDefaultPrice
		   from ProductStyle_List A
		   left join (
					   select ProdCodeStyle, max(Color1) as Color1, max(Color2) as Color2, max(Color3) as Color3, max(Color4) as Color4, max(Color5) as Color5  
					   from (
						  select ProdCodeStyle, case when (A.IID%5)= 1 then A.RGB else '' end as Color1,case when (A.IID%5)= 2 then A.RGB else '' end as Color2, 
						   case when (A.IID%5)= 3 then A.RGB else '' end as Color3, case when (A.IID%5)= 4 then A.RGB else '' end as Color4, case when (A.IID%5)= 0 then A.RGB else '' end as Color5  
						   from( 
							  select  A.ProdCodeStyle, ROW_NUMBER() over (order by A.ProdCodeStyle, C.ColorCode) as IID, C.ColorCode, C.RGB  from Product_Style A							
								left join (select ColorID, ProdCodeStyle, max(prodcode) as ProdCode 
											 from (select P.*, S.ProdCodeStyle from Product P, Product_Style S 
													 where P.ProdCode = S.ProdCode AND IsOnlineSKU=1 AND P.ProdCode in (select ProdCode from Product_Style where ProdCodeStyle in (select ProdCodeStyle from ProductStyle_List))) A group by A.ProdCodeStyle, A.ColorID) B on A.ProdCode = B.ProdCode  
								left join Color C on B.ColorID = C.ColorID 
							   where isnull(B.ColorID, 0) > 0  
							  ) A
						   ) A 
						group by ProdCodeStyle
					 ) CO on CO.ProdCodeStyle = A.ProdCodeStyle 
		   left join (
					  select ProdCodeStyle, MAX(X.ProdCode) as PPCode, max(Y.NetPrice) as MaxPrice, min(Y.NetPrice) as MinPrice, 
							max(Y.DefaultPrice) as MaxDefaultPrice, min(Y.DefaultPrice) as MinDefaultPrice 
						 from Product_style X 
						   left join (SELECT ProdCode, NetPrice,DefaultPrice 
										 FROM Product_Price where KeyID in  (SELECT  MAX(KeyID) AS KeyID FROM Product_Price 
																			   WHERE (StartDate <= GETDATE()) AND (EndDate+1 >= GETDATE())
																				 AND ProdCode in (select ProdCode from Product where IsOnlineSKU = 1) 
																			   group by ProdCode) 
									  ) Y on X.ProdCode = Y.ProdCode group by ProdCodeStyle
					 ) S on A.ProdCodeStyle = S.ProdCodeStyle
		  group by A.ProdCodeStyle
    ) B on A.ProdCodeStyle = B.ProdCodeStyle


    DECLARE CUR_ProductStyle_List CURSOR fast_forward FOR
      SELECT ProdCodeStyle FROM  ProductStyle_List
    OPEN CUR_ProductStyle_List
    FETCH FROM CUR_ProductStyle_List INTO @ProdCodeStyle
    WHILE @@FETCH_STATUS=0
    BEGIN
	  set @ProdCodeList = ''
	  set @ProdName1List = '' 
	  set @ProdName2List = '' 
	  set @ProdName3List = ''
	  set @PMList = ''
	  DECLARE CUR_ProductStyle_List_ProdCode CURSOR fast_forward FOR        
	     -- ver 1.0.0.7
        select B.ProdCode, B.ProdName1, B.ProdName2, B.ProdName3, B.DepartCode from (select * from Product_Style where ProdCodeStyle = @ProdCodeStyle) A
	     left join Product B on A.ProdCode = B.ProdCode
		 --left join (SELECT ProdCode, ProdPriceType, NetPrice, DefaultPrice FROM Product_Price where KeyID in (SELECT MAX(KeyID) KeyID FROM Product_Price WHERE (StartDate<=GETDATE()) AND (EndDate+1>=GETDATE()) group by ProdCode)) R on A.ProdCode=R.ProdCode
		 where B.IsOnlineSKU = 1 --and R.NetPrice > 0
      OPEN CUR_ProductStyle_List_ProdCode
      FETCH FROM CUR_ProductStyle_List_ProdCode INTO @ProdCode, @ProdName1, @ProdName2, @ProdName3,@DepartCode
      WHILE @@FETCH_STATUS=0
      BEGIN
	    SELECT @tempprice=NetPrice FROM Product_Price where KeyID = (SELECT MAX(KeyID) KeyID FROM Product_Price WHERE (StartDate<=GETDATE()) AND (EndDate+1>=GETDATE()) AND ProdCode = @ProdCode)

		IF @tempprice > 0 
		BEGIN
			SET @PromotionCodeList = ''
			DECLARE CUR_ProductStyle_List_ProdCode_Dept CURSOR fast_forward FOR
			  select PromotionCode from BUY_Promotion_Hit_PLU where (EntityType = 1 and EntityNum = @ProdCode) or (EntityType = 2 and EntityNum = @DepartCode)  
			OPEN CUR_ProductStyle_List_ProdCode_Dept
			FETCH FROM CUR_ProductStyle_List_ProdCode_Dept INTO @PromotionCode
			WHILE @@FETCH_STATUS=0
			BEGIN               
			   set @PromotionCodeList = @PromotionCodeList + @PromotionCode + ','
			   FETCH FROM CUR_ProductStyle_List_ProdCode_Dept INTO @PromotionCode
			END
			CLOSE CUR_ProductStyle_List_ProdCode_Dept 
			DEALLOCATE CUR_ProductStyle_List_ProdCode_Dept 

			set @ProdCodeList = @ProdCodeList + @ProdCode + ',' 
			set @ProdName1List = @ProdName1List + @ProdName1 + ','
			set @ProdName2List = @ProdName2List + @ProdName2 + ','
			set @ProdName3List = @ProdName3List + @ProdName3 + ','
			set @PMList = @PMList + @PromotionCodeList + ','
		END
	    FETCH FROM CUR_ProductStyle_List_ProdCode INTO @ProdCode, @ProdName1, @ProdName2, @ProdName3,@DepartCode
      END
      CLOSE CUR_ProductStyle_List_ProdCode 
      DEALLOCATE CUR_ProductStyle_List_ProdCode 

	  update ProductStyle_List set ProdCodeList = @ProdCodeList, ProdName1List = @ProdName1List, ProdName2List = @ProdName2List,
	     ProdName3List = @ProdName3List, PromotionCodeList = @PMList

	  where ProdCodeStyle = @ProdCodeStyle
      FETCH FROM CUR_ProductStyle_List INTO @ProdCodeStyle
    END
    CLOSE CUR_ProductStyle_List 
    DEALLOCATE CUR_ProductStyle_List 
end  

GO
