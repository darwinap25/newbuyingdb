USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSCouponRedeem]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[POSCouponRedeem]  
  @UserID               varchar(512),           -- POS用户  
  @CardNumber           varchar(512),           -- 会员卡号 (可不填. 填写则绑定到此卡)  
  @CouponUID            varchar(512),           -- 需要激活的Coupon号码.如果多coupon则填写首号码  
  @CouponCount          int,                    -- 默认1, 大于1则表示从 @CouponNumber 开始的连续Coupon, 如果中间有不同状态的coupon,则返回错误.  
  @Brandcode            varchar(512),  -- 品牌编号   
  @StoreCode            varchar(512),  -- 店铺编号  
  @RegisterCode         varchar(512),           -- 终端号码  
  @ServerCode           varchar(512)='',        -- serverid   
  @TxnNoSN              varchar(512),           -- 交易唯一序号(每次正常调用,都提供不同的序号,确保唯一)  
  @TxnNo                varchar(512),           -- 交易单号  
  @BusDate              datetime,               -- 交易busdate  
  @TxnDate              datetime,    -- 交易发生日期时间  
  @Amount               money ,           -- 支付金额（如果coupon金额大于支付金额，则redeem coupon，并且返回支付金额。如果coupon金额小于支付金额则redeem coupon，并且返回coupon金额）  
  @VoidTxnNo            varchar(64),            -- 原单交易单号 
  @VoidTxnNoSN          varchar(64),            -- 原单交易单号 序号
  @SecurityCode         varchar(512),           -- 签名    
  @NeedCallConfirm      int=0,                  -- 是否需要调用POSTxnConfirm。0：不需要。 1：需要  
  @Additional           varchar(512),           -- 附加信息  
  @ItemList             varchar(max),   -- 销售清单  
  @CouponExpiryDate     datetime output,        -- coupon有效期  
  @CouponStatus         int output,             -- coupon状态    
  @CouponAmount         money output,           -- coupon的面额    
  @ApprovalCode         varchar(512) output,    -- 操作批核号码。（只有当@NeedCallConfirm=0时，返回ApprovalCode）  
  @DeductAmount   money output,   -- 实际扣款金额  
  @ForfeitAmount  money output,   -- Forfeit金额  
  @CloseBalance   money output,   -- 扣款后余额  
  @NeedPassword         int=0,       -- 是否验证密码。0：不需要。1：需要
  @CouponPassword       varchar(512)='',   -- Coupon密码   
  @TenderCode           varchar(64)   -- redeem coupon时，使用的POS的 TenderID。用于检查这个Coupon和Tender是否匹配     
AS  
/****************************************************************************  
**  Name : POSCouponRedeem  
**  Version: 1.0.0.3     
**  Description : 在POS端使用coupon.(使用) (调用入口程序) (根据设定调用不同的方法)
**    
**  Created by: Gavin @2014-05-26  
**  Modified by: Gavin @2014-06-10 （ver 1.0.0.1）增加输入参数@TenderCode。 redeem coupon时，使用的POS的 TenderID。用于检查这个Coupon和Tender是否匹配  
**  Modify by: Gavin @2014-09-05 （ver 1.0.0.2）(for 711) 增加输入参数@VoidTxnNoSN，用于void时检查原记录的依据 
**  Modify by: Gavin @2015-06-02 （ver 1.0.0.3）(for RRG) 临时版本. 考虑RRG现有的调用，暂时去掉参数@VoidTxnNoSN，另外，@NeedCallConfirm 默认为0 时，做相反操作。 0 变成 1， 1变成0.  此过程默认需要confirm。
**  Modify by: Gavin @2015-06-24 （ver 1.0.0.4）通用版本, 恢复 ver 1.0.0.2
****************************************************************************/    
begin  
  declare @Model varchar(64), @Result int
  exec GetAppParam @Model output
 -- if @Model = 'RRG'
 -- begin
  if ISNULL(@NeedCallConfirm, 0) = 0
    set @NeedCallConfirm = 1
      
    exec @Result = POSCouponRedeemNormal @UserID,@CardNumber,@CouponUID,@CouponCount,@Brandcode,@StoreCode,@RegisterCode,
        @ServerCode,@TxnNoSN,@TxnNo,@BusDate,@TxnDate,@Amount,@VoidTxnNo, @VoidTxnNoSN, @SecurityCode,@NeedCallConfirm,
        @Additional,@ItemList, @CouponExpiryDate output,@CouponStatus output, @CouponAmount output, 
        @ApprovalCode output, @DeductAmount output, @ForfeitAmount output, @CloseBalance output,
        @NeedPassword,@CouponPassword,@TenderCode
     
--  end
  
  return @Result
end

GO
