USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessagePrepList]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessagePrepList](
	[KeyID] [bigint] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[CardNumber] [varchar](64) NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[CardBrandID] [int] NULL,
	[CouponNumber] [varchar](64) NULL,
	[CouponTypeID] [int] NOT NULL,
	[OprID] [int] NOT NULL,
	[RelateMemberID] [int] NULL,
	[RelateCardNumber] [varchar](64) NULL,
	[RelateMemberMobile] [varchar](64) NULL,
	[CustomMsg] [nvarchar](2000) NULL,
	[Status] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_MESSAGEPREPLIST] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MessagePrepList] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[MessagePrepList] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[MessagePrepList] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收人的MemberID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收人的CardNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收人的CardTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收人的CardGradeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收人的CardType的BrandID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CardBrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关的CouponNumber，如果有的话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关的CouponTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关的MemberID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'RelateMemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关的CardNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'RelateCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关的MemberMobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'RelateMemberMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户自定义的消息。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CustomMsg'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。0：未发。 1：已发。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'待发消息的准备列表。
存放需要发送的消息，只存放消息的接收人，动态的参数，消息类型。 具体发送账号，消息主题等需要下一步处理。
另外的处理过程执行后，可以清空此表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessagePrepList'
GO
