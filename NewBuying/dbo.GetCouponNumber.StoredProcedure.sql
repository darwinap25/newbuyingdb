USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCouponNumber]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCouponNumber] 
  @CouponTypeID int,  
  @Status int,  
  @CouponNumber varchar(64) output  
/****************************************************************************  
**  Name : GetCouponNumber  
**  Version: 1.0.0.5  
**  Description : 获取可用的CouponNumber，根据CouponTypeID和Status  
**  Example :  
  declare @CouponNumber varchar(64)  
  exec GetCouponNumber 1,1, @CouponNumber output  
  print @CouponNumber  
**  Created by: Robin @2014-12-17  
**  1.0.0.2  Modify by: Robin @2014-12-18 修改重取记录逻辑，使用更新锁
**  1.0.0.3  Modify by: Robin @2015-01-30 修改重取记录逻辑，增加行锁定 ，扩展池容量到1000 
**  1.0.0.4  Modify by: Robin @2015-02-04 重新加载Pool表的时候增加表锁定  
**  1.0.0.5  Modify by: Gavin @2017-10-19 因为没有 GetCouponNumberHistory 表, 注销insert.
**  
****************************************************************************/  
as  
begin  
  declare @Times int=0
  declare @Number int=0  
  select @Number=count(*) from CouponPool with (NOLock) where CouponTypeID=@CouponTypeID and [Status]=@Status   
  if @Number<=100  
  begin  
    while @Times<20 
	begin
		begin try  
			insert into CouponPool(CouponNumber,CouponTypeID,[Status]) select top 1000 CouponNumber,CouponTypeID,[Status] from Coupon where CouponTypeID=@CouponTypeID and [Status]=@Status   
			and CouponNumber not in (select CouponNumber from CouponPool with (NoLock))  
			delete from CouponPool where [Status]=99  
			select top 1 CouponNumber from CouponPool where CouponTypeID=@CouponTypeID and [Status]=@Status   
			break
		end try  
		begin catch  
			set @Times=@Times+1
			waitfor delay '00:00:01'
		end catch 
	end 
  end  
  select top 1 @CouponNumber=CouponNumber from CouponPool with (ROWLOCK,UPDLock,Readpast) where CouponTypeID=@CouponTypeID and [Status]=@Status   
  update CouponPool set [Status]=99 where CouponNumber=@CouponNumber  
  --insert into GetCouponNumberHistory(CouponNumber,ProcessTime) values (@CouponNumber,GetDate())
end  

GO
