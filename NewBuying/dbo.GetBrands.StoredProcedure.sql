USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBrands]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBrands]
  @BrandCode             VARCHAR(64),			         -- 部门编码
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetBrands
**  Version : 1.0.0.0
**  Description : 获得货品的部门数据
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  exec @a = GetBrands '', '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
  
**  Created by Gavin @2015-03-06
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1  
    
  SET @BrandCode = ISNULL(@BrandCode, '')
  IF @BrandCode <> ''  
    SET @WhereStr = ' WHERE BrandCode=''' + @BrandCode + ''' '
  ELSE  
    SET @WhereStr = ''
    
  SET @SQLStr = 'SELECT BrandID, BrandCode, CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN BrandName2 WHEN 3 THEN BrandName3 ELSE BrandName1 END AS BrandName, '
    + ' CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN BrandDesc2 WHEN 3 THEN BrandDesc3 ELSE BrandDesc1 END AS BrandDesc, '
    + ' BrandPicSFile, BrandPicMFile, BrandPicGFile, CardIssuerID, IndustryID '
    + ' FROM BUY_BRAND '
  SET @SQLStr = @SQLStr + @WhereStr
  
  EXEC SelectDataInBatchs @SQLStr, 'BrandCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr  
END

GO
