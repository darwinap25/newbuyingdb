USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberPersonalRights]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateMemberPersonalRights]
  @MemberID                 int,            --会员主键
  @ReceiveAllAdvertising    int=0,          -- 0:不接收所有广告（所有messageservicetypeid）。 1：同意接收。
  @TransPersonalInfo        int=0           -- 同意传播个人信息。0：不同意。 1：同意      默认0.       
AS
/****************************************************************************
**  Name : UpdateMemberPersonalRights  
**  Version: 1.0.0.1
**  Description : update member 表记录
Parameter :
  declare @a int, @MemberID varchar(36)
  set @MemberID = 1
  exec @a = UpdateMemberPersonalRights @MemberID, 1, 1
  print @a  

**  Created by: Gavin @2013-06-05
**  Modify by: Gavin @2013-10-14 (ver 1.0.0.1) 记录操作到UserAction_Movement表
**
****************************************************************************/
begin
  update Member set ReceiveAllAdvertising = @ReceiveAllAdvertising, TransPersonalInfo = @TransPersonalInfo
  where MemberID = @MemberID    
  exec RecordUserAction @MemberID, 2, '', '', 'Update Member Information (MemberPersonalRights)', '', '', '', @MemberID
  return 0
end

GO
