USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoImportProduct_NoPending_Bauhaus]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[DoImportProduct_NoPending_Bauhaus]
AS
/****************************************************************************
**  Name : DoImportProduct_NoPending_Bauhaus  (bauhaus)
**  Version: 1.0.0.6
**  Description :
  sp_helptext DoImportProduct_NoPending_Bauhaus
   exec DoImportProduct_NoPending_Bauhaus
   truncate table IMP_PRODUCT
   select brandcode,* from IMP_PRODUCT
   select * from IMP_PRODUCT_TEMP
   select * from gender
   select * from buy_department
   select * from buy_brand
   sp_help IMP_PRODUCT
   select * from buy_rprice_m
   select * from BUY_PRODUCT_CLASSIFY
   select * from buy_barcode
   select * from BUY_PRODUCTSTYLE
   delete from buy_product where prodcode in (select INTERNAL_PRODUCT_CODE from IMP_PRODUCT)
   delete from BUY_PRODUCT_ADD_BAU where prodcode in (select INTERNAL_PRODUCT_CODE from IMP_PRODUCT)
   delete from BUY_SUPPROD where prodcode in (select INTERNAL_PRODUCT_CODE from IMP_PRODUCT)
   delete from BUY_BARCODE where prodcode in (select INTERNAL_PRODUCT_CODE from IMP_PRODUCT)
   delete from BUY_PRODUCT_CLASSIFY where prodcode in (select INTERNAL_PRODUCT_CODE from IMP_PRODUCT)
   delete from BUY_Product_Particulars where prodcode in (select INTERNAL_PRODUCT_CODE from IMP_PRODUCT)
**  Created by Gavin @2016-09-08 (from DoImportProduct_Bauhaus ver 1.0.1.6) 
**  Modify by Gavin @2016-09-29 (ver 1.0.0.1) 
**  Modify by Gavin @2016-10-11 (ver 1.0.0.2) 修改sizem1~8 类型 由decimal 改为 varchar(64)
**  Modify by Gavin @2016-10-25 (ver 1.0.0.3) 如果sex是1, 4 就放在第一層departcode=01以下. 如果sex是2,3,5 , 就放到第一層departcode=02以下
**  Modify by Gavin @2016-12-14 (ver 1.0.0.4) 修改prodcodestyle的结构,改成 brand+year+season+article+sex+model (修正bug)
**  Modify by Gavin @2017-09-28 (ver 1.0.0.5) 增加更新： BUY_PRODUCTSTYLE.ProdStyleSizeFile = '/UploadFiles/Photo/' + @SizeCategory + '.jpg'
                                              BUY_PRODUCT_PIC.ProductFullPicFile 的内容末尾加上 '.jpg' 
											  更新BUY_PRODUCT  的ProdDesc1，ProdDesc2，ProdDesc3，ProdPicFile
**  Modify by Gavin @2017-09-29 (ver 1.0.0.6) 取消：  BUY_PRODUCT_PIC.ProductFullPicFile 的内容末尾加上 '.jpg' 
****************************************************************************/
BEGIN
  DECLARE @INTERNAL_PRODUCT_CODE varchar(64), @BrandID int
  DECLARE @COMPANY_ID varchar(64), @BARCODE varchar(64), @SUPPLIER_PRODUCT_CODE varchar(64), 
     @BRAND varchar(64), @YEAR int, @SEASON varchar(64), @ARTICLE varchar(64), @SEX int, @MODEL varchar(64), 
	 @COLOR varchar(64), @PSIZE varchar(64), @CLASS varchar(64), @DESCRIPTION nvarchar(512), @REORDER_LEVEL varchar(64), 
	 @RETIRED varchar(64), @RETIRE_DATE datetime, @LAST_UPDATE_DATE datetime, @LAST_UPDATE_USER varchar(64), 
	 @DESCRIPTION2 nvarchar(512), @SUPPLIER varchar(64), @STANDARD_COST money, @SIZE_RANGE varchar(64), 
	 @HS_CODE varchar(64), @EXPORT_COST money, @LOCAL_DESCRIPTION nvarchar(512), @COO varchar(64), @MATERIAL_1 varchar(64), 
	 @MATERIAL_2 varchar(64), @DESIGNER varchar(64), @BUYER varchar(64), @MERCHANDISER varchar(64), @SKU varchar(64),
	 @AVG_COST money, @PRICE money,@FINAL_DISCOUNT_PRICE money,
	 @ShortDescription1 nvarchar(512), @ShortDescription2 nvarchar(512), @ShortDescription3 nvarchar(512), 
	 @Material1 nvarchar(max), @Material2 nvarchar(max), @Material3 nvarchar(max),
	 @Detailed1 nvarchar(max), @Detailed2 nvarchar(max), @Detailed3 nvarchar(max), 
	 @Design1 nvarchar(max), @Design2 nvarchar(max), @Design3 nvarchar(max), 
	 @ProductName1 nvarchar(512), @ProductName2 nvarchar(512), @ProductName3 nvarchar(512), 
     @IsOnlineSKU INT, @OuterLayerMaterials varchar(512), @InnerLayerMaterials varchar(512),
	 @SizeCategory varchar(512),@Video varchar(512), @360Photos varchar(512), @Photos1 varchar(512),
	 @Photos2 varchar(512),@Photos3 varchar(512),@Photos4 varchar(512),@Photos5 varchar(512),
	 @Photos6 varchar(512),@Photos7 varchar(512),@Photos8 varchar(512),@Photos9 varchar(512),
	 @Photos10 varchar(512),@SizeM1 varchar(64),@SizeM2 varchar(64),@SizeM3 varchar(64),
	 @SizeM4 varchar(64),@SizeM5 varchar(64),@SizeM6 varchar(64),@SizeM7 varchar(64),
	 @SizeM8 varchar(64), @ProductSizeID INT
  DECLARE @UserID int, @SEXChar varchar(64)
  
  -- Insert into IMP_PRODUCT 
  INSERT INTO IMP_PRODUCT
  SELECT * FROM IMP_PRODUCT_TEMP WHERE Barcode NOT IN (SELECT Barcode FROM IMP_PRODUCT)

  UPDATE IMP_PRODUCT SET INTERNAL_PRODUCT_CODE = B.INTERNAL_PRODUCT_CODE, COMPANY_ID = B.COMPANY_ID, SUPPLIER_PRODUCT_CODE = B.SUPPLIER_PRODUCT_CODE, BRAND = B.BRAND, [YEAR] = B.[YEAR],
	SEASON = B.SEASON, ARTICLE = B.ARTICLE, SEX = B.SEX, MODEL = B.MODEL,COLOR = B.COLOR, PSIZE = B.PSIZE,CLASS = B.CLASS, DESCRIPTION = B.DESCRIPTION,
	REORDER_LEVEL = B.REORDER_LEVEL, RETIRED = B.RETIRED, RETIRE_DATE = B.RETIRE_DATE, LAST_UPDATE_DATE = B.LAST_UPDATE_DATE,LAST_UPDATE_USER = B.LAST_UPDATE_USER,
	DESCRIPTION2 = B.DESCRIPTION2, SUPPLIER = B.SUPPLIER, STANDARD_COST = B.STANDARD_COST, SIZE_RANGE = B.SIZE_RANGE,HS_CODE = B.HS_CODE,EXPORT_COST=B.EXPORT_COST,
	LOCAL_DESCRIPTION = B.LOCAL_DESCRIPTION, COO = B.COO, MATERIAL_1 = B.MATERIAL_1, MATERIAL_2 = B.MATERIAL_2,DESIGNER = B.DESIGNER,BUYER=B.BUYER,
	MERCHANDISER = B.MERCHANDISER, SKU = B.SKU, AVG_COST = B.AVG_COST, PRICE = B.PRICE,FINAL_DISCOUNT_PRICE = B.FINAL_DISCOUNT_PRICE,ShortDescription1=B.ShortDescription1,
	ShortDescription2 = B.ShortDescription2, ShortDescription3 = B.ShortDescription3, Material1 = B.Material1, Material2 = B.Material2,Material3 = B.Material3,
	Detailed1 = B.Detailed1, Detailed2 = B.Detailed2, Detailed3 = B.Detailed3, Design1 = B.Design1,Design2 = B.Design2,Design3=B.Design3,
	ProductName1 = B.ProductName1, ProductName2 = B.ProductName2, ProductName3 = B.ProductName3, IsOnlineSKU = B.IsOnlineSKU,OuterLayerMaterials = B.OuterLayerMaterials,
	InnerLayerMaterials = B.InnerLayerMaterials, SizeCategory = B.SizeCategory, Video = B.Video, [360Photos] = B.[360Photos],Photos1 = B.Photos1,Photos2=B.Photos2,
	Photos3 = B.Photos3, Photos4 = B.Photos4, Photos5 = B.Photos5, Photos6 = B.Photos6,Photos7 = B.Photos7,Photos8=B.Photos8,Photos9=B.Photos9,Photos10=B.Photos10,
	SizeM1 = B.SizeM1, SizeM2 = B.SizeM2, SizeM3 = B.SizeM3, SizeM4 = B.SizeM4, SizeM5= B.SizeM5,SizeM6=B.SizeM6,SizeM7=B.SizeM7,SizeM8=B.SizeM8
  FROM IMP_PRODUCT A LEFT JOIN IMP_PRODUCT_TEMP B ON A.BARCODE = B.BARCODE
  WHERE B.BARCODE IS NOT NULL

  -- insert department
  insert into BUY_DEPARTMENT(DepartCode, DepartName1, DepartName2, DepartName3,Additional)
  select case when isnull(Sex,0) in (1,4) then '01' else '02' end, case when isnull(Sex,0) in (1,4) then '01' else '02' end,case when isnull(Sex,0) in (1,4) then '01' else '02' end,case when isnull(Sex,0) in (1,4) then '01' else '02' end,'' FROM IMP_PRODUCT 
  where case when isnull(Sex,0) in (1,4) then '01' else '02' end not in (select DepartCode from BUY_DEPARTMENT)  and Sex is not null
    AND ISNULL([DESCRIPTION], '') <> ''
  Group By case when isnull(Sex,0) in (1,4) then '01' else '02' end 
  
  insert into BUY_DEPARTMENT(DepartCode, DepartName1, DepartName2, DepartName3,Additional)
  select case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class, case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class,case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class,case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class,'' FROM IMP_PRODUCT 
  where case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class not in (select DepartCode from BUY_DEPARTMENT) and Sex is not null and isnull(Class,'') <> ''
    AND ISNULL([DESCRIPTION], '') <> ''
  Group By case when isnull(Sex,0) in (1,4) then '01' else '02' end,Class

  insert into BUY_DEPARTMENT(DepartCode, DepartName1, DepartName2, DepartName3,Additional)
  select case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class+ARTICLE, case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class+ARTICLE,case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class+ARTICLE,case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class+ARTICLE,''  FROM IMP_PRODUCT 
  where case when isnull(Sex,0) in (1,4) then '01' else '02' end+Class+ARTICLE not in (select DepartCode from BUY_DEPARTMENT) and Sex is not null and isnull(ARTICLE,'') <> '' and isnull(Class,'') <> ''
    AND ISNULL([DESCRIPTION], '') <> ''
  Group By case when isnull(Sex,0) in (1,4) then '01' else '02' end,Class,ARTICLE 

  -- insert COMPANY
  insert into Company(CompanyCode, CompanyName)
  select COMPANY_ID, COMPANY_ID FROM IMP_PRODUCT 
  where COMPANY_ID not in (select CompanyCode from Company) and isnull(COMPANY_ID,'') <> ''
    AND ISNULL([DESCRIPTION], '') <> ''
  Group By COMPANY_ID 

  -- insert SEASON
  insert into SEASON(SeasonCode, SeasonName1,SeasonName2,SeasonName3)
  select Season, Season,Season,Season FROM IMP_PRODUCT 
  where Season not in (select SeasonCode from SEASON) and isnull(Season,'') <> ''
  Group By Season 
  
  -- insert Buy_Color
  insert into Buy_Color(ColorCode, ColorName1,ColorName2,ColorName3,ColorPicFile)
  select COLOR,COLOR,COLOR,COLOR,'' FROM IMP_PRODUCT 
  where COLOR not in (select ColorCode from Buy_Color) and isnull(COLOR,'') <> ''
  Group By COLOR 

  -- insert BUY_PRODUCTSIZE
  insert into BUY_PRODUCTSIZE(ProductSizeCode, ProductSizeType, ProductSizeName1, ProductSizeName2, ProductSizeName3, ProductSizeNote)
  select A.PSize, A.SIZE_RANGE, A.PSize , A.PSize, A.PSize,''  from
  ( select PSize, SIZE_RANGE FROM IMP_PRODUCT A
    Group by PSize, SIZE_RANGE
  ) A left join BUY_PRODUCTSIZE B on A.PSIZE = B.ProductSizeCode and A.SIZE_RANGE = B.ProductSizeType
  where B.ProductSizeID is null and isnull(A.PSize,'') <> '' and isnull(A.SIZE_RANGE,'') <> ''


  -- insert buy_brand
  insert into buy_brand(BrandCode, BrandName1, BrandName2, BrandName3)
  select Brand, Brand, Brand,Brand FROM IMP_PRODUCT 
  where Brand not in (select BrandCode from buy_brand) and isnull(Brand,'') <> ''
    AND ISNULL([DESCRIPTION], '') <> ''
  Group By Brand 


  DECLARE CUR_DoImportProduct_NoPending_Bauhaus CURSOR fast_forward FOR
    SELECT INTERNAL_PRODUCT_CODE,COMPANY_ID,BARCODE,SUPPLIER_PRODUCT_CODE,BRAND,[YEAR],SEASON,ARTICLE, RTrim(ISNULL(A.SEX,'')),MODEL, 
	 COLOR,PSIZE,CLASS,A.[DESCRIPTION],REORDER_LEVEL,RETIRED,RETIRE_DATE,LAST_UPDATE_DATE,LAST_UPDATE_USER,
	 DESCRIPTION2,SUPPLIER,STANDARD_COST,SIZE_RANGE,HS_CODE,EXPORT_COST,LOCAL_DESCRIPTION,COO,MATERIAL_1, 
	 MATERIAL_2,DESIGNER,BUYER,MERCHANDISER,SKU,AVG_COST,PRICE,FINAL_DISCOUNT_PRICE, B.UserID,
	 ShortDescription1, ShortDescription2, ShortDescription3, Material1, Material2, Material3,
	 Detailed1, Detailed2, Detailed3, Design1, Design2, Design3, ProductName1, ProductName2, ProductName3,IsOnlineSKU, 
	 OuterLayerMaterials, InnerLayerMaterials,SizeCategory,Video,360Photos, Photos1,Photos2,Photos3,
	 Photos4,Photos5,Photos6,Photos7,Photos8,Photos9,Photos10,SizeM1,SizeM2,SizeM3,SizeM4,SizeM5,SizeM6,SizeM7,SizeM8, C.ProductSizeID
	 FROM IMP_PRODUCT A
	 LEFT JOIN Accounts_Users B on A.LAST_UPDATE_USER = B.UserName
	 LEFT JOIN BUY_PRODUCTSIZE C on A.PSIZE = C.ProductSizeCode AND A.SIZE_RANGE = C.ProductSizeType
	 WHERE ISNULL(A.[DESCRIPTION], '') <> ''
  OPEN CUR_DoImportProduct_NoPending_Bauhaus
  FETCH FROM CUR_DoImportProduct_NoPending_Bauhaus INTO @INTERNAL_PRODUCT_CODE,@COMPANY_ID,@BARCODE,@SUPPLIER_PRODUCT_CODE,@BRAND,
     @YEAR,@SEASON,@ARTICLE,@SEX,@MODEL,@COLOR,@PSIZE,@CLASS,@DESCRIPTION,@REORDER_LEVEL,@RETIRED,@RETIRE_DATE,
	 @LAST_UPDATE_DATE,@LAST_UPDATE_USER,@DESCRIPTION2,@SUPPLIER,@STANDARD_COST,@SIZE_RANGE,@HS_CODE,@EXPORT_COST,
	 @LOCAL_DESCRIPTION,@COO,@MATERIAL_1,@MATERIAL_2,@DESIGNER,@BUYER,@MERCHANDISER,@SKU,@AVG_COST,@PRICE,@FINAL_DISCOUNT_PRICE,@UserID,
	 @ShortDescription1, @ShortDescription2, @ShortDescription3, @Material1, @Material2, @Material3,
	 @Detailed1, @Detailed2, @Detailed3, @Design1, @Design2, @Design3, @ProductName1, @ProductName2, @ProductName3,@IsOnlineSKU, 
	 @OuterLayerMaterials, @InnerLayerMaterials,@SizeCategory,@Video,@360Photos, @Photos1,@Photos2,@Photos3,
	 @Photos4,@Photos5,@Photos6,@Photos7,@Photos8,@Photos9,@Photos10,@SizeM1,@SizeM2,@SizeM3,@SizeM4,@SizeM5,@SizeM6,@SizeM7,@SizeM8,@ProductSizeID
  WHILE @@FETCH_STATUS=0
  BEGIN
    SET @SEXChar = case when isnull(@Sex,0) in (1,4) then '01' else '02' end
    if not exists(select * from BUY_PRODUCT where ProdCode = @BARCODE)
	begin
	  insert into BUY_PRODUCT(ProdCode,ProdDesc1,ProdDesc2,ProdDesc3,StoreBrandCode,ProdSpec,DepartCode,
            StoreCode,MinOrderQty,OrderType,NonOrder,NonSale,ProductType,Modifier,BOM,MutexFlag,
			StartDate,EndDate,DefaultPickupStoreCode,ColorCode,ProductSizeCode,
			CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,IsOnlineSKU,ProdPicFile)
      values(@BARCODE, @DESCRIPTION, @DESCRIPTION2,@LOCAL_DESCRIPTION,@BRAND,@ShortDescription1, 
	       case when isnull(@SEX,0) in (1,4) then '01' else '02' end + RIGHT('00' + RTrim(ISNULL(@CLASS,'')), 2) + RIGHT('00' + LTrim(ISNULL(@ARTICLE,'')), 2), 
	       '', 1,0,0, case when @RETIRED = 'Y' then 1 else 0 end ,0,1,0,0,
		   Getdate(), Getdate() + 1000, '', @COLOR, @ProductSizeID, 
		   Getdate(),@UserID,@LAST_UPDATE_DATE,@UserID,@IsOnlineSKU, '/UploadFiles/Photo/' + @Photos1 + '.jpg')
    end else
	begin
	  Update BUY_PRODUCT set ProdDesc1 = @DESCRIPTION,ProdDesc2 = @DESCRIPTION2,ProdDesc3 = @LOCAL_DESCRIPTION,StoreBrandCode=@BRAND,ProdSpec=@ShortDescription1,
	        DepartCode=case when isnull(@SEX,0) in (1,4) then '01' else '02' end + RIGHT('00' + RTrim(ISNULL(@CLASS,'')), 2) + RIGHT('00' + LTrim(ISNULL(@ARTICLE,'')), 2),
            NonSale=case when @RETIRED = 'Y' then 1 else 0 end, ColorCode=@COLOR,ProductSizeCode=@ProductSizeID,ProdPicFile='/UploadFiles/Photo/' + @Photos1 + '.jpg',
			UpdatedOn=Getdate()
	  where ProdCode = @BARCODE
	end

	  delete from BUY_PRODUCT_ADD_BAU where ProdCode = @BARCODE
	  delete from BUY_PRODUCT_CLASSIFY where ProdCode = @BARCODE
	  delete from BUY_Product_Particulars where ProdCode = @BARCODE
	  delete from BUY_PRODUCT_PIC where ProdCode = @BARCODE
	  delete from BUY_PRODUCTSTYLE where ProdCode = @BARCODE
	  delete from BUY_SUPPROD where ProdCode = @BARCODE
	  delete from BUY_BARCODE where ProdCode = @BARCODE 	
	  delete from BUY_RPRICE_M where ProdCode = @BARCODE   

      insert into BUY_PRODUCT_ADD_BAU (ProdCode,Standard_Cost,Export_Cost,AVG_Cost,MODEL,SKU,[YEAR],REORDER_LEVEL,HS_CODE,COO,
	      SIZE_RANGE,DESIGNER,BUYER,MERCHANDISER,RETIRE_DATE,CompanyCode, SizeM1,SizeM2,SizeM3,SizeM4,SizeM5,SizeM6,SizeM7,SizeM8)
      values(@BARCODE,@Standard_Cost,@Export_Cost,@AVG_Cost,@MODEL,@SKU,@YEAR,@REORDER_LEVEL,@HS_CODE,@COO,
	      @SIZE_RANGE,@DESIGNER,@BUYER,@MERCHANDISER,@RETIRE_DATE,@COMPANY_ID,@SizeM1,@SizeM2,@SizeM3,@SizeM4,@SizeM5,@SizeM6,@SizeM7,@SizeM8)
	 
	  if isnull(@SUPPLIER,'') <> ''
	    insert into BUY_SUPPROD(VendorCode,ProdCode,SUPPLIER_PRODUCT_CODE,in_tax,Prefer,IsDefault)
	    values(@SUPPLIER,@BARCODE,@SUPPLIER_PRODUCT_CODE,0,0,1)
	      
      insert into BUY_BARCODE(Barcode, ProdCode, InternalBarcode)
	  values(@INTERNAL_PRODUCT_CODE, @BARCODE, 1)
      insert into BUY_RPRICE_M(ProdCode, RPriceTypeCode, RefPrice, Price, StartDate, EndDate, Status)
	  values(@BARCODE, '', isnull(@PRICE,0),isnull(@FINAL_DISCOUNT_PRICE,0), Getdate(), '2025-01-01', 1)
  
      insert into BUY_PRODUCT_CLASSIFY(ProdCode, ForeignkeyID, ForeignTable)
	  select @BARCODE, SeasonID, 'SEASON' from SEASON where SeasonCode = @SEASON
	  if ISNULL(@SEX, 0) <> 0
		  insert into BUY_PRODUCT_CLASSIFY(ProdCode, ForeignkeyID, ForeignTable)
		  values(@BARCODE, @SEX, 'Gender')   

	  insert into BUY_Product_Particulars(ProdCode, LanguageID, Memo1, MemoTitle1,memo2,memo3,Memo4,memo5)
	  values(@BARCODE, 1, @Material1, @ShortDescription1,@Detailed1,@Design1,@OuterLayerMaterials,@InnerLayerMaterials)
	  insert into BUY_Product_Particulars(ProdCode, LanguageID, Memo1, MemoTitle1,memo2,memo3,Memo4,memo5)
	  values(@BARCODE, 2, @Material1, @ShortDescription2,@Detailed2,@Design2,@OuterLayerMaterials,@InnerLayerMaterials)
	  insert into BUY_Product_Particulars(ProdCode, LanguageID, Memo1, MemoTitle1,memo2,memo3,Memo4,memo5)
	  values(@BARCODE, 3, @Material1, @ShortDescription3,@Detailed3,@Design3,@OuterLayerMaterials,@InnerLayerMaterials)
	  
	  if isnull(@Video, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Video, '','','',1,0,0)
	  if isnull(@360Photos, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @360Photos, '','','',0,1,0)
	  if isnull(@SizeCategory, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @SizeCategory, '','','',0,0,1)
	  if isnull(@Photos1, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos1, '','','',0,0,0)
	  if isnull(@Photos2, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos2, '','','',0,0,0)
	  if isnull(@Photos3, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos3, '','','',0,0,0)
	  if isnull(@Photos4, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos4, '','','',0,0,0)
	  if isnull(@Photos5, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos5, '','','',0,0,0)
	  if isnull(@Photos6, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos6, '','','',0,0,0)
	  if isnull(@Photos7, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos7, '','','',0,0,0)
	  if isnull(@Photos8, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos8, '','','',0,0,0)
	  if isnull(@Photos9, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos9, '','','',0,0,0)
	  if isnull(@Photos10, '') <> ''
	    insert into BUY_PRODUCT_PIC(ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1,ProductPicNote2,ProductPicNote3, IsVideo, Is360Pic, IsSizeCategory)
		values(@BARCODE, '', @Photos10, '','','',0,0,0)

      --update BUY_PRODUCT_PIC set ProductFullPicFile = ProductFullPicFile + '.jpg'
	  --where ProdCode = @BARCODE and RIGHT(ProductFullPicFile, 4) <> '.jpg'

	  -- ver 1.0.0.1
	  --insert into BUY_PRODUCTSTYLE(ProdCode, ProdCodeStyle, ProductSizeType)
	  --values(@BARCODE, RTrim(isnull(@MODEL,@BARCODE)) + LTrim(@YEAR) , '')
	  -- ver 1.0.0.4  brand+year+season+article+sex+model  
	  insert into BUY_PRODUCTSTYLE(ProdCode, ProdCodeStyle, ProductSizeType, ProdStyleSizeFile)
	  values(@BARCODE, RTrim(@Brand) + RTrim(LTrim(@YEAR)) + RTrim(LTrim(@SEASON)) + RTrim(LTrim(@ARTICLE)) + RTrim(LTrim(@SEX)) + RTrim(LTrim(@MODEL)), '','/UploadFiles/Photo/' + @SizeCategory + '.jpg')
	  
    FETCH FROM CUR_DoImportProduct_NoPending_Bauhaus INTO @INTERNAL_PRODUCT_CODE,@COMPANY_ID,@BARCODE,@SUPPLIER_PRODUCT_CODE,@BRAND,
       @YEAR,@SEASON,@ARTICLE,@SEX,@MODEL,@COLOR,@PSIZE,@CLASS,@DESCRIPTION,@REORDER_LEVEL,@RETIRED,@RETIRE_DATE,
	   @LAST_UPDATE_DATE,@LAST_UPDATE_USER,@DESCRIPTION2,@SUPPLIER,@STANDARD_COST,@SIZE_RANGE,@HS_CODE,@EXPORT_COST,
	   @LOCAL_DESCRIPTION,@COO,@MATERIAL_1,@MATERIAL_2,@DESIGNER,@BUYER,@MERCHANDISER,@SKU,@AVG_COST,@PRICE,@FINAL_DISCOUNT_PRICE,@UserID,
	   @ShortDescription1, @ShortDescription2, @ShortDescription3, @Material1, @Material2, @Material3,
	   @Detailed1, @Detailed2, @Detailed3, @Design1, @Design2, @Design3, @ProductName1, @ProductName2, @ProductName3,@IsOnlineSKU, 
	   @OuterLayerMaterials, @InnerLayerMaterials,@SizeCategory,@Video,@360Photos, @Photos1,@Photos2,@Photos3,
	   @Photos4,@Photos5,@Photos6,@Photos7,@Photos8,@Photos9,@Photos10,@SizeM1,@SizeM2,@SizeM3,@SizeM4,@SizeM5,@SizeM6,@SizeM7,@SizeM8,@ProductSizeID
  END
  CLOSE CUR_DoImportProduct_NoPending_Bauhaus 
  DEALLOCATE CUR_DoImportProduct_NoPending_Bauhaus
    
  --TRUNCATE TABLE IMP_PRODUCT_TEMP
  return 0 
END

GO
