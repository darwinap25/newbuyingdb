USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[ProdCode] [varchar](64) NOT NULL,
	[ProdName1] [nvarchar](512) NULL,
	[ProdName2] [nvarchar](512) NULL,
	[ProdName3] [nvarchar](512) NULL,
	[DepartCode] [varchar](64) NULL,
	[ProductBrandID] [int] NULL,
	[NonSale] [int] NULL DEFAULT ((0)),
	[ProdPicFile] [nvarchar](512) NULL,
	[ProdType] [int] NULL DEFAULT ((0)),
	[ProdNote] [nvarchar](512) NULL,
	[PackQty] [int] NULL,
	[NewFlag] [int] NULL DEFAULT ((0)),
	[HotSaleFlag] [int] NULL DEFAULT ((0)),
	[Flag1] [int] NULL DEFAULT ((0)),
	[Flag2] [int] NULL DEFAULT ((0)),
	[Flag3] [int] NULL DEFAULT ((0)),
	[Flag4] [int] NULL DEFAULT ((0)),
	[Flag5] [int] NULL DEFAULT ((0)),
	[OriginID] [int] NULL,
	[ColorID] [int] NULL DEFAULT ((0)),
	[ColorCode] [varchar](64) NULL,
	[ProductSizeID] [int] NULL,
	[ProductSizeCode] [varchar](64) NULL,
	[AddPointFlag] [int] NULL DEFAULT ((1)),
	[AddPointValue] [int] NULL DEFAULT ((0)),
	[IsOnlineSKU] [int] NULL DEFAULT ((0)),
	[SKUWeight] [decimal](12, 4) NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_PRODUCT] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProdName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProdName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProdName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductBrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非销售货品。 0： 不是不能销售货品。 1：是不能销售货品。  默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'NonSale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProdPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品类型。
0： 销售商品
1： 费用类型 （非商品）（价格不定，比如运费，维修费，服务费）
2： msvc Card  （销售卡或者充值）
3： msvc Coupon （销售Coupon）
4： msvc Card  （购买积分）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProdType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProdNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售包装单位数量 (eg. 100)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'PackQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新品标志。 0：非新品。1：新品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'NewFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'热销标志。0：非热销。1：热销' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'HotSaleFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Flag1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Flag2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Flag3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Flag4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展标志5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Flag5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品原产地ID。（目前内容定义为NationID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'OriginID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'颜色ID。默认0:没有颜色可选' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ColorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品颜色。（Color表的ColorCode）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ColorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductSizeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductSizeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售货品是否参与增加积分的标志。默认1
0：不增加积分。
1：按照PointRule规则增加积分。
2：指定的积分数量 （AddPointValue的值）
3：按照照PointRule规则增加的积分 * 指定的倍数。 （AddPointValue 中设置倍数。 只允许整数倍）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'AddPointFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据AddPointFlag的设置，决定AddPointValue的内容。 默认0
AddPointFlag 为2 时，此内容为销售此货品增加的积分。
AddPointFlag 为3 时，此内容为销售此货品是按规则计算积分后 的 倍数。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'AddPointValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此货品是否需要同步到 SVA。 1：是的。 0：不需要。 默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'IsOnlineSKU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品重量。用于快递费计算。 单位公斤' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'SKUWeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品表
@2015-09-24  NonSale 定义改成和 webbuying的相同。 1 ：不能销售。 0 ：可以销售。 默认 0
@2015-12-30： 增加字段： SKUWeight
@2016-08-08  增加字段isOnlineSKU。 1：可用于onlineshopping 中销售。 0：只是用于交易单join读取数据。不在网页上上销售
@2016-08-12  增加字段ColorCode。 用于取代ColorID.   ColorID字段不再使用，暂时未删除。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product'
GO
