USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetRSAPrivateKey]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetRSAPrivateKey]
  @NewPrivateKey varchar(512) output,
  @StartDate date output,
  @EndDate date output  
AS
/****************************************************************************
**  Name : GetRSAPrivateKey
**  Version: 1.0.0.0
**  Description : 获取RSAKeys表中的有效私钥.
**
**  Parameter :
declare @NewPublicKey varchar(512), @StartDate date,  @EndDate date
exec GetRSAPrivateKey @NewPublicKey output, @StartDate output,  @EndDate output
print @NewPublicKey
  print @StartDate
  print @EndDate   
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin
  declare @BinaryValue varbinary(1024)    
  select Top 1 @BinaryValue = PrivateKey, @StartDate = StartDate, @EndDate = EndDate from RSAKeys
  where StartDate <= GetDate() and EndDate >= GetDate()   
  order by StartDate  
				
  exec DecryptRSAKey @BinaryValue, @NewPrivateKey output

  return 0  
end

GO
