USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_REPLENFORMULA]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_REPLENFORMULA](
	[ReplenFormulaID] [int] IDENTITY(1,1) NOT NULL,
	[ReplenFormulaCode] [varchar](64) NOT NULL,
	[Description] [varchar](512) NULL,
	[PreDefineDOC] [int] NULL,
	[RunningStockDOC] [int] NULL,
	[OrderRoundUpQty] [int] NULL,
	[AVGDailySalesPeriod] [int] NULL,
	[Quotation] [int] NULL,
	[Deposited] [int] NULL,
	[AdvSales] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_REPLENFORMULA] PRIMARY KEY CLUSTERED 
(
	[ReplenFormulaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_REPLENFORMULA] ADD  DEFAULT ((7)) FOR [PreDefineDOC]
GO
ALTER TABLE [dbo].[BUY_REPLENFORMULA] ADD  DEFAULT ((7)) FOR [AVGDailySalesPeriod]
GO
ALTER TABLE [dbo].[BUY_REPLENFORMULA] ADD  DEFAULT ((0)) FOR [Quotation]
GO
ALTER TABLE [dbo].[BUY_REPLENFORMULA] ADD  DEFAULT ((0)) FOR [Deposited]
GO
ALTER TABLE [dbo].[BUY_REPLENFORMULA] ADD  DEFAULT ((0)) FOR [AdvSales]
GO
ALTER TABLE [dbo].[BUY_REPLENFORMULA] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_REPLENFORMULA] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'ReplenFormulaID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'ReplenFormulaCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留库存天数. (当库存数量<=预留库存天数*平均每天销售数量, 则触发补货)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'PreDefineDOC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'正常库存天数。（补货数量为： 平均每天销售数量 * (正常库存天数- 预留库存天数）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'RunningStockDOC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货数量的倍数：
Sample:
max=300 
current=97
max-current=203
203 will be rounded up to 250 since it is the next number divisible by 50.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'OrderRoundUpQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计算平均销售数量的周期天数（即计算多少天内交易数的平均数）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'AVGDailySalesPeriod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数量统计是否包括报价销售类型。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'Quotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数量统计是否包括定金销售类型。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'Deposited'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数量统计是否包括预售销售类型。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA', @level2type=N'COLUMN',@level2name=N'AdvSales'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自动补货公式信息表
（根据销售货品统计每天的销售平均数，此表设置不区分货品，不区分店铺。）
@2016-07-13 增加正常运行库存数等字段。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLENFORMULA'
GO
