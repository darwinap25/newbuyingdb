USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCardCashDetail]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberCardCashDetail]
  @CardNumber			varchar(512)			-- 会员卡号
AS
/****************************************************************************
**  Name : GetMemberCardCashDetail  
**  Version: 1.0.0.0
**  Description : 获得会员卡的存款金额明细(可能按照有效期group)
**  Parameter :
      exec GetMemberCardCashDetail '000301831'
      select * from Card where cardnumber = '000301831'
truncate table Card_movement
select * from tender
 insert into CardCashDetail  (cardnumber, tendercode, tenderrate, addamount, balanceamount, forfeitamount, expirydate, status, createdon,createdby, updatedon,updatedby)
 values('000301831','RMB',1, '9994860.00', '9994860.00', 0, '2016-05-28', 1, getdate(),1,getdate(),1)
**  return: 
      数据集:CardNumber， CardTypeID， AddAmount（money）， BalanceAmount（money）， ExpiryDate（datetime）， Statusd（int）
**  Created by: Gavin @2012-03-06
**
****************************************************************************/
begin
  declare @CardTypeID int
  select @CardTypeID = C.CardTypeID from Card C Left join CardType T on C.CardTypeID = T.CardTypeID 
       where C.CardNumber = @CardNumber
  if isnull(@CardTypeID, 0) = 0
    return -2
  
  select * from 
  (select max(CardNumber) as CardNumber, @CardTypeID as CardTypeID,
		sum(isnull(AddAmount,0)) as RechargeAmount, sum(isnull(BalanceAmount,0)) as BalanceAmount, 
		sum(isnull(ForfeitAmount,0)) as ForfeitAmount, ExpiryDate, max([status]) as [status]  
  from CardCashDetail where CardNumber = @CardNumber and [status] = 1
  group by ExpiryDate) temp where BalanceAmount>0
  order by ExpiryDate
  return 0
end

GO
