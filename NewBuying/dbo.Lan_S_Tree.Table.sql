USE [NewBuying]
GO
/****** Object:  Table [dbo].[Lan_S_Tree]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lan_S_Tree](
	[NodeID] [int] NOT NULL,
	[Text] [nvarchar](255) NULL,
	[Lan] [nvarchar](50) NULL
) ON [PRIMARY]

GO
