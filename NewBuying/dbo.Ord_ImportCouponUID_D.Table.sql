USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportCouponUID_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportCouponUID_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ImportCouponNumber] [varchar](64) NULL,
	[CouponTypeID] [int] NULL,
	[CouponUID] [varchar](512) NULL,
	[ExpiryDate] [datetime] NULL,
	[BatchID] [varchar](512) NULL,
	[Denomination] [money] NULL,
 CONSTRAINT [PK_ORD_IMPORTCOUPONUID_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D', @level2type=N'COLUMN',@level2name=N'ImportCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实体优惠劵ID ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D', @level2type=N'COLUMN',@level2name=N'CouponUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'特定设置有效期。null表示使用coupon自己的有效期。否则使用此处的值更新到coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D', @level2type=N'COLUMN',@level2name=N'ExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入的BatchID，如果有值，则使用这个值update到绑定的Coupon的BatchCouponID中' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D', @level2type=N'COLUMN',@level2name=N'BatchID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'即CouponAmount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D', @level2type=N'COLUMN',@level2name=N'Denomination'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入Coupon的UID的 子表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_D'
GO
