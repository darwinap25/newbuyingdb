USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetRefundTransList_20171018]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetRefundTransList_20171018]
  @TransNum              VARCHAR(64),              -- 交易号。 空表示不设条件
  @TransType             INT,                      -- 交易类型。 0 表示不设条件
  @TransStatus           INT,                      -- 交易状态。 0 表示不设条件
  @StoreCode             VARCHAR(64),              -- POS 的店铺编号
  @RegisterCode          VARCHAR(64),              -- POS 的注册编号
  @BusDate               DATE,                     -- 交易的Business date
  @CashierID             INT,                      -- 收银员
  @CouponNumber          VARCHAR(64),              -- 销售的CouponNumber
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN  
AS
/****************************************************************************
**  Name : GetRefundTransList
**  Version : 1.0.0.1
**  Description : 获得可以被refund的交易数据. 
**
  declare  @a int, @PageCount int, @RecordCount int
  exec @a = GetRefundTransList '',0,0, '','',null,0, '01200002828000002',  '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  select * from sales_h
**  Created by Gavin @2015-03-26
**  Modify by Gavin @2017-09-27 (ver 1.0.0.1) 增加输入参数： @CouponNumber。 查询Sales_D中是否销售了这个CouponNumber
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1  
--  SELECT TOP 1 @BusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 ORDER BY BusDate desc
  SET @BusDate = ISNULL(@BusDate, GETDATE())    
  
  SET @TransNum = ISNULL(@TransNum, '')
  SET @TransType = ISNULL(@TransType, 0)
  SET @TransStatus = ISNULL(@TransStatus, 0)
  SET @CashierID = ISNULL(@CashierID, 0)
  SET @StoreCode = ISNULL(@StoreCode, '')
  SET @RegisterCode = ISNULL(@RegisterCode, '')
  SET @CouponNumber = ISNULL(@CouponNumber, '')
  --SET @BusDate = ISNULL(@BusDate, '1900-01-01')  

  SET @WhereStr = ' WHERE A.InvalidateFlag=0 AND A.TransType NOT IN (4,5) '
  IF @TransNum <> ''
    SET @WhereStr = @WhereStr + ' AND A.TransNum=''' + @TransNum + ''''
--  IF @TransType <> 0
--    SET @WhereStr = @WhereStr + ' AND A.TransType=' + CAST(@TransType as VARCHAR)
  IF @TransStatus <> 0
    SET @WhereStr = @WhereStr + ' AND A.Status=' + CAST(@TransStatus as VARCHAR)   
  IF @CashierID <> 0
    SET @WhereStr = @WhereStr + ' AND A.CashierID=' + CAST(@CashierID as VARCHAR)       
  IF @StoreCode <> ''
    SET @WhereStr = @WhereStr + ' AND A.StoreCode=''' + @StoreCode + ''''
  IF @RegisterCode <> ''
    SET @WhereStr = @WhereStr + ' AND A.RegisterCode=''' + @RegisterCode + ''''
  IF @CouponNumber <> ''
    SET @WhereStr = @WhereStr + ' AND TransNum IN (SELECT TransNum FROM Sales_D WHERE Additional1=''' + @CouponNumber + ''') '
    
 -- IF DATEDIFF(dd, @BusDate, '1900-01-01') < 0
 --   SET @WhereStr = @WhereStr + ' AND A.BusDate=''' + CONVERT(VARCHAR(10), @BusDate,120) + ''''

  SET @SQLStr = 'SELECT TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,TotalAmount,Status, '
    + ' TransDiscount,TransDiscountType,TransReason,RefTransNum,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,'
    + ' DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,'
    + ' RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,'
    + ' SettlementStaffID,PaySettleDate,CompleteDate,DeliveryNumber '
    + ' FROM SALES_H A '
  SET @SQLStr = @SQLStr + @WhereStr
  
  EXEC SelectDataInBatchs @SQLStr, 'TransNum', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr  

  RETURN 0
END


GO
