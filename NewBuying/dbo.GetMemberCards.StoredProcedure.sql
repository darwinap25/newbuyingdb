USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCards]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberCards]
  @MemberID				int,       -- 頗埜ID
  @ConditionStr			nvarchar(400),
  @PageCurrent			int=1,			   -- 菴撓珜暮翹ㄛ蘇1ㄛ苤衾脹衾0奀珩峈1
  @PageSize				int=0,			   -- 藩珜暮翹杅ㄛ 峈0奀ㄛ祥煦珜ㄛ蘇0
  @PageCount			int=0 output,	   -- 殿隙軞珜杅﹝
  @RecordCount			int=0 output,	   -- 殿隙軞暮翹杅﹝
  @LanguageAbbr			varchar(20)=''			   -- 1:CardTypeName1,2ㄩCardTypeName2. 3ㄩCardTypeName3.  坻ㄩCardTypeName1    
AS
/****************************************************************************
**  Name : GetMemberCards    鳳腕頗埜腔縐杅擂
**  Version: 1.0.0.6
**  Description : 怀頗埜翋瑩ㄛ殿隙扽衾頗埜腔Card
**  Parameter :
**
  declare @MemberID int, @count int, @recordcount int, @a int  
  set @MemberID = 6 
  exec @a = GetMemberCards @MemberID, '', 1, 30, @count output, @recordcount output, 'zh_CN'
  print @a  
  print @count
  print @recordcount
  sp_helptext GetMemberCards
  select * from Brand
**  Created by: Gavin @2012-02-20
**  Modified by: Robin @2012-09-19 (Ver 1.0.0.1) 硐衄CardStatus峈2,3,4 腔符埰勍腎翹
**  Modified by: Gavin @2013-06-21 (Ver 1.0.0.2) 妏蚚1.0.0.6唳掛腔SelectDataInBatchs	,怀腔@ConditionStr眻諉換跤SelectDataInBatchs 
**  Modified by: Gavin @2014-11-18 (Ver 1.0.0.3) 崝樓殿隙趼僇CardGradeName
**  Modified by: Gavin @2015-01-12 (Ver 1.0.0.4) ViewMemberCards 拸楊樓坰竘, 秪森祥妏蚚森弝芞,眻諉妏蚚SQL
**  Modified by: Gavin @2016-08-15 (Ver 1.0.0.5) (for Nick Bauhaus Onlineshopping) 崝樓殿隙蚘眊睿忒儂 岆瘁桄痐腔梓尨趼僇﹝崝樓 NickName
**  Modified by: Gavin @2017-06-22 (Ver 1.0.0.6) (for Newbuying DB) Brand党蜊趼僇﹝ 
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  if isnull(@MemberID, 0) = 0
    return -7

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  set @SQLStr = ' select case ' + CAST(@Language as varchar) + ' when 2 then CardTypeName2 when 3 then CardTypeName3 else CardTypeName1 end as CardTypeName,'
    + ' case ' + CAST(@Language as varchar) + ' when 2 then CardGradeName2 when 3 then CardGradeName3 else CardGradeName1 end as CardGradeName, '
    + ' (RTrim(C.CardTypeID) + C.CardNumber) as SeqNo, C.CardNumber, C.CardTypeID, Y.CardTypeCode, CardIssueDate, CardExpiryDate, '
    + ' C.MemberID, C.CardGradeID, C.Status as CardStatus, C.TotalPoints, C.TotalAmount, '
    + ' M.MemberEmail, G.CardGradeMaxAmount, C.ResetPassword, Y.CardTypeName1, Y.CardTypeName2, Y.CardTypeName3, '
    + ' M.MemberEngFamilyName, M.MemberEngGivenName, M.MemberChiFamilyName, M.MemberChiGivenName, '
    + ' M.MemberSex, M.MemberDateOfBirth, M.MemberDayofBirth, M.MemberMonthofBirth, M.MemberYearofBirth, M.HomeTelNum,'
    + ' V.vendorcardNumber as [UID], V.LaserID, Y.BrandID, B.StoreBrandName1 as BrandName, '
    + ' C.BatchCardID, C.ParentCardNumber, M.MemberIdentityRef, G.CardGradeName1, G.CardGradeName2, '
    + ' G.CardGradeName3, G.CardGradeLayoutFile, M.MemberRegisterMobile, M.MemberIdentityType, M.MemberMobilePhone, '
    + ' G.CardGradePicFile, G.CardGradeNotes, G.CardPointToAmountRate, G.CardAmountToPointRate, M.CountryCode, '
    + ' G.IsAllowStoreValue, G.CardPointTransfer, G.CardAmountTransfer, G.MinAmountPreAdd, G.MaxAmountPreAdd, '
    + ' G.MinAmountPreTransfer, G.MaxAmountPreTransfer, G.MinPointPreTransfer, G.MaxPointPreTransfer, '
    + ' G.DayMaxAmountTransfer, G.DayMaxPointTransfer, G.MinBalanceAmount, G.MinBalancePoint, G.MinConsumeAmount, '
    + ' B.StoreBrandPicSFile, B.StoreBrandPicMFile, B.StoreBrandPicGFile, G.CardGradeMaxPoint, G.MinPointPreAdd, G.MaxPointPreAdd, '
    + ' G.GracePeriodValue, G.GracePeriodUnit, C.CardAmountExpiryDate, C.CardPointExpiryDate, G.LoginFailureCount, '
    + ' G.QRCodePeriodValue, G.AllowOfflineQRCode, G.QRCodePrefix, G.TrainingMode '
	+ ' , MA1.VerifyFlag as EMailVerfyFlag, MA2.VerifyFlag as SMSVerfyFlag, M.NickName as MemberNickName' 
    + ' from dbo.[card] C left join dbo.Member M on C.MemberID = M.MemberID '     
    + ' left join dbo.CardGrade G on C.CardTypeID = G.CardTypeID and C.CardGradeID = G.CardGradeID '
    + ' left join dbo.CardType Y on C.CardTypeID = Y.CardTypeID '
    + ' left join dbo.Brand B on Y.BrandID = B.StoreBrandID '
    + ' left join (select top 1 * from MemberMessageAccount where MessageServiceTypeID = 1 and MemberID =  ' + convert(varchar(10), @MemberID) + ') MA1 on MA1.MemberID = C.MemberID '
    + ' left join (select top 1 * from MemberMessageAccount where MessageServiceTypeID = 2 and MemberID =  ' + convert(varchar(10), @MemberID) + ') MA2 on MA2.MemberID = C.MemberID '
    + ' left join (select * from dbo.VenCardIDMap where ValidateFlag = 0) V on C.CardNumber = V.CardNumber '
    + ' where C.MemberID = ' + convert(varchar(10), @MemberID) +' and C.Status in (2,3,4)'  

  exec SelectDataInBatchs @SQLStr, 'SeqNo', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr   

  return 0
end

GO
