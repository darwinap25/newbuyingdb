USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetAppParam]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetAppParam]
  @AppCode  varchar(64) output
as
/****************************************************************************    
**  Name : GetAppParam
**  Description :  内部使用，区分使用不同流程的客户。
  declare @AppCode varchar(64)
  exec GetAppParam @AppCode output
  print @AppCode
**  Created by: Gavin @2014-07-18    
****************************************************************************/  
begin
set @AppCode = 'RRG'
if exists (select 1 from  sysobjects where  id = object_id('AppParam') and type = 'U')
   select Top 1 @AppCode = AppCode from AppParam 
end

GO
