USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetColors]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetColors]
  @ColorID             int,               -- 颜色ID
  @ColorCode           varchar(64),       -- 颜色Code
  @LanguageAbbr        varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1 
AS
/****************************************************************************
**  Name : GetColors
**  Version: 1.0.0.0
**  Description : 获得颜色信息
**
**  Parameter :
  declare @a int, @MessageID int
  exec @a = GetColors 1,'',''
  print @a  
**  Created by: Gavin @2013-05-07
**
****************************************************************************/
begin
  declare @Language int
  set @ColorID = isnull(@ColorID, 0)
  set @ColorCode = isnull(@ColorCode, '')
  
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
      
  select case @Language when 2 then ColorName2 when 3 then ColorName3 else ColorName1 end as ColorName, 
    ColorID, ColorCode, ColorPicFile
   from Color where (ColorID = @ColorID or @ColorID = 0) and (ColorCode = @ColorCode or @ColorCode = '')
  
  return 0  
end

GO
