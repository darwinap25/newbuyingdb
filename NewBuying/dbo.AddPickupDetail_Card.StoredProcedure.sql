USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AddPickupDetail_Card]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[AddPickupDetail_Card]
  @UserID                   int,             --操作员ID
  @CardPickingNumber        varchar(64),       --单号
  @FilterType               int,             --筛选类型。1：batchID+数量。2：CardUID，3：起始cardnumber+数量 
  @CardTypeID               int,             --Card类型ID
  @CardGradeID              int,             --card级别ID
  @QTY                      int,             --要求的数量。 如果为0/null，那么就根据订单取。
  @BatchID                  int,             --优惠劵批次ID
  @CardUID                  varchar(64),    --CardUID, 使用CardUID时，一个UID一条记录。
  @StartCardNumber          varchar(64)      --卡创建单的号码
AS
/****************************************************************************
**  Name : AddPickupDetail_Card
**  Version: 1.0.0.0
**  Description : 根据AddPickupDetail_Card (Version: 1.0.0.3) 改写
**  example :
exec AddPickupDetail_Card 1, '123', 3, 1, 1, 10, 1, '234234', ''
**  Created by: Gavin @2014-10-09
**
****************************************************************************/
begin
  declare @CardOrderFormNumber varchar(64), @OrderQty int, @PickupQty int, @CardNumber varchar(64), @BrandID int
  declare @i int, @EndCardNumber varchar(64), @PrevCardNumber varchar(64), @A int
  declare @FirstCard varchar(64), @EndCard varchar(64)
  
  select @CardOrderFormNumber = ReferenceNo from Ord_CardPicking_H where CardPickingNumber = @CardPickingNumber
  select @OrderQty = sum(isnull(CardQty,0)) from Ord_CardOrderForm_D 
    where CardOrderFormNumber = @CardOrderFormNumber and CardTypeID = @CardTypeID
    group by CardTypeID
  select @PickupQty = sum(isnull(PickQTY,0)) from Ord_CardPicking_D 
     where CardPickingNumber = @CardPickingNumber and CardTypeID = @CardTypeID
    group by CardTypeID
  select @CardNumber = CardNumber from carduidmap where CardUID = @CardUID

  set @A = -2  
  set @OrderQty = isnull(@OrderQty, 0)
  set @PickupQty = isnull(@PickupQty, 0)
  set @QTY = isnull(@QTY, 0)

    if @FilterType = 1
    begin
      select @PickupQty = count(*) from Card 
        where BatchCardID = @BatchID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID 
            and Status = 0 and PickupFlag = 0 and StockStatus = 2
	    select Top 1 @StartCardNumber = CardNumber from Card 
	      where BatchCardID = @BatchID and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID 
	          and Status = 0 and PickupFlag = 0 and StockStatus = 2
	     order by CardNumber
	    exec @A = InsertPickupDetail_Card @CardPickingNumber, @CardTypeID, @CardGradeID, @StartCardNumber, @OrderQty, @PickupQty, 1
    end else if @FilterType = 2
    begin
	    exec @A = InsertPickupDetail_Card @CardPickingNumber, @CardTypeID, @CardGradeID, @CardNumber, @OrderQty, 1, 1
    end else if @FilterType = 3
    begin
      if isnull(@StartCardNumber, '') = ''
        select Top 1 @StartCardNumber = CardNumber from Card 
          where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID and Status = 0 and PickupFlag = 0 and StockStatus = 2
      set @PickupQty = isnull(@QTY, 0)  
      exec @A = InsertPickupDetail_Card @CardPickingNumber, @CardTypeID, @CardGradeID, @StartCardNumber, @OrderQty, @PickupQty, 1   
    end     
  
  if @A = 0
  begin
  -- update card的 PickupFlag
    DECLARE CUR_PickupCard_Detail CURSOR fast_forward for
      select FirstCardNumber, EndCardNumber from Ord_CardPicking_D where CardPickingNumber = @CardPickingNumber
    OPEN CUR_PickupCard_Detail
    FETCH FROM CUR_PickupCard_Detail INTO @FirstCard, @EndCard
    WHILE @@FETCH_STATUS=0 
    BEGIN 
      update card set PickupFlag = 1, StockStatus = 4 
        where CardNumber >= @FirstCard and  CardNumber <= @EndCard and CardTypeID = @CardTypeID and CardGradeID = @CardGradeID 
	    FETCH FROM CUR_PickupCard_Detail INTO @FirstCard, @EndCard
    END
    CLOSE CUR_PickupCard_Detail 
    DEALLOCATE CUR_PickupCard_Detail 
  end      
  return @A
end

GO
