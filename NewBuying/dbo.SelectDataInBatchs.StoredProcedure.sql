USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SelectDataInBatchs]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectDataInBatchs]
  @QueryStr			nvarchar(4000),    -- 查询语句例如: select * from card where cardnumber = 'XXXXX'
  @KeyField			nvarchar(60),	   -- 查询数据集的主键字段， 依据主键来确定取记录范围。
  @PageCurrent		int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize			int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount		int=1 output,	   -- 返回总页数。
  @RecordCount		int=0 output,	   -- 返回总记录数。
  @OrderCondition	nvarchar(100)='',	   -- 记录集排序条件,如果为空，则按照KeyField排序
  @QueryCondition	nvarchar(2000)=''	   -- 用户自定义查询条件。
AS
/****************************************************************************
**  Name : SelectDataInBatchs
**  Version: 1.0.0.10
**  Description : 分批查询数据集。
**  sample:
      declare @m int
      exec SelectDataInBatchs 'select * from VieweMemberCard', 'seqno', 1, 50, @m output
      print @m
**  Created by: Gavin @2012-03-01
**  Modified by: Robin @2012-11-00 不使用自增长字段，做镜像后会出现 that inherits the identity property错误，更换为RowNumber方法
**  Modified by: Gavin @2012-11-30 修正取数时的 Order 问题。
**  Modified by: Gavin @2012-12-13 (ver 1.0.0.4) 修正不分页取数时的 Order 问题。
**  Modified by: Gavin @2013-02-06 (ver 1.0.0.5) 修正  @PageSize = 0 时, 不返回@RecordCount 问题
**  Modified by: Gavin @2013-06-19 (ver 1.0.0.6) 增加输入参数@QueryCondition
**  Modified by: Gavin @2013-06-24 (ver 1.0.0.7) 修正增加@QueryCondition后，返回的@PageCount不正确问题
**  Modified by: Gavin @2013-07-31 (ver 1.0.0.8) 修正增加@QueryCondition后，分页数量计算条件问题
**  Modified by: Gavin @2014-08-20 (ver 1.0.0.9) 修正当主表有别名时, 分页和不分页的 keyid 处理不同步出现错误的问题. 修正:在不分页时同样嵌套一层,去除别名
**  Modified by: Gavin @2014-10-11 (ver 1.0.0.10)取消@PageCurrent > @Pagecount的判断, 输入的当前页大于总页数时, 返回空数据. 
**
****************************************************************************/
BEGIN
  declare @DataRecordCount int, @x int, @y int, @SQLStr nvarchar(4000), @RemainderCount int
  set @Pagecount = 0
  set @OrderCondition = isnull(@OrderCondition, '')
  set @QueryCondition = isnull(@QueryCondition, '')
  
  if isnull(@QueryStr, '') = ''
    return -1
  if @QueryCondition <> ''
  begin
    set @SQLStr = N'select @count = count(*) from (select * from (' + @QueryStr + ') A ' + ' where (' + @QueryCondition + ')) B'             
    
    exec sp_executesql @SQLStr, N'@count bigint output',@DataRecordCount output
    select @RecordCount = @DataRecordCount      
  end else
  begin  
    set @SQLStr = N'select @count = count(*) from (' + @QueryStr + ') A'             
    exec sp_executesql @SQLStr, N'@count bigint output',@DataRecordCount output
    select @RecordCount = @DataRecordCount      
  end  
  
  if @PageSize > 0
  begin             
	select @Pagecount = @DataRecordCount / @PageSize
	select @RemainderCount = @DataRecordCount % @PageSize 
	if @RemainderCount > 0
	  set @Pagecount = @Pagecount + 1
	if @PageCurrent <= 0
      set @PageCurrent = 1 
  -- ver 1.0.0.10     
--	if @PageCurrent > @Pagecount
--      set @PageCurrent = @Pagecount 
  -- END  
    if @PageCurrent = @Pagecount and @RemainderCount > 0
      set @x = @RemainderCount
    else set @x = @PageSize        
    set @y = (@PageCurrent - 1) * @PageSize 
    if isnull(@x,0) < 0 set @x = 0
    if isnull(@y,0) < 0 set @y = 0   

    if isnull(@OrderCondition, '') = ''
      set @OrderCondition = ' order by ' + @KeyField
   
    if @QueryCondition <> ''
      set @SQLStr = N'select ROW_NUMBER() OVER( ' + @OrderCondition + ' ) as iid, * into #Temp from (select * from (select * from (' + @QueryStr + ') A ' + ' where (' + @QueryCondition + ')) B) D '
    else  
      set @SQLStr = N'select ROW_NUMBER() OVER( ' + @OrderCondition + ' ) as iid, * into #Temp from (' + @QueryStr + ') A '

    set @SQLStr= @SQLStr +' select * from #Temp where iid between '+cast(@y+1 as varchar(6))+' and '+cast(@y+@x as varchar(6))

--	if @QueryCondition <> ''
--	  set @SQLStr= @SQLStr + ' and (' + @QueryCondition + ')'

    exec sp_executesql @SQLStr
  end else
  begin
    set @SQLStr = @QueryStr
	  if @QueryCondition <> ''
	    set @SQLStr= 'select * from (select * from (' + @QueryStr + ') A ' + ' where (' + @QueryCondition + ')) B'
  	else
	    set @SQLStr= 'select * from (' + @QueryStr + ') A ' 
	      
    if @OrderCondition = ''
      set @OrderCondition = ' order by ' + @KeyField
    set @SQLStr= @SQLStr + ' ' + @OrderCondition  
            
    exec sp_executesql @SQLStr
  end  
  return 0      
END

GO
