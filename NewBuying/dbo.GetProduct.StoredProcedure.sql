USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProduct]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProduct]
  @Barcode              varchar(64),       -- 条码
  @Prodcode             varchar(64),       -- 货品编码
  @DepartCode           varchar(64),       -- 部门编码
  @ProdCodeStyle	    varchar(64),       -- 货品系列编码。
  @IsIncludeChild       int,               -- 0：不包括部门子部门。1：包括部门子部门。  
  @BrandID              int,               -- 品牌ID
  @ProdType             int,               -- 货品类型
  @ProductColorID       int,               -- 货品颜色条件
  @ProductSizeID        int,			   -- 货品尺寸条件
  @ReturnMode           int=0,             -- 0: 完整返回数据。 1：只返回部分字段。 默认0.  （目的：减少数据传输）
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''     -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1    
AS
/****************************************************************************
**  Name : GetProduct
**  Version: 1.0.0.9
**  Description : 获得货品数据	   
**
  declare @a int, @PageCount int, @RecordCount int, @IsIncludeChild int
  declare @Barcode varchar(64), @Prodcode varchar(64), @DepartCode varchar(64),@ProdCodeStyle varchar(64), @BrandID int, @ProdType int
  set @Barcode = ''
  set @Prodcode = '105523101001'
  set @DepartCode = '08'
  set @BrandID = 0
  set @ProdType = 0
  set @ProdCodeStyle =''
  set @IsIncludeChild=0
  exec @a = GetProduct @Barcode,@Prodcode,@DepartCode,@ProdCodeStyle, @IsIncludeChild, @BrandID, @ProdType, 0,0, 0, 0, 100, @PageCount output, @RecordCount output, 'zh_CN'
  print @a  
  print @PageCount
  print @RecordCount
  
**  Created by: Gavin @2012-09-18
**  Modify by: Gavin @2012-09-26 (ver 1.0.0.1) @BrandCode 参数传入的是 BrandID, 参数名称暂时不变，修改处理过程
**  Modify by: Gavin @2012-10-10 (ver 1.0.0.2) 修改传入参数@BrandCode varchar(64) 为 @BrandID int
取消此修改**  Modify by: Gavin @2013-04-01 (ver 1.0.0.3) 增加返回字段: P.PackDesc, P.PackUnit, P.PackQty, P.ProdFunction, P.prodIngredients, P.ProdInstructions
**  Modify by: Gavin @2013-05-01 (ver 1.0.0.4) 增加参数@IsIncludeChild, 是否包括department的子部门。-- 0：不包括子部门。1：包括子部门。增加返回ProductBrandID
**  Modify by: Gavin @2013-05-07 (ver 1.0.0.5) 增加参数@ProdCodeLine，按照ProductLine关系查询数据
**  Modify by: Gavin @2013-06-05 (ver 1.0.0.6) (for SASA) 根据传入的@DepartCode，到Product_Catalog表中查询
**  Modify by: Gavin @2013-06-07 (ver 1.0.0.7) 增加参数@ProductColorID，@ProductSizeID。需要product表增加字段，增加color表和product_size表
**  Modify by: Gavin @2013-07-09 (ver 1.0.0.8) 取消SASA的特别修改,增加输入条件,@ReturnMode int. 
**  Modify by: Gavin @2013-07-10 (ver 1.0.0.9) 增加库存数量返回。（需要使用ProductStockOnhand表）
**
****************************************************************************/
begin

  declare @SQLStr nvarchar(4000), @Language int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  set @BrandID = isnull(@BrandID, 0)    
  
  set @Barcode = isnull(@Barcode, '')
  set @Prodcode = isnull(@Prodcode, '')
  set @DepartCode = isnull(@DepartCode, '')
  set @ProdType = isnull(@ProdType, 0)
  set @IsIncludeChild = isnull(@IsIncludeChild, 0)
  set @ProdCodeStyle = isnull(@ProdCodeStyle, '')  
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))
  set @ProductColorID = isnull(@ProductColorID, 0)
  set @ProductSizeID = isnull(@ProductSizeID, 0)          
  
  set @SQLStr = 'select T.Barcode, P.ProdCode, D.DepartCode, D.BrandID, P.ProdType, P.ProductBrandID,  P.PackQty, P.OriginID, '
    + ' P.ColorID, P.NonSale, C.ColorCode, C.ColorPicFile, P.NewFlag, P.HotSaleFlag,'	
	+ '  case ' + cast(@Language as varchar) + ' when 2 then ColorName2 when 3 then ColorName3 else ColorName1 end as ColorName,  '	     
	+ '  case ' + cast(@Language as varchar) + ' when 2 then ProductSizeName2 when 3 then ProductSizeName3 else ProductSizeName1 end as ProductSizeName, '    
	+ '  R.ProdPriceType, R.NetPrice, R.DefaultPrice, Z.ProductSizeID, Z.ProductSizeCode, Q.OnhandQty '	
  if @ReturnMode = 0 
  begin
	set @SQLStr = @SQLStr + ' , ' 
	+ '  case ' + cast(@Language as varchar) + ' when 2 then NationName2 when 3 then NationName3 else NationName1 end as NationName, '
    + '  case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName , '
	+ '  case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '
	+ '  Z.ProductSizeNote, P.ProdPicFile, P.ProdNote, '
	+ '  case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName, '
	+ '  case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandDesc2 when 3 then B.ProductBrandDesc3 else B.ProductBrandDesc1 end as ProductBrandDesc, '
	+ '  ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, '
	+ '  A.ProdFunction, A.ProdIngredients, A.ProdInstructions, A.PackDesc, A.PackUnit, '
	+ '  A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6, A.MemoTitle1, A.MemoTitle2, A.MemoTitle3, A.MemoTitle4, A.MemoTitle5, A.MemoTitle6, '	
	+ '  P.Flag1, P.Flag2, P.Flag3, P.Flag4, P.Flag5, N.NationFlagFile '
  end	
  set @SQLStr = @SQLStr + ' from Product P '
          + ' left join (select distinct Barcode, ProdCode from ProductBarcode) T on T.ProdCode = P.ProdCode '    
          + ' left join (select * from Product_Price where status = 1) R on P.ProdCode = R.ProdCode '    
          + ' left join (select * from Product_Particulars where LanguageID = ' + cast(@Language as varchar) + ') A on P.ProdCode = A.Prodcode '
          + ' left join Product_Brand B on P.ProductBrandID = B.ProductBrandID '
          + ' left join Nation N on P.OriginID = N.NationID '         
          + ' left join Color C on P.ColorID = C.ColorID '
          + ' left join Product_Size Z on P.ProductSizeID = Z.ProductSizeID '
		  + ' left join Department D on D.DepartCode = P.DepartCode '  
		  + ' left join ProductStockOnhand Q on P.ProdCode = Q.ProdCode '  		  
		    
  set @SQLStr = @SQLStr + ' where (T.Barcode = ''' + @Barcode + ''' or ''' + @Barcode + ''' = '''')'
     + ' and (P.Prodcode = ''' + @Prodcode + ''' or ''' + @Prodcode + ''' = '''')'
     + ' and (D.BrandID = ' + cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + ' = 0)'
     + ' and (P.ProdType = ' + cast(@ProdType as varchar) + ' or ' + cast(@ProdType as varchar) + ' = 0)'
     + ' and (P.ColorID = ' + cast(@ProductColorID as varchar) + ' or ' + cast(@ProductColorID as varchar) + ' = 0)'
     + ' and (P.ProductSizeID = ' + cast(@ProductSizeID as varchar) + ' or ' + cast(@ProductSizeID as varchar) + ' = 0)'     
  if @IsIncludeChild = 0
    set @SQLStr = @SQLStr + ' and (P.DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + ''' = '''')'  
  else  
    set @SQLStr = @SQLStr + ' and (P.DepartCode like ''' + @DepartCode + '%'' or ''' + @DepartCode + '''='''') '   
  if @ProdCodeStyle <> ''
	set @SQLStr = @SQLStr + ' and(P.Prodcode in (select Prodcode from Product_Style where ProdCodeStyle = ''' + @ProdCodeStyle + '''))'
	
  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '',''

  return 0
end

GO
