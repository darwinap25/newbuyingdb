USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGradeStoreCondition]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardGradeStoreCondition](
	[CardGradeStoreConditionID] [int] IDENTITY(1,1) NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[StoreConditionType] [int] NULL DEFAULT ((1)),
	[ConditionType] [int] NULL DEFAULT ((1)),
	[ConditionID] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARDGRADESTORECONDITION] PRIMARY KEY CLUSTERED 
(
	[CardGradeStoreConditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Trigger [dbo].[UpdateStoreCondition_CardGradeStoreCondition]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UpdateStoreCondition_CardGradeStoreCondition] ON [dbo].[CardGradeStoreCondition]
For INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: UpdateStoreCondition_CardGradeStoreCondition
* Version: 1.0.0.0
* Description :
* Create By Gavin @2012-07-23
*/
/*==============================================================*/
BEGIN
  exec UpdateCardGradeStoreCondition 0,0,0,0  
END

GO
/****** Object:  Trigger [dbo].[UpdateStoreCondition_DeleteCardGradeStoreCondition]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UpdateStoreCondition_DeleteCardGradeStoreCondition] ON [dbo].[CardGradeStoreCondition]
For DELETE
AS
/*==============================================================*/
/*                
* Name: UpdateStoreCondition_DeleteCardGradeStoreCondition
* Version: 1.0.0.0
* Description :
* Create By Gavin @2012-07-23
*/
/*==============================================================*/
BEGIN
  declare @CardGradeID int, @StoreConditionType int, @ConditionType int, @ConditionID int
  select @CardGradeID = CardGradeID, @StoreConditionType = StoreConditionType, @ConditionType = ConditionType, @ConditionID = ConditionID from deleted
  delete from CardGradeStoreCondition_List 
  where CardGradeID = @CardGradeID and StoreConditionType = @StoreConditionType 
    and ConditionType = @ConditionType and ConditionID = @ConditionID
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition', @level2type=N'COLUMN',@level2name=N'CardGradeStoreConditionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardGrade ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条件类型。 1：发布店铺。2：使用店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition', @level2type=N'COLUMN',@level2name=N'StoreConditionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'输入的条件类型。1：brand。2：Location。3：store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition', @level2type=N'COLUMN',@level2name=N'ConditionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条件具体的ID。 例如：ConditionType=1时，输入的为brandID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition', @level2type=N'COLUMN',@level2name=N'ConditionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别的店铺条件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition'
GO
