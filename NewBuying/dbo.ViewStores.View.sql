USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewStores]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewStores]
AS
  select StoreID, StoreCode, StoreName1, StoreName2, StoreName3, S.StoreTypeID, S.StoreGroupID, BankID, S.BrandID, StoreCountry, StoreProvince, StoreCity, StoreAddressDetail, StoreTel, StoreFax, StoreLongitude, StoreLatitude, S.LocationID, StoreNote, StoreOpenTime, StoreCloseTime, 
     case when isnull(MapsPicFile,'') = '' then B.StoreBrandPicSFile else MapsPicFile end as MapsPicFile, MapsPicShadowFile,     
     StoreTypeName1, StoreTypeName2, StoreTypeName3, StoreGroupName1, StoreGroupName2, StoreGroupName3, StoreBrandName1 as BrandName1, StoreBrandName2 as BrandName2, StoreBrandName3 as BrandName3, B.CardIssuerID, B.IndustryID,
     ParentLoactionID, LocationName1, LocationName2, LocationName3, LocationType, Lotitude, Longitude, S.Status, LocationFullPath, StorePicFile, StoreDistrict, StoreFullDetail,
     T.StoreTypeCode, StoreAddressDetail2, StoreFullDetail2, StoreAddressDetail3, StoreFullDetail3, Email, PickupStoreFlag, ReturnStoreFlag
   from store S left join StoreType T on S.StoreTypeID = T.StoreTypeID
     left join StoreGroup G on S.StoreGroupID = G.StoreGroupID
     left join Location L on S.LocationID = L.LocationID  
     left join Brand B on S.BrandID = B.StoreBrandID

GO
