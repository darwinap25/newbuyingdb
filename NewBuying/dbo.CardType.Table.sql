USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardType]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardType](
	[CardTypeID] [int] IDENTITY(1,1) NOT NULL,
	[CardTypeCode] [varchar](64) NOT NULL,
	[CardTypeName1] [nvarchar](512) NULL,
	[CardTypeName2] [nvarchar](512) NULL,
	[CardTypeName3] [nvarchar](512) NULL,
	[BrandID] [int] NOT NULL,
	[CardTypeNotes] [nvarchar](512) NULL,
	[CardNumMask] [nvarchar](512) NULL,
	[CardNumPattern] [nvarchar](512) NULL,
	[CardCheckdigit] [int] NULL DEFAULT ((0)),
	[CheckDigitModeID] [int] NULL,
	[CardNumberToUID] [int] NULL DEFAULT ((0)),
	[IsImportUIDNumber] [int] NULL DEFAULT ((0)),
	[IsConsecutiveUID] [int] NULL DEFAULT ((1)),
	[UIDCheckDigit] [int] NULL DEFAULT ((0)),
	[UIDToCardNumber] [int] NULL DEFAULT ((0)),
	[CardMustHasOwner] [int] NULL DEFAULT ((1)),
	[CardVerifyMethod] [int] NULL DEFAULT ((0)),
	[CardTypeStartDate] [datetime] NULL,
	[CardTypeEndDate] [datetime] NULL,
	[CardTypeNatureID] [int] NULL DEFAULT ((1)),
	[CurrencyID] [int] NULL,
	[Status] [int] NOT NULL DEFAULT ((1)),
	[CardExtendCode] [varchar](64) NULL,
	[CashExpiredate] [int] NULL DEFAULT ((1)),
	[PointExpiredate] [int] NULL DEFAULT ((1)),
	[PasswordRuleID] [int] NULL,
	[IsPhysicalCard] [int] NULL DEFAULT ((0)),
	[IsDumpCard] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [CardType_PK] PRIMARY KEY NONCLUSTERED 
(
	[CardTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡所属品牌' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeNotes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号码规则' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardNumMask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'按照卡号码规则的初始值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardNumPattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号是否包含校验位。默认0。 0：没有。1：有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardCheckdigit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验位产生逻辑表的外键（预留）。 CardCheckdigit设置为1时生效。 
CheckDigitMode 表 ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CheckDigitModeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建CardNumber时，是否同步产生UID。 默认：0。 
0：不产生UID。
1：原样复制CardNumber到 UID
2：复制CardNumber到 UID，并删除最后一位。    （不考虑CardNumber本身是否已经有了checkdigit）
3：复制CardNumber到 UID，并增加checkdigit，加在最后一位。 （不考虑CardNumber本身没有checkdigit的情况）
注： CD：checkdigit位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardNumberToUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardNumber号码是否需要导入。0：SVA系统根据CardNumMask设置来产生。1：号码导入，不是自动产生。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'IsImportUIDNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardUID号码是否为连续的。 0：不连续。1：连续，默认1   
（注：IsImportUIDNumber=1时有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'IsConsecutiveUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UID是否含有checkdigit。0：没有。1：有。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'UIDCheckDigit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据UID号码产生Cardnumber 控制。默认0： 不根据UID产生。
0：UID  binding  Cardnumber
1：原样复制UID到Cardnumber
2：复制UID到Cardnumber，并删除最后一位。    （不考虑UID本身是否已经有了checkdigit）
3：复制UID到Cardnumber，并增加checkdigit，加在最后一位。 （不考虑UID本身没有checkdigit的情况）
注： CD：checkdigit位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'UIDToCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'激活时是否必须要绑定member
0：不需要绑定。 1：需要绑定。 默认1。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardMustHasOwner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡验证方式。
NULL或0：不验证。 1：Visual Verify。 2：Online Verify。3：Visual Verify & Negative file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardVerifyMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeStartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeEndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型种类。外键ID
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardTypeNatureID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CurrencyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型状态。
0：无效。 1：有效。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'与其他系统关联的code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CardExtendCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金额是否有有效期。默认1
0：没有（只按照卡的有效期）。 1：有多有效期（每一笔充值金额都有独立的有效期）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'CashExpiredate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'积分是否有有效期。默认1
0：没有（只按照卡的有效期）。 1：有多有效期（每一笔充值金额都有独立的有效期）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'PointExpiredate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码规则表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'PasswordRuleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否实体卡。0：不是。1：是的。（ 如果是实体卡，可能需要写卡，使用读卡器） （虚拟卡也是可能需要UID的）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'IsPhysicalCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转储卡。比如电信类型卡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType', @level2type=N'COLUMN',@level2name=N'IsDumpCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型表。 
按照L2要求，有关卡号规则的设置字段，CardType和CardGrade中设置双份。字段包括（CardNumMask，CardNumPattern，CardCheckdigit，CheckDigitModeID，IsImportUIDNumber，IsConsecutiveUID，UIDToCardNumber）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardType'
GO
