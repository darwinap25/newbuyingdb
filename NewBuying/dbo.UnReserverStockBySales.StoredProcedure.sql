USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UnReserverStockBySales]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[UnReserverStockBySales]
  @UserID				         INT,	            -- 操作员ID
  @TransNum                      VARCHAR(64)        -- 单号码
AS
/****************************************************************************
**  Name : UnReserverStockBySales 
**  Version: 1.0.0.0
**  Description : 取消由Sales单产生的Reserver库存。 要求这个sales单的所有shipment单都还未批核。 
                  一旦取消这个销售单，则取消所有产生的picking单和shipping单，返还所有的Reserver的库存。
				  有任何一个货品出库的话，此单不能undo Reserver。 
**  Created by: Gavin @2016-12-14  
****************************************************************************/
BEGIN
  DECLARE @SalesPickOrderNumber VARCHAR(64), @PickupLocation varchar(64)

  IF NOT EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE TxnNo = @TransNum AND ISNULL(Status, 0) <> 0)  -- 销售的货品都没有出库. 有出库了就不能这么做.
  BEGIN
    INSERT INTO STK_StockMovement (OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  OpenQty,ActQty,CloseQty,SerialNoType, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
    SELECT 3,A.StoreID,A.StockTypeCode,A.ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
				  B.OnhandQty,-1 * A.ActQty, B.OnhandQty - A.ActQty, SerialNoType, SerialNo, ApprovalCode,GETDATE(),@UserID
	   FROM STK_StockMovement A 
	     LEFT JOIN STK_StockOnhand B ON A.StoreID = B.StoreID AND A.StockTypeCode = B.StockTypeCode AND A.ProdCode = B.ProdCode
	   WHERE ReferenceNo = @TransNum AND OprID = 1
    UPDATE SalesReserverDetail SET Qty = 0 WHERE TransNum = @TransNum
	UPDATE Ord_SalesShipOrder_H SET	Status = 4 WHERE TxnNo = @TransNum
	UPDATE Ord_SalesPickOrder_H SET	ApproveStatus = 'V' WHERE ReferenceNo = @TransNum

  END
  RETURN 0
END

GO
