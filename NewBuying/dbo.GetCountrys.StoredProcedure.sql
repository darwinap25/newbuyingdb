USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCountrys]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetCountrys]
  @CountryCode          varchar(64),       -- Code. 不填为全部
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=1 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1
AS
/****************************************************************************
**  Name : GetCountrys  
**  Version: 1.0.0.0
**  Description : 获得Country数据
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = GetCountrys 'CHN', 1, 30, @count output, @recordcount output, 'zh_CN'
  print @a  
  print @count
  print @recordcount  
**  Created by: Gavin @2013-01-22
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
      
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
          
  if @Language = 2
    set @SQLStr = ' select CountryName2 as CountryName, * from Country '
  else if @Language = 3
    set @SQLStr = ' select CountryName3 as CountryName, * from Country  '
  else
    set @SQLStr = ' select CountryName1 as CountryName, * from Country  '      
  set @SQLStr = @SQLStr + ' where 1=1 '   
  if isnull(@CountryCode, '') <> ''
    set @SQLStr = @SQLStr + ' and CountryCode = ''' + @CountryCode + '''' 
    
  exec SelectDataInBatchs @SQLStr, 'CountryCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', ''
  
  return 0
end

GO
