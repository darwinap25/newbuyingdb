USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_Promotion_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_Promotion_H](
	[PromotionCode] [varchar](64) NOT NULL,
	[PromotionDesc1] [varchar](512) NULL,
	[PromotionDesc2] [varchar](512) NULL,
	[PromotionDesc3] [varchar](512) NULL,
	[PromotionNote] [varchar](512) NULL,
	[StoreID] [int] NULL,
	[StoreGroupID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartTime] [datetime] NULL DEFAULT ('0'),
	[EndTime] [datetime] NULL DEFAULT ('0'),
	[DayFlagCode] [varchar](64) NULL,
	[WeekFlagCode] [varchar](64) NULL,
	[MonthFlagCode] [varchar](64) NULL,
	[LoyaltyFlag] [int] NULL DEFAULT ((0)),
	[RepeatFlag] [int] NULL DEFAULT ((0)),
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_PROMOTION_H] PRIMARY KEY CLUSTERED 
(
	[PromotionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'PromotionNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'适用的店铺ID。0/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。0/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'StoreGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效时间（例如 上午9:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'StartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销失效时间（例如 下午20:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'EndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一月中促销生效日 （Buy_DayFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'DayFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一周中促销生效日 （Buy_WeekFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'WeekFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效月 （Buy_MonthFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'MonthFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否仅会员促销。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'LoyaltyFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重复促销标志。0：不重复。1：重复。 默认0
重复促销表示，参与此促销的货品，计算时不会被扣除，下一个促销计算时，还会是此促销还是时的货品。 主要目的是用于全场促销和其他特定促销的共存。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'RepeatFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销头表。 （包含所有被动促销设置.  促销会根据具体的销售单，顾客情况，自动匹配合适的促销方案）
@2015-11-05： 增加RepeatFlag， 主要用于全场促销。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_H'
GO
