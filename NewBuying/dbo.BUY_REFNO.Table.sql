USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_REFNO]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_REFNO](
	[Code] [varchar](6) NOT NULL,
	[RefDesc] [varchar](512) NULL,
	[Header] [varchar](6) NOT NULL,
	[Seq] [int] NOT NULL,
	[Length] [int] NOT NULL,
	[Auto] [char](1) NOT NULL,
	[Active] [int] NULL,
 CONSTRAINT [PK_BUY_REFNO] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_REFNO] ADD  DEFAULT ('Y') FOR [Auto]
GO
ALTER TABLE [dbo].[BUY_REFNO] ADD  DEFAULT ((1)) FOR [Active]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO', @level2type=N'COLUMN',@level2name=N'RefDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单号头' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO', @level2type=N'COLUMN',@level2name=N'Header'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO', @level2type=N'COLUMN',@level2name=N'Seq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单号长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO', @level2type=N'COLUMN',@level2name=N'Length'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自动增长。N：不会。 Y：会。 默认Y' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO', @level2type=N'COLUMN',@level2name=N'Auto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否生效。0：不生效。1：生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO', @level2type=N'COLUMN',@level2name=N'Active'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单单号维护表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REFNO'
GO
