USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGradeStoreCondition_List]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardGradeStoreCondition_List](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[StoreConditionType] [int] NULL DEFAULT ((1)),
	[ConditionType] [int] NULL DEFAULT ((1)),
	[ConditionID] [int] NULL,
	[StoreID] [int] NOT NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARDGRADESTORECONDITION_LIS] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition_List', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardGrade ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition_List', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条件类型。 1：发布店铺。2：使用店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition_List', @level2type=N'COLUMN',@level2name=N'StoreConditionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'输入的条件类型。1：brand。2：Location。3：store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition_List', @level2type=N'COLUMN',@level2name=N'ConditionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条件具体的ID。 例如：ConditionType=1时，输入的为brandID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition_List', @level2type=N'COLUMN',@level2name=N'ConditionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition_List', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺条件子表（店铺列表）
根据店铺查询条件子表（CardGradeStoreCondition），解析到具体的storeID 列表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeStoreCondition_List'
GO
