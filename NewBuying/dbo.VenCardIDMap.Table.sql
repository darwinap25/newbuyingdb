USE [NewBuying]
GO
/****** Object:  Table [dbo].[VenCardIDMap]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VenCardIDMap](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[VendorCardNumber] [varchar](512) NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[CardNumberEncrypt] [varchar](512) NULL,
	[ValidateFlag] [int] NOT NULL DEFAULT ((1)),
	[LaserID] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_VENCARDIDMAP] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VenCardIDMap', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实体卡ID。（UID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VenCardIDMap', @level2type=N'COLUMN',@level2name=N'VendorCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VenCardIDMap', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号密文' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VenCardIDMap', @level2type=N'COLUMN',@level2name=N'CardNumberEncrypt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效标志，默认1。
0：无效。 1：生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VenCardIDMap', @level2type=N'COLUMN',@level2name=N'ValidateFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LaserID或者其他ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VenCardIDMap', @level2type=N'COLUMN',@level2name=N'LaserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实体卡绑定表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VenCardIDMap'
GO
