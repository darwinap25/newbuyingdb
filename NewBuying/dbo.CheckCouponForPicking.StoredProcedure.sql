USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckCouponForPicking]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[CheckCouponForPicking]
  @CouponOrderFormNumber  varchar(64)  
AS
/****************************************************************************
**  Name : CheckCouponForPicking
**  Version : 1.0.0.0
**  Description : 在产生Picking之前，检查是否有足够的Coupon提供给Picking单。
** 
declare @a int
exec @a = CheckCouponForPicking 
print @a
  select * from Ord_CouponOrderForm_H
**
**  Create by: Gavin @2014-07-02 
****************************************************************************/
begin
  declare @BrandID int, @CouponTypeID int, @CouponQty int, @StockQty int 
  declare @Return int

  set @StockQty = 0
  set @Return = 0
  DECLARE CUR_CheckCouponForPicking CURSOR fast_forward for
    select max(BrandID) as BrandID, CouponTypeID, sum(isnull(CouponQty,0))  
       from Ord_CouponOrderForm_D D Left join Ord_CouponOrderForm_H H on H.CouponOrderFormNumber = D.CouponOrderFormNumber 
      where D.CouponOrderFormNumber = @CouponOrderFormNumber
     group by CouponTypeID   
  OPEN CUR_CheckCouponForPicking
  FETCH FROM CUR_CheckCouponForPicking INTO @BrandID, @CouponTypeID, @CouponQty  
  WHILE @@FETCH_STATUS=0
  BEGIN  
    select @StockQty = isnull(Count(*), 0) from Coupon 
      where status = 0  and StockStatus = 2 and PickupFlag = 0 and CouponTypeID = @CouponTypeID
    
    if isnull(@StockQty, 0) < @CouponQty
    begin
      set @Return = -1
      break
    end 
     
    FETCH FROM CUR_CheckCouponForPicking INTO @BrandID, @CouponTypeID, @CouponQty  
  END
  CLOSE CUR_CheckCouponForPicking 
  DEALLOCATE CUR_CheckCouponForPicking 
  
  return @Return 
end

GO
