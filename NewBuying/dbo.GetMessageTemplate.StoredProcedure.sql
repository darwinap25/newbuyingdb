USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMessageTemplate]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMessageTemplate]
  @MessageTemplateID    int,  --主键ID
  @BrandID              int,  --品牌ID
  @OprID                int,  --操作类型ID
  @MessageServiceTypeID int,  --消息通道ID
  @LanguageAbbr			varchar(20)=''	
AS
/****************************************************************************
**  Name : GetMessageTemplate  
**  Version: 1.0.0.0
**  Description :   获取消息模板
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = GetMessageTemplate 0,0,0,0, 'zh_CN'
  print @a   
**  Created by: Gavin @2013-02-01
**
****************************************************************************/
begin
  declare  @Language int
   
  set @MessageTemplateID = isnull(@MessageTemplateID, 0)
  set @BrandID = isnull(@BrandID, 0)
  set @OprID = isnull(@OprID, 0)
  if  @OprID = 0
    set  @OprID = -1
  set @MessageServiceTypeID = isnull(@MessageServiceTypeID, 0)
 
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1

  select case @Language when 2 then MessageTitle2 when 3 then MessageTitle3 else MessageTitle1 end as MessageTitle,
      case @Language when 2 then TemplateContent2 when 3 then TemplateContent3 else TemplateContent1 end as TemplateContent,
      M.MessageTemplateID, M.MessageTemplateCode, M.MessageTemplateDesc, BrandID, OprID, Remark,
      D.MessageServiceTypeID, D.Status
  from MessageTemplateDetail D left join MessageTemplate M on D.MessageTemplateID = M.MessageTemplateID
  where (M.MessageTemplateID = @MessageTemplateID or @MessageTemplateID = 0)
        and (M.BrandID = @BrandID or @BrandID = 0)
        and (M.OprID = @OprID or @OprID = -1)
        and (D.MessageServiceTypeID = @MessageServiceTypeID or @MessageServiceTypeID = 0)
  
  return 0
end

GO
