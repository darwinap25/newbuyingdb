USE [NewBuying]
GO
/****** Object:  Table [dbo].[LogisticsPrice]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogisticsPrice](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[LogisticsProviderID] [int] NOT NULL,
	[CountryCode] [varchar](64) NULL,
	[ProvinceCode] [varchar](64) NULL,
	[StartPrice] [money] NULL,
	[StartWeight] [decimal](16, 6) NULL,
	[OverflowPricePerKG] [money] NULL,
	[FreeAmount] [money] NULL,
 CONSTRAINT [PK_LOGISTICSPRICE] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流供应商主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'LogisticsProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家编码主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'省编码主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'ProvinceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起步价。(基础运费) 示例： 6元起' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'StartPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起步重量， 单位公斤。0表示不计重量。  示例： 10 公斤  （10公斤以上收溢价）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'StartWeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每公斤溢出价格。 示例：  1.5元 每公斤。 0表示不计溢出重量价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'OverflowPricePerKG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'免运费的阈值。（总计消费金额大于设定值时，免运费）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice', @level2type=N'COLUMN',@level2name=N'FreeAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流价格设置表。 按省份，按供应商，设置报价
@2016-10-25  改动能适用bauhaus 的需求。 计算用存储过程：DoCalcFreight' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogisticsPrice'
GO
