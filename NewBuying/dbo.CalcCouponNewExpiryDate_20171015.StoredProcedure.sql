USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcCouponNewExpiryDate_20171015]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  PROCEDURE [dbo].[CalcCouponNewExpiryDate_20171015]  
  @CouponTypeID int,
  @OprID  int,
  @OldExpirydate datetime,      -- 当前有效期，如果是手动调节，则填入将要设置的日期
  @NewExpiryDate datetime out   -- 新有效期
AS
/****************************************************************************
**  Name : CalcCouponNewExpiryDate
**  Version: 1.0.0.3
**  Description : 计算coupon在此操作后，应该变更成的新有效期
**
declare @NewExpiryDate datetime
exec CalcCouponNewExpiryDate 4, 27, 0, @NewExpiryDate output
print @NewExpiryDate
**  Created by: Gavin @2012-09-07
**  Modify by: Gavin @2012-11-21 (ver 1.0.0.1) 增加OprID=53 的情况, 这是POS提交的激活请求,也要重新计算有效期
**  Modify by: Gavin @2013-01-08 (ver 1.0.0.2) 增加CouponValidityUnit=6的情况, 按照CouponSpecifyExpiryDate的设置来重置有效期.
**  Modify by: Gavin @2013-09-11 (ver 1.0.0.3) 增加OprID=27 的情况, 这是奖励Coupon绑定到用户
**  Modify by: Gavin @2014-10-22 (ver 1.0.0.4)
**
****************************************************************************/
begin
  declare @CouponValidityDuration int, @CouponValidityUnit int, @ActiveResetExpiryDate int, @CouponSpecifyExpiryDate datetime
  
  select @CouponValidityUnit = CouponValidityUnit, @CouponValidityDuration = CouponValidityDuration, 
       @ActiveResetExpiryDate = ActiveResetExpiryDate, @CouponSpecifyExpiryDate = CouponSpecifyExpiryDate
      from CouponType where CouponTypeID = @CouponTypeID
    
  set @NewExpiryDate = @OldExpirydate
  
  if @OprID in (27,31,32,33,53) and @ActiveResetExpiryDate = 1
  begin      
      -- 设置Coupon 新有效期
      if @CouponValidityUnit = 1
        set @NewExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, isnull(@CouponValidityDuration, 0), GetDate()))))
      else if @CouponValidityUnit = 2
        set @NewExpiryDate = convert(datetime, floor(convert(float, DateAdd(mm, isnull(@CouponValidityDuration, 0), GetDate()))))
      else if @CouponValidityUnit = 3
        set @NewExpiryDate = convert(datetime, floor(convert(float, DateAdd(ww, isnull(@CouponValidityDuration, 0), GetDate()))))
      else if @CouponValidityUnit = 4
        set @NewExpiryDate = convert(datetime, floor(convert(float, DateAdd(dd, isnull(@CouponValidityDuration, 0), GetDate()))))
      else if @CouponValidityUnit = 6
      begin
        if isnull(@CouponSpecifyExpiryDate, 0) = 0
          set @CouponSpecifyExpiryDate = getdate()
        set @NewExpiryDate = convert(datetime, floor(convert(float, @CouponSpecifyExpiryDate)))        
      end else if isnull(@CouponValidityUnit, 0) = 0  
        set @NewExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, 100, GetDate()))))
  end
end


GO
