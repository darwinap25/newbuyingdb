USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenSalesPickupOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[GenSalesPickupOrder]
  @UserID				         INT,	        -- 操作员ID
  @TxnNo                         VARCHAR(64),   -- 交易号码
  @IsReserve                     INT            -- 0: 不是reserve库存,直接挂机销售单产生拣货单. 1: 是有预留库存. 需要对照SalesReserverDetail表.
AS
/****************************************************************************
**  Name : GenSalesPickupOrder (bauhaus)
**  Version: 1.0.0.6
**  Description : 必须状态是4的交易单(PS). 单子中所有需要扣库存的货品.  Collected状态目前不太确定,暂定只要是 2和4的都产生pick单
select * from Ord_SalesPickOrder_H
select * from Ord_SalesPickOrder_D
update Ord_SalesPickOrder_D set actualqty = 4
select * from sales_H where status = 4
exec GenSalesPickupOrder 1,'KTXNNO000001028' 
sp_helptext GenSalesPickupOrder
**  Created by: Gavin @2016-05-24 
**  Modified by Gavin @2016-10-18 （1.0.0.1）自动选择pickuplocation. (hardcode:默认选择sales_H的storecode（ECO），如果没有库存，则选择WH。（WH也没有，则不管了）) 
                                             即自动产生的Picking单,最多可能从两个地方提货: 设置的storecode或者WH。
**  Modified by Gavin @2016-10-18 （1.0.0.2) INSERT INTO Ord_SalesPickOrder_H的 DeliveryFlag 时,使用sales_H 的 PickupType
**  Modified by Gavin @2016-12-29 （1.0.0.3) 可能在CopyAfterSalesToBUY中调用, 此时调用,除了exchange换新货,还有可能是退货的. (退货数量小于0). 此时直接处理库存.不用创建SalesPickOrder单
                                             IsReserver = 0 时， 需要补上 -G+R 的movement记录
**  Modified by Gavin @2017-03-16 （1.0.0.4) 根据要求,默认ActualQty 填上和 OrderQty相同的数量,而不是0
**  Modified by Gavin @2017-03-22 （1.0.0.5) 增加@NOWHStoreCode
**  Modified by Gavin @2017-03-23 （1.0.0.6) exchange的新单产生pick时,不使用ReserveSalesStock_ByPickOrder,改用Reserve_ByExchangePick
 注: @2017-03-24,  @IsReserve=0 的情况不会出现. 这一段不会再使用了.  return 和 exchange 也同样调用ReserveSalesStock,再调用此过程.
**
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @TxnDate DATETIME, @StoreCode varchar(64),
          @PickupStoreCode VARCHAR(64), @StoreID INT
  DECLARE @OriginalTxnNo VARCHAR(64), @AfterTxnNo VARCHAR(64)
  DECLARE @NOWHStoreCode VARCHAR(64)

  SELECT @BusDate = BusDate, @TxnDate = TxnDate, @StoreCode = StoreCode FROM Sales_H WHERE TransNum = @TxnNo AND Status = 4
  IF  @@ROWCOUNT = 0
  BEGIN 
    RETURN 0
  END ELSE
  BEGIN
    IF @IsReserve = 1 
	BEGIN
	  IF EXISTS(SELECT * FROM SalesReserverDetail WHERE StoreCode = 'WH' AND TransNum = @TxnNo AND Qty > 0)
	  BEGIN
		  EXEC GetRefNoString 'SPUO', @NewNumber OUTPUT
		  INSERT INTO Ord_SalesPickOrder_D(SalesPickOrderNumber,ProdCode,OrderQty,ActualQty,StockTypeCode,Remark)
		  SELECT @NewNumber, ProdCode, Qty, Qty, 'G', '' FROM SalesReserverDetail  
		  WHERE StoreCode = 'WH' AND TransNum = @TxnNo AND Qty > 0
		  IF @@ROWCOUNT <> 0
		  BEGIN
			  INSERT INTO Ord_SalesPickOrder_H(SalesPickOrderNumber,OrderType,MemberID,CardNumber,ReferenceNo,PickupLocation,PickupStaff,PickupDate,
				  DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
				  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,CreatedBusDate,ApproveBusDate,
				  ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
			  SELECT @NewNumber, PickupType, MemberID,CardNumber,TransNum,'WH',@UserID,GETDATE(),
				  PickupType,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
				  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,@BusDate,null,
				  '','P',null,@UserID,GETDATE(),@UserID,GETDATE(),@UserID 
			  FROM Sales_H WHERE TransNum = @TxnNo
		  END
      END
	  IF EXISTS(SELECT * FROM SalesReserverDetail WHERE StoreCode <> 'WH' AND TransNum = @TxnNo AND Qty > 0)
	  BEGIN
	      SELECT TOP 1 @NOWHStoreCode = StoreCode FROM SalesReserverDetail WHERE StoreCode <> 'WH' AND TransNum = @TxnNo AND Qty > 0
		  EXEC GetRefNoString 'SPUO', @NewNumber OUTPUT
		  INSERT INTO Ord_SalesPickOrder_D(SalesPickOrderNumber,ProdCode,OrderQty,ActualQty,StockTypeCode,Remark)
		  SELECT @NewNumber, ProdCode, Qty, Qty, 'G', '' FROM SalesReserverDetail  
		  WHERE StoreCode <> 'WH' AND TransNum = @TxnNo AND Qty > 0
		  IF @@ROWCOUNT <> 0
		  BEGIN
			  INSERT INTO Ord_SalesPickOrder_H(SalesPickOrderNumber,OrderType,MemberID,CardNumber,ReferenceNo,PickupLocation,PickupStaff,PickupDate,
				  DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
				  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,CreatedBusDate,ApproveBusDate,
				  ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
			  SELECT @NewNumber, PickupType, MemberID,CardNumber,TransNum,@NOWHStoreCode,@UserID,GETDATE(),
				  PickupType,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
				  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,@BusDate,null,
				  '','P',null,@UserID,GETDATE(),@UserID,GETDATE(),@UserID 
			  FROM Sales_H WHERE TransNum = @TxnNo
		  END
	  END
	  UPDATE SALES_D SET Collected = 4 WHERE TransNum = @TxnNo AND Collected IN (0,1)
	  UPDATE SalesReserverDetail SET Qty = 0 WHERE TransNum = @TxnNo 
    END ELSE
	BEGIN
	  -- @2017-03-24,  @IsReserve=0 的情况不会出现. 这一段不会再使用了.  return 和 exchange 也同样调用ReserveSalesStock,再调用此过程.
	  IF EXISTS(SELECT * FROM Sales_D WHERE TransNum = @TxnNo AND Collected IN (0,1) AND TotalQty > 0)
	  BEGIN
		  SET @PickupStoreCode = 'ECO'  
		  EXEC GetRefNoString 'SPUO', @NewNumber OUTPUT

		  INSERT INTO Ord_SalesPickOrder_D(SalesPickOrderNumber,ProdCode,OrderQty,ActualQty,StockTypeCode,Remark)
		  SELECT @NewNumber, ProdCode, TotalQty, TotalQty, 'G', '' FROM Sales_D  
		  WHERE TransNum = @TxnNo AND Collected IN (0,1) 
		  IF @@ROWCOUNT <> 0
		  BEGIN
			  INSERT INTO Ord_SalesPickOrder_H(SalesPickOrderNumber,OrderType,MemberID,CardNumber,ReferenceNo,PickupLocation,PickupStaff,PickupDate,
				  DeliveryFlag,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
				  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,CreatedBusDate,ApproveBusDate,
				  ApprovalCode,ApproveStatus,ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
			  SELECT @NewNumber, PickupType, MemberID,CardNumber,TransNum,@PickupStoreCode,@UserID,GETDATE(),
				  PickupType,DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
				  LogisticsProviderID,Contact,ContactPhone,RequestDeliveryDate,DeliveryDate,DeliveryBy,Remark,@BusDate,null,
				  '','P',null,@UserID,GETDATE(),@UserID,GETDATE(),@UserID 
			  FROM Sales_H WHERE TransNum = @TxnNo
		  END

		  -- IsReserver = 0 时， 需要补上 -G+R 的movement记录
		  SELECT @StoreID = StoreID FROM buy_store WHERE storecode = @PickupStoreCode
		  --exec ReserveSalesStock_ByPickOrder 1,@NewNumber, @TxnNo, @StoreID
		  exec Reserve_ByExchangePick 1,@NewNumber, @TxnNo, @StoreID
		  
		  UPDATE SALES_D SET Collected = 4 WHERE TransNum = @TxnNo AND Collected IN (0,1)
	  END
	END
  END

  RETURN 0
END

GO
