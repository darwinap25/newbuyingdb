USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ChangePromotionHitPLUList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[ChangePromotionHitPLUList]
  @PromotionCode     varchar(64),
  @HitSeq            int
AS
/*
Pormotion_Hit_PLU 内的货品条件都作为过滤条件。
*/
begin
  declare @HitPCode varchar(64), @Seq int, @EntityNum varchar(64), @EntityType int, @HitSign int
  set @PromotionCode = ISNULL(@PromotionCode, '')
  set @HitSeq = ISNULL(@HitSeq, 0)
  
  delete from PromotionHitPLUList 
    where (PromotionCode = @PromotionCode or @PromotionCode = '') and (HitSeq = @HitSeq or @HitSeq = 0)
  
    DECLARE CUR_PromotionHit CURSOR fast_forward FOR
      SELECT A.PromotionCode, A.HitSeq, A.EntityNum, A.EntityType, A.HitSign FROM Promotion_Hit_PLU A
         left join Promotion_H B on A.PromotionCode = B.PromotionCode
        where (A.PromotionCode = @PromotionCode or @PromotionCode = '') and (HitSeq = @HitSeq or @HitSeq = 0)
          and EntityType in (0, 1, 2) and PLUHitType = 2 and B.ApproveStatus = 'A'
       order by A.PromotionCode, HitSeq, HitPLUSeq, KeyID 
    OPEN CUR_PromotionHit
    FETCH FROM CUR_PromotionHit INTO @HitPCode, @Seq, @EntityNum, @EntityType, @HitSign
    WHILE @@FETCH_STATUS=0
    BEGIN
      if @HitSign = 1 
      begin 
        if @EntityType = 0
        begin
          --delete from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq 
          insert into PromotionHitPLUList (PromotionCode, HitSeq, ProdCode, DepartCode)
          select @HitPCode, @Seq, Prodcode, DepartCode from Product 
            where ProdCode not in (select ProdCode from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq)
        end 
        else if @EntityType = 1    
        begin
          insert into PromotionHitPLUList (PromotionCode, HitSeq, ProdCode, DepartCode)
          select @HitPCode, @Seq, Prodcode, DepartCode from Product 
            where ProdCode = @EntityNum and ProdCode not in (select ProdCode from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq)
        end 
        else if @EntityType = 2
        begin
          insert into PromotionHitPLUList (PromotionCode, HitSeq, ProdCode, DepartCode)
          select @HitPCode, @Seq, Prodcode, DepartCode from Product 
            where DepartCode like (RTrim(@EntityNum) + '%') and ProdCode not in (select ProdCode from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq)
        end  
      end else if @HitSign = 2
      begin
        if @EntityType = 0
        begin
          --delete from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq  
          insert into PromotionHitPLUList (PromotionCode, HitSeq, ProdCode, DepartCode)
          select @HitPCode, @Seq, Prodcode, DepartCode from Product 
            where ProdCode not in (select ProdCode from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq)              
        end 
        else if @EntityType = 1    
        begin      
          delete from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq and ProdCode = @EntityNum
        end 
        else if @EntityType = 2
        begin
          delete from PromotionHitPLUList where PromotionCode = @HitPCode and HitSeq = @Seq and DepartCode like (RTrim(@EntityNum) + '%')
        end        
      end
      FETCH FROM CUR_PromotionHit INTO @HitPCode, @Seq, @EntityNum, @EntityType, @HitSign
    END
    CLOSE CUR_PromotionHit 
    DEALLOCATE CUR_PromotionHit    
end

GO
