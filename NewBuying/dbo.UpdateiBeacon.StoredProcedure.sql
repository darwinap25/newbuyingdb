USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateiBeacon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateiBeacon]
  @iBeaconID            int,
  @CardNumber           varchar(64),  
  @TokenID              varchar(64),
  @AssociationType      int,
  @AssociationID        varchar(64),
  @Battery              decimal(12,2)
AS
/****************************************************************************
**  Name : UpdateiBeacon
**  Version: 1.0.0.1
**  Description : 由iBeacon触发提交的请求。 更新iBeacon的电量. 新增iBeacon_Movement记录
   declare @A int
   exec @A=UpdateiBeacon '1201-6', '01000045', '', '1','',''
   print @A
select * from ibeacon
**  Created by: Gavin @2015-08-24
**  Modified by :Ray @2015-10-05 (ver 1.0.0.1)  1. 修正当iBeaconID 是NULL, insert iBeacon_movement出错 2. 当更新电量时，加上更新UpdatedOn
**
****************************************************************************/
begin
  if isNUll (@iBeaconID,'') = ''
    return 0 

  insert into iBeacon_Movement(iBeaconID, CardNumber, TokenID, AssociationType, AssociationID, Battery)
  values (@iBeaconID, @CardNumber, @TokenID, @AssociationType, @AssociationID, @Battery)
 
-- Ver 1.0.0.1 ?更新?量時, 會更新UpdatedOn ************
  update iBeacon set Battery = @Battery, UpdatedOn = GetDate() where iBeaconID = @iBeaconID  
  
end

GO
