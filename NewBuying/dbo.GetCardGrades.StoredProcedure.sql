USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCardGrades]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetCardGrades]
  @CardTypeID			int,       -- 
  @ConditionStr			nvarchar(1000),     -- 查询条件  
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr			varchar(20)=''	   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1
AS
/****************************************************************************
**  Name : GetCardGrades  
**  Version: 1.0.0.1
**  Description : 获得所有CardGrade数据, 不加过滤限制
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetCardGrades null, null, 1, 30, @count output, @recordcount output, 1
  print @a  
  print @count
  print @recordcount

**  Created by: Gavin @2013-10-16
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @CardTypeID = isnull(@CardTypeID, 0)     
  set @SQLStr = ' select case ' + cast(@Language as varchar) + ' when 2 then CardGradeName2 when 3 then CardGradeName3 else CardGradeName1 end as CardGradeName, '
    + ' CardGradeID, CardGradeCode, G.CardTypeID, CardGradeName1, CardGradeName2, CardGradeName3, CardGradeRank, '
    + ' IsAllowStoreValue, CardGradeLayoutFile, CardGradePicFile, CardGradeNotes, CardGradeStatementFile, T.CardTypeCode '
    + ' from cardgrade G left join CardType T on T.CardTypeID = G.CardTypeID '    
    + ' where G.CardTypeID = ' + cast(@CardTypeID as varchar) + ' or ' + cast(@CardTypeID as varchar) + ' = 0 ' 

  exec SelectDataInBatchs @SQLStr, 'CardGradeID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr
  
  return 0
end

GO
