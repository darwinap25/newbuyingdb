USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[EncryptRSAKey]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EncryptRSAKey]
   @ClearCode  varchar(1024),
   @SecurityCode varbinary(1024) output
AS
/****************************************************************************
**  Name : EncryptRSAKey
**  Version: 1.0.0.0
declare @A varchar(512), @B varbinary(512), @C int
set @A = '123456789'
set @C = 123456
exec EncryptRSAKey @C, @B output
print @B
select * from rsakeys
delete from RSAKeys
**  Description : 加密table中的RSA key
**  Created by: Gavin @2012-08-21
**
****************************************************************************/
begin
  --OPEN SYMMETRIC KEY RSADESKey DECRYPTION BY PASSWORD='abc@123' 
  OPEN SYMMETRIC KEY RSADESKey DECRYPTION BY PASSWORD='rrg1$th3b3$t'
  set @SecurityCode = ENCRYPTBYKEY(KEY_GUID('RSADESKey'),@ClearCode)
--  insert into RSAKeys
--  values(cast(@ClearCode as varbinary), @SecurityCode, getdate(), getdate() + 100)
--  return 0
end

GO
