USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DecryptString]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DecryptString] 
   @SecurityCode varchar(4000),
   @ClearText varchar(4000) output  
AS
/****************************************************************************
**  Name : DecryptString
**  Version: 1.0.0.0
**  Description : 解密字符串

declare @a int, @out varchar(4000)
exec @a = DecryptString '4D656D62657249443D392C4D656D6265724163636F756E74547970653D322C4D656D6265724163636F756E743D6162634073696E612E636F6D', @out output
print @a
print @out
				
**  Created by: Gavin @2013-08-20
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000)

  set @SQLStr='select @n= cast(0x' + @SecurityCode + ' as varchar(4000))'
  exec sp_executesql @SQLStr, N'@n varchar(4000) output',@ClearText output 
  return 0 
end

GO
