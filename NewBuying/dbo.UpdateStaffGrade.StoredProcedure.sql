USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateStaffGrade]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[UpdateStaffGrade]
  @MemberID  int
AS
/****************************************************************************
**  Name : UpdateStaffGrade
**  Version: 1.0.0.1 
**  Description : 根据StaffImport 和 Member表数据，决定是否升级member card。 （hardcode 由 1 升级到 5）
   declare @A int
   exec @A=UpdateStaffGrade 1
   print @A

**  Created by: Gavin @2017-06-26
**  Modify by: Gavin @2017-07-03 (ver 1.0.0.1) ImportStaff 中的字段为空时，不作为比较字段。
**
****************************************************************************/
begin
  DECLARE @CardGradeID int, @CardGradeUpdMethod int, @CardGradeUpdThreshold int, @CardGradeRank int
  DECLARE @CardValidityDuration int, @CardValidityUnit int, @NewCardExpiryDate datetime
  DECLARE @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate date, @NewCouponExpiryDate date, @CouponAmount money, 
          @ApprovalCode varchar(6), @i int, @BusDate datetime, @TxnDate datetime, @CouponNumber varchar(64), 
		  @BindCouponCount int,@BindCouponTypeID int , @CardNumber varchar(64)
  DECLARE @IsStaff INT
  
  set @MemberID = ISNULL(@MemberID, 0)
  if @MemberID = 0
    return -1
 
  SET @IsStaff = 0

  select @CardGradeID = CardGradeID, @CardGradeUpdMethod = CardGradeUpdMethod, 
         @CardGradeUpdThreshold = CardGradeUpdThreshold, @CardGradeRank = CardGradeRank,
         @CardValidityDuration = CardValidityDuration, @CardValidityUnit = CardValidityUnit 
       from cardGrade where CardGradeID = 5

  -- 取busdate
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 
  if isnull(@TxnDate, 0) = 0
    set @TxnDate = getdate()          
  
  -- 如果会员不存在staffimport中，那么直接退出。
  if not exists ( select * from Member A left join ImportStaff B 
	                on (A.MemberMobilePhone = B.MobilePhone and A.CountryCode = B.CountryCode and isnull(B.MobilePhone,'') <> '' and isnull(B.CountryCode,'') <> '') 
					  or (A.MemberEmail = B.EmailAddr and isnull(B.EmailAddr,'') <> '') or (A.MemberEmail = B.EmailAddr2 and isnull(B.EmailAddr2,'') <> '')
                   where B.StaffNo is not null and (A.MemberID = @MemberID ) ) 
	return 0
	
          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR
            select A.CardNumber, B.CouponTypeID, B.HoldCount from
            (
              select 5 as NextCardGradeID, * from Card where MemberID = @MemberID and CardGradeID = 1
            ) A left join (select * from CardGradeHoldCouponRule where RuleType = 1 and CardGradeID = 5) B on A.NextCardGradeID = B.CardGradeID         
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN

            set @CouponNumber = ''
            set @i = 1
            while @i <= @BindCouponCount
            begin
              exec GetCouponNumber @BindCouponTypeID, 1, @CouponNumber output            
              if isnull(@CouponNumber, '') <> ''
              begin
                select @CouponExpiryDate = CouponExpiryDate, @CouponAmount = CouponAmount from Coupon where CouponNumber = @CouponNumber
                
                exec GenApprovalCode @ApprovalCode output
                exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                values(  
                      32, @CardNumber, @CouponNumber, @BindCouponTypeID, '', 0, '', @CouponAmount, 0, @CouponAmount,
                      @BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2)
              end
              set @i = @i + 1              
            end                         
            FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
          /*=====================================================*/  

   		if @CardValidityUnit = 1
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 2
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(mm, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 3
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(ww, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 4
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(dd, isnull(@CardValidityDuration, 0), Getdate()))))  
		else if @CardValidityUnit = 5    -- 不变更。
		  set @NewCardExpiryDate = 0
		else if isnull(@CardValidityUnit, 0) = 0  
        set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, 100, Getdate()))))  
          
      INSERT INTO CARD_MOVEMENT
        (OPRID, CARDNUMBER, REFKEYID, REFRECEIVEKEYID, REFTXNNO, OPENBAL, AMOUNT, CLOSEBAL, POINTS, BUSDATE, TXNDATE, 
         CARDCASHDETAILID, CARDPOINTDETAILID, ADDITIONAL, REMARK, SECURITYCODE, CREATEDBY, STOREID, REGISTERCODE, SERVERCODE,
	     OPENPOINT, CLOSEPOINT, ORGEXPIRYDATE, NEWEXPIRYDATE, ORGSTATUS, NEWSTATUS)
      select 82, C.CardNumber, NULL, 0, '', C.TotalAmount, 0, C.TotalAmount, 0, Getdate(), Getdate(), 
         NULL, NULL, C.MemberID, NULL, '', C.MemberID, NULL, NULL, NULL,
          C.TotalPoints, C.TotalPoints,  C.CardExpiryDate, C.CardExpiryDate, C.Status, C.Status
	   from Card C 
	    left join Member M on C.MemberID = M.MemberID
	   where M.MemberID = @MemberID

end


GO
