USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetNations]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetNations]
  @NationID             int,       -- 主键, 为null或0, 则返回全部
  @LanguageAbbr         varchar(20)=''
AS
/****************************************************************************
**  Name : GetNations
**  Version: 1.0.0.1
**  Description :查找Nation数据
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetNations 0
  print @a  
**  Created by: Gavin @2012-05-23
**  Modify by: Gavin @2013-05-08 (ver 1.0.0.1) 增加返回字段: NationFlagFile
**
****************************************************************************/
begin
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select NationID, NationCode, CountryCode, 
     case when @Language = 2 then NationName2 when @Language = 3 then NationName3 else NationName1 end as NationName, NationFlagFile 
   from Nation where NationID = @NationID or isnull(@NationID, 0) = 0
  return 0  
end

GO
