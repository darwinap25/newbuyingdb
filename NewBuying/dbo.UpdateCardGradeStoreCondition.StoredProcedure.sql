USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCardGradeStoreCondition]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateCardGradeStoreCondition]
  @CardGradeID			int,			-- 指定CardGradeID. 
  @BrandID				int,			-- 条件中brand变动，或者brand 内容变动
  @LocationID			int,			-- 条件中location 变动，或者location 内容变动
  @StoreID				int				-- 条件中location 变动
AS
/****************************************************************************
**  Name : UpdateCardGradeStoreCondition 
**  Version: 1.0.0.1
**  Description :  CardGrade的店铺条件。把条件（brandid，locationid 转换到storeid list）
**
  declare @MemberID int, @A int
  exec @A = UpdateCardGradeStoreCondition 0,0,0,0
  print @A

**  Created by: Gavin @2012-05-28
**  Modify By Gavin @2012-10-31 (ver 1.0.0.1) 修正Bug，删除List记录时，必须加上StoreConditionType条件
**
****************************************************************************/
begin
  declare @LocationFullPath varchar(512), @SQLStr nvarchar(4000)
  declare @CardGradeStoreConditionID int, @StoreConditionType int, @ConditionType int, @ConditionID int, @CreatedBy int, @UpdatedBy int

  if isnull(@CardGradeID, 0) = 0    -- 未指定CardGradeID，表示修改的是brand或者location。 需要同步CardGradeStoreCondition_List中所有使用到的记录。
  begin        
    DECLARE CUR_AllCardGradeStoreCondition CURSOR fast_forward local FOR
      select CardGradeID, CardGradeStoreConditionID, StoreConditionType, ConditionType, ConditionID, CreatedBy, UpdatedBy from CardGradeStoreCondition        
    OPEN CUR_AllCardGradeStoreCondition
    FETCH FROM CUR_AllCardGradeStoreCondition INTO @CardGradeID, @CardGradeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    WHILE @@FETCH_STATUS=0
    BEGIN
      if @ConditionType = 1
      begin
        delete from CardGradeStoreCondition_List where ConditionType = @ConditionType and ConditionID = @ConditionID and CardGradeID = @CardGradeID  and StoreConditionType = @StoreConditionType       
        insert into CardGradeStoreCondition_List 
          (CardGradeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)        
        select @CardGradeID, @StoreConditionType, @ConditionType, @ConditionID, StoreID, getdate(), @CreatedBy, getdate(), @UpdatedBy
        from Store where BrandID = @ConditionID         
      end
      if @ConditionType = 2
      begin
        delete from CardGradeStoreCondition_List where ConditionType = @ConditionType and ConditionID = @ConditionID and CardGradeID = @CardGradeID and StoreConditionType = @StoreConditionType    
        select @LocationFullPath = LocationFullPath from Location where LocationID = @ConditionID        
        set @SQLStr = ' insert into CardGradeStoreCondition_List (CardGradeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy) '
		  + ' select @CardGradeID, @StoreConditionType, @ConditionType, @ConditionID, StoreID, getdate(), @CreatedBy, getdate(), @UpdatedBy '          
          + ' from Store where LocationID in (select LocationID from Location where LocationID in (' + @LocationFullPath + '))'
        exec sp_executesql @SQLStr  
      end 
      if @ConditionType = 3
      begin
        delete from CardGradeStoreCondition_List where ConditionType = @ConditionType and ConditionID = @ConditionID and CardGradeID = @CardGradeID and StoreConditionType = @StoreConditionType       
        insert into CardGradeStoreCondition_List 
          (CardGradeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)        
        values (@CardGradeID, @StoreConditionType, @ConditionType, @ConditionID, @ConditionID, getdate(), @CreatedBy, getdate(), @UpdatedBy)   
      end       
      FETCH FROM CUR_AllCardGradeStoreCondition INTO @CardGradeID, @CardGradeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    END
    CLOSE CUR_AllCardGradeStoreCondition 
    DEALLOCATE CUR_AllCardGradeStoreCondition     
  end else                           -- 指定CouponTypeID，则清空CouponTypeStoreCondition_List中此CouponTypeID的记录，重新产生。 
  begin
    delete from CardGradeStoreCondition_List where CardGradeID = @CardGradeID
    DECLARE CUR_CardGradeStoreCondition CURSOR fast_forward local FOR
      select CardGradeStoreConditionID, StoreConditionType, ConditionType, ConditionID, CreatedBy, UpdatedBy from CardGradeStoreCondition
        where CardGradeID = @CardGradeID
    OPEN CUR_CardGradeStoreCondition
    FETCH FROM CUR_CardGradeStoreCondition INTO @CardGradeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    WHILE @@FETCH_STATUS=0
    BEGIN
      if @ConditionType = 1
      begin     
		insert into CardGradeStoreCondition_List (CardGradeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
        select @CardGradeID, @StoreConditionType, @ConditionType, @ConditionID, StoreID, getdate(), @CreatedBy, getdate(), @UpdatedBy
        from Store where BrandID = @ConditionID
      end
      if @ConditionType = 2
      begin
        select @LocationFullPath = LocationFullPath from Location where LocationID = @ConditionID        
        set @SQLStr = ' insert into CardGradeStoreCondition_List (CardGradeID, StoreConditionType, ConditionType, ConditionID, StoreID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy) '
		  + ' select @CardGradeID, @StoreConditionType, @ConditionType, @ConditionID, StoreID, getdate(), @CreatedBy, getdate(), @UpdatedBy '
          + ' from Store where LocationID in (select LocationID from Location where LocationID in (' + @LocationFullPath + '))'
        exec sp_executesql @SQLStr  
      end  
      if @ConditionType = 3  
        insert into CardGradeStoreCondition_List
          (StoreID, CardGradeID, StoreConditionType, ConditionType, ConditionID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)    
        values
          (@ConditionID, @CardGradeID, @StoreConditionType, @ConditionType, @ConditionID, getdate(), @CreatedBy, getdate(), @UpdatedBy)  
      FETCH FROM CUR_CardGradeStoreCondition INTO @CardGradeStoreConditionID, @StoreConditionType, @ConditionType, @ConditionID, @CreatedBy, @UpdatedBy
    END
    CLOSE CUR_CardGradeStoreCondition 
    DEALLOCATE CUR_CardGradeStoreCondition              
  end
  return 0
end

GO
