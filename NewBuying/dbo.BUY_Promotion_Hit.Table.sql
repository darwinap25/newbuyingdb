USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_Promotion_Hit]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_Promotion_Hit](
	[PromotionCode] [varchar](64) NOT NULL,
	[HitSeq] [int] NOT NULL,
	[HitLogicalOpr] [int] NULL DEFAULT ((0)),
	[HitType] [int] NULL DEFAULT ((0)),
	[HitValue] [int] NULL,
	[HitOP] [int] NULL DEFAULT ((1)),
	[HitItem] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_BUY_PROMOTION_HIT] PRIMARY KEY CLUSTERED 
(
	[PromotionCode] ASC,
	[HitSeq] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hit条件序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit', @level2type=N'COLUMN',@level2name=N'HitSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'hit条件之间的逻辑关系。 默认0
0：and   1：or' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit', @level2type=N'COLUMN',@level2name=N'HitLogicalOpr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中类型：
0：无条件命中
1：数量。 
2：金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit', @level2type=N'COLUMN',@level2name=N'HitType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据HitType，填写金额或者数量。  只支持整数。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit', @level2type=N'COLUMN',@level2name=N'HitValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中关系操作符。默认1
0：没有操作符
1： =     （等于时， 如果金额大于此值，也符合条件。 扣减等于的数量，余下的继续参与计算）
2： <>
3： <=
4：>=  （大于等于时，只要数量大于此值，即符合条件，此时所有数量都扣除）
5：<
6：>' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit', @level2type=N'COLUMN',@level2name=N'HitOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中货品条件。默认0
0：没有具体的货品条件，全场货品都参与。
1：Promotion_Hit_PLU中的任意货品合计。
2：Promotion_Hit_PLU中任意一个单独货品满足数量或者金额
3：Promotion_Hit_PLU中每一个货品都需要满足数量或者金额
4：Promotion_Hit_PLU中为支付类型条件。
5:   Promotion_Hit_PLU中 货品都不包含在内。 （即除了这些货品，都可以。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit', @level2type=N'COLUMN',@level2name=N'HitItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promotion_H的子表。 促销命中条件表
（多个货品固定搭配的情况，可以用多条记录，之间用and关系关联（HitLogicalOpr=0））
@2016-11-11 ： hititem多增加一种情况（目前promotion engine还未实现）
5:   Promotion_Hit_PLU中 货品都不包含在内。 （即除了这些货品，都可以。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Hit'
GO
