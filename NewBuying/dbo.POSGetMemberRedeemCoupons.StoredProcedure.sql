USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSGetMemberRedeemCoupons]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[POSGetMemberRedeemCoupons]
  @MemberID				int,       -- 会员ID
  @CardNumber			varchar(512),      -- 如填写CardNumber，忽略MemberID
  @Brandcode  varchar(512),  -- 品牌编号   
  @StoreCode            varchar(512),  -- 店铺编号  
  @RegisterCode         varchar(512),           -- 终端号码  
  @ServerCode           varchar(512)='',        -- serverid     
  @ItemList    varchar(max)   -- 销售清单   	  
AS
/****************************************************************************
**  Name : POSGetMemberRedeemCoupons  POS获得登录会员可以使用的Coupon。
**  Version: 1.0.0.0
**  Description : POS获得登录会员可以使用的Coupon。
**  Parameter :
**
select * from member where memberid = 170
select * from card where memberid = 170
select * from CouponTypeStoreCondition_List
select * from CouponTypeExchangeBinding
select * from coupon where couponnumber like '12345000295%'

declare @a int
exec @a = POSGetMemberRedeemCoupons 0, '000100086', '123123', '1', '1', '1', N'<ROOT><ITEM PRODCODE= 400006  DEPTCODE=   QTY= 1  NETPRICE= 10.00  /></ROOT>'
print @a

**  Created by: Gavin @2013-01-17
**
****************************************************************************/
begin  
  Declare @T Table(SEQ int identity, PRODCODE varchar(512),DEPTCODE varchar(512),QTY int ,NETPRICE money)
  Declare @T1 Table(PRODCODE varchar(512),DEPTCODE varchar(512),QTY int ,TOTALAMT money)
  Declare @MyCoupon Table(CouponNumber varchar(64), CouponTypeID int)  
  Declare @MyCoupon1 Table(CouponNumber varchar(64), CouponTypeID int, CouponAmount money)  
  declare @idoc int, @StoreID int, @BrandID int
  declare @CouponTypeID int, @TotalAmt money, @TempAmt money, @CouponAmount money, @CouponNumber varchar(64)
  
  select @BrandID=StoreBrandID from brand where StoreBrandCode=@Brandcode
  select @StoreID=StoreID from Store where BrandID=@BrandID and StoreCode=@StoreCode    

  if (isnull(@MemberID, 0) = 0) and (isnull(@CardNumber, '') = '')  
    return -7  
      
  if (isnull(@CardNumber, '') = '')   
  begin
    insert into @MyCoupon1 (CouponNumber, CouponTypeID, CouponAmount)
	select CouponNumber, CouponTypeID, CouponAmount from Coupon A left join Card B on A.CardNumber = B.CardNumber 
	  where A.Status = 2 and B.MemberID = @MemberID and A.CouponTypeID in 
        (select CouponTypeID from CouponTypeStoreCondition_List where StoreID = @StoreID and StoreConditionType = 2)            
  end else
  begin
    insert into @MyCoupon1 (CouponNumber, CouponTypeID, CouponAmount)
    select CouponNumber, CouponTypeID, CouponAmount from Coupon 
      where Status = 2 and CardNumber = @CardNumber and CouponTypeID in 
        (select CouponTypeID from CouponTypeStoreCondition_List where StoreID = @StoreID and StoreConditionType = 2) 
  end     

  EXEC sp_xml_preparedocument @idoc OUTPUT, @ItemList  
  -- Execute a SELECT statement that uses the OPENXML rowset provider.  
  if exists(SELECT PRODCODE,DEPTCODE,QTY,NETPRICE FROM OPENXML (@idoc, '/ROOT/ITEM', 1) WITH (PRODCODE varchar(512),DEPTCODE varchar(512),QTY int, NETPRICE money))  
  begin  
    insert into @T(prodcode,deptcode,qty,netprice) SELECT PRODCODE,DEPTCODE,QTY,NETPRICE FROM OPENXML (@idoc, '/ROOT/ITEM', 1) WITH (PRODCODE varchar(512),DEPTCODE varchar(512),QTY int, NETPRICE money)  
    insert into @T1(PRODCODE,DEPTCODE,QTY,TOTALAMT) 
    select PRODCODE,DEPTCODE,sum(QTY),sum(NETPRICE) from @T group by PRODCODE,DEPTCODE
  end 
  

  DECLARE CUR_T1 CURSOR fast_forward local FOR  
     select B.CouponTypeID, I.TOTALAMT from CouponTypeExchangeBinding B, @T1 I 
       where B.Prodcode = I.ProdCode or B.DepartCode = I.DeptCode and B.BindingType = 2 and B.BrandID=@BrandID
  OPEN CUR_T1  
  FETCH FROM CUR_T1 INTO @CouponTypeID, @TotalAmt
  WHILE @@FETCH_STATUS=0  
  BEGIN
  
	set @TempAmt = @TotalAmt
    DECLARE CUR_CalCoupon CURSOR fast_forward local FOR  
      select CouponNumber, CouponAmount from @MyCoupon1 where CouponTypeID = @CouponTypeID
    OPEN CUR_CalCoupon  
    FETCH FROM CUR_CalCoupon INTO @CouponNumber, @CouponAmount  
    WHILE @@FETCH_STATUS=0  
    BEGIN
	  if @TempAmt > 0
	  begin
		insert into @MyCoupon (CouponNumber, CouponTypeID)
		values(@CouponNumber, @CouponTypeID)
		set @TempAmt = @TempAmt - @CouponAmount
	  end else
	    break
      FETCH FROM CUR_CalCoupon INTO @CouponNumber, @CouponAmount   
    END  
    CLOSE CUR_CalCoupon   
    DEALLOCATE CUR_CalCoupon 
          
    FETCH FROM CUR_T1 INTO @CouponTypeID, @TotalAmt  
  END  
  CLOSE CUR_T1   
  DEALLOCATE CUR_T1 

  select CouponNumber, CouponTypeID from @MyCoupon        
--  select * from @MyCoupon where CouponTypeID in  
--    (select CouponTypeID from CouponTypeExchangeBinding B, @T I 
--      where B.Prodcode = I.ProdCode or B.DepartCode = I.DeptCode and B.BindingType = 2 and B.BrandID=@BrandID)       

  if @idoc>0  
   EXECUTE sp_xml_removedocument @idoc  
     
  return 0
end

GO
