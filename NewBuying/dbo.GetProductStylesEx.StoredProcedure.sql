USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductStylesEx]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductStylesEx]
  @MemberID             int,               -- 会员ID
  @BrandID              int,               -- 品牌ID
  @DepartCode           varchar(64),       -- 部门编码
  @IsIncludeChild       int,               -- 0：不包括部门子部门。1：包括部门子部门。    
  @ProdType             int,               -- 货品类型
  @ClassifyFilter       varchar(max),      -- 自定义货品分类过滤条件。格式：'school=1|aa=2|bb=3', 多选时格式： 'school=1,2'
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @OrderCondition		nvarchar(1000),	   -- 自定义排序条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr         varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1     
AS
/****************************************************************************
**  Name : GetProductStylesEx
**  Version: 1.0.0.10
**  Description : 获得Product Line数据	(扩展) 
**
select * from product_style
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = '' 
  --exec @a = GetProductStylesEx 0, null, 0, 0, N'<ROOT><FILTER TABLENAME= school  KEYID= schoolID  VALUEID= 1 /><FILTER TABLENAME= Region  KEYID= RegionID  VALUEID= 2 /></ROOT>', '','', 0,1000,@PageCount output,@RecordCount output,'zh_BigCN'
  exec @a = GetProductStylesEx 837, 0, null, 0, 0, N'', '','', 0,1000,@PageCount output,@RecordCount output,'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount  
		
**  Created by: Gavin @2013-07-29
**  Modify by: Gavin @2013-08-07 (ver 1.0.0.1) 增加返回同一个style不同货品的最高价和最低价
**  Modify by: Gavin @2013-08-28 (ver 1.0.0.2) 返回货品的尺寸和颜色。
**  Modify by: Gavin @2013-12-20 (ver 1.0.0.3) 增加输入参数MemberID，增加收藏和购物车的返回标志. 没有MemberID时，这两个标志返回0
**  Modify by: Gavin @2013-12-24 (ver 1.0.0.4) 因为Product_Price中prodcode已经不是唯一了，使用在取最大最小价格，和Product_Price关联时，需要做group by。
**  Modify by: Gavin @2013-12-26 (ver 1.0.0.5) 修改收藏和购物车的返回标志的内容。 0 表示false， 非0时，表示对应表的KeyID值
**  Modify by: Robin @2013-12-27 (ver 1.0.0.6) 修改取货品价格逻辑，取Product_Price表中，一个货品的在时间范围内的KeyID最大的价格作为有效价格
**  Modify by: Gavin @2014-01-09 (ver 1.0.0.7) 增加返回字段ProductPriority
**  Modify by: Gavin @2014-01-23 (ver 1.0.0.8) 修改@ClassifyFilter 多选情况的格式。
**  Modify by: Gavin @2015-08-20 (ver 1.0.0.9) 因为MemberFavorite增加prodcode字段，关联字段要改成prodcode
**  Modify by: Gavin @2016-04-27 (ver 1.0.0.10) 增加返回defaultprice的最大最小价格 
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @ForeignTable varchar(64), @ForeignID varchar(100)
  DECLARE @idoc int  
  declare @FilterTable Table(TABLENAME varchar(512),VALUEID int)
  
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  
  set @BrandID = isnull(@BrandID, 0)        
  set @DepartCode = RTrim(LTrim(isnull(@DepartCode, '')))
  set @ProdType = isnull(@ProdType, 0)
  set @ClassifyFilter = isnull(@ClassifyFilter, '')
  set @IsIncludeChild = isnull(@IsIncludeChild, 0)
  set @QueryCondition = isnull(@QueryCondition, '')
  set @OrderCondition = isnull(@OrderCondition, '')
    
  set @SQLStr = 'select P.ProdCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then ProdName2 when 3 then ProdName3 else ProdName1 end as ProdName , '	  
	  + '	 P.ProdPicFile, P.DepartCode, '
      + '   case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '	  
	  + '   D.BrandID, P.ProdType, P.ProdNote, P.ProductBrandID, '	  
      + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandName2 when 3 then B.ProductBrandName3 else B.ProductBrandName1 end as ProductBrandName, '
	  + '   case ' + cast(@Language as varchar) + ' when 2 then B.ProductBrandDesc2 when 3 then B.ProductBrandDesc3 else B.ProductBrandDesc1 end as ProductBrandDesc, '
	  + '   ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, S.ProdCodeStyle, '
	  + '   A.Memo1, A.Memo2, A.Memo3, A.Memo4, A.Memo5, A.Memo6, S.MaxPrice, S.MinPrice, P.NonSale, P.ColorID, P.ProductSizeID, '
	  + ' case when M.KeyID is null then 0 else M.KeyID end as IsShoppingCart, '
	  + ' case when N.KeyID is null then 0 else N.KeyID end as IsFavorite, A.ProductPriority, '
	  + ' S.MaxDefaultPrice, S.MinDefaultPrice, P.CreatedOn'		  
  set @SQLStr = @SQLStr + ' from product P '      
      + ' left join Product_Brand B on P.ProductBrandID = B.ProductBrandID '
      + ' left join Color C on P.ColorID = C.ColorID ' 
      + ' left join Product_style S1 on P.ProdCode = S1.ProdCode '       
      + ' left join (select ProdCodeStyle, min(X.ProdCode) as PPCode, max(Y.NetPrice) as MaxPrice, min(Y.NetPrice) as MinPrice, max(Y.DefaultPrice) as MaxDefaultPrice, min(Y.DefaultPrice) as MinDefaultPrice  from Product_style X '
      + '             left join (SELECT ProdCode, NetPrice,DefaultPrice FROM Product_Price where KeyID in  (SELECT  MAX(KeyID) AS KeyID FROM Product_Price WHERE (StartDate <= GETDATE()) AND (EndDate+1 >= GETDATE()) group by ProdCode) ) Y on X.ProdCode = Y.ProdCode group by ProdCodeStyle) S on P.ProdCode = S.PPCode ' 
/*																		
      + ' left join (select MinColorProdCode as PPCode, P1.ProdCodeStyle, P1.MaxPrice, P1.MinPrice from '
      + '             (select ProdCodeStyle, max(X.ProdCode) as PPCode, max(Y.NetPrice) as MaxPrice, min(Y.NetPrice) as MinPrice '
      + '               from Product_style X left join Product_Price Y on X.ProdCode = Y.ProdCode group by ProdCodeStyle) P1 '
      + '               left join '
      + '              (select S.ProdCodeStyle, min(P.ProdCode) as MinColorProdCode from product P left join Product_style S on P.ProdCode = S.ProdCode left join '
      + '               (select SS.ProdCodeStyle, min(PP.ColorID) as Color from Product_style SS left join Product PP  on PP.Prodcode = SS.Prodcode  '
      + '                group by SS.ProdCodeStyle) A  on S.ProdCodeStyle = A.ProdCodeStyle and P.ColorID = A.Color	'
      + '               where isnull(A.ProdCodeStyle,'''') <> ''''   '
      + '              group by S.ProdCodeStyle) P2 on P1.ProdCodeStyle = P2.ProdCodeStyle )  S on P.ProdCode = S.PPCode  '
*/      
      + ' left join (select * from Product_Particulars where LanguageID = ' + cast(@Language as varchar) + ') A on P.ProdCode = A.Prodcode '		

	  + ' left join (select KeyID, ProdCode from MemberShoppingCart where MemberID = ' + cast(@MemberID as varchar) + ') M on P.ProdCode = M.ProdCode '
	  + ' left join (select KeyID, ProdCodeStyle,ProdCode from MemberFavorite where MemberID = ' + cast(@MemberID as varchar) + ') N on S.PPCode = N.ProdCode '
	  	  
  if exists(select top 1 * from Product_Catalog) and @DepartCode <> ''         
    set @SQLStr = @SQLStr + ' left join Department D on D.DepartCode = ''' + @DepartCode + ''''
  else  
    set @SQLStr = @SQLStr + ' left join Department D on D.DepartCode = P.DepartCode '     
             
  set @SQLStr = @SQLStr + ' where isnull(S.PPCode, '''') <> '''' '     
    + ' and (P.ProductBrandID = ' + cast(@BrandID as varchar)+ ' or ' + cast(@BrandID as varchar) + '= 0)'      
    + ' and (P.ProdType = ' + cast(@ProdType as varchar)+ ' or ' + cast(@ProdType as varchar) + '= 0)'          
  if exists(select top 1 * from Product_Catalog) and @DepartCode <> ''                    
     set @SQLStr = @SQLStr + ' and (P.Prodcode in (select ProdCode from Product_Catalog where DepartCode = ''' + @DepartCode + '''))'   
  else   
     set @SQLStr = @SQLStr + ' and (P.DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + '''= '''')'  
     
  if @ClassifyFilter <> ''
  begin
    declare @TempStr varchar(max), @POS int, @ConditionStr varchar(200)
    set @TempStr = RTrim(LTrim(@ClassifyFilter))
    while len(@TempStr) > 0
    begin
	  set @POS = CharIndex('|', @TempStr)
	  if @POS > 0 
	  begin
	    set @ConditionStr = substring(@TempStr, 1, @POS - 1)
		set @TempStr = substring(@TempStr, @POS + 1, len(@TempStr) - @POS)
	  end  else
	  begin
	    set @ConditionStr = @TempStr
		set @TempStr = ''
	  end 
	  if len(@ConditionStr) > 0
	  begin
		set @POS = CharIndex('=', @ConditionStr)	
		set @ForeignTable = substring(@ConditionStr, 1, @POS - 1)		
		set @ForeignID = substring(@ConditionStr, @POS + 1, len(@ConditionStr) - @POS)		
		set @SQLStr = @SQLStr + ' and (P.Prodcode in (select Prodcode from Product_Classify where ForeignTable = ''' + @ForeignTable + ''' and ForeignkeyID in (' + @ForeignID + ')))' 
	  end
    end
  end  
/*
  if @ClassifyFilter <> ''
  begin
    EXEC sp_xml_preparedocument @idoc OUTPUT, @ClassifyFilter 
    insert into @FilterTable
    SELECT TABLENAME, VALUEID FROM OPENXML (@idoc, '/ROOT/FILTER', 1) WITH (TABLENAME varchar(512), VALUEID int)
         
    declare CUR_Foreign cursor fast_forward local for 
      select TABLENAME, VALUEID from @FilterTable
    Open CUR_Foreign  
    Fetch From CUR_Foreign INTO @ForeignTable, @ForeignID
    WHILE @@FETCH_STATUS=0      
    BEGIN          
      set @SQLStr = @SQLStr + ' and (P.Prodcode in (select Prodcode from Product_Classify where ForeignTable = ''' + @ForeignTable + ''' and ForeignkeyID = ' + cast(@ForeignID as varchar) + '))'     
      Fetch From CUR_Foreign INTO @ForeignTable, @ForeignID
    end  
    CLOSE CUR_Foreign  
    Deallocate CUR_Foreign      
  end  
*/ 
  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @QueryCondition
  
  return 0
end

GO
