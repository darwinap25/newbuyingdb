USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSKU]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\darwin.pasco
 * Created At: 2017/10/06 06:56:03
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetSKU]
  
  
/************************************
v 1.0 - Created by Darwin 10/06/2017

- Get all BoxSales SKU


************************************/
AS

SET NOCOUNT ON;

BEGIN
  
    SELECT 
        [p].[ProdCode], 
        [p].[ProdDesc1],
        [p].[ProdDesc2],
        [p].[ProdDesc3],
        CASE [p].[BOM]
            WHEN 1 THEN 'YES'
            ELSE 'NO'
        END AS IsBoxSale            
    FROM [dbo].[BUY_PRODUCT] p
    WHERE [BOM] = 1
  
END



GO
