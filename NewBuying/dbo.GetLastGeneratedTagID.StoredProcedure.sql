USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetLastGeneratedTagID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/14 04:12:24
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetLastGeneratedTagID]
  @tagtypeid            int,
  @result               [bigint] OUTPUT
AS
SET NOCOUNT ON;

BEGIN

    SET @result = (SELECT [c].[LastSequenceGenerated] FROM [dbo].[CouponType] c
                        WHERE [c].[CouponTypeID] = @tagtypeid)

END


GO
