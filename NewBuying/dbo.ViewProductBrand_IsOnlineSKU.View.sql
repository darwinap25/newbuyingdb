USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewProductBrand_IsOnlineSKU]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewProductBrand_IsOnlineSKU]
AS
/*
*/
select * from Product_Brand where ProductBrandID in
(select distinct ProductBrandID from product where IsOnlineSKU = 1)

GO
