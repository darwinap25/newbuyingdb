USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_Catalog]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Catalog](
	[ProdCode] [varchar](64) NOT NULL,
	[DepartCode] [varchar](64) NOT NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PRODUCT_CATALOG] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC,
	[DepartCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Catalog', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Catalog', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品目录绑定表（货品和部门，多对多）
当此表数据存在时，根据部门查询货品，将根据此表查询。否则根据product表的 departcode查询' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Catalog'
GO
