USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberMessageObject]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[UpdateMemberMessageObject]
  @MemberID            int,
  @MessageID           int,
  @IsRead              int=1      -- 设置IsRead标志:  0：未被读。 1：已读。 -1: 已经被删除
AS
/****************************************************************************
**  Name : UpdateMemberMessageObject
**  Version: 1.0.0.1
**  Description : 标志消息已经读过
**
**  Parameter :
  declare @a int, @MessageID int
  exec @a = UpdateMemberMessageObject 2, 2
  print @a  
**  Created by: Gavin @2012-05-23
**  Modify by: Gavin @2014-10-28 （ver 1.0.0.1） 增加参数@IsRead
sp_helptext UpdateMemberMessageObject
**
****************************************************************************/
begin
  update MessageReceiveList set IsRead = @IsRead where MessageID = @MessageID and MemberID = @MemberID
  if @@Rowcount <> 0
    return 0
  else  
    return -97

return 0    
end

GO
