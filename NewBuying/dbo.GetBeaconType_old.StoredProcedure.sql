USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetBeaconType_old]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetBeaconType_old]
  @PositionID      varchar(64),      -- iBeacon的ID 必填
  @CardNumber     varchar(64),      -- 卡号 如果不填，获取的Coupon不绑定card。
  @Additional     varchar(512),     -- 预留
  @Battery        varchar(64),      -- iBeacon电池余量
  @TokenID        varchar(64),      -- 手机的TokenID
  @LanguageAbbr   varchar(20)=''	 
AS
/****************************************************************************
**  Name : iBeaconRequest
**  Version: 1.0.0.0
**  Description : 由iBeacon触发提交的请求。
  (根据ibeaconid在iBeaconGroup中找到所能提供的功能)
 
   declare @A int
   exec @A=GetBeaconType '1201-6', '01000045', '', '1','',''
   print @A
select * from ibeacon
**  Created by: Gavin @2015-07-09
**
****************************************************************************/
BEGIN
  declare @Language int, @MemberID int, @CardTotalAmount money, @CardTotalPoints int, @count int, @recordcount int
  declare @AreaID int, @iBeaconGroupID int, @AssociationType int, @AssociationID int, @iBeaconID int
  declare @EachCoupon varchar(64), @MaxKeyID int, @StoreID int, @StoreCode varchar(64) 

  --DECLARE @ReturnTable table(Title varchar(512), Association_Type int, Association_ID varchar(64), Assiocation_Code varchar(64), 
  --  PicFile varchar(512), URL varchar(512), [Desc] varchar(512))

  Create Table #ReturnTable (Title varchar(512), Association_Type int, Association_ID varchar(64), Assiocation_Code varchar(64), 
    PicFile varchar(512), URL varchar(512), [Desc] varchar(512))
  
  set @PositionID = ISNULL(@PositionID, '')
  set @CardNumber = ISNULL(@CardNumber, '')
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1	
  
  select @MemberID = MemberID, @CardTotalAmount = TotalAmount, @CardTotalPoints = TotalPoints 
    from Card where CardNumber = @CardNumber	
  select @AreaID = AreaID, @iBeaconID = iBeaconID from iBeacon where iBeaconCode = @PositionID
  select @iBeaconGroupID = iBeaconGroupID from iBeaconGroupMap where iBeaconID = @iBeaconID
  select @AssociationID = AssociationID, @AssociationType = AssociationType from iBeaconGroup where iBeaconGroupID = @iBeaconGroupID 

  set @MemberID = ISNULL(@MemberID, 0)
  set @AreaID = ISNULL(@AreaID, 0)
  set @iBeaconGroupID = ISNULL(@iBeaconGroupID, 0)
  set @AssociationID = ISNULL(@AssociationID, 0)
  set @AssociationType = ISNULL(@AssociationType, 0)

  declare @ReturnAmount money ,@ReturnPoint int ,@ReturnMessageStr varchar(max), @ReturnCouponNumber varchar(64)

  insert into #ReturnTable --(Title, Association_Type, Association_ID, Assiocation_Code, PicFile, URL, [Desc])
  exec MemberCardEarnCouponByPosition @CardNumber, @PositionID, '1',
     @ReturnAmount output, @ReturnPoint output, @ReturnCouponNumber output, @ReturnMessageStr output, '','','',@LanguageAbbr

  insert into #ReturnTable(Title, Association_Type, Association_ID, Assiocation_Code, PicFile, URL, [Desc])
  exec MemberCardPromotionMsgByPosition @CardNumber, @PositionID, '1', @LanguageAbbr
      
  insert into #ReturnTable(Title, Association_Type, Association_ID, Assiocation_Code, PicFile, URL, [Desc])
  exec MemberCardMessageByPosition @CardNumber, @PositionID, '1', @LanguageAbbr 
  
  select * from #ReturnTable
END

GO
