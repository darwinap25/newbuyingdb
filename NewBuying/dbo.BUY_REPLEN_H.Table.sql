USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_REPLEN_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_REPLEN_H](
	[ReplenCode] [varchar](64) NOT NULL,
	[UseReplenFormula] [int] NULL,
	[ReplenFormulaID] [int] NULL,
	[StoreID] [int] NOT NULL,
	[TargetType] [int] NOT NULL,
	[OrderTargetID] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL,
	[Priority] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_REPLEN_H] PRIMARY KEY CLUSTERED 
(
	[ReplenCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_REPLEN_H] ADD  DEFAULT ((0)) FOR [UseReplenFormula]
GO
ALTER TABLE [dbo].[BUY_REPLEN_H] ADD  DEFAULT ((1)) FOR [TargetType]
GO
ALTER TABLE [dbo].[BUY_REPLEN_H] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[BUY_REPLEN_H] ADD  DEFAULT (getdate()) FOR [EndDate]
GO
ALTER TABLE [dbo].[BUY_REPLEN_H] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[BUY_REPLEN_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_REPLEN_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'ReplenCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否使用补货公式。0：不使用（此时如果需要补货，使用BUY_REPLEN_D的数量设置）。
 1：使用（补货数量使用BUY_REPLENFORMULA的设置来计算）。 默认0.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'UseReplenFormula'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BUY_REPLENFORMULA表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'ReplenFormulaID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货目标类型。 0：供应商。 1：总部。 默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'TargetType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货目标ID。（如果是店铺订货，则填写总部的ID。 如果总部订货，则填写供应商ID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'OrderTargetID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。0：无效，1：有效。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级。 （库存不足情况下。按 从小到大 优先分配。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺补货设置。主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_REPLEN_H'
GO
