USE [NewBuying]
GO
/****** Object:  Table [dbo].[POSDAYREPORT]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POSDAYREPORT](
	[StoreCode] [varchar](64) NOT NULL,
	[RegisterCode] [varchar](64) NOT NULL,
	[BusDate] [date] NOT NULL,
	[ReportID] [int] NULL,
	[ReportName] [varchar](512) NULL,
	[ReportContext] [nvarchar](max) NULL,
 CONSTRAINT [PK_POSDAYREPORT] PRIMARY KEY CLUSTERED 
(
	[StoreCode] ASC,
	[RegisterCode] ASC,
	[BusDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSDAYREPORT', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS机编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSDAYREPORT', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'business day' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSDAYREPORT', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'报表ID：
1： 部门报表
2： 时段报表
3： 付款报表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSDAYREPORT', @level2type=N'COLUMN',@level2name=N'ReportID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'报表名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSDAYREPORT', @level2type=N'COLUMN',@level2name=N'ReportName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'报表内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSDAYREPORT', @level2type=N'COLUMN',@level2name=N'ReportContext'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS的日报表存储表。
存放每天日结时产生的POS交易报表。
报表数据按照 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POSDAYREPORT'
GO
