USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessageTemplateDetail]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageTemplateDetail](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MessageTemplateID] [int] NOT NULL,
	[MessageServiceTypeID] [int] NULL,
	[status] [int] NULL,
	[MessageTitle1] [nvarchar](512) NULL,
	[MessageTitle2] [nvarchar](512) NULL,
	[MessageTitle3] [nvarchar](512) NULL,
	[TemplateContent1] [nvarchar](max) NULL,
	[TemplateContent2] [nvarchar](max) NULL,
	[TemplateContent3] [nvarchar](max) NULL,
	[PicFile] [nvarchar](512) NULL,
	[URL] [nvarchar](512) NULL,
 CONSTRAINT [PK_MESSAGETEMPLATEDETAIL] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MessageTemplate表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'MessageTemplateID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息发送方式 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：不使用。1：使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息标题1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'MessageTitle1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息标题2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'MessageTitle2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息标题3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'MessageTitle3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板内容1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'TemplateContent1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板内容2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'TemplateContent2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板内容3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'TemplateContent3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板相关的图片文件路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'PicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'URL。 用户自填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail', @level2type=N'COLUMN',@level2name=N'URL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息内容模板明细。
@2015-08-27 增加字段：PicFile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplateDetail'
GO
