USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberClause]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberClause]
  @ClauseTypeCode		varchar(512),  -- 条款类型编码 
  @ClauseSubCode		varchar(512),  -- 条款编码（根据条款类型不同，不同内容）
  @BrandID              int,           -- 品牌ID
  @LanguageAbbr			varchar(20)=''	
AS
/****************************************************************************
**  Name : GetMemberClause
**  Version: 1.0.0.2
**  Description : 返回用户条款
**
**  Parameter :
  exec GetMemberClause '','',''
**  select * from  MemberClause
**  Created by: Gavin @2012-02-07
**  Modify by: Gavin @2012-09-26 (ver 1.0.0.1) @BrandCode 参数传入的是 BrandID, 参数名称暂时不变，修改处理过程
**  Modify by: Gavin @2012-10-10 (ver 1.0.0.2) 修改传入参数@BrandCode varchar(64) 为 @BrandID int
**MemberClause
****************************************************************************/
begin 
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
 
  set @BrandID = isnull(@BrandID, 0)
  select MemberClauseID, ClauseTypeCode, ClauseSubCode, case @Language when 2 then MemberClauseDesc2 when 3 then MemberClauseDesc3 else MemberClauseDesc1 end as MemberClauseDesc  
   from MemberClause
     where (ClauseTypeCode = @ClauseTypeCode or isnull(@ClauseTypeCode, '') = '') and (ClauseSubCode = @ClauseSubCode or isnull(@ClauseSubCode, '') = '')    
       and (BrandID = @BrandID or isnull(@BrandID, 0) = 0)
  return 0 
end

GO
