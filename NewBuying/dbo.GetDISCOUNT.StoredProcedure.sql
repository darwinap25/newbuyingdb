USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetDISCOUNT]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDISCOUNT]
  @DiscountCode				   VARCHAR(64),			         -- 折扣编码
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetDISCOUNT
**  Version : 1.0.0.0
**  Description : 折扣选项列表
**
  declare  @a int, @PageCount int, @RecordCount int
  exec @a = GetDISCOUNT null,   '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  select * from buy_discount
  select * from BUY_REASON
**  Created by Gavin @2015-03-06
**  Modified by Gavin @2015-04-16 (ver 1.0.0.1) BUY_CURRENCY表增加字段CurrencyPicFile， SP增加返回此字段
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SET @DiscountCode = ISNULL(@DiscountCode, '')
  SET @WhereStr = ' WHERE A.Status = 1 '
  IF @DiscountCode <> ''  
    SET @WhereStr = ' AND A.DiscountCode=''' + @DiscountCode + ''' '
    
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1
  
  SET @SQLStr = 'SELECT A.DiscountID as KeyID,A.DiscountCode,A.ReasonCode, '
    + ' CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN A.Description2 WHEN 3 THEN A.Description3 ELSE A.Description1 END AS Description, '
    + ' A.DiscType,A.SalesDiscLevel,A.DiscPrice,A.AuthLevel,A.AllowDiscOnDisc,A.AllowChgDisc,A.TransDiscControl,A.Status,A.Note, '
    + ' B.ReasonID, CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN B.Description2 WHEN 3 THEN B.Description3 ELSE B.Description1 END AS ReasonDesc '
    + ' FROM BUY_DISCOUNT A LEFT JOIN BUY_REASON B ON A.ReasonCode = B.ReasonCode '
    + @WhereStr

  EXEC SelectDataInBatchs @SQLStr, 'DiscountCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr

  RETURN 0
END

GO
