USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenSalesTxnByAfterSales_Old]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenSalesTxnByAfterSales_Old]  
  @AfterSalesTxnNo				varchar(64),    -- 售后单号
  @RefSalesTxnNo				varchar(64),    -- 售后单中相关的交易单号
  @CreatedBy                    int
AS
/****************************************************************************
**  Name : GenSalesTxnByAfterSales_Old
**  Version: 1.0.0.5
**  Description : (bauhaus需求) 根据aftersales单，产生sales数据。（退货，换货） 
**  Parameter :
**
declare @a int, @TxnNo varchar(64)
exec @a = GenSalesTxnByAfterSales @TxnNo output
print @a
print @TxnNo
** Create By Gavin @2016-10-13
** Modify By Gavin @2016-10-21 (ver 1.0.0.1) 允许前台分多次退货。退货时检查退货数量 和所有的 退货单，只有本单中所有货品被退货，才可以关闭。
                                             修正aftersales_D和sales_D的join关联字段，应使用refseqno
** Modify By Gavin @2016-11-24 (ver 1.0.0.2) 新增的sales_H单中RefTxnNo 改成存放 aftersales的txnno
** Modify By Gavin @2016-12-29 (ver 1.0.0.3) 退货的 RefTxnNo 也填写 aftersales的txnno
** Modify By Gavin @2017-01-25 (ver 1.0.0.4) 退货的sales_H 的 salestype 应该是10. exchange 的应该是11
** Modify By Gavin @2017-03-03 (ver 1.0.0.5) 退货时,需要从aftersales_D中读取Qty 。（qty数量可能不是这条记录的全部数量。）
****************************************************************************/
begin
  declare @InvalidFlag int, @TxnType int, @TxnNo varchar(64)

  set @AfterSalesTxnNo = isnull(@AfterSalesTxnNo, '')
  set @RefSalesTxnNo = isnull(@RefSalesTxnNo, '')
  select @TxnType = TxnType from aftersales_h where TxnNo = @AfterSalesTxnNo
  select @InvalidFlag = isnull(InvalidateFlag,0) from sales_h where TransNum = @RefSalesTxnNo
  set @InvalidFlag = isnull(@InvalidFlag, -1)

  if @InvalidFlag = 0
  begin
    if @TxnType = 2  -- 退货
	begin

	  exec GetRefNoString 'KTXNNO', @TxnNo output   
	  insert into sales_D (TransNum,SeqNo,ProdCode,ProdDesc,Serialno,Collected,UnitPrice,NetPrice,Qty,NetAmount,Additional1,POPrice,POReasonCode,
	    DiscountPrice,DiscountAmount,ReservedDate,PickupDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	  select @TxnNo,ROW_NUMBER() OVER(order by A.ProdCode) as iid,A.ProdCode,A.ProdDesc,A.Serialno, A.Collected,
	     --case Collected when 0 then 80 when 1 then 81 when 82 then 2 when 4 then 84 else -1 end,
	     A.UnitPrice,A.NetPrice,-1 * abs(B.Qty), -1 * A.NetPrice * abs(B.Qty), A.Additional1,A.POPrice,A.POReasonCode,
	     A.DiscountPrice,A.DiscountAmount,A.ReservedDate,
		 A.PickupDate,Getdate(),@CreatedBy,Getdate(),@CreatedBy 
	  from (select * from sales_D where TransNum = @RefSalesTxnNo) A 
	    left join (select * from aftersales_D where txnno = @AfterSalesTxnNo) B on A.SeqNo = B.RefSeqNo and A.ProdCode = B.ProdCode
	 -- where A.SeqNo in (select * from aftersales_D where txnno = @AfterSalesTxnNo)

      insert into sales_T (TransNum,SeqNo,TenderID,TenderCode,TenderDesc,TenderAmount,LocalAmount,ExchangeRate,Additional,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	  select @TxnNo,ROW_NUMBER() OVER(order by TenderID) as iid,TenderID,TenderCode,TenderDesc,-1*TenderAmount,-1*LocalAmount,ExchangeRate,Additional,getdate(),@CreatedBy,getdate(),@CreatedBy 
	    from aftersales_T where txnno = @AfterSalesTxnNo

	  insert into sales_h (TransNum,RefTransNum,StoreCode,RegisterCode,ServerCode,BrandCode,BusDate,TxnDate,TransType,CashierID,SalesManID,Channel,TotalAmount,
			Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliveryBy,DeliveryFullAddress,RequestDeliveryDate,DeliveryDate,
			DeliveryLongitude,DeliveryLatitude,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
			Additional,SalesTag,PaySettleDate,PickupType,PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
			BillContactPhone,InvalidateFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	  select top 1 @TxnNo,@AfterSalesTxnNo,H.StoreCode,H.RegisterCode,ServerCode,BrandCode,AH.BusDate,AH.TxnDate,10,H.CashierID,H.SalesManID,Channel,-1 * abs(AH.TotalAmount),
			4,H.MemberID,MemberName,MemberMobilePhone,H.CardNumber,MemberAddress,H.DeliveryBy,H.DeliveryFullAddress,RequestDeliveryDate,H.DeliveryDate,
			DeliveryLongitude,DeliveryLatitude,H.DeliveryNumber,H.LogisticsProviderID,H.Contact,H.ContactPhone,SalesReceipt,SalesReceiptBIN,
			Additional,SalesTag,H.PaySettleDate,H.PickupType,H.PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
			BillContactPhone,H.InvalidateFlag,getdate(),@CreatedBy,getdate(),@CreatedBy
	   from sales_h H left join aftersales_H AH on H.TransNum = AH.RefTransNum
	   where H.TransNum = @RefSalesTxnNo

	   update Sales_D set Collected = case D.Collected when 0 then 80 when 1 then 81 when 2 then 82 when 4 then 84 else D.Collected end,
	                      UpdatedOn = GETDATE()
	   from (select * from Sales_D where TransNum = @RefSalesTxnNo) D 
	     left join (select * from aftersales_D where TxnNo = @AfterSalesTxnNo) AD on D.SeqNo = AD.RefSeqNo and D.ProdCode = AD.RefSeqNo 
	   where AD.TxnNo is not null AND AD.Qty <= D.Qty

	   if not exists(select * 
	               from (select * from Sales_D where TransNum = @RefSalesTxnNo) A 
	               left join (select D.ProdCode, D.RefSeqNo, sum(D.Qty) as Qty from aftersales_D D left join aftersales_H H on D.TxnNo = H.TxnNo 	                 
	                            Where H.RefTransNum = @RefSalesTxnNo
				              group by D.ProdCode, D.RefSeqNo) B on A.ProdCode = B.ProdCode and A.SeqNo = B.RefSeqNo
				  where ISNULL(A.Qty,0) <= ISNULL(B.Qty,0) OR (ISNULL(B.QTY,0) = 0 and ISNULL(A.Qty,0) = 0)
				 )
       begin
	     Update Sales_H set InvalidateFlag = 2, UpdatedOn = GETDATE()
	     where TransNum = @RefSalesTxnNo
	   end else
	   begin
	     Update Sales_H set InvalidateFlag = 4, UpdatedOn = GETDATE()
	     where TransNum = @RefSalesTxnNo
	   end

	end else if @TxnType = 3  -- 换货
	begin

	  exec GetRefNoString 'KTXNNO', @TxnNo output   
	  insert into sales_D (TransNum,SeqNo,ProdCode,ProdDesc,Serialno,Collected,UnitPrice,NetPrice,Qty,NetAmount,Additional1,POPrice,POReasonCode,
	    DiscountPrice,DiscountAmount,ReservedDate,PickupDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	  select @TxnNo,ROW_NUMBER() OVER(order by ProdCode) as iid,ProdCode,ProdDesc,'', Collected,
	     RetailPrice,NetPrice,Qty,NetAmount,'',POPrice,'',
	     0,0,null,
		 null,Getdate(),@CreatedBy,Getdate(),@CreatedBy 
	  from aftersales_D A 
	  where txnno = @AfterSalesTxnNo

      insert into sales_T (TransNum,SeqNo,TenderID,TenderCode,TenderDesc,TenderAmount,LocalAmount,ExchangeRate,Additional,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	  select @TxnNo,ROW_NUMBER() OVER(order by TenderID) as iid,TenderID,TenderCode,TenderDesc,TenderAmount,LocalAmount,ExchangeRate,Additional,getdate(),@CreatedBy,getdate(),@CreatedBy 
	    from aftersales_T where txnno = @AfterSalesTxnNo
      
	  -- RefTxnNo 改成存放 aftersales的@AfterSalesTxnNo 而不是 @RefSalesTxnNo 
	  insert into sales_h (TransNum,RefTransNum,StoreCode,RegisterCode,ServerCode,BrandCode,BusDate,TxnDate,TransType,CashierID,SalesManID,Channel,TotalAmount,
			Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliveryBy,DeliveryFullAddress,RequestDeliveryDate,DeliveryDate,
			DeliveryLongitude,DeliveryLatitude,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
			Additional,SalesTag,PaySettleDate,PickupType,PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
			BillContactPhone,InvalidateFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
	  select top 1 @TxnNo,@AfterSalesTxnNo,H.StoreCode,H.RegisterCode,ServerCode,BrandCode,AH.BusDate,AH.TxnDate,11,H.CashierID,H.SalesManID,Channel,AH.TotalAmount,
			4,H.MemberID,MemberName,MemberMobilePhone,H.CardNumber,MemberAddress,H.DeliveryBy,H.DeliveryFullAddress,RequestDeliveryDate,H.DeliveryDate,
			DeliveryLongitude,DeliveryLatitude,H.DeliveryNumber,H.LogisticsProviderID,H.Contact,H.ContactPhone,SalesReceipt,SalesReceiptBIN,
			Additional,SalesTag,H.PaySettleDate,H.PickupType,H.PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
			BillContactPhone,H.InvalidateFlag,getdate(),@CreatedBy,getdate(),@CreatedBy
	   from sales_h H left join aftersales_H AH on H.TransNum = AH.RefTransNum
	   where H.TransNum = @RefSalesTxnNo

	   update Sales_D set Collected = case D.Collected when 0 then 90 when 1 then 91 when 2 then 92 when 4 then 94 else D.Collected end,
	     UpdatedOn = GETDATE()
	   from (select * from Sales_D where TransNum = @RefSalesTxnNo) D 
	     left join (select * from aftersales_D where TxnNo = @AfterSalesTxnNo) AD on D.SeqNo = AD.RefSeqNo and D.ProdCode = AD.RefSeqNo 
	   where AD.TxnNo is not null and AD.Qty < 0
	   Update Sales_H set InvalidateFlag = 3, UpdatedOn = GETDATE()
	   where TransNum = @RefSalesTxnNo

	end
  end    
  return 0
end

GO
