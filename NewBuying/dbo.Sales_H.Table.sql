USE [NewBuying]
GO
/****** Object:  Table [dbo].[Sales_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales_H](
	[TransNum] [varchar](64) NOT NULL,
	[TransType] [int] NOT NULL DEFAULT ((0)),
	[StoreCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[ServerCode] [varchar](64) NULL,
	[BrandCode] [varchar](64) NULL,
	[BusDate] [datetime] NULL,
	[TxnDate] [datetime] NULL DEFAULT (getdate()),
	[CashierID] [int] NOT NULL,
	[RefTransNum] [varchar](64) NULL,
	[SalesManID] [int] NULL,
	[Channel] [varchar](512) NULL,
	[TotalAmount] [decimal](12, 4) NOT NULL,
	[Status] [int] NOT NULL,
	[TransDiscount] [decimal](12, 4) NULL,
	[TransDiscountType] [int] NULL DEFAULT ((0)),
	[TransReason] [nvarchar](512) NULL,
	[InvalidateFlag] [int] NULL,
	[MemberSalesFlag] [int] NULL DEFAULT ((0)),
	[MemberID] [varchar](64) NULL,
	[MemberName] [nvarchar](512) NULL,
	[MemberMobilePhone] [varchar](64) NULL,
	[MemberAddress] [nvarchar](512) NULL,
	[CardNumber] [varchar](64) NULL,
	[DeliveryFlag] [int] NULL DEFAULT ((0)),
	[DeliveryCountry] [varchar](64) NULL,
	[DeliveryProvince] [varchar](64) NULL,
	[DeliveryCity] [varchar](64) NULL,
	[DeliveryDistrict] [varchar](64) NULL,
	[DeliveryAddressDetail] [nvarchar](512) NULL,
	[DeliveryFullAddress] [nvarchar](512) NULL,
	[DeliveryLongitude] [varchar](512) NULL,
	[DeliveryLatitude] [varchar](512) NULL,
	[DeliveryNumber] [varchar](64) NULL,
	[LogisticsProviderID] [int] NULL,
	[RequestDeliveryDate] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryBy] [int] NULL,
	[Contact] [nvarchar](512) NULL,
	[ContactPhone] [nvarchar](512) NULL,
	[PickupType] [int] NULL DEFAULT ((1)),
	[PickupStoreCode] [varchar](64) NULL,
	[CODFlag] [int] NULL DEFAULT ((0)),
	[SettlementDate] [datetime] NULL,
	[SettlementStaffID] [int] NULL,
	[PaySettleDate] [datetime] NULL,
	[CompleteDate] [datetime] NULL,
	[SalesReceipt] [varchar](max) NULL,
	[SalesReceiptBIN] [varbinary](max) NULL,
	[PromotionFlag] [int] NULL,
	[BillAddress] [nvarchar](512) NULL,
	[BillContact] [varchar](64) NULL,
	[BillContactPhone] [varchar](64) NULL,
	[Remark] [nvarchar](512) NULL,
	[Additional] [nvarchar](max) NULL,
	[SalesTag] [varchar](512) NULL,
	[MemberEmail] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SALES_H] PRIMARY KEY CLUSTERED 
(
	[TransNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'TransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型.
0：Normal Sales
1：Advance Sales
2：Deposit Sales
3：Remote Collection
9：Void Txn
10：return
11：exchange


' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'TransType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS机编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'BrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期时间。（system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收银员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'CashierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'RefTransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'SalesManID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易来源通道。（具体未定）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'Channel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总销售金额.（已经减去TransDiscount）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'TotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易状态。（2013.06.06增加status=0，表示在购物车中）
-- 交易状态：0:购物车.  1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货. 5:交易完成. 6:交付运送. 8:拒收。9:已延迟， 10：拣货。11：待出库。 12：出库。13：部分送达，14：全部送达
-- 不需要快递状态: 0:购物车.  1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货. 5:交易完成.  10：拣货。 11：待出库。 12：出库。13：部分送达，14：全部送达
-- 需要快递状态: 0:购物车.  1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货. 6:交付运送. 5:交易完成. (6 和 5 之间可能插入 8, 9)   10：拣货。11：待出库。 12：出库。13：部分送达，14：全部送达
-- Bauhaus整个流程： 0:购物车，1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货，10：拣货。 11：待出库。 12：出库。13：部分送达，14：全部送达，5:交易完成
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'整单折扣 （Sales off ）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'TransDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'整单折扣内容的类型。 0：没有折扣。1：金额。 2：百分比' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'TransDiscountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'整单原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'TransReason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单无效标志。订单无效时，状态为完成。 默认0
0： 订单有效
1： 已经被Void。
2： 已经被refund。
3： 已经被exchange
4： 已经被部分refund或者exchange。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'InvalidateFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否会员销售。默认0.
0：不是会员销售。
1： MemberNo 中填写 MemberID
2： MemberNo 中填写 手机号
3： MemberNo 中填写 邮箱号
4： MemberNo 中填写 会员卡号
5： MemberNo 中填写 身份证号
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'MemberSalesFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员号码。 MemberSalesFlag = 1时需要填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'MemberName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员的检索号码（手机号）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'MemberMobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'MemberAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡号码。 MemberSalesFlag = 1时需要填' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货标志。0： 自提，不送货。 1：送货' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在国家（存放Country表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryCountry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在省 （存放Province表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryProvince'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在城市 （存放City表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryCity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址所在区县 （存放District表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryDistrict'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货详细地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryAddressDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货完整地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryFullAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货坐标，经度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryLongitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货坐标，纬度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryLatitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货单号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流供应商ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'LogisticsProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要求送货日期  （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'RequestDeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货物送达日期   （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'DeliveryBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货方式。默认1
1：门店自提。
2：送货上门。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'PickupType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货店铺编号。 （PickupType=1 时有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'PickupStoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货到付款标志。默认 0
0：先款后货。 1：钱货两讫。
当PickupType=2，选择CODFlag=1 ，即表示货到付款。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'CODFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货最后完成日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'SettlementDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货最后确认人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'SettlementStaffID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付最后完成日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'PaySettleDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单最终完成日期 （business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'CompleteDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售小票（格式文本）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'SalesReceipt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售小票（二进制格式）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'SalesReceiptBIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已经做过SVA Promotion计算（coupon or point）。 null/0：没有。1：已经做过' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'PromotionFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账单地址（信用卡的账单，不是发票）（用于校验）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'BillAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账单联系人（信用卡的联系人）（用于校验）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'BillContact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账单联系人电话（信用卡的联系人）（用于校验）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'BillContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单Tag信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'SalesTag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员的Email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'MemberEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售交易表。主表.
@2015-10-23 增加字段 LogisticsProviderID
@2016-05-24 
   Status 增加状态： 10：拣货。 11：出库。12：部分送达，13：全部送达
@2016-06-14
   Status 状态修改： 10：拣货。 11：待出库。 12：出库。13：部分送达，14：全部送达

@2017-03-20 bauhaus 的交易状态：
-- Bauhaus整个流程： 0:购物车，1:暂存. 2:取消. 3:未确认支付. 4:已付款未提货，10：拣货。 11：待出库。 12：出库。13：部分送达，14：全部送达，5:交易完成' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_H'
GO
