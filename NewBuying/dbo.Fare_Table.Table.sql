USE [NewBuying]
GO
/****** Object:  Table [dbo].[Fare_Table]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fare_Table](
	[DestinationID] [int] IDENTITY(1,1) NOT NULL,
	[LPNType] [int] NOT NULL,
	[DestinationCode] [varchar](64) NOT NULL,
	[DestinationType] [int] NULL,
	[DestinationTypeDesc_En] [nvarchar](512) NULL,
	[DestinationTypeDesc_En_UI] [nvarchar](512) NULL,
	[DestinationTypeDesc_Cn] [nvarchar](512) NULL,
	[Destination_En] [nvarchar](512) NULL,
	[Destination_En_UI] [nvarchar](512) NULL,
	[Destination_Cn] [nvarchar](512) NULL,
	[MinPrice] [varchar](64) NULL,
	[MaxPrice] [varchar](64) NULL,
	[PriceRange] [varchar](64) NULL,
	[GreenFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_FARE_TABLE] PRIMARY KEY CLUSTERED 
(
	[DestinationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Fare_Table] ADD  DEFAULT ((1)) FOR [DestinationType]
GO
ALTER TABLE [dbo].[Fare_Table] ADD  DEFAULT ('1') FOR [DestinationTypeDesc_En]
GO
ALTER TABLE [dbo].[Fare_Table] ADD  DEFAULT ('1') FOR [DestinationTypeDesc_En_UI]
GO
ALTER TABLE [dbo].[Fare_Table] ADD  DEFAULT ('1') FOR [DestinationTypeDesc_Cn]
GO
ALTER TABLE [dbo].[Fare_Table] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Fare_Table] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key for destination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'DestinationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车牌类型。 或者说是大区类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'LPNType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'destination code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'DestinationCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Hong Kong Island 香港島
2: Kowloon 九龍
3: New Territories 新界
4: North Lantau Island 北大嶼山
5: South Lantau Island 南大嶼山
6: Airport Island 機場島
7: Green Card - Airport Island 綠卡－機場島
8: Green Card - North Lantau Island 綠卡－北大嶼山' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'DestinationType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型描述，英文' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'DestinationTypeDesc_En'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型描述，英文 UI 使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'DestinationTypeDesc_En_UI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型描述，中文' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'DestinationTypeDesc_Cn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual local district 英文' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'Destination_En'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual local district 英文, UI 使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'Destination_En_UI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual local district 中文繁体' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'Destination_Cn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小价，字符串，可以存放格式化数值，方便打印' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'MinPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大价，字符串，可以存放格式化数值，方便打印' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'MaxPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格范围' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'PriceRange'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绿卡标志' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'GreenFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to create and maintain taxi fare table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Fare_Table'
GO
