USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenCouponVerifyNum]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenCouponVerifyNum]
  @MemberID int,
  @CouponNumber varchar(1000),  
  @VerifyNum varchar(64) output,
  @ReturnMessageStr varchar(max) output
AS
/****************************************************************************
**  Name : GenCouponVerifyNum
**  Version: 1.0.0.1
**  Description : 给指定的coupon产生一个校验码，并填写到coupon表中.(目前自动产生的号码是4位随机数字)
  declare @a int, @VerifyNum varchar(64), @ReturnMessageStr varchar(max)
  exec @a = GenCouponVerifyNum 1, '01000001', @VerifyNum output, @ReturnMessageStr output
  print @a
  print @VerifyNum
  print @ReturnMessageStr
  select * from MemberMessageAccount
  select top 10 * from coupon	select top 10 * from coupontype	   update coupontype set isneedverifynum = 1 where coupontypeid=4
**  Created by: Gavin @2014-02-19
**  Modified by: Gavin @2014-02-26 (ver 1.0.0.1) 输入参数允许输入多个CouponNumber，号码之间用“ , ”隔开，同一批输入的，使用相同的校验码， 增加输入参数，并增加返回消息（json格式）
**
****************************************************************************/
begin
  declare @IsNeedVerifyNum int, @nCouponNumber varchar(64),  @TempStr varchar(64), @Position int, @Returnstatus int 
  declare @T table (CouponNumber varchar(64), IsNeedVerify int)
  set @IsNeedVerifyNum = 0
  set @VerifyNum = ''
  set @nCouponNumber = ''
  set @Returnstatus = 0
  set @TempStr = RTrim(LTrim(isnull(@CouponNumber, '')))
   
  while len(@TempStr) > 0 
  begin
	set @Position = CharIndex(',', @TempStr)
	if @Position > 0 
	begin
	  set @nCouponNumber = substring(@TempStr, 1, @Position - 1)
	  set @TempStr = substring(@TempStr, @Position + 1, len(@TempStr) - @Position)
	end else
	begin
	  set @nCouponNumber = @TempStr
	  set @TempStr = ''
	end   	
	insert into @T (CouponNumber, IsNeedVerify)
    select CouponNumber, IsNeedVerifyNum 
      from Coupon C left join CouponType T on C.CouponTypeID = T.CouponTypeID 
    where CouponNumber = @nCouponNumber 	 
  end

  if exists(select * from @T where IsNeedVerify = 1)
  begin
    select @VerifyNum = cast(floor(rand() * 10000) as varchar)
    select @VerifyNum	= right('0000' + @VerifyNum, 4)
    update Coupon set VerifyNum = @VerifyNum, VerifyNumUpdatedOn = getdate() 
    where CouponNumber in (select CouponNumber from @T)   
    if @@rowcount = 0
    begin
      set @VerifyNum = ''
      set @Returnstatus = -1
    end else
      set @Returnstatus = 0   
  end else
  begin
    set @VerifyNum  = ''
    set @Returnstatus = 0
  end 
  
  -- 返回消息。
  if @Returnstatus = 0 and @VerifyNum <> ''
  begin
    declare @MessageServiceTypeID int, @AccountNumber varchar(64), @InputString varchar(max)
    set @InputString = ''
    set @MessageServiceTypeID = 0
    set @AccountNumber = ''
    DECLARE CUR_MemberCardSubmit CURSOR fast_forward FOR
      select MessageServiceTypeID, AccountNumber from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1  
    OPEN CUR_MemberCardSubmit
    FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber
    WHILE @@FETCH_STATUS=0
    BEGIN
	  if isnull(@AccountNumber, '') <> '' 
	  begin
	    if @InputString <> ''
	      set @InputString = @InputString + '|' 
	    set @InputString = @InputString + 'COUPONNUMBER=' + @CouponNumber + ';VERIFYNUMBER=' + @VerifyNum	 
	       + ';MEMBERID=' + cast(@MemberID as varchar)	
     	   + ';MSGACCOUNT=' + @AccountNumber + ';MSGACCOUNTTYPE=' + cast(@MessageServiceTypeID as varchar)	   
	  end
      FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber   
    END
    CLOSE CUR_MemberCardSubmit 
    DEALLOCATE CUR_MemberCardSubmit   
   		  
    exec GenMessageJSONStr 'GenCouponVerifyNum', @InputString, @ReturnMessageStr  output      
  end  
  return @Returnstatus
    
end

GO
