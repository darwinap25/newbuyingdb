USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenerateTagNumber]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2017/09/01 18:50:49
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GenerateTagNumber]
  
  @batchtagcode         [varchar](64),
  @tagtypeid            int,
  @tagdesigncode		[varchar](64),
  @startingid           [bigint],
  @endingid             [bigint],
  @createdby            [int],
  @result               [int] OUTPUT 
  
  
AS
SET NOCOUNT ON;

BEGIN

    DECLARE @tagtypeprefix [varchar](64)
    DECLARE @tagnumber [varchar](64)
    DECLARE @counter [bigint]
    DECLARE @sequence [varchar](20)
    DECLARE @tagtypecode  varchar(64)
    DECLARE @createdon [datetime]
    DECLARE @batchtagid int
    DECLARE @result1 int
    
    SET @createdon = GETDATE()
    
    
    SELECT @tagtypecode = [c].[CouponTypeCode],
            @tagtypeprefix = [c].[QRCodePrefix]            
    FROM [dbo].[CouponType] c
    WHERE [c].[CouponTypeID] = @tagtypeid
    
    
    EXEC [dbo].[GenerateBatchTag] 
        @tagtypeid, 
        @tagtypecode, 
		@tagdesigncode,
        @startingid,
        @endingid, 
        @createdby, 
        @batchtagcode,
        @batchtagid = @result1 OUTPUT 
    
    SET  @counter = @startingid
    WHILE @counter <= @endingid
        BEGIN
            SET @sequence = '0000000000' + CAST(@counter AS varchar)
            SET @tagnumber = @tagtypeprefix + RIGHT(@sequence, 10)
            
            
            INSERT INTO [dbo].[Coupon] 
            (
                [CouponNumber], 
                [CouponTypeID], 
                [CouponIssueDate], 
                [CouponExpiryDate], 
                [BatchCouponID], 
                [Status], 
                [CreatedOn], 
                [UpdatedOn], 
                [CreatedBy], 
                [UpdatedBy]
            )

            VALUES
            (
                @tagnumber,
                @tagtypeid,
                @createdon,
                '2056-12-31',
                @result1,
                0,
                @createdon,
                @createdon,
                @createdby,
                @createdby
                
            )
                  
            SET @counter = @counter + 1
        END
        
        SET @counter = @counter - 1 
        
        UPDATE [dbo].[CouponType]
        SET [LastSequenceGenerated] = @counter
        WHERE [CouponTypeCode] = @tagtypecode
        
        SET @result = @counter
        
END





GO
