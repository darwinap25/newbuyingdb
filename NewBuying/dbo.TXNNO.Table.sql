USE [NewBuying]
GO
/****** Object:  Table [dbo].[TXNNO]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TXNNO](
	[StoreCode] [varchar](64) NOT NULL,
	[RegisterCode] [varchar](64) NOT NULL,
	[BusDate] [date] NOT NULL,
	[LastNo] [int] NOT NULL,
 CONSTRAINT [PK_TXNNO] PRIMARY KEY CLUSTERED 
(
	[StoreCode] ASC,
	[RegisterCode] ASC,
	[BusDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TXNNO', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS机编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TXNNO', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TXNNO', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后的序号。（已经被使用的）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TXNNO', @level2type=N'COLUMN',@level2name=N'LastNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单号表。 用于产生交易单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TXNNO'
GO
