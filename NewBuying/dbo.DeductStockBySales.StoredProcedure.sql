USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DeductStockBySales]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[DeductStockBySales]
  @UserID				         INT,	            -- 操作员ID
  @SalesShipOrderNumber          VARCHAR(64),       -- 出库单号码
  @TxnNo                         VARCHAR(64),       -- 销售单号码
  @PickupStoreID                 INT,               -- 提货的storeID
  @IsReserve                     INT=0              -- 0: 不是reserve库存,直接挂机销售单产生拣货单. 1: 是有预留库存. 需要对照SalesReserverDetail表.
AS
/****************************************************************************
**  Name : DeductStockBySales   
**  Version: 1.0.0.1
**  Description : 根据SalesShipping单来扣库存。(只在approve trigger中调用,不检查shipping单状态)
exec DeductStockBySales 1, 'SSO000000000007','KTXNNO000000440',
**  Created by: Gavin @2016-10-19 
**  Created by: Gavin @2016-10-24 (ver 1.0.0.1) 修正bug
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @TxnDate DATETIME, @StoreCode varchar(64)
  SET @SalesShipOrderNumber = ISNULL(@SalesShipOrderNumber, '')
  SET @TxnNo = ISNULL(@TxnNo, '')
  SET @PickupStoreID = ISNULL(@PickupStoreID, 0)
  SET @IsReserve = ISNULL(@IsReserve, 0)
  
  SELECT @Busdate = Busdate, @Txndate = Txndate FROM SALES_H WHERE TransNum = @TxnNo
  IF EXISTS(SELECT * FROM Ord_SalesShipOrder_D WHERE SalesShipOrderNumber = @SalesShipOrderNumber AND ISNULL(OrderQty, 0) > 0) 
  BEGIN
    IF @IsReserve = 1
	BEGIN     
      INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	  SELECT 4, @PickupStoreID, 'R', D.ProdCode, @TxnNo, '', @Busdate, @Txndate,
	         ISNULL(T.OnhandQty,0), (-1 * D.OrderQty), ISNULL(T.OnhandQty,0) - D.OrderQty, null, '', GETDATE(), @UserID 
	  FROM Ord_SalesShipOrder_D D 
	  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @PickupStoreID AND StockTypeCode = 'R' ) T ON D.ProdCode = T.ProdCode 
	  WHERE D.SalesShipOrderNumber = @SalesShipOrderNumber
	END ELSE
	BEGIN
      INSERT INTO STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty, ActQty, CloseQty, SerialNo, ApprovalCode,CreatedOn,CreatedBy)
	  SELECT 4, @PickupStoreID, 'G', D.ProdCode, @SalesShipOrderNumber, @TxnNo, @Busdate, @Txndate,
	         ISNULL(T.OnhandQty,0), (-1 * D.OrderQty), ISNULL(T.OnhandQty,0)- D.OrderQty, null, '',  GETDATE(), @UserID 
	  FROM Ord_SalesShipOrder_D D 
	  LEFT JOIN (SELECT * FROM STK_STOCKONHAND WHERE StoreID = @PickupStoreID AND StockTypeCode = 'G' ) T ON D.ProdCode = T.ProdCode
	  WHERE D.SalesShipOrderNumber = @SalesShipOrderNumber 
	END
  END
  RETURN 0
END

GO
