USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AddPickupDetail_Coupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[AddPickupDetail_Coupon]
  @UserID                   int,             --操作员ID
  @CouponPickingNumber      varchar(64),     --单号
  @FilterType               int,             --筛选类型。1：batchID+数量。2：CouponUID，3：起始couponnumber+数量 
  @CouponTypeID             int,             --优惠劵类型ID
  @QTY                      int,             --要求的数量。 如果为0/null，那么就根据订单取。
  @BatchID                  int,             --优惠劵批次ID
  @CouponUID                varchar(512),    --CouponUID, 使用CouponUID时，一个UID一条记录。
  @StartCouponNumber        varchar(64),      --卡创建单的号码
  @damaged                  [int],
  @returned                 [int]
AS
/****************************************************************************
**  Name : AddPickupDetail_Coupon
**  Version: 1.0.0.3
**  Description : 根据输入的coupon筛选条件和pickupo单号，选择
**  example :
  declare @a int  , @CouponOrderFormNumber varchar(64)
  set @CouponOrderFormNumber = 'COPO00000000139'
  exec @a = AddPickupDetail_Coupon 1, @CouponOrderFormNumber, 3, 177, 10, 0, '', '4444400001'   
  print @a  
  delete from Ord_CouponPicking_D where keyid >= 277
  select * from Ord_CouponOrderForm_H
  select * from Ord_CouponOrderForm_D
  select * from Ord_CouponPicking_H
  select * from Ord_CouponPicking_D
  select * from coupon where couponnumber = 'CN0200006'
**  Created by: Gavin @2012-09-21
**  Modify by: Gavin @2012-09-26 (ver 1.0.0.1) InsertPickupDetail_Coupon增加Order form 的orderqty 传入
**  Modify by: Gavin @2012-10-09 (ver 1.0.0.2) 1.batch,  整个batch添加， 不管order 是多少数量的.  3.couponnumber+数量。  同样 不管order 是多少数量的
**  Modify by: Gavin @2014-07-22 (ver 1.0.0.3) 第3种方式时，允许不输入startCouponNumber，从Coupon表中读取。
**
****************************************************************************/
begin
  declare @CouponOrderFormNumber varchar(64), @OrderQty int, @PickupQty int, @CouponNumber varchar(64), @BrandID int
  declare @i int, @EndCouponNumber varchar(64), @PrevCouponNumber varchar(64), @A int
  declare @FirstCoupon varchar(64), @EndCoupon varchar(64)
  
  select @CouponOrderFormNumber = ReferenceNo from Ord_CouponPicking_H where CouponPickingNumber = @CouponPickingNumber
  select @OrderQty = sum(isnull(CouponQty,0)) from Ord_CouponOrderForm_D 
    where CouponOrderFormNumber = @CouponOrderFormNumber and CouponTypeID = @CouponTypeID
    group by CouponTypeID
  select @PickupQty = sum(isnull(PickQTY,0)) from Ord_CouponPicking_D 
     where CouponPickingNumber = @CouponPickingNumber and CouponTypeID = @CouponTypeID
    group by CouponTypeID
  select @CouponNumber = CouponNumber from couponuidmap where CouponUID = @CouponUID

  set @A = -2  
  set @OrderQty = isnull(@OrderQty, 0)
  set @PickupQty = isnull(@PickupQty, 0)
  set @QTY = isnull(@QTY, 0)

--  if @PickupQty < @OrderQty
  begin
--    set @PickupQty = @OrderQty - @PickupQty
--    if @QTY = 0
--      set @QTY = @PickupQty
--    if @QTY < @PickupQty
--      set @PickupQty = @QTY
    if @FilterType = 1
    begin
      select @PickupQty = count(*) from Coupon where BatchCouponID = @BatchID and CouponTypeID = @CouponTypeID and Status = 0  and StockStatus IN (2,@damaged,4,@returned)
	  select Top 1 @StartCouponNumber = CouponNumber from Coupon where BatchCouponID = @BatchID and CouponTypeID = @CouponTypeID and Status = 0 and StockStatus IN (2,@damaged,4,@returned)
	  order by CouponNumber
	  exec @A = InsertPickupDetail_Coupon @CouponPickingNumber, @CouponTypeID, @StartCouponNumber, @OrderQty, @PickupQty, 1, @damaged, @returned
    end else if @FilterType = 2
    begin
	  exec @A = InsertPickupDetail_Coupon @CouponPickingNumber, @CouponTypeID, @CouponNumber, @OrderQty, 1, 1, @damaged, @returned
    end else if @FilterType = 3
    begin
      if isnull(@StartCouponNumber, '') = ''
        select Top 1 @StartCouponNumber = CouponNumber from Coupon where CouponTypeID = @CouponTypeID and Status = 0 and PickupFlag = 0 and StockStatus = 2
      set @PickupQty = isnull(@QTY, 0)  
      exec @A = InsertPickupDetail_Coupon @CouponPickingNumber, @CouponTypeID, @StartCouponNumber, @OrderQty, @PickupQty, 1, @damaged, @returned   
    end    
  end 
  
  if @A = 0
  begin
  -- update coupon的 PickupFlag
    DECLARE CUR_PickupCoupon_Detail CURSOR fast_forward for
      select FirstCouponNumber, EndCouponNumber from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber
	OPEN CUR_PickupCoupon_Detail
    FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon
    WHILE @@FETCH_STATUS=0 
    BEGIN 
      update coupon set PickupFlag = 1, StockStatus = 4 where CouponNumber >= @FirstCoupon and  CouponNumber <= @EndCoupon and CouponTypeID = @CouponTypeID
	  FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon
	END
    CLOSE CUR_PickupCoupon_Detail 
    DEALLOCATE CUR_PickupCoupon_Detail 
  end      
  return @A
end



GO
