USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCouponDetails]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\darwin.pasco
 * Created At: 2017/10/06 11:35:54
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[GetCouponDetails]
  
  @CouponNumber                    [varchar](512)

  
/************************************
v 1.0 - Created by Darwin 10/06/2017

- Get all Coupon Details


************************************/  
AS
SET NOCOUNT ON;


BEGIN

    SELECT 
        [c].[CouponNumber],
        [ct].[CouponTypeCode],
        [ct].[CouponTypeName1],
        [ct].[CouponTypeName2],
        [ct].[CouponTypeName3],
        [cb].[ProdCode]
    FROM [dbo].[Coupon] c
        INNER JOIN [dbo].[CouponType] ct
            ON [ct].[CouponTypeID] = [c].[CouponTypeID]
        INNER JOIN [dbo].[CouponTypeExchangeBinding] cb
            ON [cb].[CouponTypeID] = [ct].[CouponTypeID]
    WHERE [c].[CouponNumber] = @CouponNumber

END





GO
