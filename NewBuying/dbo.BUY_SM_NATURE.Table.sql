USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_SM_NATURE]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BUY_SM_NATURE](
	[TransType] [int] NOT NULL,
	[TransTypeName1] [nvarchar](512) NULL,
	[TransTypeName2] [nvarchar](512) NULL,
	[TransTypeName3] [nvarchar](512) NULL,
	[Nature] [nvarchar](512) NULL,
 CONSTRAINT [PK_BUY_SM_NATURE] PRIMARY KEY CLUSTERED 
(
	[TransType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键。 交易类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SM_NATURE', @level2type=N'COLUMN',@level2name=N'TransType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SM_NATURE', @level2type=N'COLUMN',@level2name=N'TransTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SM_NATURE', @level2type=N'COLUMN',@level2name=N'TransTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SM_NATURE', @level2type=N'COLUMN',@level2name=N'TransTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SM_NATURE', @level2type=N'COLUMN',@level2name=N'Nature'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型定义表。
（固定交易类型）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SM_NATURE'
GO
