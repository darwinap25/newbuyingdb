USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DeleteMemberRecord]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[DeleteMemberRecord]
  @MemberID             int,               -- 会员ID
  @MemberRegisterMobile varchar(64),       -- 注册号码
  @MemberMobilePhone    nvarchar(512),     -- 手机号码
  @MemberEmail          nvarchar(512)      -- email邮箱
AS
/****************************************************************************
**  Name : DeleteMemberRecord  
**  Version: 1.0.0.0
**  Description : 删除指定会员记录（提供给API ，Demo UAT使用）
**  Parameter :
  declare @a int
  exec @a = DeleteMemberRecord 0,'','','123@123.com'
  print @a
  select * from member
**  Created by: Gavin @2014-06-20
**
****************************************************************************/
begin
  declare @MID int
  select @MID = MemberID from Member where (MemberID=@MemberID or ISNULL(@MemberID,0)=0) 
    and (MemberRegisterMobile=@MemberRegisterMobile or ISNULL(@MemberRegisterMobile,'')='')
    and (MemberMobilePhone=@MemberMobilePhone or ISNULL(@MemberMobilePhone,'')='')
    and (MemberEmail=@MemberEmail or ISNULL(@MemberEmail,'')='')
  if isnull(@MID,0) > 0
  begin  
    delete from MemberMessageAccount where MemberID = @MID
    delete from Member where MemberID = @MID
  end    
  return 0
end

GO
