USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardUIDMap]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardUIDMap](
	[CardUID] [varchar](512) NOT NULL,
	[ImportCardNumber] [varchar](64) NULL,
	[BatchCardID] [int] NULL,
	[CardGradeID] [int] NULL,
	[CardNumber] [varchar](64) NULL,
	[Status] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_CARDUIDMAP] PRIMARY KEY CLUSTERED 
(
	[CardUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card导入号码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardUIDMap', @level2type=N'COLUMN',@level2name=N'CardUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardUIDMap', @level2type=N'COLUMN',@level2name=N'ImportCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card的 batchID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardUIDMap', @level2type=N'COLUMN',@level2name=N'BatchCardID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定的CardGradeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardUIDMap', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绑定的CardNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardUIDMap', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。0：无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardUIDMap', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card号码和UID对照表。
（UID：客户提供的卡号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardUIDMap'
GO
