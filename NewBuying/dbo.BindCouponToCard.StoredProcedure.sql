USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BindCouponToCard]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BindCouponToCard]
  @CardNumber                         VARCHAR(64),
  @CouponUID                          VARCHAR(64)
AS
/****************************************************************************
**  Name : BindCouponToCard
**  Version: 1.0.0.0
**  Description :  
declare @a int
exec @a=BindCouponToCard '2','01200000505063813'
print @a
select * from card where cardnumber = '2'
**  Created by:  Gavin @2017-03-28
****************************************************************************/
BEGIN
  DECLARE @CouponNumber VARCHAR(64), @Status INT, @CouponTypeID INT, @CouponExpiryDate DATETIME, @MemberID INT,
          @NewCouponExpiryDate DATETIME, @BusDate DATETIME, @TxnDate DATETIME, @CouponAmount MONEY, @BindCardNumber VARCHAR(64) 
  SELECT @MemberID = MemberID FROM Card WHERE CardNumber = @CardNumber
  SELECT @CouponNumber = CouponNumber FROM CouponUIDMap WHERE CouponUID = @CouponUID
  IF ISNULL(@CouponNumber, '') = ''
    SET @CouponNumber = @CouponUID
  SELECT @Status = C.Status, @CouponTypeID = C.CouponTypeID, @CouponAmount = C.CouponAmount,
      @CouponExpiryDate = C.CouponExpiryDate, @BindCardNumber = CardNumber
    FROM Coupon C LEFT JOIN CouponType T ON C.CouponTypeID = T.CouponTypeID
  WHERE C.CouponNumber = @CouponNumber

  IF @CouponNumber = ''
    RETURN -19
  IF ISNULL(@Status,0) <> 1 
    RETURN -20
  IF ISNULL(@MemberID, 0) = 0
    RETURN -2
  IF ISNULL(@BindCardNumber,'') <> '' 
    RETURN -20
  
  SELECT TOP 1 @BusDate=BusDate FROM SODEOD WHERE EOD=0 and SOD=1
  SET @BusDate=isNull(@BusDate,getdate())
  SET @TxnDate=getdate()

  IF isnull(@CouponNumber, '') <> ''
  BEGIN
    EXEC CalcCouponNewExpiryDate @CouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
    INSERT INTO Coupon_Movement
       (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
        BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
        OrgExpirydate, OrgStatus, NewStatus)    
    VALUES ( 
        32, @CardNumber, @CouponNumber, @CouponTypeID, '', 0, '', @CouponAmount, 0, @CouponAmount,
        @BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, '', null, '', '',
        @CouponExpiryDate, 1, 2 ) 
  END

  RETURN 0
END

GO
