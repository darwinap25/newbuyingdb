USE [NewBuying]
GO
/****** Object:  Table [dbo].[EarnCouponRule]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EarnCouponRule](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[ExchangeType] [int] NOT NULL DEFAULT ((1)),
	[ExchangeRank] [int] NOT NULL DEFAULT ((0)),
	[ExchangeAmount] [money] NOT NULL DEFAULT ((0)),
	[ExchangePoint] [int] NOT NULL DEFAULT ((0)),
	[ExchangeCouponTypeID] [int] NOT NULL DEFAULT ((0)),
	[ExchangeCouponCount] [int] NOT NULL DEFAULT ((1)),
	[ExchangeConsumeRuleOper] [int] NULL,
	[ExchangeConsumeAmount] [money] NULL,
	[CardTypeIDLimit] [int] NULL,
	[CardGradeIDLimit] [int] NULL,
	[CardTypeBrandIDLimit] [int] NULL,
	[StoreBrandIDLimit] [int] NULL,
	[StoreGroupIDLimit] [int] NULL,
	[StoreIDLimit] [int] NULL,
	[MemberBirthdayLimit] [int] NULL DEFAULT ((0)),
	[MemberSexLimit] [int] NULL,
	[MemberAgeMinLimit] [int] NULL,
	[MemberAgeMaxLimit] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL DEFAULT ((1)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[DeviceID] [int] NULL,
 CONSTRAINT [PK_EARNCOUPONRULE] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'需要获取的couponID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换方式（默认1）：
1、金额兑换
2、积分兑换
3、金额+积分兑换
4、coupon兑换
5、消费金额兑换
6、充值金额兑换
7、免费兑换' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换规则排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangeRank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换需要的金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangeAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换需要的积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangePoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换需要的CouponTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangeCouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换需要的指定的CouponTypeID的 数量。 默认为1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangeCouponCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换需要的消费金额的 计算方式设置：
0：每当消费ExchangeConsumeAmount指定的金额，就能获得指定的coupon
1：当消费金额大于等于ExchangeConsumeAmount指定的金额，就能获得指定的coupon
2：当消费金额小于等于ExchangeConsumeAmount指定的金额，就能获得指定的coupon

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangeConsumeRuleOper'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兑换需要的消费金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'ExchangeConsumeAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡类型限制。  NULL或空表示不做限制。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'CardTypeIDLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡级别限制。  NULL或空表示不做限制。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'CardGradeIDLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型的品牌限制：
NUll或空：不做限制。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'CardTypeBrandIDLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺的品牌限制：
NUll或空：不做限制。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'StoreBrandIDLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺组限制：
Null或空：不加限制。  填写后即只有此店铺组登录才可以看到并领取。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'StoreGroupIDLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺限制：
Null或空：不加限制。  填写后即只有此店铺登录才可以看到并领取。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'StoreIDLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生日限制：
null 或 0： 不加限制。 1：生日当月。 2：生日当周。 3：生日当日。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'MemberBirthdayLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别设置。
null或0：不加限制。 1：男性。 2：女性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'MemberSexLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最低年龄限制：
null或者0：不加限制。 其他：最低年龄（包括）。（当年 - 生年 + 1 = 年龄）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'MemberAgeMinLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最高年龄限制:
null或者0：不加限制。 其他：最高年龄（包括）。（当年 - 生年 + 1 = 年龄）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'MemberAgeMaxLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态.0:无效。1：有效。 默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有关设备的限制。0表示不限制。否则为指定的设备ID。目前这里填的值是iBeaconGroupID。可以后扩展' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule', @level2type=N'COLUMN',@level2name=N'DeviceID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'获取优惠劵的规则表
@2015-01-14 增加字段： DeviceID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EarnCouponRule'
GO
