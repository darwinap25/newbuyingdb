USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_VENDOR]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_VENDOR](
	[VendorID] [int] IDENTITY(1,1) NOT NULL,
	[VendorCode] [varchar](64) NOT NULL,
	[VendorName1] [nvarchar](512) NULL,
	[VendorName2] [nvarchar](512) NULL,
	[VendorName3] [nvarchar](512) NULL,
	[VendorAddress1] [nvarchar](512) NULL,
	[VendorAddress2] [nvarchar](512) NULL,
	[VendorAddress3] [nvarchar](512) NULL,
	[VendorNote] [nvarchar](512) NULL,
	[PreferFlag] [int] NOT NULL DEFAULT ((0)),
	[Contact] [nvarchar](512) NULL,
	[ContactPosition] [nvarchar](512) NULL,
	[ContactTel] [nvarchar](512) NULL,
	[ContactFax] [nvarchar](512) NULL,
	[ContactMobile] [nvarchar](512) NULL,
	[ContactEmail] [nvarchar](512) NULL,
	[Terms] [int] NULL,
	[Limit] [decimal](12, 4) NULL,
	[Shipment] [nvarchar](512) NULL,
	[PayTerm] [nvarchar](512) NULL,
	[AccountCode] [nvarchar](512) NULL,
	[Oversea] [int] NULL DEFAULT ((0)),
	[InTax] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[NonOrder] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_VENDOR] PRIMARY KEY CLUSTERED 
(
	[VendorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorAddress1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorAddress2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorAddress3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'VendorNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否受欢迎供应商. 1：是。0：不是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'PreferFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人职位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'ContactPosition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'ContactTel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'ContactFax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'ContactMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'ContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'付款天数设定' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'Terms'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'信用额度限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'Limit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货运' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'Shipment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'付款条款' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'PayTerm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'AccountCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否海外供应商. 1：是的。 0：不是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'Oversea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'税率（供应商提供）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'InTax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否看订货供应商。 1：是的。 0：不是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR', @level2type=N'COLUMN',@level2name=N'NonOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_VENDOR'
GO
