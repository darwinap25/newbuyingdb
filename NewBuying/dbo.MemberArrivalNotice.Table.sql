USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberArrivalNotice]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberArrivalNotice](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[ProdCode] [varchar](64) NULL,
	[NoticeEmail] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_MEMBERARRIVALNOTICE] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberArrivalNotice] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberArrivalNotice', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberArrivalNotice', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberArrivalNotice', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberArrivalNotice', @level2type=N'COLUMN',@level2name=N'NoticeEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员订货到货通知' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberArrivalNotice'
GO
