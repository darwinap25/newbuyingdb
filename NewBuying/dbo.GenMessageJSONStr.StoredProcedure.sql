USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenMessageJSONStr]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GenMessageJSONStr]
  @ID                   varchar(1023),    -- 识别ID, 例如:LostPassword
  @InputStr				varchar(max),    -- 输入内容。
  @ReturnJSONStr         varchar(max) output    -- 返回的XML字符串。
AS
/****************************************************************************
**  Name : GenMessageJSONStr
**  Version: 1.0.0.2
**  Description : 根据输入的字符串,产生符合要求的JSON格式字符串
**
@InputStr format:
  MPASSWORD=1234;MEMBERID=1;MSGACCOUNT=32424;MSGACCOUNTTYPE=1|MPASSWORD=1234;MEMBERID=1;MSGACCOUNT=32424;MSGACCOUNTTYPE=2
@@ReturnJSONStr format (固定结构: 功能ID -> 下面具体的内容):
{
"LostPassword":
 [
  {
   "MPASSWORD":"1234",
   "MEMBERID":"13801605014",
   "MSGACCOUNT":"1234",
   "MSGACCOUNTTYPE":"1234"
  },
  {
   "MPASSWORD":"1234",
   "MEMBERID":"13801605014",
   "MSGACCOUNT":"1234",
   "MSGACCOUNTTYPE":"1234"
  }
 ]
}
declare @ReturnXMLstr  varchar(max)
exec GenMessageJSONStr 'LostPassword', 'MPASSWORD=1234;MEMBERID=1;MSGACCOUNT=32424;MSGACCOUNTTYPE=1|MPASSWORD=1234;MEMBERID=1;MSGACCOUNT=32424;MSGACCOUNTTYPE=2', @ReturnXMLstr output
print @ReturnXMLstr
**  Created by:  Gavin @2013-08-19
**  Modify by Gavin @2013-09-04 (ver 1.0.0.1)，输入的字符串属性之间分隔符用 ";" 替换 ","。 因为有些属性内容中用到 "," 分割  
**  Modify by Gavin @2013-10-11 (ver 1.0.0.2)，增加特定字符串:JSON， 表示后面内容是json格式字符串，不需要转换，直接添加上去  
**
****************************************************************************/
begin
  declare @JSONStr varchar(max), @TempStr varchar(max), @POS int, @ConditionTempStr varchar(max),
          @ConditionStr varchar(1023), @Parameter varchar(1023), @Value varchar(1023), @StrLen int
  
  set @ReturnJSONStr = ''
  if isnull(@InputStr, '') = ''
  begin
    return 0
  end
           
  set @JSONStr = '{"' + @ID + '":'
  set @JSONStr = @JSONStr + '['

  set @TempStr = RTrim(LTrim(@InputStr))
  while len(@TempStr) > 0
  begin
    set @JSONStr = @JSONStr + '{'
    set @POS = CharIndex('|', @TempStr)
    if @POS > 0 
    begin
      set @ConditionTempStr = substring(@TempStr, 1, @POS - 1)
	  set @TempStr = substring(@TempStr, @POS + 1, len(@TempStr) - @POS)
    end  else
    begin
      set @ConditionTempStr = @TempStr
	  set @TempStr = ''
    end
    
    set @ConditionTempStr = RTrim(LTrim(@ConditionTempStr))
    while len(@ConditionTempStr) > 0
    begin
      set @POS = CharIndex(';', @ConditionTempStr)
      if @POS > 0 
      begin
        set @ConditionStr = substring(@ConditionTempStr, 1, @POS - 1)
	    set @ConditionTempStr = substring(@ConditionTempStr, @POS + 1, len(@ConditionTempStr) - @POS)
      end  else
      begin
        set @ConditionStr = @ConditionTempStr
	    set @ConditionTempStr = ''
      end
          
      if len(@ConditionStr) > 0
      begin
        set @POS = CharIndex('=', @ConditionStr)
        set @Parameter = substring(@ConditionStr, 1, @POS - 1)
	    set @Value = substring(@ConditionStr, @POS + 1, len(@ConditionStr) - @POS)
	    if @Parameter = 'JSON'
	      set @JSONStr = @JSONStr + @Value + ','
	    else  
	      set @JSONStr = @JSONStr + '"' + @Parameter + '":"' + @Value + '",'
      end
    end
--print  'GenMessageJSONStr'
--print @InputStr
--print @JSONStr
--print Len(@JSONStr)
     
    set @StrLen = Len(@JSONStr)
    if right(@JSONStr, 1) = ',' 
      set @JSONStr = substring(@JSONStr, 1, @StrLen - 1)
        
    set @JSONStr = @JSONStr + '},'    
  end 

  set @StrLen = Len(@JSONStr)
  if right(@JSONStr, 1) = ',' 
    set @JSONStr = substring(@JSONStr, 1, @StrLen - 1) 
          
  set @JSONStr = @JSONStr + ']}'
  set @ReturnJSONstr = @JSONStr
  return 0
end

GO
