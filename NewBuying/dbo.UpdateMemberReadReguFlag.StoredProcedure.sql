USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberReadReguFlag]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[UpdateMemberReadReguFlag]
  @MemberID         int,
  @CardNumber       varchar(64)=''
AS
/****************************************************************************
**  Name : UpdateMemberReadReguFlag
**  Version: 1.0.0.4
**  Description : 置Member的 readReguflag标志。
**
**  Parameter :
  declare @a int, @TxnNo          varchar(512)
  exec @a = UpdateMemberReadReguFlag '1'
  print @a  
**  Created by: Gavin @2012-11-13
**  Modify by: Gavin @2013-04-28 (ver 1.0.0.1) 更新ReadReguFlag标志，更新逻辑： 确认每个用户第一次更新的是阅读条款（ReadReguFlag=1），第二次更新的是玩游戏（ReadReguFlag=3）
**  Modify by: Gavin @2013-09-09 (ver 1.0.0.2) 当ReadReguFlag第一次从1 到 3 时，奖励推荐人
**  Modify by: Gavin @2013-09-26 (ver 1.0.0.3) 当ReadReguFlag第一次从1 到 3 时，增加新会员注册奖励，需要增加cardnumber参数传入
**  Modify by: Gavin @2013-10-14 (ver 1.0.0.4) 记录操作到UserAction_Movement表
**
****************************************************************************/
begin
  declare @ReadReguFlag int, @ReferCardNumber varchar(64)
  declare @temp varchar(200)
  
  select @ReadReguFlag = ReadReguFlag, @ReferCardNumber = ReferCardNumber  from member where MemberID = @MemberID  
  set @ReadReguFlag = isnull(@ReadReguFlag, 0)
  if @ReadReguFlag = 0
    set @ReadReguFlag = 1
  else if @ReadReguFlag = 1
  begin
    set @ReadReguFlag = 3  
    -- 赠送推荐人奖励
    if isnull(@ReferCardNumber, '') <> ''
    begin
      set @temp = 'REF_' + @ReferCardNumber + '_' + cast(@MemberID as varchar)
	  exec DoSVAReward @ReferCardNumber,  @temp, 1
    end
    ------
    -- 新注册奖励    
    if isnull(@CardNumber, '') <> ''
    begin      
      set @temp = 'NEW_' + @CardNumber + '_' + cast(@MemberID as varchar)
	  exec DoSVAReward @CardNumber, @temp, 2
    end
    ------    
  end  
  update Member set ReadReguFlag = @ReadReguFlag where MemberID = @MemberID  
  if @@ROWCOUNT = 0
    return -1
  else
  begin
    exec RecordUserAction @MemberID, 2, @CardNumber, '', 'Update Member MemberReadReguFlag', '', '', '', @MemberID  
    return 0
  end      
end

GO
