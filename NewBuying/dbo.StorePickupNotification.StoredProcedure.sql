USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[StorePickupNotification]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[StorePickupNotification]                

  @SalesShipOrderNumber          VARCHAR(64),        -- 單號碼
  @TxnNo                         VARCHAR(64) output, -- 交易號
  @MemberID                      int output,         -- 會員號
  @MemberEmail                   VARCHAR(64) output, -- 會員電郵
  @Language                      VARCHAR(10) output

AS

/****************************************************************************
**  Name : StorePickupNotification 
**  Version: 1.0.0.0
**  Description : 透過SalesShipOrderNumber獲取對應的TxnNo, 以及會員訊息
exec StorePickupNotification 'SSO000000000393', '','','',''
**  Created by : Ivan @2017-03-17
**  Modified by: Ivan @2017-03-31 - 把SP從BuyingDB轉移到SVADB
****************************************************************************/

BEGIN

	SELECT @TxnNo=TxnNo , @MemberID=MemberID FROM Ord_SalesShipOrder_H WHERE SalesShipOrderNumber=@SalesShipOrderNumber;
	
	SELECT @MemberEmail=M.MemberRegisterMobile, @Language=LM.LanguageAbbr 
	FROM Member M
		JOIN LanguageMap LM ON M.MemberDefLanguage=LM.KeyID 
	WHERE M.MemberID=@MemberID;
	
/*
	print @SalesShipOrderNumber
	print @TxnNo
	print @MemberID
	print @MemberEmail
	print @Language
*/

END
GO
