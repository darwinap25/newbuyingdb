USE [NewBuying]
GO
/****** Object:  Table [dbo].[S_Log]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[S_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[datetime] [datetime] NOT NULL,
	[loginfo] [varchar](500) NULL,
	[Particular] [varchar](1000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
