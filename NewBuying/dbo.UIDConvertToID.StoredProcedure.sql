USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UIDConvertToID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UIDConvertToID]
  @InputUID varchar(20),
  @ResultID varchar(20) output
AS
/****************************************************************************
**  Name : UIDConvertToID
**  Version: 1.0.0.0
**  Description : 输入实体卡的十六进制UID，转换成实体卡上印刷的加密ID。
**  Parameter :
**         @InputUID varchar(20)		-- 十六进制UID
**         @ResultID:varchar(20) 
**
**  Created by: Gavin @2011-03-18
**  Modifiy by: Gavin @2011-06-14 返回的laseID为18位长度，小于18位，则在前面补0。如果大于18位，则计算有错误。
**
****************************************************************************/
BEGIN
  set @ResultID = @InputUID
  declare @ss bigint, @TempUID varchar(20)     
  set @ss = 72057594037927935   -- hardcode 的转换参数.
  
  set @TempUID = RTrim(LTrim(@InputUID))
  
  if len(@TempUID) = 14 
  begin    
    declare @m bigint
    declare @runSqlStr nvarchar(200)    
    set @TempUID = '0x' + @TempUID
    select @runSqlStr=N'select @n=convert(bigint, '+ @TempUID + ')'
    exec sp_executesql @runSqlStr,N'@n bigint output',@m output 
    select @ResultID = convert(varchar(20), @ss - @m)
    set @ResultID = Right('00000000000000000000' + RTrim(LTrim(@ResultID)), 18)
  end  
END

GO
