USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetLogisticsProvider]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetLogisticsProvider]
  @LogisticsProviderCode		 varchar(64),      -- 指定的物流供应商编码
  @LanguageAbbr			         varchar(20)=''	   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1  
AS
/****************************************************************************
**  Name : GetLogisticsProvider  
**  Version: 1.0.0.0
**  Description : 获得指定的LogisticsProvider信息
**  Parameter :
**   exec GetLogisticsProvider '', ''
select * from LogisticsProvider
**  Created by: Gavin @2016-01-14
**
****************************************************************************/
begin
  declare @Language int, @StateofCoupon int, @CouponRealNumber varchar(512) --, @ConditionStr nvarchar(400)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @LogisticsProviderCode = isnull(@LogisticsProviderCode, '')
  select LogisticsProviderID, LogisticsProviderCode, 
      case when @Language = 2 then ProviderName2 when @Language = 3 then ProviderName3 else ProviderName1 end as ProviderName,
	  ProviderContact, ProviderContactTel, ProviderContactEmail, OrdQueryAddr, Remark, CreatedOn, UpdatedOn
   from LogisticsProvider
   where  LogisticsProviderCode = @LogisticsProviderCode or @LogisticsProviderCode = ''

  return 0
end

GO
