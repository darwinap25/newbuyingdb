USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckStockOnhand]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[CheckStockOnhand]
  @CheckType                INT,             --  类型。  1：交易单。 2：salespickup 单。 3： 货品编号. 4: productstyle
  @OrderNumber              VARCHAR(64),     --  单号
  @QTY                      INT=0,           --  查询的数量. 当@Type=3 时需要使用。 如果qty=0， 则返回库存情况。 如果去qty >0。  则库存不足时才返回，正常不不返还数据集。
  @StoreCode                VARCHAR(64)=0    --  查询的storecode。 当@Type=3 时需要使用。 其他情况，storecode将从order中读取。当查询货品时
AS
/****************************************************************************
**  Name : CheckStockOnhand   
**  Version: 1.0.0.3
**  Description :  检查库存
sp_helptext CheckStockOnhand
Declare @a int
exec @a =CheckStockOnhand 0, '101216A701890104', 1, 'ECO'
print @a

select * from buy_productstyle where prodcode = '601216C002972100'
601216C002972800
601216C002974400
select * from buy_store where storecode = '8888'
select * from stk_stockonhand where storeid = 162 and prodcode = '601216C002972800'
**  Created by: Gavin @2016-10-20  
**  Modify by: Gavin @2016-10-21 (ver 1.0.0.1) 修正查询单个货品没有库存时的错误。 
                                               @Type g改名为 @CheckType
**  Modify by: Gavin @2016-10-27 (ver 1.0.0.2) vinnce要求加上hardcode部分: 如果输入的storecode是 ECO,则检查 ECO+WH 两个库存的数量.
**  Modify by: Gavin @2016-10-27 (ver 1.0.0.3) 增加type=4 的情况。 输入的是productstyle。                      
****************************************************************************/
BEGIN
  SET NOCOUNT ON
  DECLARE @CheckStoreID INT, @CheckStoreCode VARCHAR(64)
  DECLARE @A TABLE(Product VARCHAR(64), OrderQty INT, OnhandQTY INT)
  DECLARE @WHStoreID  INT
  DECLARE @OQTY INT, @WHQTY INT

  SELECT @WHStoreID = StoreID FROM BUY_STORE WHERE StoreCode = 'WH'

  SET @CheckType = ISNULL(@CheckType, 0)
  IF @CheckType = 1
  BEGIN
    SELECT @CheckStoreCode = CASE ISNULL(PickupStoreCode,'') WHEN '' THEN StoreCode ELSE PickupStoreCode END FROM Sales_H WHERE TransNum = @OrderNumber
	SELECT @CheckStoreID = StoreID FROM BUY_STORE WHERE StoreCode = @CheckStoreCode
	INSERT INTO @A
	SELECT A.ProdCode, A.TotalQty as OrderQty, B.OnhandQTY 
	FROM (
		SELECT ProdCode, TotalQty FROM Sales_D WHERE Collected = 0 AND TransNum = @OrderNumber
	    ) A LEFT JOIN (SELECT * FROM STK_StockOnhand 
	                     WHERE StockTypeCode = 'G' AND StoreID = @CheckStoreID) B ON A.ProdCode = B.ProdCode
    WHERE ISNULL(B.OnhandQty, 0) < A.TotalQty	  
  END
  IF @CheckType = 2
  BEGIN
    SELECT @CheckStoreCode = PickupLocation FROM Ord_SalesPickOrder_H WHERE SalesPickOrderNumber = @OrderNumber
	SELECT @CheckStoreID = StoreID FROM BUY_STORE WHERE StoreCode = @CheckStoreCode
	INSERT INTO @A
	SELECT A.ProdCode, A.OrderQty as OrderQty, B.OnhandQTY 
	FROM (
		SELECT ProdCode, OrderQty FROM Ord_SalesPickOrder_D WHERE SalesPickOrderNumber = @OrderNumber
	    ) A LEFT JOIN (SELECT * FROM STK_StockOnhand 
	                     WHERE StockTypeCode = 'G' AND StoreID = @CheckStoreID) B ON A.ProdCode = B.ProdCode
    WHERE ISNULL(B.OnhandQty, 0) < A.OrderQty
  END
  IF @CheckType = 3
  BEGIN  
	SELECT @CheckStoreID = StoreID FROM BUY_STORE WHERE StoreCode = @StoreCode

	IF @StoreCode = 'ECO'
	BEGIN
	  SELECT @OQTY = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @CheckStoreID AND ProdCode = @OrderNumber
	  SELECT @WHQTY = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @WHStoreID AND ProdCode = @OrderNumber
	  SET @OQTY = ISNULL(@OQTY, 0) + ISNULL(@WHQTY, 0)
	END ELSE
	BEGIN
	  SELECT @OQTY = OnhandQty FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @CheckStoreID AND ProdCode = @OrderNumber
	END

	IF ISNULL(@OQTY, 0) < @QTY
	BEGIN
      INSERT INTO @A
      VALUES(@OrderNumber, @QTY, ISNULL(@OQTY, 0))
	END
  END
  IF @CheckType = 4
  BEGIN
    SELECT @CheckStoreID = StoreID FROM BUY_STORE WHERE StoreCode = @StoreCode
    IF @StoreCode = 'ECO'
	BEGIN
	  INSERT INTO @A
	  SELECT P.ProdCode, -1, ISNULL(O.OnhandQty,0) + ISNULL(WO.OnhandQty,0) FROM 
	  (
	    SELECT * FROM BUY_Product WHERE ProdCode IN (SELECT ProdCode FROM BUY_PRODUCTSTYLE WHERE ProdCodeStyle = @OrderNumber)
	  ) P LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @CheckStoreID) O ON P.ProdCode = O.ProdCode
	      LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @WHStoreID) WO ON P.ProdCode = WO.ProdCode
	END ELSE
	BEGIN
	  INSERT INTO @A
	  SELECT P.ProdCode, -1, ISNULL(O.OnhandQty,0) FROM 
	  (
	    SELECT * FROM BUY_Product WHERE ProdCode IN (SELECT ProdCode FROM BUY_PRODUCTSTYLE WHERE ProdCodeStyle = @OrderNumber)
	  ) P LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StockTypeCode = 'G' AND StoreID = @CheckStoreID) O ON P.ProdCode = O.ProdCode
	END
  END

  SELECT * FROM @A
  IF @@ROWCOUNT > 0
    RETURN 0     
  ELSE
    RETURN -1

  SET NOCOUNT OFF  
END

GO
