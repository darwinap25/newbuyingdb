USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetAssociatedProducts]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SetAssociatedProducts]
  @KeyID                int,               -- 主键. 0或者null,表示新增. 有值表示update
  @Prodcode             varchar(64),       -- 货品编码  
  @AssociatedProdCode   varchar(64),       -- 关联货品编码
  @SeqNo                int,               -- 关联货品序号
  @AssociatedProdName   nvarchar(512),	   -- 关联货品名称
  @AssociatedProdFile   nvarchar(512),	   -- 关联货品图片路径
  @Note                 nvarchar(512)	   -- 关联货品备注
AS
/****************************************************************************
**  Name : SetAssociatedProducts
**  Version: 1.0.0.0
**  Description : 设置指定货品的相关货品信息
**
**  Parameter :
  declare @a int, @MessageID int
  exec @a = GetAssociatedProducts ''
  print @a  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin
  set @KeyID = isnull(@KeyID, 0)
  set @SeqNo = isnull(@SeqNo, 0)
  if @KeyID = 0 and @SeqNo = 0 
  begin
    select @SeqNo = max(SeqNo) from Product_Associated where Prodcode = @Prodcode
    set @SeqNo = isnull(@SeqNo, 0) + 1
  end  
  if (@KeyID = 0) 
    insert Product_Associated(ProdCode, SeqNo, AssociatedProdCode, AssociatedProdName, AssociatedProdFile, Note)
    values(@ProdCode, @SeqNo, @AssociatedProdCode, @AssociatedProdName, @AssociatedProdFile, @Note)
  else
    update Product_Associated set ProdCode = @ProdCode, SeqNo = @SeqNo, AssociatedProdCode = @AssociatedProdCode, 
        AssociatedProdName = @AssociatedProdName, AssociatedProdFile = @AssociatedProdFile, Note = @Note
    where KeyID = @KeyID
  return 0  
end

GO
