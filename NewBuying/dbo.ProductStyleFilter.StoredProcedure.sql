USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ProductStyleFilter]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ProductStyleFilter]
  @StoreID              int,               -- 店铺ID
  @BrandID              int,               -- 品牌ID  
  @ProductType          int,               -- 货品类型
  @ClassifyFilter       varchar(max),      -- 自定义货品分类过滤条件。格式：'school=1|aa=2|bb=3', 多选时格式： 'school=1,2'    
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @OrderCondition		nvarchar(1000),	   -- 自定义排序条件  
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr         varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1     
AS
/****************************************************************************
**  Name : ProductStyleFilter
**  Version: 1.0.0.0
**  Description : 获得BUY_ProductStyle数据	 （for bauhaus）。 (BUYING DB)
**
  declare @a int, @PageCount int, @RecordCount int
  exec @a = ProductStyleFilter  0,'',7,50,@PageCount output,@RecordCount output,'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount  
  select * from product
  select * from Gender
  select * from buy_store
  select * from ProductStockOnhand
**  Created by: Thomas @2016-08-24
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @SQLStr1 nvarchar(4000), @ForeignTable varchar(64), @ForeignID varchar(100), @WhereStr nvarchar(4000),@Language int
  set @StoreID = isnull(@StoreID, 0)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1     
  set @BrandID = isnull(@BrandID, 0)        
  
  set @QueryCondition = isnull(@QueryCondition, '')
  set @OrderCondition = isnull(@OrderCondition, '')
  set @WhereStr = isnull(@WhereStr, '')
 
  set nocount on
  if object_id('dbo.#tempProdDB') is not NULL 
     drop table #tempProdDB	 
  CREATE TABLE #tempProdDB (ProdCodeStyle nvarchar(50) collate database_default)
    set @SQLStr1 = ' select distinct s.ProdCodeStyle '      
	  + ' from (select ProdCode, case ' +  cast(@Language as varchar) + ' when 2 then prodDesc2 when 3 then prodDesc3 else prodDesc1 end as prodDesc,Flag1,NewFlag,HotSaleFlag, productbrandcode, ProductSizeCode, ColorCode, DepartCode,IsOnlineSKU from Buy_Product where IsonlineSKU = 1) P '
      + ' left join (select ProdCode AS SProdCode, ProdCodeStyle from buy_Productstyle ) s on p.prodcode = s.SProdcode '
	  
	  + ' left join (select Colorcode CCode from Buy_Color) C on P.ColorCode = C.CCode '
	  
	  + ' left join (select ProductSizeId SizeId from Buy_ProductSize) sz on P.ProductSizeCode = sz.SizeId '
	  + ' left join (select A.ProdCode as SProdCode, B.SeasonCode from (select * from buy_Product_Classify where ForeignTable = ''SEASON'') A left join SEASON B on A.ForeignKeyID = B.SeasonID) E on P.ProdCode = E.SProdCode '
	  + ' left join (select A.ProdCode as GProdCode, B.GenderCode from (select * from buy_Product_Classify where ForeignTable = ''Gender'') A left join Gender B on A.ForeignKeyID = B.GenderID) G on P.ProdCode = G.GProdCode ' 
	  + ' left join (SELECT ProdCode as RProdCode, RPriceTypeCode, Price as NetPrice, RefPrice as DefaultPrice FROM BUY_RPRICE_M '
	  + ' 	       where KeyID in  (SELECT  MAX(KeyID) AS KeyID FROM BUY_RPRICE_M BM WHERE (StartDate <= GETDATE()) AND (EndDate+1 >= GETDATE()) AND EXISTS (SELECT * FROM Buy_Product BP WHERE IsonlineSKU = 1 AND BP.ProdCode = BM.ProdCode) group by ProdCode)) R on P.ProdCode = R.RProdCode '
	  + ' where IsOnlineSKU = 1 '
	  
  if isnull(@QueryCondition,'')<>''
    SET @WhereStr = ' and ' + @QueryCondition  
  SET @SQLStr1 = @SQLStr1 + @WhereStr
  
  INSERT INTO #tempProdDB
  EXEC sp_executesql @SQLStr1, N'@QueryCondition varchar(1000)',@QueryCondition = @QueryCondition
  CREATE INDEX IDX_tempProdDB ON #tempProdDB(ProdCodeStyle)  
  SET @QueryCondition =''
  SET nocount off
  set @SQLStr = 'select P.ProdCode, S.ProdCodeStyle, P.Flag1, P.NewFlag, P.HotSaleFlag, E.SeasonCode as Season, G.GenderCode as sex,'
		
      + '   case ' + cast(@Language as varchar) + ' when 2 then ProdDesc2 when 3 then ProdDesc3 else ProdDesc1 end as ProdDesc , '	  
      + '   case ' + cast(@Language as varchar) + ' when 2 then SZ.ProductSizeName2 when 3 then SZ.ProductSizeName3 else SZ.ProductSizeName1 end as ProductSizeName, '	  
	  
	  + '	 P.ProdPicFile, P.DepartCode, '
--    + '   case ' + cast(@Language as varchar) + ' when 2 then D.DepartName2 when 3 then D.DepartName3 else D.DepartName1 end as DepartName, '	  
--	  + '   D.BrandID, P.ProdType, P.ProdNote, P.ProductBrandID, '	  
	  + '   P.productbrandcode, P.ProductType, B.BrandID, '	  
      + '   case ' + cast(@Language as varchar) + ' when 2 then B.BrandName2 when 3 then B.BrandName3 else B.BrandName1 end as BrandName, '
	  + '   case ' + cast(@Language as varchar) + ' when 2 then B.BrandDesc2 when 3 then B.BrandDesc3 else B.BrandDesc1 end as BrandDesc, '
	  + '   B.BrandPicSFile, B.BrandPicMFile, B.BrandPicGFile, '
	  + '   Q.TotQty OnHandQty, S.MaxPrice, S.MinPrice, P.NonSale, C.ColorID, P.ProductSizeCode, '  
      + '   S.MaxDefaultPrice, S.MinDefaultPrice, P.CreatedOn'
  
  set @SQLStr = @SQLStr + ' from buy_product  P '      
      + ' left join buy_Brand B on P.productbrandcode = B.BrandCode '
      + ' left join buy_Color C on P.ColorCode = C.ColorCode ' 
      + ' left join buy_Productstyle S1 on P.ProdCode = S1.ProdCode '       
	  + ' left join Buy_ProductSize SZ on P.ProductSizeCode = SZ.ProductSizeID '
	  + ' left join (select A.ProdCode, B.SeasonCode from (select * from buy_Product_Classify where ForeignTable = ''SEASON'') A left join SEASON B on A.ForeignKeyID = B.SeasonID) E on P.ProdCode = E.ProdCode '
	  + ' left join (select A.ProdCode, B.GenderCode from (select * from buy_Product_Classify where ForeignTable = ''Gender'') A left join Gender B on A.ForeignKeyID = B.GenderID) G on P.ProdCode = G.ProdCode ' 	  
      + ' left join (select ProdCodeStyle, min(X.ProdCode) as PPCode, max(Y.Price) as MaxPrice, min(Y.Price) as MinPrice, max(Y.RefPrice) as MaxDefaultPrice, min(Y.RefPrice) as MinDefaultPrice  from buy_Productstyle X '
      + '             left join (SELECT ProdCode, Price,RefPrice FROM Buy_Rprice_M where KeyID in  (SELECT  MAX(KeyID) AS KeyID FROM Buy_Rprice_M BM WHERE (StartDate <= GETDATE()) AND (EndDate+1 >= GETDATE()) AND EXISTS (SELECT * FROM Buy_Product BP WHERE BP.IsonlineSKU = 1 AND BP.ProdCode = BM.ProdCode) group by ProdCode) ) Y on X.ProdCode = Y.ProdCode group by ProdCodeStyle) S on P.ProdCode = S.PPCode '   
            
  if @StoreID = 0
	 set @SQLStr = @SQLStr + ' left join (select ProdCode,sum(Isnull(OnHandQty, 0)) as TotQty from stk_StockOnhand where StockTypeCode = ''G'' group by ProdCode ) Q on P.ProdCode = Q.ProdCode '
  else
   set @SQLStr = @SQLStr + ' left join (select ProdCode,Isnull(OnHandQty, 0) as TotQty from stk_StockOnhand where StockTypeCode = ''G'' and storeid =' + CAST(@StoreID as varchar) + ') Q on P.ProdCode = Q.ProdCode '
   set @SQLStr = @SQLStr + ' where isnull(S.PPCode, '''') <> '''' '     
    + ' and (B.BrandID = ' + cast(@BrandID as varchar)+ ' or ' + cast(@BrandID as varchar) + '= 0)'      
    + ' and (P.ProductType = ' + cast(@ProductType as varchar)+ ' or ' + cast(@ProductType as varchar) + '= 0)'          
	
	+ ' and P.IsonlineSKU = 1 '
	+ ' and exists (select * from #tempProdDB tmp where s.ProdCodeStyle = tmp.ProdCodeStyle) '
	
--	+ ' and s.ProdCodeStyle in (select ProdCodeStyle from #tempProdDB) '
  if @ClassifyFilter <> ''
  begin
    declare @TempStr varchar(max), @POS int, @ConditionStr varchar(200)
    set @TempStr = RTrim(LTrim(@ClassifyFilter))
    while len(@TempStr) > 0
    begin
	  set @POS = CharIndex('|', @TempStr)
	  if @POS > 0 
	  begin
	    set @ConditionStr = substring(@TempStr, 1, @POS - 1)
		set @TempStr = substring(@TempStr, @POS + 1, len(@TempStr) - @POS)
	  end  else
	  begin
	 
   set @ConditionStr = @TempStr
		set @TempStr = ''
	  end 
	  if len(@ConditionStr) > 0
	  begin
		set @POS = CharIndex('=', @ConditionStr)	
		set @ForeignTable = substring(@ConditionStr, 1, @POS - 1)
		set @ForeignID = substring(@ConditionStr, @POS + 1, len(@ConditionStr) - @POS)
		set @SQLStr = @SQLStr + ' and (P.Prodcode in (select Prodcode from Product_Classify where ForeignTable = ''' + @ForeignTable + ''' and ForeignkeyID in (' + @ForeignID + ')))' 
	  end
    end
  end  
  exec SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @QueryCondition  
  return 0
end

GO
