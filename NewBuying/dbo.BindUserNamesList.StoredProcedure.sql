USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BindUserNamesList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/11 14:27:34
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[BindUserNamesList]
  
AS
SET NOCOUNT ON;

BEGIN


    SELECT
        [u].[UserID],
        [u].[UserName]
    FROM [dbo].[Accounts_Users] u
    ORDER BY [u].[UserName] 
END


GO
