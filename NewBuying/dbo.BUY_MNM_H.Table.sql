USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_MNM_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_MNM_H](
	[MNMCode] [varchar](64) NOT NULL,
	[MNMType] [int] NULL,
	[Description1] [nvarchar](512) NULL,
	[Description2] [nvarchar](512) NULL,
	[Description3] [nvarchar](512) NULL,
	[StoreCode] [varchar](64) NULL,
	[StoreGroupCode] [varchar](64) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[DayFlagCode] [varchar](64) NULL,
	[WeekFlagCode] [varchar](64) NULL,
	[MonthFlagCode] [varchar](64) NULL,
	[LoyaltyOnly] [int] NULL,
	[LoyaltyCardTypeCode] [varchar](64) NULL,
	[LoyaltyCardGradeCode] [varchar](64) NULL,
	[LoyaltyThreshold] [varchar](64) NULL,
	[HitType] [int] NULL,
	[HitOP] [int] NOT NULL,
	[HitAmount] [dbo].[Buy_Amt] NOT NULL,
	[HitQty] [int] NULL,
	[GiftType] [int] NULL,
	[GiftQty] [int] NULL,
	[PromotionType] [int] NULL,
	[PromotionOn] [int] NULL,
	[AmountType] [char](1) NULL,
	[Amount] [dbo].[Buy_Amt] NOT NULL,
	[Note] [nvarchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_MNM_H] PRIMARY KEY CLUSTERED 
(
	[MNMCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ('0') FOR [StartTime]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ('0') FOR [EndTime]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [LoyaltyOnly]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [HitType]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [HitAmount]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [HitQty]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [GiftType]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [GiftQty]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [PromotionType]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ((0)) FOR [PromotionOn]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT ('$') FOR [AmountType]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_MNM_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'MNMCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销命中条件类型。
0: Promotion By Hit Qty
1: Promotion By Hit Amount
2: Promotion By Sales Amount
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'MNMType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'Description1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'Description2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'Description3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺Code。空/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。空/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'StoreGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效时间（例如 上午9:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'StartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销失效时间（例如 下午20:00），默认0表示全天有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'EndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一月中促销生效日 （Buy_DayFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'DayFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'一周中促销生效日 （Buy_WeekFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'WeekFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销生效月 （Buy_MonthFlag表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'MonthFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否仅会员混配促销。0：不是。1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'LoyaltyOnly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销指定的会员卡类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'LoyaltyCardTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销指定的会员卡等级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'LoyaltyCardGradeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销指定的会员忠诚度阀值。一般指会员积分数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'LoyaltyThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中类型：
0: Hit must be same SKU
1: Hit must be each SKU
2: Hit can be any SKU
99: Hit Qty is fixed
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'HitType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中关系操作符。
0：没有操作符
1： =     （等于时， 如果金额大于此值，也符合条件）
2： <>
3： <=
4：>=
5：<
6：>
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'HitOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'HitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命中数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'HitQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'礼品类型：
0: Gift must be same SKU
1: Gift must be each SKU
2: Gift can be any SKU
3: Gift must be same SKU with Hit
99: Gift Qty is fixed
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'GiftType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'礼品货品数量。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'GiftQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣金额设定类型。0 : Sales at   1 : Sales off' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'PromotionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣实现方式。0: Discount on Hit     1: Discount on Gift    2: Discount on Transaction

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'PromotionOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'指定Amount内容类型。 
$: 金额。   %:百分比' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'AmountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金额或者折扣' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'混配促销配置表。主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MNM_H'
GO
