USE [NewBuying]
GO
/****** Object:  Table [dbo].[Promotion_Segment]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion_Segment](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PromotionCode] [varchar](64) NOT NULL,
	[SegmentID] [int] NULL,
 CONSTRAINT [PK_PROMOTION_SEGMENT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Promotion_Segment] ADD  DEFAULT ((0)) FOR [SegmentID]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Segment', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Segment', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许此促销的SegmentID。 Null/0 表示不做限制。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Segment', @level2type=N'COLUMN',@level2name=N'SegmentID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promotion_H的子表，促销针对人群表。
（记录之间是 or 的关系）
此表为空，表示不做限制。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Promotion_Segment'
GO
