USE [NewBuying]
GO
/****** Object:  Table [dbo].[iBeaconGroupMap]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[iBeaconGroupMap](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[iBeaconGroupID] [int] NOT NULL,
	[iBeaconID] [int] NOT NULL,
 CONSTRAINT [PK_IBEACONGROUPMAP] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroupMap', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeaconGroup表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroupMap', @level2type=N'COLUMN',@level2name=N'iBeaconGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeacon表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroupMap', @level2type=N'COLUMN',@level2name=N'iBeaconID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'iBeaconGroup 和 iBeacon 的绑定 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'iBeaconGroupMap'
GO
