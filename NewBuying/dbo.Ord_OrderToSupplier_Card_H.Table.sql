USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_OrderToSupplier_Card_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_OrderToSupplier_Card_H](
	[OrderSupplierNumber] [varchar](64) NOT NULL,
	[SupplierID] [int] NULL,
	[OrderSupplierDesc] [varchar](512) NULL,
	[PurchaseType] [int] NULL,
	[OrderType] [int] NULL,
	[ReferenceNo] [varchar](64) NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[SupplierAddress] [nvarchar](512) NULL,
	[SuppliertContactName] [varchar](512) NULL,
	[SupplierPhone] [varchar](512) NULL,
	[SupplierEmail] [varchar](512) NULL,
	[SupplierMobile] [varchar](512) NULL,
	[StoreID] [int] NULL,
	[StoreContactName] [varchar](512) NULL,
	[StorePhone] [varchar](512) NULL,
	[StoreEmail] [varchar](512) NULL,
	[StoreMobile] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[Subject] [varchar](512) NULL,
	[CompanyID] [int] NULL,
	[PackageQty] [int] NULL,
	[IsProvideNumber] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_ORDERTOSUPPLIER_CARD_H] PRIMARY KEY CLUSTERED 
(
	[OrderSupplierNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_H] ADD  DEFAULT ((1)) FOR [PurchaseType]
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_H] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_H] ADD  DEFAULT ((1)) FOR [PackageQty]
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_H] ADD  DEFAULT ((0)) FOR [IsProvideNumber]
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Ord_OrderToSupplier_Card_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_OrderToSupplier_Card_H] ON [dbo].[Ord_OrderToSupplier_Card_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_OrderToSupplier_Card_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2014-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @PurchaseType int, @ordernumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @PurchaseType = PurchaseType, @ordernumber = OrderSupplierNumber FROM INSERTED 
  if isnull(@PurchaseType, 1) = 1  
    exec GenUserMessage @CreatedBy, 9, 0, @ordernumber  
  else
    exec GenUserMessage @CreatedBy, 20, 0, @ordernumber 
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_OrderToSupplier_Card_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_OrderToSupplier_Card_H] ON [dbo].[Ord_OrderToSupplier_Card_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_OrderToSupplier_Card_H
* Version: 1.0.0.4
* Description : 给供应商的Card订单表的批核触发器
*  
** Create By Gavin @2014-10-09
** Modify by Gavin @2014-12-01 (ver 1.0.0.1) 增加PurchaseType的区分。 PurchaseType=1 是原有的card库存订单。 PurchaseType=2是成card购买充值订单
** Modify by Gavin @2015-02-11 (ver 1.0.0.2) PurchaseType=2时,调用GenCardReceiveOrder_Purchase,产生收货单,而不是调用DoCardRecharge,直接增值
** Modify by Gavin @2015-04-07 (ver 1.0.0.3) 产生receive order 时, 需要检查卡状态, 如果状态不正确和 找不到卡 相同处理. raiserror ...
** Modify by Gavin @2015-04-30 (ver 1.0.0.4) 加上begin end 
*/
/*==============================================================*/
BEGIN  
  declare @OrderSupplierNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6),
          @PurchaseType int, @a int
  
  DECLARE CUR_OrderToSupplier_Card_H CURSOR fast_forward FOR
    SELECT OrderSupplierNumber, ApproveStatus, CreatedBy, PurchaseType FROM INSERTED 
  OPEN CUR_OrderToSupplier_Card_H
  FETCH FROM CUR_OrderToSupplier_Card_H INTO @OrderSupplierNumber, @ApproveStatus, @CreatedBy, @PurchaseType
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where OrderSupplierNumber = @OrderSupplierNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin   
      exec GenApprovalCode @ApprovalCode output    
      if isnull(@PurchaseType, 1) = 1
        exec GenCardReceiveOrder @CreatedBy, @OrderSupplierNumber
      else 
      begin
        exec @a = GenCardReceiveOrder_Purchase @CreatedBy, @OrderSupplierNumber, 1   
        if @a <> 0
        begin
          RAISERROR ('Not find HQ Card.', 16, 1)
          ROLLBACK TRANSACTION
        end  
      end  
        --exec DoCardRecharge @CreatedBy, @OrderSupplierNumber
        
      update Ord_OrderToSupplier_Card_H set ApprovalCode = @ApprovalCode where OrderSupplierNumber = @OrderSupplierNumber   
      -- 产生提示消息.
      if isnull(@PurchaseType, 1) = 1
        exec GenUserMessage @CreatedBy, 9, 1 , @OrderSupplierNumber
      else
        exec GenUserMessage @CreatedBy, 20, 1, @OrderSupplierNumber   
    end
    
    FETCH FROM CUR_OrderToSupplier_Card_H INTO @OrderSupplierNumber, @ApproveStatus, @CreatedBy, @PurchaseType    
  END
  CLOSE CUR_OrderToSupplier_Card_H 
  DEALLOCATE CUR_OrderToSupplier_Card_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提交供应商订单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'OrderSupplierNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商主键，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SupplierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单供应商描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'OrderSupplierDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货种类。1：实体卡订货。 2： 指定卡购买金额或者积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'PurchaseType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。如果是由其他单据产生的，就填此单据号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network，5：快递送货（实体Coupon）。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址。（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SupplierAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SuppliertContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SupplierPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SupplierEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'SupplierMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID （总部）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'StorePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'StoreEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任意内容。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'Subject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装数量。默认为1. 不得为0 或者 null' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'PackageQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要向供应商提供号码。（供应商根据提供的号码打印）。 默认0。  0：不提供。 1：提供' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'IsProvideNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'面向供应商的订货单（订货实体Card）
注：工作流程：总部向供应商下订单时，提供需要的Card号码（首号码 和 数量）。供应商根据提供的号码 提供Card。
从Ord_OrderToSupplier_H 复制过来@2014-09-17
@2014-11-28: 功能扩展：可向供应商购买金额/积分。 主表增加类型字段区分。子表加操作金额操作积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_H'
GO
