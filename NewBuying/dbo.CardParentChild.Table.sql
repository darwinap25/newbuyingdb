USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardParentChild]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CardParentChild](
	[ParentCardNumber] [varchar](64) NOT NULL,
	[ChildCardNumber] [varchar](64) NOT NULL,
 CONSTRAINT [PK_CARDPARENTCHILD] PRIMARY KEY CLUSTERED 
(
	[ParentCardNumber] ASC,
	[ChildCardNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主卡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardParentChild', @level2type=N'COLUMN',@level2name=N'ParentCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子卡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardParentChild', @level2type=N'COLUMN',@level2name=N'ChildCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡父子关系表。 
（目前为方便svaweb的设置UI，可能取代card表中的parentcardnumber字段）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardParentChild'
GO
