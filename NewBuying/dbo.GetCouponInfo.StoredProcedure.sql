USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCouponInfo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetCouponInfo]
  @CouponNumber			varchar(512),      -- 指定的CouponNumber
  @LanguageAbbr			varchar(20)=''	   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1  
AS
/****************************************************************************
**  Name : GetCouponInfo  
**  Version: 1.0.0.3
**  Description : 获得指定的Coupon信息
**  Parameter :
 select Campaigncode, * from ViewMemberCoupons
  declare @CouponNumber varchar(512), @a int
  set @CouponNumber = '000000001034'
  exec @a = GetCouponInfo @CouponNumber, ''
  print @a  
**
**  Created by: Gavin @2012-06-28
**  Modify by: Gavin @2012-07-27 (Ver 1.0.0.1) 输入的为CouponUID,需要转换为CouponNumber
**  Modify by: Gavin @2015-10-12 (Ver 1.0.0.2) 增加根据语言区分的MemberClauseDesc
**  Modify by: Gavin @2017-10-11 (Ver 1.0.0.3) 增加返回Coupon绑定的销售的prodcode，和redeem 的prodcode （只针对MPOS销售机场Tag的情况）
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @StateofCoupon int, @CouponRealNumber varchar(512) --, @ConditionStr nvarchar(400)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1

  select @CouponRealNumber=CouponNumber from CouponUIDMap where CouponUID=@CouponNumber
    
  select case when @Language = 2 then CouponTypeName2 when @Language = 3 then CouponTypeName3 else CouponTypeName1 end as CouponTypeName, 
      case when @Language = 2 then MemberClauseDesc2 when @Language = 3 then MemberClauseDesc3 else MemberClauseDesc1 end as MemberClauseDesc,
	  B.ProdCode AS SellSKU, 
      A.* 
	from ViewMemberCoupons A 
	  LEFT JOIN (select * from CouponTypeExchangeBinding where BindingType=1) B ON A.CouponTypeID = B.CouponTypeID
    where CouponNumber = @CouponRealNumber 
      
  return 0
end

GO
