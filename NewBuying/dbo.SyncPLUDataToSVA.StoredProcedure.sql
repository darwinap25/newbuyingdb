USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SyncPLUDataToSVA]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SyncPLUDataToSVA]
AS
/****************************************************************************
**  Name : SyncPLUDataToSVA  £¨ºÏ²¢DB°æ±¾£©
**  Version: 1.0.0.2 (for Bauhaus)
**  Description :  ´ÓBuying DB »ñµÃ PLU Ïà¹ØÊý¾ÝÐ´Èë SVA¡£ ´Ë¹ý³ÌÔÚBUY DBÉÏÖ´ÐÐ¡£
                  
**  Created by: Gavin @2017-04-26 
**  Modify by: Gavin @2017-07-05 £¨ver 1.0.0.1£© ºÏ²¢DB°æ±¾£¬ Ã»ÓÐlinkserver.
**  Modify by: Gavin @2017-08-16 £¨ver 1.0.0.2£© Ôö¼Óµ÷ÓÃInitProductStyle_List¡£Ä¿µÄÎªÁË¸üÐÂproductÊý¾Ýºó£¬Á¢¿ÌÓ°Ïìµ½onlineshopping¡£ InitProductStyle_ListÔËÐÐÐèÒªÏûºÄÊ±¼ä£¬Ð§¹ûÓÐ´ý¹Û²ì¡£
**
****************************************************************************/
begin 
  
  -- ¿¼ÂÇÐÔÄÜÎÊÌâ£¬ÕâÀï¶Ô»ù´¡Êý¾Ý£¬Ö»ÊÇ×öÈ±ÉÙ¼ÇÂ¼µÄÌí¼Ó£¬²»¶ÔÒÑÓÐÊý¾Ý×öupdate¡£

  INSERT INTO Color (ColorCode, ColorName1, ColorName2, ColorName3, ColorPicFile, RGB)
  SELECT ColorCode, ColorName1, ColorName2, ColorName3, ColorPicFile, RGB
    FROM BUY_COLOR WHERE ColorCode NOT IN (SELECT ColorCode FROM  Color)

  INSERT INTO Product_Size (ProductSizeID, ProductSizeCode, ProductSizeName1, ProductSizeName2, ProductSizeName3, ProductSizeNote, ProductSizeType)
  SELECT ProductSizeID, ProductSizeCode, ProductSizeName1, ProductSizeName2, ProductSizeName3, ProductSizeNote, ProductSizeType
    FROM BUY_PRODUCTSIZE WHERE ProductSizeCode NOT IN (SELECT ProductSizeCode FROM Product_Size)  

  INSERT INTO Department (DepartCode, DepartName1, DepartName2, DepartName3, DepartPicFile, 
     DepartPicFile2, DepartPicFile3, DepartNote, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  SELECT DepartCode, DepartName1, DepartName2, DepartName3, DepartPicFile, 
     DepartPicFile2, DepartPicFile3, DepartNote, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy
    FROM BUY_DEPARTMENT 
  WHERE DepartCode NOT IN (SELECT DepartCode FROM Department)    


  INSERT INTO Product_Style (ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile)
  SELECT ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile
    FROM BUY_PRODUCTSTYLE A 
  WHERE A.ProdCodeStyle not in (SELECT ProdCodeStyle FROM Product_Style)
    
  INSERT INTO Product_Brand (ProductBrandCode, ProductBrandName1, ProductBrandName2, ProductBrandName3, 
     ProductBrandDesc1, ProductBrandDesc2, ProductBrandDesc3, ProductBrandPicSFile, ProductBrandPicMFile, ProductBrandPicGFile, 
     CardIssuerID, IndustryID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
  SELECT BrandCode, BrandName1, BrandName2, BrandName3, 
     BrandDesc1, BrandDesc2, BrandDesc3, BrandPicSFile, BrandPicMFile, BrandPicGFile, 
     CardIssuerID, IndustryID, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy
    FROM BUY_BRAND A 
  WHERE A.BrandCode not in (SELECT ProductBrandCode FROM Product_Brand)
 
  INSERT INTO Product (ProdCode, ProdName1, ProdName2, ProdName3, DepartCode, ProductBrandID, NonSale, 
    ProdPicFile, ProdType, ProdNote, PackQty, NewFlag, HotSaleFlag, Flag1, Flag2, Flag3, Flag4, Flag5, 
    OriginID, ColorID, ProductSizeID, AddPointFlag, AddPointValue, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy,ColorCode,ProductSizeCode, IsOnlineSKU)
  SELECT P.ProdCode, ProdDesc1, ProdDesc2, ProdDesc3, D.DepartCode, B.ProductBrandID, NonSale, 
    ProdPicFile, ProductType, ProdSpec, G.PackageSizeQty, NewFlag, HotSaleFlag, Flag1, Flag2, Flag3, Flag4, Flag5, 
    N.NationID, C.ColorID, S.ProductSizeID, AddPointFlag, AddPointValue,     
    P.CreatedOn, P.CreatedBy, P.UpdatedOn, P.UpdatedBy,P.ColorCode,S.ProductSizeCode,P.IsOnlineSKU
   FROM BUY_PRODUCT P left join Product_Brand B on P.ProductBrandCode = B.ProductBrandCode 
     left join Department D on D.DepartCode = P.DepartCode 
     left join BUY_PRODUCTPACKAGE G on P.PackageSizeCode = G.PackageSizeCode
     left join Nation N on P.OriginCountryCode = N.CountryCode
     left join Color C on P.ColorCode = C.ColorCode 
     left join Product_Size S on P.ProductSizeCode = S.ProductSizeID    
   WHERE P.ProdCode not in (SELECT ProdCode FROM Product)   

   UPDATE Product SET ProdName1 = B.ProdDesc1,ProdName2 = B.ProdDesc2, ProdName3 = B.ProdDesc3, ProductBrandID=C.ProductBrandID,
	        DepartCode=B.DepartCode, NonSale = B.NonSale, ColorCode = B.ColorCode,ProductSizeCode=B.ProductSizeCode,
			UpdatedOn=Getdate(), IsOnlineSKU = B.isOnlineSKU
   FROM Product A LEFT JOIN BUY_PRODUCT B ON A.ProdCode=B.ProdCode
    left join Product_Brand C on B.ProductBrandCode = C.ProductBrandCode 
   WHERE A.ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)

   DELETE FROM Product_PIC 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)

  INSERT INTO Product_PIC (ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1, ProductPicNote2, ProductPicNote3)  
  SELECT ProdCode, ProductThumbnailsFile, ProductFullPicFile, ProductPicNote1, ProductPicNote2, ProductPicNote3
    FROM BUY_PRODUCT_PIC A
  WHERE A.ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)
    

   DELETE FROM Product_Price 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)
   INSERT INTO Product_Price (ProdCode, ProdPriceType, DefaultPrice, NetPrice, StoreCode, StoreGroupCode,  --MemberPrice, PromotionPrice, 
     Status, StartDate, EndDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
   SELECT M.ProdCode, 1, RefPrice, Price, StoreCode, StoreGroupCode,          -- MemberPrice, PromotionPrice, 
     M.Status, M.StartDate, M.EndDate, Getdate(), M.UpdatedBy, M.UpdatedOn, M.UpdatedBy
    FROM BUY_RPRICE_M M 
	  left join Product P on M.ProdCode = P.ProdCode
    WHERE DateDiff(dd, M.startdate, getdate()) >= 0 and DateDiff(dd, getdate(), M.enddate) >= 0 and M.status = 1 
	  AND M.ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)
 
   DELETE FROM ProductBarcode 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)
   INSERT INTO ProductBarcode (Barcode, ProdCode)
   SELECT B.Barcode, B.ProdCode 
    FROM BUY_BARCODE B left join Product P on B.ProdCode = P.ProdCode 
	  LEFT JOIN ProductBarcode A ON B.Barcode = A.BARCODE
   WHERE P.ProdCode is not null and B.ProdCode in (select BarCode from IMP_PRODUCT_TEMP)


   DELETE FROM Product_Associated 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)  
   INSERT INTO Product_Associated (ProdCode, SeqNo, AssociatedProdCode, AssociatedProdName, AssociatedProdFile, Note,
     CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
   SELECT ProdCode, SeqNo, AssociatedProdCode, AssociatedProdName, AssociatedProdFile, Note,
     CreatedOn, CreatedBy, UpdatedOn, UpdatedBy
   FROM BUY_ProductAssociated A
   WHERE A.ProdCode in (SELECT BarCode FROM IMP_PRODUCT_TEMP)
 	 
   DELETE FROM Product_Particulars 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)  
  INSERT INTO Product_Particulars (ProdCode, LanguageID, ProdFunction, ProdIngredients, ProdInstructions, PackDesc,
   PackUnit, Memo1, Memo2, Memo3, Memo4, Memo5, Memo6, MemoTitle1, MemoTitle2, MemoTitle3, MemoTitle4, 
   MemoTitle5, MemoTitle6)
  SELECT ProdCode, LanguageID, ProdFunction, ProdIngredients, ProdInstructions, PackDesc,
   PackUnit, Memo1, Memo2, Memo3, Memo4, Memo5, Memo6, MemoTitle1, MemoTitle2, MemoTitle3, MemoTitle4, 
   MemoTitle5, MemoTitle6 
  FROM BUY_Product_Particulars A	
  WHERE A.ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP) 
	 
   DELETE FROM Product_Classify 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)  
   INSERT INTO Product_Classify (ProdCode, ForeignkeyID, ForeignTable,
      CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
   SELECT ProdCode, ForeignkeyID, ForeignTable,
    CreatedOn, CreatedBy, UpdatedOn, UpdatedBy 
   FROM BUY_PRODUCT_CLASSIFY A
   WHERE A.ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP) 

   DELETE FROM Product_Style 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)    
   INSERT INTO Product_Style (ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile)
   SELECT ProdCodeStyle, ProdCode, ProductSizeType,ProdStyleSizeFile
     FROM BUY_PRODUCTSTYLE A
   WHERE A.ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP) 

   DELETE FROM Product_Catalog 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)    
   INSERT INTO Product_Catalog (ProdCode, DepartCode, 
      CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
   SELECT ProdCode, DepartCode, 
    CreatedOn, CreatedBy, UpdatedOn, UpdatedBy 
   FROM BUY_PRODUCT A
   WHERE A.ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)

   DELETE FROM PRODUCT_ADD_BAU 
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)   
   INSERT INTO PRODUCT_ADD_BAU (ProdCode,Standard_Cost,Export_Cost,AVG_Cost,MODEL,SKU,[YEAR],REORDER_LEVEL, 
      HS_CODE,COO,SIZE_RANGE,DESIGNER,BUYER,MERCHANDISER,RETIRE_DATE,CompanyCode, SizeM1,SizeM2,SizeM3,SizeM4,SizeM5,SizeM6,SizeM7,SizeM8)
   SELECT ProdCode,Standard_Cost,Export_Cost,AVG_Cost,MODEL,SKU,[YEAR],REORDER_LEVEL, 
      HS_CODE,COO,SIZE_RANGE,DESIGNER,BUYER,MERCHANDISER,RETIRE_DATE,CompanyCode, SizeM1,SizeM2,SizeM3,SizeM4,SizeM5,SizeM6,SizeM7,SizeM8
   FROM BUY_PRODUCT_ADD_BAU
   WHERE ProdCode IN (SELECT BarCode FROM IMP_PRODUCT_TEMP)  
   
   -- ver 1.0.0.2
   EXEC InitProductStyle_List
               
end

GO
