USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoReactivateMemberCard]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DoReactivateMemberCard]
  @Busdate          DATETIME = NULL
AS
/****************************************************************************
**  Name : DoReactivateMemberCard
**  Version: 1.0.0.0
**  Description :  
**  Created by:  Gavin @2017-01-16
****************************************************************************/
BEGIN
  DECLARE @NewBatchCode VARCHAR(64), @QTY INT, @CardGradeID INT, @CardTypeID INT, @NewBatchID INT
  DECLARE @ImportCardNumber VARCHAR(64) 

  SET @Busdate = ISNULL(@Busdate, GETDATE())

/*
    INSERT INTO Card_Movement (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, ApprovalCode, OrgStatus, NewStatus,
         OpenPoint, ClosePoint, StoreID)
    SELECT 22, CardImportNumber, null, null, '', 0, 0, 0, 0, GETDATE(), GETDATE(),NULL, 
	     null, null, '', '', '', 1, '', 0, 0,
         0, 0, null
    FROM CardImport WHERE Status = 0
	 
	exec [GetCardsforReactivation] 0

	SELECT * FROM
	(
	    SELECT 
        [h].[EffectiveDate],
        h.[CardAdjustNumber],
        [d].[CardNumber],
        [d].[ActPoints],
        [d].[ActAmount],
        row_number() over(partition by d.CardNumber order by d.[KeyID] desc) as RowNum
    FROM [dbo].[Card] c
        INNER JOIN [dbo].[Card_Movement] cm
            ON [cm].[CardNumber] = [c].[CardNumber]
        INNER JOIN [dbo].[Ord_CardAdjust_D] d 
            ON [d].[CardNumber] = [c].[CardNumber]
        INNER JOIN [dbo].[Ord_CardAdjust_H] h
            ON [h].[CardAdjustNumber] = [d].[CardAdjustNumber]
    WHERE [c].[Status] = 9
    AND [h].[ApproveStatus] = 'A'
    AND LEFT([h].[CardAdjustNumber],3) = 'CCS'
    AND CAST([h].[EffectiveDate] AS DATE)= CAST(@businessdate AS DATE)
    )
	WHERE 1=0
  END
*/
END

GO
