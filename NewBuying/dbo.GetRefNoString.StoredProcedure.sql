USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetRefNoString]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[GetRefNoString]
    @CODE  CHAR(6)
  , @REFNO NVARCHAR(50) output
AS
/****************************************************************************
**  Name : GetRefNoString
**  Version : 1.0.0.1
**  Description : Get RefNo 
**
**  Parameters: @CODE  - Code of search.
**              @REFNO - for return, if null is not found.
** 
declare @REFNO NVARCHAR(50)
exec GetRefNoString 'BTHCAD', @REFNO output
print @REFNO
**  Modified: 20050120
**  Modify by: Gavin @2012-06-12 RefNo表增加Active字段。0表示不产生号码。1：表示产生号码
****************************************************************************/
begin
DECLARE @Seq  int 
     ,  @Head varchar(6)
     ,  @Len int 
     ,  @Auto char(1)
     ,  @STOREID VARCHAR(6)
     , @VersionID VARCHAR(10)
     , @BusinessDate Datetime
     , @RefDate VARCHAR(8)
     , @Y CHAR(1)
     , @MM CHAR(2)
     , @DD CHAR(2)
     , @Trans_Date Datetime
     , @Active int
  set @REFNO = ''
  EXECUTE GetRefNo @CODE, @Seq output, @Head output, @Len output, @Auto output, @Active output

  if @Active = 1
    SET @REFNO = LTRIM(RTRIM(@Head)) + REPLICATE('0',@Len - LEN(LTRIM(RTRIM(@Head))) - LEN(LTRIM(STR(@Seq))))+ LTRIM(STR(@Seq))  
end

GO
