USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSSalesSubmit]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[POSSalesSubmit]
  @UserID                        INT,                              -- 操作人ID
  @StoreCode	                   VARCHAR(64),                      -- 店铺编码   
  @RegisterCode	                 VARCHAR(64),                      -- 终端编码
  @ServerCode	                   VARCHAR(64),                      -- server编码
  @TxnNo                         VARCHAR(64) output,               -- 交易号码
  @Busdate                       DATETIME output,                  -- 交易busdate
  @Txndate                       DATETIME,                         -- 交易系统时间
  @SalesType	                   INT,                              -- 交易类型
  @RefTxnNo                      VARCHAR(64),                      -- 相关交易号码
  @CashierID                     INT,                              -- 收银员ID  
  @SalesManID                    INT,                              -- 销售员ID
  @TotalAmount                   MONEY,                            -- 交易总额
  @TxnStatus                     INT,                              -- 交易状态
  @MemberSalesFlag               INT,                              -- 是否会有销售
  @MemberID                      INT,                              -- 会员ID
  @CardNumber	                   VARCHAR(64),                      -- 会员卡号。  
  @CustomerName                  VARCHAR(64),                      -- 联系人
  @CustomerPhone                 VARCHAR(64),                      -- 联系人电话
  @Deliveryer                    INT,                              -- 送货员
  @DeliveryFlag                  INT,                              -- 送货标志
  @DeliveryaAddr                 VARCHAR(512),                     -- 送货地址
  @DeliveryDate                  DATETIME,                         -- 送货时间
  @DeliveryCompleteDate          DATETIME,                         -- 送货完成时间
  @SalesDetail                   NVARCHAR(max),                    -- 交易单货品明细(交易货品, XML格式字符串)
  @SalesTender                   NVARCHAR(max),                    -- 交易单支付明细(交易支付记录, XML格式字符串)
  @SalesDisc                     NVARCHAR(max),                    -- 交易单折扣明细(交易中使用的改价，促销，折扣，BOM等等改变销售价格的操作, XML格式字符串)
  @SalesReceipt                  VARCHAR(max),                     -- 交易单小票（字符串格式）  
  @SalesReceiptBIN               VARBINARY(max),                   -- 交易单小票（二进制格式）
  @SalesAdditional               NVARCHAR(512),                    -- 交易单附加信息
  @ReturnMessageStr              VARCHAR(max) output,              -- 返回信息。（json格式）
  @PickupType                    INT = 1,                          -- 提货方式。 默认 1.  1：门店自提	 2：送货
  @PickupStoreCode               VARCHAR(64) = '',                 -- 门店自提时，设置提货店铺code
  @CODFlag                       INT= 0,                           -- 0：先款后货。 1：钱货两讫。@PickupType =2 时，表示货到付款
  @DeliveryNumber                VARCHAR(64)='',                   -- 送货单号.
  @LogisticsProviderID           INT=0                             -- 送货的物流供应商。  
AS
/****************************************************************************
**  Name : POSSalesSubmit    
**  Version: 1.0.0.2
**  Description : 保存POS的销售数据。(不检查库存, 交易提交就扣除库存, 允许负库存)
**  
@SalesDetail格式范例:  
<ROOT>
  <PLU TxnNo="" SeqNo="" ProdCode="0001" ProdDesc="" Serialno="" Collected="" RetailPrice="" NetPrice="" QTY="" NetAmount="" Additional="" ReservedDate="" PickupDate=""></PLU>
  <PLU TxnNo="" SeqNo="" ProdCode="0002" ProdDesc="" Serialno="" Collected="" RetailPrice="" NetPrice="" QTY="" NetAmount="" Additional="" ReservedDate="" PickupDate=""></PLU>
<ROOT>	 
@SalesTender格式范例:  
<ROOT>
  <TENDER TxnNo="" SeqNo="" TenderID="" TenderCode="" TenderType="" TenderName="" TenderAmount="" LocalAmount="" ExchangeRate="" Additional=""></TENDER>
  <TENDER TxnNo="" SeqNo="" TenderID="" TenderCode="" TenderType="" TenderName="" TenderAmount="" LocalAmount="" ExchangeRate="" Additional=""></TENDER>
</ROOT> 
@SalesDisc格式范例:  
<ROOT>
  <DISCOUNT TxnNo="" ItemSeqNo="" ProdCode="" TenderSeqNo="" TenderID="" TenderCode="" TenderType="" TenderName="" 
    SalesDiscCode="" SalesDiscDesc="" SalesDiscType="" SalesDiscDataType="" SalesDiscPrice="" 
    SalesPrice="" SalesDiscOffAmount="" SalesDiscQty="" AffectNetPrice="" SalesDiscLevel=""></DISCOUNT>
  <DISCOUNT TxnNo="" ItemSeqNo="" ProdCode="" TenderSeqNo="" TenderID="" TenderCode="" TenderType="" TenderName="" 
    SalesDiscCode="" SalesDiscDesc="" SalesDiscType="" SalesDiscDataType="" SalesDiscPrice="" 
    SalesPrice="" SalesDiscOffAmount="" SalesDiscQty="" AffectNetPrice="" SalesDiscLevel=""></DISCOUNT>
</ROOT> 
	         
  declare @UserID int, @a int, @BrandCode varchar(64), @StoreCode varchar(64), @RegisterCode varchar(64), @ServerCode varchar(64),
   @TxnNo varchar(64), @Busdate datetime, @Txndate datetime, @SalesType int,	@RefTxnNo varchar(64), @CashierID int, 
   @SalesManID int, @TotalAmount money, @TxnStatus int, @MemberSalesFlag int, @MemberID int, @CardNumber varchar(64), 
   @CustomerName varchar(64), @CustomerPhone varchar(64), @Deliveryer INT, @DeliveryFlag INT, @DeliveryaAddr nvarchar(512), 
   @DeliveryDate datetime, @DeliveryCompleteDate datetime, @Contact VARCHAR(64), @ContactPhone VARCHAR(64),
   @SalesDetail nvarchar(max),@SalesTender nvarchar(max), @SalesDisc nvarchar(max), @SalesReceipt varchar(max), 
   @SalesReceiptBIN  varbinary(max), @SalesAdditional NVARCHAR(512), @ReturnMessageStr varchar(max)
  set @UserID = 1
  set @MemberID = 1
  set @CardNumber = '000100086'
  set @BrandCode = '123123'
  set @StoreCode = '1'
  set @RegisterCode = '001'
  set @ServerCode = '02'
  set @TxnNo = ''
  set @Busdate = '2012-09-18'
  set @Txndate = '2012-09-18 12:12:12'  				
  set @SalesType = 0
  set @CashierID =1
  set @SalesManID = 1
  set @TotalAmount = 100
  set @TxnStatus = 5
  set @Contact = 'rrg'
  set @ContactPhone = '120'
  set @DeliveryaAddr = 'sldkdfjjsdlkfjfgsd'
  set @DeliveryDate = '2012-09-18'
  set @DeliveryCompleteDate = '2012-09-18'
  set @SalesDetail = '<ROOT>
  <PLU TxnNo="test201309182" SeqNo="1" ProdCode="0001" ProdDesc="是数据库的法律卡位" Serialno="" Collected="4" RetailPrice="100" NetPrice="100" QTY="1" NetAmount="100" Additional=""></PLU>
  <PLU TxnNo="test201309182" SeqNo="2" ProdCode="0002" ProdDesc="塞德里克个记录款未付" Serialno="" Collected="4" RetailPrice="100" NetPrice="100" QTY="2" NetAmount="200" Additional=""></PLU>
</ROOT>'
  set @SalesTender = '<ROOT>
  <TENDER TxnNo="test201309182" SeqNo="1" TenderID="1" TenderCode="1" TenderName="RMD" TenderAmount="200" LocalAmount="200" ExchangeRate="1" Additional=""></TENDER>
</ROOT> '
set @SalesDisc = '<ROOT>
  <DISCOUNT TxnNo="test201309182" ItemSeqNo="1" ProdCode="0001" TenderSeqNo="" TenderID="" TenderCode="" TenderType="" TenderName="" 
    SalesDiscCode="1" SalesDiscDesc="1" SalesDiscType="1" SalesDiscDataType="1" SalesDiscPrice="1" 
    SalesPrice="2" SalesDiscOffAmount="1" SalesDiscQty="1" AffectNetPrice="1" SalesDiscLevel="1"></DISCOUNT>
</ROOT>'
  set @SalesReceipt = ''
  exec @a = POSSalesSubmit @UserID,@StoreCode,@RegisterCode,@ServerCode,@TxnNo output,@Busdate output,@Txndate,@SalesType,@RefTxnNo,
    @CashierID, @SalesManID, @TotalAmount, @TxnStatus, @MemberSalesFlag, @MemberID, @CardNumber, @CustomerName, @CustomerPhone, 
    @Deliveryer, @DeliveryFlag, @DeliveryaAddr, @DeliveryDate, @DeliveryCompleteDate,
    @SalesDetail,@SalesTender, @SalesDisc, @SalesReceipt, @SalesReceiptBIN, @SalesAdditional, @ReturnMessageStr output
  print @a 
  print @ReturnMessageStr 

select * from sales_D
select * from sales_T
select * from Tender
select * from product
select cast(messagebody as nvarchar(max)), * from messageobject
select * from messagereceivelist
select * from card_movement where createdon > '2012-09-25'
**  Created by: Gavin @2015-03-06
**  Modify by: Gavin @2015-11-02 (ver 1.0.0.1) 修正insert salers_T时的字段顺序问题。 
**  Modify by Gavin @2016-01-07 (1.0.0.2) 获取busdate时增加StoreCode条件
**
****************************************************************************/
begin


  declare @OldTxnStatus int, @Longitude varchar(512), @latitude varchar(512), @MemberName varchar(512), @MemberMobilePhone varchar(512),
		      @CardTotalAmount money, @CardTotalPoints int, @ExpiryDate datetime, @CardStatus int, @MemberAddress nvarchar(512), 
		      @ApprovalCode varchar(6)
  declare @SalesDData xml, @SalesTData xml, @SalesDiscData xml
  declare @EarnPoints int, @StoreID int, @CalcResult int
  declare @ReceiveList varchar(64), @MessageID int, @MessageServiceTypeID int
  declare @CardGradeID int, @BindCouponTypeID int, @BindCouponCount int, @CouponExpiryDate datetime, @CouponNumber varchar(64)
  declare @CouponAmount money, @i int
  declare @CouponStatus int, @NewCouponStatus int, @NewCouponExpiryDate date
  declare @Result int, @OprID int, @AddValueCardNumber varchar(64), @AddValue money, @TenderType int, @LocalAmount money, @SalesTAdditional varchar(512)
  declare @ReturnCouponTypeAmount money, @ReturnCouponTypePoint int , @ReturnCouponTypeDiscount decimal(16,6), @ReturnBindPLU varchar(64),
          @CardExpiryDate datetime,  @ReturnMessageStrCardSubmit varchar(max)
  declare @ProdType int, @CardAmountToPointRate decimal(16,6), @AddPoint int, @CardPointToAmountRate decimal(16,6)        
  declare @paymentDoneOn datetime, @PointRuleType int
  declare @MemberCardAmountBalance money, @MemberCardAmountAct money, @MemberCardPointAct int, @MemberCardPointBalance int,
          @ActCoupon varchar(512), @StockTypeID int, @StockTypeCode varchar(64)
  declare @InputString varchar(max), @AccountNumber nvarchar(512), @SalesDMsgString varchar(max), @SalesDMsgJSON varchar(max)            
  declare @JsonProdDesc nvarchar(512), @JsonNetPrice money, @JsonQty int, @JsonNetAmount money, @Memo1 varchar(100), @Memo2 varchar(100)
  declare @JsonTenderID int, @JsonTenderType int , @JsonTenderCode varchar(64), @JsonTenderName nvarchar(512), @JsonTenderAmount money, 
          @JsonLocalAmount money, @JsonExchangeRate decimal(16,6), @JsonSalesTAdditional varchar(512)
  declare @SalesTMsgString varchar(max), @SalesTMsgJSON varchar(max), @HasCOD int
  declare @BrandCode varchar(64), @BrandName varchar(512)
    
  declare @SalesD table (TxnNo varchar(64), SeqNo varchar(64), ProdCode varchar(64), ProdDesc nvarchar(512), Serialno varchar(512), 
        Collected int, RetailPrice money, NetPrice money, Qty int, NetAmount money, Additional varchar(512),ReservedDate datetime, PickupDate datetime)
  declare @SalesT table (TxnNo varchar(64), SeqNo int, TenderCode varchar(64), TenderID int, TenderType int, TenderName nvarchar(512), TenderAmount money, 
        LocalAmount money, ExchangeRate decimal(16,6), TenderAdditional varchar(512), SalesTAdditional varchar(512))
  declare @SalesDS table(TxnNo varchar(64), ItemSeqNo varchar(64),ProdCode varchar(64), TenderSeqNo int, TenderCode varchar(64), 
        TenderID int, TenderType int, TenderName varchar(512), SalesDiscCode varchar(64),
        SalesDiscDesc varchar(512), SalesDiscType int, SalesDiscDataType int, SalesDiscPrice money, SalesPrice money, 
        SalesDiscOffAmount money, SalesDiscQty int, AffectNetPrice int, SalesDiscLevel int)  
  declare @Sign int      
          
  set @Result = 0
  set @Sign = 1
          
  --PLU detail数据写入临时表，判断货品库存数量
  if isnull(@SalesDetail, '') <> ''
  begin
  	set @SalesDData = @SalesDetail
	  Insert into @SalesD(TxnNo, SeqNo, ProdCode, ProdDesc, Serialno, Collected, RetailPrice, NetPrice, Qty, NetAmount, Additional, ReservedDate, PickupDate)
	  select @TxnNo, T.v.value('@SeqNo',' varchar(64)') as SeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode, 
	    T.v.value('@ProdDesc','nvarchar(512)') as ProdDesc, T.v.value('@Serialno','varchar(512)') as Serialno, 
	    -- for demo hardcode collected to 4
	    --T.v.value('@Collected','int') as Collected, 
	    4 as Collected, 
	    ----------------
	    T.v.value('@RetailPrice','money') as RetailPrice, 
	    T.v.value('@NetPrice','money') as NetPrice, T.v.value('@QTY','int') as QTY, T.v.value('@NetAmount','money') as NetAmount, 
	    T.v.value('@Additional','varchar(512)') as Additional,
	    T.v.value('@ReservedDate','datetime') as ReservedDate, T.v.value('@PickupDate','datetime') as PickupDate	    
	  from @SalesDData.nodes('/ROOT/PLU') T(v)
  end								
  --Tender detail数据写入临时表
  if isnull(@SalesTender, '') <> ''
  begin
  	set @SalesTData = @SalesTender
	  Insert into @SalesT(TxnNo, SeqNo, TenderID, TenderCode, TenderType, TenderName, TenderAmount, LocalAmount, ExchangeRate, TenderAdditional, SalesTAdditional)
	  select @TxnNo, T.v.value('@SeqNo','int') as SeqNo, T.v.value('@TenderID','int') as TenderID, T.v.value('@TenderCode','varchar(64)') as TenderCode, isnull(S.TenderType, 0) as TenderType,
	    T.v.value('@TenderName','nvarchar(512)') as TenderName, T.v.value('@TenderAmount','money') as TenderAmount, 
	    T.v.value('@LocalAmount','money') as LocalAmount, 
	    T.v.value('@ExchangeRate','decimal(16,6)') as ExchangeRate, '',  T.v.value('@Additional','varchar(512)') as SalesTAdditional
	  from @SalesTData.nodes('/ROOT/TENDER') T(v) left join BUY_CURRENCY S on T.v.value('@TenderCode','varchar(64)') = S.CURRENCYCODE
	  where isnull(S.CURRENCYCODE, '') <> ''	   
  end        		 

  --Discount 数据写入临时表
  if isnull(@SalesDisc, '') <> ''
  begin
  	set @SalesDiscData = @SalesDisc
	  Insert into @SalesDS(TxnNo, ItemSeqNo, ProdCode, TenderSeqNo, TenderCode, TenderID, TenderType, TenderName, SalesDiscCode,
	     SalesDiscDesc, SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, SalesDiscLevel)
	  select @TxnNo, T.v.value('@ItemSeqNo','varchar(64)') as ItemSeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode,
	     T.v.value('@TenderSeqNo','int') as TenderSeqNo, T.v.value('@TenderCode','varchar(64)') as TenderCode,
	     T.v.value('@TenderID','int') as TenderID, T.v.value('@TenderType','int') as TenderType, 
	     T.v.value('@TenderName','varchar(64)') as TenderName, 
	     T.v.value('@SalesDiscCode','varchar(64)') as SalesDiscCode, T.v.value('@SalesDiscDesc','varchar(512)') as SalesDiscDesc, 
	     T.v.value('@SalesDiscType','int') as SalesDiscType, T.v.value('@SalesDiscDataType','int') as SalesDiscDataType, 
	     T.v.value('@SalesDiscPrice','money') as SalesDiscPrice, T.v.value('@SalesPrice','money') as SalesPrice, 
	     T.v.value('@SalesDiscOffAmount','money') as SalesDiscOffAmount, T.v.value('@SalesDiscQty','int') as SalesDiscQty, 
	     T.v.value('@AffectNetPrice','int') as AffectNetPrice, T.v.value('@SalesDiscLevel','int') as SalesDiscLevel 
	  from @SalesDiscData.nodes('/ROOT/DISCOUNT') T(v) 
  end  

    -- 检查Busdate
    DECLARE @SODEODBUSDATE DATE
	SELECT TOP 1 @SODEODBusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0  ORDER BY BusDate desc
   -- SELECT TOP 1 @SODEODBusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate desc
    IF @SODEODBusDate is null
      RETURN -510        
    IF @BusDate IS NOT NULL
    BEGIN
      IF DATEDIFF(DD, @BusDate,@SODEODBusDate) <> 0
        RETURN -511
    END
    SET @BusDate = @SODEODBusDate
    --=======  
    if isnull(@TxnDate, 0) = 0 
      set @TxnDate = getdate()
    --======= 
  
  -- 开始插入数据    
  begin tran	   -- 开始事务
  
  begin try  
    -- 如果没有单号,则自动产生一个.  如果有输入，则检查这个txnno是否已经存在。如果已经存在的，则可以是update数据
    if isnull(@TxnNo, '') <> ''
      select @OldTxnStatus = Status from sales_H where TransNum = @TxnNo
    else begin
      exec GenNewTxnNo @StoreCode, @RegisterCode, @Busdate, @TxnNo output
    end  
    set @OldTxnStatus = isnull(@OldTxnStatus, -1)

 
  --写入PLU detail数据
  if isnull(@SalesDetail, '') <> ''
  begin
    if exists(select TransNum from sales_D where TransNum = @TxnNo)
      delete from sales_D where TransNum = @TxnNo 
    
    Insert into Sales_D(TransNum, SeqNo, TransType, StoreCode, RegisterCode, BusDate, TxnDate, ProdCode, ProdDesc, DepartCode,
        Qty, OrgPrice, UnitPrice, NetPrice, OrgAmount, UnitAmount, NetAmount, TotalQty, DiscountPrice, DiscountAmount, ExtraPrice,
        POReasonCode, Additional1, Additional2, Additional3, POPrice, RPriceTypeCode, IsBOM, IsCoupon, IsBuyBack, IsService, 
        SerialNoStockFlag, SerialNoType, SerialNo, IMEI, StockTypeCode, Collected,  ReservedDate, PickupLocation, PickupStaff, 
        DeliveryDate, DeliveryBy, ActPrice, ActAmount, OrgTransNum, OrgSeqNo, Remark, RefGUID, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn)                          
    select @TxnNo, SeqNo, @SalesType,@StoreCode, @RegisterCode, @BusDate, @TxnDate, A.ProdCode, A.ProdDesc, B.DepartCode, 
        A.Qty, A.RetailPrice, A.RetailPrice, A.NetPrice, A.Qty*A.RetailPrice, A.Qty*A.RetailPrice, A.NetAmount, A.Qty, A.NetPrice - A.RetailPrice, A.NetAmount - A.Qty*A.RetailPrice, 0,
        '', A.Additional, '', '', 0, '', B.BOM, B.CouponSKU, 0, B.VisuaItem,
        0, 1, Serialno, '', 'G', Collected, ReservedDate, B.DefaultPickupStoreCode, @UserID, 
        @DeliveryDate, @Deliveryer, 0, 0, '', '', '', '', @UserID, Getdate(), @UserID, Getdate()
    from @SalesD A left join Buy_Product B on A.ProdCode = B.ProdCode
  end  
  
  --写入Tender detail数据      
  if isnull(@SalesTender, '') <> ''
  begin
    if exists(select TransNum from sales_T where TransNum = @TxnNo)
      delete from sales_T where TransNum = @TxnNo    
    Insert into Sales_T(SeqNo, TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,TenderID,TenderCode,TenderDesc,
       TenderType,TenderAmount,LocalAmount,ExchangeRate,PaymentType,Additional,Status,PayConfirmDate,CardNumber,CardType,
       CardHolder,CardApprovalCode,CardExpiryDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    select SeqNo, @TxnNo, @SalesType,@StoreCode, @RegisterCode, @BusDate,@TxnDate, A.TenderID, A.TenderCode, A.TenderName, 
      B.TenderType, A.TenderAmount, A.LocalAmount, A.ExchangeRate, 0, A.SalesTAdditional, 1, @Busdate, A.SalesTAdditional, B.CardType, 
      '', '', '', GetDate(), @UserID, GetDate(),@UserID 
    from @SalesT A left join BUY_CURRENCY B on A.TenderID = B.CurrencyID
  end

  --写入Discount detail数据
  if isnull(@SalesDisc, '') <> ''
  begin
    if exists(select TransNum from sales_Disc where TransNum = @TxnNo)
      delete from sales_Disc where TransNum = @TxnNo    
    Insert into sales_Disc(TransNum, ItemSeqNo, ProdCode, TenderSeqNo, TenderID, TenderCode, SalesDiscCode, SalesDiscDesc,  
      SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, 
      SalesDiscLevel,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)      
    select @TxnNo, ItemSeqNo, ProdCode, TenderSeqNo, TenderID, TenderCode, SalesDiscCode, SalesDiscDesc,  
      SalesDiscType, SalesDiscDataType, SalesDiscPrice, SalesPrice, SalesDiscOffAmount, SalesDiscQty, AffectNetPrice, 
      SalesDiscLevel, GetDate(), @UserID, GetDate(),@UserID 
    from @SalesDS A 
  end
  
  -- 写入sales_H数据    
  if isnull(@OldTxnStatus, -1) <> -1
  begin 
    if exists(select TransNum from Sales_H where TransNum = @TxnNo)     
    begin
      update sales_H set [Status] = @TxnStatus, SalesReceipt = @SalesReceipt, 
            UpdatedOn = getdate(), UpdatedBy = @UserID, SalesReceiptBIN = @SalesReceiptBIN,
            DeliveryNumber = @DeliveryNumber
       where TransNum = @TxnNo
    end   
  end else
  begin     
    insert into sales_H(TransNum,TransType,StoreCode,RegisterCode,BusDate,TxnDate,CashierID,SalesManID,TotalAmount,Status,
      TransDiscount,TransDiscountType,TransReason,RefTransNum,InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,DeliveryFlag,
      DeliveryCountry,DeliveryProvince,DeliveryCity,DeliveryDistrict,DeliveryAddressDetail,DeliveryFullAddress,DeliveryNumber,
      RequestDeliveryDate,DeliveryDate,DeliveryBy,Contact,ContactPhone,PickupType,PickupStoreCode,CODFlag,Remark,SettlementDate,
      SettlementStaffID,PaySettleDate,SalesReceipt, SalesReceiptBIN ,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    values
      (@TxnNo, @SalesType,@StoreCode, @RegisterCode, @BusDate, @TxnDate, @CashierID, @SalesManID, @TotalAmount, @TxnStatus, 
       0, 0, '', @RefTxnNo, 0, @MemberSalesFlag, @MemberID, @CardNumber,  @DeliveryFlag, 
       '', '', '', '', '', @DeliveryaAddr, @DeliveryNumber, 
       @DeliveryDate, @DeliveryCompleteDate, @Deliveryer, @CustomerName, @CustomerPhone, @PickupType, @PickupStoreCode, @CODFlag, '', null,
       0, getdate(), @SalesReceipt, @SalesReceiptBIN,  getdate(), @UserID, getdate(), @UserID)	
  end
  
  -- 如果是Void交易,必须更改原单.
  if @SalesType = 4
  begin
    Update Sales_H set InvalidateFlag = 1, RefTransNum = @TxnNo, UpdatedOn = GETDATE(), UpdatedBy = @CashierID 
      where TransNum = @RefTxnNo
  end
  -- 如果是Refund交易,必须更改原单.
  if @SalesType = 5
  begin
    Update Sales_H set InvalidateFlag = 2, RefTransNum = @TxnNo, UpdatedOn = GETDATE(), UpdatedBy = @CashierID 
      where TransNum = @RefTxnNo
  end
    
  -- 非购物车, 首次完成支付或者交易完成.
  if @TxnStatus = 5	and @OldTxnStatus < 4
  begin
    -- 扣减库存。
    if exists(select * from @SalesD where Collected = 4)
    begin
      set @stocktypecode = 'G'
      select @StoreID = StoreID from BUY_STORE where StoreCode = @StoreCode
      set @StockTypeID = ISNULL(@StockTypeID, 0)
      set @StoreID = ISNULL(@StoreID, 0)
      insert into STK_StockMovement(OprID,StoreID,StockTypeCode,ProdCode,ReferenceNo,ReferenceNo_Other,BusDate,TxnDate,
          OpenQty,ActQty,CloseQty,SerialNoType, SerialNo,ApprovalCode,CreatedOn,CreatedBy)
      select 4, @StoreID, @StockTypeCode, A.ProdCode, A.TxnNo, '', @Busdate, @Txndate,
          ISNULL(B.OnhandQty, 0), -ISNULL(A.Qty,0), ISNULL(B.OnhandQty,0) - ISNULL(A.Qty,0), 1, A.Serialno, @ApprovalCode, GETDATE(), @UserID  
      from @SalesD A left join (select * from STK_StockOnhand where StoreID = @StoreID and StockTypeCode = @StockTypeCode) B 
                        on A.ProdCode = B.ProdCode  
    end
  end

  commit tran
  
  end try
  begin catch
    
    rollback tran
  end catch 

         
  return @Result
end

GO
