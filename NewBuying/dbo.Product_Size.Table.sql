USE [NewBuying]
GO
/****** Object:  Table [dbo].[Product_Size]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Size](
	[ProductSizeID] [int] NOT NULL,
	[ProductSizeType] [varchar](64) NOT NULL,
	[ProductSizeCode] [varchar](64) NOT NULL,
	[ProductSizeName1] [nvarchar](512) NULL,
	[ProductSizeName2] [nvarchar](512) NULL,
	[ProductSizeName3] [nvarchar](512) NULL,
	[ProductSizeNote] [nvarchar](512) NULL,
 CONSTRAINT [PK_PRODUCT_SIZE] PRIMARY KEY CLUSTERED 
(
	[ProductSizeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size', @level2type=N'COLUMN',@level2name=N'ProductSizeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size', @level2type=N'COLUMN',@level2name=N'ProductSizeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size', @level2type=N'COLUMN',@level2name=N'ProductSizeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size', @level2type=N'COLUMN',@level2name=N'ProductSizeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size', @level2type=N'COLUMN',@level2name=N'ProductSizeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size', @level2type=N'COLUMN',@level2name=N'ProductSizeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size', @level2type=N'COLUMN',@level2name=N'ProductSizeNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产品尺寸属性。
@2016--09-01  唯一索引Index_ProductSizeCode 改为， ProductSizeCode + ProductSizeType 才是唯一。
    去掉自增长主键。 改成从 buying DB 那里导入。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product_Size'
GO
