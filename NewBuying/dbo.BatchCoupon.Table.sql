USE [NewBuying]
GO
/****** Object:  Table [dbo].[BatchCoupon]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BatchCoupon](
	[BatchCouponID] [int] IDENTITY(1,1) NOT NULL,
	[BatchCouponCode] [varchar](64) NOT NULL,
	[SeqFrom] [bigint] NULL,
	[SeqTo] [bigint] NULL,
	[Qty] [int] NOT NULL DEFAULT ((1)),
	[CouponTypeID] [int] NOT NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[TagDesignCode] [varchar](64) NULL,
	[Remark] [varchar](512) NULL,
 CONSTRAINT [PK_BatchCoupon] PRIMARY KEY CLUSTERED 
(
	[BatchCouponID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCoupon', @level2type=N'COLUMN',@level2name=N'BatchCouponID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次ID。 组成规则： CardTypeID（或CouponTypeID） + 序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCoupon', @level2type=N'COLUMN',@level2name=N'BatchCouponCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号范围（开始序号）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCoupon', @level2type=N'COLUMN',@level2name=N'SeqFrom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号范围（结束序号）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCoupon', @level2type=N'COLUMN',@level2name=N'SeqTo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次创建数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCoupon', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCoupon', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon批次表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BatchCoupon'
GO
