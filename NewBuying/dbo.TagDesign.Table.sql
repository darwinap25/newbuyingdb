USE [NewBuying]
GO
/****** Object:  Table [dbo].[TagDesign]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TagDesign](
	[TagDesignID] [int] IDENTITY(1,1) NOT NULL,
	[TagDesignCode] [varchar](64) NULL,
	[TagDesignName1] [nvarchar](512) NULL,
	[TagDesignName2] [nvarchar](512) NULL,
	[TagDesignName3] [nvarchar](512) NULL,
	[TagDesignDesc1] [nvarchar](512) NULL,
	[TagDesignDesc2] [nvarchar](512) NULL,
	[TagDesignDesc3] [nvarchar](512) NULL,
	[TagImageURL] [nvarchar](500) NULL,
	[TagThumbURL] [nvarchar](500) NULL,
	[TagDesignTypeCode] [varchar](64) NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[TagDesignIsActive] [int] NULL,
 CONSTRAINT [PK_TagDesign] PRIMARY KEY CLUSTERED 
(
	[TagDesignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
