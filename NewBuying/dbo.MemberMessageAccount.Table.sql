USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberMessageAccount]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberMessageAccount](
	[MessageAccountID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[MessageServiceTypeID] [int] NOT NULL,
	[AccountNumber] [nvarchar](512) NOT NULL,
	[Status] [int] NOT NULL DEFAULT ((1)),
	[Note] [nvarchar](512) NULL,
	[IsPrefer] [int] NULL DEFAULT ((0)),
	[TokenUID] [varchar](64) NULL,
	[TokenStr] [varchar](512) NULL,
	[TokenUpdateDate] [datetime] NULL,
	[PromotionFlag] [int] NULL DEFAULT ((0)),
	[VerifyFlag] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MEMBERMESSAGEACCOUNT] PRIMARY KEY CLUSTERED 
(
	[MessageAccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Upate_MemberMessageAccount]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Upate_MemberMessageAccount] ON [dbo].[MemberMessageAccount]
FOR INSERT,UPDATE
AS
/*==============================================================*/
/*                
* Name: Upate_MemberMessageAccount
* Version: 1.0.0.0
* Description :验证邮箱和手机后更新Member的NewMemberFlag标识
*  
** Create By Gavin @2014-08-06
*/
/*==============================================================*/
BEGIN  
  declare @VerifyFlag int, @MemberID int, @MessageServiceTypeID int
  
  DECLARE CUR_MemberMessageAccount CURSOR fast_forward FOR
    SELECT  VerifyFlag, MemberID, MessageServiceTypeID FROM INSERTED  
  OPEN CUR_MemberMessageAccount
  FETCH FROM CUR_MemberMessageAccount INTO @VerifyFlag, @MemberID, @MessageServiceTypeID
  WHILE @@FETCH_STATUS=0
  BEGIN    
    if @VerifyFlag = 1
    begin
      if @MessageServiceTypeID = 1
        if exists(select * from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2 and VerifyFlag = 1)
          update Member set NewMemberFlag = 0 where MemberID = @MemberID and NewMemberFlag = 1
      if @MessageServiceTypeID = 2
        if exists(select * from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 1 and VerifyFlag = 1)
          update Member set NewMemberFlag = 0 where MemberID = @MemberID and NewMemberFlag = 1
    end         
    FETCH FROM CUR_MemberMessageAccount INTO @VerifyFlag, @MemberID, @MessageServiceTypeID
  END
  CLOSE CUR_MemberMessageAccount 
  DEALLOCATE CUR_MemberMessageAccount       
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'MessageAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MessageServiceType 表外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'AccountNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。0：无效。 1：生效。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金额积分变动时，是否使用此账号发送消息。0: 不使用。 1：使用。 默认0。 
一个会员只能有一个 IsPrefer=1的账号。  可以全部为0.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'IsPrefer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录网站的UID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'TokenUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录网站的Token' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'TokenStr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录网站的Token的更新' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'TokenUpdateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此账号是否接收优惠消息。默认0，不接收。 
（这个功能当前只是做记录。不做实际的业务逻辑操作）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'PromotionFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否经过验证。 0：没有。 1：已经验证。  默认为0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount', @level2type=N'COLUMN',@level2name=N'VerifyFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员消息服务账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberMessageAccount'
GO
