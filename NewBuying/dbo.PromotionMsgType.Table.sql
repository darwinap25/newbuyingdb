USE [NewBuying]
GO
/****** Object:  Table [dbo].[PromotionMsgType]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionMsgType](
	[PromotionMsgTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[BrandID] [int] NULL,
	[PromotionMsgTypeName1] [nvarchar](512) NULL,
	[PromotionMsgTypeName2] [nvarchar](512) NULL,
	[PromotionMsgTypeName3] [nvarchar](512) NULL,
 CONSTRAINT [PK_PROMOTIONMSGTYPE] PRIMARY KEY CLUSTERED 
(
	[PromotionMsgTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsgType', @level2type=N'COLUMN',@level2name=N'PromotionMsgTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属父的PromotionMsgTypeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsgType', @level2type=N'COLUMN',@level2name=N'ParentID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsgType', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsgType', @level2type=N'COLUMN',@level2name=N'PromotionMsgTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsgType', @level2type=N'COLUMN',@level2name=N'PromotionMsgTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsgType', @level2type=N'COLUMN',@level2name=N'PromotionMsgTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息类型表。
1.1： 改成树状结构，增加ParentID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromotionMsgType'
GO
