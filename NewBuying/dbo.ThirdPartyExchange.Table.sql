USE [NewBuying]
GO
/****** Object:  Table [dbo].[ThirdPartyExchange]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThirdPartyExchange](
	[ThirdPartyExchangeID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyID] [int] NULL,
	[CardGradeID] [int] NULL,
	[CardAMTToThirdAMT] [decimal](16, 6) NULL,
	[CardAMTToThirdPoint] [decimal](16, 6) NULL,
	[CardPointToThirdAMT] [decimal](16, 6) NULL,
	[CardPointToThirdPoint] [decimal](16, 6) NULL,
	[ThirdAMTToCardAMT] [decimal](16, 6) NULL,
	[ThirdPointToCardAMT] [decimal](16, 6) NULL,
	[ThirdAMTToCardPoint] [decimal](16, 6) NULL,
	[ThirdPointToCardPoint] [decimal](16, 6) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_THIRDPARTYEXCHANGE] PRIMARY KEY CLUSTERED 
(
	[ThirdPartyExchangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[ThirdPartyExchange] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ThirdPartyExchange] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'ThirdPartyExchangeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三方ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'ThirdPartyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CardGradeID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SVA Card金额转换对方卡金额的比率X。 SVA*X=THIRD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'CardAMTToThirdAMT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SVA Card金额转换对方卡积分的比率X。 SVA*X=THIRD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'CardAMTToThirdPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SVA Card积分转换对方卡金额的比率X。 SVA*X=THIRD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'CardPointToThirdAMT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SVA Card积分转换对方卡积分的比率X。 SVA*X=THIRD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'CardPointToThirdPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对方卡金额转换SVA Card金额的比率X。 THIRD*X=SVA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'ThirdAMTToCardAMT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对方卡积分转换SVA Card金额的比率X。 THIRD*X=SVA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'ThirdPointToCardAMT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对方卡金额转换SVA Card积分的比率X。 THIRD*X=SVA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'ThirdAMTToCardPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对方卡积分转换SVA Card积分的比率X。 THIRD*X=SVA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange', @level2type=N'COLUMN',@level2name=N'ThirdPointToCardPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三方兑换设置。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThirdPartyExchange'
GO
