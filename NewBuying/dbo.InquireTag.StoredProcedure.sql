USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[InquireTag]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/16 18:33:18
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[InquireTag]
    @PageNumber		    int,
	@RowsperPage		int,
    @airline			varchar(512),
	@tagdesign			varchar(512),
	@tagtype			varchar(512),
	@tagbatchcode		varchar(64),
	@tagid				varchar(64),
	@status				int,
	@createdatefrom			datetime,
	@createdateto			datetime,
    @userid             int,
    @orderby            [varchar](50),
    @sortdirection      [varchar](10)
AS
SET NOCOUNT ON;

BEGIN


    DECLARE @FirstRec int, @LastRec int
        
    
    SELECT @FirstRec = (@PageNumber - 1) * @RowsperPage
	SELECT @LastRec = (@PageNumber * @RowsperPage + 1);
    
    DECLARE @charstatus [char] (1);
    
    SET @charstatus = (SELECT CASE
                        WHEN @status = -1 THEN (SELECT [c].[Status] FROM [dbo].[Coupon] c WHERE [c].[CouponNumber] =@tagid)
                        ELSE CAST(@status AS char)
                        END
                        );
                        
	IF @sortdirection = 'DESC'
        BEGIN
        	IF @charstatus IS NOT NULL
        		BEGIN
        
        			With TempResult AS
        				(        
        			
        					Select ROW_NUMBER() OVER(ORDER BY
                                                           CASE @orderby
                                                            WHEN 'TagID' THEN [c].[CouponNumber]
                                                            WHEN 'TagType' THEN [ct].[CouponTypeName1]
                                                            WHEN 'Airline' THEN [b].[BrandName1]
                                                            WHEN 'TagBatchCode,' THEN [bc].[BatchCouponCode]
                                                            WHEN 'CreatedOn' THEN CAST([c].[CreatedOn] AS CHAR)
                                                            WHEN 'TagDesign' THEN [td].[TagDesignDesc1] 
                                                            WHEN 'TagDesignType' THEN [tdt].[TagDesignTypeDesc1]
                                                            WHEN 'Status' THEN CASE  
                                                    							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
                                                    							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
                                                    							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
                                                    							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
                                                    							END                                                            
                                                            ELSE [c].[CouponNumber]
                                                            END DESC) AS RowNum,
        						@charstatus AS charstatus,
        						[c].[CouponNumber] AS TagID,			
        						[ct].[CouponTypeName1] AS TagType,
        						[b].[BrandName1] AS Airline,
        						[bc].[BatchCouponCode] AS TagBatchCode,
        						--[c].[Status] AS [Status],
        						CASE  
        							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
        							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
        							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
        							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
        							END AS [Status],		
        						[c].[CreatedOn] AS CreatedOn,
        						[td].[TagDesignDesc1] AS [TagDesign],
        						[tdt].[TagDesignTypeDesc1] AS [TagDesignType],
        						(SELECT COUNT(1) FROM [dbo].[Coupon] c
        								INNER JOIN [dbo].[BatchCoupon] bc
        									ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        								INNER JOIN [dbo].[CouponType] ct 
        									ON [ct].[CouponTypeID] = [bc].[CouponTypeID]
        								INNER JOIN [dbo].[Brand] b
        									ON [b].[BrandCode] = [ct].[BrandCode]
        								INNER JOIN dbo.TagDesignType tdt
        									on b.BrandCode = tdt.BrandCode
        								INNER JOIN dbo.TagDesign td
        									on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        								INNER JOIN [dbo].[UserAirlineMap] u
        									ON [u].[AirlineCode] = [b].[BrandCode]
                            
        							WHERE [b].[BrandName1] LIKE '%' + @airline + '%'  
        								AND td.TagDesignName1 like  '%' + @tagdesign + '%'      
        								AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        								AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        								AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        							   AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%' 
        								AND [u].[UserID] = @userid
        						        AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        								) AS [Count]
        					FROM [dbo].[Coupon] c
        						INNER JOIN [dbo].[BatchCoupon] bc
        							ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        						INNER JOIN [dbo].[CouponType] ct 
        							ON [ct].[CouponTypeID] = [c].[CouponTypeID]
        						INNER JOIN [dbo].[Brand] b
        							ON [b].[BrandCode] = [ct].[BrandCode]
        						INNER JOIN dbo.TagDesignType tdt
        							on b.BrandCode = tdt.BrandCode
        						INNER JOIN dbo.TagDesign td
        							on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        						INNER JOIN [dbo].[UserAirlineMap] u
        							ON [u].[AirlineCode] = [b].[BrandCode]
        					WHERE [b].[BrandName1] LIKE '%' + @airline + '%'       
        						AND td.TagDesignName1 like  '%' + @tagdesign + '%'       
        						AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        						AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        						AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        						AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%'  
        						AND [u].[UserID] = @userid
        					    AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        				)
        
        			   SELECT top (@LastRec-1) *
                					FROM TempResult
                					WHERE RowNum > @FirstRec 
                					AND RowNum < @LastRec  
                            
                 END
        		  
            ELSE
        		BEGIN
        
        			With TempResult AS
        				(        
        			
        					Select ROW_NUMBER() OVER(ORDER BY
                                                           CASE @orderby
                                                            WHEN 'TagID' THEN [c].[CouponNumber]
                                                            WHEN 'TagType' THEN [ct].[CouponTypeName1]
                                                            WHEN 'Airline' THEN [b].[BrandName1]
                                                            WHEN 'TagBatchCode,' THEN [bc].[BatchCouponCode]
                                                            WHEN 'CreatedOn' THEN CAST([c].[CreatedOn] AS CHAR)
                                                            WHEN 'TagDesign' THEN [td].[TagDesignDesc1] 
                                                            WHEN 'TagDesignType' THEN [tdt].[TagDesignTypeDesc1]
                                                            WHEN 'Status' THEN CASE  
                                                    							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
                                                    							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
                                                    							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
                                                    							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
                                                    							END                                                            
                                                            ELSE [c].[CouponNumber]
                                                            END DESC) AS RowNum,
        						@charstatus AS charstatus,
        						[c].[CouponNumber] AS TagID,			
        						[ct].[CouponTypeName1] AS TagType,
        						[b].[BrandName1] AS Airline,
        						[bc].[BatchCouponCode] AS TagBatchCode,
        						--[c].[Status] AS [Status],
        						CASE  
        							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
        							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
        							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
        							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
        							END AS [Status],		
        						[c].[CreatedOn] AS CreatedOn,
        						[td].[TagDesignDesc1] AS [TagDesign],
        						[tdt].[TagDesignTypeDesc1] AS [TagDesignType],
        						(SELECT COUNT(1) FROM [dbo].[Coupon] c
        								INNER JOIN [dbo].[BatchCoupon] bc
        									ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        								INNER JOIN [dbo].[CouponType] ct 
        									ON [ct].[CouponTypeID] = [bc].[CouponTypeID]
        								INNER JOIN [dbo].[Brand] b
        									ON [b].[BrandCode] = [ct].[BrandCode]
        								INNER JOIN dbo.TagDesignType tdt
        									on b.BrandCode = tdt.BrandCode
        								INNER JOIN dbo.TagDesign td
        									on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        								INNER JOIN [dbo].[UserAirlineMap] u
        									ON [u].[AirlineCode] = [b].[BrandCode]
                            
        							WHERE [b].[BrandName1] LIKE '%' + @airline + '%'  
        								AND td.TagDesignName1 like  '%' + @tagdesign + '%'      
        								AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        								AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        								AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        							--   AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%' 
        								AND [u].[UserID] = @userid
        						        AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        								) AS [Count]
        					FROM [dbo].[Coupon] c
        						INNER JOIN [dbo].[BatchCoupon] bc
        							ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        						INNER JOIN [dbo].[CouponType] ct 
        							ON [ct].[CouponTypeID] = [c].[CouponTypeID]
        						INNER JOIN [dbo].[Brand] b
        							ON [b].[BrandCode] = [ct].[BrandCode]
        						INNER JOIN dbo.TagDesignType tdt
        							on b.BrandCode = tdt.BrandCode
        						INNER JOIN dbo.TagDesign td
        							on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        						INNER JOIN [dbo].[UserAirlineMap] u
        							ON [u].[AirlineCode] = [b].[BrandCode]
        					WHERE [b].[BrandName1] LIKE '%' + @airline + '%'       
        						AND td.TagDesignName1 like  '%' + @tagdesign + '%'       
        						AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        						AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        						AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        						--AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%'  
        						AND [u].[UserID] = @userid
        					    AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        				)
        
        			   SELECT top (@LastRec-1) *
                					FROM TempResult
                					WHERE RowNum > @FirstRec 
                					AND RowNum < @LastRec  
                            
                 END         
                   
        END
    ELSE

        BEGIN
        	IF @charstatus IS NOT NULL
        		BEGIN
        
        			With TempResult AS
        				(        
        			
        					Select ROW_NUMBER() OVER(ORDER BY
                                                           CASE @orderby
                                                            WHEN 'TagID' THEN [c].[CouponNumber]
                                                            WHEN 'TagType' THEN [ct].[CouponTypeName1]
                                                            WHEN 'Airline' THEN [b].[BrandName1]
                                                            WHEN 'TagBatchCode,' THEN [bc].[BatchCouponCode]
                                                            WHEN 'CreatedOn' THEN CAST([c].[CreatedOn] AS CHAR)
                                                            WHEN 'TagDesign' THEN [td].[TagDesignDesc1] 
                                                            WHEN 'TagDesignType' THEN [tdt].[TagDesignTypeDesc1]
                                                            WHEN 'Status' THEN CASE  
                                                    							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
                                                    							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
                                                    							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
                                                    							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
                                                    							END
                                                            ELSE [c].[CouponNumber]
                                                            END ASC) AS RowNum,
        						@charstatus AS charstatus,
        						[c].[CouponNumber] AS TagID,			
        						[ct].[CouponTypeName1] AS TagType,
        						[b].[BrandName1] AS Airline,
        						[bc].[BatchCouponCode] AS TagBatchCode,
        						--[c].[Status] AS [Status],
        						CASE  
        							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
        							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
        							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
        							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
        							END AS [Status],		
        						[c].[CreatedOn] AS CreatedOn,
        						[td].[TagDesignDesc1] AS [TagDesign],
        						[tdt].[TagDesignTypeDesc1] AS [TagDesignType],
        						(SELECT COUNT(1) FROM [dbo].[Coupon] c
        								INNER JOIN [dbo].[BatchCoupon] bc
        									ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        								INNER JOIN [dbo].[CouponType] ct 
        									ON [ct].[CouponTypeID] = [bc].[CouponTypeID]
        								INNER JOIN [dbo].[Brand] b
        									ON [b].[BrandCode] = [ct].[BrandCode]
        								INNER JOIN dbo.TagDesignType tdt
        									on b.BrandCode = tdt.BrandCode
        								INNER JOIN dbo.TagDesign td
        									on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        								INNER JOIN [dbo].[UserAirlineMap] u
        									ON [u].[AirlineCode] = [b].[BrandCode]
                            
        							WHERE [b].[BrandName1] LIKE '%' + @airline + '%'  
        								AND td.TagDesignName1 like  '%' + @tagdesign + '%'      
        								AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        								AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        								AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        							   AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%' 
        								AND [u].[UserID] = @userid
        						        AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        								) AS [Count]
        					FROM [dbo].[Coupon] c
        						INNER JOIN [dbo].[BatchCoupon] bc
        							ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        						INNER JOIN [dbo].[CouponType] ct 
        							ON [ct].[CouponTypeID] = [c].[CouponTypeID]
        						INNER JOIN [dbo].[Brand] b
        							ON [b].[BrandCode] = [ct].[BrandCode]
        						INNER JOIN dbo.TagDesignType tdt
        							on b.BrandCode = tdt.BrandCode
        						INNER JOIN dbo.TagDesign td
        							on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        						INNER JOIN [dbo].[UserAirlineMap] u
        							ON [u].[AirlineCode] = [b].[BrandCode]
        					WHERE [b].[BrandName1] LIKE '%' + @airline + '%'       
        						AND td.TagDesignName1 like  '%' + @tagdesign + '%'       
        						AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        						AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        						AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        						AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%'  
        						AND [u].[UserID] = @userid
        					    AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        				)
        
        			   SELECT top (@LastRec-1) *
                					FROM TempResult
                					WHERE RowNum > @FirstRec 
                					AND RowNum < @LastRec  
                            
                 END
        		  
            ELSE
        		BEGIN
        
        			With TempResult AS
        				(        
        			
        					Select ROW_NUMBER() OVER(ORDER BY
                                                           CASE @orderby
                                                            WHEN 'TagID' THEN [c].[CouponNumber]
                                                            WHEN 'TagType' THEN [ct].[CouponTypeName1]
                                                            WHEN 'Airline' THEN [b].[BrandName1]
                                                            WHEN 'TagBatchCode,' THEN [bc].[BatchCouponCode]
                                                            WHEN 'CreatedOn' THEN CAST([c].[CreatedOn] AS CHAR)
                                                            WHEN 'TagDesign' THEN [td].[TagDesignDesc1] 
                                                            WHEN 'TagDesignType' THEN [tdt].[TagDesignTypeDesc1]
                                                            WHEN 'Status' THEN CASE  
                                                    							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
                                                    							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
                                                    							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
                                                    							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
                                                    							END
                                                            ELSE [c].[CouponNumber]
                                                            END ASC) AS RowNum,
        						@charstatus AS charstatus,
        						[c].[CouponNumber] AS TagID,			
        						[ct].[CouponTypeName1] AS TagType,
        						[b].[BrandName1] AS Airline,
        						[bc].[BatchCouponCode] AS TagBatchCode,
        						--[c].[Status] AS [Status],
        						CASE  
        							WHEN [c].[Status] = 0 THEN 'ENROLLED' --'REGISTERED'
        							WHEN [c].[Status] = 1 THEN 'ISSUED' --'ACTIVE'
        							WHEN [c].[Status] = 2 THEN 'VOID' --'UNREGISTERED'
        							WHEN [c].[Status] = 3 THEN 'REPORT LOST'
        							END AS [Status],		
        						[c].[CreatedOn] AS CreatedOn,
        						[td].[TagDesignDesc1] AS [TagDesign],
        						[tdt].[TagDesignTypeDesc1] AS [TagDesignType],
        						(SELECT COUNT(1) FROM [dbo].[Coupon] c
        								INNER JOIN [dbo].[BatchCoupon] bc
        									ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        								INNER JOIN [dbo].[CouponType] ct 
        									ON [ct].[CouponTypeID] = [bc].[CouponTypeID]
        								INNER JOIN [dbo].[Brand] b
        									ON [b].[BrandCode] = [ct].[BrandCode]
        								INNER JOIN dbo.TagDesignType tdt
        									on b.BrandCode = tdt.BrandCode
        								INNER JOIN dbo.TagDesign td
        									on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        								INNER JOIN [dbo].[UserAirlineMap] u
        									ON [u].[AirlineCode] = [b].[BrandCode]
                            
        							WHERE [b].[BrandName1] LIKE '%' + @airline + '%'  
        								AND td.TagDesignName1 like  '%' + @tagdesign + '%'      
        								AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        								AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        								AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        							--   AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%' 
        								AND [u].[UserID] = @userid
        						        AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        								) AS [Count]
        					FROM [dbo].[Coupon] c
        						INNER JOIN [dbo].[BatchCoupon] bc
        							ON [bc].[BatchCouponID] = [c].[BatchCouponID]
        						INNER JOIN [dbo].[CouponType] ct 
        							ON [ct].[CouponTypeID] = [c].[CouponTypeID]
        						INNER JOIN [dbo].[Brand] b
        							ON [b].[BrandCode] = [ct].[BrandCode]
        						INNER JOIN dbo.TagDesignType tdt
        							on b.BrandCode = tdt.BrandCode
        						INNER JOIN dbo.TagDesign td
        							on tdt.TagDesignTypeCode = td.TagDesignTypeCode AND [td].[TagDesignCode] = [bc].[TagDesignCode]
        						INNER JOIN [dbo].[UserAirlineMap] u
        							ON [u].[AirlineCode] = [b].[BrandCode]
        					WHERE [b].[BrandName1] LIKE '%' + @airline + '%'       
        						AND td.TagDesignName1 like  '%' + @tagdesign + '%'       
        						AND [ct].[CouponTypeName1] LIKE '%' + @tagtype + '%'       
        						AND [bc].[BatchCouponCode] LIKE '%' + @tagbatchcode + '%'       
        						AND [c].[CouponNumber]  LIKE '%' + @tagid + '%'       
        						--AND CAST([c].[Status] AS varchar) LIKE '%' + isnull(@charstatus,'')  + '%'  
        						AND [u].[UserID] = @userid
        					    AND CAST([c].[CreatedOn] AS DATE) BETWEEN @createdatefrom AND @createdateto
        				)
        
        			   SELECT top (@LastRec-1) *
                					FROM TempResult
                					WHERE RowNum > @FirstRec 
                					AND RowNum < @LastRec  
                            
                 END         
                   
        END    
    
END








GO
