USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardGradeHoldCouponRule]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardGradeHoldCouponRule](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[HoldCount] [int] NULL DEFAULT ((0)),
	[RuleType] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CARDGRADEHOLDCOUPONRULE] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeHoldCouponRule', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡等级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeHoldCouponRule', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'coupontypeid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeHoldCouponRule', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许持有数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeHoldCouponRule', @level2type=N'COLUMN',@level2name=N'HoldCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0：HoldCount为，本级别卡允许持有数量。 
1：HoldCount为，升级到本级别时，赠送的数量。以及，新增会员获得这个CardGardeID时，赠送的数量
2：HoldCount为，用户允许下载的最大coupon数量。（这里指从Kiosk 中earncoupon 的最大数量，不包括转赠等方式获得的） (不考虑card的status。)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeHoldCouponRule', @level2type=N'COLUMN',@level2name=N'RuleType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每个CardGrade 允许持有的指定 coupontype 的数量。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardGradeHoldCouponRule'
GO
