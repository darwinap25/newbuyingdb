USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportMember_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportMember_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ImportMemberNumber] [varchar](64) NOT NULL,
	[RRCCardNumber] [varchar](64) NULL,
	[OLDCardNumber] [varchar](64) NULL,
	[OLDRRCCardNumber] [varchar](64) NULL,
	[LName] [varchar](512) NULL,
	[FName] [varchar](512) NULL,
	[VPPTS] [varchar](64) NULL,
	[RetailID] [varchar](64) NULL,
	[StoreID] [varchar](64) NULL,
	[CardStatus] [int] NULL,
 CONSTRAINT [PK_ORD_IMPORTMEMBER_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_D', @level2type=N'COLUMN',@level2name=N'ImportMemberNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新的卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_D', @level2type=N'COLUMN',@level2name=N'RRCCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ord_ImportMember_H的子表。 当导入Member数据时, 那么这个子表不需要填充。
当导入Card数据时，需要保存导入的数据。
（根据CSV文件中的字段名来判断。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_D'
GO
