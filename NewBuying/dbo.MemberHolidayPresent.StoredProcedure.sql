USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberHolidayPresent]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[MemberHolidayPresent]
  @UserID				varchar(512), 
  @CardNumber			varchar(512),
  @ReceiveMemberRegisterMobile varchar(512),  
  @MessageType			int, 
  @MessageTitle			nvarchar(512),
  @MessageContent		nvarchar(max), 
  @MessageReceiveMode	int, 
  @SNSTypeID			int,
  @UsefulExpressionsID	int,    
  @ActAmount			money,
  @ActPoints			int,
  @ActCouponNumbers		varchar(max),
  @ActionDate			datetime,
  @Remark				nvarchar(512),
  @StoreCode			varchar(512),
  @RegisterCode			varchar(512),
  @ServerCode			varchar(512),
  @BrandCode			varchar(512),
  @ReturnAmount			money output,
  @ReturnPoints			int output,
  @ReceiveCardNumber	varchar(512) output,
  @ActCoupons			int output,
  @TxnNo				varchar(512),
  @TxnNoKeyID			varchar(512),
  @Busdate				datetime = 0,
  @TxnDate				datetime = 0,
  @SecurityCode         varchar(512),			-- 签名
  @SendMemberID         int output,
  @ReceiveMemberID      int output,
  @RemainCouponCount	int output,         -- 赠送后剩余的Coupon数量。
  @ReturnMessageStr		varchar(max) output,	-- 返回信息。（json格式） 
  @ReceiveAccountType   int=0               --0：表示输入的@ReceiveMemberRegisterMobile 内容是 register号码。1：表示输入的是 MemberEmail。  2： 表示输入的是MemberMobile	 
AS
/****************************************************************************
**  Name : MemberHolidayPresent
**  Version: 1.0.0.16
**  Description : 执行会员假日转赠.(包括发送消息和执行转赠动作。可以运行定时发送（扣除部分为即时执行）)
**
**  Parameter :
  declare @ReturnAmount money, @ReturnPoints int, @return int, @ReceiveCardNumber	varchar(512), @ActCoupons int, @S int, @R int
  declare @RemainCouponCount int, @ReturnMessageStr varchar(max)
  exec @return = MemberHolidayPresent '1', '000100001', '0863445', 1, 'test', 'test message', 1, 0,0, 50, 0, '', 
    '2012-05-11', '', '11','','', '1231235555', @ReturnAmount output, @ReturnPoints output, @ReceiveCardNumber output, 
    @ActCoupons output, '987654321', '987654321001', '2012-06-08','2012-06-08 12:21:12', '', @S output, @R output,
    @RemainCouponCount output, @ReturnMessageStr output, 2
  print  @return
  print  @ReturnAmount
  print  @ReturnPoints
  print @ReceiveCardNumber
  print @S
  Print @R
  print @RemainCouponCount 
  print @ReturnMessageStr 
 SecurityCode='000030000030000030000030000030000030000030000033000032000030',
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120) + cast(cast(floor(@ActAmount) as int) as varchar)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
   sp_helptext CheckSecurityCode
  update card set status = 5 where status = 0
  update Card set totalamount = 100000 where CardNumber = '000100001'
  select * from member where memberregistermobile = '13482842734' 
  delete from member where memberid = 27 
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**  Modify by Gavin @2012-08-27 (ver 1.0.0.1) 解决判断转赠积分和金额范围的问题。（金额和积分在判断范围时，需要转成正数）
**  Modify by: Gavin @2012-10-22 （ver 1.0.0.2）不传入Busdate，由存储过程自己获取
**  Modify by: Gavin @2013-01-31 （ver 1.0.0.3）增加两个memberid的返回，发送消息过程需要使用。
**  Modify by: Gavin @2013-08-26 （ver 1.0.0.4）增加返回参数，返回操作信息字符串
**  Modify by: Gavin @2013-08-26 （ver 1.0.0.5）增加返回Member 的剩余coupon 数量。（不区分coupontype，只要属于此member的所有status=2 的coupon数量）
**  Modify by: Gavin @2014-01-02 （ver 1.0.0.6）JSON增加返回CardNumber。
**  Modify by: Gavin @2014-06-26 （ver 1.0.0.7）增加参数@ReceiveAccountType.
**  Modify by: Gavin @2014-08-14 （ver 1.0.0.8）(for 711)如果遇到相同电话号码的情况,取最新update的那个Member
**  Modify by: Gavin @2014-08-26 （ver 1.0.0.9）(for 711) King要求转赠时不校验金额和积分的上限, 允许接收方超过上限。
**  Modify by: Gavin @2014-08-27 （ver 1.0.0.10）(for 711) 发送失败也返回 JSON 消息。 
**  Modify by: Gavin @2014-08-28 （ver 1.0.0.11）(for 711) 增加call genbusinessmessage
**  Modify by: Gavin @2014-08-30 （ver 1.0.0.12）(for 711) 修正genbusinessmessage中传入的Couponnumber
**  Modify by: Gavin @2014-10-15 （ver 1.0.0.13）增加检查转赠的Coupon是本卡所持有. 如果不是,则返回: -109   -- Coupon不属于此卡 
                                                 发送方和接收方不得相同，否则返回 -110
**  Modify by: Gavin @2015-03-06 （ver 1.0.0.14）增加检查转赠的Coupon状态是否正确（status = 2），如果不正确返回 -20 
**  Modify by: Gavin @2015-09-28 （ver 1.0.0.15）修正bug。（ouput错误）
**  Modify by: Gavin @2015-10-13 （ver 1.0.0.16）增加 call GenBusinessMessage 的参数                                               
**
****************************************************************************/
begin 
  declare @MemberPresentID int, @returnvalue int, @SenderMemberID int    
  declare @CardTypeID int, @TargetCardTypeID int, @ActCouponTypeID int, @TargetCardNumber varchar(512), @MemberID int, @CardGradeID int
  declare @SourceTotalAmt money, @ReceiveTotalAmt money, @SourcePoints int, @ReceiveCardGradeID int, @MaxAmt money
  declare @ReturnMemberID int, @ReturnMemberPassword varchar(512), @A int, @StoreID int, @CardAmountTransfer int, @CardPointTransfer int
  declare @BrandID int, @TargetBrandID int, @CardIssuedID int, @TargetCardIssuedID int
  declare @MinAmountPreTransfer money, @MaxAmountPreTransfer money, @MinPointPreTransfer int, @MaxPointPreTransfer int
  declare @SendCardExpDate datetime, @ReceiveCardExpDate datetime, @MemberHoldActiveCouponCount int
  declare @ReceiveAccountNumber varchar(64)
  declare @Result int
  declare @couponnum nvarchar(64), @CouponType int, @NewMemberFlag int, @sumstatus int, @CustomMsg nvarchar(2000)
  set @ReceiveAccountType = isnull(@ReceiveAccountType, 0)

  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120) + cast(cast(floor(@ActAmount) as int) as varchar)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
    return @CheckResult
  -- end

  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
  select @StoreID = S.StoreID from Store S left join Brand B on S.BrandID = B.StoreBrandID where S.StoreCode = @StoreCode and B.StoreBrandCode = @BrandCode
  select @CardTypeID = CardTypeID, @SenderMemberID = memberID, @SourceTotalAmt = TotalAmount, 
      @SourcePoints = TotalPoints, @CardGradeID = CardGradeID, @SendCardExpDate = CardExpiryDate 
    from Card where CardNumber = @CardNumber
  -- ver 1.0.0.7  
  if @ReceiveAccountType = 0     
    select @MemberID = MemberID, @NewMemberFlag = NewMemberFlag from member where MemberRegisterMobile = @ReceiveMemberRegisterMobile 
  if @ReceiveAccountType = 1     
    select @MemberID = MemberID, @NewMemberFlag = NewMemberFlag from member where MemberEmail = @ReceiveMemberRegisterMobile  
  if @ReceiveAccountType = 2     
    select @MemberID = MemberID, @NewMemberFlag = NewMemberFlag from member where MemberMobilePhone = @ReceiveMemberRegisterMobile              
 -- ver 1.0.0.8
  if @ReceiveAccountType = 2
  begin
    select top 1 @ReceiveAccountNumber = AccountNumber from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2
    select top 1 @MemberID = MemberID from MemberMessageAccount where AccountNumber = @ReceiveAccountNumber and MessageServiceTypeID = 2 order by UpdatedOn desc 
  end
    
  select @TargetCardNumber = CardNumber, @TargetCardTypeID = CardTypeID, @ReceiveTotalAmt = TotalAmount, 
      @ReceiveCardGradeID = CardGradeID, @ReceiveCardExpDate = CardExpiryDate  
    from Card where MemberID = @MemberID and CardTypeID = @CardTypeID  
  select @BrandID = BrandID from CardType where CardTypeID = @CardTypeID
  select @TargetBrandID = @BrandID
  select @CardIssuedID = CardIssuerID from Brand where StoreBrandID = @BrandID
  select @TargetCardIssuedID = @CardIssuedID 
  select @CardAmountTransfer = isnull(CardAmountTransfer, 0), @CardPointTransfer = isnull(CardPointTransfer, 0), 
      @MinAmountPreTransfer = isnull(MinAmountPreTransfer, 0), @MaxAmountPreTransfer = isnull(MaxAmountPreTransfer, 0),
      @MinPointPreTransfer = isnull(MinPointPreTransfer, 0), @MaxPointPreTransfer = isnull(MaxPointPreTransfer, 0) 
    from CardGrade where CardGradeID = @CardGradeID
  select @MaxAmt = isnull(CardGradeMaxAmount,0) from CardGrade where CardTypeID = @TargetCardTypeID and CardGradeID = @ReceiveCardGradeID
  set @ActAmount = -abs(@ActAmount)
  set @ActPoints = -abs(@ActPoints)
  set @ReceiveCardNumber = @TargetCardNumber
  
  -- ver 1.0.0.5
  select @MemberHoldActiveCouponCount = count(CouponNumber) from Coupon where Status = 2 and CardNumber = @CardNumber 
  set @MemberHoldActiveCouponCount = isnull(@MemberHoldActiveCouponCount, 0)
  set @RemainCouponCount = @MemberHoldActiveCouponCount
  
  if isnull(@CardNumber, '') = ''
    return -2
  if isnull(@MemberID, 0) = 0   -- 没有找到获赠的会员ID
    return -35
  if isnull(@TargetCardNumber, '') = ''  -- 会员下没有找到对应的卡。
    return -36

  if @SourceTotalAmt - abs(@ActAmount) < 0
    return -10 
  if @SourcePoints -  abs(@ActPoints) < 0
    return -10 
  --ver 1.0.0.9       
  --if @ReceiveTotalAmt + abs(@ActAmount) > @MaxAmt
  --  set @Result = -14   -- return -14
  
  -- ver 1.0.0.13
   if @CardNumber = @TargetCardNumber  
     return -110    -- 发送方和接收方不得相同  
     
   declare @sqlstr_CheckCoupon nvarchar(1024), @errcount int
   if isNull(@ActCouponNumbers,'')<>'' 
   begin
     set @sqlstr_CheckCoupon='select @n=count(*) from Coupon where CardNumber <> ''' + @CardNumber + ''' and CouponNumber in ('+@ActCouponNumbers+')'
     exec sp_executesql @sqlstr_CheckCoupon,N'@n int output',@errcount output
     if isnull(@errcount, 0) > 0
       return -109   -- Coupon不属于此卡   
     -- ver 1.0.0.14 
     set @sqlstr_CheckCoupon='select @n=count(*), @status=sum(isnull(status, 0)) from Coupon where CouponNumber in ('+@ActCouponNumbers+')'
     exec sp_executesql @sqlstr_CheckCoupon,N'@n bigint output, @status bigint output',@ActCoupons output, @sumstatus output
     if (ISNULL(@ActCoupons, 0) <> 0)
     begin
       if (@sumstatus / @ActCoupons <> 2)
         return -20
     end else
       return -109           
   end        

  if (abs(@ActAmount) > 0) 
  begin
	if @CardAmountTransfer = 0 
	  return -50
	if (@CardAmountTransfer = 1) and (@CardGradeID <> @ReceiveCardGradeID)
	  return -51	
	if (@CardAmountTransfer = 2) and (@CardTypeID <> @TargetCardTypeID)
	  return -52
	if (@CardAmountTransfer = 3) and (@BrandID <> @TargetBrandID)
	  return -53
	if (@CardAmountTransfer = 4) and (@CardIssuedID <> @TargetCardIssuedID)
	  return -54	
	if (abs(@ActAmount) > @MaxAmountPreTransfer and @MaxAmountPreTransfer > 0) or (abs(@ActAmount) < @MinAmountPreTransfer and @MinAmountPreTransfer > 0)
	  return -60	  	     	 
  end  
  if (abs(@ActPoints) > 0)
  begin
	if @CardPointTransfer = 0 
	  return -50
	if (@CardPointTransfer = 1) and (@CardGradeID <> @ReceiveCardGradeID)
	  return -51	
	if (@CardPointTransfer = 2) and (@CardTypeID <> @TargetCardTypeID)
	  return -52
	if (@CardPointTransfer = 3) and (@BrandID <> @TargetBrandID)
	  return -53
	if (@CardPointTransfer = 4) and (@CardIssuedID <> @TargetCardIssuedID)
	  return -54	 	  	     	 
	if (abs(@ActPoints) > @MaxPointPreTransfer and @MaxPointPreTransfer > 0) or (abs(@ActPoints) < @MinPointPreTransfer and @MinPointPreTransfer > 0)
	  return -61
  end    

    set @ReturnAmount	= @SourceTotalAmt - abs(@ActAmount)
    set @ReturnPoints = @SourcePoints - abs(@ActPoints)         
    exec @returnvalue = SaveMemberPresent @UserID, @TxnNo, @Busdate, @Txndate, @CardNumber, @TargetCardNumber, @ReceiveMemberRegisterMobile, @ActAmount, @ActPoints, @ActCouponNumbers, @ActionDate, 2, @Remark, @StoreID, @RegisterCode, @ServerCode, @MemberPresentID output
    if @returnvalue <> 0
      return @returnvalue 

    if @Result = 0
    begin
      declare @sqlstr nvarchar(1024)
      if isNull(@ActCouponNumbers,'')<>''
      begin          
        set @sqlstr='select top 1 @n1=CouponNumber, @n2=CouponTypeID from Coupon where CouponNumber in ('+@ActCouponNumbers+')'
        exec sp_executesql @sqlstr,N'@n1 varchar(64) output, @n2 bigint output',@couponnum output, @CouponType output        
        -- ver 1.0.0.5
        set @MemberHoldActiveCouponCount = @MemberHoldActiveCouponCount - @ActCoupons
        set @RemainCouponCount = @MemberHoldActiveCouponCount 
      end        
    end
    --exec @returnvalue = SaveMemberMessage @UserID, @MemberID, @MessageType, @MessageTitle, @MessageContent, @SenderMemberID, @MessageReceiveMode, @SNSTypeID, @UsefulExpressionsID, @MemberPresentID

  
  set @ReceiveMemberID = @MemberID
  set @SendMemberID = @SenderMemberID 
   
  --ver 1.0.0.10 
  if @ActCouponNumbers <> ''
    set @couponnum = SUBSTRING(@ActCouponNumbers, 2, Len(@ActCouponNumbers) -2)   
  set @couponnum = isnull(@couponnum, '') 
  set @CouponType = isnull(@CouponType, 0)
  set @CustomMsg = substring(@MessageContent,1,2000)
  exec GenBusinessMessage 36, @SenderMemberID, @CardNumber,  @couponnum, @CardTypeID, @CardGradeID, @BrandID,  @CouponType, @ReceiveMemberRegisterMobile, 0, @CustomMsg, @MessageTitle        
  exec GenBusinessMessage 37, @MemberID, @TargetCardNumber,  @couponnum, @TargetCardTypeID, @ReceiveCardGradeID, @TargetBrandID,  @CouponType, @ReceiveMemberRegisterMobile, @SenderMemberID, @CustomMsg, @MessageTitle     
  if @NewMemberFlag = 1
    exec GenBusinessMessage 509, @MemberID, @CardNumber,  @couponnum, @CardTypeID, @CardGradeID, @BrandID,  @CouponType, @ReceiveMemberRegisterMobile, @SenderMemberID, @CustomMsg, @MessageTitle
  -----------
   
  -- 产生消息字符串
  declare @InputString varchar(max), @MessageServiceTypeID int, @AccountNumber nvarchar(512), @MemberType varchar(10)
  declare @MsgExpiryDate datetime, @MsgMemberID varchar(64), @MsgOtherMemberID varchar(64), @MsgCardNumber varchar(64)
  set @InputString = ''
     	
  DECLARE CUR_HolidayPresent CURSOR fast_forward FOR
    select 'OUT' as MemberType, MessageServiceTypeID, AccountNumber from MemberMessageAccount where MemberID = @SenderMemberID and IsPrefer = 1
    union all
    select 'IN' as MemberType, MessageServiceTypeID, AccountNumber from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1  
  OPEN CUR_HolidayPresent
  FETCH FROM CUR_HolidayPresent INTO @MemberType, @MessageServiceTypeID, @AccountNumber
  WHILE @@FETCH_STATUS=0
  BEGIN
	if isnull(@AccountNumber, '') <> '' 
	begin
	  if @InputString <> ''
	    set @InputString = @InputString + '|' 
	  if @MemberType = 'OUT'
	  begin
	    set	@MsgExpiryDate = @SendCardExpDate
	    set @MsgMemberID = cast(@SenderMemberID as varchar)
	    set @MsgOtherMemberID = cast(@MemberID as varchar)
	    set @MsgCardNumber = @CardNumber
	  end else
	  begin
	    set	@MsgExpiryDate = @ReceiveCardExpDate
	    set @MsgMemberID = cast(@MemberID as varchar)
	    set @MsgOtherMemberID = cast(@SenderMemberID as varchar)
	    set @MsgCardNumber = @TargetCardNumber	  
	  end     
      set @InputString = @InputString + 'Type=' + @MemberType + ';MSGACCOUNT=' + @AccountNumber + ';MSGACCOUNTTYPE=' + cast(@MessageServiceTypeID as varchar)
           + ';MEMBERID=' + @MsgMemberID + ';TXNNO=' + @TxnNo + ';EXPDATE=' + convert(varchar(10), @MsgExpiryDate, 120)	
           + ';AMOUNT=' + cast(abs(@ActAmount) as varchar)	+ ';AMOUNTBALANCE=' + cast(abs(@ReturnAmount) as varchar)
     	   + ';POINT=' + cast(abs(@ActPoints) as varchar) + ';POINTBALANCE=' + cast(abs(@ReturnPoints) as varchar)
     	   + ';COUPONS=' + replace(@ActCouponNumbers,'''','')+ ';RECEIVEACCOUNT=' + @MsgOtherMemberID
     	   + ';CARDNUMBER=' + @MsgCardNumber
     	   
	end
    FETCH FROM CUR_HolidayPresent INTO @MemberType, @MessageServiceTypeID, @AccountNumber   
  END
  CLOSE CUR_HolidayPresent 
  DEALLOCATE CUR_HolidayPresent 
  exec GenMessageJSONStr 'Transaction', @InputString, @ReturnMessageStr  output
   
  -- 产生消息字符串 END
  
  return 0
end

GO
