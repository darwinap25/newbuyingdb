USE [NewBuying]
GO
/****** Object:  Table [dbo].[DropdownList]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DropdownList](
	[TypeCode] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[Code] [nvarchar](20) NULL,
	[Text] [nvarchar](100) NOT NULL,
	[OrderID] [int] NULL,
	[Lan] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_DROPDOWNLIST] PRIMARY KEY CLUSTERED 
(
	[TypeCode] ASC,
	[Text] ASC,
	[Lan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DropdownList', @level2type=N'COLUMN',@level2name=N'TypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DropdownList', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'页面的dropdownlist的value值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DropdownList', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'页面的dropdownlist的Text值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DropdownList', @level2type=N'COLUMN',@level2name=N'Text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'en-us  zh-cn  zh-hk' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DropdownList', @level2type=N'COLUMN',@level2name=N'Lan'
GO
