USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenStockTakeVAR]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenStockTakeVAR]
  @StockTakeNumber  VARCHAR(64)
AS
/****************************************************************************
**  Name : GenStockTakeVAR
**  Version: 1.0.0.1
**  Description : 盘点表和注册表的差异. 以第二次盘点为准. 如果没有第二次,则以第一次盘点.
**  Parameter :
select * from STK_STAKEVAR
select * from STK_STAKE02
**  Created by: Gavin @2015-06-23
**  Modify by: Gavin @2015-11-10 (ver 1.0.0.1) 产生差异报表时, 数量为0 的不需要产生
**
****************************************************************************/
BEGIN
  SELECT * INTO #TEMP FROM STK_STAKE02 WHERE StockTakeNumber = @StockTakeNumber
  IF @@ROWCOUNT = 0
  BEGIN
    INSERT INTO #TEMP SELECT * FROM STK_STAKE01 WHERE StockTakeNumber = @StockTakeNumber
    IF @@ROWCOUNT = 0
      RETURN -1 
  END   
  
  DELETE FROM STK_STAKEVAR WHERE StockTakeNumber = @StockTakeNumber

  INSERT INTO STK_STAKEVAR(StoreID,StockTakeNumber,ProdCode,STOCKTYPE,VARQTY,SerialNo)
  SELECT * FROM
  ( 
  SELECT A.StoreID,A.StockTakeNumber,A.ProdCode,A.STOCKTYPE,(ISNULL(A.QTY,0) - ISNULL(B.QTY,0)) as VARQTY, A.SerialNo 
    FROM #TEMP A 
      LEFT JOIN STK_STAKEBOOK B ON A.StoreID = B.StoreID AND A.ProdCode = B.ProdCOde AND A.StockType = B.StockType 
                          AND A.SerialNo = B.SerialNo                     
  ) C WHERE C.VARQTY <> 0
  
  UPDATE STK_STAKE_H SET STATUS = 3, UpdatedOn=Getdate() WHERE StockTakeNumber = @StockTakeNumber
  DROP TABLE #TEMP
  RETURN 0   
END

GO
