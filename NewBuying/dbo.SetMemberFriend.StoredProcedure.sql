USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetMemberFriend]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SetMemberFriend]
  @UserID				varchar(10),    -- 操作员ID.
  @MemberID				int,			-- 会员ID
  @MemberFriendID		int,			-- MemberFriend表主键，如果是有效值，则表示更新此记录
  @FriendMemberID		int,			-- 好友的会员ID
  @FriendFamilyName		nvarchar(512),	-- 会员姓
  @FriendGivenName		nvarchar(512),	-- 会员名
  @FriendSex			int,				-- 会员性别（null或0：保密。1：男性。 2：女性）
  @MobileNumber			nvarchar(512),	-- 好友手机号  
  @CountryCode			varchar(512),	-- 好友手机号国家码  
  @EMail				nvarchar(512),	-- 好友email
  @Status				int  --0：待审核。 1：好友状态。 2：审核不通过。 3：黑名单。 4：删除。  
AS
/****************************************************************************
**  Name : SetMemberFriend
**  Version: 1.0.0.0
**  Description : 设置会员好友
**
**  Parameter :
  declare @A int
  exec @A = SetMemberFriend '1', 1, 12, 0, 'xin23334234', 'min', 1, '41224311', '087', '352@sd.com', 4
  print @A
  select * from MemberFriend
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**MemberClause
****************************************************************************/
begin 
  if isnull(@MemberFriendID, 0) = 0
  begin
    insert into MemberFriend(MemberID, FriendMemberID, FriendFamilyName, FriendGivenName, FriendSex, MobileNumber, CountryCode, EMail, Status, CreatedOn, UpdatedOn)
    values(@MemberID, @FriendMemberID, @FriendFamilyName, @FriendGivenName, @FriendSex, @MobileNumber, @CountryCode, @EMail, @Status, Getdate(), GetDate())
  end
  else 
  begin
    if isnull(@Status, 0) = 4
      delete from MemberFriend where MemberFriendID = @MemberFriendID
    else  
      update MemberFriend set MemberID=@MemberID, FriendMemberID=@FriendMemberID, FriendFamilyName = @FriendFamilyName, FriendGivenName = @FriendGivenName, FriendSex = @FriendSex, MobileNumber = @MobileNumber, CountryCode = @CountryCode, EMail = @EMail, Status=@Status, UpdatedOn=Getdate()
       where MemberFriendID = @MemberFriendID
  end 
  
  if @@ROWCOUNT <> 0
    return 0 
  else return -1 
end

GO
