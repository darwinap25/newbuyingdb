USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessageAttachment]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessageAttachment](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[MessageID] [int] NOT NULL,
	[AttachFileName] [varchar](64) NOT NULL,
	[AttachFilePath] [nvarchar](512) NULL,
	[AttachFileBinaryContent] [varbinary](max) NULL,
 CONSTRAINT [PK_MESSAGEATTACHMENT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageAttachment', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息接收方的会员主键。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageAttachment', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息主体表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageAttachment', @level2type=N'COLUMN',@level2name=N'MessageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageAttachment', @level2type=N'COLUMN',@level2name=N'AttachFileName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附件文件路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageAttachment', @level2type=N'COLUMN',@level2name=N'AttachFilePath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附件文件内容（二进制）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageAttachment', @level2type=N'COLUMN',@level2name=N'AttachFileBinaryContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息附件。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageAttachment'
GO
