USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponReturn_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponReturn_H](
	[CouponReturnNumber] [varchar](64) NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[BrandID] [int] NULL,
	[FromStoreID] [int] NULL,
	[StoreID] [int] NULL,
	[CustomerType] [int] NULL,
	[CustomerID] [int] NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[FromAddress] [nvarchar](512) NULL,
	[StoreContactName] [nvarchar](512) NULL,
	[StoreContactPhone] [nvarchar](512) NULL,
	[StoreContactEmail] [nvarchar](512) NULL,
	[StoreMobile] [nvarchar](512) NULL,
	[FromContactName] [nvarchar](512) NULL,
	[FromContactNumber] [nvarchar](512) NULL,
	[FromEmail] [nvarchar](512) NULL,
	[FromMobile] [nvarchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_COUPONRETURN_H] PRIMARY KEY CLUSTERED 
(
	[CouponReturnNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CouponReturn_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CouponReturn_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CouponReturn_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CouponReturn_H] ON [dbo].[Ord_CouponReturn_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CouponReturn_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2014-06-23
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @CouponReturnNumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @CouponReturnNumber = CouponReturnNumber FROM INSERTED  
  exec GenUserMessage @CreatedBy, 13, 0, @CouponReturnNumber  
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponReturn_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponReturn_H] ON [dbo].[Ord_CouponReturn_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponReturn_H
* Version: 1.0.0.0
* Description : 供应商的Coupon退货单表的批核触发器

  select * from Ord_CouponReturn_H 
  select * from Ord_CouponReturn_D
  update Ord_CouponReturn_H set ApproveStatus = 'A' where CouponReturnNumber = 'RTH000000000049'
  select * from Ord_CouponReceive_H
  select * from Ord_CouponReceive_D
  CREC00000000031
  select * from coupon_onhand where coupontypeid = 123
** Create By Gavin @2014-06-04
*/
/*==============================================================*/
BEGIN  
  declare @CouponReturnNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  
  DECLARE CUR_CouponReturn_H CURSOR fast_forward FOR
    SELECT CouponReturnNumber, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_CouponReturn_H
  FETCH FROM CUR_CouponReturn_H INTO @CouponReturnNumber, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CouponReturnNumber = @CouponReturnNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output    
      exec CouponOrderApprove 1, 2, @CouponReturnNumber
        -- 库存数量变动
      exec ChangeCouponStockStatus 6, @CouponReturnNumber, 1   
  
      update Ord_CouponReturn_H set ApprovalCode = @ApprovalCode where CouponReturnNumber = @CouponReturnNumber
    end      
    FETCH FROM CUR_CouponReturn_H INTO @CouponReturnNumber, @ApproveStatus, @CreatedBy  
  END
  CLOSE CUR_CouponReturn_H 
  DEALLOCATE CUR_CouponReturn_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货确认单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'CouponReturnNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。指coupon拣货单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源店铺，一般可能是总部' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送达店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户类型。1：客戶訂貨。 2：店鋪訂貨' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'CustomerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'customer表 主键， 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'FromContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'退货确认单。主表 （店铺向总部退货）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReturn_H'
GO
