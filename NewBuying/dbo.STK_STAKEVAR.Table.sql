USE [NewBuying]
GO
/****** Object:  Table [dbo].[STK_STAKEVAR]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STK_STAKEVAR](
	[StoreID] [int] NOT NULL,
	[StockTakeNumber] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[STOCKTYPE] [varchar](64) NOT NULL,
	[VARQTY] [dbo].[Buy_Qty] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[SerialNo] [varchar](64) NOT NULL,
 CONSTRAINT [PK_STK_STAKEVAR] PRIMARY KEY CLUSTERED 
(
	[StoreID] ASC,
	[StockTakeNumber] ASC,
	[ProdCode] ASC,
	[STOCKTYPE] ASC,
	[SerialNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_Store表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKEVAR', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'STK_STAKE_H 表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKEVAR', @level2type=N'COLUMN',@level2name=N'StockTakeNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKEVAR', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'差异数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKEVAR', @level2type=N'COLUMN',@level2name=N'VARQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存盘点差异表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STK_STAKEVAR'
GO
