USE [NewBuying]
GO
/****** Object:  Table [dbo].[Rel_AttachmentMessageInfo]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rel_AttachmentMessageInfo](
	[MessageInfoID] [bigint] NOT NULL,
	[AttachmentID] [bigint] IDENTITY(1,1) NOT NULL,
	[PathFile] [nvarchar](512) NULL,
	[BinaryContent] [varbinary](max) NULL,
 CONSTRAINT [PK_REL_ATTACHMENTMESSAGEINFO] PRIMARY KEY CLUSTERED 
(
	[MessageInfoID] ASC,
	[AttachmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息主体表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rel_AttachmentMessageInfo', @level2type=N'COLUMN',@level2name=N'MessageInfoID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rel_AttachmentMessageInfo', @level2type=N'COLUMN',@level2name=N'AttachmentID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附件文件路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rel_AttachmentMessageInfo', @level2type=N'COLUMN',@level2name=N'PathFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附件文件内容（二进制）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rel_AttachmentMessageInfo', @level2type=N'COLUMN',@level2name=N'BinaryContent'
GO
