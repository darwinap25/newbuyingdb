USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_SETTLEMENT]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_SETTLEMENT](
	[SettlementCode] [varchar](64) NOT NULL,
	[SettlementDate] [datetime] NOT NULL,
	[SettlementTotal] [dbo].[Buy_Amt] NOT NULL,
	[SettlementNetTotal] [dbo].[Buy_Amt] NOT NULL,
	[NoOfTxn] [int] NOT NULL,
	[BankInDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_SETTLEMENT] PRIMARY KEY CLUSTERED 
(
	[SettlementCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_SETTLEMENT] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[BUY_SETTLEMENT] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_SETTLEMENT] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单号，主键。（一般：Bus_Date+StoreID+Bank_Code+Card_Type）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'SettlementCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结算日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'SettlementDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此单总金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'SettlementTotal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此单总净额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'SettlementNetTotal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此单总交易数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'NoOfTxn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'汇款日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'BankInDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态
0: Outstanding 
1: Partial Payment  
2: Full Payment ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易的结算信息表，主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SETTLEMENT'
GO
