USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CopyStockTake1To2]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyStockTake1To2]
  @StockTakeNumber  VARCHAR(64)
AS
/****************************************************************************
**  Name : CopyStockTake1To2
**  Version: 1.0.0.0
**  Description : 复制STK_STAKE01内容到STK_STAKE02。 （复制前先清空STK_STAKE02）
**  Parameter :
select * from STK_STAKE01
select * from STK_STAKE02
**  Created by: Gavin @2015-06-23
**
****************************************************************************/
BEGIN  
  IF NOT EXISTS(SELECT * FROM STK_STAKE02 WHERE StockTakeNumber = @StockTakeNumber)
  BEGIN
    INSERT INTO STK_STAKE02 
      (StoreID,StockTakeNumber,ProdCode,STOCKTYPE,QTY,[STATUS],CreatedON,SerialNo,SEQ)
    SELECT StoreID,StockTakeNumber,ProdCode,STOCKTYPE,QTY,[STATUS],CreatedON,SerialNo,SEQ 
    FROM STK_STAKE01 WHERE StockTakeNumber = @StockTakeNumber
    
    UPDATE STK_STAKE_H SET STATUS = 2, UpdatedOn=Getdate() WHERE StockTakeNumber = @StockTakeNumber
  END
  RETURN 0   
END

GO
