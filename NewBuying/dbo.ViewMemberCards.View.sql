USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewMemberCards]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- 不使用这个视图. 因为不能加上索引.
CREATE VIEW [dbo].[ViewMemberCards] 
--  WITH SCHEMABINDING
AS
  select (RTrim(C.CardTypeID) + C.CardNumber) as SeqNo, C.CardNumber, C.CardTypeID, Y.CardTypeCode, CardIssueDate, CardExpiryDate, 
    C.MemberID, C.CardGradeID, C.Status as CardStatus, C.TotalPoints, C.TotalAmount, 
	M.MemberEmail, G.CardGradeMaxAmount, C.ResetPassword,
	Y.CardTypeName1, Y.CardTypeName2, Y.CardTypeName3, 
	M.MemberEngFamilyName, M.MemberEngGivenName, M.MemberChiFamilyName, M.MemberChiGivenName,
	M.MemberSex, M.MemberDateOfBirth, M.MemberDayofBirth, M.MemberMonthofBirth, M.MemberYearofBirth, M.HomeTelNum,
	V.vendorcardNumber as [UID], V.LaserID, Y.BrandID, B.StoreBrandName1 as BrandName, 
--	'' as [UID], '' as LaserID, Y.BrandID, B.BrandName1 as BrandName, 
	C.BatchCardID, C.ParentCardNumber, M.MemberIdentityRef, G.CardGradeName1, G.CardGradeName2, G.CardGradeName3, G.CardGradeLayoutFile, M.MemberRegisterMobile,	
	M.MemberIdentityType, M.MemberMobilePhone, G.CardGradePicFile, G.CardGradeNotes, G.CardPointToAmountRate, G.CardAmountToPointRate, M.CountryCode,
	G.IsAllowStoreValue, G.CardPointTransfer, G.CardAmountTransfer, G.MinAmountPreAdd, G.MaxAmountPreAdd, G.MinAmountPreTransfer, G.MaxAmountPreTransfer, 
	G.MinPointPreTransfer, G.MaxPointPreTransfer, G.DayMaxAmountTransfer, G.DayMaxPointTransfer, G.MinBalanceAmount, G.MinBalancePoint, G.MinConsumeAmount,
	B.StoreBrandPicSFile, B.StoreBrandPicMFile, B.StoreBrandPicGFile, G.CardGradeMaxPoint, G.MinPointPreAdd, G.MaxPointPreAdd, G.GracePeriodValue, G.GracePeriodUnit,
	C.CardAmountExpiryDate, C.CardPointExpiryDate, G.LoginFailureCount, G.QRCodePeriodValue, G.AllowOfflineQRCode, G.QRCodePrefix, G.TrainingMode
    from dbo.[card] C left join dbo.Member M on C.MemberID = M.MemberID      
      left join dbo.CardGrade G on C.CardTypeID = G.CardTypeID and C.CardGradeID = G.CardGradeID
      left join dbo.CardType Y on C.CardTypeID = Y.CardTypeID 
      left join dbo.Brand B on Y.BrandID = B.StoreBrandID
      inner join dbo.VenCardIDMap V on C.CardNumber = V.CardNumber and ValidateFlag = 0

GO
