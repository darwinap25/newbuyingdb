USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenSalesTxnByAfterSales]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*================================================================================*/
CREATE PROCEDURE [dbo].[GenSalesTxnByAfterSales]
  @AfterSalesTxnNo				varchar(64),    -- 售后单号
  @RefSalesTxnNo				varchar(64),    -- 售后单中相关的交易单号
  @CreatedBy                    int
AS
/****************************************************************************
**  Name : GenSalesTxnByAfterSales
**  Version: 1.0.1.3
**  Description : (bauhaus需求) 根据aftersales单，产生sales数据。（退货，换货） 
**  Parameter :
**
declare @a int, @TxnNo varchar(64)
exec @a = GenSalesTxnByAfterSales @TxnNo output
print @a
print @TxnNo
** Create By Gavin @2016-10-13
** Modify By Gavin @2016-10-21 (ver 1.0.0.1) 允许前台分多次退货。退货时检查退货数量 和所有的 退货单，只有本单中所有货品被退货，才可以关闭。
                                             修正aftersales_D和sales_D的join关联字段，应使用refseqno
** Modify By Gavin @2016-11-24 (ver 1.0.0.2) 新增的sales_H单中RefTxnNo 改成存放 aftersales的txnno
** Modify By Gavin @2016-12-29 (ver 1.0.0.3) 退货的 RefTxnNo 也填写 aftersales的txnno
** Modify By Gavin @2017-01-25 (ver 1.0.0.4) 退货的sales_H 的 salestype 应该是10. exchange 的应该是11
** Modify By Gavin @2017-03-03 (ver 1.0.0.5) 退货时,需要从aftersales_D中读取Qty 。（qty数量可能不是这条记录的全部数量。）
** Modify By Gavin @2017-03-15 (ver 1.0.0.6) (bauhaus, LINKDB:Buying_621_Test) TxnType增加作废。 增加字段NewTransNum。
** Modify By Gavin @2017-03-23 (ver 1.0.0.7) update原单sales_D时增加填写Sales_D.RetiredQty
** Modify By Gavin @2017-04-10 (ver 1.0.0.8) 原单已经完成，则正常退货时， 要在aftersales单中有的货品才写入return单的salesD。
** Modify By Gavin @2017-07-12 (ver 1.0.1.0) 按照新的业务流程：不管原单是否完成，都不void原单。 
** Modify By Gavin @2017-07-24 (ver 1.0.1.1) 换货需要补交金额时，sales_T不插入记录。 不用补交金额时需要插入，直接完成交易。
** Modify By Gavin @2017-08-15 (ver 1.0.1.2) 合并版本 
** Modify By Gavin @2017-08-24 (ver 1.0.1.3) sales_T的tendertype 是不可为null的，必须填入. 
                                             换货时，判断是否要补价，不需要的话，直接status=4. 否则status=3
											 把storecode存入sales_D
****************************************************************************/
begin
  declare @InvalidFlag int, @TxnType int, @TxnNo varchar(64), @Status INT, @NewTxnNo varchar(64),
          @NewOrderStatus INT, @AfterSalesTotalAmount MONEY, @TransType INT, @StoreCode VARCHAR(64), @RegisterCode VARCHAR(64)

  set @AfterSalesTxnNo = isnull(@AfterSalesTxnNo, '')
  set @RefSalesTxnNo = isnull(@RefSalesTxnNo, '')
  select @TxnType = TxnType, @AfterSalesTotalAmount = TotalAmount from aftersales_h where TxnNo = @AfterSalesTxnNo
  select @InvalidFlag = isnull(InvalidateFlag,0), @Status = Status, @TransType = TransType,
       @StoreCode = StoreCode, @RegisterCode = RegisterCode
    from sales_h where TransNUm = @RefSalesTxnNo
  set @InvalidFlag = isnull(@InvalidFlag, -1)

  if @InvalidFlag in (0, 4)
  begin

	-- 原单已经完成，则正常退货，换货： 原单不作废，产生refund单和exchange单。
		if @TxnType = 2  -- 退货
		begin
		  exec GetRefNoString 'KTXNNO', @TxnNo output   
		  insert into sales_D (TransType,StoreCode, RegisterCode, TransNum,SeqNo,ProdCode,ProdDesc,Serialno,Collected,UnitPrice,NetPrice,Qty,NetAmount,Additional1,POPrice,POReasonCode,
			DiscountAmount,DiscountPrice,ReservedDate,PickupDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,TotalQty)
		  select @TransType,@StoreCode, @RegisterCode, @TxnNo,ROW_NUMBER() OVER(order by A.ProdCode) as iid,A.ProdCode,A.ProdDesc,A.Serialno, A.Collected,
			 --case Collected when 0 then 80 when 1 then 81 when 82 then 2 when 4 then 84 else -1 end,
			 A.UnitPrice,A.NetPrice,-1 * abs(B.Qty), -1 * A.NetPrice * abs(B.Qty), A.Additional1,A.POPrice,A.POReasonCode,
			 A.DiscountAmount,A.DiscountPrice,A.ReservedDate,
			 A.PickupDate,Getdate(),@CreatedBy,Getdate(),@CreatedBy,-1 * abs(B.Qty)
		  from (select * from sales_D where TransNUm = @RefSalesTxnNo) A 
			left join (select * from aftersales_D where TxnNo = @AfterSalesTxnNo) B on A.SeqNo = B.RefSeqNo and A.ProdCode = B.ProdCode
		 -- where A.SeqNo in (select * from aftersales_D where TransNUm = @AfterSalesTxnNo)
		  where B.TxnNo is not null   -- ver 1.0.0.8

		  insert into sales_T (TransType,StoreCode, RegisterCode,TenderType, TransNUm,SeqNo,TenderID,TenderCode,TenderDesc,TenderAmount,LocalAmount,ExchangeRate,Additional,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		  select @TransType,@StoreCode, @RegisterCode,B.TenderType, @TxnNo,ROW_NUMBER() OVER(order by A.TenderID) as iid,A.TenderID,A.TenderCode,A.TenderDesc,-1*TenderAmount,-1*LocalAmount,A.ExchangeRate,A.Additional,getdate(),@CreatedBy,getdate(),@CreatedBy 
			from aftersales_T A
			 left join TENDER B ON A.TenderCode = B.TenderCode 
		   where txnno = @AfterSalesTxnNo
			
	      insert into sales_h (TransNum,RefTransNum,StoreCode,RegisterCode,ServerCode,BrandCode,BusDate,TxnDate,TransType,CashierID,SalesManID,Channel,TotalAmount,
		        Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliveryBy,DeliveryFullAddress,RequestDeliveryDate,DeliveryDate,
			    DeliveryLongitude,DeliveryLatitude,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
			    Additional,SalesTag,PaySettleDate,PickupType,PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
			    BillContactPhone,InvalidateFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		  select top 1 @TxnNo,@AfterSalesTxnNo,H.StoreCode,H.RegisterCode,ServerCode,BrandCode,AH.BusDate,AH.TxnDate,10,H.CashierID,H.SalesManID,Channel,-1 * abs(AH.TotalAmount),
				4,H.MemberID,MemberName,MemberMobilePhone,H.CardNumber,MemberAddress,H.DeliveryBy,H.DeliveryFullAddress,H.RequestDeliveryDate,H.DeliveryDate,
				DeliveryLongitude,DeliveryLatitude,H.DeliveryNumber,H.LogisticsProviderID,H.Contact,H.ContactPhone,SalesReceipt,SalesReceiptBIN,
				Additional,SalesTag,H.PaySettleDate,H.PickupType,H.PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
				BillContactPhone,H.InvalidateFlag,getdate(),@CreatedBy,getdate(),@CreatedBy
		   from sales_h H left join aftersales_H AH on H.TransNUm = AH.RefTransNum
		   where H.TransNUm = @RefSalesTxnNo

		   update Sales_D set Collected = case Sales_D.Collected when 0 then 80 when 1 then 81 when 2 then 82 when 4 then 84 else Sales_D.Collected end,
			 UpdatedOn = GETDATE(), RetiredQty = abs(AD.Qty)
		   from (select * from aftersales_D where TxnNo = @AfterSalesTxnNo) AD 
		   where Sales_D.SeqNo= AD.RefSeqNo and Sales_D.ProdCode = AD.ProdCode and AD.TxnNo is not null AND AD.Qty <= Sales_D.Qty
		   and Sales_D.KeyID in (select KeyID from Sales_D where TxnNo = @RefSalesTxnNo)

           if exists(select * from Sales_D where TransNUm = @RefSalesTxnNo and isnull(Qty,0) > isnull(RetiredQty,0) and Collected <> 9) 
		   begin
			 Update Sales_H set InvalidateFlag = 4, UpdatedOn = GETDATE()
			 where TransNum = @RefSalesTxnNo
		   end else
		   begin
			 Update Sales_H set InvalidateFlag = 2, UpdatedOn = GETDATE()
			 where TransNUm = @RefSalesTxnNo
		   end

		end else if @TxnType = 3  -- 换货
		begin
		  SET @NewOrderStatus = 3
		  exec GetRefNoString 'KTXNNO', @TxnNo output   
		  insert into sales_D (TransType,StoreCode, RegisterCode,TransNum,SeqNo,ProdCode,ProdDesc,Serialno,Collected,UnitPrice,NetPrice,Qty,NetAmount,Additional1,POPrice,POReasonCode,
			DiscountAmount,DiscountPrice,ReservedDate,PickupDate,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,TotalQty)
		  select @TransType,@StoreCode, @RegisterCode,@TxnNo,ROW_NUMBER() OVER(order by ProdCode) as iid,ProdCode,ProdDesc,'', Collected,
			 RetailPrice,NetPrice,Qty,NetAmount,'',POPrice,0,
			 0,0,null,null,Getdate(),@CreatedBy,Getdate(),@CreatedBy,Qty
		  from aftersales_D A 
		  where txnno = @AfterSalesTxnNo

		  if ISNULL(@AfterSalesTotalAmount, 0) <= 0
		  begin
		     set @NewOrderStatus = 4
             insert into sales_T (TenderType,TransNUm,SeqNo,TenderID,TenderCode,TenderDesc,TenderAmount,LocalAmount,ExchangeRate,Additional,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		     select B.TenderType, @TxnNo,ROW_NUMBER() OVER(order by A.TenderID) as iid,A.TenderID,A.TenderCode,A.TenderDesc,TenderAmount,LocalAmount,A.ExchangeRate,A.Additional,getdate(),@CreatedBy,getdate(),@CreatedBy 
			    from aftersales_T A
				  left join TENDER B ON A.TenderCode = B.TenderCode 
			  where txnno = @AfterSalesTxnNo
          end
		  -- RefTxnNo 改成存放 aftersales的@AfterSalesTxnNo 而不是 @RefSalesTxnNo 
	      insert into sales_h (TransNum,RefTransNum,StoreCode,RegisterCode,ServerCode,BrandCode,BusDate,TxnDate,TransType,CashierID,SalesManID,Channel,TotalAmount,
			    Status,MemberID,MemberName,MemberMobilePhone,CardNumber,MemberAddress,DeliveryBy,DeliveryFullAddress,RequestDeliveryDate,DeliveryDate,
			    DeliveryLongitude,DeliveryLatitude,DeliveryNumber,LogisticsProviderID,Contact,ContactPhone,SalesReceipt,SalesReceiptBIN,
			    Additional,SalesTag,PaySettleDate,PickupType,PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
			    BillContactPhone,InvalidateFlag,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
		  select top 1 @TxnNo,@AfterSalesTxnNo,H.StoreCode,H.RegisterCode,ServerCode,BrandCode,AH.BusDate,AH.TxnDate,11,H.CashierID,H.SalesManID,Channel,AH.TotalAmount,
				@NewOrderStatus, H.MemberID,MemberName,MemberMobilePhone,H.CardNumber,MemberAddress,H.DeliveryBy,H.DeliveryFullAddress,H.RequestDeliveryDate,H.DeliveryDate,
				DeliveryLongitude,DeliveryLatitude,H.DeliveryNumber,H.LogisticsProviderID,H.Contact,H.ContactPhone,SalesReceipt,SalesReceiptBIN,
				Additional,SalesTag,H.PaySettleDate,H.PickupType,H.PickupStoreCode,CODFlag,PromotionFlag,BillAddress,BillContact,
				BillContactPhone,H.InvalidateFlag,getdate(),@CreatedBy,getdate(),@CreatedBy
		   from sales_h H left join aftersales_H AH on H.TransNum = AH.RefTransNum
		   where H.TransNum = @RefSalesTxnNo

		   update Sales_D set Collected = case Sales_D.Collected when 0 then 90 when 1 then 91 when 2 then 92 when 4 then 94 else Sales_D.Collected end,
			 UpdatedOn = GETDATE(), RetiredQty = abs(AD.Qty)
		   from (select * from aftersales_D where TxnNo = @AfterSalesTxnNo) AD 
		   where Sales_D.SeqNo= AD.RefSeqNo and Sales_D.ProdCode = AD.ProdCode and AD.TxnNo is not null and AD.Qty < 0 
		   and Sales_D.KeyID in (select KeyID from Sales_D where TransNUm = @RefSalesTxnNo)

		   if exists(select * from Sales_D where TransNUm = @RefSalesTxnNo and isnull(Qty,0) > isnull(RetiredQty,0) and Collected <> 9) 
		   begin
			 Update Sales_H set InvalidateFlag = 4, UpdatedOn = GETDATE()
			 where TransNUm = @RefSalesTxnNo
		   end else
		   begin
			 Update Sales_H set InvalidateFlag = 3, UpdatedOn = GETDATE()
			 where TransNUm = @RefSalesTxnNo
		   end
		end
	    else if @TxnType = 4  -- 作废本单。
	    begin
          exec VoidSalesByAfterSales_SVA @AfterSalesTxnNo, @RefSalesTxnNo, @CreatedBy 
        end

		EXEC ReturnToStockByAfter @AfterSalesTxnNo
        IF @txntype = 3
	    BEGIN
	      EXEC ReserveSalesStock @TxnNo
          EXEC GenSalesPickupOrder 1, @TxnNo, 1
		END
  end    
  return 0
end

GO
