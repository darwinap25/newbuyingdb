USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCouponPushImport]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DoCouponPushImport]
  @MemberRegisterMobile               varchar(64),
  @MemberMobilePhone                  varchar(64),
  @MemberID                           int,
  @CardNumber                         varchar(64)
AS
/****************************************************************************
**  Name : DoCouponPushImport 
**  Version: 1.0.0.0
**  Description : 处理CouponPushImport中的记录。 此过程只在NewMemberAccount中调用。
**  Parameter :
**
  declare @MemberID varchar(36), @A int
  exec @A = MemberCardAdjustment 
  print @A
  print @MemberID
  select*  from CouponPushImport
  select * from member
**  Created by: Gavin @2017-01-10
**
****************************************************************************/
begin
  declare @RegisterMobile varchar(64), @MobilePhone varchar(64), @CouponTypeCode varchar(64), @CouponTypeID int
  declare @CouponExpiryDate datetime, @NewCouponExpiryDate datetime, @CouponNumber varchar(64)
  declare @ApprovalCode varchar(10), @CouponAmount money, @BUSDATE datetime, @TXNDATE datetime, @Qty int
  declare @type int, @i int

  set @MemberRegisterMobile = isnull(@MemberRegisterMobile, '')
  set @MemberMobilePhone = ISNULL(@MemberMobilePhone, '')
  set @MemberID = ISNULL(@MemberID, 0)
  set @CardNumber = ISNULL(@CardNumber, '')
  set @BUSDATE = convert(varchar(10), getdate(), 120)
  set @TXNDATE = getdate()

  if @MemberID = 0 or @CardNumber = '' 
    return -1

  set @type = 1
  select @CouponTypeCode = CouponTypeCode, @Qty = Qty from CouponPushImport where NumberType = 1 and MemberOnlyNumber = @MemberRegisterMobile and status = 0
  if isnull(@CouponTypeCode, '') = ''
  begin
    select @CouponTypeCode = CouponTypeCode, @Qty = Qty  from CouponPushImport where NumberType = 2 and MemberOnlyNumber = @MemberMobilePhone and status = 0
	set @type = 2
  end

  set @CouponTypeCode = isnull(@CouponTypeCode, '')
  if @CouponTypeCode = '' 
    return -2
  
  select @CouponTypeID = CouponTypeID from CouponType where CouponTypeCode = @CouponTypeCode
  set @i = 0
  while @i < @Qty
  begin
    exec GetCouponNumber @CouponTypeID, 1, @CouponNumber output 
    select @CouponAmount = CouponAmount, @CouponExpiryDate = CouponExpiryDate from coupon where couponnumber = @CouponNumber
    exec CalcCouponNewExpiryDate @CouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output 
    exec GenApprovalCode @ApprovalCode output
   
	INSERT INTO COUPON_MOVEMENT
		(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
		BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
		OrgExpirydate, OrgStatus, NewStatus)    
	values(32,@CardNumber, @CouponNumber, @CouponTypeID, '', 0, '', @CouponAmount, 0, @CouponAmount,
		@BUSDATE, @TXNDATE, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, NULL, '', '',
		@CouponExpiryDate, 1, 2)

    set @i = @i + 1
  end

  if @type = 1
    update CouponPushImport set status = 1 where CouponTypeCode = @CouponTypeCode and NumberType = 1 and MemberOnlyNumber = @MemberRegisterMobile and status = 0
  if @type = 2
    update CouponPushImport set status = 1 where CouponTypeCode = @CouponTypeCode and NumberType = 2 and MemberOnlyNumber = @MemberMobilePhone and status = 0

  return 0
end

GO
