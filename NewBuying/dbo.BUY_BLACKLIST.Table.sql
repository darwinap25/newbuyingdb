USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_BLACKLIST]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_BLACKLIST](
	[BlackID] [int] IDENTITY(1,1) NOT NULL,
	[AccountCode] [varchar](64) NULL,
	[CustomerName] [nvarchar](512) NULL,
	[CustomerCode] [nvarchar](512) NULL,
	[ExtgServiceNo] [varchar](64) NULL,
	[DeliveryAddress] [nvarchar](512) NULL,
	[CustomerContact] [nvarchar](512) NULL,
	[CustomerContact1] [nvarchar](512) NULL,
	[CreditCardNo] [nvarchar](512) NULL,
	[NickName] [varchar](64) NULL,
	[MobileNo] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_BLACKLIST] PRIMARY KEY CLUSTERED 
(
	[BlackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_BLACKLIST] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_BLACKLIST] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'AccountCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'CustomerName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'CustomerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extg Service No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'ExtgServiceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'DeliveryAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'CustomerContact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'CustomerContact1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'信用卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'CreditCardNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'昵称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'NickName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST', @level2type=N'COLUMN',@level2name=N'MobileNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'黑名单表
@2016-06-14 增加字段： CreditCardNo，NickName' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BLACKLIST'
GO
