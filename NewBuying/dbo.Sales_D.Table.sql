USE [NewBuying]
GO
/****** Object:  Table [dbo].[Sales_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[SeqNo] [varchar](64) NOT NULL,
	[TransNum] [varchar](64) NOT NULL,
	[TransType] [int] NULL DEFAULT ((0)),
	[StoreCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[BusDate] [datetime] NULL,
	[TxnDate] [datetime] NULL DEFAULT (getdate()),
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[ProdDesc] [nvarchar](512) NULL,
	[DepartCode] [varchar](64) NULL,
	[Qty] [dbo].[Buy_Qty] NULL,
	[RetiredQty] [int] NULL DEFAULT ((0)),
	[OrgPrice] [dbo].[Buy_Amt] NULL,
	[UnitPrice] [dbo].[Buy_Amt] NULL,
	[NetPrice] [dbo].[Buy_Amt] NULL,
	[OrgAmount] [dbo].[Buy_Amt] NULL,
	[UnitAmount] [dbo].[Buy_Amt] NULL,
	[NetAmount] [dbo].[Buy_Amt] NULL,
	[TotalQty] [dbo].[Buy_Qty] NULL,
	[DiscountPrice] [dbo].[Buy_Amt] NULL,
	[DiscountAmount] [dbo].[Buy_Amt] NULL,
	[POPrice] [dbo].[Buy_Amt] NULL,
	[ExtraPrice] [dbo].[Buy_Amt] NULL,
	[POReasonCode] [varchar](512) NULL,
	[Additional1] [varchar](512) NULL,
	[Additional2] [varchar](512) NULL,
	[Additional3] [varchar](512) NULL,
	[RPriceTypeCode] [varchar](64) NULL,
	[IsBOM] [int] NULL DEFAULT ((0)),
	[IsCoupon] [int] NULL DEFAULT ((0)),
	[IsBuyBack] [int] NULL DEFAULT ((0)),
	[IsService] [int] NULL DEFAULT ((0)),
	[SerialNoStockFlag] [int] NULL DEFAULT ((0)),
	[SerialNoType] [int] NULL,
	[SerialNo] [varchar](64) NULL,
	[IMEI] [varchar](64) NULL,
	[StockTypeCode] [varchar](64) NULL,
	[Collected] [int] NULL,
	[ReservedDate] [datetime] NULL,
	[PickupLocation] [varchar](64) NULL,
	[PickupStaff] [varchar](64) NULL,
	[PickupDate] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryBy] [int] NULL,
	[ActPrice] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[ActAmount] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[OrgTransNum] [varchar](64) NULL,
	[OrgSeqNo] [varchar](64) NULL,
	[Remark] [nvarchar](512) NULL,
	[RefGUID] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SALES_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单中货品序号。使用字符串是为了 预留 BOM功能时使用。 如果不使用BOM功能， 可以直接用 自然数。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'SeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'TransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型.
0：Normal Sales
1：Advance Sales
2：Deposit Sales
3：Remote Collection
4：Void
5：Refund
6：Exchange' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'TransType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS机编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期。（business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期时间。（system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'ProdDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已退数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'RetiredQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品原始价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'OrgPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品销售价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'UnitPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品实际销售价格。（包括了折扣，改价 ...）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'NetPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' = TotalQty * OrgPrice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'OrgAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' = TotalQty * UnitPrice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'UnitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' = TotalQty * NetPrice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'NetAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品总数量。（一般情况等同Qty，BOM子货品时 = Qty * BOM主货品数量）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'TotalQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品折扣掉的价格（sale off）（一般情况DiscountPrice为负数：NetPrice= UnitPrice+DiscountPrice）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'DiscountPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品折扣掉的金额（sale off）（= TotalQty * DiscountPrice）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'DiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品改价扣除的价格（sale off）（一般情况DiscountPrice为负数：NetPrice= UnitPrice+POPrice）。 DiscountPrice 和 POPrice同时发生时，优先PO，在PO的基础上discount，不算作折上折。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'POPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加价格。（除正常价格外额外的价格，一般是BOM子货品）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'ExtraPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'改价原因编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'POReasonCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品附加信息1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'Additional1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品附加信息2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'Additional2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品附加信息3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'Additional3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'零售价类型。
0： NS， 一般零售
1： BOM， 捆绑销售价格。（作为BOM中的子货品）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'RPriceTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否BOM货品（主货品）。 0：不是， 1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'IsBOM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否Coupon货品。 0：不是， 1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'IsCoupon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否BuyBack货品。 0：不是， 1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'IsBuyBack'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否服务类货品。 0：不是， 1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'IsService'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SerialNo是否有库存。 0：不是， 1：是的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'SerialNoStockFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SerialNo的类型
0： 没有
1： IMSI
2： MSISDN
3： SIM' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'SerialNoType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品唯一序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'SerialNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电子设备唯一标示' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'IMEI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售货品的库存类型。（和扣库有关）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品提货状态。
0：POS提交，需要预扣，还未正式预扣
1：预扣完成
2：POS提交，需要提货，还未提货
4：提货完成
9：无库存货品
-9：运费
被refund的记录
80：对应原单collected状态 0
81：对应原单collected状态 1
82：对应原单collected状态 2
84：对应原单collected状态 4

被Exchange的记录
90：对应原单collected状态 0
91：对应原单collected状态 1
92：对应原单collected状态 2
94：对应原单collected状态 4
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'Collected'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'ReservedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货仓库（店铺）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'PickupLocation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'PickupStaff'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提货日期 （business date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'PickupDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货物送达日期   （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'DeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'DeliveryBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BOM时有效。数据为货品在整个BOM中分摊的单价' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'ActPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'= ActPrice * （TotalQty / Qty）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'ActAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原单交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'OrgTransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原单对应的salesD记录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'OrgSeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留，外部编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'RefGUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售交易表。货品明细表
@2017-03-03 （for bauhaus） 增加字段 RetiredQty， 已退数量。 当 一条记录 的 Qty > 1 时， 退货其中一个， 需要能支持多次退货。 所以需要加此字段标示。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sales_D'
GO
