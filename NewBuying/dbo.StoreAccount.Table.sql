USE [NewBuying]
GO
/****** Object:  Table [dbo].[StoreAccount]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StoreAccount](
	[StoreAccountID] [int] IDENTITY(1,1) NOT NULL,
	[StoreAccountCode] [varchar](64) NOT NULL,
	[StoreID] [int] NOT NULL,
	[POSCode] [varchar](64) NULL,
	[TerminalCode] [varchar](64) NULL,
	[Remark] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_STOREACCOUNT] PRIMARY KEY CLUSTERED 
(
	[StoreAccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[StoreAccount] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[StoreAccount] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreAccount', @level2type=N'COLUMN',@level2name=N'StoreAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreAccount', @level2type=N'COLUMN',@level2name=N'StoreAccountCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreAccount', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商铺的POS编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreAccount', @level2type=N'COLUMN',@level2name=N'POSCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商铺的的终端编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreAccount', @level2type=N'COLUMN',@level2name=N'TerminalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreAccount', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商铺表。（store的子表。目前只是记录一下，没有逻辑功能）
其中的POSCode，TerminalCode 只是做记录，不做判断（包括重复判断）。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StoreAccount'
GO
