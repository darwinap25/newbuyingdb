USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductSize]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProductSize]
  @BrandID              int,
  @Prodcode             varchar(64),       -- 货品编码
  @DepartCode           varchar(64),       -- 部门编码
  @ProdType             int,               -- 货品类型
  @QueryCondition       nvarchar(1000),    -- 自定义查询条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr         varchar(20)=''     -- 1:取Name1,2：取Name2. 3：取Name3.  其他：取Name1     
AS
/****************************************************************************
**  Name : GetProductSize
**  Version: 1.0.0.1
**  Description : 获得Product_Size数据	
**
  declare @DepartCode varchar(64), @a int, @PageCount int, @RecordCount int
  set @DepartCode = '' 
  exec @a = GetProductSize 0,'',@DepartCode,0,  null,0,1000,@PageCount output,@RecordCount output,'zh_BigCN'
  print @a  
  print @PageCount
  print @RecordCount  
  select* from Product_Size
**  Created by: Gavin @2016-03-28
**  Modify by: Gavin @2016-05-09 (ver 1.0.0.1) 防止出来的结果重复，加上distinct
**
****************************************************************************/
BEGIN
  declare @SQLStr nvarchar(4000), @Language int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @BrandID = ISNULL(@BrandID, 0)
  set @Prodcode = isnull(@Prodcode, '')
  set @DepartCode = isnull(@DepartCode, '')
  set @ProdType = isnull(@ProdType, 0)
  set @SQLStr = 'select distinct S.ProductSizeID, S.ProductSizeCode, S.ProductSizeNote, S.ProductSizeType,'
    + ' case ' + cast(@Language as varchar) + ' when 2 then ProductSizeName2 when 3 then ProductSizeName3 else ProductSizeName1 end as ProductSizeName  '	 
    + ' from Product_Size S left join Product P on S.ProductSizeID = P.ProductSizeID ' 
	+ ' where (P.Prodcode = ''' + @Prodcode + ''' or ''' + @Prodcode + ''' = '''')'
	+ ' and (P.DepartCode = ''' + @DepartCode + ''' or ''' + @DepartCode + ''' = '''')' 
    + ' and (P.ProductBrandID = ' + cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + ' = 0)'
    + ' and (P.ProdType = ' + cast(@ProdType as varchar) + ' or ' + cast(@ProdType as varchar) + ' = 0)'    
  exec SelectDataInBatchs @SQLStr, 'ProductSizeCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @QueryCondition
END

GO
