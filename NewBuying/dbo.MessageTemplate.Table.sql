USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessageTemplate]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessageTemplate](
	[MessageTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[MessageTemplateCode] [varchar](64) NOT NULL,
	[MessageTemplateDesc] [nvarchar](512) NULL,
	[BrandID] [int] NULL,
	[OprID] [int] NULL,
	[Remark] [nvarchar](512) NULL,
 CONSTRAINT [PK_MESSAGETEMPLATE] PRIMARY KEY CLUSTERED 
(
	[MessageTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplate', @level2type=N'COLUMN',@level2name=N'MessageTemplateID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplate', @level2type=N'COLUMN',@level2name=N'MessageTemplateCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplate', @level2type=N'COLUMN',@level2name=N'MessageTemplateDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplate', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplate', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplate', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息内容模板。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageTemplate'
GO
