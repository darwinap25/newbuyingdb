USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetFileExchangeSetting]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetFileExchangeSetting]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Select
		f.FileExchangeIntervalMin,
		f.ExecutionTime
	From FileExchangeSetting f
	Where f.ID = 1

END


GO
