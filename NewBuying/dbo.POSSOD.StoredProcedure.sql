USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSSOD]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[POSSOD]
  @StoreCode             VARCHAR(64),              -- POS 的店铺编号
  @RegisterCode          VARCHAR(64),              -- POS 的注册编号
  @BusDate               DATE OUTPUT,              -- 交易的Business date
  @CashierID             INT                       -- 收银员
AS
/****************************************************************************
**  Name : POSSOD
**  Version : 1.0.0.1
**  Description : POS SOD
**
  declare @a int, @BusDate date
  exec @a = POSSOD '1', 'R01', @BusDate output, 1
  print @a  print @BusDate
  select * from sodeod
**  Created by Gavin @2015-03-26
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
****************************************************************************/
BEGIN
  SET @BusDate = ISNULL(@BusDate, GETDATE())
    
  IF NOT EXISTS(SELECT * FROM SODEOD WHERE BusDate = @BusDate AND StoreCode = @StoreCode)  
  BEGIN
    INSERT INTO SODEOD
    VALUES(@BusDate, @StoreCode, 1, 0, GETDATE())
    RETURN 0
  END 
  ELSE
  BEGIN 
    IF EXISTS(SELECT * FROM SODEOD WHERE BusDate = @BusDate AND SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode)
      RETURN 0
    ELSE   
      RETURN -513
  END    
END

GO
