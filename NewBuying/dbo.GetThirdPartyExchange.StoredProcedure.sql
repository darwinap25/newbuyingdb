USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetThirdPartyExchange]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetThirdPartyExchange]
  @CardNumber			varchar(512),   --  卡号  
  @ThirdPartyCode       varchar(512),   --  第三方编码 
  @LanguageAbbr			varchar(20)=''
AS
/****************************************************************************
**  Name : GetThirdPartyExchange  
**  Version: 1.0.0.0
**  Description : 获得会员卡金额或积分和第三方卡的金额或者积分的兑换比率
**  Parameter :
**
  exec MemberCardExchangeThirdParty
**  Created by: Gavin @2012-05-21
**
****************************************************************************/
begin
  declare @CardGradeID int, @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  select @CardGradeID = CardGradeID from Card where CardNumber = @CardNumber
  if @@ROWCOUNT = 0
    return -2
  select T.ThirdPartyID, T.ThirdPartyCode, case @Language when 2 then ThirdPartyName2 when 3 then ThirdPartyName3 else ThirdPartyName1 end as ThirdPartyName,
     E.CardGradeID, CardAMTToThirdAMT, CardAMTToThirdPoint, CardPointToThirdAMT, CardPointToThirdPoint, 
     ThirdAMTToCardAMT, ThirdPointToCardAMT, ThirdAMTToCardPoint, ThirdPointToCardPoint
    from ThirdPartyExchange E left join ThirdParty T on E.ThirdPartyID = T.ThirdPartyID 
   where (T.ThirdPartyCode = @ThirdPartyCode or (isnull(@ThirdPartyCode, '') = '')) and E.CardGradeID = @CardGradeID

  return 0
end

GO
