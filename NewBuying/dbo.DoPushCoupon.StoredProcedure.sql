USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoPushCoupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoPushCoupon]
  @CouponPushNumber varchar(64),   -- 推送单的单号
  @PushCardBrandID  int, 
  @PushCardTypeID   int, 
  @PushCardGradeID  int, 
  @RepeatPush       int,
  @CreatedBy        int,
  @ApprovalCode     char(6) output
AS
/****************************************************************************
**  Name : DoPushCoupon
**  Version: 1.0.0.3
**  Description : Ord_CouponPush_H 触发器中调用，用于实现推送coupon
**
**  Parameter :
  declare @a int  
  exec @a = DoPushCoupon_New '', 0
  print @a  
**  Created by: Gavin @2013-03-06
**  Modify by: Gavin @2013-03-13 (ver 1.0.0.1) Coupon_movement的reftxnno中填入  @CouponPushNumber 。
                                              选卡时加上条件：isnull(MemberID, 0) <> 0  and C.status = 2 
**  Modify by: Gavin @2014-09-18 (ver 1.0.0.2) (for 711) 赠送Coupon时发送消息。（要压力测试） 
**  Modify by: Gavin @2014-12-22 (ver 1.0.0.3) 优化速度                                             
**
****************************************************************************/
begin 
  declare @BusDate date, @TxnDate datetime, @CouponTypeID int, @CouponCount int, @OprID int
  declare @CouponExpiryDate datetime, @NewCouponExpiryDate datetime, @CardNumber varchar(64), @CouponNumber varchar(64),
          @CouponStatus int, @NewCouponStatus int, @CouponAmount money, @MemberID int, @CardTypeID int, @CardGradeID int, @CardBrandID int
  declare @i int
  set @CouponExpiryDate = getdate() 
  set @PushCardBrandID = isnull(@PushCardBrandID, 0)
  set @PushCardTypeID = isnull(@PushCardTypeID, 0)
  set @PushCardGradeID = isnull(@PushCardGradeID, 0)
  set @RepeatPush = isnull(@RepeatPush, 0)
  
  --设置Busdate
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  if @BusDate is null
    set @BusDate=convert(varchar(10), getdate(), 120)
  set @TxnDate=getdate()
 
  --获得Approvalcode  
  exec GenApprovalCode @ApprovalCode output 

  DECLARE CUR_DoPushCoupon_CouponType CURSOR fast_forward local FOR
    SELECT CouponTypeID, sum(CouponQty) as CouponCount FROM Ord_CouponPush_D where CouponPushNumber = @CouponPushNumber
       Group By CouponTypeID 
  OPEN CUR_DoPushCoupon_CouponType
  FETCH FROM CUR_DoPushCoupon_CouponType INTO @CouponTypeID, @CouponCount
  WHILE @@FETCH_STATUS=0
  BEGIN
    exec CalcCouponNewExpiryDate @CouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output 

    INSERT INTO COUPON_MOVEMENT
       (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
        BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
        OrgExpirydate, OrgStatus, NewStatus)    
    select 32, A.CardNumber, B.CouponNumber, @CouponTypeID, '', 0, @COUPONPUSHNUMBER, B.CouponAmount, 0, B.CouponAmount,
       @BUSDATE, @TXNDATE, '','', A.MemberID, @NewCouponExpiryDate, @ApprovalCode, NULL, '', '',
       B.CouponExpiryDate, 1, 2
    from
    (                      
      select ROW_NUMBER() OVER(order by CardNumber) as iid, CardNumber, MemberID, C.CardTypeID, C.CardGradeID, T.BrandID
        from Card C left join CardType T on C.CardTypeID = T.CardTypeID 
        where (T.BrandID = @PushCardBrandID or @PushCardBrandID = 0) 
          and (C.CardTypeID = @PushCardTypeID or @PushCardTypeID = 0)
          and (C.CardGradeID = @PushCardGradeID or @PushCardGradeID = 0)
          and C.status = 2 and isnull(MemberID, 0) <> 0  
          and ( (@RepeatPush = 1) or
                (not exists(select CouponNumber from Coupon where CardNumber = C.CardNumber and CouponTypeID = @CouponTypeID))
              )  
     ) A,
     (
      select ROW_NUMBER() OVER(order by CardNumber) as iid, CouponNumber, CouponAmount, CouponExpiryDate 
        from coupon where Status = 1 and CouponTypeID = @CouponTypeID      
     ) B where A.iid = B.iid   
            
    FETCH FROM CUR_DoPushCoupon_CouponType INTO @CouponTypeID, @CouponCount
  END
  CLOSE CUR_DoPushCoupon_CouponType 
  DEALLOCATE CUR_DoPushCoupon_CouponType   
    
end

GO
