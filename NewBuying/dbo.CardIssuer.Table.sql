USE [NewBuying]
GO
/****** Object:  Table [dbo].[CardIssuer]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardIssuer](
	[CardIssuerID] [int] IDENTITY(1,1) NOT NULL,
	[CardIssuerName1] [nvarchar](512) NULL,
	[CardIssuerName2] [nvarchar](512) NULL,
	[CardIssuerName3] [nvarchar](512) NULL,
	[CardIssuerDesc1] [nvarchar](max) NULL,
	[CardIssuerDesc2] [nvarchar](max) NULL,
	[CardIssuerDesc3] [nvarchar](max) NULL,
	[DomesticCurrencyID] [int] NOT NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [CardIssuer_PK] PRIMARY KEY CLUSTERED 
(
	[CardIssuerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'CardIssuerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行方名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'CardIssuerName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行方名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'CardIssuerName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行方名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'CardIssuerName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行方描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'CardIssuerDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行方描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'CardIssuerDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发行方描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'CardIssuerDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'本币的currencyid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer', @level2type=N'COLUMN',@level2name=N'DomesticCurrencyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡发行方' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CardIssuer'
GO
