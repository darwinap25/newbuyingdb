USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponTypeStoreCondition]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponTypeStoreCondition](
	[CouponTypeStoreConditionID] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[StoreConditionType] [int] NULL DEFAULT ((1)),
	[ConditionType] [int] NULL DEFAULT ((1)),
	[ConditionID] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_COUPONTYPESTORECONDITION] PRIMARY KEY CLUSTERED 
(
	[CouponTypeStoreConditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Trigger [dbo].[UpdateStoreCondition_CouponTypeStoreCondition]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UpdateStoreCondition_CouponTypeStoreCondition] ON [dbo].[CouponTypeStoreCondition]
For INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: UpdateStoreCondition_CouponTypeStoreCondition
* Version: 1.0.0.0
* Description :
* Create By Gavin @2012-07-23
*/
/*==============================================================*/
BEGIN
  exec UpdateCouponTypeStoreCondition 0,0,0,0 
END

GO
/****** Object:  Trigger [dbo].[UpdateStoreCondition_DeleteCouponStoreCondition]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UpdateStoreCondition_DeleteCouponStoreCondition] ON [dbo].[CouponTypeStoreCondition]
For DELETE
AS
/*==============================================================*/
/*                
* Name: UpdateStoreCondition_DeleteCouponStoreCondition
* Version: 1.0.0.0
* Description :
* Create By Gavin @2012-07-23
*/
/*==============================================================*/
BEGIN
  declare @CouponTypeID int, @StoreConditionType int, @ConditionType int, @ConditionID int
  select @CouponTypeID = CouponTypeID, @StoreConditionType = StoreConditionType, @ConditionType = ConditionType, @ConditionID = ConditionID from deleted
  delete from CouponTypeStoreCondition_List 
  where CouponTypeID = @CouponTypeID and StoreConditionType = @StoreConditionType 
    and ConditionType = @ConditionType and ConditionID = @ConditionID
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeStoreCondition', @level2type=N'COLUMN',@level2name=N'CouponTypeStoreConditionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'coupontype ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeStoreCondition', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条件类型。 1：发布店铺。2：使用店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeStoreCondition', @level2type=N'COLUMN',@level2name=N'StoreConditionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'输入的条件类型。1：brand。2：Location。3：store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeStoreCondition', @level2type=N'COLUMN',@level2name=N'ConditionType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条件具体的ID。 例如：ConditionType=1时，输入的为brandID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeStoreCondition', @level2type=N'COLUMN',@level2name=N'ConditionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵的店铺条件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponTypeStoreCondition'
GO
