USE [NewBuying]
GO
/****** Object:  Table [dbo].[Nation]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nation](
	[NationID] [int] IDENTITY(1,1) NOT NULL,
	[NationCode] [varchar](64) NULL,
	[CountryCode] [varchar](64) NULL,
	[NationName1] [nvarchar](512) NULL,
	[NationName2] [nvarchar](512) NULL,
	[NationName3] [nvarchar](512) NULL,
	[NationFlagFile] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_NATION] PRIMARY KEY CLUSTERED 
(
	[NationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation', @level2type=N'COLUMN',@level2name=N'NationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家编码: ZH-CN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation', @level2type=N'COLUMN',@level2name=N'NationCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家代码: 086' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation', @level2type=N'COLUMN',@level2name=N'NationName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation', @level2type=N'COLUMN',@level2name=N'NationName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation', @level2type=N'COLUMN',@level2name=N'NationName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国旗图标文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation', @level2type=N'COLUMN',@level2name=N'NationFlagFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nation'
GO
