USE [NewBuying]
GO
/****** Object:  Table [dbo].[InitialOptionData]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InitialOptionData](
	[TableName] [varchar](100) NOT NULL,
	[FieldName] [varchar](100) NOT NULL,
	[FieldValue] [int] NOT NULL,
	[LanguageAbbr] [varchar](512) NOT NULL,
	[Describtion] [varchar](512) NULL,
 CONSTRAINT [PK_INITIALOPTIONDATA] PRIMARY KEY CLUSTERED 
(
	[TableName] ASC,
	[FieldName] ASC,
	[FieldValue] ASC,
	[LanguageAbbr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[InitialOptionData] ADD  DEFAULT ('ALLTABLE') FOR [TableName]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表名。不定表名默认使用ALLTABLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InitialOptionData', @level2type=N'COLUMN',@level2name=N'TableName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字段名。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InitialOptionData', @level2type=N'COLUMN',@level2name=N'FieldName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字段值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InitialOptionData', @level2type=N'COLUMN',@level2name=N'FieldValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'语言。LanguageMap表的LanguageAbbr' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InitialOptionData', @level2type=N'COLUMN',@level2name=N'LanguageAbbr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'值对应的描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InitialOptionData', @level2type=N'COLUMN',@level2name=N'Describtion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'初始数据表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InitialOptionData'
GO
