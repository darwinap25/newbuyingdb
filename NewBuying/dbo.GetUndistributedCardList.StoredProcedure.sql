USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetUndistributedCardList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetUndistributedCardList]
  @CardTypeID           int,            -- 指定的cardtypeid.
  @CardGradeID          int,             -- 指定的cardgradeid.  
  @ConditionStr			nvarchar(400),   -- 特定查询条件。
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1       
AS
/****************************************************************************
**  Name : GetUndistributedCardList  获得未分配的卡号列表
**  Version: 1.0.0.2
**  Description : 获得未分配的卡号列表， 用户根据此做选择。
**  Parameter :
**
 declare @a int, @NewPassword			varchar(512)
  ,@PageCount			int
  ,@RecordCount			int
-- set @NewPassword = '123'
  exec @a=GetUndistributedCardList 0, 1, '', 1, 20, @PageCount output,  @RecordCount output, ''
  print @a
  print @NewPassword
  select * from member
**  Created by: Gavin @2012-12-12
**  Modify by: Gavin @2012-12-13 (ver 1.0.0.1) 改成 0,1 状态的卡 都可以取出
**  Modify by: Gavin @2012-12-13 (ver 1.0.0.2) 按King的要求,取出的card 需要随机排序,并且条件hardcode: select * from card order when cardnumber= %888888% 
**
****************************************************************************/
begin
    
  declare @SQLStr nvarchar(4000), @Language int, @OrderCondition nvarchar(100)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  
  set @CardTypeID = isnull(@CardTypeID, 0)
  set @CardGradeID = isnull(@CardGradeID, 0)
  set @SQLStr = ' select C.CardNumber, C.CardTypeID, C.CardGradeID, isnull(M.CardUID, '''') as CardUID from Card C left join CardUIDMap M on C.CardNumber = M.CardNumber '
    + ' where (C.CardNumber >= ''88888800'') and (C.CardNumber <= ''88888899'') and '
    + '  (C.CardTypeID = ' + Cast(@CardTypeID as varchar) + ' or ' + Cast(@CardTypeID as varchar) + ' = 0) '
    + ' and (C.CardGradeID = ' + Cast(@CardGradeID as varchar) + ' or ' + Cast(@CardGradeID as varchar) + ' = 0) '
  if isnull(@ConditionStr, '') <> ''
    set @SQLStr = @SQLStr + ' and (' + @ConditionStr  + ')'
  else set @SQLStr = @SQLStr + ' and C.Status in (0,1) '  
  
  set @OrderCondition = 'order by newid() '
  exec SelectDataInBatchs @SQLStr, 'CardNumber', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition   
 -- sp_helptext SelectDataInBatchs
  return 0
end

GO
