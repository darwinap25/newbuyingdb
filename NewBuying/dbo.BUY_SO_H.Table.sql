USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_SO_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_SO_H](
	[SOCode] [varchar](64) NOT NULL,
	[OrderType] [int] NOT NULL,
	[VendorCode] [varchar](64) NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[CurrencyCode] [varchar](64) NOT NULL,
	[Note1] [nvarchar](512) NULL,
	[Note2] [nvarchar](512) NULL,
	[Note3] [nvarchar](512) NULL,
	[ExpectDate] [datetime] NOT NULL,
	[TotalAmount] [dbo].[Buy_Amt] NOT NULL,
	[VATAmount] [dbo].[Buy_Amt] NOT NULL,
	[DiscountAmount] [dbo].[Buy_Amt] NOT NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_SO_H] PRIMARY KEY CLUSTERED 
(
	[SOCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_SO_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_SO_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'SOCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型： 1:WH order 3:Store Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'VendorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'CurrencyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'Note1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'Note2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'Note3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预期装货日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'ExpectDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单总金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'TotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单增值税总额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'VATAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单折扣总额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'DiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺订货单主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_SO_H'
GO
