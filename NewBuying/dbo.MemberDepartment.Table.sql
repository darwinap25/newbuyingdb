USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberDepartment]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberDepartment](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[DeptCode] [varchar](64) NOT NULL,
	[DeptName1] [nvarchar](512) NULL,
	[DeptName2] [nvarchar](512) NULL,
	[DeptName3] [nvarchar](512) NULL,
	[DeptPhone] [varchar](512) NULL,
	[DeptAddress] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MemberDepartment] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberDepartment] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[MemberDepartment] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment', @level2type=N'COLUMN',@level2name=N'DeptCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment', @level2type=N'COLUMN',@level2name=N'DeptName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment', @level2type=N'COLUMN',@level2name=N'DeptName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment', @level2type=N'COLUMN',@level2name=N'DeptName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment', @level2type=N'COLUMN',@level2name=N'DeptPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment', @level2type=N'COLUMN',@level2name=N'DeptAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员部门表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberDepartment'
GO
