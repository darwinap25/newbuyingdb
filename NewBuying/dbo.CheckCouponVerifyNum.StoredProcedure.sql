USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckCouponVerifyNum]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckCouponVerifyNum]
  @CouponNumber varchar(64),  
  @VerifyNum varchar(64)
AS
/****************************************************************************
**  Name : CheckCouponVerifyNum
**  Version: 1.0.0.0
**  Description : 检查coupon表中的VerifyNum是否符合,并且是不是过期.
  declare @VerifyNum varchar(64) , @a int
  exec @a = CheckCouponVerifyNum '01000001', '2928'
  print @a
  
  select top 10 * from coupon	select top 10 * from coupontype   
**  Created by: Gavin @2014-02-19
**
****************************************************************************/
begin
  declare @IsNeedVerifyNum int, @VerifyNumLifecycle int, @CouponDBVerifyNum varchar(64), @VerifyNumUpdatedOn datetime,
     	@ExpirationTime datetime
  select @CouponDBVerifyNum = VerifyNum, @VerifyNumUpdatedOn = VerifyNumUpdatedOn,
      @IsNeedVerifyNum = IsNeedVerifyNum, @VerifyNumLifecycle = VerifyNumLifecycle
    from Coupon C left join CouponType T on C.CouponTypeID = T.CouponTypeID 
  where CouponNumber = @CouponNumber
    
  set @VerifyNum = RTrim(isnull(@VerifyNum, ''))
  set @CouponDBVerifyNum = RTrim(isnull(@CouponDBVerifyNum, ''))
  set @IsNeedVerifyNum = isnull(@IsNeedVerifyNum, 0)
  set @VerifyNumLifecycle = isnull(@VerifyNumLifecycle, 0)
  set @VerifyNumUpdatedOn = isnull(@VerifyNumUpdatedOn, getdate())
  
  if @IsNeedVerifyNum = 1
  begin   
	if @VerifyNum = @CouponDBVerifyNum 
	begin
	  set @ExpirationTime = DateAdd(mm, @VerifyNumLifecycle, @VerifyNumUpdatedOn) 	  
	  if @ExpirationTime >= getdate()
	    return 0
	  else
	    return -106
	end else
	begin
	  return -105
	end
  end else
  begin
	return 0
  end   
end

GO
