USE [NewBuying]
GO
/****** Object:  Table [dbo].[SystemCode]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemCode](
	[SSCode] [char](3) NOT NULL,
	[SSName] [varchar](20) NULL,
 CONSTRAINT [PK_SYSTEMCODE] PRIMARY KEY CLUSTERED 
(
	[SSCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCode', @level2type=N'COLUMN',@level2name=N'SSCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCode', @level2type=N'COLUMN',@level2name=N'SSName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统编码
两条记录：
SVA
BUY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCode'
GO
