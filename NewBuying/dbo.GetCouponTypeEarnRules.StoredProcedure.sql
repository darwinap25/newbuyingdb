USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCouponTypeEarnRules]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetCouponTypeEarnRules]												
  @CouponTypeID			int,
  @ConditionStr			nvarchar(1000),     -- 查询条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=1 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1  
AS
/****************************************************************************
**  Name : GetCouponTypeEarnRules
**  Version: 1.0.0.1
**  Description : 获取coupon的兑换方式. 不做规则权限校验
**
**  Parameter :
  exec GetCouponTypeEarnRules 92 , ''
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**  Modify by: Gavin @2013-09-23 (ver 1.0.0.1) 增加换页方式，增加自定义条件，修改查询条件。增加返回字段。
**
****************************************************************************/
begin 
  declare @SQLStr varchar(2000)
  
  set @CouponTypeID = isnull(@CouponTypeID, 0) 
       
  set @SQLStr = 'select KeyID, ExchangeType, CouponTypeID, ExchangeAmount, ExchangePoint, ExchangeCouponTypeID, ExchangeCouponCount, ExchangeRank, '
     + ' ExchangeConsumeRuleOper, ExchangeConsumeAmount, CardTypeIDLimit, CardGradeIDLimit, CardTypeBrandIDLimit, StoreBrandIDLimit, ' 
     + ' StoreGroupIDLimit, StoreIDLimit, MemberBirthdayLimit, MemberSexLimit, MemberAgeMinLimit, MemberAgeMaxLimit, '
     + ' StartDate, EndDate, Status '
     + ' from EarnCouponRule where (CouponTypeID = ' + cast(@CouponTypeID as varchar) + ' or ' + cast(@CouponTypeID as varchar) + ' = 0) '
      
  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr
  return 0 
end

GO
