USE [NewBuying]
GO
/****** Object:  Table [dbo].[Store]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Store](
	[StoreID] [int] IDENTITY(1,1) NOT NULL,
	[StoreCode] [varchar](64) NULL,
	[Status] [int] NULL CONSTRAINT [DF__Store__Status__5B3E2ADD]  DEFAULT ((1)),
	[StoreName1] [nvarchar](512) NULL,
	[StoreName2] [nvarchar](512) NULL,
	[StoreName3] [nvarchar](512) NULL,
	[StoreTypeID] [int] NULL,
	[StoreGroupID] [int] NULL,
	[BankID] [int] NULL,
	[BrandID] [int] NULL,
	[StoreCountry] [varchar](64) NULL,
	[StoreProvince] [varchar](64) NULL,
	[StoreCity] [varchar](64) NULL,
	[StoreDistrict] [varchar](64) NULL,
	[StoreAddressDetail] [varchar](512) NULL,
	[StoreAddressDetail2] [varchar](512) NULL,
	[StoreAddressDetail3] [varchar](512) NULL,
	[StoreFullDetail] [nvarchar](512) NULL,
	[StoreFullDetail2] [nvarchar](512) NULL,
	[StoreFullDetail3] [nvarchar](512) NULL,
	[StoreTel] [varchar](512) NULL,
	[StoreFax] [varchar](512) NULL,
	[Email] [nvarchar](512) NULL,
	[Contact] [nvarchar](512) NULL,
	[ContactPhone] [nvarchar](512) NULL,
	[StoreLongitude] [varchar](512) NULL,
	[StoreLatitude] [varchar](512) NULL,
	[StorePicFile] [nvarchar](512) NULL,
	[MapsPicFile] [nvarchar](512) NULL,
	[MapsPicShadowFile] [nvarchar](512) NULL,
	[LocationID] [int] NULL,
	[StoreNote] [nvarchar](512) NULL,
	[StoreOpenTime] [varchar](512) NULL,
	[StoreCloseTime] [varchar](512) NULL,
	[PickupStoreFlag] [int] NULL CONSTRAINT [DF__Store__PickupSto__5C324F16]  DEFAULT ((1)),
	[ReturnStoreFlag] [int] NULL CONSTRAINT [DF__Store__ReturnSto__5D26734F]  DEFAULT ((0)),
	[CompanyID] [int] NULL,
	[CreatedOn] [datetime] NULL CONSTRAINT [DF__Store__CreatedOn__5E1A9788]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL CONSTRAINT [DF__Store__UpdatedOn__5F0EBBC1]  DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED 
(
	[StoreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[UpdateStoreCondition_DeleteStore]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UpdateStoreCondition_DeleteStore] ON [dbo].[Store]
For DELETE
AS
/*==============================================================*/
/*                
* Name: UpdateStoreCondition_DeleteStore
* Version: 1.0.0.0
* Description :
* Create By Gavin @2012-07-23
*/
/*==============================================================*/
BEGIN
  delete from CouponTypeStoreCondition_list where StoreID in (select StoreID from deleted)
  delete from CardGradeStoreCondition_list where StoreID in (select StoreID from deleted)
END

GO
/****** Object:  Trigger [dbo].[UpdateStoreCondition_Store]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UpdateStoreCondition_Store] ON [dbo].[Store]
For INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: UpdateStoreCondition_Store
* Version: 1.0.0.0
* Description :
* Create By Gavin @2012-07-23
*/
/*==============================================================*/
BEGIN
  exec UpdateCardGradeStoreCondition 0,0,0,0  
  exec UpdateCouponTypeStoreCondition 0,0,0,0 
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码（不是唯一的）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺状态。0：关闭。1：开放' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺名称。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺名称。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺名称。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属店铺组' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'BankID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺品牌' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺所在国家（存放Country表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreCountry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺所在省 （存放Province表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreProvince'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺所在城市 （存放City表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreCity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺地址所在区县 （存放District表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreDistrict'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺详细地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreAddressDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺详细地址（第二种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreAddressDetail2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺详细地址（第三种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreAddressDetail3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺完整地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreFullDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺完整地址（第二种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreFullDetail2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺完整地址（第三种语言）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreFullDetail3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreTel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreFax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺坐标，经度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreLongitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺坐标，纬度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreLatitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺图片地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StorePicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺图标文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'MapsPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'阴影图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'MapsPicShadowFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区域ID，外键。一个店铺 只能属于一个 区域。（  最下层的那个区域）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'LocationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开店时间，例如 09:00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreOpenTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关店时间，例如 20:00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'StoreCloseTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否提货店铺标志。0：非提货店铺。 1：提货店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'PickupStoreFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否退货店铺标志。0：非退货店铺。 1：接受退货的店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'ReturnStoreFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键， Company表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺表。
2012-06-25：取消storecode的唯一约束，允许不同记录相同的storecode
@2016-01-14： 增加字段ReturnStoreFlag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Store'
GO
