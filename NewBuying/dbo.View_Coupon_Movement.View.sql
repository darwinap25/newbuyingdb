USE [NewBuying]
GO
/****** Object:  View [dbo].[View_Coupon_Movement]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Coupon_Movement]
AS
/*
RRG 的SVA Web使用, 目的为了能产生 oprid=40的movement数据. (因为RRG大批量数据，为加快速度，40的movement不再产生了)
*/
select ServerCode,StoreID,RegisterCode,OprID,RefTxnNo,BusDate,Txndate,ApprovalCode,CouponTypeID, OpenBal, Amount,CloseBal,
  CouponNumber, CreatedOn
from coupon_movement
union all
SELECT '', H.StoreID, '', 40, H.CouponPickingNumber, H.ApproveBusDate, H.ApproveOn, H.ApprovalCode, C.CouponTypeID,
  C.CouponAmount, 0, C.CouponAmount, D.FirstCouponNumber, H.UpdatedOn   
 from Ord_CouponPicking_D D 
   left join Ord_CouponPicking_H H on H.CouponPickingNumber = D.CouponPickingNumber
   left join Coupon C on D.FirstCouponNumber = C.CouponNumber
   where H.ApproveStatus = 'A'


GO
