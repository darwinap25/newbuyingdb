USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SaveMemberPresent]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SaveMemberPresent]
  @UserID				varchar(512),
  @TxnNo				varchar(512),  
  @Busdate				datetime,
  @Txndate				datetime,
  @CardNumber			varchar(512),
  @ReceiveCardNumber	varchar(512),
  @ReceiveMobileNumber	varchar(512),
  @ActAmount			money,
  @ActPoints			int,
  @ActCouponNumbers		varchar(max),
  @ActionDate			datetime,
  @Status				int,
  @Remark				nvarchar(512),
  @StoreID				int,
  @RegisterCode			varchar(512),
  @ServerCode			varchar(512),  
  @MemberPresentID		int output
AS
/****************************************************************************
**  Name : SaveMemberPresent
**  Version: 1.0.0.1
**  Description : 保存用户的转赠请求
**
**  Parameter :

**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**  Modify by: Gavin @2012-10-22 （ver 1.0.0.1）不传入Busdate，由存储过程自己获取
**MemberClause
****************************************************************************/
begin
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
    
  insert into MemberPresent
    (CardNumber, ReceiveCardNumber, ActAmount, ActPoints, ActCouponNumbers, ActionDate, Status, Remark, StoreID, RegisterCode, ServerCode, CreatedOn, UpdatedOn, RefTxnNo, ReceiveMobileNumber, Busdate, TxnDate)
  values
    (@CardNumber, @ReceiveCardNumber, @ActAmount, @ActPoints, @ActCouponNumbers, @ActionDate, @Status, @Remark, @StoreID, @RegisterCode, @ServerCode, Getdate(), GetDate(), @TxnNo, @ReceiveMobileNumber, @Busdate, @TxnDate)    
  
  set @MemberPresentID = SCOPE_IDENTITY()
  return 0 
end

GO
