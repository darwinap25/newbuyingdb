USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSHourReport]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[POSHourReport]
  @StoreCode          VARCHAR(64),     -- StoreCode
  @RegisterCode       VARCHAR(64),     -- RegisterCode
  @BusDate            DATETIME,        -- 交易日
  @CashierID          INT              -- 收银员
AS
/****************************************************************************
**  Name : POSHourReport
**  Version : 1.0.0.1
**  Description : POS 小时报表
**
declare @a int
exec @a=POSHourReport '1','R01','2015-03-24',1
print @a
select * from posstaff
select * from sales_h

**  Created by Gavin @2015-03-26
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
****************************************************************************/
BEGIN
  SET NOCOUNT ON
  DECLARE @LineLen INT, @LineStr VARCHAR(100), @SPACESTR VARCHAR(50)
  DECLARE @Report TABLE(KeyID INT IDENTITY(1,1), LineStr VARCHAR(100))  
  SET @LineLen = 100
  SET @SPACESTR = '                                                                 '
  DECLARE @TotalQty INT, @TotalAmt MONEY, @i INT, @LINESUM MONEY
  SET @TotalAmt = 0
  SET @TotalQty = 0
  SET @i = 0
  SET @CashierID = ISNULL(@CashierID, 0)
  SET @BusDate = ISNULL(@BusDate, 0)
  IF @BusDate = 0
    SELECT TOP 1 @BusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode  ORDER BY BusDate desc
      
  SET @LineStr = '******************************************'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '               CONSOLIDATED'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '                 时段报表'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '******************************************'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = LEFT(RTRIM('店舖號:' + @StoreCode) + @SPACESTR, 24)
  SET @LineStr = @LineStr + RTRIM('交易日:' + CONVERT(VARCHAR(10), @BusDate, 120))
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)  
  
  -- 数据开始
  SET @LineStr = '小時 (24H)                            銷售'
  INSERT INTO @Report  VALUES(@LineStr)   
  
  WHILE @i < 24
  BEGIN   
    SELECT @LINESUM = SUM(ISNULL(A.TotalAmount,0)) FROM Sales_H A
    WHERE A.StoreCode = @StoreCode AND A.RegisterCode = @RegisterCode AND A.BusDate = @BusDate 
      AND (A.CashierID = @CashierID or @CashierID = 0)
      AND A.InvalidateFlag = 0 AND A.Status = 5 AND A.TransType NOT IN (4,5)
      AND DATEPART(HH, A.TxnDate) = @i 
    SET @LineStr = RIGHT('00' + CAST(@i AS VARCHAR), 2) + ':00 - ' + RIGHT('00' + CAST(@i AS VARCHAR), 2) + ':59'
    SET @LineStr = @LineStr + RIGHT(@SPACESTR + CAST(ISNULL(@LINESUM,0) AS VARCHAR), 29)    
    INSERT INTO @Report  VALUES(@LineStr)
    
    SET @TotalAmt = @TotalAmt + ISNULL(@LINESUM,0)
    SET @i = @i + 1
  END
  -- 数据结束

  SET @LineStr = '------------------------------------------'
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '净销售统计' + RIGHT(@SpaceStr + CAST(ISNULL(@TotalAmt,0) AS VARCHAR), 32)
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '------------------------------------------'
  INSERT INTO @Report  VALUES(@LineStr)

  SELECT @LINESUM = SUM(ISNULL(A.TotalAmount,0)) FROM Sales_H A
    WHERE A.StoreCode = @StoreCode AND A.RegisterCode = @RegisterCode AND A.BusDate = @BusDate 
      AND A.InvalidateFlag = 0 AND A.Status = 5 AND A.TransType = 5
      AND (A.CashierID = @CashierID or @CashierID = 0)
  SET @LineStr = '退回' + RIGHT(@SpaceStr + CAST(ISNULL(@LINESUM,0) AS VARCHAR), 38)
  INSERT INTO @Report  VALUES(@LineStr)

  SELECT @LINESUM = SUM(ISNULL(A.TotalAmount,0)) FROM Sales_H A
    WHERE A.StoreCode = @StoreCode AND A.RegisterCode = @RegisterCode AND A.BusDate = @BusDate 
      AND A.InvalidateFlag = 0 AND A.Status = 5 AND A.TransType = 4
      AND (A.CashierID = @CashierID or @CashierID = 0)
  SET @LineStr = '作废' + RIGHT(@SpaceStr + CAST(ISNULL(@LINESUM,0) AS VARCHAR), 38)
  INSERT INTO @Report  VALUES(@LineStr)

  SET @LineStr = '折扣' + RIGHT(@SpaceStr + '0.00', 38)
  INSERT INTO @Report  VALUES(@LineStr)
  
  SET @LineStr = '------------------------------------------'
  INSERT INTO @Report  VALUES(@LineStr)

  SELECT @LINESUM = SUM(ISNULL(A.TotalAmount,0)) FROM Sales_H A
    WHERE A.StoreCode = @StoreCode AND A.RegisterCode = @RegisterCode AND A.BusDate = @BusDate 
      AND A.Status = 5 AND A.TransType NOT IN (4,5)   
      AND (A.CashierID = @CashierID or @CashierID = 0)                      
  SET @LineStr = '毛销售统计' + RIGHT(@SPACESTR + CAST(ISNULL(@LINESUM,0) AS VARCHAR), 15)
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = '=========================================='
  INSERT INTO @Report  VALUES(@LineStr)    
  SET @LineStr = @SPACESTR
  INSERT INTO @Report  VALUES(@LineStr)
  SET @LineStr = CONVERT(VARCHAR(19), GETDATE(), 120) + RIGHT(@SPACESTR + @StoreCode, 8) + RIGHT(@SPACESTR + @RegisterCode, 4)+ RIGHT(@SPACESTR + CAST(@CashierID as VARCHAR), 11)
  INSERT INTO @Report  VALUES(@LineStr) 
  SET @LineStr = @SPACESTR
  INSERT INTO @Report  VALUES(@LineStr)   
  SET @LineStr = '                  報告結束'
  INSERT INTO @Report  VALUES(@LineStr)                 
                    
  SELECT * FROM  @Report 
  SET NOCOUNT OFF      
  RETURN 0
END

GO
