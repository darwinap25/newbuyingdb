USE [NewBuying]
GO
/****** Object:  Table [dbo].[DistributeTemplate]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DistributeTemplate](
	[DistributionID] [int] IDENTITY(1,1) NOT NULL,
	[DistributionCode] [varchar](64) NOT NULL,
	[DistributionDesc1] [nvarchar](512) NULL,
	[DistributionDesc2] [nvarchar](512) NULL,
	[DistributionDesc3] [nvarchar](512) NULL,
	[TemplateFile] [nvarchar](512) NULL,
	[Remark] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_DISTRIBUTETEMPLATE] PRIMARY KEY CLUSTERED 
(
	[DistributionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[DistributeTemplate] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DistributeTemplate] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate', @level2type=N'COLUMN',@level2name=N'DistributionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发布方法编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate', @level2type=N'COLUMN',@level2name=N'DistributionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分发描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate', @level2type=N'COLUMN',@level2name=N'DistributionDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分发描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate', @level2type=N'COLUMN',@level2name=N'DistributionDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分发描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate', @level2type=N'COLUMN',@level2name=N'DistributionDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板文件名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate', @level2type=N'COLUMN',@level2name=N'TemplateFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分发模板
发布方法包括：
1.	Delivery
2.	Email
3.	SMS
4.	Social Network
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistributeTemplate'
GO
