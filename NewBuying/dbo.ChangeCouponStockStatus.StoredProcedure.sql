USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ChangeCouponStockStatus]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[ChangeCouponStockStatus]
  @TypeID                int,          -- 类型: 1. 补充货品规则运行,产生订单.  2. OrderToSupplier. 3: CouponOrderForm (增加了是否产生picking单的状态). 4:收货单。 5：Picking单. 6；退货单, 7:送货单
  @OrderNumber           varchar(64),  -- 订单号码
  @Act                   int           -- 1. 批核动作.  2：作废。 3: 产生picking单.(特例，只对于CouponOrderForm)
AS
/****************************************************************************
**  Name : ChangeCouponStockStatus
**  Version: 1.0.0.3
**  Description : Coupon库存状态变化逻辑处理过程.(应订单操作产生的实体Coupon库存变动.)
**  example :
  exec GenUserMessage 1, 7, 1
  select * from UserMessageSetting_H  
  select * from UserMessageSetting_D
  select * from Accounts_Users
  select cast(MessageBody as nvarchar(max)), * from  messageobject
  select * from  messagereceivelist  
**  Created by: Gavin @2014-07-28
**  Created by: Gavin @2014-09-01 (ver 1.0.0.1) 修正Coupon_Onhand是否存在记录的判断错误。（错误会导致Coupon_onhand主键冲突）
**  Created by: Gavin @2014-10-10 (ver 1.0.0.2) 供应商订单批核,写入Coupon_onhand，在更新Coupon的StockStatus事，同步更新LocateStoreID，方便之后对数。
**  Modified by Darwin 2017-10-24 - if @TypeID , Coupon.Status = 1 and Coupon.StockStatus = 6
****************************************************************************/
begin
  declare @StoreID int, @CouponTypeID int, @CouponCount int, @FromStoreID int, @ReceiveType int, @CouponStockStatus int
  declare @PickQty int, @StartCouponNum varchar(64), @EndCouponNum varchar(64)

--  if @TypeID = 1    -- 产生Picking单时没有库存变动.
--  begin
--  end
  
  if @TypeID = 2     -- 给供应商订单批核 (@Act 只能是1， 不做判断)
  begin    
    DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR
      select H.StoreID, D.CouponTypeID, count(*) as CouponCount from Ord_OrderToSupplier_D D 
         left join Ord_OrderToSupplier_H H on H.OrderSupplierNumber = D.OrderSupplierNumber 
        where H.OrderSupplierNumber = @OrderNumber
      group by H.StoreID, D.CouponTypeID
    OPEN CUR_ChangeCouponStockStatus
    FETCH FROM CUR_ChangeCouponStockStatus INTO @StoreID, @CouponTypeID, @CouponCount
    WHILE @@FETCH_STATUS=0
    BEGIN 
      if exists(select * from Coupon_Onhand where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 1)
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
           where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 1 
      else
        Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
        values(@CouponTypeID, 1, @StoreID, 0, 1, @CouponCount, GETDATE(), 1)       
      FETCH FROM CUR_ChangeCouponStockStatus INTO @StoreID, @CouponTypeID, @CouponCount
    END
    CLOSE CUR_ChangeCouponStockStatus
    DEALLOCATE CUR_ChangeCouponStockStatus 
    
    update Coupon set StockStatus = 1, LocateStoreID = @StoreID  from Coupon C left join Ord_OrderToSupplier_D D on C.CouponNumber = D.FirstCouponNumber
      where D.OrderSupplierNumber = @OrderNumber    
  end
    
  if @TypeID = 3      -- 店铺给总部的订单
  begin
    if @Act = 1       -- 只批核，未产生picking单, 但是Coupon库存需要预扣.....(因为没有选中Coupon，所以Coupon上的PickingFlag没有变化，只是Coupon_Onhand中有变化)
    begin    
      DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR    
        select H.FromStoreID, H.StoreID, D.CouponTypeID, sum(D.CouponQty) as CouponCount from Ord_CouponOrderForm_D D 
           left join Ord_CouponOrderForm_H H on H.CouponOrderFormNumber = D.CouponOrderFormNumber 
          where H.CouponOrderFormNumber = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CouponTypeID
      OPEN CUR_ChangeCouponStockStatus
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      WHILE @@FETCH_STATUS=0
      BEGIN 
      -- 店铺没有收到货品，数量不变。 如果自动订货时需要去掉这部分数据，则减去Order中的数量。             
      -- 总部减去好货
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
           where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2  
       -- 总部加上4：Picked (總部收到了店舖的訂單)
        if exists(select * from Coupon_Onhand where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID 
                   and CouponStatus = 0 and CouponStockStatus = 4)
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
             where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID 
                     and CouponStatus = 0 and CouponStockStatus = 4
        else       
          Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CouponTypeID, 1, @FromStoreID, 0, 4, @CouponCount, GETDATE(), 1)              
                                  
        FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      END
      CLOSE CUR_ChangeCouponStockStatus
      DEALLOCATE CUR_ChangeCouponStockStatus 
    end
    else if @Act = 2      -- void. 只有已经批核过或者 产生过程Picking单的,在void时需要调用
    begin    
      DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR    
        select H.FromStoreID, H.StoreID, D.CouponTypeID, sum(D.CouponQty) as CouponCount from Ord_CouponOrderForm_D D 
           left join Ord_CouponOrderForm_H H on H.CouponOrderFormNumber = D.CouponOrderFormNumber 
          where H.CouponOrderFormNumber = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CouponTypeID
      OPEN CUR_ChangeCouponStockStatus
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      WHILE @@FETCH_STATUS=0
      BEGIN                  
      -- 总部减去Picking的库存数量
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
           where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 4 

      -- 总部好货调整
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount   
           where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2              
        FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      END
      CLOSE CUR_ChangeCouponStockStatus
      DEALLOCATE CUR_ChangeCouponStockStatus 
    end    
    else if @Act = 3            -- 产生picking单。 （需要从Picking单中获取数据）
    begin  
      DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR    
        select H.FromStoreID, H.StoreID, D.CouponTypeID, sum(isnull(D.PickQty,0)) from Ord_CouponPicking_D D 
           left join Ord_CouponPicking_H H on H.CouponPickingNumber = D.CouponPickingNumber
          where H.ReferenceNo = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CouponTypeID
      OPEN CUR_ChangeCouponStockStatus
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @PickQty
      WHILE @@FETCH_STATUS=0
      BEGIN 
      -- 这一步更改数量，目的是处理 OrderQty 和Picking Qty不同的情况。     
      -- 总部数量调整 （4：Picked (總部收到了店舖的訂單)）
        select @CouponCount = sum(CouponQty) from Ord_CouponOrderForm_D 
          where CouponOrderFormNumber = @OrderNumber and CouponTypeID = @CouponTypeID
          
        if @PickQty <> @CouponCount
        begin
          if exists(select * from Coupon_Onhand where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 4)
            update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + (@PickQty - @CouponCount)  
               where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 4
          else       
            Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
            values(@CouponTypeID, 1, @FromStoreID, 0, 4, @PickQty, GETDATE(), 1)    
            
        -- 总部好货调整
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - (@PickQty - @CouponCount)   
             where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2                              
        end                                  
        FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @PickQty
      END
      CLOSE CUR_ChangeCouponStockStatus
      DEALLOCATE CUR_ChangeCouponStockStatus     
    end
  end
  
  if @TypeID = 4      -- 收货单批核。(@Act 只能是1， 不做判断)（收货单都是总部的。 店铺收货只有批核delivery单）
  begin
    DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR
      select H.SupplierID, H.StoreID, D.CouponTypeID, D.CouponStockStatus, sum(D.OrderQty) as CouponCount, max(ReceiveType)
        from Ord_CouponReceive_D D 
         left join Ord_CouponReceive_H H on H.CouponReceiveNumber = D.CouponReceiveNumber 
        where H.CouponReceiveNumber = @OrderNumber
      group by H.SupplierID, H.StoreID, D.CouponTypeID, D.CouponStockStatus
    OPEN CUR_ChangeCouponStockStatus
    FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponStockStatus, @CouponCount, @ReceiveType
    WHILE @@FETCH_STATUS=0
    BEGIN 
      if @ReceiveType = 1  -- 供应商订单产生的收货单。
      begin
        -- 加上好货
        if @CouponStockStatus = 2
        begin
          if exists(select * from Coupon_Onhand where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2)
            update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
               where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2 
          else
            Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
            values(@CouponTypeID, 1, @StoreID, 0, 2, @CouponCount, GETDATE(), 1)
      
          -- 减去订单的 For Printing 数量. 没有的话是数据错误
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
               where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 1   

          -- 好货做Coupon状态变动
          update Coupon set Status = 0, StockStatus = 2, PickupFlag = 0, LocateStoreID = @StoreID 
            where CouponNumber in (select FirstCouponNumber from Ord_CouponReceive_D 
                                     where CouponReceiveNumber = @OrderNumber and CouponStockStatus = 2 and CouponTypeID = @CouponTypeID)
        end 
        -- 坏货
        if @CouponStockStatus = 3
        begin
          if exists(select * from Coupon_Onhand where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 3)
            update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
               where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 3 
          else
            Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
            values(@CouponTypeID, 1, @StoreID, 0, 3, @CouponCount, GETDATE(), 1)
      
          -- 减去订单的 For Printing 数量. 没有的话是数据错误
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
               where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 1  
               
          -- 好货做Coupon状态变动
          update Coupon set Status = 0, StockStatus = 3, PickupFlag = 0, LocateStoreID = @StoreID 
            where CouponNumber in (select FirstCouponNumber from Ord_CouponReceive_D 
                                     where CouponReceiveNumber = @OrderNumber and CouponStockStatus = 3 and CouponTypeID = @CouponTypeID)               
        end  
 /*       
        -- 未区分,需要另外产生收货单
        if @CouponStockStatus = 0
        begin
          -- 减去订单的 For Printing 数量. 没有的话是数据错误
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
               where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 1                         
        end 
*/                
      end
      else if @ReceiveType = 2  -- 店铺订单退货产生的收货单。
      begin    
        -- 总部加回好货  
        if exists(select * from Coupon_Onhand where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2)
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
            where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2 
        else
          Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CouponTypeID, 1, @StoreID, 0, 2, @CouponCount, GETDATE(), 1)
      
        -- 店铺扣除库存退货货品
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
          where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 7     
        
        -- Coupon状态变动  
        update Coupon set Status = 0, StockStatus = 2, PickupFlag = 0, LocateStoreID = @StoreID 
          where CouponNumber in (select FirstCouponNumber from Ord_CouponReceive_D 
                                   where CouponReceiveNumber = @OrderNumber and CouponStockStatus = 2 and CouponTypeID = @CouponTypeID)              
      end
      
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponStockStatus, @CouponCount, @ReceiveType
    END
    CLOSE CUR_ChangeCouponStockStatus
    DEALLOCATE CUR_ChangeCouponStockStatus 
  end
   
  if @TypeID = 5      -- picking单。(@Act 只能是1， 不做判断)（picking单都是总部向店铺的）
  begin
    if @Act = 1 
    begin
      DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR
        select H.FromStoreID, H.StoreID, D.CouponTypeID, sum(isnull(D.PickQty,0)) from Ord_CouponPicking_D D 
           left join Ord_CouponPicking_H H on H.CouponPickingNumber = D.CouponPickingNumber
          where H.CouponPickingNumber = @OrderNumber
          group by H.FromStoreID, H.StoreID, D.CouponTypeID
      OPEN CUR_ChangeCouponStockStatus
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      WHILE @@FETCH_STATUS=0
      BEGIN 
        -- 总部加上送货库存  
        if exists(select * from Coupon_Onhand where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 5)
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
            where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 5 
        else
          Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CouponTypeID, 1, @FromStoreID, 0, 5, @CouponCount, GETDATE(), 1)
      
        -- 总部扣除Pick库存
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
          where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 1 and CouponStockStatus = 6 --CouponStatus = 0 and CouponStockStatus = 4         
      
        FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      END
      CLOSE CUR_ChangeCouponStockStatus
      DEALLOCATE CUR_ChangeCouponStockStatus
      
    end 
    else if @Act = 2      -- void
    begin
      DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR
        select H.FromStoreID, H.StoreID, D.CouponTypeID, sum(isnull(D.OrderQty,0)), sum(isnull(D.PickQty,0)) from Ord_CouponPicking_D D 
           left join Ord_CouponPicking_H H on H.CouponPickingNumber = D.CouponPickingNumber
          where H.CouponPickingNumber = @OrderNumber
          group by H.FromStoreID, H.StoreID, D.CouponTypeID
      OPEN CUR_ChangeCouponStockStatus
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount, @PickQty
      WHILE @@FETCH_STATUS=0
      BEGIN 
        -- 总部更改PickQty
        if (@PickQty <> @CouponCount) 
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - (@PickQty - @CouponCount)
            where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 4         
      
        FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount, @PickQty
      END
      CLOSE CUR_ChangeCouponStockStatus 
      DEALLOCATE CUR_ChangeCouponStockStatus 
      
     update coupon set PickupFlag = 0, StockStatus = 2, LocateStoreID = @FromStoreID 
        where CouponNumber in (select FirstCouponNumber from Ord_CouponPicking_D where CouponPickingNumber = @OrderNumber)   
    end  
  end  
    
  if @TypeID = 6      -- 退货单。
  begin
    DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR
      select H.FromStoreID, H.StoreID, D.CouponTypeID, sum(D.OrderQty) as CouponCount from Ord_CouponReturn_D D 
         left join Ord_CouponReturn_H H on H.CouponReturnNumber = D.CouponReturnNumber 
        where H.CouponReturnNumber = @OrderNumber
      group by H.FromStoreID, H.StoreID, D.CouponTypeID
    OPEN CUR_ChangeCouponStockStatus
    FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
    WHILE @@FETCH_STATUS=0
    BEGIN 
        -- 店铺加上库存退货货品  
        if exists(select * from Coupon_Onhand where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 7)
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
            where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 7 
        else
          Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CouponTypeID,2, @FromStoreID, 0, 7, @CouponCount, GETDATE(), 1)
      
        -- 店铺扣除库存好货
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
          where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 2
                                      
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
    END
    CLOSE CUR_ChangeCouponStockStatus
    DEALLOCATE CUR_ChangeCouponStockStatus 
    
    -- 更改Coupon库存状态
    update Coupon set StockStatus = 7
          where CouponNumber in (select FirstCouponNumber From Ord_CouponReturn_D where CouponReturnNumber = @OrderNumber)
  end 
   
  if @TypeID = 7      -- 送货单。(送货单批核后表示店铺已经收到）
  begin
    if @Act = 1
    begin
      DECLARE CUR_ChangeCouponStockStatus CURSOR fast_forward local FOR
        select H.FromStoreID, H.StoreID, D.CouponTypeID, sum(D.OrderQty) as CouponCount from Ord_CouponDelivery_D D 
           left join Ord_CouponDelivery_H H on H.CouponDeliveryNumber = D.CouponDeliveryNumber
          where H.CouponDeliveryNumber = @OrderNumber
        group by H.FromStoreID, H.StoreID, D.CouponTypeID
      OPEN CUR_ChangeCouponStockStatus
      FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      WHILE @@FETCH_STATUS=0
      BEGIN 
        -- 店铺加上送货数量  
        if exists(select * from Coupon_Onhand where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 6)
          update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) + @CouponCount  
            where StoreID = @StoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 6
        else
          Insert into Coupon_Onhand (CouponTypeID, StoreTypeID, StoreID, CouponStatus, CouponStockStatus, OnhandQty, UpdatedOn, UpdatedBy)
          values(@CouponTypeID, 2, @StoreID, 0, 6, @CouponCount, GETDATE(), 1)        
      
        -- 总部扣除运货
        update Coupon_Onhand set OnhandQty = isnull(OnhandQty, 0) - @CouponCount  
          where StoreID = @FromStoreID and CouponTypeID = @CouponTypeID and CouponStatus = 0 and CouponStockStatus = 5  
                
        FETCH FROM CUR_ChangeCouponStockStatus INTO @FromStoreID, @StoreID, @CouponTypeID, @CouponCount
      END
      CLOSE CUR_ChangeCouponStockStatus
      DEALLOCATE CUR_ChangeCouponStockStatus
    end   
  end 
   
  return 0
end


GO
