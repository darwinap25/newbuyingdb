USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberPromotions]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberPromotions]
  @MemberID				int,					-- 会员ID
  @CardNumber			varchar(512),			-- 如填写CardNumber，忽略MemberID	
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),            -- 终端号码
  @ServerCode           varchar(512)='',			-- serverid 暂时用不到,预留
  @BrandCode			varchar(512),			-- 品牌编码
  @ConditionStr			nvarchar(1000),			-- 自定义查询条件
  @DeviceType			int=0,					-- NULL或0:全部  1:kiosk  2: mobile  3:PC  
  @PromotionType		int=0,				    -- NULL或0:全部  1:广告促销 2:店铺促销. 3:生日促销  4:VIP促销.
  @IncludeTypeSub       int,                   --是否包括指定PromotionMsgType的子类型。 0：不包含子。 1：包含下一级。   
  @StartDate			datetime,           -- 优惠消息记录创建时间的起始范围，0：取消这个日期条件。
  @EndDate				datetime,           -- 优惠消息记录创建时间的结束范围，0：取消这个日期条件。
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=1 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr			varchar(20)='',	   -- 1:取Msg1,2：取Msg2. 3：取Msg3.  其他：取Msg1
  @IsRead               int=-1             -- -1: 不判断是否读过的。  0：未读的。 1： 已读的。  默认 -1
AS
/****************************************************************************
**  Name : GetMemberPromotions  获得会员的滚动优惠消息
**  Version: 1.0.0.20
**  Description : 根据输入的条件，返回滚动优惠消息。（可能会根据会员的年龄范围，性别，生日，地址，店铺地址，设备类型 ....）
**  Parameter :
  declare @MemberID int, @count int, @recordcount int, @a int
  set @MemberID = 1
  exec @a = GetMemberPromotions null, '', null, null, '', '', '', 0, 105, 0, null,null,1, 30, @count output, @recordcount output, ''  
  --exec @a = GetMemberPromotions null, '', null, null, '', '', 'PromotionMsgCode in (''001'')', 0, 0, 1, null,null,1, 30, @count output, @recordcount output, 'zh_CN'  
  print @a  
  print @count
  print @recordcount
  sp_helptext GetMemberPromotions
  select * from promotionmsg
   select * from promotionmsgtype
滚动优惠消息
1.会员号 2.店铺号 3.设备类型， 4.消息类型。
可能 根据会员年龄,性别，店铺地址，等等 来过滤 优惠消息。
**  Created by: Gavin @2012-03-01
**  Modify by: Gavin @2012-07-20 (ver 1.0.0.1) 增加brandcode输入。 
**  Modify by: Gavin @2013-01-28 (ver 1.0.0.2) 由于PromotionMsg表结构修改,存储过程做相应修改.
**  Modify by: Gavin @2013-02-05 (ver 1.0.0.3) 按照PromotionMsgCode排序。返回字段增加 PromotionMsgCode
**  Modify by: Gavin @2013-03-13 (ver 1.0.0.4) PromotionMsgType表结构修改，改成树状结构，增加输入参数。
**  Modify by: Gavin @2013-03-14 (ver 1.0.0.5) 修正union all SQL问题。
**  Modify by: Gavin @2013-03-15 (ver 1.0.0.6) @PromotionType=0时，如果@IncludeTypeSub=0，则只取第一级的。@IncludeTypeSub=1时取全部
**  Modify by: Gavin @2013-03-20 (ver 1.0.0.7) 增加输入参数@StartDate,@EndDate
**  Modify by: Robin @2013-03-22 (ver 1.0.0.8) 修改SQL语句，把CreatedOn转换为日期再和StartDate及EndDate比较
**  Modify by: Gavin @2013-05-28 (ver 1.0.0.9) 查询结果按照updatedon 倒序排序.
**  Modify by: Gavin @2013-06-19 (ver 1.0.0.10) 使用1.0.0.6版本的SelectDataInBatchs	,输入的@ConditionStr直接传给SelectDataInBatchs 
**  Modify by: Gavin @2013-06-20 (ver 1.0.0.11) 去除memberid 和 cardnumber的校验, 允许不输入会员信息.
**  Modify by: Gavin @2013-07-02 (ver 1.0.0.12) 查询结果按照Createdon 倒序排序
**  Modify by: Gavin @2013-08-07 (ver 1.0.0.13) 增加返回字段PromotionMsgTypeID
**  Modify by: Gavin @2014-07-03 (ver 1.0.0.14) 增加返回字段PromotionPicFile2，PromotionPicFile3
**  Modify by: Gavin @2014-07-11 (ver 1.0.0.15) 增加返回 URL
**  Modify by: Gavin @2014-08-05 (ver 1.0.0.16) 读取CardGrade的NumberOfNewsDisplay设置. 如果输入的@PageSize=0，则返回NumberOfNewsDisplay的设置数
**  Modify by: Gavin @2014-08-28 (ver 1.0.0.17) （for 711）hardcode 的URL 内容更改
**  Modify by: Gavin @2014-12-31 (Ver 1.0.0.18) 统一startdate和enddate的比较规则：如果填写的日期不带时间（时间为0），则和 getdate（）比较时，startdate可以直接判断 < getdate， enddate 需要 >= (getdate() - 1)
**  Modify by: Gavin @2016-07-04 (Ver 1.0.0.19) 为兼容nick的数据库驱动，把 nvarchar(max) 改成 text 输出。 否则不能识别。
**  Modify by: Gavin @2017-04-13 (Ver 1.0.0.20) 增加返回新增的字段 AttachFile1，AttachFile2，AttachFile3
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @MemberSex char(1), @MemberYearOfBirth int, @MemberMonthOfBirth int, 
		  @MemberDayOfBirth int, @CardBrandID int, @StoreGroupID int, @CardGradeID int, @CardTypeID int,
          @StoreBrandID int, @StoreCountry nvarchar(20), @StoreProvince nvarchar(20), @StoreCity nvarchar(30),
          @MemberBirthdayLimit int, @Language int, @StoreID int, @BrandID int
  declare @OrderCondition nvarchar(max), @NumberOfNewsDisplay int
  declare @URL varchar(500), @TempCard varchar(64) 
--  set @URL = 'http://7fansdemo.value-exch.com:81/Test_7Fans/html/coupons/%#.html'    
--  set @URL = 'http://7fansdemo.value-exch.com/SVA_Test/html/News/%#.html'  
  set @URL = 'http://www.7fans.com.hk/SVA/html/News/%#.html' 

  set @NumberOfNewsDisplay = 0
  set @MemberID = ISNULL(@MemberID, 0)    
--  if (isnull(@MemberID, '') = '') and (isnull(@CardNumber, '') = '')  
--    return -7 

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  if @Language = 1
    set @URL = REPLACE(@URL, '#', '_en-us')  
  else if @Language = 2
    set @URL = REPLACE(@URL, '#', '_zh-cns')  
  else if @Language = 3
    set @URL = REPLACE(@URL, '#', '_zh-hk')  
  else if @Language = 3
    set @URL = REPLACE(@URL, '#', '_en-us')  
      
  set @PromotionType = isnull(@PromotionType, 0)
-- 准备查询条件  
  -- 获取会员信息。  
  if (isnull(@CardNumber, '') <> '')
  begin
    select @MemberSex = MemberSex, @MemberYearOfBirth = MemberYearOfBirth, @MemberMonthOfBirth = MemberMonthOfBirth, @MemberDayOfBirth = MemberDayOfBirth,
        @MemberDayOfBirth = MemberDayOfBirth, @CardTypeID = C.CardTypeID, @CardBrandID = T.BrandID, @CardGradeID = C.CardGradeID,
        @MemberID = C.MemberID, @NumberOfNewsDisplay = G.NumberOfNewsDisplay
      from Card C left join Member M on C.MemberID = M.MemberID 
        left join CardType T on C.CardTypeID = T.CardTypeID
        left join CardGrade G on C.CardGradeID = G.CardGradeID 
    where C.CardNumber = @CardNumber
  end else if (isnull(@MemberID, '') <> '')
  begin
    select @MemberSex = MemberSex, @MemberYearOfBirth = MemberYearOfBirth, @MemberMonthOfBirth = MemberMonthOfBirth, @MemberDayOfBirth = MemberDayOfBirth,
        @MemberDayOfBirth = MemberDayOfBirth, @CardTypeID = 0, @CardBrandID = 0, @CardGradeID = 0
      from Member where MemberID = @MemberID
      
    select Top 1 @NumberOfNewsDisplay = G.NumberOfNewsDisplay from Card C
        left join CardGrade G on C.CardGradeID = G.CardGradeID 
      where C.MemberID = @MemberID 
  end
  -- 获取店铺信息
  if isnull(@StoreCode, '') <> '' and isnull(@BrandCode, '') <> ''
  begin
    select @BrandID = StoreBrandID from Brand where StoreBrandCode = @BrandCode
    select @StoreGroupID = StoreGroupID, @StoreCountry = StoreCountry,
          @StoreProvince = StoreProvince, @StoreCity = StoreCity, @StoreID = StoreID  
      from Store where StoreCode = @StoreCode and BrandID = @BrandID
  end     
  set @StoreBrandID = ''
  -- 会员生日限制.    
  if isnull(@MemberDayOfBirth, '') <> ''
  begin
    if (DatePart(dd, getdate()) = DatePart(dd, @MemberDayOfBirth)) and (DatePart(mm, getdate()) = DatePart(mm, @MemberDayOfBirth))
      set @MemberBirthdayLimit = 3
    else if (DatePart(ww, getdate()) = DatePart(ww, @MemberDayOfBirth))
      set @MemberBirthdayLimit = 2
    else if (DatePart(mm, getdate()) = DatePart(mm, @MemberDayOfBirth))  
      set @MemberBirthdayLimit = 1
    else set @MemberBirthdayLimit = 0
  end      
  set @StoreGroupID = isnull(@StoreGroupID, 0)
  set @StoreBrandID = isnull(@StoreBrandID, 0)
  set @StoreCountry = isnull(@StoreCountry, '''''')
  set @StoreProvince = isnull(@StoreProvince, '''''')
  set @StoreCity = isnull(@StoreCity, '''''')
  set @StoreID = isnull(@StoreID, 0)
  set @MemberBirthdayLimit = isnull(@MemberBirthdayLimit, 0)
  set @CardTypeID = isnull(@CardTypeID, 0)
  set @CardBrandID = isnull(@CardBrandID, 0)
  set @CardGradeID = isnull(@CardGradeID, 0)
  set @PromotionType = isnull(@PromotionType, 0)
  set @IncludeTypeSub = isnull(@IncludeTypeSub, 0)
  set @StartDate = isnull(@StartDate, 0)
  set @EndDate = isnull(@EndDate, 0)

  set @SQLStr = N'select KeyID, case when ' + convert(varchar(1),@Language) + '= 2 then (cast(PromotionMsgStr2 as Text)) when '+convert(varchar(1),@Language)+' = 3 then (cast(PromotionMsgStr3 as Text)) else (cast(PromotionMsgStr1 as Text)) end as PromotionDesc '
    + ' , case when ' + convert(varchar(1),@Language) + '= 2 then (PromotionTitle2) when '+convert(varchar(1),@Language)+' = 3 then (PromotionTitle3) else (PromotionTitle1) end as PromotionTitle '
    + ' , PromotionPicFile, PromotionRemark, CreatedOn, StartDate, EndDate, M.PromotionMsgCode, UpdatedOn, PromotionMsgTypeID '
    + ' , PromotionPicFile2, PromotionPicFile3, isnull(R.IsRead, 0) as IsRead, M.Link '
    + ' , replace(''' + @URL + ''', ''%'', M.PromotionMsgCode) as URL, '
	+ ' AttachFile1,AttachFile2,AttachFile3,case when ' + convert(varchar(1),@Language) + '= 2 then (AttachFile2) when '+convert(varchar(1),@Language)+' = 3 then (AttachFile3) else (AttachFile1) end as AttachFile '
    + ' from PromotionMsg M'
    + ' left join (select PromotionMsgCode, IsRead from MemberPromotionMsgRead where MemberID = ' + cast(@MemberID as varchar) + ') R '
    + '     on M.PromotionMsgCode = R.PromotionMsgCode '
    + ' where StartDate <= convert(varchar(10), getdate(), 120) and EndDate >= convert(varchar(10), getdate()-1, 120) and Status = 1 and '
--    + ' (PromotionMsgTypeID = ' + convert(varchar(10), @PromotionType) + ' or isnull(' + convert(varchar(10), @PromotionType) + ', 0) = 0) and '
    + ' (BirthPromotion = '+ convert(varchar(10), @MemberBirthdayLimit) + ' or isnull(BirthPromotion, 0) = 0) and ' 
    + ' (KeyID in (select PromotionMsgID from PromotionCardCondition where CardGradeID = ' + cast(@CardGradeID as varchar) + ') ' + ' or ' + cast(@CardGradeID as varchar) + ' = 0) '
    + ' and (convert(varchar(10), CreatedOn, 120) >= ''' + convert(varchar(10), @StartDate, 120) + ''' or ' + cast(cast(@StartDate as int) as varchar) + ' = 0 )'
	+ ' and (convert(varchar(10), CreatedOn-1, 120) <= ''' + convert(varchar(10), @EndDate, 120) + ''' or ' + cast(cast(@EndDate as int) as varchar) + ' = 0 )'	
	+ ' and (isnull(R.IsRead,0) = '	+ cast(@IsRead as varchar) + ' or ' + cast(@IsRead as varchar) + ' = -1) ' 
print @SQLStr		
  if @PromotionType = 0
  begin
	  if @IncludeTypeSub = 0
	    set @SQLStr = @SQLStr + ' and PromotionMsgTypeID in (select PromotionMsgTypeID from PromotionMsgType where isnull(ParentID, 0) = 0) '
  end else
  begin
    set @SQLStr = @SQLStr + ' and PromotionMsgTypeID = ' + cast(@PromotionType as varchar)
  	if @IncludeTypeSub = 1
  	begin	  	 
      set @SQLStr = @SQLStr + N'  Union all '
        + ' select KeyID, case when ' + convert(varchar(1),@Language) + '= 2 then (cast(PromotionMsgStr2 as Text)) when '+convert(varchar(1),@Language)+' = 3 then (cast(PromotionMsgStr3 as Text)) else (cast(PromotionMsgStr1 as Text)) end as PromotionDesc '
        + ' , case when ' + convert(varchar(1),@Language) + '= 2 then (PromotionTitle2) when '+convert(varchar(1),@Language)+' = 3 then (PromotionTitle3) else (PromotionTitle1) end as PromotionTitle '
        + ' , PromotionPicFile, PromotionRemark, CreatedOn, StartDate, EndDate, M.PromotionMsgCode, UpdatedOn, PromotionMsgTypeID '
        + ' , PromotionPicFile2, PromotionPicFile3, isnull(R.IsRead, 0) as IsRead, M.Link ' 
        + ' , replace(''' + @URL + ''', ''%'', M.PromotionMsgCode) as URL, '    
		+ ' AttachFile1,AttachFile2,AttachFile3,case when ' + convert(varchar(1),@Language) + '= 2 then (AttachFile2) when '+convert(varchar(1),@Language)+' = 3 then (AttachFile3) else (AttachFile1) end as AttachFile '           
        + ' from PromotionMsg M '
        + ' left join (select PromotionMsgCode, IsRead from MemberPromotionMsgRead where MemberID = ' + cast(@MemberID as varchar) + ') R '
        + '     on M.PromotionMsgCode = R.PromotionMsgCode '        
        + ' where StartDate <= convert(varchar(10), getdate(), 120) and EndDate >= convert(varchar(10), getdate()-1, 120) and Status = 1 and '
        + ' (PromotionMsgTypeID in (select PromotionMsgTypeID from PromotionMsgType where ParentID = ' + cast(@PromotionType as varchar) + ')) and '
        + ' (BirthPromotion = '+ convert(varchar(10), @MemberBirthdayLimit) + ' or isnull(BirthPromotion, 0) = 0) and ' 
        + ' (KeyID in (select PromotionMsgID from PromotionCardCondition where CardGradeID = ' + cast(@CardGradeID as varchar) + ') ' + ' or ' + cast(@CardGradeID as varchar) + ' = 0) '	
        + ' and (convert(varchar(10), CreatedOn, 120) >= ''' + convert(varchar(10), @StartDate, 120) + ''' or ' + cast(cast(@StartDate as int) as varchar) + ' = 0 )'
	    + ' and (convert(varchar(10), CreatedOn-1, 120) <= ''' + convert(varchar(10), @EndDate, 120) + ''' or ' + cast(cast(@EndDate as int) as varchar) + ' = 0 )'	
        + ' and (isnull(R.IsRead,0) = '	+ cast(@IsRead as varchar) + ' or ' + cast(@IsRead as varchar) + ' = -1) ' 	
	  end  
  end

--  if isnull(@ConditionStr, '') <> ''
--    set @SQLStr = @SQLStr + ' and (' + @ConditionStr  + ')' 
  set @NumberOfNewsDisplay = ISNULL(@NumberOfNewsDisplay, 0)
  if @PageSize = 0
    set @PageSize = @NumberOfNewsDisplay 
  set @OrderCondition='order by CreatedOn desc'
  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr
  
  return 0
end  

GO
