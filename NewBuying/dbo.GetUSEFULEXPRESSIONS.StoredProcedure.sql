USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetUSEFULEXPRESSIONS]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetUSEFULEXPRESSIONS]
  @LanguageAbbr			varchar(20)=''			   
AS
/****************************************************************************
**  Name : 
**  Version: 1.0.0.0
**  Description : 返回常用短语
**
**  Parameter :
  exec GetMemberFriends 1
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**MemberClause
****************************************************************************/
begin 
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  select USEFULEXPRESSIONSID, 
         case @Language when 2 then PhraseTitle2 when 3 then PhraseTitle3 else PhraseTitle1 end as PhraseTitle,
         case @Language when 2 then PhraseContent2 when 3 then PhraseContent3 else PhraseContent1 end as PhraseContent,
         CampaignID, PhrasePicFile from USEFULEXPRESSIONS
  return 0 
end

GO
