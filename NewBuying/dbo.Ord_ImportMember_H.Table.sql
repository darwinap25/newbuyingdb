USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportMember_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportMember_H](
	[ImportMemberNumber] [varchar](64) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[Note] [nvarchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
 CONSTRAINT [PK_ORD_IMPORTMEMBER_H] PRIMARY KEY CLUSTERED 
(
	[ImportMemberNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_ImportMember_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_ImportMember_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Update_Ord_ImportMember_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_ImportMember_H] ON [dbo].[Ord_ImportMember_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_ImportMember_H 
* Version: 1.0.0.1
* Description : 导入member数据的批核触发器
*   select * from Ord_ImportMember_H
* Create By Gavin @2013-03-20
*/
/*==============================================================*/
BEGIN  
  declare @ImportMemberNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)

  DECLARE CUR_Ord_ImportMember_H CURSOR fast_forward FOR
    SELECT ImportMemberNumber, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_Ord_ImportMember_H
  FETCH FROM CUR_Ord_ImportMember_H INTO @ImportMemberNumber, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where ImportMemberNumber = @ImportMemberNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output       
      update Ord_ImportMember_H set ApprovalCode = @ApprovalCode  where ImportMemberNumber = @ImportMemberNumber
    end
    FETCH FROM CUR_Ord_ImportMember_H INTO @ImportMemberNumber, @ApproveStatus, @CreatedBy
  END
  CLOSE CUR_Ord_ImportMember_H 
  DEALLOCATE CUR_Ord_ImportMember_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H', @level2type=N'COLUMN',@level2name=N'ImportMemberNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入Member数据（主表） （MII）（只是存储记录，只有主表，批核不做操作）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportMember_H'
GO
