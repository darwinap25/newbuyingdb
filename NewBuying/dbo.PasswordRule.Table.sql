USE [NewBuying]
GO
/****** Object:  Table [dbo].[PasswordRule]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PasswordRule](
	[PasswordRuleID] [int] IDENTITY(1,1) NOT NULL,
	[PasswordRuleCode] [varchar](64) NULL,
	[Name1] [nvarchar](512) NULL,
	[Name2] [nvarchar](512) NULL,
	[Name3] [nvarchar](512) NULL,
	[PWDMinLength] [int] NULL DEFAULT ((0)),
	[PWDMaxLength] [int] NULL DEFAULT ((0)),
	[PWDSecurityLevel] [int] NULL DEFAULT ((0)),
	[PWDStructure] [int] NULL DEFAULT ((0)),
	[PWDValidDays] [int] NULL,
	[PWDValidDaysUnit] [int] NULL,
	[ResetPWDDays] [int] NULL,
	[MemberPWDRule] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PASSWORDRULE] PRIMARY KEY CLUSTERED 
(
	[PasswordRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PasswordRuleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PasswordRuleCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'Name3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码最小长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PWDMinLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码最大长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PWDMaxLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码安全级别。0级最低。依次提高' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PWDSecurityLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码构成。
0：没有要求。
1.只能為數字
2.只能為字母
3.必須數字+字母
4.特殊符号+数字+字母
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PWDStructure'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码有效天数。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PWDValidDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码有效天数的单位。0：永久。 1：年。 2：月。 3：星期。 4：天。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'PWDValidDaysUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码到期前，提示用户修改密码的 警告天数。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'ResetPWDDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员的密码的规则。0：不是。1：是。 默认0。
（只能有一条记录是 1。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule', @level2type=N'COLUMN',@level2name=N'MemberPWDRule'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码规则' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PasswordRule'
GO
