USE [NewBuying]
GO
/****** Object:  Table [dbo].[ProductExtraAttributes]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductExtraAttributes](
	[KeyID] [int] NOT NULL,
	[TableName] [varchar](64) NOT NULL,
	[TableDesc1] [varchar](512) NULL,
	[TableDesc2] [varchar](512) NULL,
	[TableDesc3] [varchar](512) NULL,
 CONSTRAINT [PK_PRODUCTEXTRAATTRIBUTES] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductExtraAttributes', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductExtraAttributes', @level2type=N'COLUMN',@level2name=N'TableName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductExtraAttributes', @level2type=N'COLUMN',@level2name=N'TableDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductExtraAttributes', @level2type=N'COLUMN',@level2name=N'TableDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductExtraAttributes', @level2type=N'COLUMN',@level2name=N'TableDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品扩展属性设置表。（货品可以使用哪些属性表）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductExtraAttributes'
GO
