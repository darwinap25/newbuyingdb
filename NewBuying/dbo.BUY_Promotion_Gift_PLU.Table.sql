USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_Promotion_Gift_PLU]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_Promotion_Gift_PLU](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PromotionCode] [varchar](64) NOT NULL,
	[GiftSeq] [int] NOT NULL,
	[EntityNum] [varchar](64) NULL,
	[EntityType] [int] NOT NULL,
 CONSTRAINT [PK_BUY_PROMOTION_GIFT_PLU] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift_PLU', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift_PLU', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promotion_Gift表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift_PLU', @level2type=N'COLUMN',@level2name=N'GiftSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift_PLU', @level2type=N'COLUMN',@level2name=N'EntityNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'决定EntityNum中内容的含义。
0：所有货品。EntityNum中为空
1：EntityNum内容为 prodcode。
2：EntityNum内容为 DepartCode。
4：EntityNum内容为 BUY_Product_Catalog表中的DepartCode。 需要对照BUY_Product_Catalog表来查找货品。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift_PLU', @level2type=N'COLUMN',@level2name=N'EntityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销赠送表的指定货品列表
@2017-03-07 EntityType增加内容： 4：EntityNum内容为 BUY_Product_Catalog表中的DepartCode。 需要对照BUY_Product_Catalog表来查找货品。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift_PLU'
GO
