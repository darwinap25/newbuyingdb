USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenReceiveOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenReceiveOrder]
  @OrderNumber VARCHAR(64)     -- 订单号码
AS
/****************************************************************************
**  Name : GenReceiveOrder
**  Version: 1.0.0.2
**  Description : 送货单批核后产生收货单
**  Parameter :
select * from Ord_ShipmentOrder_H
select * from Ord_ReceiveOrder_H
select * from Ord_ReceiveOrder_D
delete from Ord_ReceiveOrder_H where ReceiveOrderNumber = 'RECIO0000000018'
delete from Ord_ReceiveOrder_D where ReceiveOrderNumber = 'RECIO0000000018'
update Ord_ShipmentOrder_H set approvestatus = 'A' where shipmentordernumber = 'SHP000000000024'
sp_helptext GenReceiveOrder
**  Created by: Gavin @2015-03-24
**  Created by: Gavin @2015-04-14 (ver 1.0.0.1) 只有总部给店铺的发货单的收货
**  Modify by Gavin @2016-01-07 (1.0.0.2) 获取busdate时增加StoreCode条件
**
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @StoreCode varchar(64)

  select @StoreCode = StoreCode FROM Ord_ShipmentOrder_H A left join Buy_STORE B on A.StoreID = B.StoreID
    WHERE ShipmentOrderNumber = @OrderNumber
  EXEC GetRefNoString 'RECIO', @NewNumber OUTPUT
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC
  
    INSERT INTO Ord_ReceiveOrder_H (ReceiveOrderNumber,OrderType,ReceiveType,ReferenceNo,FromStoreID,FromContactName,
        FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
        StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
        ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
    SELECT @NewNumber, 1, 1, ShipmentOrderNumber, FromStoreID, FromContactName,
        FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
        StoreMobile,StoreAddress,Remark,@BusDate,NULL,'','P',
        NULL,ApproveBy,GETDATE(),CreatedBy,GETDATE(),UpdatedBy
    FROM Ord_ShipmentOrder_H 
    WHERE ShipmentOrderNumber = @OrderNumber
    
    INSERT INTO Ord_ReceiveOrder_D (ReceiveOrderNumber, ProdCode, OrderQty, ReceiveQty, StockTypeCode, Remark)
    SELECT @NewNumber, ProdCode, ActualQty, ActualQty, 'G', 'Shipment Order Auto Gen Receive Order'  
    FROM Ord_ShipmentOrder_D D left join Ord_ShipmentOrder_H H on H.ShipmentOrderNumber = D.ShipmentOrderNumber
    WHERE H.ShipmentOrderNumber = @OrderNumber

  RETURN 0   
END

GO
