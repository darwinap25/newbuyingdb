USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetStoreListByCoupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetStoreListByCoupon]
  @MemberID int,                   -- 会员ID
  @CardNumber varchar(64),         -- 会员卡号
  @CouponNumber varchar(64),        -- Coupon号码
  @LanguageAbbr         varchar(20)='' 
AS
/****************************************************************************
**  Name : GetStoreListByCoupon  
**  Version: 1.0.0.0
**  Description : 查询指定Coupon可以使用的Store列表(for YB)
**  Parameter :
  declare @a int
  exec @a = GetStoreListByCoupon null, null, 'CN0101395', ''
  print @a
 
**  Created by: Gavin @2014-11-20
**
****************************************************************************/
begin
  declare @CardTypeID int, @CardGradeID int, @CouponTypeID int, @Language int
  --select top 1 @CardTypeID = CardTypeID, @CardGradeID = CardGradeID from Card where CardNumber = @CardNumber or MemberID = @MemberID
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  select @CouponTypeID = CouponTypeID from Coupon where CouponNumber = @CouponNumber 
  
  select StoreID, case @Language when 2 then StoreName2 when 3 then StoreName3 else StoreName1 end as StoreName, 
    T.CityName, D.DistrictName
  from Store S 
    left join (select CityCode, case @Language when 2 then CityName2 when 3 then CityName3 else CityName1 end as CityName from City) T on S.StoreCity = T.CityCode
    left join (select DistrictCode, case @Language when 2 then DistrictName2 when 3 then DistrictName3 else DistrictName1 end as DistrictName from District) D on S.StoreDistrict = D.DistrictCode
  where S.StoreID in (
        select distinct StoreID from CouponTypeStoreCondition_List where StoreConditionType = 2 and CouponTypeID = @CouponTypeID
      )  
  return 0
end

GO
