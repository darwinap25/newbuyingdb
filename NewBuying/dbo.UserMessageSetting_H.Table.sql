USE [NewBuying]
GO
/****** Object:  Table [dbo].[UserMessageSetting_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMessageSetting_H](
	[UserMessageCode] [varchar](64) NOT NULL,
	[UserMessageDesc] [varchar](512) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL,
	[SendUserID] [int] NULL,
	[UserMessageType] [int] NULL,
	[UserMessageTitle] [varchar](512) NULL,
	[UserMessageContent] [nvarchar](max) NULL,
	[TriggeType] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_USERMESSAGESETTING_H] PRIMARY KEY CLUSTERED 
(
	[UserMessageCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[UserMessageSetting_H] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[UserMessageSetting_H] ADD  DEFAULT (getdate()) FOR [EndDate]
GO
ALTER TABLE [dbo].[UserMessageSetting_H] ADD  DEFAULT ((1)) FOR [TriggeType]
GO
ALTER TABLE [dbo].[UserMessageSetting_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[UserMessageSetting_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'UserMessageCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'UserMessageDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效状态。 0：无效。1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送人员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'SendUserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户消息大类： （选项内容固定）
1、Ord_CardBatchCreate
2、Ord_CardOrderForm_H
3、Ord_CardPicking_H
4、Ord_CardDelivery_H
5、Ord_CardAdjust_H
6、Ord_CardTransfer
7、Ord_CouponBatchCreate
8、Ord_CouponOrderForm_H
9、Ord_OrderToSupplier_Card_H
10、Ord_CouponReceive_H
11、Ord_CouponPicking_H
12、Ord_CouponDelivery_H
13、Ord_CouponReturn_H
14、Ord_CouponPush_H
15、Ord_CouponAdjust_H
16、Ord_ImportCardUID_H
17、Ord_ImportCouponUID_H
18、Ord_ImportCouponDispense_H
19、Ord_ImportMember_H

Teleco card
20、Ord_OrderToSupplier_Card_H
21、Ord_CardOrderForm_H
22、Ord_CardPicking_H
23、Ord_CardDelivery_H
24、Ord_CardAdjust_H
25、Ord_CardTransfer

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'UserMessageType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'UserMessageTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息内容。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'UserMessageContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发条件。0：创建时。  1：批核时。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H', @level2type=N'COLUMN',@level2name=N'TriggeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统用户消息通知设定。头表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_H'
GO
