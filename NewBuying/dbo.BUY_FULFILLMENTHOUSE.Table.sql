USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_FULFILLMENTHOUSE]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_FULFILLMENTHOUSE](
	[FulfillmentHouseID] [int] IDENTITY(1,1) NOT NULL,
	[FulfillmentHouseCode] [varchar](64) NOT NULL,
	[FulfillmentHouseName1] [nvarchar](512) NULL,
	[FulfillmentHouseName2] [nvarchar](512) NULL,
	[FulfillmentHouseName3] [nvarchar](512) NULL,
	[Phone] [nvarchar](512) NULL,
	[Email] [nvarchar](512) NULL,
	[Priority] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_FULFILLMENTHOUSE] PRIMARY KEY CLUSTERED 
(
	[FulfillmentHouseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_FULFILLMENTHOUSE] ADD  DEFAULT ((0)) FOR [Priority]
GO
ALTER TABLE [dbo].[BUY_FULFILLMENTHOUSE] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_FULFILLMENTHOUSE] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'FulfillmentHouseID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'FulfillmentHouseCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'FulfillmentHouseName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'FulfillmentHouseName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'FulfillmentHouseName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'Phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电子邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先标志。 1：优先。 0：普通' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供货仓库表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_FULFILLMENTHOUSE'
GO
