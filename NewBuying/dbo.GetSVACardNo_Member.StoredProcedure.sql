USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSVACardNo_Member]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetSVACardNo_Member]
  @MemberIdentityType	int,				  --会员类型（member.MemberIdentityType）(目前不起作用，只能用会员的手机号码)
  @MemberIdentityRef	nvarchar(20),	      -- 会员唯一账号(手机号)
  @UID					varchar(20),          -- 仅根据UID 即可找到主卡。
  @CardNumber			nvarchar(30),         -- 卡号或加密卡号。 可以不填写，只根据物理卡ID或手机号查询
  @CardTypeCode			nvarchar(20),         -- 卡类型编号
  @OutputUID            varchar(20) Output,   -- 有可能输入的UID是 laseid,所以要转换UID后通过@OutputUID输出
  @OutputCardNumber     nvarchar(30) Output,  -- 返回卡号(明文)
  @OutputCardTypeID     int Output,           -- 返回卡类型ID
  @OutMemberID			int output    -- 返回会员的memberID
AS
/****************************************************************************
**  Name : GetSVACardNo_Member  (未确定Card是否与SOGO一样的逻辑, 将根据情况修改)
**  Version: 1.0.0.1
**  Description : GetSVACardNo的扩展, 增加输入memberrefID的情况。（以UID为主）
**  Created by: Gavin @2012-01-05
**  Modify by: Gavin @2012-02-29  增加MemberIdentityType以扩展MemberIdentityRef 的类型。
**
****************************************************************************/
begin  
  set @OutMemberID = '';
  -- 查询CardNo ---------------------------------------------
  set @OutputUID = @UID
  declare @LaseID varchar(20), @TUID varchar(20), @OutputCardNo nvarchar(30)
  declare @GetSVACardNoResult int
  
  if isnull(@UID, '') <> ''
  begin
    exec UIDConvertToID @UID, @LaseID output  
    --卡号解密---    
    exec @GetSVACardNoResult = GetSVACardNo @UID, @CardNumber, @CardTypeCode, @OutputUID output, @OutputCardNo output, @OutputCardTypeID output
    if @GetSVACardNoResult <> 0
      return -99
    select @OutMemberID = MemberID from Card where CardNumber = @OutputCardNo and CardTypeID = @OutputCardTypeID
  end else if isnull(@MemberIdentityRef, '') <> ''
  begin
    select Top 1 @OutputCardNo = C.CardNumber, @OutputCardTypeID = C.CardTypeID, @OutMemberID = C.MemberID 
       from card C left join CardType T on C.CardTypeID = T.CardTypeID
      where C.MemberID = (select top 1 MemberID from member where MemberRegisterMobile = @MemberIdentityRef)
    if @OutputCardNo is null
      return -99    
  end else if isnull(@CardNumber, '') <> '' and isnull(@CardTypeCode, '') <> ''
  begin
    select @OutputCardTypeID = CardTypeID from CardType where CardTypeCode = @CardTypeCode
    select @OutputCardNo = CardNumber, @OutMemberID = MemberID from card where CardNumber = @CardNumber and CardTypeID = @OutputCardTypeID
    if @OutputCardNo is null
      return -99        
  end else
    return -1
  --------------------------------
  return 0
end  

GO
