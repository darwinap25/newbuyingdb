USE [NewBuying]
GO
/****** Object:  Table [dbo].[TelcoCardTopUpRule_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TelcoCardTopUpRule_H](
	[CardTopUpCode] [varchar](64) NOT NULL,
	[Description] [varchar](512) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL,
	[BrandID] [int] NULL,
	[CardTypeID] [int] NOT NULL,
	[CardGradeID] [int] NOT NULL,
	[StoreTypeID] [int] NOT NULL,
	[AutoCreateOrder] [int] NULL,
	[AutoApproveOrder] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[DayFlagID] [int] NULL,
	[MonthFlagID] [int] NULL,
	[WeekFlagID] [int] NULL,
	[ActiveTime] [datetime] NULL,
 CONSTRAINT [PK_TELCOCARDTOPUPRULE_H] PRIMARY KEY CLUSTERED 
(
	[CardTopUpCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TelcoCardTopUpRule_H] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[TelcoCardTopUpRule_H] ADD  DEFAULT ((1)) FOR [AutoCreateOrder]
GO
ALTER TABLE [dbo].[TelcoCardTopUpRule_H] ADD  DEFAULT ((0)) FOR [AutoApproveOrder]
GO
ALTER TABLE [dbo].[TelcoCardTopUpRule_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[TelcoCardTopUpRule_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，规则代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'CardTopUpCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否生效。 0：无效，1：生效。 默认生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card的品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据brandid选择 CardType ID （MediaType=2有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据brandid选择 CardGradeID （MediaType=2有效）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型ID。1：总部。 2：店铺
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自动创建订单' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'AutoCreateOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自动创建的订单，是否直接批核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'AutoApproveOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效天的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'DayFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效月的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'MonthFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效周的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'WeekFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H', @level2type=N'COLUMN',@level2name=N'ActiveTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电信卡自动充值设置表。 （for RRG）（指定一种cardtype作为电信卡）
特别设置： 用于设置总部和每个店铺的 电信充值卡的金额。  根据设置，总部的卡金额 自动 补充到店铺的卡中（相同CardGrade）。
@2015-02-25 此表作废，功能并入InventoryReplenishRule_H表。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TelcoCardTopUpRule_H'
GO
