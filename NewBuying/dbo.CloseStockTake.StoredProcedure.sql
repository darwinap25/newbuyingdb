USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CloseStockTake]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CloseStockTake]
  @StockTakeNumber  VARCHAR(64),
  @UserID           int
AS
/****************************************************************************
**  Name : CloseStockTake
**  Version: 1.0.0.1
**  Description : 根据盘点差异表来产生库存调整单
**  Parameter :
select * from STK_STAKEVAR
select * from STK_STAKE02
select * from STK_STAKE_H
declare @a int
exec @a=  CloseStockTake 'SRKNUM0000000122', 1
print @a
select * from ord_stockadjust_h
**  Created by: Gavin @2015-06-25
**  Modify by: Gavin @2016-04-05
**
****************************************************************************/
BEGIN
  DECLARE @A INT, @Status INT

  SELECT @Status = Status FROM STK_STAKE_H WHERE StockTakeNumber = @StockTakeNumber
  
  IF ISNULL(@Status,5) in (5,6)
  BEGIN
    RETURN -1
  END ELSE
  BEGIN
      -- 关闭时自动产生调整单
	  EXEC @A = GenStockADJOrderBySTKVAR @StockTakeNumber
	  IF @A = 0 
	  BEGIN
		DELETE FROM STK_STAKEBOOK WHERE StockTakeNumber = @StockTakeNumber
		DELETE FROM STK_STAKE01 WHERE StockTakeNumber = @StockTakeNumber
		DELETE FROM STK_STAKE02 WHERE StockTakeNumber = @StockTakeNumber
		DELETE FROM STK_STAKEVAR WHERE StockTakeNumber = @StockTakeNumber
		UPDATE STK_STAKE_H SET STATUS = 5, UpdatedOn=Getdate(),  UpdatedBy = @UserID WHERE StockTakeNumber = @StockTakeNumber
		RETURN 0
	  END ELSE
	    RETURN -2
  END
END

GO
