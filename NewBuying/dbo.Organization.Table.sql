USE [NewBuying]
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Organization](
	[OrganizationID] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationCode] [varchar](64) NULL,
	[OrganizationName1] [nvarchar](512) NULL,
	[OrganizationName2] [nvarchar](512) NULL,
	[OrganizationName3] [nvarchar](512) NULL,
	[OrganizationDesc1] [nvarchar](max) NULL,
	[OrganizationDesc2] [nvarchar](max) NULL,
	[OrganizationDesc3] [nvarchar](max) NULL,
	[OrganizationNote] [nvarchar](max) NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[CumulativePoints] [int] NULL,
	[CumulativeAmt] [money] NULL,
	[OrganizationType] [int] NULL,
	[OrganizationPicFile] [nvarchar](512) NULL,
	[CallInterface] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORGANIZATION] PRIMARY KEY CLUSTERED 
(
	[OrganizationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Organization] ADD  DEFAULT ((0)) FOR [CallInterface]
GO
ALTER TABLE [dbo].[Organization] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Organization] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联的CardNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累计获得的积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'CumulativePoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累计获得的金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'CumulativeAmt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构类型：' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'OrganizationPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否调用接口。0：不调用。1：调用。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization', @level2type=N'COLUMN',@level2name=N'CallInterface'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构表donate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Organization'
GO
