USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ImportCouponDispense]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[ImportCouponDispense]
  @CouponDispenseNumber varchar(64),   -- 导入单的单号
  @NeedActive int,                   -- 是否需要立即激活（0：不需要，1：需要）
  @NeedNewBatch int,                 -- 是否需要建立新批次。（0） 
  @CreatedBy int
AS
/****************************************************************************
**  Name : ImportCouponDispense
**  Version: 1.0.0.5
**  Description : ImportCouponDispense 根据导入的列表, 给列表中指定的会员,派送coupon，coupon激活.
  （注意: 因为导入数据只有会员手机号和coupontypecode，所以，必须保证一个brand下只有一个cardtype，同时会员在一个cardtype下只有一张卡（不允许同时存在一个cardtype下不同cardgrade的卡））
    exec ImportCouponDispense 'BICOU0000000037', 1, 1, 1
** 
**  Created by:  Gavin @2012-09-04
**  Modify by:  Gavin @2012-09-13 (ver 1.0.0.1) 去除触发器，由UI直接调用此过程，返回分配的coupon数据集。（此版本暂订）
**  Modify by:  Gavin @2015-12-01 (ver 1.0.0.2) 只更新Ord_ImportCouponDispense_H的approvecode， 其他都由SVAWeb负责更新
**  Modify by:  Gavin @2015-12-14 (ver 1.0.0.3) 实现@NeedNewBatch的控制，可以使用已经存在的Coupon
**  Modify by:  Gavin @2016-01-15 (ver 1.0.0.4) 使用已经存在的Coupon时, 不同Coupon可能是不同的CouponExpirydate, 所以不能使用Top 1 的
**  Modify by:  Gavin @2016-04-18 (ver 1.0.0.5) bug fix: 
************************/
begin
  declare @CouponTypeCode varchar(64), @CouponCount bigint, @A int, @CouponTypeID int, @BrandID int, @CardTypeID int 
  declare @IssuedDate datetime, @CouponTypeAmount money, @CouponTypePoint int, @CouponExpiryDate date
  declare @ApprovalCode char(6),  @ReturnBatchID varchar(30), @ReturnStartNumber nvarchar(30), @ReturnEndNumber nvarchar(30),
		  @BatchCouponID int, @Busdate datetime, @TxnDate datetime, @NewCouponStatus int, @NewCouponExpiryDate datetime
  set @NeedNewBatch = isnull(@NeedNewBatch, 0)

  create table #TempTable(
    MemberID int, CouponNumber varchar(64),CouponUID varchar(512), CouponTypeCode varchar(64), CouponTypeName1 varchar(512), CouponTypeName2 varchar(512), CouponTypeName3 varchar(512), HomeAddress nvarchar(512), EmailAccount nvarchar(512), MobileAccount nvarchar(512)
    , MemberEngFamilyName nvarchar(512), MemberEngGivenName nvarchar(512), MemberChiFamilyName nvarchar(512), MemberChiGivenName nvarchar(512)
  )
  exec GenApprovalCode @ApprovalCode output 
  set @IssuedDate = Convert(date, GETDATE(), 120)
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate, @IssuedDate)  
  set @TxnDate = Getdate()
  set @CouponExpiryDate = getdate()
  
	if exists(select * from Ord_ImportCouponDispense_H where CouponDispenseNumber = @CouponDispenseNumber and ApproveStatus = 'P')
	begin
	  DECLARE CUR_Ord_ImportCouponDispense_D_Group CURSOR fast_forward local FOR
		SELECT CouponTypeCode, count(KeyID) as CouponCount FROM Ord_ImportCouponDispense_D where CouponDispenseNumber = @CouponDispenseNumber
		   Group By CouponTypeCode
		 order by CouponTypeCode
	  OPEN CUR_Ord_ImportCouponDispense_D_Group
	  FETCH FROM CUR_Ord_ImportCouponDispense_D_Group INTO @CouponTypeCode, @CouponCount
	  WHILE @@FETCH_STATUS=0
	  BEGIN
		-- 获取相关数据
		select @BrandID = BrandID , @CouponTypeID = CouponTypeID, @CouponTypeAmount = CouponTypeAmount, 
		    @CouponTypePoint = CouponTypePoint
		  from CouponType where CouponTypeCode = @CouponTypeCode 
		--select top 1 @CardTypeID = CardTypeID from CardType where BrandID = @BrandID   -- 只能有一条记录，有多条的话，表示数据不符合使用的条件。
		
		if @NeedNewBatch = 1
		begin
			-- 批量创建coupon
			exec @A = BatchGenerateNumber @CreatedBy, 1, @CouponTypeID, 0,0, @CouponCount, @IssuedDate, @CouponTypeAmount, @CouponTypePoint, 0, '', 
				 0, @CouponDispenseNumber, @ApprovalCode, @CreatedBy, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, ''
			if @A = 0
			  set @BatchCouponID = @ReturnBatchID      
                    
			-- 绑定Coupon到 member card
			select top 1 @CouponExpiryDate = CouponExpiryDate from coupon where BatchCouponID = @BatchCouponID
			  exec CalcCouponNewStatus 0, @CouponTypeID, 32, 0, @NewCouponStatus output
			  exec CalcCouponNewExpiryDate @CouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
	
			insert into #TempTable
			select D.MemberID, C.CouponNumber, P.CouponUID, U.CouponTypeCode, C.CouponTypeName1,C.CouponTypeName2,C.CouponTypeName3, M.HomeAddress, A.AccountNumber as EmailAccount, B.AccountNumber as MobileAccount
				   ,M.MemberEngFamilyName, M.MemberEngGivenName, M.MemberChiFamilyName, M.MemberChiGivenName
				from (SELECT ROW_NUMBER() over (order by KeyID) as SeqNo, KeyID, CouponTypeCode, CardNumber
						from Ord_ImportCouponDispense_D where CouponDispenseNumber = @CouponDispenseNumber and CouponTypeCode = @CouponTypeCode) U 
					left join (select ROW_NUMBER() over (order by CouponNumber) as SeqNo, BatchCouponID, X.CouponTypeID, CouponNumber, 
								  CouponExpiryDate, X.status, T.CouponTypeName1, T.CouponTypeName2, T.CouponTypeName3
								 from coupon X left join CouponType T on X.CouponTypeID = T.CouponTypeID
								 where X.CouponTypeID = @CouponTypeID and BatchCouponID = @BatchCouponID and X.Status = 0) C                          
						 on U.SeqNo = C.SeqNo
					left join Card D on U.CardNumber = D.CardNumber 
					left join CouponUIDMap P on C.CouponNumber = P.CouponNUmber     
					left join Member M on D.MemberID = M.MemberID
					left join (select * from MemberMessageAccount where MessageServiceTypeID = 1) A on D.MemberID = A.MemberID
					left join (select * from MemberMessageAccount where MessageServiceTypeID = 2) B on D.MemberID = B.MemberID   --  ver 1.0.0.5         
			   where U.KeyID is not null and C.CouponNumber is not null
       	
			insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
				 BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode, 
				 OrgExpiryDate, OrgStatus, NewStatus)    
			select 32, U.CardNumber, C.CouponNumber, C.CouponTypeID, null, null, @CouponDispenseNumber, @CouponTypeAmount, 0, @CouponTypeAmount,
				 @BusDate, @Txndate, '', '', @CreatedBy, @NewCouponExpiryDate, @ApprovalCode, 0, '', '', C.CouponExpiryDate,
				 C.Status, @NewCouponStatus
				from (SELECT ROW_NUMBER() over (order by KeyID) as SeqNo, KeyID, CouponTypeCode, CardNumber
						from Ord_ImportCouponDispense_D where CouponDispenseNumber = @CouponDispenseNumber and CouponTypeCode = @CouponTypeCode) U 
					left join (select ROW_NUMBER() over (order by CouponNumber) as SeqNo, BatchCouponID, CouponTypeID, CouponNumber, CouponExpiryDate, status
								 from coupon where CouponTypeID = @CouponTypeID and BatchCouponID = @BatchCouponID and Status = 0) C 
					on U.SeqNo = C.SeqNo
			   where U.KeyID is not null and C.CouponNumber is not null                  
        end else
		begin
			-- 绑定Coupon到 member card
			--select top 1 @CouponExpiryDate = CouponExpiryDate from coupon where CouponTypeID = @CouponTypeID
			set @CouponExpiryDate = cast(0 as datetime)
			  
			exec CalcCouponNewStatus 0, @CouponTypeID, 32, 0, @NewCouponStatus output
			exec CalcCouponNewExpiryDate @CouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
	
			insert into #TempTable
			select D.MemberID, C.CouponNumber, P.CouponUID, U.CouponTypeCode, C.CouponTypeName1,C.CouponTypeName2,C.CouponTypeName3, M.HomeAddress, A.AccountNumber as EmailAccount, B.AccountNumber as MobileAccount
				    ,M.MemberEngFamilyName, M.MemberEngGivenName, M.MemberChiFamilyName, M.MemberChiGivenName
				from (SELECT ROW_NUMBER() over (order by KeyID) as SeqNo, KeyID, CouponTypeCode, CardNumber
						from Ord_ImportCouponDispense_D where CouponDispenseNumber = @CouponDispenseNumber and CouponTypeCode = @CouponTypeCode) U 
					left join (select ROW_NUMBER() over (order by CouponNumber) as SeqNo, BatchCouponID, X.CouponTypeID, CouponNumber, 
								  CouponExpiryDate, X.status, T.CouponTypeName1, T.CouponTypeName2, T.CouponTypeName3
								 from coupon X left join CouponType T on X.CouponTypeID = T.CouponTypeID
								 where X.CouponTypeID = @CouponTypeID and X.Status = 1) C                          
						 on U.SeqNo = C.SeqNo
					left join Card D on U.CardNumber = D.CardNumber 
					left join CouponUIDMap P on C.CouponNumber = P.CouponNUmber     
					left join Member M on D.MemberID = M.MemberID
					left join (select * from MemberMessageAccount where MessageServiceTypeID = 1) A on D.MemberID = A.MemberID
					left join (select * from MemberMessageAccount where MessageServiceTypeID = 2) B on D.MemberID = B.MemberID   --  ver 1.0.0.5                 
			   where U.KeyID is not null and C.CouponNumber is not null
       	
			insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
				 BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode, 
				 OrgExpiryDate, OrgStatus, NewStatus)    
			select 32, U.CardNumber, C.CouponNumber, C.CouponTypeID, null, null, @CouponDispenseNumber, @CouponTypeAmount, 0, @CouponTypeAmount,
				 @BusDate, @Txndate, '', '', @CreatedBy, case when @NewCouponExpiryDate = cast(0 as datetime) then C.CouponExpiryDate else @NewCouponExpiryDate end, 
				 @ApprovalCode, 0, '', '', C.CouponExpiryDate, C.Status, @NewCouponStatus
				from (SELECT ROW_NUMBER() over (order by KeyID) as SeqNo, KeyID, CouponTypeCode, CardNumber
						from Ord_ImportCouponDispense_D where CouponDispenseNumber = @CouponDispenseNumber and CouponTypeCode = @CouponTypeCode) U 
					left join (select ROW_NUMBER() over (order by CouponNumber) as SeqNo, BatchCouponID, CouponTypeID, CouponNumber, CouponExpiryDate, status
								 from coupon where CouponTypeID = @CouponTypeID and Status = 1) C 
					on U.SeqNo = C.SeqNo
			   where U.KeyID is not null and C.CouponNumber is not null      
		end
		
		FETCH FROM CUR_Ord_ImportCouponDispense_D_Group INTO @CouponTypeCode, @CouponCount
	  END
	  CLOSE CUR_Ord_ImportCouponDispense_D_Group
	  DEALLOCATE CUR_Ord_ImportCouponDispense_D_Group
  
	  -- 只更新Ord_ImportCouponDispense_H的approvecode， 其他都由SVAWeb负责更新
	  update Ord_ImportCouponDispense_H set ApprovalCode = @ApprovalCode
		where CouponDispenseNumber = @CouponDispenseNumber            
	end

	-- 如果使用CouponPool，需要清空这个表
	truncate table CouponPool 
     
    select * from #TempTable
    return 0
end

GO
