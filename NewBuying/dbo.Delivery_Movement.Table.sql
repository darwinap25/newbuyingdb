USE [NewBuying]
GO
/****** Object:  Table [dbo].[Delivery_Movement]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Delivery_Movement](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[DeliveryNumber] [varchar](64) NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[LogisticsProviderID] [int] NULL,
	[LogisticsProviderName] [nvarchar](512) NULL,
	[Status] [int] NULL,
	[Seller] [nvarchar](512) NULL,
	[ShipAddress] [nvarchar](512) NULL,
	[Shipper] [nvarchar](512) NULL,
	[ShipperContact] [nvarchar](512) NULL,
	[DeliveryAddress] [nvarchar](512) NULL,
	[Consignee] [nvarchar](512) NULL,
	[ConsigneeContact] [nvarchar](512) NULL,
	[Content] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_DELIVERY_MOVEMENT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货单号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'DeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留参考编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快递公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'LogisticsProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快递公司名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'LogisticsProviderName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卖家名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'Seller'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'ShipAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'Shipper'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货人联系方式（电话号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'ShipperContact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'DeliveryAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'Consignee'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货人联系方式（电话号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'ConsigneeContact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'Content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快递记录。@2016-05-27 for Bauhaus
数据来自快递公司， 记录当前货品物流状态。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Delivery_Movement'
GO
