USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetPINList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetPINList] 
  @PINID                int,               -- 主键 
  @PINCode              varchar(64),       -- 编码
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''	  
AS
/****************************************************************************
**  Name : GetPINList  
**  Version: 1.0.0.0
**  Description : 获得PIN主档.(Disney使用)
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = GetPINList 0, '', 1, 30, @count output, @recordcount output, 'zh_CN'
  print @a  
  print @count
  print @recordcount
**  Created by: Gavin @2013-02-20
**
****************************************************************************/
begin  
  declare @SQLStr nvarchar(4000), @Language int --, @ConditionStr nvarchar(400)

  set @PINID = isnull(@PINID, 0)
  set @PINCode = isnull(@PINCode, '')

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
	
  set @SQLStr = ' select case ' + cast(@Language as varchar) + ' when 2 then PINDesc2 when 3 then PINDesc3 else PINDesc1 end as PINDesc, '
      + ' PINID, PINCode, PINDesc1, PINDesc2, PINDesc3 '
      + ' from DisneyPIN where (PINID = ' + cast(@PINID as varchar) + ' or ' + cast(@PINID as varchar) + ' = 0) '
      + ' and (PINCode = ''' + @PINCode + ''' or ''' + @PINCode + ''' = '''') '
     
  exec SelectDataInBatchs @SQLStr, 'PINID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', ''
  return 0
end

GO
