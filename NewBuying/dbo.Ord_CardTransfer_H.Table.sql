USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardTransfer_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardTransfer_H](
	[CardTransferNumber] [varchar](64) NOT NULL,
	[PurchaseType] [int] NULL,
	[OrderType] [int] NULL,
	[Description] [varchar](512) NULL,
	[ReasonID] [int] NULL,
	[Remark] [varchar](512) NULL,
	[CopyCardFlag] [int] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_ORD_CARDTRANSFER_H] PRIMARY KEY CLUSTERED 
(
	[CardTransferNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CardTransfer_H] ADD  DEFAULT ((1)) FOR [PurchaseType]
GO
ALTER TABLE [dbo].[Ord_CardTransfer_H] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[Ord_CardTransfer_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CardTransfer_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CardTransfer_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CardTransfer_H] ON [dbo].[Ord_CardTransfer_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Ord_CardTransfer_H
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2015-09-25
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @PurchaseType int, @ordernumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @PurchaseType = PurchaseType, @ordernumber = CardTransferNumber FROM INSERTED  
  if ISNULL(@PurchaseType, 1) = 1
    exec GenUserMessage @CreatedBy, 6, 0, @ordernumber
  else exec GenUserMessage @CreatedBy, 25, 0, @ordernumber  
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CardTransfer_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CardTransfer_H] ON [dbo].[Ord_CardTransfer_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CardTransfer_H
* Version: 1.0.0.1
* Description : 转赠事件 (从原来的Update_Ord_CardTransfer 改编)
*  
select * from ord_cardtransfer_D 
* Create By Gavin @2014-11-21
* Modify By Gavin @2015-07-20 (ver 1.0.0.1) 增加卡号校验。如果没有填卡号则返回错误。 
*/
/*==============================================================*/
BEGIN
  declare @CardTransferNumber varchar(512), @SourceCardNumber varchar(512), @DestCardNumber varchar(512), 
          @RefTxnNo varchar(512), @TxnDate datetime, @StoreCode varchar(512), @ServerCode varchar(512), 
          @RegisterCode varchar(512), @ReasonID int, @ActAmount money, @ActPoints int, @OprID int, @Note nvarchar(512),
          @ActCouponNumbers varchar(max), @ApproveStatus char(1), @ApproveBy varchar(512),
          @SourceCardAmount money, @SourceCardPoints int, @TargetCardAmount money, @TargetCardPoints int
  declare @OldApproveStatus char(1), @BusDate datetime
  declare @OrderType int, @CopyCardFlag int
  declare @ApprovalCode char(6), @a int  
  
  DECLARE CUR_Ord_CardTransfer_H CURSOR fast_forward local FOR
    SELECT CardTransferNumber, ApproveStatus, OrderType, CopyCardFlag FROM INSERTED
  OPEN CUR_Ord_CardTransfer_H
  FETCH FROM CUR_Ord_CardTransfer_H INTO @CardTransferNumber, @ApproveStatus, @OrderType, @CopyCardFlag
  WHILE @@FETCH_STATUS=0
  BEGIN
    set @BusDate = @TxnDate
    select @OldApproveStatus = ApproveStatus from Deleted where CardTransferNumber = @CardTransferNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A'    
    begin
      IF EXISTS(SELECT KeyID FROM Ord_CardTransfer_D where CardTransferNumber = @CardTransferNumber
                     AND (ISNULL(SourceCardNumber,'') = '' OR ISNULL(DestCardNumber,'') = '') )
      BEGIN
        RAISERROR ('Order missing card number.', 16, 1)
        if (@@Trancount > 0)
          ROLLBACK TRANSACTION        
        return                       
      END ELSE
      BEGIN
        DECLARE CUR_Ord_CardTransfer_D CURSOR fast_forward local FOR
          SELECT  SourceCardNumber, DestCardNumber, OriginalTxnNo, TxnDate, StoreCode, ServerCode, RegisterCode, ReasonID, ActAmount, 
              ActPoints, ActCouponNumbers, Note 
            FROM Ord_CardTransfer_D where CardTransferNumber = @CardTransferNumber 
        OPEN CUR_Ord_CardTransfer_D
        FETCH FROM CUR_Ord_CardTransfer_D INTO @SourceCardNumber, @DestCardNumber, @RefTxnNo, @TxnDate, @StoreCode, 
          @ServerCode, @RegisterCode, @ReasonID, @ActAmount, @ActPoints, @ActCouponNumbers, @Note
        WHILE @@FETCH_STATUS=0
        BEGIN    
          if @CopyCardFlag = 1
          begin 
            set @OprID = 16
            exec MemberCardTransfer @ApproveBy, @SourceCardNumber, @DestCardNumber, @StoreCode, 
                 @RegisterCode, @ServerCode, '', @CardTransferNumber, @CardTransferNumber, @BusDate, @TxnDate, @OprID, @ActAmount, @ActPoints, @ActCouponNumbers, '',
                 null, @Note,  @SourceCardAmount output, @SourceCardPoints output, @TargetCardAmount output, @TargetCardPoints output      
          end else
          begin
            if isnull(@ActAmount,0) > 0 or isnull(@ActPoints, 0) > 0
            begin
              set @OprID = 6
              exec @a = MemberCardTransfer @ApproveBy, @SourceCardNumber, @DestCardNumber, @StoreCode, 
                 @RegisterCode, @ServerCode, '', @CardTransferNumber, @CardTransferNumber, @BusDate, @TxnDate, @OprID, @ActAmount, @ActPoints, @ActCouponNumbers, '',
                 null, @Note,  @SourceCardAmount output, @SourceCardPoints output, @TargetCardAmount output, @TargetCardPoints output      
            end
            if isnull(@ActCouponNumbers,'') <> ''
            begin
              set @OprID = 36
              exec MemberCardTransfer @ApproveBy, @SourceCardNumber, @DestCardNumber, @StoreCode, 
                 @RegisterCode, @ServerCode, '', @CardTransferNumber, @CardTransferNumber, @BusDate, @TxnDate, @OprID, @ActAmount, @ActPoints, @ActCouponNumbers, '',
                 null, @Note,  @SourceCardAmount output, @SourceCardPoints output, @TargetCardAmount output, @TargetCardPoints output      
            end            
          end
        
          FETCH FROM CUR_Ord_CardTransfer_D INTO @SourceCardNumber, @DestCardNumber, @RefTxnNo, @TxnDate, @StoreCode, 
            @ServerCode, @RegisterCode, @ReasonID, @ActAmount, @ActPoints, @ActCouponNumbers, @Note
        END  
        CLOSE CUR_Ord_CardTransfer_D 
        DEALLOCATE CUR_Ord_CardTransfer_D         
        exec GenApprovalCode @ApprovalCode output            
        update Ord_CardTransfer_H set ApprovalCode = @ApprovalCode where CardTransferNumber = @CardTransferNumber  
      END           
    end  

    FETCH FROM CUR_Ord_CardTransfer_H INTO @CardTransferNumber, @ApproveStatus, @OrderType, @CopyCardFlag
  END  
  CLOSE CUR_Ord_CardTransfer_H 
  DEALLOCATE CUR_Ord_CardTransfer_H 
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'CardTransferNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货种类。1：实体卡订货。 2： 指定卡购买金额或者积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'PurchaseType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 默认0.  0：普通。 1：Telco
1： Telco 卡的操作。  （店铺向总部请求， 批核后，扣除总部卡金额，充值入店铺的卡）
2： Telco 卡的return操作。（1 操作的 return）
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原因ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'ReasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否复制旧卡到新卡。
null/0：
1：只是从一个卡中转移指定值到另外一个卡。 
2：旧卡换新卡。（所有数据复制到新卡），旧卡注销。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'CopyCardFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转赠单据表. 主表
@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D， 加上Ord_CardTransfer_H表。组成主从表。
注：转移的表结构上不加限制， 由填写者自行定义业务逻辑。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_H'
GO
