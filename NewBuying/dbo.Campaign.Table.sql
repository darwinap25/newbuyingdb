USE [NewBuying]
GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Campaign](
	[CampaignID] [int] IDENTITY(1,1) NOT NULL,
	[CampaignCode] [varchar](64) NOT NULL,
	[CampaignName1] [nvarchar](512) NULL,
	[CampaignName2] [nvarchar](512) NULL,
	[CampaignName3] [nvarchar](512) NULL,
	[CampaignDetail1] [nvarchar](max) NULL,
	[CampaignDetail3] [nvarchar](max) NULL,
	[CampaignDetail2] [nvarchar](max) NULL,
	[CampaignType] [int] NOT NULL,
	[CampaignPicFile] [nvarchar](512) NULL,
	[Status] [int] NULL,
	[BrandID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CAMPAIGN] PRIMARY KEY CLUSTERED 
(
	[CampaignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Campaign] ADD  DEFAULT ((1)) FOR [CampaignType]
GO
ALTER TABLE [dbo].[Campaign] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Campaign] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动表code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动详细描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignDetail1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动详细描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignDetail3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动详细描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignDetail2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动类型。1：coupon。 2：promotion   默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动图标文件名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'CampaignPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1、創建。2、已發布。3、過期。4、作廢' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Campaign'
GO
