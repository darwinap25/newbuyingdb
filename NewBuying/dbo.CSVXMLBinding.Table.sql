USE [NewBuying]
GO
/****** Object:  Table [dbo].[CSVXMLBinding]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CSVXMLBinding](
	[BindingCode] [varchar](64) NOT NULL,
	[Description] [varchar](512) NULL,
	[Func] [varchar](64) NULL,
	[CSVColumnName] [varchar](64) NULL,
	[XMLFieldName] [varchar](64) NULL,
	[DataType] [int] NULL,
	[FixedValue] [varchar](512) NULL,
	[Length] [int] NULL,
	[SaveToDB] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CSVXMLBINDING] PRIMARY KEY CLUSTERED 
(
	[BindingCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CSVXMLBinding] ADD  DEFAULT ((1)) FOR [DataType]
GO
ALTER TABLE [dbo].[CSVXMLBinding] ADD  DEFAULT ((0)) FOR [Length]
GO
ALTER TABLE [dbo].[CSVXMLBinding] ADD  DEFAULT ((0)) FOR [SaveToDB]
GO
ALTER TABLE [dbo].[CSVXMLBinding] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CSVXMLBinding] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'BindingCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'功能' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'Func'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CSV文件的列名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'CSVColumnName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'XML文件中的字段名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'XMLFieldName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据类型。 默认 1
1： int
2： string
3： datetime' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'DataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'固定值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'FixedValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据长度。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'Length'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否保存到数据库。 0：不保存。 1：保存。  默认 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding', @level2type=N'COLUMN',@level2name=N'SaveToDB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CSV-XML Binding 表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CSVXMLBinding'
GO
