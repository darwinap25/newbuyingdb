USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckMobileNumberList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckMobileNumberList]
  @MobileNumberList       varchar(max),  -- 手机号码列表 (字符串，XML格式： <ROOT><MOBILE NUMBER= 1234123 /><MOBILE NUMBER= 9999999 /></ROOT>)
  @IncludeCountryCode     int = 0     -- 提供的手机号码是否包含国家码。 0：不包含。 1： 包含 
AS
/****************************************************************************
**  Name : CheckMobileNumberList   
**  Version: 1.0.0.0
**  Description : 检查输入的手机号码，过滤其中不属于会员的号码。 返回这些号码中的会员列表
**  Parameter :
**
  declare @Mobile varchar(max), @PageCount int, @recordcount int, @a int  
  set @Mobile = '<ROOT><MOBILE NUMBER= 1234123 /><MOBILE NUMBER= 9999999 /></ROOT>'
  exec @a = CheckMobileNumberList @Mobile, 0, '', '', 1, 0, @PageCount output, @recordcount output, ''
  print @a  
  print @PageCount
  print @recordcount
  
**  Created by: Gavin @2014-11-10
****************************************************************************/
begin
  declare @idoc int 
  declare @MobileList table(MoblieNumber varchar(64))

  set @MobileNumberList = ISNULL(@MobileNumberList, '')
  
  if @MobileNumberList <> ''
  begin
    EXEC sp_xml_preparedocument @idoc OUTPUT, @MobileNumberList 
    insert into @MobileList(MoblieNumber)
    SELECT MoblieNumber FROM OPENXML (@idoc, '/ROOT/MOBILE', 1) 
        WITH (MoblieNumber varchar(64))             
    if @idoc>0  
      EXECUTE sp_xml_removedocument @idoc  
  end
  if @IncludeCountryCode = 0
    select MemberID, MemberMobilePhone from Member where MemberMobilePhone in (select MoblieNumber from @MobileList)
  else
    select MemberID, MemberMobilePhone from Member 
      where MemberID in (select MemberID from MemberMessageAccount 
                           where MessageServiceTypeID = 2 
                              and AccountNumber in (select MoblieNumber from @MobileList))
       
  return 0
end

GO
