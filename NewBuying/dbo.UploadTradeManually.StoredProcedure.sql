USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UploadTradeManually]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[UploadTradeManually]
  @MemberID int,                   -- 会员ID
  @CardNumber varchar(64),         -- 会员卡号，有卡号时以卡号为准。只有MemberID时，选第一个卡
  @Action				char(1),           -- 操作： 'A': 增加。'U': 更新（需要KeyID）。'D':删除（需要KeyID）.
  @TradeManuallyCode varchar(64),  -- 表主键，Update时必填。
  @ReferenceNo varchar(64),        -- 单号
  @StoreID int,                    -- 店铺ID
  @Busdate datetime,               -- 单据日期
  @TraderAmount money,             -- 交易金额
  @BrandID int,                    -- 品牌(店铺)
  @TenderID int,                   -- 支付tender ID
  @Remark varchar(512),            -- 备注
  @Receipt varchar(512),           -- 上传的小票文件地址.
  @TradeStatus int                 -- 0: 仅上传，不提交批核。  1： 提交批核
AS
/****************************************************************************
**  Name : UploadTradeManually  
**  Version: 1.0.0.2
**  Description : 手动上传交易 (Kiosk使用的接口，一个Head 下只有一个 detail 记录)
**  Parameter :
  declare @a int
  exec @a = UploadTradeManually null, null, 'CN0101395', ''
  print @a
 
**  Created by: Gavin @2014-11-24
**  Modify by: Gavin @2015-11-06 (ver 1.0.0.1) 检查交易号是否已经存在时，增加ApproveStatus的条件，只有P和A的有效
**  Modify by: Gavin @2016-11-25 (ver 1.0.0.2) 修正@TradeStatus 的逻辑判断。确定 0：仅上传，不提交批核。  1： 提交批核 
**
****************************************************************************/
begin
  declare @NewNum varchar(64), @AppStatus char(1)
  declare @ApproveOn datetime, @ApproveBy int, @ApprovalCode varchar(6)
  
  set @ApprovalCode = ''
  set @ApproveOn = null
  set @ApproveBy = null
  set @TradeStatus = ISNULL(@TradeStatus, 0) 
  set @MemberID = ISNULL(@MemberID, 0) 
  set @CardNumber = ISNULL(@CardNumber, '')
  if @MemberID = 0 
    return -7          -- 没有输入MemberID
  if @CardNumber = '' 
    return -8          -- 没有输入CardNumber    

  if @TradeStatus = 0
    set @AppStatus = 'P'
  else
    set @AppStatus = 'A'
  if @AppStatus = 'A'
  begin
    exec GenApprovalCode @ApprovalCode output
    set @ApproveOn = Getdate()
    set @ApproveBy = @MemberID
  end
       
  if @Action = 'A'
  begin
    if exists(select * from Ord_TradeManually_D D left join Ord_TradeManually_H H on H.TradeManuallyCode = D.TradeManuallyCode 
                  where D.ReferenceNo = @ReferenceNo and H.ApproveStatus in ('P', 'A'))
      return - 15    -- TxnNo已经存在（重复提交）
      
    exec GetRefNoString 'MTRD', @NewNum output
    insert into Ord_TradeManually_H(TradeManuallyCode, CardNumber, BrandID, Busdate, TenderID, Remark, ApprovalCode, 
       ApproveStatus, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CreatedBusDate, ApproveOn, ApproveBy)
    values(@NewNum, @CardNumber, @BrandID, @Busdate, @TenderID, @Remark, @ApprovalCode, 
       @AppStatus, GETDATE(), @MemberID, GETDATE(), @MemberID, @Busdate, @ApproveOn, @ApproveBy)   
    insert into Ord_TradeManually_D (TradeManuallyCode, StoreID, TraderAmount, EarnPoint, ReferenceNo, Receipt)
    values (@NewNum, @StoreID, @TraderAmount, 0, @ReferenceNo, @Receipt)
  end else if @Action = 'U'
  begin
    update Ord_TradeManually_H set BrandID = @BrandID, Busdate = @Busdate, TenderID = @TenderID, Remark = @Remark,  
        ApproveStatus = @AppStatus, UpdatedOn = GETDATE(), ApprovalCode = @ApprovalCode, ApproveOn=@ApproveOn, ApproveBy=@ApproveBy
    where TradeManuallyCode = @TradeManuallyCode
    update Ord_TradeManually_D set StoreID = @StoreID, TraderAmount = @TraderAmount, ReferenceNo = @ReferenceNo, Receipt = @Receipt
    where TradeManuallyCode = @TradeManuallyCode  
    if @@ROWCOUNT = 0 
      return -11  -- 没有找到原单.
  end else if @Action = 'D'
  begin 
    delete from Ord_TradeManually_D
      where TradeManuallyCode in (select TradeManuallyCode from Ord_TradeManually_H where TradeManuallyCode = @TradeManuallyCode and ApproveStatus <> 'A') 
    delete from Ord_TradeManually_H where TradeManuallyCode = @TradeManuallyCode and ApproveStatus <> 'A'
  end 

  return 0
end

GO
