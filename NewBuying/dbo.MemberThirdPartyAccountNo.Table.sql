USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberThirdPartyAccountNo]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberThirdPartyAccountNo](
	[MemberThirdPartyAccountNoID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyID] [int] NULL,
	[MemberID] [int] NULL,
	[AccountNumber] [varchar](512) NULL,
	[AccountPWD] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MEMBERTHIRDPARTYACCOUNTNO] PRIMARY KEY CLUSTERED 
(
	[MemberThirdPartyAccountNoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MemberThirdPartyAccountNo] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[MemberThirdPartyAccountNo] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberThirdPartyAccountNo', @level2type=N'COLUMN',@level2name=N'MemberThirdPartyAccountNoID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三方ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberThirdPartyAccountNo', @level2type=N'COLUMN',@level2name=N'ThirdPartyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberThirdPartyAccountNo', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三方账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberThirdPartyAccountNo', @level2type=N'COLUMN',@level2name=N'AccountNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三方账号密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberThirdPartyAccountNo', @level2type=N'COLUMN',@level2name=N'AccountPWD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员的第三方账号（未定）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberThirdPartyAccountNo'
GO
