USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AddMemberSalesPoint]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[AddMemberSalesPoint]
  @UserID               int,               -- 操作员
  @CardNumber           varchar(64),
  @AddPoint             int,               --  
  @TxnNo                varchar(64)='',       -- 交易单号
  @StoreID              int = null,
  @RegisterCode         varchar(64) = '',
  @ServerCode           varchar(64) = '',
  @BusDate              datetime = null,
  @TxnDate              datetime = null
AS
/****************************************************************************
**  Name : AddMemberSalesPoint  
**  Version: 1.0.0.0
**  Description : 会员增加积分操作。（交易单奖励积分）
**  Parameter :
  declare @a int  
  exec @a = AddMemberSalesPoint 1, '123213', 20 
  print @a  
  
**  Created by: Gavin @2013-11-29
**
****************************************************************************/
begin  	  
    if @AddPoint > 0 
	begin
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
	  select 25, @CardNumber, null, 0, @TxnNo, TotalAmount, 0, TotalAmount, @AddPoint, @BusDate, @Txndate, 
	      null, null, null, null, '', @UserID, @StoreID, @RegisterCode, @ServerCode,
	      TotalPoints, isnull(TotalPoints, 0) + @AddPoint, CardExpiryDate, CardExpiryDate, Status, Status 
	    from card where CardNumber = @CardNumber 
    end
    
    return 0
end

GO
