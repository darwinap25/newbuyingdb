USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewSex_isOnlineSKU]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewSex_isOnlineSKU]
AS
/*
*/
select * from Gender where GenderID in
(select distinct A.ForeignkeyID from (select * from Product_Classify where ForeignTable = 'GENDER') A 
   left join Product B on A.ProdCode = B.ProdCode where B.IsOnlineSKU = 1)




GO
