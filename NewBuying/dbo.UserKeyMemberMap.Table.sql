USE [NewBuying]
GO
/****** Object:  Table [dbo].[UserKeyMemberMap]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserKeyMemberMap](
	[UserKey] [varchar](64) NOT NULL,
	[MemberID] [int] NULL,
	[OSID] [varchar](64) NULL,
	[DeviceID] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_USERKEYMEMBERMAP] PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[UserKeyMemberMap] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NewTempMemberAccount 产生的36位字符串' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserKeyMemberMap', @level2type=N'COLUMN',@level2name=N'UserKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产生的MemberID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserKeyMemberMap', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' 1=IOS, 2=Android, 3=Desktop' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserKeyMemberMap', @level2type=N'COLUMN',@level2name=N'OSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备的deviceID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserKeyMemberMap', @level2type=N'COLUMN',@level2name=N'DeviceID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserKeyMemberMap', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UserKey 和 MemberID的Mapping 表。  for Bauhaus
@2016-02-15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserKeyMemberMap'
GO
