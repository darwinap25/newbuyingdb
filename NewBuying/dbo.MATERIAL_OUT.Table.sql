USE [NewBuying]
GO
/****** Object:  Table [dbo].[MATERIAL_OUT]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MATERIAL_OUT](
	[MATERIALID] [int] NOT NULL,
	[MATERIALCode] [varchar](64) NULL,
	[MATERIALName1] [varchar](512) NULL,
	[MATERIALName2] [varchar](512) NULL,
	[MATERIALName3] [varchar](512) NULL,
 CONSTRAINT [PK_MATERIAL_OUT] PRIMARY KEY CLUSTERED 
(
	[MATERIALID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MATERIAL_OUT', @level2type=N'COLUMN',@level2name=N'MATERIALID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'季节编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MATERIAL_OUT', @level2type=N'COLUMN',@level2name=N'MATERIALCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MATERIAL_OUT', @level2type=N'COLUMN',@level2name=N'MATERIALName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MATERIAL_OUT', @level2type=N'COLUMN',@level2name=N'MATERIALName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MATERIAL_OUT', @level2type=N'COLUMN',@level2name=N'MATERIALName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外部材质。  for Bauhaus
@2016-02-24' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MATERIAL_OUT'
GO
