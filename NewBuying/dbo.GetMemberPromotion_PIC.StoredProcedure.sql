USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberPromotion_PIC]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberPromotion_PIC]
  @PromotionMsgCode     varchar(64),			
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。   
  @LanguageAbbr			varchar(20)=''
AS
/****************************************************************************
**  Name : GetMemberPromotion_PIC  获得会员的优惠消息的图片组
**  Version: 1.0.0.0
**  Description : 
**  Parameter :
 declare @PageCount int, @RecordCount int
 exec GetMemberPromotion_PIC '', 1,0, @PageCount output, @RecordCount output
**  Created by: Gavin @2014-07-09
****************************************************************************/
begin
  declare @Language int, @SQLStr nvarchar(4000)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @PromotionMsgCode = isnull(@PromotionMsgCode, '')  
  set @SQLStr = 'select * from PromotionMsg_Pic where PromotionMsgCode = ''' + @PromotionMsgCode + ''' or ''' + @PromotionMsgCode + '''= '''' '
 
  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, 'order by PromotionMsgCode, KeyID', ''  
  return 0
end

GO
