USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckSecurityCode]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckSecurityCode]
   @SecurityCode varchar(512), 
   @ClearText varchar(512), 
   @EncryptDate datetime 
AS
/****************************************************************************
**  Name : CheckSecurityCode
**  Version: 1.0.0.2
select * from RSAKeys
declare @a int
exec @a = CheckSecurityCode '', '', '2013-04-22 12:23:00'
print @a
**  Description : 解密@SecurityCode
**  Created by: Gavin @2012-02-08
**  Modify by: Gavin @2013-09-20 (ver 1.0.0.1) 如果输入的@SecurityCode为空,那么跳过校验,直接返回0
**  Modify by: Gavin @2014-08-30 (ver 1.0.0.2) for 711，调用方产生的校验码出现问题，暂时直接返回0不做校验
**
****************************************************************************/
begin
  declare @PrivateKey varbinary(1024), @PublicKey varbinary(1024), @a int, @b int, @DecryStr varchar(512), @ReturnResult int,
		  @DecryPriKeyStr varchar(1024)
		  
  --调试阶段
  --return 0
  --ver 1.0.0.2
  return 0
  
  if isnull(@SecurityCode, '') = ''
    return 0
  
  set @ReturnResult = 0		  
  set @a = 1163
  set @b = 59
  select @PrivateKey = PrivateKey, @PublicKey = PublicKey from RSAKeys where StartDate <= @EncryptDate and EndDate >= @EncryptDate
  if isnull(@PrivateKey, 0) = 0 
  begin
    set @ReturnResult = -40
  end else
  begin
    exec DecryptRSAKey @PrivateKey, @DecryPriKeyStr output
    select @DecryStr = dbo.f_RSADecry(@SecurityCode, @DecryPriKeyStr, @a, @b)  
    if LTrim(RTrim(@ClearText)) = @DecryStr
      set @ReturnResult = 0
    else
      set @ReturnResult = -42  
  end    
  return @ReturnResult  
end

GO
