USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberPromotionMsgRead]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberPromotionMsgRead](
	[MemberID] [int] NOT NULL,
	[PromotionMsgCode] [varchar](64) NOT NULL,
	[IsRead] [int] NULL,
 CONSTRAINT [PK_MEMBERPROMOTIONMSGREAD] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC,
	[PromotionMsgCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPromotionMsgRead', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠消息Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPromotionMsgRead', @level2type=N'COLUMN',@level2name=N'PromotionMsgCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已读。0：没有。 1： 是的。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPromotionMsgRead', @level2type=N'COLUMN',@level2name=N'IsRead'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员对PromotionMsg 表的读取状态表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPromotionMsgRead'
GO
