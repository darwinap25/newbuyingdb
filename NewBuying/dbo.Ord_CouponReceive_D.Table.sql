USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponReceive_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponReceive_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponReceiveNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NULL,
	[Description] [nvarchar](512) NULL,
	[OrderQTY] [int] NULL,
	[ActualQTY] [int] NULL,
	[FirstCouponNumber] [varchar](64) NULL,
	[EndCouponNumber] [varchar](64) NULL,
	[BatchCouponCode] [varchar](64) NULL,
	[CouponStockStatus] [int] NULL DEFAULT ((2)),
	[ReceiveDateTime] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_ORD_COUPONRECEIVE_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'CouponReceiveNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述。（例如改为损坏状态时填写的具体描述）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货的订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'OrderQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收到的数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'ActualQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收货批次的首coupon号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'FirstCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际收货批次的末coupon号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'EndCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstCouponNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'BatchCouponCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收到的Coupon状况。默认 2
2. Good For Release (總部確認收貨)
3. Damaged (總部發現有優惠劵損壞)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'CouponStockStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建此明细的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D', @level2type=N'COLUMN',@level2name=N'ReceiveDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货确认单子表
（需要逐个Coupon收货时， OrderQty=ActiveQTY=1， FirstCouponNumber=EndCouponNumber）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponReceive_D'
GO
