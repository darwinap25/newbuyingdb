USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewMemberCoupons]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewMemberCoupons]
   
AS
  select (RTrim(C.CouponTypeID) + CouponNumber) as SeqNo, CouponNumber, C.CouponTypeID, Y.CouponTypeCode, CouponExpiryDate, 
    D.MemberID, C.Status as CouponStatus, Y.CouponTypeAmount, Y.CouponTypePoint, Y.CouponTypeDiscount, '' as CouponTypeBindPLU,
	Y.CouponTypeName1, Y.CouponTypeName2, Y.CouponTypeName3, 
	M.MemberEmail, M.MemberEngFamilyName, M.MemberEngGivenName, M.MemberChiFamilyName, M.MemberChiGivenName,
	M.MemberSex, M.MemberDateOfBirth, M.MemberDayofBirth, M.MemberMonthofBirth, M.MemberYearofBirth, M.HomeTelNum, 
	Y.BrandID, B.StoreBrandName1 as BrandName, C.BatchCouponID, C.CardNumber, Y.CouponTypeNotes, Y.CouponTypeLayoutFile, C.CouponAmount, Y.CouponTypePicFile, C.UpdatedOn,
	Y.CampaignID, G.CampaignCode, G.CampaignType, Y.IsPhysicalCoupon, Y.IsNeedVerifyNum, Y.VerifyNumLifecycle, Y.CouponNumMask, Y.CouponTypeNatureID, Y.CouponTypeTransfer,
	Y.AllowShareCoupon, C.CouponRedeemDate, C.IsRead, C.IsFavorite, 
	Y.AllowDeleteCoupon, Y.QRCodePrefix, Y.MaxDownloadCoupons, Y.CouponReturnValue, Y.TrainingMode, Y.UnlimitedUsage
	, A.ClauseName, A.ClauseName2, A.ClauseName3, A.MemberClauseDesc1, A.MemberClauseDesc2, A.MemberClauseDesc3
    from dbo.Coupon C 
      left join dbo.Card D on C.CardNumber = D.CardNumber
      left join dbo.Member M on D.MemberID = M.MemberID
      left join dbo.CouponType Y on C.CouponTypeID = Y.CouponTypeID 
      left join dbo.CouponTypeNature N on Y.CouponTypeNatureID = N.CouponTypeNatureID 
      left join dbo.Brand B on Y.BrandID = B.StoreBrandID
      left join (select CouponTypeID, max(CampaignID) as CampaignID from dbo.CampaignCouponType group by CouponTypeID) P on C.CouponTypeID = P.CouponTypeID  
      left join dbo.Campaign G on P.CampaignID = G.CampaignID  
      left join (select ClauseSubCode, ClauseName,ClauseName2,ClauseName3,MemberClauseDesc1,MemberClauseDesc2,MemberClauseDesc3  from dbo.MemberClause where ClauseTypeCode = 'CouponType') A on A.ClauseSubCode = cast(C.CouponTypeID as varchar)

GO
