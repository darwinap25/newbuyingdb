USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoWalletRule]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[DoWalletRule]
  @RuleCode               varchar(64)     -- 单据号码
AS
/****************************************************************************
**  Name : DoWalletRule
**  Version: 1.0.0.2
**  Description : 根据需求自动创建卡, 并且绑定到 store, 并且激活. 一步完成.
**  example :
  declare @a int  , @CardOrderFormNumber varchar(64)
  set @CardOrderFormNumber = 'COPOCP000000006'
  exec @a = DoWalletRule  @CardOrderFormNumber  
  print @a  
  select * from WalletRule_D
**  Created by: Gavin @2015-04-23
**  Modify by: Gavin @2015-05-08 （ver 1.0.0.1） 查询卡时，需要查询这个卡是否被void了， 只有 active 的卡 才是有效的。 否则一律无效
**  Modify by: Gavin @2015-05-28 （ver 1.0.0.2） 调用新的BatchGenerateNumber_base,增加输入参数 storeid
**
****************************************************************************/
begin
  DECLARE @CardQty int, @CardTypeID int, @CardGradeID int, @StoreID int
  DECLARE @A int,  @ReturnBatchID varchar(30),  @ReturnStartNumber nvarchar(30),  @ReturnEndNumber nvarchar(30)  
  DECLARE @TempTable table(CardTypeID int, CardGradeID int, StoreID int)
  DECLARE @IssueDate DATETIME
  SET @IssueDate = GETDATE()
  
  -- 检查指定的store，cardgrade，是不是已经分配了卡。 如果都已经分配了则推出
  INSERT INTO @TempTable (CardTypeID, CardGradeID, StoreID)
  SELECT DISTINCT D.CardTypeID, D.CardGradeID, D.StoreID from WalletRule_D D 
    left join WalletRule_H H on D.WalletRuleCode = H.WalletRuleCode    
    left join Card C on D.CardGradeID = C.CardGradeID and D.CardTypeID = C.CardTypeID and D.StoreID = C.IssueStoreID and C.Status = 2 
  where C.CardNumber is null and D.WalletRuleCode = @RuleCode 
  
  if not exists(select * from @TempTable)
    return 0    
  
  DECLARE CUR_DoWalletRule_CreateCard CURSOR fast_forward local FOR
    SELECT CardTypeID, CardGradeID, StoreID FROM @TempTable 
  OPEN CUR_DoWalletRule_CreateCard
  FETCH FROM CUR_DoWalletRule_CreateCard INTO @CardTypeID, @CardGradeID, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN 
     -- 创建卡。(逐张创建)
    exec @A = BatchGenerateNumber_base 1, 0, @CardTypeID, @CardGradeID, null, 1, @IssueDate, 0, 0, 0, '', 0, 
      '','', 1, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, '', 2, 6, @StoreID
    
    -- 绑定店铺.
    if @A = 0
      Update Card set IssueStoreID = @StoreID where CardNumber = @ReturnStartNumber
    
    FETCH FROM CUR_DoWalletRule_CreateCard INTO @CardTypeID, @CardGradeID, @StoreID
  END
  CLOSE CUR_DoWalletRule_CreateCard
  DEALLOCATE CUR_DoWalletRule_CreateCard  

  return 0
end

GO
