USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessageReceiveList]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageReceiveList](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MessageID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[AccountNumber] [nvarchar](512) NOT NULL,
	[IsRead] [int] NULL DEFAULT ((0)),
	[Status] [int] NOT NULL DEFAULT ((0)),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[ReceiverType] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_MESSAGERECEIVELIST] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息表主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList', @level2type=N'COLUMN',@level2name=N'MessageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员主键。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号。此账号从MemberMessageAccount获得，根据MemberID和主表的MessageServiceTypeID。（冗余字段，为方便读取）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList', @level2type=N'COLUMN',@level2name=N'AccountNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息是否已读。 0：未被读。 1：已读。 默认0
@2014-10-28： 增加 -1 标志， 表示 已被删除。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList', @level2type=N'COLUMN',@level2name=N'IsRead'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'-1：未准备好，需要处理 。0,待发送。1：发送成功。 2：发送失败。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息接受者类型。
0：会员。1：管理人员。
（决定MemberID内容来源。
 0：表示MemberID，AccountNumber可以到MemberMessageAccount 中取
1：表示Accounts_Users的ID）
默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList', @level2type=N'COLUMN',@level2name=N'ReceiverType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息接收方ID列表。（支持一个消息同时发送多人）。
接收方式在 MessageObject中设置。因为是会员，具体账号在会员系列表中。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageReceiveList'
GO
