USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCardMovementList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberCardMovementList]
  @MemberID			int,           --过滤条件，会员ID
  @CardNumber			varchar(512),  --过滤条件，会员卡号     
  @JustTxn				int = 0,		   --过滤条件，0：所有movement记录。1：仅ReceiveTxn记录
  @ConditionStr			nvarchar(1000),    --自定义条件
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数
  @TotalAmount			money output,	   -- 返回总金额
  @TotalPoints			int output	   -- 返回总积分   
AS
/****************************************************************************
**  Name : GetMemberCardMovementList
**  Version: 1.0.0.5
**  Description :查找SVA中的receiveTxn 和 Card_Movement
**
**  Parameter :
  declare  @PageCount int, @RecordCount int, @TotalAMount money, @TotalPoints			int
  exec GetMemberCardMovementList 0, '000300933', 0, '', 1, 0, @PageCount output, @RecordCount output,@TotalAMount output,@TotalPoints output
  print @TotalAMount
  print @TotalPoints
  select count(*) from card_movement
select KeyID, OprID, M.CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, Amount, Points, BusDate, Txndate, NewExpiryDate, 
  CardCashDetailID, CardPointDetailID, TenderID, Additional, M.ApprovalCode, Remark, M.CreatedOn, M.CreatedBy  
from Card_movement M left join Card C on M.CardNumber = C.CardNumber  
where (C.MemberID = 741 or  741 = 0) and (C.CardNumber = '000300933' or isnull('000300933', '') = '') 

select sum(Amount), sum(Points)
     from Card_movement M left join Card C on M.CardNumber = C.CardNumber
where (C.MemberID = 0 or  0 = 0) and (C.CardNumber = '000300933' or isnull('000300933', '') = '') 

  select * from coupon
  select * from card_movement where cardnumber = '000300933'
**  Created by: Gavin @2012-05-23
**  Modify by Gavin @2012-09-10 (ver 1.0.0.1) 获得TotalAMount和 TotalPoints 的条件应该按照输入的条件.
**  Modified by: Gavin @2013-06-21 (Ver 1.0.0.2) 使用1.0.0.6版本的SelectDataInBatchs	,输入的@ConditionStr直接传给SelectDataInBatchs 
**  Modified by: Gavin @2013-08-06 (Ver 1.0.0.3) 读取CardGrade的NumberOfTransDisplay设置. 如果输入的@PageSize=0，则返回NumberOfTransDisplay的设置数
**  Modified by: Gavin @2014-09-23 (Ver 1.0.0.4) （for 711) 必须输入memberid或者card。 即必须确定用户，不填用户则报错 -7
**  Modified by: Gavin @2015-01-15 (Ver 1.0.0.5) 查询条件中忽略 memberid, 只按照cardnumber来查询. 用户必须输入cardnumber. 取消CardNumber允许为空的条件。
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(max), @MemberIDStr varchar(512), @OrderCondition nvarchar(max), @NumberOfTransDisplay int
  set @MemberIDStr = convert(varchar(512), isnull(@MemberID, 0))
  set @CardNumber = isnull(@CardNumber, '')
  
  if isnull(@CardNumber, '') = ''
    return -7

  select @NumberOfTransDisplay = G.NumberOfTransDisplay from Card C left join CardGrade G on C.CardGradeID = G.CardGradeID
    where C.CardNumber = @CardNumber 
  set @NumberOfTransDisplay = isnull(@NumberOfTransDisplay, 0)

  if @JustTxn = 0 
  begin  
    set @SQLStr = 'select KeyID, OprID, M.CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, Amount, Points, BusDate, Txndate, NewExpiryDate, CardCashDetailID, CardPointDetailID, TenderID, Additional, M.ApprovalCode, Remark, M.CreatedOn, M.CreatedBy '
        + ' from Card_movement M left join Card C on M.CardNumber = C.CardNumber '
        + ' where M.CardNumber = '''+ @CardNumber + ''' '
  end else
  begin
    set @SQLStr = N'select KeyID, OprID, M.CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, Amount, Points, BusDate, Txndate, NewExpiryDate, CardCashDetailID, CardPointDetailID, TenderID, Additional, M.ApprovalCode, Remark, M.CreatedOn, M.CreatedBy '
        + ' from Card_movement M left join Card C on M.CardNumber = C.CardNumber '
        + ' where M.CardNumber = '''+ @CardNumber + ''' '
  end

  if isnull(@ConditionStr, '') = ''
  begin
    select @TotalAmount=sum(Amount), @TotalPoints=sum(Points)
      from Card_movement M left join Card C on M.CardNumber = C.CardNumber
     where M.CardNumber = @CardNumber        
  end else
  begin
    declare @SQLStr_Total nvarchar(max)
    set @SQLStr_Total = 'select @TA =sum(Amount), @TP =sum(Points) '
      + ' from Card_movement M left join Card C on M.CardNumber = C.CardNumber '
      + ' where M.CardNumber = '''+ @CardNumber + ''' '
      + ' and (' + @ConditionStr  + ')'      
    exec sp_executesql @SQLStr_Total, N'@TA money output, @TP int output',@TotalAmount output, @TotalPoints output
  end   
  
--  if isnull(@ConditionStr, '') <> ''
--    set @SQLStr = @SQLStr + ' and (' + @ConditionStr  + ')'
  if @PageSize = 0
    set @PageSize = @NumberOfTransDisplay

  set @OrderCondition='order by Txndate desc'
  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr
   
  return 0
end

GO
