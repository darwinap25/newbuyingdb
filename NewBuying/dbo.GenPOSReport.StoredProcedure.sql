USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenPOSReport]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenPOSReport]
  @StoreCode          VARCHAR(64),     -- StoreCode
  @RegisterCode       VARCHAR(64),     -- RegisterCode
  @BusDate            DATETIME,        -- 交易日
  @ReportID           INT              -- 报表ID
AS
/****************************************************************************
**  Name : GenPOSReport
**  Version : 1.0.0.0
**  Description : 产生报表
**
declare @a int
exec @a=GetPOSReport '1','R01','2015-03-25',1
print @a

**  Created by Gavin @2015-03-26
****************************************************************************/
BEGIN
  SET NOCOUNT ON
  DECLARE @TenderDesc VARCHAR(64), @TenderType INT, @COUNT INT, @LocalAmount MONEY, @LINESUM MONEY
  DECLARE @LineLen INT, @LineStr VARCHAR(100), @SPACESTR VARCHAR(50)
  DECLARE @Report TABLE(KeyID INT, LineStr VARCHAR(100))
    

  INSERT INTO @Report
  exec POSHourReport '1','R01','2015-03-24',1

  select * from @Report 
  --INSERT INTO POSDAYREPORT

  SET NOCOUNT OFF
  RETURN 0
END

GO
