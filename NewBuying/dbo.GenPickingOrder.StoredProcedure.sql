USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenPickingOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenPickingOrder] 
  @StoreOrderNumber VARCHAR(64)
AS
/****************************************************************************
**  Name : GenPickingOrder  
**  Version: 1.0.0.2
**  Description : 店铺订单批核后，产生Picking 单
**  Parameter :
select *  from Ord_PickingOrder_D
update Ord_StoreOrder_H set approvestatus = 'A' where storeordernumber = 'SPO000000000011'
Ord_StoreOrder_H
**  Created by: Gavin @2015-04-13
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
**  Modify by Gavin @2016-10-25 (1.0.0.2) 单号前缀有 SHP改为 WPO 
**
****************************************************************************/
BEGIN
  DECLARE @NewNumber VARCHAR(64), @BusDate DATETIME, @StoreCode varchar(64)
  EXEC GetRefNoString 'WPO', @NewNumber OUTPUT
 
  select @StoreCode = StoreCode FROM Ord_StoreOrder_H A left join Buy_STORE B on A.StoreID = B.StoreID
    WHERE StoreOrderNumber = @StoreOrderNumber

  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC
  
  INSERT INTO Ord_PickingOrder_H (PickingOrderNumber,OrderType,ReferenceNo,FromStoreID,FromContactName,
      FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
      StoreMobile,StoreAddress,Remark,CreatedBusDate,ApproveBusDate,ApprovalCode,ApproveStatus,
      ApproveOn,ApproveBy,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)
  SELECT @NewNumber, 1, StoreOrderNumber, FromStoreID, FromContactName,
      FromContactPhone,FromMobile,FromEmail,FromAddress,StoreID,StoreContactName,StoreContactPhone,StoreContactEmail,
      StoreMobile,StoreAddress,Remark,@BusDate,NULL,'','P',
      NULL,ApproveBy,GETDATE(),CreatedBy,GETDATE(),UpdatedBy
    FROM Ord_StoreOrder_H 
    WHERE StoreOrderNumber = @StoreOrderNumber
    
  INSERT INTO Ord_PickingOrder_D (PickingOrderNumber,ProdCode,OrderQty,ActualQty,Remark)
  SELECT @NewNumber, ProdCode, OrderQty, OrderQty, 'Auto Gen Picking Order'  
    FROM Ord_StoreOrder_D D left join Ord_StoreOrder_H H on H.StoreOrderNumber = D.StoreOrderNumber
    WHERE H.StoreOrderNumber = @StoreOrderNumber

  RETURN 0   
END

GO
