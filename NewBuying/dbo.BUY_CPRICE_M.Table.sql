USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_CPRICE_M]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_CPRICE_M](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CPriceCode] [varchar](64) NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[VendorCode] [varchar](64) NULL,
	[StoreCode] [varchar](64) NULL,
	[StoreGroupCode] [varchar](64) NULL,
	[CurrencyCode] [varchar](64) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CPriceGrossCost] [dbo].[Buy_Amt] NOT NULL,
	[CPriceNetCost] [dbo].[Buy_Amt] NOT NULL,
	[CPriceDisc1] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[CPriceDisc2] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[CPriceDisc3] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[CPriceDisc4] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[CPriceBuy] [dbo].[Buy_Qty] NULL DEFAULT ((0)),
	[CPriceGet] [dbo].[Buy_Qty] NULL DEFAULT ((0)),
	[Status] [int] NULL DEFAULT ((1)),
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_CPRICE_M] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BUY_CPRICE_H表主键。用户可以直接写表记录，允许为空' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'VendorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺Code。空/NULL： 表示所有。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'这组价格针对的店铺组ID。空/NULL： 表示所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'StoreGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码。一般应该是本币代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CurrencyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价格失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'毛成本价。不包含所有折扣' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceGrossCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'净成本价。包含所有折扣' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceNetCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易折扣。（Transaction Discount）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceDisc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他折扣。（Other Discount）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceDisc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'特别折扣。（Special Discount）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceDisc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣4。（Discount 4）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceDisc4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How many to buy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceBuy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How many to get free' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'CPriceGet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。1：有效。0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主档记录。（BUY_CPRICE_H批核后产生此表记录。 也允许单独插入（此时CPriceCode为空））' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_CPRICE_M'
GO
