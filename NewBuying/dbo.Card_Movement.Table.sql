USE [NewBuying]
GO
/****** Object:  Table [dbo].[Card_Movement]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Card_Movement](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[StoreID] [int] NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[OprID] [int] NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[RefKeyID] [int] NULL,
	[RefReceiveKeyID] [int] NULL,
	[RefTxnNo] [varchar](512) NULL,
	[OpenBal] [money] NOT NULL,
	[Amount] [money] NOT NULL,
	[CloseBal] [money] NOT NULL,
	[OpenPoint] [int] NULL,
	[Points] [int] NULL,
	[ClosePoint] [int] NULL,
	[BusDate] [datetime] NULL,
	[Txndate] [datetime] NULL,
	[OrgExpiryDate] [datetime] NULL,
	[NewExpiryDate] [datetime] NULL,
	[OrgStatus] [int] NULL,
	[NewStatus] [int] NULL,
	[CardCashDetailID] [int] NULL,
	[CardPointDetailID] [int] NULL,
	[TenderID] [int] NULL,
	[Additional] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[SecurityCode] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [varchar](512) NULL,
 CONSTRAINT [PK_EGIFT_MOVEMENT] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Card_Movement] ADD  DEFAULT ((0)) FOR [OpenPoint]
GO
ALTER TABLE [dbo].[Card_Movement] ADD  DEFAULT ((0)) FOR [ClosePoint]
GO
ALTER TABLE [dbo].[Card_Movement] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Trigger [dbo].[Insert_Card_Movement]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Card_Movement] ON [dbo].[Card_Movement]
for INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_Card_Movement
* Version: 1.0.0.22
* Description :
*     Card_Movement表的Insert trigger，用于更新card表的 totalamount.(不判断卡状态是否允许，只是简单改卡状态，比如由Active 改为 already redeem)
*     此触发器中不再做校验，插入Card_Movement时应该已经校验通过的。
* Create By Gavin @2012-03-19
* Modify By Gavin @2012-08-13  (ver 1.0.0.1) 增加Oprid：25. （根据上传交易金额，获得积分）
* Modify By Gavin @2012-09-26 （ver 1.0.0.2）增加发送会员消息的功能。 如果金额或者积分变化时发送
* Modify By Gavin @2012-10-22 （ver 1.0.0.3）增加OprID:61,62 的处理。 （61:减CashDetail，加pointdetail。 62：减pointdetail，加cashdetail）
                                             修正NewStatus字段填写错误。
* Modify By Gavin @2012-12-28 （ver 1.0.0.4）增加OprID:26(会员绑定卡并激活)
* Modify By Gavin @2013-03-12 （ver 1.0.0.5）增加OprID:57,58,59 （POS端Card操作：充值，消费，PostVoid）
* Modify By Gavin @2013-08-20 （ver 1.0.0.6）取消消息发送功能,此功能由其他服务完成.
* Modify By Gavin @2013-09-04 （ver 1.0.0.7）使用游标方式读取Inserted表数据，因为card_movement表数据可能批量写入，比如 DoCreateMember时
* Modify By Gavin @2014-05-28 （ver 1.0.0.8）card状态为 1 时(issue),可以直接充值,充值后状态变为2
* Modify By Gavin @2014-06-06 （ver 1.0.0.9）增加issue （OprID=28）
* Modify By Gavin @2014-07-04 （ver 1.0.0.10）增加issue （OprID=29）
* Modify By Gavin @2014-08-27 （ver 1.0.0.11）增加消息发送。
* Modify By Gavin @2014-08-29 （ver 1.0.0.12）取消消息发送。（未知原因：在触发器调用发送消息的过程没有效果。（过程已经调用，但是没有产生记录，单独调用没有问题））
* Modify By Gavin @2014-09-16 （ver 1.0.0.13）增加奖励获得的积分或者金额的有效期（OprID=27）
* Modify By Gavin @2014-09-23 （ver 1.0.0.14）修正 OprID=31时的逻辑判断, 这个是使用积分购买Coupon, 所以应该减积分.
* Modify By Gavin @2014-10-11 （ver 1.0.0.15）增加Card过期积分/金额, forfeit的处理 （OprID=63）
* Modify By Gavin @2015-04-10 （ver 1.0.0.16）更新amount, point时, 直接使用 movement的操作值, 加到 card表上.以防止并发提交产生的movement的 closebalance 计算不正确.
* Modify By Gavin @2015-04-21 （ver 1.0.0.17）读取card表数据时, 加上独享锁. 防止并发产生  (ROWLOCK,UPDLock)
* Modify By Gavin @2015-07-10 （ver 1.0.0.18）增加负金额负积分的判断. 如果操作会导致负数时, raise error.
* Modify By Gavin @2015-10-13 （ver 1.0.0.19）忽略OprID>1000 的记录。(OprID =1001 是卡升级)
* Modify By Gavin @2017-01-19 （ver 1.0.0.20）增加OprID=66 （清0） 的操作
* Modify By Gavin @2017-02-10 （ver 1.0.0.21）（for SWD） 增加处理Orpid = 68， 111
* Modify By Gavin @2017-04-27 （ver 1.0.0.22）(for Bauhaus）增加OprID=81的处理。（会员升级。 仅bauhaus使用。card_movement中没有记录升级的cardgradeid字段，所以只能hardcode的（101 -> 102）。 bauhaus单独版本。）
*/
/*==============================================================*/
BEGIN
  declare @OprID int
  declare @Movementkeyid int, @CardCashDetailID int, @CardPointDetailID int, @CardNumber nvarchar(30), @CardTypeID int, @ActAmount money,
          @RefTxnNo varchar(30), @Remark varchar(100), @RefReceiveKeyID varchar(30), @CreatedBy varchar(10), @Points int
  declare @CardGradeID int, @CardStatus int, @TotalAmount money, @TotalPoints int, @CardExpiryDate datetime    
  declare @MultExpireDate int
  declare @NewCardExpiryDate datetime, @NewAmountExpiryDate datetime, @NewPointExpiryDate datetime, @NewCardStatus int,
          @NewTotalAmount money, @NewTotalPoints int
  declare @TempAmt money, @BalanceAmount money, @TempPoint int, @BalancePoint int, @AmountExpiryDate datetime, @KeyID int
  declare @VoidTxnNo varchar(30), @VoidOprID int, @VoidReceiveKeyID varchar(36), @VoidCardCashDetailID int, 
          @VoidCardPointDetailID int, @VoidAddPoint int, @VoidBalancePoint int, @VoidAddAmount money, @VoidBalanceAmount money
  declare @EarnP int, @CalcResult int, @CumulativeConsumptionAmt money, @CashExpiredate int, @PointExpiredate int, @ActCardExpiryDate datetime
  declare @MemberID int, @Additional varchar(512), @NewStatus int
  declare @ForfeitAmt money, @ForfeitPoint int

DECLARE CUR_Card_Movement CURSOR fast_forward local FOR
  select keyid, CardNumber, isnull(Amount,0), isnull(Points,0), NewExpiryDate,
         OprID, RefTxnNo, Remark, CreatedBy, RefReceiveKeyID, Additional, NewStatus 
    from inserted
OPEN CUR_Card_Movement
FETCH FROM CUR_Card_Movement INTO @Movementkeyid, @CardNumber, @ActAmount, @Points, @ActCardExpiryDate, 
         @OprID, @RefTxnNo, @Remark, @Remark, @RefReceiveKeyID, @Additional, @NewStatus
WHILE @@FETCH_STATUS=0
BEGIN
     if @OprID = 81
	 begin
		  Update Card set CardGradeID = 102 
		  where CardNumber = @CardNumber and CardGradeID = 101
	 end

	 IF @OprID<>0 and @OprID <= 1000 and @OprID <> 81
	 BEGIN 
  
	  select @CardStatus = Status, @TotalAmount = isnull(TotalAmount,0), @CardExpiryDate = CardExpiryDate, @CardGradeID = CardGradeID,
			 @TotalPoints = isnull(TotalPoints, 0), @CardTypeID = CardTypeID, @MemberID = MemberID
		from Card with(ROWLOCK,UPDLock) where CardNumber = @CardNumber
	  select @CashExpiredate = isnull(CashExpiredate,0), @PointExpiredate = isnull(PointExpiredate, 0) from cardtype where cardtypeid = @CardTypeID   

  
	  -- 消费时，累计消费金额
	  set @CumulativeConsumptionAmt = 0
	  if @OprID in (3, 58)
		set @CumulativeConsumptionAmt = abs(isnull(@Actamount, 0))
    
	  -- 处理卡状态
	  set @NewCardStatus = @CardStatus
	  if @OprID = 111
	    set @NewCardStatus = 9
	  if @OprID = 68
	  begin
	    set @NewCardStatus = 2
      end

	  if (@OprID in (2, 11, 12, 21, 57)) and @CardStatus in (1, 4, 5)
		set @NewCardStatus = 2
	  if @OprID = 5
		set @NewCardStatus = 5
	  if @OprID = 9
		set @NewCardStatus = 4 
	  if (@OprID in (26))   -- 激活并绑定Card到Member
	  begin
		set @NewCardStatus = 2
		if isnull(@MemberID, 0) = 0
		  set @MemberID = cast(@Additional as int)
	  end
	  if @OprID = 28
		set @NewCardStatus = 1 
	  if @OprID = 29
		set @NewCardStatus = @NewStatus
          
	  -- 处理卡有效期 （充值，激活时的有效期计算）
	  exec CalcExpiryDate @OprID, @CardNumber, @ActAmount, @NewCardExpiryDate output, 
						  @NewAmountExpiryDate output, @NewPointExpiryDate output, @MultExpireDate output
                      
	  --如果OprID=2，忽略Point，如果OprID=14，忽略ActAmount。
	  -- 计算Card 的操作后值. (通用操作)  
	  if @OprID in (2, 57)
		set @Points = 0 
	  if @OprID = 14
		set @ActAmount = 0   

      -- ver 1.0.0.20 增加OprID=66的处理
	  if @OprID = 66
	  begin
	    set @ActAmount = -@TotalAmount
		set @Points = -@TotalPoints
	  end 
  
	  -- ver 1.0.0.18 增加负金额负积分的判断. 如果操作会导致负数时, raise error
	  set @NewTotalAmount = isnull(@TotalAmount,0) + isnull(@ActAmount,0)
	  set @NewTotalPoints = isnull(@TotalPoints,0) + isnull(@Points,0)
	  if @NewTotalAmount < 0 or @NewTotalPoints < 0
	  begin
		RAISERROR ('Card amount balance or point balance not allow less than 0 ', 16, 1)
		if (@@Trancount > 0)
		  ROLLBACK TRANSACTION 
		return   
	  end
	  -------------------------------
  
	  -- 多有效期设置，额外多加的计算。
	  if @MultExpireDate = 1
	  begin 
		-- 充值
		if (@OprID in (2, 11, 12, 21, 8, 7, 17, 57, 27) or (@OprID = 22 and @Actamount > 0)) or (@OprID = 62 and @Actamount > 0) or (@OprID = 68 and @Actamount > 0)  
		begin
			--特别处理转入
			if (@OprID in (7,17)) and (isnull(@Points,0)<>0 )
			begin
			  --Todo有效期应该从转入卡拿过来
			  insert into CardPointDetail 
				  (CardNumber, AddPoint, BalancePoint, ForfeitPoint, ExpiryDate, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
			  values
				  (@CardNumber, isnull(@Points,0), isnull(@Points,0), 0, @NewPointExpiryDate, 1, getdate(), @CreatedBy, getdate(), @CreatedBy)        
			  set @CardPointDetailID = SCOPE_IDENTITY()        
			end else
			begin        
			  insert into CardCashDetail 
				(CardNumber, TenderCode, TenderRate, AddAmount, BalanceAmount, ForfeitAmount, ExpiryDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, [Status])
			  values
				(@CardNumber, '', 1, isnull(@Actamount,0), isnull(@Actamount,0), 0, @NewAmountExpiryDate, getdate(), @CreatedBy, getdate(), @CreatedBy, 1)           
			  set @CardCashDetailID = SCOPE_IDENTITY()
			end
		end
		-- 消费 (3: 消费，根据消费金额计算获取积分。 15：金额兑换积分。根据要求兑换的积分，计算扣减的金额)(两者相同动作：减金额加积分)
		if @OprID in (3, 15, 6, 16, 58) or (@OprID = 22 and @Actamount < 0) or (@OprID = 61 and @Actamount < 0) or (@OprID = 63 and @Actamount < 0) or (@OprID = 68 and @Actamount < 0)
		begin
		  -- 扣减 CardCashDetail
		  set @TempAmt = abs(@Actamount)
		  declare CUR_MULTIEXPIRE CURSOR fast_forward local FOR
			select KeyID, BalanceAmount, ExpiryDate from CardCashDetail 
			  where [status] = 1 and CardNumber = @CardNumber and BalanceAmount > 0
			order by ExpiryDate
		  OPEN CUR_MULTIEXPIRE    
		  FETCH NEXT FROM CUR_MULTIEXPIRE INTO @KeyID, @BalanceAmount, @AmountExpiryDate
		  WHILE @@FETCH_STATUS = 0  
		  BEGIN 
			if @TempAmt >= @BalanceAmount
			begin
			  set @TempAmt = @TempAmt - @BalanceAmount
			  if @OprID = 63
				set @ForfeitAmt = @BalanceAmount
			  else
				set @ForfeitAmt = 0  
			  update CardCashDetail set BalanceAmount = 0, [status] = 0, ForfeitAmount = @ForfeitAmt,
				  UpdatedBy = @CreatedBy, UpdatedOn = getdate() 
				where KeyID = @KeyID 
			  set @CardCashDetailID = @KeyID             
			end       
			else if @TempAmt < @BalanceAmount
			begin
			  if @OprID = 63
				set @ForfeitAmt = @TempAmt
			  else
				set @ForfeitAmt = 0 
                    
			  update CardCashDetail set BalanceAmount = BalanceAmount - @TempAmt, ForfeitAmount = @ForfeitAmt,
				 UpdatedBy = @CreatedBy, UpdatedOn = getdate() 
				where KeyID = @KeyID             
			  set @TempAmt = 0
			  set @CardCashDetailID = @KeyID
			end
			if @TempAmt <= 0 
			  break          
			FETCH NEXT FROM CUR_MULTIEXPIRE INTO @KeyID, @BalanceAmount, @AmountExpiryDate
		  END
		  CLOSE CUR_MULTIEXPIRE  
		  DEALLOCATE CUR_MULTIEXPIRE  
           
		  -- 增加 CardPointDetail    
		  if @Points > 0
		  begin
			insert into CardPointDetail 
				(CardNumber, AddPoint, BalancePoint, ForfeitPoint, ExpiryDate, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
			values
				(@CardNumber, isnull(@Points,0), isnull(@Points,0), 0, @NewPointExpiryDate, 1, getdate(), @CreatedBy, getdate(), @CreatedBy)        
			--set @CardPointDetailID = @@IDENTITY              
			set @CardPointDetailID = SCOPE_IDENTITY()
		  end  
		end
    
		-- 积分消费
		if @OprID in (13, 31) or (@OprID = 23 and @Points < 0) or (@OprID in (6,16) and @Points<0) or (@OprID = 62 and @Points < 0) or (@OprID = 63 and @Points < 0)
		begin
		  set @TempPoint = abs(@Points)
		  declare CUR_POINTMULTIEXPIRE CURSOR fast_forward local FOR
			select KeyID, BalancePoint, ExpiryDate from CardPointDetail 
			  where [status] = 1 and CardNumber = @CardNumber and BalancePoint > 0
			order by ExpiryDate
		  OPEN CUR_POINTMULTIEXPIRE    
		  FETCH NEXT FROM CUR_POINTMULTIEXPIRE INTO @KeyID, @BalancePoint, @NewPointExpiryDate
		  WHILE @@FETCH_STATUS = 0  
		  BEGIN 
			if @TempPoint >= @BalancePoint
			begin
			  if @OprID = 63
				set @ForfeitPoint = @BalancePoint
			  else
				set @ForfeitPoint = 0          
			  set @TempPoint = @TempPoint - @BalancePoint
			  update CardPointDetail set BalancePoint = 0, [status] = 0, ForfeitPoint = @ForfeitPoint,
				  UpdatedBy = @CreatedBy, UpdatedOn = getdate() 
				where KeyID = @KeyID 
			  set @CardPointDetailID = @KeyID             
			end       
			else if @TempPoint < @BalancePoint
			begin 
			  if @OprID = 63
				set @ForfeitPoint = @TempPoint
			  else
				set @ForfeitPoint = 0 
                    
			  update CardPointDetail set BalancePoint = BalancePoint - @TempPoint, ForfeitPoint = @ForfeitPoint,
				 UpdatedBy = @CreatedBy, UpdatedOn = getdate()  
				where KeyID = @KeyID             
			  set @TempPoint = 0
			  set @CardPointDetailID = @KeyID
			end
			if @TempPoint <= 0 
			  break          
			FETCH NEXT FROM CUR_POINTMULTIEXPIRE INTO @KeyID, @BalancePoint, @AmountExpiryDate
		  END
		  CLOSE CUR_POINTMULTIEXPIRE  
		  DEALLOCATE CUR_POINTMULTIEXPIRE      
		end  
  
		-- 购买积分
		if @OprID=14 or (@OprID = 23 and @Points > 0) or (@OprID = 25 and @Points > 0) or @OprID = 27
		begin
		  -- 增加 CardPointDetail  
		  if @Points > 0   
		  begin
			insert into CardPointDetail 
			   (CardNumber, AddPoint, BalancePoint, ForfeitPoint, ExpiryDate, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
			values
			   (@CardNumber, isnull(@Points,0), isnull(@Points,0), 0, @NewPointExpiryDate, 1, getdate(), @CreatedBy, getdate(), @CreatedBy)        
			--set @CardPointDetailID = @@IDENTITY        
			set @CardPointDetailID = SCOPE_IDENTITY()
		  end  
		end
    
		-- void 交易，需要退回有效期。单独计算 (初步逻辑: void时是增加(金额或积分),则直接增加detail记录. 如果void时是扣减, 如果找到原来增加的detail记录,并且未被使用过,则update原记录, 否则按消费扣减)
		if @OprID in (4, 59)
		begin
		  select @VoidTxnNo = VoidTxnNo from receivetxn where KeyID = @RefReceiveKeyID             -- 本单receivetxn
		  select top 1 @VoidOprID = OprID, @VoidReceiveKeyID = KeyID from receivetxn where TxnNo = @VoidTxnNo  -- 原单receivetxn
		  select @VoidCardCashDetailID = CardCashDetailID, @VoidCardPointDetailID = CardPointDetailID    -- 原单receivetxn对应的card_movement
			from Card_Movement where RefReceiveKeyID = @VoidReceiveKeyID    
		  select @VoidAddAmount = AddAmount, @VoidBalanceAmount = BalanceAmount from CardCashDetail where KeyID = @VoidCardCashDetailID  
		  select @VoidAddPoint = AddPoint, @VoidBalancePoint = BalancePoint from CardPointDetail where KeyID = @VoidCardPointDetailID  

		  if @VoidOprID in (2, 11, 12, 21, 8, 57) 
		  begin
			if abs(@VoidAddAmount - @VoidBalanceAmount) < 0.0001    -- 充值金额没有被使用
			  update CardCashDetail set BalanceAmount = 0, UpdatedOn = getdate(), status = 0 where KeyID = @VoidCardCashDetailID
			else begin                                              -- 充值金额已经被使用，需要逐条加回去。
			  set @TempAmt = abs(@Actamount)
			  declare CUR_VOIDMULTIEXPIRE CURSOR fast_forward local FOR
				select KeyID, BalanceAmount, ExpiryDate from CardCashDetail 
				  where CardNumber = @CardNumber and BalanceAmount > 0 and Status = 1
				order by ExpiryDate
			  OPEN CUR_VOIDMULTIEXPIRE    
			  FETCH NEXT FROM CUR_VOIDMULTIEXPIRE INTO @KeyID, @BalanceAmount, @AmountExpiryDate
			  WHILE @@FETCH_STATUS = 0  
			  BEGIN 
				if @TempAmt >= @BalanceAmount
				begin
				  set @TempAmt = @TempAmt - @BalanceAmount
				  update CardCashDetail set BalanceAmount = 0, [status] = 0, UpdatedBy = @CreatedBy, UpdatedOn = getdate() where KeyID = @KeyID 
				  set @CardCashDetailID = @KeyID             
				end       
				else if @TempAmt < @BalanceAmount
				begin
				  update CardCashDetail set BalanceAmount = BalanceAmount - @TempAmt, UpdatedBy = @CreatedBy, UpdatedOn = getdate() where KeyID = @KeyID             
				  set @TempAmt = 0
				  set @CardCashDetailID = @KeyID
				end
				if @TempAmt <= 0 
				  break          
				FETCH NEXT FROM CUR_VOIDMULTIEXPIRE INTO @KeyID, @BalanceAmount, @AmountExpiryDate
			  END
			  CLOSE CUR_VOIDMULTIEXPIRE  
			  DEALLOCATE CUR_VOIDMULTIEXPIRE            
			end          
		  end
      
		  if @VoidOprID in (3, 58)  
		  begin
			-- 退回金额：简单逻辑， 直接加钱到@VoidCardCashDetailID指定的记录上（如果消费时在多个记录上扣钱，也只加回到最后一个记录上）        
			update CardCashDetail set BalanceAmount = BalanceAmount + @Actamount, [status] = 0, UpdatedBy = @CreatedBy, UpdatedOn = getdate()
			  where KeyID = @VoidCardCashDetailID
          
			if @VoidBalancePoint = @VoidAddPoint 
			  update CardPointDetail set BalancePoint = 0, UpdatedBy = getdate(), status = 0 where KeyID = @VoidCardPointDetailID
			else begin
			  set @TempPoint = abs(@Points)
			  declare CUR_VOIDPOINTMULTIEXPIRE CURSOR fast_forward local FOR
				select KeyID, BalancePoint, ExpiryDate from CardPointDetail 
				  where [status] = 1 and CardNumber = @CardNumber and BalancePoint > 0
				order by ExpiryDate
			  OPEN CUR_VOIDPOINTMULTIEXPIRE    
			  FETCH NEXT FROM CUR_VOIDPOINTMULTIEXPIRE INTO @KeyID, @BalancePoint, @NewPointExpiryDate
			  WHILE @@FETCH_STATUS = 0  
			  BEGIN 
				if @TempPoint >= @BalancePoint
				begin
				  set @TempPoint = @TempPoint - @BalancePoint
				  update CardPointDetail set BalancePoint = 0, [status] = 0, UpdatedBy = @CreatedBy, UpdatedOn = getdate() where KeyID = @KeyID 
				  set @CardPointDetailID = @KeyID             
				end       
				else if @TempPoint < @BalancePoint
				begin 
				  update CardPointDetail set BalancePoint = BalancePoint - @TempPoint, UpdatedBy = @CreatedBy, UpdatedOn = getdate()  where KeyID = @KeyID             
				  set @TempPoint = 0
				  set @CardPointDetailID = @KeyID
				end
				if @TempPoint <= 0 
				  break          
				FETCH NEXT FROM CUR_VOIDPOINTMULTIEXPIRE INTO @KeyID, @BalancePoint, @AmountExpiryDate
			  END
			  CLOSE CUR_VOIDPOINTMULTIEXPIRE  
			  DEALLOCATE CUR_VOIDPOINTMULTIEXPIRE      
			end                   
		  end
		end

		-- ver 1.0.0.20 清0操作。
        if @OprID = 66
		begin
		  update CardCashDetail set ForfeitAmount = ForfeitAmount + BalanceAmount, BalanceAmount = 0, 
				 UpdatedBy = @CreatedBy, UpdatedOn = getdate() 
				where KeyID = @KeyID
		  update CardPointDetail set [status] = 0, ForfeitPoint = @ForfeitPoint + BalancePoint, BalancePoint = 0, 
			 UpdatedBy = @CreatedBy, UpdatedOn = getdate() 
		  where CardNumber = @CardNumber
        end
	  end  

	  if @OprID = 24   -- 调整卡的有效期。
		set @NewCardExpiryDate = @ActCardExpiryDate
    
	  -- 更新Card值.
	  declare @TempDate datetime
	  select Top 1 @TempDate = ExpiryDate from CardCashDetail where CardNumber = @CardNumber order by ExpiryDate desc
	  if @TempDate > @NewAmountExpiryDate
		set @NewAmountExpiryDate = @TempDate
	  select Top 1 @TempDate = ExpiryDate from CardPointDetail where CardNumber = @CardNumber order by ExpiryDate desc
	  if @TempDate > @NewPointExpiryDate
		set @NewPointExpiryDate = @TempDate  
    
	  -- 执行更新Card表  
	  update card set TotalAmount = TotalAmount + @ActAmount, TotalPoints = TotalPoints + @Points, Status = @NewCardStatus, 
			CardExpiryDate = @NewCardExpiryDate, CardAmountExpiryDate = @NewAmountExpiryDate, CardPointExpiryDate = @NewPointExpiryDate, 
			UpdatedOn = Getdate(), CumulativeConsumptionAmt = isnull(CumulativeConsumptionAmt, 0) + @CumulativeConsumptionAmt, 
		CumulativeEarnPoints= isnull(CumulativeEarnPoints, 0) + case when isnull(@Points,0)>0 then @Points else 0 end,
		 MemberID = @MemberID
		where CardNumber = @CardNumber and CardTypeID = @CardTypeID
    
	   -- 更新 @CardCashDetailID, @CardPointDetailID 到 Card_Movement 
	   update Card_Movement set CardCashDetailID =@CardCashDetailID, CardPointDetailID = @CardPointDetailID, OpenPoint = @TotalPoints, 
			ClosePoint = @NewTotalPoints, OrgExpiryDate = @CardExpiryDate, NewExpiryDate = @NewCardExpiryDate, OrgStatus = @CardStatus, NewStatus = @NewCardStatus
 		 where KeyID = @Movementkeyid 

	   -- ver 1.0.0.11  发送消息
	   --exec GenBusinessMessage @OprID, @MemberID, @CardNumber, ''

	 END
  FETCH FROM CUR_Card_Movement INTO @Movementkeyid, @CardNumber, @ActAmount, @Points, @ActCardExpiryDate, 
         @OprID, @RefTxnNo, @Remark, @Remark, @RefReceiveKeyID, @Additional, @NewStatus  
END
CLOSE CUR_Card_Movement  
DEALLOCATE CUR_Card_Movement   

END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子增长主键。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺主键。receivetxn数据写入时，需要根据storecode+brandcode转换' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作ID。（见receiveTxn中OprID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果有相关联记录，则填写Card_movement 的关联记录的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'RefKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'填写关联的ReceiveTxn的KeyID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'RefReceiveKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'填写关联的Receivetxn的TxnNo。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'RefTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作之前卡余额。金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'OpenBal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作金额。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作之后卡的余额 （金额）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'CloseBal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作前积分值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'OpenPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作积分值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'Points'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作后积分值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'ClosePoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'Txndate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作前卡的有效期，见receivetxn中的OrgExpiryDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'OrgExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作前卡的有效期，见receivetxn中的NewExpiryDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'NewExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此操作前Card状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'OrgStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此操作后card状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'NewStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果是多有效期，则需要填写CardCashDetail表，这里填写CardCashDetail表的新增ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'CardCashDetailID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果是积分多有效期，则需要填写CardPointDetail表，这里填写CardPointDetail表的新增ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'CardPointDetailID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息。从其他银行卡转账时，记录银行卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用银行卡支付后返回的号码。可能用于记录SVA自己的approvecode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校验字段。 （预留）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement', @level2type=N'COLUMN',@level2name=N'SecurityCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card操作的流水表。
通过insert触发器，增加此表记录来操作Card表，CardCashDetail表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Card_Movement'
GO
