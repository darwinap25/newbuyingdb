USE [NewBuying]
GO
/****** Object:  Table [dbo].[PointRule_Item]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PointRule_Item](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[PointRuleCode] [varchar](64) NOT NULL,
	[EntityType] [int] NOT NULL,
	[EntityNum] [varchar](64) NOT NULL,
 CONSTRAINT [PK_POINTRULE_ITEM] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule_Item', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'积分规则编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule_Item', @level2type=N'COLUMN',@level2name=N'PointRuleCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'积分规则适用类型
1、EntityNum内容为ProductBrandCode
2、EntityNum内容为DepartCode
3、EntityNum内容为ProdCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule_Item', @level2type=N'COLUMN',@level2name=N'EntityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'积分规则适用编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule_Item', @level2type=N'COLUMN',@level2name=N'EntityNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'积分计算规则的货品条件。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PointRule_Item'
GO
