USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetProductOnhand]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProductOnhand]
  @StoreID                  INT,			          -- 店铺ID
  @ProdCode                 VARCHAR(64),		      -- 货品编码
  @ProdStyleCode            VARCHAR(64),		      -- 货品编码
  @StockTypeCode            VARCHAR(64),			  -- 库存类型
  @ConditionStr             NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	        NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent              INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize                 INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount                INT=0 OUTPUT,	          -- 返回总页数。
  @RecordCount              INT=0 OUTPUT,	          -- 返回总记录数。
  @LanguageAbbr			    VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN
AS
/****************************************************************************
**  Name : GetProductOnhand
**  Version : 1.0.0.0
**  Description : 获得货品的库存数量 (同一个product不会在不不同的productstyle中)
**
  declare  @a int, @PageCount int, @RecordCount int
  exec @a = GetProductOnhand 1,  '', '', 'G', '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
**  Created by Gavin @2016-07-26
**
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000)
  SET @StoreID = ISNULL(@StoreID, 0)
  SET @ProdCode = ISNULL(@ProdCode, '')
  SET @StockTypeCode = ISNULL(@StockTypeCode, '')
  SET @ProdStyleCode = ISNULL(@ProdStyleCode, '')
  
  SET @SQLStr = 'SELECT A.StoreID, A.ProdCode, A.StockTypeCode, A.OnhandQty, B.ProdCodeStyle FROM STK_StockOnhand A '
    + ' LEFT JOIN BUY_PRODUCTSTYLE B ON A.ProdCode = B.ProdCode '
    + ' WHERE (A.StoreID = ' + CAST(@StoreID AS VARCHAR) + ' or ' + CAST(@StoreID AS VARCHAR) + ' = 0)'
	+ ' AND (A.ProdCode = ''' + @ProdCode + ''' or ''' + @ProdCode + '''='''')'
	+ ' AND (A.StockTypeCode = ''' + @StockTypeCode + ''' or ''' + @StockTypeCode + '''='''')'
	+ ' AND (B.ProdCodeStyle = ''' + @ProdStyleCode + ''' or ''' + @ProdStyleCode + '''='''')'

  EXEC SelectDataInBatchs @SQLStr, 'ProdCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr

  RETURN 0
END

GO
