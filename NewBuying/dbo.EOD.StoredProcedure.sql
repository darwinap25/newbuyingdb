USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[EOD]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[EOD]
  @UserID int,
  @BusDate Date output --返回Business Date
AS
/****************************************************************************
**  Name : EOD
**  Version: 1.0.0.9
**  Description : EOD 过程，每日执行。 检查和更新Card 状态，
**
**  Parameter :
**         @UserID int    	-User code
**         @BusDate Date output --返回Business Date
**         return:  0: 修改成功. 非0为不成功，返回值可扩展。
exec EOD 1, '2015-04-07'
**  Created by:  Robin @2012-06-29
**  Modified By: Robin @2012-10-24
**  Modified By: Gavin @2014-06-11 (ver 1.0.0.2) 增加执行Coupon自动补货
**  Modified By: Gavin @2014-07-11 (ver 1.0.0.3) 增加执行DoDailyOperCouponOrder
**  Modified By: Gavin @2014-07-24 (ver 1.0.0.4) 会员的 密码输错次数清0
**  Modified By: Gavin @2014-11-12 (ver 1.0.0.5) 取消AutoGenCouponOrder, 改为执行AutoGenOrder_New
**  Modified By: Gavin @2014-11-19 (ver 1.0.0.6) 增加执行DoDailyInventoryCheck
**  Modified By: Gavin @2014-11-24 (ver 1.0.0.7) 移除DoDailyInventoryCheck（店铺订单批核，未产生picking单时，同步数据有错）
**  Modified By: Gavin @2015-04-24 (ver 1.0.0.8) 增加 自动产生 telco 单. 
**  Modified By: Gavin @2017-02-10 (ver 1.0.0.9) 增加调用BatchExecuteCardAdjust 
****************************************************************************/
begin
  
  declare @ApprovalCode char(6)
  declare @TxnDate datetime --Transaction Date

  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())
  set @TxnDate=getdate()
  
  exec GenApprovalCode @ApprovalCode output 

--修改过期Coupon的状态和金额
  insert into Coupon_Movement
			(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
			BusDate, Txndate, Remark, SecurityCode, CreatedBy, ApprovalCode, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
  select 42, CardNumber, CouponNumber, CouponTypeID,'',0,'',CouponAmount,-CouponAmount,0,
	@BusDate,@Txndate,'','',@UserID,@ApprovalCode, CouponExpiryDate, CouponExpiryDate, status, 4
	from Coupon where CouponExpiryDate<DateAdd(day,1,@BusDate) and [Status] in (1,2)
	
-- 同步Card和Coupon的onhand数量和record count  
  exec DoDailyInventoryCheck
  	
-- 生日赠送
  exec DoMemberBirthdayReward	
  
-- 积分即将过期 (for 711 版本)
  -- exec DoMemberPointExpireWarn
  
-- 印花自动转换 (for 711 版本)
  --exec AutoExchangeCoupon
  	
--修改过期卡的状态，金额，积分  Todo

-- 执行forfeit操作
  exec DoCouponForfeit @UserID, '', '', @BusDate, @TxnDate, @ApprovalCode, 0, '', '', 1 
  
-- 执行Coupon自动补货
--  exec AutoGenCouponOrder 1, '', '', null, null, 1
  
  -- 自动补货,包括Coupon和Card 
  exec AutoGenOrder_New  1, '', '', null, null, 1  
 
  -- TelCo的自动补货
  exec AutoTopUpTelcoCard  1, '', '', null, null, 1  

-- 执行CouponOrderForm 自动产生Picking单
  exec AutoGenCouponPicking '', 1
  
-- 会员的 密码输错次数清0
  update Member set PWDFailCount = 0 where PWDFailCount > 0

    
-- 执行定时执行的CardAdjust单。 
  exec BatchExecuteCardAdjust ''

--更新SODEOD表          
  update SODEOD set EOD=1 where EOD=0 and SOD=1
  insert into SODEOD (BusDate, SOD, EOD, CreatedOn) values (DateAdd(day,1,@BusDate),1,0,getdate())
  return 0
end


GO
