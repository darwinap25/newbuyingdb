USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberTxnEJ]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetMemberTxnEJ]   
  @TxnNo				varchar(512) 
AS
/****************************************************************************
**  Name : GetMemberTxnEJ
**  Version: 1.0.0.0
**  Description : 获得会员交易单的EJ内容.
**  Parameter :
**
declare @a int
exec @a = GetMemberTxnEJ 'KTXNNO000001133'
print @a
select top 1 SalesReceipt, cast(SalesReceipt as varchar) as SalesReceipt_Char from sales_h 
****************************************************************************/
begin
  select SalesReceipt, SalesReceiptBIN from sales_h where TransNum = @TxnNo
  if @@ROWCOUNT = 0
    return -11
  else  
    return 0
end

GO
