USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberGrade]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateMemberGrade]
  @MemberID  int
AS
/****************************************************************************
**  Name : UpdateMemberGrade
**  Version: 1.0.0.7
**  Description : 根据MemberImport 和 Member表数据，决定是否升级member card。 （hardcode 由 1 升级到 2）
   declare @A int
   exec @A=UpdateMemberGrade 
   print @A

**  Created by: Gavin @2015-09-17
**  Modify by Gavin @2015-10-28 (ver 1.0.0.1) 升级同时更新Member的UpdatedOn
**  Modify by Gavin @2015-11-02 (ver 1.0.0.2)  A.MemberMobilePhone = B.MemberMobilePhone or A.MemberRegisterMobile = B.MemberRegisterMobile 
**  Modify by Gavin @2015-11-06 (ver 1.0.0.3)  不更新Member数据
**  Modify by Gavin @2016-03-30 (ver 1.0.0.4) (for bauhaus) 增加测试代码
**  Modify by Gavin @2016-04-06 (ver 1.0.0.5) (for bauhaus) update card 时，同时update member 的updatedon
**  Modify by Gavin @2016-09-01 (ver 1.0.0.6) (for bauhaus) 去掉update member， 去掉 “ or @MemberID = 0” 的条件， 输入的memberID必须有值
**  Modify by Gavin @2017-04-26 (ver 1.0.0.7) (for bauhaus) 使用Card_movement的方式来更新Card表。 新加OprID=81 （CardGrade升级）
**
****************************************************************************/
begin
  DECLARE @CardGradeID int, @CardGradeUpdMethod int, @CardGradeUpdThreshold int, @CardGradeRank int
  DECLARE @CardValidityDuration int, @CardValidityUnit int, @NewCardExpiryDate datetime
    declare @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate date, @NewCouponExpiryDate date, @CouponAmount money, 
          @ApprovalCode varchar(6), @i int, @BusDate datetime, @TxnDate datetime, @CouponNumber varchar(64), @BindCouponCount int
         ,@BindCouponTypeID int , @CardNumber varchar(64)

  set @MemberID = ISNULL(@MemberID, 0)
  if @MemberID = 0
    return -1
 
  select @CardGradeID = CardGradeID, @CardGradeUpdMethod = CardGradeUpdMethod, 
         @CardGradeUpdThreshold = CardGradeUpdThreshold, @CardGradeRank = CardGradeRank,
         @CardValidityDuration = CardValidityDuration, @CardValidityUnit = CardValidityUnit 
       from cardGrade where CardGradeID = 2

          
  -- 取busdate
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 
  if isnull(@TxnDate, 0) = 0
    set @TxnDate = getdate()          

          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR
            select A.CardNumber, B.CouponTypeID, B.HoldCount from
            (
              select 2 as NextCardGradeID, * from Card where MemberID in
                (select A.MemberID from Member A left join MemberImport B 
	                  on A.MemberMobilePhone = B.MemberMobilePhone or A.MemberRegisterMobile = B.MemberRegisterMobile  
                  where B.MemberID is not null and (A.MemberID = @MemberID )                   
                ) and CardGradeID = 1
            ) A left join (select * from CardGradeHoldCouponRule where RuleType = 1 and CardGradeID = 2) B on A.NextCardGradeID = B.CardGradeID         
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN

            set @CouponNumber = ''
            set @i = 1
            while @i <= @BindCouponCount
            begin
              exec GetCouponNumber @BindCouponTypeID, 1, @CouponNumber output            
              if isnull(@CouponNumber, '') <> ''
              begin
                select @CouponExpiryDate = CouponExpiryDate, @CouponAmount = CouponAmount from Coupon where CouponNumber = @CouponNumber
                
                exec GenApprovalCode @ApprovalCode output
                exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                values(  
                      32, @CardNumber, @CouponNumber, @BindCouponTypeID, '', 0, '', @CouponAmount, 0, @CouponAmount,
                      @BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2)
              end
              set @i = @i + 1              
            end                         
            FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
          /*=====================================================*/  

   		if @CardValidityUnit = 1
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 2
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(mm, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 3
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(ww, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 4
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(dd, isnull(@CardValidityDuration, 0), Getdate()))))  
		else if @CardValidityUnit = 5    -- 不变更。
		  set @NewCardExpiryDate = 0
		else if isnull(@CardValidityUnit, 0) = 0  
      set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, 100, Getdate()))))  

                      
      INSERT INTO CARD_MOVEMENT
        (OPRID, CARDNUMBER, REFKEYID, REFRECEIVEKEYID, REFTXNNO, OPENBAL, AMOUNT, CLOSEBAL, POINTS, BUSDATE, TXNDATE, 
         CARDCASHDETAILID, CARDPOINTDETAILID, ADDITIONAL, REMARK, SECURITYCODE, CREATEDBY, STOREID, REGISTERCODE, SERVERCODE,
	     OPENPOINT, CLOSEPOINT, ORGEXPIRYDATE, NEWEXPIRYDATE, ORGSTATUS, NEWSTATUS)
      select 81, C.CardNumber, NULL, 0, '', C.TotalAmount, 0, C.TotalAmount, 0, Getdate(), Getdate(), 
         NULL, NULL, C.MemberID, NULL, '', C.MemberID, NULL, NULL, NULL,
          C.TotalPoints, C.TotalPoints,  C.CardExpiryDate, C.CardExpiryDate, C.Status, C.Status
	   from Card C 
	    left join Member M on C.MemberID = M.MemberID
		left join memberimport I on  M.membermobilephone = I.membermobilephone or M.memberregistermobile = I.memberregistermobile  
	   where I.MemberID is not null and (M.MemberID = @MemberID)

--if not exists (select 1 from  sysobjects where  id = object_id('UpdateMemberGrade_Log') and   type = 'U')
--create table UpdateMemberGrade_Log (
--   keyid              int                  identity(1,1) ,
--   MemberID           int                  null,
--   Context            varchar(100)         null,
--   CreatedOn          datetime             null default getdate()
--)

  
/*
  update Member set UpdatedOn = Getdate() 
  where MemberID in
  (
	  select MemberID from Card where MemberID in
	  (
		select A.MemberID from Member A left join MemberImport B 
		  on A.MemberMobilePhone = B.MemberMobilePhone or A.MemberRegisterMobile = B.MemberRegisterMobile  
		where B.MemberID is not null and (A.MemberID = @MemberID or @MemberID = 0)
	  ) 
	  and CardGradeID = 101
  )
*/

/*
  Update Card set CardGradeID = 102, CardExpiryDate = case when @NewCardExpiryDate = 0 then CardExpiryDate else @NewCardExpiryDate end where MemberID in
  (
    select A.MemberID from Member A left join MemberImport B 
	  on A.MemberMobilePhone = B.MemberMobilePhone or A.MemberRegisterMobile = B.MemberRegisterMobile  
    where B.MemberID is not null and (A.MemberID = @MemberID)
  ) 
  and CardGradeID = 101


  if @@ROWCOUNT = 0
  begin
    insert into UpdateMemberGrade_Log(MemberID, Context)
	values(@MemberID, 'Do UpdateMemberGrade fail')  
  end else
  begin
    insert into UpdateMemberGrade_Log(MemberID, Context)
	values(@MemberID, 'Do UpdateMemberGrade fail ok')  
  end
*/
end

GO
