USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCardForfeit]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[DoCardForfeit]
  @UserID			varchar(512),
  @CardNumber		varchar(512),    -- 如果不为空，则指定的Card做forfeit. 否则就是所有的激活状态的（active，redeem）Card， 都做forfeit操作。
  @RefTxnNo			varchar(512),
  @BusDate			datetime,
  @TxnDate			datetime,
  @ApprovalCode	varchar(6),
  @StoreID			int, 
  @ServerCode		varchar(512), 
  @RegisterCode		varchar(512),
  @AmountOrPoint  int=1         -- 1:处理积分。 2：处理金额。 3：两者都处理
AS
/****************************************************************************
**  Name : DoCardForfeit
**  Version: 1.0.0.0
**  Description : DoCardForfeit 过程，每日执行。 检查和更新Card 的过期积分和金额
**  exec DoCardForfeit 1, '', '', getdate(), getdate(), '', 0, '', '', 1
**  Created by:  Gavin @2014-10-10
****************************************************************************/
begin
  declare @CardTypeID int,  @CardGradeID int,@CouponforfeitControl int, @CardAmount money, @CardPoints int, @ForfeitTxnNo varchar(50)
  declare @CardStatus int, @NewCardStatus int, @CardExpiryDate datetime, @NewCardExpiryDate datetime
  declare @ExpiryAmount money, @ExpiryPoint int
  
  if isnull(@RefTxnNo, '') = ''
  begin
    exec GetRefNoString 'FORFCA', @ForfeitTxnNo output   
    set @RefTxnNo = @ForfeitTxnNo
  end  
  if isnull(@ApprovalCode, '') = ''  
    exec GenApprovalCode @ApprovalCode output 

  select @CardTypeID = CardTypeID, @CardGradeID = CardGradeID, @CardAmount = TotalAmount, 
      @CardPoints = TotalPoints, @CardStatus = status, @CardExpiryDate = CardExpiryDate
    from Card where CardNumber = @CardNumber and Status in (2,3,4)
  
  if isnull(@CardTypeID, 0) > 0 and ISNULL(@CardNumber, '') <>'' -- 单个Card做forfeit。
  begin
    select @ExpiryAmount = SUM(ISNULL(BalanceAmount, 0)) from CardCashDetail where CardNumber = @CardNumber and DATEDIFF(DD, ExpiryDate, getdate()) > 0
    select @ExpiryPoint = SUM(ISNULL(BalancePoint, 0)) from CardPointDetail where CardNumber = @CardNumber and DATEDIFF(DD, ExpiryDate, getdate()) > 0  
    if @AmountOrPoint = 1
      set @ExpiryAmount = 0
    else if @AmountOrPoint = 2
      set @ExpiryPoint = 0 
    set @ExpiryAmount = isnull(@ExpiryAmount, 0)        
    set @ExpiryPoint = isnull(@ExpiryPoint, 0)   
    insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, 
           BusDate, Txndate, TenderID, CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, 
           CreatedBy, StoreID, ServerCode, RegisterCode, OrgStatus, NewStatus, OrgExpiryDate, NewExpiryDate)  
    values
      (63, @CardNumber, null, null, @RefTxnNo, @CardAmount, -@ExpiryAmount, @CardAmount-@ExpiryAmount, -@ExpiryPoint,
       @BusDate, @TxnDate, null, null, null, '', 'Do DoCardForfeit', '', 
       @UserID, @StoreID, @ServerCode, @RegisterCode, @CardStatus, @CardStatus, @CardExpiryDate, @CardExpiryDate)  
  end else if isnull(@CardNumber, '') = '' 
  begin
    insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, 
           BusDate, Txndate, TenderID, CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, 
           CreatedBy, StoreID, ServerCode, RegisterCode, OrgStatus, NewStatus, OrgExpiryDate, NewExpiryDate)  
    select
      63, C.CardNumber, null, null, @RefTxnNo, TotalAmount, ISNULL(-ExpAmt,0), TotalAmount-isnull(ExpAmt,0), -isnull(ExpPoint,0),
       @BusDate, @TxnDate, null, null, null, '', 'Do DoCardForfeit', '', 
       @UserID, @StoreID, @ServerCode, @RegisterCode, Status, Status, CardExpiryDate, CardExpiryDate
    from Card C 
      left join (select CardNumber, SUM(ISNULL(BalanceAmount, 0)) as ExpAmt 
                   from CardCashDetail where DATEDIFF(DD, ExpiryDate, getdate()) > 0 group by CardNumber) A on A.CardNumber = C.CardNumber
      left join (select CardNumber, SUM(ISNULL(BalancePoint, 0)) as ExpPoint
                   from CardPointDetail where DATEDIFF(DD, ExpiryDate, getdate()) > 0 group by CardNumber) B on B.CardNumber = C.CardNumber
    where (isnull(A.ExpAmt, 0) > 0 and @AmountOrPoint in (2,3)) or (isnull(B.ExpPoint, 0) > 0 and @AmountOrPoint in (1,3))
  end
  return 0
end

GO
