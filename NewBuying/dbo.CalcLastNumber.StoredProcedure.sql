USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcLastNumber]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CalcLastNumber]
  @Type                  int,                -- 1: @FirstNumber是 CardNUmber。 2：@FirstNumber是CouponNumber  
  @Qty                   int,                -- 连续数量 
  @FirstNumber           varchar(64),        -- 开始号码
  @EndNumber             varchar(64) output  -- 结束号码
AS
/****************************************************************************
**  Name : CalcLastNumber
**  Version: 1.0.0.0
**  Description : 根据Card或者Coupon的 Number的生成规则，输入起始号码和数量,返回结束号码

  declare @EndNumber varchar(64)
    exec CalcLastNumber 2, 3, '1234500002094', @EndNumber output
  print @EndNumber  

** 
**  Created by:  Gavin @2014-08-22
****************************************************************************/
begin
  declare @NumberMask varchar(64), @NumberPattern varchar(30), @PatternLen int
  declare @NumberHead varchar(100), @NumberSeqNo varchar(100), @NumberSeqNoLength int
  declare @Checkdigit int, @FirstSeqNoStr varchar(64), @EndSeqNoStr varchar(64), @FirstSeqNo bigint 
  declare @ModeID int, @ModeCode varchar(64) 
  
  if @Type = 1
  begin
    select @NumberMask = case when isnull(G.CardNumMask, '') = '' then T.CardNumMask else G.CardNumMask end, 
       @NumberPattern = case when isnull(G.CardNumPattern, '') = '' then T.CardNumPattern else G.CardNumPattern end,
       @Checkdigit =  case when isnull(G.CardCheckdigit, -1) = -1 then T.CardCheckdigit else G.CardCheckdigit end,
       @ModeID = case when isnull(G.CheckDigitModeID, -1) = -1 then T.CheckDigitModeID else G.CheckDigitModeID end
      from Card C 
      Left join CardType T on C.CardTypeID = T.CardTypeID 
      left join CardGrade G on C.CardGradeID = G.CardGradeID
     where C.CardNumber = @FirstNumber
  end
  else
  begin
    select @NumberMask = T.CouponNumMask, @NumberPattern = T.CouponNumPattern,
       @Checkdigit = T.CouponCheckdigit, @ModeID = T.CheckDigitModeID
      from Coupon C 
      Left join CouponType T on C.CouponTypeID = T.CouponTypeID 
     where C.CouponNumber = @FirstNumber  
  end 
 
  select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
  set @ModeCode = isnull(@ModeCode, 'EAN13')
       
  set @PatternLen = 0
  while @PatternLen < Len(@NumberMask)
  begin      
    if substring(@NumberMask, @PatternLen + 1, 1) = 'A'
      set @PatternLen = @PatternLen + 1     
    else  
      break  
  end      

print @FirstNumber
print @PatternLen
print @NumberMask
print @Checkdigit
  if @Checkdigit = 1 
    set @FirstSeqNoStr = substring(@FirstNumber, @PatternLen + 1,  Len(@NumberMask) - @PatternLen - 1) 
  else  
    set @FirstSeqNoStr = substring(@FirstNumber, @PatternLen + 1,  Len(@NumberMask) - @PatternLen)

  set @FirstSeqNo=Convert(bigint, @FirstSeqNoStr)       
  set @FirstSeqNo = isnull(@FirstSeqNo, 0) + @Qty - 1
  set @EndSeqNoStr = Convert(varchar(30), @FirstSeqNo)
             
  if @Checkdigit = 1
  begin
    select @EndNumber = substring(@NumberPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + @EndSeqNoStr, Len(@NumberMask) - @PatternLen - 1)
    select @EndNumber = dbo.CalcCheckDigit(@EndNumber, @ModeCode)
  end else  
    select @EndNumber = substring(@NumberPattern, 1, @PatternLen) + Right('0000000000000000000000000000000000000000000000000000000000000000' + @EndSeqNoStr, Len(@NumberMask) - @PatternLen)

  return 0
end

GO
