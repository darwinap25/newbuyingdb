USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponPicking_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponPicking_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponPickingNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NULL,
	[Description] [nvarchar](512) NULL,
	[OrderQTY] [int] NULL,
	[PickQTY] [int] NULL,
	[ActualQTY] [int] NULL,
	[FirstCouponNumber] [varchar](64) NULL,
	[EndCouponNumber] [varchar](64) NULL,
	[BatchCouponCode] [varchar](64) NULL,
	[PickupDateTime] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_ORD_COUPONPICKING_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Delete_Ord_CouponPicking_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Delete_Ord_CouponPicking_D] ON [dbo].[Ord_CouponPicking_D]
FOR DELETE
AS
/*==============================================================*/
/*                
* Name: Delete_Ord_CouponPicking_D
* Version: 1.0.1.0
* Description : Coupon订单表的批核触发器
*  select * from Ord_CouponPicking_H where CouponPickingNumber = 'COPO00000000173'
 update Ord_CouponPicking_H set ApproveStatus = 'A' where CouponPickingNumber = 'COPO00000000173'
* Create By Gavin @2012-09-21
* Modify by Gavin @2014-04-14 (ver 1.0.1.0) 实体Coupon订单表结构修改, 存储过程做相应修改。(增加StockStatus)
*/
/*==============================================================*/
BEGIN  
  declare @CouponOrderFormNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @FirstCoupon varchar(64), @EndCoupon varchar(64), @CouponTypeID int
  
    DECLARE CUR_PickupCoupon_Detail CURSOR fast_forward for
      select FirstCouponNumber, EndCouponNumber, CouponTypeID from Deleted
	OPEN CUR_PickupCoupon_Detail
    FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
    WHILE @@FETCH_STATUS=0 
    BEGIN 
      update coupon set PickupFlag = 0, StockStatus = 2 where CouponNumber >= @FirstCoupon and  CouponNumber <= @EndCoupon and CouponTypeID = @CouponTypeID
	  FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
	END
    CLOSE CUR_PickupCoupon_Detail 
    DEALLOCATE CUR_PickupCoupon_Detail 
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'CouponPickingNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'OrderQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要求拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'PickQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'ActualQTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的首coupon号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'FirstCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际拣货批次的末coupon号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'EndCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstCouponNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'BatchCouponCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建此明细的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D', @level2type=N'COLUMN',@level2name=N'PickupDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵拣货单子表
表中brandID，CouponTypeID关系不做校验，由UI在创建单据时做校验。
存储过程实际操作时，只按照CouponTypeID来做。
（实际拣货首号码必须填，尾号码可以根据数量来计算，具体根据业务需求来做）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_D'
GO
