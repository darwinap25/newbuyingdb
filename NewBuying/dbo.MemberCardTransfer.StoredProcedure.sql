USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardTransfer]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[MemberCardTransfer]
  @UserID		        varchar(512),			-- 登录用户
  @CardNumber			varchar(512),			-- 会员卡号  
  @TargetCardNumber		nvarchar(512),			-- 目标会员卡号 （调用方需要拿到卡号，如果是新用户，调用方需要创建新卡）  
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),            -- 终端号码
  @ServerCode           varchar(512)='',			-- serverid 暂时用不到,预留
  @BrandCode			varchar(512),			-- 品牌编码
  @TxnNoKeyID           varchar(512),			-- 交易唯一号码.
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间
  @OprID		        int,					-- 操作类型
  @ActAmount		    money,					-- 操作金额
  @ActPoint		        int,					-- 操作积分  
  @ActCouponNumbers		varchar(max),           -- 操作coupon  
  @SecurityCode         varchar(512),			-- 签名
  @Additional           varchar(512),			-- 附加信息
  @Remark				nvarchar(1000),			-- 注释。（或者transfer原因）
  @SourceCardAmount     money output,			-- 转出卡的余额
  @SourceCardPoints     int output,			    -- 转出卡的积分余额
  @TargetCardAmount     money output,			-- 转入卡的余额
  @TargetCardPoints     int output,			  -- 转入卡的积分余额 
  @TradeInFlag          int = 0,          -- 以旧换新标志. null/0: 按照指定条件转移金额积分coupon.  1: 整个卡合并到另外一个卡.  2: 整个卡复制到另外一个卡.(新卡内的金额等价值 等于旧卡,即新卡先清空) 
  @CloseOutCard         int = 0           -- 是否要关闭转出的卡. @TradeInFlag 大于 0 时有效.
as
/****************************************************************************
**  Name : MemberCardTransfer  
**  Version: 1.0.0.4
**  Description : 卡中金额,或者积分,或者Coupon,转移到其他的卡中, 只判断转出卡中是否有足够的值, 转入卡中是否超出上限, 以及转出转入卡状态
                  不判断业务逻辑（比如不同卡类别之间是否允许转赠，）。(传入@Oprid为6,16,36，@Oprid只作为记录操作类型以供查询，实际操作动作根据Act 的参数)
**  Parameter :
  declare
    @Result int	, @SourceCardAmount money,  @TargetCardAmount money, @SourceCardPoints int, @TargetCardPoints int
  exec @Result = MemberCardTransfer '1', '000400001', '000400002', 'store1', '001', '01', '201205010100001', '2012050101', '2012-04-26', '2012-04-26', 6, -70, 0, '',   '', '','',
     @SourceCardAmount output, @TargetCardAmount output, @SourceCardPoints output, @TargetCardPoints output
  print @Result    
  print @SourceCardAmount
  print @SourceCardPoints
  print @TargetCardAmount
  print @TargetCardPoints   
  select * from coupon
   select * from coupon_movement
**  return: 

**  Created by: Gavin @2012-04-01
**  Modify by: Gavin @2014-10-08 (Ver 1.0.0.1) 旧卡换新卡时(OprID=16)
**  Modify by: Gavin @2014-11-21 (Ver 1.0.0.2) 增加参数@CloseOutCard,重新定义@TradeInFlag. @TradeInFlag >= 1 时可以不填act 值
**  Modify by: Gavin @2015-02-11 (Ver 1.0.0.3) 增加Telco 卡的 转账.(店铺和总部之间)
**  Modify by: Gavin @2015-04-08 (Ver 1.0.0.4) 使用card 的 issuestoreid, 写入card_movement的 storeID
**
****************************************************************************/
begin
  declare @OprID_1 int  -- 对应的OprID. 
  set @OprID_1 = @OprID + 1
  declare @OpenBal money, @CloseBal money, @TargetOpenBal money, @TargetCloseBal money, @TargetCardGradeID int, @MaxAmt money, @CardGradeID int
  declare @OpenPoints int, @TargetOpenPoints int, @SQLStr nvarchar(4000), @SQLStr1 nvarchar(4000), @CouponNumber varchar(512), @CouponTypeID int
  declare @CardPointTransfer int, @CardAmountTransfer int, @MinAmountPreTransfer money, @MaxAmountPreTransfer money, @MinPointPreTransfer int, @MaxPointPreTransfer int, @DayMaxAmountTransfer money, @DayMaxPointTransfer int
  declare @BrandID int, @TargetBrandID int, @CardIssuedID int, @TargetCardIssuedID int, @CardTypeID int, @TargetCardTypeID int
  declare @StoreID int
  declare @CouponStatus int, @CouponExpiryDate datetime
  declare @issuestoreid int, @Tagetissuestoreid int
  
  -- 操作数总是负数。
  set @ActAmount = -abs(@ActAmount)
  set @ActPoint = -abs(@ActPoint)
  select @CardTypeID = CardTypeID, @OpenBal = isnull(TotalAmount,0), @OpenPoints = isnull(TotalPoints, 0), @CardGradeID = CardGradeID, @issuestoreid = issuestoreid from Card where cardNumber = @CardNumber
  select @TargetCardTypeID = CardTypeID, @TargetOpenBal = isnull(TotalAmount,0), @TargetOpenPoints = isnull(TotalPoints, 0), @TargetCardGradeID = CardGradeID, @Tagetissuestoreid = issuestoreid from Card where cardNumber = @TargetCardNumber 
  select @BrandID = BrandID from CardType where CardTypeID = @CardTypeID
  select @TargetBrandID = BrandID from CardType where CardTypeID = @TargetCardTypeID
  select @CardIssuedID = CardIssuerID from Brand where StoreBrandID = @BrandID
  select @TargetCardIssuedID = CardIssuerID from Brand where StoreBrandID = @TargetBrandID 
  select @StoreID = S.StoreID from Store S left join Brand B on S.BrandID = B.StoreBrandID where S.StoreCode = @StoreCode and B.StoreBrandCode = @BrandCode
  
  -- ver 1.0.0.2 ---
  if @TradeInFlag >= 1   -- 整个卡移动, 可以不用外部传入的act数据, 自己来读取
  begin
    select @ActAmount = TotalAmount, @ActPoint = TotalPoints from Card where CardNumber = @CardNumber    
  end
  ------------------
  
  -- ver 1.0.0.1 如果是以旧换新，那么新卡初始金额，积分，coupon都要清空。 --ver 1.0.0.2  条件改为2
  if @TradeInFlag = 2
  begin
    set @TargetOpenBal = 0
    set @TargetOpenPoints = 0
    update Coupon set CardNumber = '', Status = 0 where CardNumber = @TargetCardNumber  
  end
  -----------------------
   
  select @CardPointTransfer = CardPointTransfer, @CardAmountTransfer = CardAmountTransfer, @MinAmountPreTransfer = MinAmountPreTransfer, @MaxAmountPreTransfer = MaxAmountPreTransfer, 
		@MinPointPreTransfer = MinPointPreTransfer, @MaxPointPreTransfer = MaxPointPreTransfer
    from CardGrade where CardTypeID = @CardTypeID and CardGradeID = @CardGradeID
  select @MaxAmt = isnull(CardGradeMaxAmount,0) from CardGrade where CardTypeID = @TargetCardTypeID and CardGradeID = @TargetCardGradeID
  set @CloseBal = @OpenBal + isnull(@ActAmount,0)
  set @TargetCloseBal = @TargetOpenBal - isnull(@ActAmount,0)
  if @CloseBal < 0 
    return -37
  if @OpenPoints + isnull(@ActPoint, 0) < 0 
    return -38    
  if @TargetCloseBal > @MaxAmt
    return -14
  if (abs(@ActAmount) > 0) and (@OprID <> 46) 
  begin
	if @CardAmountTransfer = 0 
	  return -50
	if (@CardAmountTransfer = 1) and (@CardGradeID <> @TargetCardGradeID)
	  return -51	
	if (@CardAmountTransfer = 2) and (@CardTypeID <> @TargetCardTypeID)
	  return -52
	if (@CardAmountTransfer = 3) and (@BrandID <> @TargetBrandID)
	  return -53
	if (@CardAmountTransfer = 4) and (@CardIssuedID <> @TargetCardIssuedID)
	  return -54	 	  	     	 
  end  
  if (abs(@ActPoint) > 0) and (@OprID <> 46)
  begin
	if @CardPointTransfer = 0 
	  return -50
	if (@CardPointTransfer = 1) and (@CardGradeID <> @TargetCardGradeID)
	  return -51	
	if (@CardPointTransfer = 2) and (@CardTypeID <> @TargetCardTypeID)
	  return -52
	if (@CardPointTransfer = 3) and (@BrandID <> @TargetBrandID)
	  return -53
	if (@CardPointTransfer = 4) and (@CardIssuedID <> @TargetCardIssuedID)
	  return -54	 	  	     	 
  end   
    
  set @SourceCardAmount = @CloseBal 
  set @TargetCardAmount = @TargetCloseBal 
  set @SourceCardPoints = @OpenPoints + isnull(@ActPoint, 0)
  set @TargetCardPoints = @TargetOpenPoints - isnull(@ActPoint, 0)  
  
  if @OprID in (6, 16, 46, 48)
  begin
    -- 扣除原卡
    insert into Card_Movement
      (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
       CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, ServerCode, RegisterCode)
    values
      (@OprID, @CardNumber, null, 0, @TxnNo, @OpenBal, @ActAmount, @CloseBal, @ActPoint, @BusDate, @TxnDate, 
       null, null, null, @Remark, @SecurityCode, @UserID, @issuestoreid, @ServerCode, @RegisterCode)
    -- 加入目的卡
    insert into Card_Movement
      (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
       CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, ServerCode, RegisterCode)
    values
      (@OprID_1, @TargetCardNumber, null, 0, @TxnNo, @TargetOpenBal, -@ActAmount, @TargetCloseBal, -@ActPoint, @BusDate, @TxnDate, 
       null, null, null, @Remark, @SecurityCode, @UserID, @Tagetissuestoreid, @ServerCode, @RegisterCode)
  end                  
  if @OprID in (16, 36)
  begin 
    if object_id('TempDB..##CouponNumberList') is not null
      drop table ##CouponNumberList  
      
    -- ver 1.0.0.2 ------------  
    if @TradeInFlag >= 1
    begin
      select CouponNumber, CouponTypeID, CouponExpiryDate, status into ##CouponNumberList from coupon 
        where CardNumber = @CardNumber     
    end else
    begin    
      set @SQLStr = N'select CouponNumber, CouponTypeID, CouponExpiryDate, status into ##CouponNumberList from coupon where CouponNumber in ( ' + @ActCouponNumbers + ')'
      set @SQLStr1 = @SQLStr    
      exec sp_executesql @SQLStr1
    end    
    ---------------------------
            
    DECLARE CUR_TransferCoupon CURSOR fast_forward local FOR
      select CouponNumber, CouponTypeID, CouponExpiryDate, status from ##CouponNumberList      
    OPEN CUR_TransferCoupon
    FETCH FROM CUR_TransferCoupon INTO @CouponNumber, @CouponTypeID, @CouponExpiryDate, @CouponStatus
    WHILE @@FETCH_STATUS=0
    BEGIN
      -- 解除指定的Coupon的绑定
      insert into Coupon_Movement
        (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
         BusDate, Txndate, Remark, SecurityCode, CreatedBy, StoreID, ServerCode, RegisterCode, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      values
        (@OprID, @CardNumber, @CouponNumber, @CouponTypeID, '', 0, @TxnNo, 0, 0, 0,
         @BusDate, @TxnDate, @Remark,@SecurityCode, @UserID, @issuestoreid, @ServerCode, @RegisterCode, @CouponExpiryDate, @CouponExpiryDate, @CouponStatus, @CouponStatus)           
      -- 绑定coupon      
      insert into Coupon_Movement
        (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
         BusDate, Txndate, Remark, SecurityCode, CreatedBy, StoreID, ServerCode, RegisterCode, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      values
        (@OprID_1, @TargetCardNumber, @CouponNumber, @CouponTypeID, '', 0, @TxnNo, 0, 0, 0,
         @BusDate, @TxnDate, '',@SecurityCode, @UserID, @Tagetissuestoreid, @ServerCode, @RegisterCode, @CouponExpiryDate, @CouponExpiryDate, @CouponStatus, @CouponStatus)
      FETCH FROM CUR_TransferCoupon INTO @CouponNumber, @CouponTypeID, @CouponExpiryDate, @CouponStatus
    END
    CLOSE CUR_TransferCoupon 
    DEALLOCATE CUR_TransferCoupon        
    DROP table ##CouponNumberList
  end  
  
  return 0      
end

GO
