USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardExchangeCoupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[MemberCardExchangeCoupon]
  @UserID		        varchar(512),
  @CardNumber			varchar(512),			-- 会员卡号
  @CouponNumber			varchar(512),			-- 使用的coupon号码
  @CouponTypeCode		varchar(512),			-- 使用的coupontypeID
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),            -- 终端号码
  @ServerCode           varchar(512)='',			-- serverid 暂时用不到,预留
  @BrandCode			varchar(512)='',			-- 品牌编码
  @TxnNoKeyID           varchar(512),			-- ReceiveTxn 表主键
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间  
  @SecurityCode         varchar(512),			-- 签名
  @Additional           varchar(512),			-- 附加信息    
  @ReturnCouponTypeAmount money out,            -- 返回此coupon的金额
  @ReturnCouponTypePoint  int out,              -- 返回此coupon的积分
  @ReturnCouponTypeDiscount float out,          -- 返回此coupon的折扣额
  @ReturnBindPLU        varchar(20) output		-- 返回此coupon的绑定的PLU
as
/****************************************************************************
**  Name : MemberCardExchangeCoupon  
**  Version: 1.0.0.7
**  Description : 使用coupon
**  Parameter :
  declare @ReturnCouponTypeAmount money, @ReturnCouponTypePoint int, @ReturnCouponTypeDiscount float, @ReturnBindPLU varchar(20), @Result int	    
   exec @Result = MemberCardExchangeCoupon '1', '000100086', '05000100', '', '2','001', '01', '123123', '201204010100001', '20120401010', '2012-03-26', '2012-03-26', '', '', @ReturnCouponTypeAmount output, @ReturnCouponTypePoint output, @ReturnCouponTypeDiscount OUTPUT, @ReturnBindPLU output
  print @Result   
  print @ReturnCouponTypeAmount   
  print @ReturnCouponTypePoint
  print @ReturnCouponTypeDiscount
  print @ReturnBindPLU
 
 update coupon set couponstatus = 1, memberid = 1, parentCard = '000400001', parentcardtypeid = '1'  where couponnumber = 'CN0100001'
 
**  return: 0: 成功。  -1：按照memberid，cardnumber，cardtypeid 没有找到card。
**  Created by: Gavin @2012-03-09
**  Modify by: Gavin @2012-07-20 (ver 1.0.0.1) 增加店铺条件.(coupon是否只允许发行店铺使用， coupon是否设置了使用店铺条件)
**  Modify by: Gavin @2012-08-28 (ver 1.0.0.2) 去掉demo代码，必须要输入couponnumber
**  Modify by: Gavin @2012-10-22 （ver 1.0.0.3）不传入Busdate，由存储过程自己获取
**  Modify By: Gavin @2012-11-21 (ver 1.0.0.4) 修正计算Coupon New expirydate后,填写到couponmovement的错误.
**  Modify By: Gavin @2013-08-02 (ver 1.0.0.5) Coupon没有设置storeid时，跳过storeid校验
**  Modify by: Gavin @2015-06-08 (ver 1.0.0.6) 使用Coupon后发送消息(OprID=517).
**  Modify by: Gavin @2015-12-25 (ver 1.0.0.7) 允许没有卡号输入, 即 redeem 非绑定Coupon
**
****************************************************************************/
begin
  declare @CardExpiryDate datetime, @MemberID varchar(36), @CardGradeID nvarchar(10), @CardTypeID int, @CouponTypeID int,
          @BatchID varchar(30), @TotalPoints int, @TotalAmount money, @CardStatus int,
          @CouponStatus int, @CouponExpiryDate datetime, @OprID int, @StoreID int, @CouponIssueStoreID int, @CouponRedeemStoreID int
  declare @ApprovalCode varchar(6), @LocateStore int, @NewCouponStatus int, @NewCouponExpiryDate date, @MsgCardBrandID int
/*          
  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
    return @CheckResult
  -- end
*/
  set @CardNumber = isnull(@CardNumber, '')

  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
      
  select @StoreID = StoreID from Store S left join Brand B on S.BrandID = B.StoreBrandID
    where S.StoreCode = @StoreCode and B.StoreBrandCode = @BrandCode
  select top 1 @CardNumber = CardNumber, @CardTypeID = C.CardTypeID,  @CardStatus = C.Status, @CardExpiryDate = CardExpiryDate, 
       @CardGradeID = CardGradeID, @TotalAmount = isnull(TotalAmount, 0), @TotalPoints = isnull(TotalPoints, 0),
       @MemberID = MemberID, @MsgCardBrandID = T.BrandID
    from Card C left join CardType T on C.CardTypeID = T.CardTypeID
    where CardNumber = @CardNumber


---  if @@ROWCOUNT = 0      -- remove -- 1.0.0.7
--    return -2


  -- for demo 当前Retailx POS无法传入具体的couponnumber,coupontypeid或者coupon数量，所以使用coupon时，把这个卡下面的所有coupon一次性全部使用。
  -- demo 允许接受不输入coupontypecode 和 couponnumber的 方式。
--  if inull(@CouponTypeCode, '') = ''
--  begin
--    update Coupon set Status = 3, UpdatedOn = Getdate() where CardNumber = @CardNumber and Status = 2
--    return 0   
--  end else if isnull(@CouponNumber, '') = ''
--  begin
--    select @CouponTypeID = CouponTypeID from CouponType where CouponTypeCode = @CouponTypeCode
--    update Coupon set Status = 3, UpdatedOn = Getdate() where CardNumber = @CardNumber and CouponTypeID = @CouponTypeID and Status = 1
--    return 0     
--  end else
--  begin
    select @CouponStatus = C.Status, @CouponExpiryDate = C.CouponExpiryDate, @ReturnCouponTypeAmount = T.CouponTypeAmount, 
        @ReturnCouponTypePoint = T.CouponTypePoint, @ReturnCouponTypeDiscount = T.CouponTypeDiscount, @CouponTypeID = C.CouponTypeID,
        @LocateStore = T.LocateStore, @CouponIssueStoreID = C.StoreID, @CouponRedeemStoreID = C.RedeemStoreID
      from Coupon C left join CouponType T on C.CouponTypeID = T.CouponTypeID
     where C.CouponNumber = @CouponNumber
    if @@ROWCOUNT = 0
      return -19
           
     -- 检查coupon是否可以在此店铺使用
   if isnull(@CouponIssueStoreID, 0) <> 0
   begin
	   if @LocateStore = 1 and @CouponIssueStoreID <> @StoreID
	     return -28
	   if exists(select * from CouponTypeStoreCondition_List where StoreConditionType = 2 and CouponTypeID = @CouponTypeID)  
	   begin
	  	if not exists(select * from CouponTypeStoreCondition_List where StoreConditionType = 2 and CouponTypeID = @CouponTypeID and StoreID = @StoreID)
	  	  return -28
	   end
   end	 
-- END

    if @CouponStatus <> 2
      return -20
    if datediff(dd, @CouponExpiryDate, Getdate()) <= 0
    begin
      set @OprID = 34
      exec GenApprovalCode @ApprovalCode output
	  exec CalcCouponNewStatus @CouponNumber, @CouponTypeID, @OprID, @CouponStatus, @NewCouponStatus output
	  exec CalcCouponNewExpiryDate @CouponTypeID, @OprID, @CouponExpiryDate, @NewCouponExpiryDate output           
      insert into Coupon_Movement
		(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
		 BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
         OrgExpirydate, OrgStatus, NewStatus)             
      values
        (@OprID, @CardNumber, @CouponNumber, @CouponTypeID, '', 0, @TxnNo, @ReturnCouponTypeAmount, -@ReturnCouponTypeAmount, 0,
         @BusDate, @TxnDate, '',@SecurityCode, @UserID, @NewCouponExpiryDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode,
         @CouponExpiryDate, @CouponStatus, @NewCouponStatus)   
         
   -- 增加消息 ?
     exec GenBusinessMessage 517, @MemberID, @CardNumber, @CouponNumber, @CardTypeID, @CardGradeID, @MsgCardBrandID,  @CouponTypeID, ''   
    ---  ---------  
             
      return 0   
    end else
      return -21     
--  end
end  

GO
