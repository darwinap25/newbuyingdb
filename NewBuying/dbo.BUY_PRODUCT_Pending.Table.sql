USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PRODUCT_Pending]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PRODUCT_Pending](
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[ProdDesc1] [nvarchar](512) NULL,
	[ProdDesc2] [nvarchar](512) NULL,
	[ProdDesc3] [nvarchar](512) NULL,
	[ProdPicFile] [nvarchar](512) NULL,
	[ScanDesc1] [nvarchar](512) NULL,
	[ScanDesc2] [nvarchar](512) NULL,
	[ScanDesc3] [nvarchar](512) NULL,
	[ProductBrandCode] [varchar](64) NULL,
	[StoreBrandCode] [varchar](64) NULL,
	[PackageSizeCode] [varchar](64) NULL,
	[DepartCode] [varchar](64) NULL,
	[StoreCode] [varchar](64) NULL,
	[MinOrderQty] [dbo].[Buy_Qty] NULL,
	[OrderType] [int] NULL,
	[WarehouseCode] [varchar](64) NULL,
	[ProdClassCode] [varchar](64) NULL,
	[GapProdCode] [varchar](64) NULL,
	[ShelfLife] [int] NULL,
	[ProdSpec] [nvarchar](512) NULL,
	[ProdLength] [decimal](12, 4) NULL,
	[ProdWidth] [decimal](12, 4) NULL,
	[ProdHeight] [decimal](12, 4) NULL,
	[RefGP] [dbo].[Buy_Amt] NULL,
	[NonOrder] [int] NULL,
	[NonSale] [int] NULL,
	[Consignment] [int] NULL,
	[WeightItem] [int] NULL,
	[DiscAllow] [int] NULL,
	[CouponAllow] [int] NULL,
	[VisuaItem] [int] NULL,
	[TaxRate] [dbo].[Buy_Amt] NULL,
	[ImportTax] [dbo].[Buy_Amt] NULL,
	[Insurance] [dbo].[Buy_Amt] NULL,
	[Freight] [dbo].[Buy_Amt] NULL,
	[OthersExpense] [dbo].[Buy_Amt] NULL,
	[OriginCountryCode] [varchar](64) NULL,
	[ProductType] [int] NULL,
	[Modifier] [int] NULL,
	[BOM] [int] NULL,
	[MutexFlag] [int] NULL,
	[OnAccount] [int] NULL,
	[FulfillmentHouseCode] [varchar](64) NULL,
	[ReplenFormulaCode] [varchar](64) NULL,
	[DiscountLimit] [decimal](12, 4) NULL,
	[CostCCC] [varchar](64) NULL,
	[CostWO] [varchar](64) NULL,
	[RevenueCCC] [varchar](64) NULL,
	[RevenueWO] [varchar](64) NULL,
	[QuotaPerShopPeriod] [int] NULL,
	[CouponSKU] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[DefaultPickupStoreCode] [varchar](64) NULL,
	[ColorCode] [varchar](64) NULL,
	[ProductSizeCode] [varchar](64) NULL,
	[AddPointFlag] [int] NULL,
	[AddPointValue] [int] NULL,
	[InTax] [int] NULL,
	[Additional] [nvarchar](512) NULL,
	[Flag1] [int] NULL,
	[Flag2] [int] NULL,
	[Flag3] [int] NULL,
	[Flag4] [int] NULL,
	[Flag5] [int] NULL,
	[Flag6] [int] NULL,
	[Flag7] [int] NULL,
	[Flag8] [int] NULL,
	[Flag9] [int] NULL,
	[Flag10] [int] NULL,
	[Memo1] [nvarchar](512) NULL,
	[Memo2] [nvarchar](512) NULL,
	[Memo3] [nvarchar](512) NULL,
	[Memo4] [nvarchar](512) NULL,
	[Memo5] [nvarchar](512) NULL,
	[Memo6] [nvarchar](512) NULL,
	[Memo7] [nvarchar](512) NULL,
	[Memo8] [nvarchar](512) NULL,
	[Memo9] [nvarchar](512) NULL,
	[Memo10] [nvarchar](512) NULL,
	[Status] [int] NULL,
	[isOnlineSKU] [int] NULL,
	[SKUWeight] [decimal](12, 4) NULL,
	[NewFlag] [int] NULL,
	[HotSaleFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[ApprovedBy] [int] NULL,
	[ApprovedOn] [datetime] NULL,
 CONSTRAINT [PK_BUY_PRODUCT_PENDING] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [MinOrderQty]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [ProdLength]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [ProdWidth]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [ProdHeight]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [RefGP]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [NonOrder]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [NonSale]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Consignment]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [WeightItem]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [DiscAllow]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [CouponAllow]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [VisuaItem]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [TaxRate]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [ImportTax]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Insurance]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Freight]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [OthersExpense]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [ProductType]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [Modifier]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [BOM]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [MutexFlag]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [OnAccount]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((100)) FOR [DiscountLimit]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [QuotaPerShopPeriod]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [CouponSKU]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT (getdate()+(365)) FOR [EndDate]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [AddPointFlag]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [AddPointValue]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag1]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag2]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag3]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag4]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag5]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag6]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag7]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag8]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag9]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Flag10]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((1)) FOR [isOnlineSKU]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [SKUWeight]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [NewFlag]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT ((0)) FOR [HotSaleFlag]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_Pending] ADD  DEFAULT (getdate()) FOR [ApprovedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片文件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小票打印名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ScanDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小票打印名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ScanDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小票打印名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ScanDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品品牌Code（Product_Brand的ProductBrandCode）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProductBrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺的运营商品牌code（Brand->StoreBrandCode）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'StoreBrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装尺寸Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'PackageSizeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'DepartCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'适用店铺ID。默认空，所有店铺适用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小订单数量。（一个订单中最小销售数量）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'MinOrderQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'适用订单类型
0:All
1:WH Order (Order at Buying) 
2:Store Order (Order at Store) 
3:Center Order (Order at Buying) 
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'仓库编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'WarehouseCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大类Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdClassCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'缺货替代货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'GapProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'保质天数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ShelfLife'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品细节内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdSpec'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品宽度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品高度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProdHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代售货品毛利比例。（0~100 百分比）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'RefGP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非订单货品。 0： 不是非订单货品。 1：是非订单货品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'NonOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非销售货品。 0： 不是不能销售货品。 1：是不能销售货品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'NonSale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'委托销售货品。（货品所有权归供应商所有）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Consignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'称重销售货品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'WeightItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许折扣。0：不允许，1：允许' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'DiscAllow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许用Coupon支付。0：不允许，1：允许' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'CouponAllow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'虚拟货品。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'VisuaItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'税率。0：表示没税。 数值为百分比值。 
例如： 7， 表示 7% 的税率。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'TaxRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'进口税率。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ImportTax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'保险费率 （百分比）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Insurance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运费' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Freight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他费用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'OthersExpense'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产地（国家code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'OriginCountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品类型：
0： 销售商品
1： 费用类型 （非商品）（价格不定，比如运费，维修费，服务费）
2： msvc Card  （销售卡或者充值）
3： msvc Coupon （销售Coupon）
4： msvc Card  （购买积分）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProductType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许修改。0：不允许。1：允许' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否BOM货品，默认0
0： 不是BOM主货品
1： 是BOM主货品
（所有货品都可以加入BOM中）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'BOM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'互斥标志。 必须BOM主货品，此设置才生效。默认0。  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'MutexFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许记账销售。0：不允许。1：允许。  默认1
注：此设置和POS端有关。 允许记账销售时，才可以用记账的 支付方式。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'OnAccount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货仓库编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'FulfillmentHouseCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自动补货公式编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ReplenFormulaCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许的最大百分比折扣限制。 0~100， 默认100.   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'DiscountLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cost CCC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'CostCCC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cost Work Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'CostWO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Revenue CCC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'RevenueCCC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Revenue Work Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'RevenueWO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每个商店一个时期段内的配额数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'QuotaPerShopPeriod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否Coupon货品。0：不是。 1：是。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'CouponSKU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'默认提货仓库。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'DefaultPickupStoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品颜色' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ColorCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品尺寸Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ProductSizeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售货品是否参与增加积分的标志。默认1
0：不增加积分。
1：按照PointRule规则增加积分。
2：指定的积分数量 （AddPointValue的值）
3：按照照PointRule规则增加的积分 * 指定的倍数。 （AddPointValue 中设置倍数。 只允许整数倍）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'AddPointFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据AddPointFlag的设置，决定AddPointValue的内容。 默认0
AddPointFlag 为2 时，此内容为销售此货品增加的积分。
AddPointFlag 为3 时，此内容为销售此货品是按规则计算积分后 的 倍数。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'AddPointValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商的进货税率' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'InTax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag8'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag9'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Flag10'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo8'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo9'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留属性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Memo10'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核状态。 1：已经批核。0：未批核。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此货品是否需要同步到 SVA。 1：是的。 0：不需要。 默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'isOnlineSKU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品重量。用于快递费计算。 单位公斤' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'SKUWeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新品标志' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'NewFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'热卖标志' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'HotSaleFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ApprovedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending', @level2type=N'COLUMN',@level2name=N'ApprovedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品表 (待审核表)。 字段与buy_product相同。 只是加上status。 status=0 表示待审核。 status=1 表示已经审核通过。 审核通过的记录将被删除。 数据将更新到 BUY_Product表。
@2016-08-08 增加货品表 修改审核流程。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_Pending'
GO
