USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[verifyTagRange]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 * Stored Procedure: stproc_TestProcedure 
 * Created By: TAP-GROUP\Darwin.Pasco
 * Created At: 2016/11/07 18:01:06
 * Comments: Inserts values into TestTable
 */
CREATE PROCEDURE [dbo].[verifyTagRange]
    
    @coupontypeid                   int,
    @validstartingid                [bigint],
    @validendingid                  [bigint]
    
AS
SET NOCOUNT ON;



BEGIN

    DECLARE @lastsequencegenerated bigint    
    DECLARE @NextAvailableSequence  bigint
    DECLARE @SkippedSequence                bigint
    DECLARE @tagnumberlength                int
    DECLARE @tagtypeprefix                  varchar(64)   	
    DECLARE @numbroftagsallowed             int  
    DECLARE @quantity                       int
    DECLARE @isvalid                        int
    
    SET @quantity = (@validendingid - @validstartingid) + 1
    
    
    SELECT @tagtypeprefix = c.[QRCodePrefix]
    FROM [dbo].[CouponType] c
    WHERE [c].[CouponTypeID] = @coupontypeid
    


	
    IF EXISTS(SELECT [c].[CouponNumber] FROM [dbo].[Coupon] c
                 --WHERE CAST(REPLACE([c].[CouponNumber],'CX','') AS BIGINT) = @validstartingid)
                --WHERE [c].[CouponNumber] = CAST(@validstartingid AS char))
                 INNER JOIN [dbo].[CouponType] ct
                    ON [ct].[CouponTypeID] = [c].[CouponTypeID]
                 WHERE RIGHT([c].[CouponNumber],LEN(CAST(@validendingid AS char))) = CAST(@validendingid AS char)
                        AND [ct].[QRCodePrefix] = @tagtypeprefix)
        BEGIN
            
            SET @lastsequencegenerated = @validstartingid       
        END
                
    SET @SkippedSequence = (SELECT TOP (1) CAST(REPLACE([c].[CouponNumber], @tagtypeprefix,'') AS bigint)
                                FROM [dbo].[Coupon] c
                                WHERE CAST(REPLACE([c].[CouponNumber], @tagtypeprefix,'') AS int) > @lastsequencegenerated
                                AND [c].[CouponTypeID] = @coupontypeid
                                ORDER BY [c].[CouponNumber] DESC)
        
    /*
    IF NOT EXISTS(SELECT [b].[BatchCouponCode] FROM [dbo].[BatchCoupon] b
                WHERE CAST(REPLACE([b].[BatchCouponCode],'BTHCOU','') AS BIGINT) = @validstartingid)
        BEGIN
            
            SET @lastsequencegenerated = @validstartingid        
        END
        
    SET @SkippedSequence = (SELECT TOP (1) CAST(REPLACE([b].[BatchCouponCode], 'BTHCOU','') AS int)
                                FROM [dbo].[BatchCoupon] b
                                WHERE CAST(REPLACE([b].[BatchCouponCode], 'BTHCOU','') AS int) > @lastsequencegenerated
								order by [b].[BatchCouponCode] DESC)
    */
    
    IF @SkippedSequence is NULL 
        BEGIN
            SET @NextAvailableSequence = ISNULL(@SkippedSequence, @lastsequencegenerated)
            SET @numbroftagsallowed = ISNULL(@NextAvailableSequence - @lastsequencegenerated,0)

            IF EXISTS(SELECT [c].[CouponNumber] FROM [dbo].[Coupon] c
                         INNER JOIN [dbo].[CouponType] ct
                            ON [ct].[CouponTypeID] = [c].[CouponTypeID]
                         WHERE CAST(REPLACE([c].[CouponNumber], @tagtypeprefix,'') AS int)  BETWEEN @validstartingid and @validendingid
                                AND [ct].[QRCodePrefix] = @tagtypeprefix)
                BEGIN
                    SET @isvalid = 0
                END
            ELSE
                BEGIN
                    SET @isvalid = 1
                END
                
            SET @validendingid = @NextAvailableSequence + @quantity
            
        END
        
    ELSE
    
        BEGIN
            SET @NextAvailableSequence = @lastsequencegenerated
            SET @numbroftagsallowed = @NextAvailableSequence - @lastsequencegenerated
            SET @validendingid = @SkippedSequence
            SET @quantity = (@validendingid -  @lastsequencegenerated) + 1
            SET @isvalid = 0
            
        END
  
        
  
SELECT 
       @NextAvailableSequence AS ValidStartingID, 
        @validendingid AS ValidEndingID,  
        @quantity AS Quantity,
        @isvalid AS IsValid,
        @SkippedSequence
        
END







GO
