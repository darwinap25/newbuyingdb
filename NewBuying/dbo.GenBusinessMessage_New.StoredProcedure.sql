USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenBusinessMessage_New]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenBusinessMessage_New]
AS
/****************************************************************************
**  Name : GenBusinessMessage_New
**  Version: 1.0.0.0
**  Description : 根据MessagePrepList表数据来产生消息（拼写MessageContent，填写发送账号，写入messageobject）

** 
**  Created by:  Gavin @2014-09-01
****************************************************************************/
begin
  declare @MessageServiceTypeID int, @AccountNumber varchar(512), @MessageID int, @keyID int
  declare @MessageTitle nvarchar(512), @TemplateContent nvarchar(max), @DescFieldNo int, @MemberNickName varchar(512)
          , @CouponTypeName varchar(512), @RelateMemberNickName varchar(512)
  declare @MemberID int, @CardNumber varchar(64), @CardTypeID int, @CardGradeID int, @CardBrandID int, @CouponNumber varchar(64),
          @CouponTypeID int, @OprID int, @RelateMemberID int, @RelateCardNumber varchar(64), @RelateMemberMobile varchar(64),
          @CustomMsg nvarchar(2000)       
  declare @ParamCouponNumber varchar(64),@ParamCardNumber varchar(64), @ParamMemberID varchar(64), @ParamMemberName varchar(64),
          @ParamReceiveMobileNumber  varchar(64), @ParamCouponTypeName varchar(64), @ParamSystemDateTime varchar(64),
          @ParamOtherMemberName varchar(64), @ParamCustomMsg varchar(64)
  set @ParamCouponNumber = '{COUPONNUMBER}'
  set @ParamCardNumber = '{CARDNUMBER}'
  set @ParamMemberID = '{MEMBERID}'
  set @ParamMemberName = '{MEMBERNAME}'
  set @ParamReceiveMobileNumber = '{RECEIVEMEMBER}'
  set @ParamCouponTypeName = '{COUPONNAME}'
  set @ParamSystemDateTime = '{SYSTEMDATETIME}' 
  set @ParamOtherMemberName = '{OTHERMEMBERNAME}'
  set @ParamCustomMsg = '{CUSTOMMSG}'
  
  declare cur_MessagePrepList cursor fast_forward for
    select L.KeyID, L.MemberID,isnull(L.CardNumber,''),isnull(L.CardTypeID,0),isnull(L.CardGradeID,0),isnull(T.BrandID,0),isnull(L.CouponNumber,''),
        isnull(L.CouponTypeID,0),isnull(L.OprID,0),
        isnull(L.RelateMemberID,0),isnull(L.RelateCardNumber,''),isnull(L.RelateMemberMobile,''),isnull(L.CustomMsg,''),
        isnull(M.NickName,''), isnull(P.DescFieldNo,0), isnull(RM.NickName,''),
        case P.DescFieldNo when 2 then C.CouponTypeName2 when 3 then C.CouponTypeName3 else C.CouponTypeName1 end
      from MessagePrepList L 
        left join CardType T on L.CardTypeID = T.CardTypeID
        left join Member M on L.MemberID = M.MemberID
        left join LanguageMap P on P.KeyID = M.MemberDefLanguage 
        left join CouponType C on L.CouponTypeID = C.CouponTypeID
        left join Member RM on L.RelateMemberID = RM.MemberID and isnull(L.RelateMemberID, 0) <> 0
     where L.Status = 0
  Open cur_MessagePrepList
  fetch from cur_MessagePrepList into @keyID, @MemberID,@CardNumber,@CardTypeID,@CardGradeID,@CardBrandID,@CouponNumber,@CouponTypeID,@OprID,
        @RelateMemberID,@RelateCardNumber,@RelateMemberMobile,@CustomMsg, @MemberNickName, @DescFieldNo, @RelateMemberNickName,
        @CouponTypeName        
  while @@FETCH_STATUS = 0 
  begin
    set @CouponTypeName = ISNULL(@CouponTypeName, '')
         
    DECLARE CUR_GenBusinessMessage CURSOR fast_forward FOR
      select A4.MessageServiceTypeID, A4.AccountNumber, 
          case @DescFieldNo when 2 then A1.MessageTitle2 when 3 then A1.MessageTitle3 else A1.MessageTitle1 end, 
          case @DescFieldNo when 2 then A1.TemplateContent2 when 3 then A1.TemplateContent3 else A1.TemplateContent1 end        
        from MessageTemplateDetail A1
        left join MessageTemplate A2 on A1.MessageTemplateID = A2.MessageTemplateID
        left join (select B1.NoticeNumber, B1.MessageTemplateID from MemberNotice_MessageType B1
                     left join MemberNotice B2 on B1.NoticeNumber = B2.NoticeNumber
                    where B2.ApproveStatus = 'A' 
                      and B1.NoticeNumber in ( select NoticeNumber from MemberNotice_Filter 
                                                 where (BrandID=@CardBrandID or isnull(@CardBrandID,0)=0)
                                                       and (isnull(CardTypeID, -1)='-1' or CardTypeID =@CardTypeID)
                                                       and (isnull(CardGradeID, -1)='-1' or CardGradeID=@CardGradeID)
                                                       and (isnull(CouponTypeID, -1)='-1' or CouponTypeID=@CouponTypeID)
                                              )  
                  ) A3 on A1.MessageTemplateID = A3.MessageTemplateID   
        left join (select * from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1) A4 on A1.MessageServiceTypeID = A4.MessageServiceTypeID  
        where A2.OprID = @OprID and isnull(A4.AccountNumber,'') <> ''
        order by A1.MessageServiceTypeID   
    OPEN CUR_GenBusinessMessage
    FETCH FROM CUR_GenBusinessMessage INTO @MessageServiceTypeID, @AccountNumber, @MessageTitle, @TemplateContent
    WHILE @@FETCH_STATUS=0
    BEGIN 
      set @TemplateContent = REPLACE(@TemplateContent, @ParamCouponNumber, ISNULL(@CouponNumber,''))
      set @TemplateContent = REPLACE(@TemplateContent, @ParamCardNumber, ISNULL(@CardNumber,''))
      set @TemplateContent = REPLACE(@TemplateContent, @ParamMemberID, ISNULL(@MemberID,''))
      set @TemplateContent = REPLACE(@TemplateContent, @ParamMemberName, ISNULL(@MemberNickName,''))
      set @TemplateContent = REPLACE(@TemplateContent, @ParamReceiveMobileNumber, ISNULL(@RelateMemberMobile,''))
      set @TemplateContent = REPLACE(@TemplateContent, @ParamCouponTypeName, ISNULL(@CouponTypeName,''))
      set @TemplateContent = REPLACE(@TemplateContent, @ParamSystemDateTime, CONVERT(varchar(10), getdate(), 120))  
      set @TemplateContent = REPLACE(@TemplateContent, @ParamOtherMemberName, ISNULL(@RelateMemberNickName,''))
      set @TemplateContent = REPLACE(@TemplateContent, @ParamCustomMsg, ISNULL(@CustomMsg,''))   

      insert into MessageObject(MessageServiceTypeID, MessageType, MessagePriority, MessageCoding, MessageTitle, 
         MessageBody, FromMemberID, IsInternal, [Status], CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ResponseCode)
      values (@MessageServiceTypeID, 1, 1, 1, @MessageTitle, 
         cast(@TemplateContent as varbinary(max)), 1, 0, 0, Getdate(), 1, Getdate(), 1, 0)         
      set @MessageID = SCOPE_IDENTITY()
      insert into MessageReceiveList(MessageID, MemberID, AccountNumber, IsRead, status, UpdatedOn, UpdatedBy) 
      values (@MessageID, @MemberID, @AccountNumber, 0, 0, Getdate(), 1)  
      FETCH FROM CUR_GenBusinessMessage INTO  @MessageServiceTypeID, @AccountNumber, @MessageTitle, @TemplateContent 
    END
    CLOSE CUR_GenBusinessMessage 
    DEALLOCATE CUR_GenBusinessMessage 
     
    update MessagePrepList set Status = 1 where KeyID = @KeyID    
      
    fetch from cur_MessagePrepList into @keyID, @MemberID,@CardNumber,@CardTypeID,@CardGradeID,@CardBrandID,@CouponNumber,@CouponTypeID,@OprID,
        @RelateMemberID,@RelateCardNumber,@RelateMemberMobile,@CustomMsg, @MemberNickName, @DescFieldNo,@RelateMemberNickName,
        @CouponTypeName
  end
  close cur_MessagePrepList
  deallocate cur_MessagePrepList
    
  return 0   
end

GO
