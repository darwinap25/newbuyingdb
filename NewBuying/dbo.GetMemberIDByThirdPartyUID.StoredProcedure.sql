USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberIDByThirdPartyUID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberIDByThirdPartyUID]  
  @TokenUID             varchar(64),            --第三方网页的时间戳  
  @TokenStr             varchar(512) output,    --第三方网页的Token  
  @MessageServiceTypeID int output,             --第三方类型。
  @AccountNumber        varchar(64) output,     --第三方账号    
  @MemberID             int output             -- 返回Member表主键
as  
/****************************************************************************
**  Name : 根据第三方时间戳查找member.
**  Version: 1.0.0.0
**  Description : 用户提供第三方账号（比如QQ），根据MemberMessageAccount记录来判断登录。如果没有，则自动新增此Member。
**  Example :
    select * from useraction_movement
**  Created by: Gavin @2014-05-14
**
****************************************************************************/
begin
  select top 1 @MemberID = MemberID, @TokenStr = TokenStr, @MessageServiceTypeID = MessageServiceTypeID, @AccountNumber = AccountNumber
    from MemberMessageAccount where TokenUID = @TokenUID
  if isnull(@MemberID, 0) > 0
    return 0
  else
    return -1  
end

GO
