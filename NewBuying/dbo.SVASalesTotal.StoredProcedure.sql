USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SVASalesTotal]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SVASalesTotal]
  @UserID				int,		         -- 操作员ID
  @MemberID				int,                 -- 会员ID
  @CardUID				varchar(512),		 -- 会员卡的UID
  @CardNumber			varchar(64),        -- 会员卡号。
  @BrandID			    int, 		        -- 店铺brandID
  @StoreCode            varchar(64),        -- 店铺编码   
  @RegisterCode         varchar(64),        -- 终端编码
  @ServerCode			varchar(64),        -- server编码
  @TxnNo				varchar(512),       -- 交易号码
  @Busdate				datetime,           -- 交易busdate
  @Txndate				datetime,			 -- 交易系统时间
  @SalesType			int,				 -- 交易类型
  @CashierCode			varchar(64),		 -- 收银员Code  
  @SalesManCode			varchar(64),		 -- 销售员Code
  @TotalAmount			money,				 -- 交易总额
  @TxnStatus			int,				 -- 交易状态
  @Contact				varchar(512),		 -- 联系人
  @ContactPhone			varchar(512),        -- 联系人电话
  @Courier				varchar(512),		 -- 送货员
  @DeliveryaAddr		nvarchar(512),		 -- 送货地址
  @DeliveryDate			datetime,			 -- 送货时间
  @DeliveryCompleteDate datetime,			 -- 送货完成时间
  @SalesDetail			nvarchar(max),		 -- 交易单货品明细(交易货品, XML格式字符串)
  @SalesTender			nvarchar(max),		 -- 交易单支付明细(交易支付记录, XML格式字符串)
  @SalesReceipt         varchar(max),		 -- 交易单小票（字符串格式）  
  @SalesReceiptBIN      varbinary(max),		 -- 交易单小票（二进制格式）
  @SalesAdditional		nvarchar(512),		 -- 交易单附加信息
  @IsShoppingCart       int=0,				 -- 是否购物车订单. 0:不是. 1:是的. 默认0
  @DeliveryNumber       varchar(512)='',     -- 送货单号.
  @PickupType           int = 1,             -- 提货方式。 默认 1.  1：门店自提	 2：送货 
  @DeliveryCountryCode  varchar(64)=''       -- 送货国家     
AS
/****************************************************************************
**  Name : SVASalesTotal    
**  Version: 1.0.0.8
**  Description : 提交Online shopping 的 销售数据。(用于计算折扣，费用)	（此存储过程hardcode，单独针对某客户，不同版本不兼容）
**  
@SalesDetail格式范例:  
<ROOT>
  <PLU TxnNo="" SeqNo="" ProdCode="0001" ProdDesc="" Serialno="" Collected="" RetailPrice="" NetPrice="" QTY="" NetAmount="" Additional="" ReservedDate="" PickupDate=""></PLU>
  <PLU TxnNo="" SeqNo="" ProdCode="0002" ProdDesc="" Serialno="" Collected="" RetailPrice="" NetPrice="" QTY="" NetAmount="" Additional="" ReservedDate="" PickupDate=""></PLU>
<ROOT>
@SalesTender格式范例:  
<ROOT>
  <TENDER TxnNo="" SeqNo="" TenderID="" TenderCode="" TenderName="" TenderAmount="" LocalAmount="" ExchangeRate="" Additional=""></TENDER>
  <TENDER TxnNo="" SeqNo="" TenderID="" TenderCode="" TenderName="" TenderAmount="" LocalAmount="" ExchangeRate="" Additional=""></TENDER>
</ROOT> 
sp_helptext svasalestotal
  declare @a int, 
  @UserID				int,		         -- 操作员ID
  @MemberID				int,                 -- 会员ID
  @CardUID				varchar(512),		 -- 会员卡的UID
  @CardNumber			varchar(64),        -- 会员卡号。
  @BrandCode			varchar(64), 		 -- 店铺brandcode 
  @StoreCode            varchar(64),        -- 店铺编码   
  @RegisterCode         varchar(64),        -- 终端编码
  @ServerCode			varchar(64),        -- server编码
  @TxnNo				varchar(512),        -- 交易号码
  @Busdate				datetime,            -- 交易busdate
  @Txndate				datetime,			 -- 交易系统时间
  @SalesType			int,				 -- 交易类型
  @CashierCode			varchar(64),		 -- 收银员Code  
  @SalesManCode			varchar(64),		 -- 销售员Code
  @TotalAmount			money,				 -- 交易总额
  @TxnStatus			int,				 -- 交易状态
  @Contact				varchar(512),		 -- 联系人
  @ContactPhone			varchar(512),        -- 联系人电话
  @Courier				varchar(512),		 -- 送货员
  @DeliveryaAddr		nvarchar(512),		 -- 送货地址
  @DeliveryDate			datetime,			 -- 送货时间
  @DeliveryCompleteDate datetime,			 -- 送货完成时间
  @SalesDetail			nvarchar(max),		 -- 交易单货品明细(交易货品, XML格式字符串)
  @SalesTender			nvarchar(max),		 -- 交易单支付明细(交易支付记录, XML格式字符串)
  @SalesReceipt         varchar(max),	 -- 交易单小票   
  @SalesReceiptBIN  varbinary(max)
  set @UserID = 1
  set @MemberID = 1
  set @CardUID = ''
  set @CardNumber = '000100086'
  set @BrandCode = '123123'
  set @StoreCode = '1'
  set @RegisterCode = '001'
  set @ServerCode = '02'
  set @TxnNo = 'test201209181'
  set @Busdate = '2012-09-18'
  set @Txndate = '2012-09-18 12:12:12'  				
  set @SalesType = 0
  set @CashierCode = '4444'
  set @SalesManCode = '5555'
  set @TotalAmount = 100
  set @TxnStatus = 5
  set @Contact = 'rrg'
  set @ContactPhone = '120'
  set @Courier = 'sogo'
  set @DeliveryaAddr = 'hongsldkdfjjsdlkfjfgsd'
  set @DeliveryDate = '2012-09-18'
  set @DeliveryCompleteDate = '2012-09-18'
  set @SalesDetail = '<ROOT>
  <PLU TxnNo="12345" SeqNo="1" ProdCode="0001" ProdDesc="是数据库的法律卡位" Serialno="" Collected="1" RetailPrice="100" NetPrice="100" QTY="1" NetAmount="100" Additional=""></PLU>
  <PLU TxnNo="12345" SeqNo="2" ProdCode="0002" ProdDesc="塞德里克个记录款未付" Serialno="" Collected="1" RetailPrice="100" NetPrice="100" QTY="2" NetAmount="200" Additional=""></PLU>
</ROOT>'
  set @SalesTender = '<ROOT>
  <TENDER TxnNo="12345" SeqNo="1" TenderID="1" TenderCode="RMD" TenderName="RMD" TenderAmount="200" LocalAmount="200" ExchangeRate="1" Additional=""></TENDER>
  <TENDER TxnNo="12345" SeqNo="2" TenderID="2" TenderCode="USD" TenderName="USD" TenderAmount="100" LocalAmount="100" ExchangeRate="1" Additional=""></TENDER>
</ROOT> '
  set @SalesReceipt = ''
  exec @a = SVASalesTotal @UserID,@MemberID,@CardUID,@CardNumber,@BrandCode,@StoreCode,@RegisterCode,@ServerCode,@TxnNo,@Busdate,
    @Txndate,@SalesType,@CashierCode,@SalesManCode,@TotalAmount,@TxnStatus,@Contact,@ContactPhone,@Courier,@DeliveryaAddr,@DeliveryDate,
    @DeliveryCompleteDate,@SalesDetail,@SalesTender,@SalesReceipt, @SalesReceiptBIN, '',0,'',2
  print @a  
select * from sales_D
select cast(messagebody as nvarchar(max)), * from messageobject
select * from messagereceivelist
select * from card_movement where createdon > '2012-09-25'
**  Created by: Gavin @2013-07-31
**  Modify by: Gavin @2013-08-06 (ver 1.0.0.1) 修改运费条件，小于200 才有运费， 大于等于200，免运费。(for s4s)
**  Modify by: Gavin @2013-12-19 (ver 1.0.0.2) （for VIVA 版本） 增加输入参数，修改运费条件：当@PickupType=2，则需要加运费200。	（此版本hardcode，和上一版本不兼容）
**  Modify by: Gavin @2016-10-12 (ver 1.0.0.3) 根据country表的DeliveryCharge字段来计算运费.
**  Modify by: Gavin @2016-10-13 (ver 1.0.0.4) (for bauhaus) @DeliveryAddress 中，第一段是国家， 和后面的地址用 “ ”隔开
**  Modify by: Gavin @2016-10-17 (ver 1.0.0.5) (for bauhaus) 增加参数@DeliveryCountryCode, 根据 @DeliveryCountry来判断国家,计算运费.
**  Modify by: Gavin @2016-10-19 (ver 1.0.0.6) (for bauhaus) 删除国家名字和完整地址中的空格,再来部分匹配查询
**  Modify by: Gavin @2017-03-27 (ver 1.0.0.7) (for bauhaus) 计算运费时,考虑Country新加的字段FreeThreshold, 总金额超过这个金额将免运费. ( >= ).  如果是0 ，则表示没有免费的设置。
**  Modify by: Gavin @2017-03-28 (ver 1.0.0.8) (for bauhaus) 由于调用方没有传入@TotalAmount值， 所以使用本单的sales_D 的 NetAmount 合计来取代
**
****************************************************************************/
begin
SET ARITHABORT ON
SET NOCOUNT ON

  declare @OldTxnStatus int, @Longitude varchar(512), @latitude varchar(512), @MemberName varchar(512), @MemberMobilePhone varchar(512),
		  @CardTotalAmount money, @CardTotalPoints int, @ExpiryDate datetime, @CardStatus int, @MemberAddress nvarchar(512), @ApprovalCode varchar(6)
  declare @SalesDData xml, @SalesTData xml
  declare @EarnPoints int, @StoreID int, @CalcResult int
  declare @ReceiveList varchar(64), @MessageID int, @MessageServiceTypeID int
  declare @CardGradeID int, @BindCouponTypeID int, @BindCouponCount int, @CouponExpiryDate datetime, @CouponNumber varchar(64)
  declare @CouponAmount money, @i int
  declare @CouponStatus int, @NewCouponStatus int, @NewCouponExpiryDate date, @FreeThreshold money, @FindFreeThreshold money
     
  set @IsShoppingCart = isnull(@IsShoppingCart, 0)
  set @DeliveryCountryCode = isnull(@DeliveryCountryCode, '')

  declare @SalesD table (TxnNo varchar(512), SeqNo int, ProdCode varchar(64), ProdDesc nvarchar(512), Serialno varchar(512), 
        Collected int, RetailPrice money, NetPrice money, Qty int, NetAmount money, Additional varchar(512),ReservedDate datetime, PickupDate datetime)
  --PLU detail数据写入临时表，判断货品库存数量
  if isnull(@SalesDetail, '') <> ''
  begin
	set @SalesDData = @SalesDetail
	Insert into @SalesD(TxnNo, SeqNo, ProdCode, ProdDesc, Serialno, Collected, RetailPrice, NetPrice, Qty, NetAmount, Additional, ReservedDate, PickupDate)
	select @TxnNo, T.v.value('@SeqNo','int') as SeqNo, T.v.value('@ProdCode','varchar(64)') as ProdCode, 
	    T.v.value('@ProdDesc','nvarchar(512)') as ProdDesc, T.v.value('@Serialno','varchar(512)') as Serialno, T.v.value('@Collected','int') as Collected, T.v.value('@RetailPrice','money') as RetailPrice, 
	    T.v.value('@NetPrice','money') as NetPrice, T.v.value('@QTY','int') as QTY, T.v.value('@NetAmount','money') as NetAmount, T.v.value('@Additional','varchar(512)') as Additional,
	    T.v.value('@ReservedDate','datetime') as ReservedDate, T.v.value('@PickupDate','datetime') as PickupDate	    
	  from @SalesDData.nodes('/ROOT/PLU') T(v)
	if exists(  
	          select A.ProdCode, A.SalesQty, B.OnhandQty from (select ProdCode, sum(isnull(Qty,0)) as salesqty from @SalesD group by ProdCode) A 
	            left join ProductStockOnhand B on A.ProdCode = B.ProdCode
	           where B.OnhandQty < A.SalesQty )
	  return -101     
	  
	if isnull(@TotalAmount, 0) = 0
	  select @TotalAmount = sum(isnull(NetAmount, 0)) from @SalesD        
  end 
      		  
  --准备数据---  

  -- 获取会员   
  if isnull(@CardNumber, '') = '' and isnull(@CardUID, '') <> ''
    select @CardNumber = CardNumber from CardUIDMap where CardUID = @CardUID
  if isnull(@CardNumber, '') <> ''
    select @MemberID = MemberID, @CardTotalAmount = TotalAmount, @CardTotalPoints = isnull(TotalPoints,0),
           @ExpiryDate = CardExpiryDate, @CardStatus = Status, @CardGradeID = CardGradeID
     from Card where CardNumber = @CardNumber  

  select @MemberAddress = AddressFullDetail from memberaddress where MemberFirstAddr = 1 and MemberID = @MemberID  
  select @MemberName = RTrim(LTrim(MemberChiFamilyName)) + RTrim(LTrim(MemberChiGivenName)), 
     @MemberMobilePhone = MemberRegisterMobile from Member where MemberID = @MemberID         
     
 -- if @@rowcount = 0
  --  return -1 
     
  declare @SeqNo int, @ProdCode varchar(64), @ProdDesc nvarchar(512), @Amount money	 
  -- 计算费用 （运费）(for S4S)
/*  
  if @TotalAmount  < 200
  begin
    select @SeqNo = max(SeqNo) from @SalesD
    set @SeqNo = isnull(@SeqNo, 0) + 1
    set @ProdCode = '9999'
    set @ProdDesc = '运费'
    set @Amount = 20
	insert into @SalesD(SeqNo, ProdCode, ProdDesc, RetailPrice, NetPrice, Qty, NetAmount, Collected)
	values(@SeqNo, @ProdCode, @ProdDesc, @Amount, @Amount, 1, @Amount, -9)
  end
*/  
   -- 计算费用 （运费）(for bauhaus)   
  if @PickupType = 2
  begin
    declare @Countryname nvarchar(512), @pos int, @CountryCode varchar(64), @weight decimal(16,6), @DeliveryCharge money

	if @DeliveryCountryCode = ''
	begin
/*
		select @Countryname = RTrim(LTRim(@DeliveryaAddr))
		select @pos = CharIndex(' ', @Countryname)

		if ISNULL(@Countryname, '') <> '' and @pos > 0
		begin
		  select @Countryname = substring(@Countryname, 1, @pos - 1)
		  select @CountryCode = CountryCode, @DeliveryCharge = DeliveryCharge from Country 
			  where RTrim(LTRim(Countryname1)) = RTrim(LTRim(@Countryname)) 
				or RTrim(LTRim(Countryname2)) = RTrim(LTRim(@Countryname))
				or RTrim(LTRim(Countryname2)) = RTrim(LTRim(@Countryname))
		end
*/
 		select @Countryname = RTrim(LTRim(Replace(@DeliveryaAddr,' ', '')))

		declare @FindCountryCode varchar(64), @FindDeliveryCharge money, @CountryName1 nvarchar(512), @CountryName2 nvarchar(512), @CountryName3 nvarchar(512),
		        @countrynamelen int

		if ISNULL(@Countryname, '') <> ''
		begin
		    set @CountryCode = ''
		    set @DeliveryCharge = 0
			DECLARE CUR_Ord_GetConuntry CURSOR fast_forward FOR
			  SELECT CountryCode, RTrim(LTRim(Replace(CountryName1,' ', ''))), RTrim(LTRim(Replace(CountryName2,' ', ''))), RTrim(LTRim(Replace(CountryName3,' ', ''))),
			    DeliveryCharge, FreeThreshold  FROM Country
			OPEN CUR_Ord_GetConuntry
			FETCH FROM CUR_Ord_GetConuntry INTO @FindCountryCode, @CountryName1, @CountryName2, @CountryName3,@FindDeliveryCharge, @FindFreeThreshold
			WHILE @@FETCH_STATUS=0
			BEGIN
			  if @CountryName1 = substring(@Countryname, 1, len(@CountryName1)) 
			    or @CountryName2 = substring(@Countryname, 1, len(@CountryName2)) 
				or @CountryName3 = substring(@Countryname, 1, len(@CountryName3)) 
              begin
		        set @CountryCode = @FindCountryCode
		        set @DeliveryCharge = @FindDeliveryCharge
				set @FreeThreshold = @FindFreeThreshold
			    break
			  end
			  FETCH FROM CUR_Ord_GetConuntry INTO @FindCountryCode, @CountryName1, @CountryName2, @CountryName3,@FindDeliveryCharge, @FindFreeThreshold
			END
			CLOSE CUR_Ord_GetConuntry 
			DEALLOCATE CUR_Ord_GetConuntry 
        end
    end
	else 
	    select @CountryCode = @DeliveryCountryCode, @DeliveryCharge = DeliveryCharge, @FreeThreshold=FreeThreshold  from Country where CountryCode = @DeliveryCountryCode

      
	  set @FreeThreshold = isnull(@FreeThreshold, 0)
	  select @SeqNo = max(SeqNo) from @SalesD

	  if isnull(@DeliveryCharge,0) > 0
	  begin
	    if (isnull(@TotalAmount, 0) >= @FreeThreshold) and (@FreeThreshold > 0)
		begin
		  set @Amount = 0
		  set @ProdDesc = '免运费'
        end 
		else
		begin
		  set @Amount = isnull(@DeliveryCharge, 0)
		  set @ProdDesc = '运费'
		end
        set @SeqNo = isnull(@SeqNo, 0) + 1
        set @ProdCode = '9999'
            
    	insert into @SalesD(SeqNo, ProdCode, ProdDesc, RetailPrice, NetPrice, Qty, NetAmount, Collected)
  	    values(@SeqNo, @ProdCode, @ProdDesc, @Amount, @Amount, 1, @Amount, -9)
      end
  end 
  ---
  
  select SeqNo, ProdCode, ProdDesc, Serialno, Collected, RetailPrice, NetPrice, Qty, NetAmount
    from @SalesD
    
  return 0
end

GO
