USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PRODUCT_CLASSIFY_Pending]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PRODUCT_CLASSIFY_Pending](
	[ProdCode] [varchar](64) NOT NULL,
	[ForeignkeyID] [int] NOT NULL,
	[ForeignTable] [varchar](64) NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_PRODUCT_CLASSIFY_PENDIN] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC,
	[ForeignkeyID] ASC,
	[ForeignTable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_PRODUCT_CLASSIFY_Pending] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_CLASSIFY_Pending] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_CLASSIFY_Pending', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键ID。（ForeignTable指定表的外键）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_CLASSIFY_Pending', @level2type=N'COLUMN',@level2name=N'ForeignkeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键表。（表名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_CLASSIFY_Pending', @level2type=N'COLUMN',@level2name=N'ForeignTable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品划分。（辅助表。 货品和其他表配合，多对多关系）
Pending表 @2016-08-08 (for bauhaus)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_CLASSIFY_Pending'
GO
