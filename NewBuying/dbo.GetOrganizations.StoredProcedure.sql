USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetOrganizations]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetOrganizations]
  @LanguageAbbr			varchar(20)=''			   
AS
/****************************************************************************
**  Name : 
**  Version: 1.0.0.0
**  Description : 返回机构列表
**
**  Parameter :
  exec GetOrganizations 'zh_CN'
  select * from LanguageMap
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**MemberClause
****************************************************************************/
begin 
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  select OrganizationID, case @Language when 2 then OrganizationName2 when 3 then OrganizationName3 else OrganizationName1 end as OrganizationName,
     CardNumber, CumulativePoints, CumulativeAmt, OrganizationType, CallInterface, OrganizationPicFile
    from Organization
  return 0 
end

GO
