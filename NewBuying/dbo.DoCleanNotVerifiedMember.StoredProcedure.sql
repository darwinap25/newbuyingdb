USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DoCleanNotVerifiedMember]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[DoCleanNotVerifiedMember]
AS
/****************************************************************************
**  Name : DoCleanNotVerifiedMember
**  Version : 2.0.0.2
**  Description : 检查是否存在超过期限未验证的用户，如果存在则删除
** 
declare @a int
exec @a = DoCleanNotVerifiedMember 
print @a
  select * from Ord_CouponOrderForm_H
**
**  Create by: Gavin @2014-08-05 
**  Modify by: Gavin @2014-08-06 (ver 1.0.0.1) 根据要求在Member增加NewmemberFlag字段,用于标识用户是否新增的.
**  Modify by: Gavin @2014-08-22 (ver 1.0.0.2) 归还赠送方的金额,积分. 作废此会员下的卡和Coupon(void)
**  Modify by: Gavin @2014-09-03 (ver 1.0.0.3) 转赠时自动增加的,和主动注册的, 两种NewMemberFlag=1 的Member要分开对待
**  Modify by: Gavin @2014-09-16 (ver 1.0.0.4) 作废卡时, card_movement中的 points必须填0.
**  Modify by: Gavin @2014-09-25 (ver 1.0.0.5) 发消息时增加 被删除方的 手机号码
**  Modify by: Gavin @2014-10-08 (ver 1.0.0.6) 作废卡时,不要加积分.
**  Modify by: Gavin @2014-10-24 (ver 1.0.0.7) 归还Coupon时,检查一下这个Coupon目前所属的Card是否要被删除的Card,如果已经归还,则不用重复执行
**  Modify by: Gavin @2015-01-16 (ver 1.0.0.8) 按照CardGrade的 期限删除会员时, 只删主动注册的会员. (加回检查 MemberRegisterMobile的条件)
**  Modify by: Robin @2015-07-14 (ver 2.0.0.0) 全面优化SQL语句
**  Modify by: Gavin @2015-07-20 (ver 2.0.0.1) 全面优化SQL语句. (只用一个in 的条件...)
**  Modify by: Gavin @2016-01-21 (ver 2.0.0.2) (for Bauhaus) 本次被删除的会员转存到另外一个表中DeletedMember，导出会员时会读取出了。
****************************************************************************/
BEGIN 
  -- ver 2.0.0.2
  if not exists (select 1 from sysobjects where id = object_id('DeletedMember') and type = 'U')
  begin
    select top 1 C.CardNumber, M.CreatedOn, M.NickName, M.MemberMobilePhone, MemberRegisterMobile, CardGradeID,
	  MemberSex, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth, M.UpdatedOn, 'C' as UpdateFlag  
	into DeletedMember 
	from Member M left join Card C on C.MemberID = M.MemberID
  end
  truncate table DeletedMember
  ----

  declare @CardNumber varchar(64),  @MsgCouponNumber varchar(64), @MsgCardTypeID int, @MsgCardGradeID int, @MemberID int
          ,@MsgCouponTypeID int ,@MsgCardBrandID int, @DelMemberMobile varchar(64)
  declare  @Del1 table(CardNumber varchar(64), CouponNumber varchar(64), CardTypeID int, CardGradeID int, MemberID int
          ,CouponTypeID int, CardBrandID int, DelMemberMobile varchar(64))
  declare  @Del2 table(CardNumber varchar(64), MemberID int) 
  declare  @Del3 table(CardNumber varchar(64), MemberID int) 
  
  insert into @Del1 (CardNumber, CouponNumber, CouponTypeID, CardTypeID, CardGradeID, MemberID ,CardBrandID, DelMemberMobile)
  select B.CardNumber, B.CouponNumber, B.CouponTypeID, C.CardTypeID, C.CardGradeID, C.MemberID, T.BrandID, CC.MemberMobilePhone 
    from (select  S.* from (select * from Coupon_Movement where KeyID in (select max(KeyID) from Coupon_Movement where OprID = 36 group by CouponNumber)) S where  S.CouponNumber  in  
           ( 
               select A.CouponNumber from Coupon_Movement A left join Coupon B on A.CouponNumber = B.CouponNumber  
                 where OprID = 37 and A.CardNumber = B.CardNumber and A.CardNumber in ( 
                          select C.CardNumber from Card C 
                              left join CardGrade G on C.CardGradeID = G.CardGradeID  
                              left join Member M on C.MemberID = M.MemberID
                           where (M.MemberID is not null) and M.NewMemberFlag = 1 and CHARINDEX('@', M.MemberRegisterMobile) = 0 
                        )                        
           ) 
       )B
	 left join CouponType P on B.CouponTypeID = P.CouponTypeID  
    left join Card C on B.CardNumber = C.CardNumber  
    left join CardType T on C.CardTypeID = T.CardTypeID  
    left join (select distinct A.CardNumber, B.MemberID, B.MemberMobilePhone from Card A 
                left join Member B on A.MemberID = B.MemberID) CC on B.CardNumber = CC.CardNumber
    where  DATEADD(DD, isnull(P.CouponReturnValue,0), B.CreatedOn) < GETDATE()
   
    insert into @Del3(CardNumber, MemberID)      
     select D.CardNumber, D.MemberID from Coupon_Movement M 
       left join Coupon C on M.CouponNumber = C.CouponNumber
       left join Card D on C.CardNumber = D.CardNumber
       left join CouponType P on C.CouponTypeID = P.CouponTypeID
      where KeyID in (select max(KeyID) from Coupon_Movement where OprID = 37 group by CouponNumber) 
        and C.CardNumber in ( 
                select C.CardNumber from Card C 
                    left join CardGrade G on C.CardGradeID = G.CardGradeID  
                    left join Member M on C.MemberID = M.MemberID
                 where (M.MemberID is not null) and M.NewMemberFlag = 1 and CHARINDEX('@', M.MemberRegisterMobile) = 0 
          )
     and DATEADD(DD, isnull(P.CouponReturnValue,0), M.CreatedOn) < GETDATE()  
                 
  -- 转赠创建的会员的删除
  DECLARE CUR_DoCleanNotVerifiedMember CURSOR fast_forward FOR
    select distinct CardNumber, CouponNumber, CouponTypeID, CardTypeID, CardGradeID, MemberID ,CardBrandID, DelMemberMobile 
      from @Del1
  OPEN CUR_DoCleanNotVerifiedMember
  FETCH FROM CUR_DoCleanNotVerifiedMember INTO @CardNumber, @MsgCouponNumber, @MsgCouponTypeID, @MsgCardTypeID, @MsgCardGradeID, @MemberID, @MsgCardBrandID, @DelMemberMobile
  WHILE @@FETCH_STATUS=0
  BEGIN
    exec GenBusinessMessage 511, @MemberID, @CardNumber, @MsgCouponNumber, @MsgCardTypeID, @MsgCardGradeID, @MsgCardBrandID,  @MsgCouponTypeID, @DelMemberMobile   
       
    FETCH FROM CUR_DoCleanNotVerifiedMember INTO @CardNumber, @MsgCouponNumber, @MsgCouponTypeID, @MsgCardTypeID, @MsgCardGradeID, @MemberID, @MsgCardBrandID, @DelMemberMobile 
  END
  CLOSE CUR_DoCleanNotVerifiedMember 
  DEALLOCATE CUR_DoCleanNotVerifiedMember   
    
 -- 归还赠送的coupon
	insert into Coupon_Movement
			(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
			BusDate, Txndate, Remark, SecurityCode, CreatedBy, OrgExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
			NewExpiryDate, OrgStatus, NewStatus)
   select 45, B.CardNumber, A.CouponNumber, A.CouponTypeID, null, null, '', A.CouponAmount, 0, A.CouponAmount,
      convert(varchar(10), GETDATE(), 120), GETDATE(), 'Give back coupon', '', 1, A.CouponExpiryDate, '', null, '', '',
			A.CouponExpiryDate, A.Status, A.Status				
    from Coupon A 
    left join CouponType P on A.CouponTypeID = P.CouponTypeID    
    left join 
      ( select S.* from (select * from Coupon_Movement where KeyID in (select max(KeyID) from Coupon_Movement where OprID = 36 group by CouponNumber) ) S
          where S.CouponNumber in  
           ( 
               select A.CouponNumber from Coupon_Movement A left join Coupon B on A.CouponNumber = B.CouponNumber  
                 where OprID = 37 and A.CardNumber = B.CardNumber and A.CardNumber in ( 
                          select C.CardNumber from Card C 
                              left join CardGrade G on C.CardGradeID = G.CardGradeID  
                              left join Member M on C.MemberID = M.MemberID
                           where (M.MemberID is not null) and M.NewMemberFlag = 1 and CHARINDEX('@', M.MemberRegisterMobile) = 0 
                        )    
           ) 
       )B on A.CouponNumber = B.CouponNumber
    left join Card C on B.CardNumber = C.CardNumber  
    left join CardType T on C.CardTypeID = T.CardTypeID  
    where  DATEADD(DD, isnull(P.CouponReturnValue,0), B.CreatedOn) < GETDATE()

   -- 作废删除会员的Coupon
    insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
				BusDate, Txndate, Remark, SecurityCode, CreatedBy, OrgExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
				NewExpiryDate, OrgStatus, NewStatus)
     select 35, CardNumber, CouponNumber, CouponTypeID, null, null, '', CouponAmount, 0, CouponAmount,
        convert(varchar(10), GETDATE(), 120), GETDATE(), 'Delete Member', '', 1, CouponExpiryDate, '', null, '', '',
				CouponExpiryDate, Status, 5
     from Coupon where CardNumber in ( select CouponNumber from @Del3)   
                        
  -- 作废删除会员的Card
     insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, 
           BusDate, Txndate, TenderID, CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, 
           CreatedBy, StoreID, ServerCode, RegisterCode, OrgStatus, NewStatus, OrgExpiryDate, NewExpiryDate)
     select 5, CardNumber, null, null, '', TotalAmount, 0, TotalAmount, 0, 
        convert(varchar(10), GETDATE(), 120), GETDATE(), null, null , null, '', 'Delete Member', '',  
         1, null, '', '', C.[Status], C.[Status], CardExpiryDate, CardExpiryDate
      from Card C        
      where C.CardNumber in ( select CardNumber from @Del3)   
  
  -- 删除前写入deletedmember表
   Insert into DeletedMember
    select C.CardNumber, M.CreatedOn, case when isnull(M.NickName,'')='' then ('SVA_' + MemberMobilePhone) else M.NickName end as NickName, 
	  M.MemberMobilePhone, MemberRegisterMobile, CardGradeID,
	  MemberSex, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth, M.UpdatedOn, 'D' as UpdateFlag  
	from @Del3 D left join Member M on D.MemberID = M.MemberID left join Card C on C.MemberID = M.MemberID
		       
  -- 删除会员Account
   delete from MemberMessageAccount where MemberID in (select MemberID from @Del3 )                     
                   
  -- 删除会员
   delete from Member where MemberID in (select MemberID from @Del3)
    
 -- 以下正常注册会员到期删除.
 
  -- 作废删除会员的Coupon
  
    insert into @Del2(CardNumber, MemberID)
    select C.CardNumber, C.MemberID from Card C 
        left join CardGrade G on C.CardGradeID = G.CardGradeID   --NonValidatedPeriodValue
        left join Member M on C.MemberID = M.MemberID
     where (M.MemberID is not null) and DATEADD(HH, isnull(G.NonValidatedPeriodValue,0), M.CreatedOn) < GETDATE()
          and M.NewMemberFlag = 1 and CHARINDEX('@', M.MemberRegisterMobile) > 0 
              
		insert into Coupon_Movement
				(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
				BusDate, Txndate, Remark, SecurityCode, CreatedBy, OrgExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
				NewExpiryDate, OrgStatus, NewStatus)
     select 35, CardNumber, CouponNumber, CouponTypeID, null, null, '', CouponAmount, 0, CouponAmount,
        convert(varchar(10), GETDATE(), 120), GETDATE(), 'Delete Member', '', 1, CouponExpiryDate, '', null, '', '',
				CouponExpiryDate, Status, 5
     from  Coupon where CardNumber in (select CardNumber from @Del2)   
                       
  -- 作废删除会员的Card
     insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, 
           BusDate, Txndate, TenderID, CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, 
           CreatedBy, StoreID, ServerCode, RegisterCode, OrgStatus, NewStatus, OrgExpiryDate, NewExpiryDate)
     select 5, CardNumber, null, null, '', TotalAmount, 0, TotalAmount, 0, 
        convert(varchar(10), GETDATE(), 120), GETDATE(), null, null , null, '', 'Delete Member', '',  
         1, null, '', '', C.[Status], C.[Status], CardExpiryDate, CardExpiryDate
      from Card C where C.CardNumber in (select CardNumber from @Del2)   
 
  -- 删除前写入deletedmember表
   Insert into DeletedMember
    select C.CardNumber, M.CreatedOn, case when isnull(M.NickName,'')='' then ('SVA_' + MemberMobilePhone) else M.NickName end as NickName, 
	  M.MemberMobilePhone, MemberRegisterMobile, CardGradeID,
	  MemberSex, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth, M.UpdatedOn, 'D' as UpdateFlag  
	from @Del2 D left join Member M on D.MemberID = M.MemberID left join Card C on C.MemberID = M.MemberID
	       
  -- 删除会员Account
   delete from MemberMessageAccount where MemberID in (select MemberID from @Del2)   
                   
  -- 删除会员
   delete from Member where MemberID in (select MemberID from @Del2)   


END

GO
