USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ConvertReturnStatus]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  PROCEDURE [dbo].[ConvertReturnStatus]
  @OrgReturnValue       int,         -- 输入的返回值
  @Status               int,         -- Coupon的状态值
  @NewReturnValue       int output   -- 增加状态值后的返回值
as
/****************************************************************************    
**  Name : POSCouponSelling
**  Description :  内部使用，细分返回状态值。  e.g 输入 -23，Couponstatus=1，返回： -2301
  declare @out int
  set @out = -22
  exec ConvertReturnStatus @out, 1, @out output
  print @out
**  Created by: Gavin @2014-06-05    
****************************************************************************/  
begin
  declare @StatusStr varchar(3), @InputStr varchar(10), @OutputStr varchar(10)
  set @StatusStr = '0' + cast(@Status as varchar)
  set @InputStr = cast(abs(@OrgReturnValue) as varchar)
  set @OutputStr = @InputStr + @StatusStr 
  set @NewReturnValue = -1 * CAST(@OutputStr as int)
end

GO
