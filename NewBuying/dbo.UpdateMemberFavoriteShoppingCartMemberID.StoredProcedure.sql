USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateMemberFavoriteShoppingCartMemberID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[UpdateMemberFavoriteShoppingCartMemberID]
  @TempMemberID         bigint,
  @MemberID             int                 -- 会员ID
AS
/****************************************************************************
**  Name : UpdateMemberFavoriteShoppingCartMemberID  
**  Version: 1.0.0.1
**  Description : 临时顾客, 使用已经注册号的 memberID 登录时, 把临时购物车中内容 update过去.
**  Parameter :
  declare @a int , @FavoriteCount int, @ShoppingCartCount int  
  exec @a = UpdateMemberFavoriteShoppingCartMemberID 837, 1, @FavoriteCount output, @ShoppingCartCount output
  print @a
  print @FavoriteCount 
  print @ShoppingCartCount  

**  Created by: Gavin @2016-10-17
**
****************************************************************************/
begin
  set @MemberID = isnull(@MemberID, 0)
  set @TempMemberID = isnull(@TempMemberID, 0)
  update MemberFavorite set MemberID = @MemberID where MemberID = @TempMemberID
  update MemberShoppingCart set MemberID = @MemberID where MemberID = @TempMemberID
  return 0
end

GO
