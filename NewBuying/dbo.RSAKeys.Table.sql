USE [NewBuying]
GO
/****** Object:  Table [dbo].[RSAKeys]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RSAKeys](
	[PrivateKey] [varbinary](1024) NULL,
	[PublicKey] [varbinary](1024) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密钥对，私钥' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSAKeys', @level2type=N'COLUMN',@level2name=N'PrivateKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密钥对，公钥' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSAKeys', @level2type=N'COLUMN',@level2name=N'PublicKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期（这一天属于有效）。同一时间，有且只有一对生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSAKeys', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期（这一天属于有效）。同一时间，有且只有一对生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSAKeys', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RSA密钥存放表，素数：4001,4003' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSAKeys'
GO
