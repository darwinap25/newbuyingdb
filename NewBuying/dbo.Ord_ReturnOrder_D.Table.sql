USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ReturnOrder_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ReturnOrder_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ReturnOrderNumber] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[StockTypeCode] [varchar](64) NULL DEFAULT ('G'),
	[ReturnQty] [int] NOT NULL,
	[Remark] [varchar](512) NULL,
 CONSTRAINT [PK_ORD_RETURNORDER_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ReturnOrder_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ReturnOrder_D', @level2type=N'COLUMN',@level2name=N'ReturnOrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ReturnOrder_D', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BUY_STOCKTYPE 表的 StockTypeCode。 （类似prodcode， 特殊意义表，使用code，不使用ID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ReturnOrder_D', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ReturnOrder_D', @level2type=N'COLUMN',@level2name=N'ReturnQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ReturnOrder_D', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'退货单明细
@20150925 增加stocktypecode，相关SP和UI都还没有修改。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ReturnOrder_D'
GO
