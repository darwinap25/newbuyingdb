USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetPromotionMsgTypeList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetPromotionMsgTypeList]  
  @PromotionMsgTypeID          int,        --主键ID  
  @BrandID          int,                   --品牌ID 
  @IncludeSub       int,                   --是否包括子类型。 0：不包含子。 1：包含下一级。              
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。   
  @LanguageAbbr			varchar(20)=''
AS
/****************************************************************************
**  Name : GetPromotionMsgTypeList
**  Version: 1.0.0.3
**  Description :查找SVA中的GetPromotionMsgTypeList列表
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetPromotionMsgTypeList 2, 0, 0, 1, 30, @count output, @recordcount output, 1
  print @a  
  print @count
  print @recordcount
  select * from PromotionMsgType
**  Created by: Gavin @2013-01-29
**  Modify by: Gavin @2013-03-13 (ver 1.0.0.2) PromotionMsgType表结构修改，改成树状结构，增加输入参数。 
**  Modify by: Gavin @2013-03-15 (ver 1.0.0.3) @PromotionMsgTypeID=0时，如果@IncludeSub=0，则只取第一级的。	@IncludeSub=1时取全部
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int, @PromotionMsgTypeStr varchar(20)
  
  set @PromotionMsgTypeID = isnull(@PromotionMsgTypeID, 0)
  set @BrandID = isnull(@BrandID, 0)
  set @IncludeSub = isnull(@IncludeSub, 0)
  set @PromotionMsgTypeStr = cast(@PromotionMsgTypeID as varchar)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  
  if @PromotionMsgTypeID = 0
  begin
     set @SQLStr =  'select case ' + cast(@Language as varchar) + ' when 2 then PromotionMsgTypeName2 when 3 then PromotionMsgTypeName3 else PromotionMsgTypeName1 end as PromotionMsgTypeName, '
           + ' PromotionMsgTypeID, BrandID, PromotionMsgTypeName1, PromotionMsgTypeName2, PromotionMsgTypeName3 '
           + ' from PromotionMsgType where (BrandID = '	+ cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + ' = 0) '   
    if @IncludeSub = 0  
      set @SQLStr = @SQLStr + ' and (isnull(ParentID, 0) = 0) '
          
  end else
  begin
    set @SQLStr =  'select case ' + cast(@Language as varchar) + ' when 2 then PromotionMsgTypeName2 when 3 then PromotionMsgTypeName3 else PromotionMsgTypeName1 end as PromotionMsgTypeName, '
         + ' PromotionMsgTypeID, BrandID, PromotionMsgTypeName1, PromotionMsgTypeName2, PromotionMsgTypeName3 '
         + ' from PromotionMsgType where (BrandID = '	+ cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + ' = 0) ' 
         + ' and PromotionMsgTypeID = '	+ cast(@PromotionMsgTypeID as varchar) 
    if @IncludeSub = 1
    begin
    set @SQLStr = @SQLStr + ' union all '
         + ' select case ' + cast(@Language as varchar) + ' when 2 then PromotionMsgTypeName2 when 3 then PromotionMsgTypeName3 else PromotionMsgTypeName1 end as PromotionMsgTypeName, '
         + ' PromotionMsgTypeID, BrandID, PromotionMsgTypeName1, PromotionMsgTypeName2, PromotionMsgTypeName3 '
         + ' from PromotionMsgType where (BrandID = '	+ cast(@BrandID as varchar) + ' or ' + cast(@BrandID as varchar) + ' = 0) ' 
         + ' and (PromotionMsgTypeID in (select PromotionMsgTypeID from PromotionMsgType where ParentID = ' + @PromotionMsgTypeStr + ')) '
    end  
  end 
       
  exec SelectDataInBatchs @SQLStr, 'PromotionMsgTypeID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', ''
  return 0
end

GO
