USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[DecryptRSAKey]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DecryptRSAKey]
   @SecurityCode  varbinary(1024),
   @ClearCode varchar(1024) output
AS
/****************************************************************************
**  Name : DecryptRSAKey
**  Version: 1.0.0.0
**  Description : 解密table中的RSA key
declare @B varchar(1024), @C varbinary(1024) ,@aa int
select top 1 @C = PrivateKey from RSAKeys  where startdate <= getdate() and enddate >= getdate() order by startdate
--select top 1 @C = PublicKey from RSAKeys where startdate <= getdate() and enddate >= getdate() order by startdate
--set @C = 0x008763013DC51A49A50864CB2E1E6C6E010000002AE79369900392C4C98D8C15820EE12F0467950212FAA5BC
exec @aa = DecryptRSAKey @C, @B output
print @B
print @aa
44931
3

  OPEN SYMMETRIC KEY RSADESKey DECRYPTION BY PASSWORD='rrg1$th3b3$t'
  print DECRYPTBYKEY(0x008763013DC51A49A50864CB2E1E6C6E0)
  print CAST(DECRYPTBYKEY(0x008763013DC51A49A50864CB2E1E6C6E010000002AE79369900392C4C98D8C15820EE12F0467950212FAA5BC) AS VARCHAR(1024))
**  Created by: Gavin @2012-08-21
**
****************************************************************************/
begin
  --OPEN SYMMETRIC KEY RSADESKey DECRYPTION BY PASSWORD='abc@123'
  OPEN SYMMETRIC KEY RSADESKey DECRYPTION BY PASSWORD='rrg1$th3b3$t'
  set @ClearCode = CAST(DECRYPTBYKEY(@SecurityCode) AS VARCHAR(1024))
  return 0
end

GO
