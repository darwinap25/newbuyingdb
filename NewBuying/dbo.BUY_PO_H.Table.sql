USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PO_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PO_H](
	[POCode] [varchar](64) NOT NULL,
	[OrderType] [int] NOT NULL,
	[VendorID] [int] NOT NULL,
	[StoreID] [int] NULL,
	[CurrencyCode] [varchar](64) NULL,
	[Note1] [nvarchar](512) NULL,
	[Note2] [nvarchar](512) NULL,
	[Note3] [nvarchar](512) NULL,
	[ExpectDate] [datetime] NOT NULL,
	[TotalAmount] [dbo].[Buy_Amt] NOT NULL,
	[VATAmount] [dbo].[Buy_Amt] NOT NULL,
	[DiscountAmount] [dbo].[Buy_Amt] NOT NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_PO_H] PRIMARY KEY CLUSTERED 
(
	[POCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_BUY_PO_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_BUY_PO_H] ON [dbo].[BUY_PO_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_BUY_PO_H
* Version: 1.0.0.1
* Description :
     select * from Ord_ReturnOrder_H
     update Ord_ReturnOrder_H set approvestatus = 'A' where StoreOrderNumber = 'COPO00000000012'
* Create By Gavin @2015-03-27
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
*/
/*==============================================================*/
BEGIN  
  DECLARE @POCode varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  DECLARE @Busdate DATE, @StoreID int, @StoreCode varchar(64)

  DECLARE CUR_BUY_PO_H  CURSOR fast_forward FOR
    SELECT POCode, ApproveStatus, CreatedBy, StoreID FROM INSERTED
  OPEN CUR_BUY_PO_H
  FETCH FROM CUR_BUY_PO_H INTO @POCode, @ApproveStatus, @CreatedBy, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where POCode = @POCode
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
	  select @StoreCode = StoreCode from BUY_STORE WHERE StoreID = @StoreID
	  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC

      exec GenApprovalCode @ApprovalCode output
      EXEC GenHQReceiveOrder @POCode, 1
      update BUY_PO_H set ApprovalCode = @ApprovalCode, ApproveOn = GETDATE(), ApproveBusDate = @Busdate --, ApproveBy = @CreatedBy
        where POCode = @POCode
    end

    FETCH FROM CUR_BUY_PO_H INTO @POCode, @ApproveStatus, @CreatedBy, @StoreID
  END
  CLOSE CUR_BUY_PO_H 
  DEALLOCATE CUR_BUY_PO_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'POCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型： 1:WH order 3:Store Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'VendorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货币编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'CurrencyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'Note1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'Note2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'Note3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预期装货日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'ExpectDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单总金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'TotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单增值税总额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'VATAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单折扣总额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'DiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'ApproveOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'ApproveBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品订货单主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PO_H'
GO
