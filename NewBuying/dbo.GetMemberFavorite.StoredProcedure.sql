USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberFavorite]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetMemberFavorite]
  @MemberID             bigint,               -- 会员ID
  @FavoriteType         int = 0,            -- 收藏信息的类型。 默认0, 1:指收藏货品。输入0 表示忽略此条件
  @ConditionStr         nvarchar(400),
  @PageCurrent          int=1,-- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize             int=0,             -- 每页记录数， 为0时，不分页，默认0
  @PageCount            int=0 output,      -- 返回总页数。
  @RecordCount          int=0 output,      -- 返回总记录数。
  @LanguageAbbr         varchar(20)=''    
AS
/****************************************************************************
**  Name : GetMemberFavorite  
**  Version: 1.0.0.5
**  Description : 获取会员的收藏信息 (同一个memberid的 购物车和收藏中，product必须唯一)
**  Parameter :
  declare @a int , @PageCount int, @RecordCount int  
  exec @a = GetMemberFavorite 1037, 0, '', 1,0, @PageCount output, @RecordCount output,  ''
  print @a
  print @PageCount 
  print @RecordCount  
	select * from   MemberFavorite where memberid = 1037
**  Created by: Gavin @2013-12-20
**  Modified by: Gavin @2013-12-26 (ver 1.0.0.1) 增加分页
**  Modified by: Gavin @2014-01-07 (ver 1.0.0.2) 增加返回UpdatedOn字段，用户可以根据此字段自定义过滤条件。
**  Modified by: Gavin @2015-08-19 (ver 1.0.0.3) MemberFavorite增加prodcode, 和MemberShoppingCart关联时使用prodcode
**  Modified by: Gavin @2016-08-18 (ver 1.0.0.4) 调用方无法读取 varchar(max)类型， 所以转换成TEXT的。（调用方使用的是旧版2005）
**  Modified by: Gavin @2016-10-17 (ver 1.0.0.5) @MemberID 类型由int改为 bigint
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  if @Language = 2
  
  set @MemberID = isnull(@MemberID, 0)
  set @FavoriteType = isnull(@FavoriteType, 0)
  set @SQLStr = ' select F.KeyID, F.MemberID, F.FavoriteType, F.ProdCodeStyle, F.ProdStyleDesc, F.ProdStylePIC, cast(F.Memo as text) as Memo, '
            + ' case C.KeyID when null then 0 else C.KeyID end as IsShoppingCart, F.ProdStyleTitle, F.UpdatedOn, F.ProdCode '
            + ' from MemberFavorite F '
            --+ ' left join (select max(KeyID) as KeyID, ProdCodeStyle from MemberShoppingCart where MemberID = ' + cast(@MemberID as varchar) + ' group by ProdCodeStyle) C  on C.ProdCodeStyle = F.ProdCodeStyle '
            + ' left join (select max(KeyID) as KeyID, ProdCode from MemberShoppingCart where MemberID = ' + cast(@MemberID as varchar) + ' group by ProdCode) C  on C.ProdCode = F.ProdCode '
            + ' where F.MemberID = ' + cast(@MemberID as varchar) + ' and ((FavoriteType = ' + cast(@FavoriteType as varchar) + ') or (' + cast(@FavoriteType as varchar) + ' = 0)) '
  exec SelectDataInBatchs @SQLStr, 'KeyID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr     
  return 0
end

GO
