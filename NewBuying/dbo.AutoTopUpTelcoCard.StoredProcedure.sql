USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoTopUpTelcoCard]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[AutoTopUpTelcoCard]
  @UserID               int,                     -- 操作用户ID
  @InventoryReplenishCode varchar(1000)='',        -- 自动充值规则表Code, 可能会给多个，用 ,  隔开
  @ReferenceNo          varchar(64) = '',        -- 相关单号。（如果有的话）
  @BusDate              datetime = null,         -- 交易日
  @TxnDate              datetime = null,         -- 当前交易时间
  @IsSchedule           int=0                    -- 是否 日程安排 自动执行. 0:不是. 1: 是的  
AS
/****************************************************************************
**  Name : AutoTopUpTelcoCard
**  Version: 1.0.0.9
**  Description : 自动充值(根据规则设置自动产生充值顶单) (for RRG  暂定)
                  用于自动充值的卡. 每种CardType, 每种CardGrade，每个店铺只能有 1 张。 多于1张，后果不能预测。
    exec AutoTopUpTelcoCard 1, ''
   
** 
**  Created by:  Gavin @2014-11-28
**  Modify by Gavin @2015-01-30 (ver 1.0.0.1) 取消使用 Ord_PurchaseOrder_H, Ord_PurchaseOrder_D, 改为使用新的Ord_OrderToSupplier_Card_H, Ord_OrderToSupplier_Card_D
**  Modify by Gavin @2015-02-12 (ver 1.0.0.2) 主表增加PurchaseType. 如果是店铺订单，由Ord_CardTransfer_H 改成写入Ord_CardOrderForm_H表。
**  Modify by Gavin @2015-02-25 (ver 1.0.0.3) 使用InventoryReplenishRule_H 表.
**  Modify by Gavin @2015-04-16 (ver 1.0.0.4) 因为insert Detail表有判断. 所以先insert detail表,如果有数据产生,再写入head表
**  Modify by Gavin @2015-04-30 (ver 1.0.0.5) 修改Telco补货数量计算规则：（总金额 = 店铺订单金额（已经批核，picking单还未批核） + MinAMount（rule）- Card.TotalAMount） 
**  Modify by Gavin @2015-05-07 (ver 1.0.0.6) StoreOrderAmount 金额改成从 ord_cardorderform 状态为P 的 订单读取。
**  Modify by Gavin @2015-05-08 (ver 1.0.0.7) 产生的Ord_CardOrderForm_H单, 直接是批核状态  
                                              给供应商的订单,每个订单只能有一条detail记录,使用内存表     
                                              Ord_CardOrderForm_H 状态条件 改成 A 
**  Modify by Gavin @2015-06-23 (ver 1.0.0.8) 过滤amount为0 的记录.    
**  Modify by Gavin @2015-12-16 (ver 1.0.0.9) 提交的金额自动向上取整.                                                                              
****************************************************************************/

begin
  declare @StoreID int, @StoreTypeID int, @OrderTargetID int, @StoreName varchar(512), @StoreEmail varchar(512), 
          @StoreFullDetail varchar(512), @StoreContact varchar(512), @StoreContactPhone varchar(512)
  declare @TStoreName varchar(512), @TStoreEmail varchar(512), 
          @TStoreFullDetail varchar(512), @TStoreContact varchar(512), @TStoreContactPhone varchar(512)        
  declare @CardTypeID int, @CardGradeID int, @CardNumber varchar(64), @MinBalance money, @RunningBalance money, 
        @HQCard varchar(64), @StoreBalance money, @HQBalance money, @HQStoreID int
  declare @ActAmt money, @NewNumber varchar(64), @ActPoint int, @ReplenishType int, @CompanyID int
  declare @OrderToSupplierDetail table (KeyID int identity(1,1), CardTypeID int, CardGradeID int, OrderQty int , FirstCardNumber varchar(64), 
            EndCardNumber varchar(64), BatchCardCode varchar(64), PackageQty int, OrderRoundUpQty int , OrderAmount money, OrderPoint int)
  declare @OrdCardOrderFormD table(CardOrderFormNumber varchar(64), CardTypeID int, CardGradeID int, CardQty int, OrderAmount money, OrderPoint int)
  declare @KeyID int
  
  select Top 1 @CompanyID = CompanyID from Company Order by IsDefault desc
  set @InventoryReplenishCode = ISNULL(@InventoryReplenishCode, '')
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())
  set @TxnDate=getdate() 
      
  DECLARE CUR_TelcoCardAutoTopUp CURSOR fast_forward FOR
    select D.StoreID, H.StoreTypeID, D.OrderTargetID, max(S.StoreName1), max(S.Email), max(S.StoreFullDetail), 
        max(S.Contact), max(S.ContactPhone), D.ReplenishType, max(T.StoreName1), max(T.Email), max(T.StoreFullDetail), max(T.Contact), max(T.ContactPhone)
      from InventoryReplenishRule_D D 
        left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
        left join Store S on D.StoreID = S.StoreID 
        left join Store T on D.OrderTargetID = T.StoreID
      where (@InventoryReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @InventoryReplenishCode) > 0)
        and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1
        and ( (@IsSchedule = 0) or 
              (
                    ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
                and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
                and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		          )        
            ) 
        and H.PurchaseType = 2 and H.MediaType = 2         
    group by D.OrderTargetID, H.StoreTypeID, D.StoreID, D.ReplenishType          
  OPEN CUR_TelcoCardAutoTopUp
  FETCH FROM CUR_TelcoCardAutoTopUp INTO @StoreID, @StoreTypeID, @OrderTargetID, @StoreName, @StoreEmail, @StoreFullDetail, 
      @StoreContact, @StoreContactPhone, @ReplenishType, @TStoreName, @TStoreEmail, @TStoreFullDetail, 
      @TStoreContact, @TStoreContactPhone
  WHILE @@FETCH_STATUS=0
  BEGIN          
      if @StoreTypeID = 1
      begin
        -- exec GetRefNoString 'TELCO', @NewNumber output 
        delete from @OrderToSupplierDetail      
        insert into @OrderToSupplierDetail(CardTypeID, CardGradeID, OrderQty, FirstCardNumber, 
            EndCardNumber, BatchCardCode, PackageQty, OrderRoundUpQty, OrderAmount, OrderPoint)
        select H.CardTypeID, H.CardGradeID, 1, CC.CardNumber, CC.CardNumber, max(B.BatchCardCode), 1,1,
            case D.ReplenishType when 2 then max(isnull(D.MinAmtBalance, 0)) - max(isnull(CC.TotalAmount, 0)) - MAX(isnull(X.SupplierAmount, 0)) +  MAX(isnull(Y.StoreOrderAmount, 0)) else 0 end,
            case D.ReplenishType when 3 then max(isnull(D.RunningPointBalance, 0)) - max(isnull(CC.TotalPoints, 0)) - MAX(isnull(X.SupplierPoint, 0)) else 0 end   
         from InventoryReplenishRule_D D left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
          left join Store S on D.StoreID = S.StoreID 
          left join Card CC on D.StoreID = CC.IssueStoreID and H.CardTypeID = CC.CardTypeID and H.CardGradeID = CC.CardGradeID and CC.Status = 2        
          left join BatchCard B on B.BatchCardID = CC.BatchCardID
          left join (select D.CardTypeID, D.CardGradeID, sum(D.OrderAmount) as SupplierAmount, sum(D.OrderPoint) as SupplierPoint, H.StoreID 
                     from Ord_OrderToSupplier_Card_D D left join Ord_OrderToSupplier_Card_H H on D.OrderSupplierNumber = H.OrderSupplierNumber 
                     where approvestatus = 'P' and H.PurchaseType = 2
                     group by D.CardTypeID, D.CardGradeID, H.StoreID) X on H.CardTypeID = X.CardTypeID and H.CardGradeID = X.CardGradeID and D.StoreID = X.StoreID                
          left join (select D.CardTypeID, D.CardGradeID, sum(D.OrderAmount) as StoreOrderAmount, sum(D.OrderPoint) as StoreOrderPoint, H.FromStoreID 
                     from Ord_CardOrderForm_D D left join Ord_CardOrderForm_H H on D.CardOrderFormNumber = H.CardOrderFormNumber  
                     where approvestatus = 'A' and H.PurchaseType = 2
                     group by D.CardTypeID, D.CardGradeID, H.FromStoreID) Y on H.CardTypeID = Y.CardTypeID and H.CardGradeID = Y.CardGradeID and D.StoreID = Y.FromStoreID      
          where D.StoreID = @StoreID and D.OrderTargetID = @OrderTargetID
             and (@InventoryReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @InventoryReplenishCode) > 0)
             and ( (isnull(CC.TotalAmount, 0) + isnull(X.SupplierAmount, 0) - isnull(Y.StoreOrderAmount,0) <= isnull(D.MinAmtBalance, 0) and isnull(D.MinAmtBalance, 0) >= 0)
                    or (isnull(CC.TotalPoints, 0) + isnull(X.SupplierPoint, 0) <= isnull(D.MinPointBalance, 0) and isnull(D.MinPointBalance, 0) > 0)
                 )
             and H.AutoCreateOrder = 1
             and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1
             and ( (@IsSchedule = 0) or 
                   (
                          ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
                      and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
                      and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		               )        
                 ) 
             and H.PurchaseType = 2 and H.MediaType = 2          
        group by H.CardTypeID, H.CardGradeID, CC.CardNumber, D.ReplenishType  
        
        DECLARE CUR_earchdetail CURSOR fast_forward FOR
          SELECT KeyID FROM @OrderToSupplierDetail where isnull(OrderAmount, 0) <> 0
        OPEN CUR_earchdetail
        FETCH FROM CUR_earchdetail INTO @KeyID
        WHILE @@FETCH_STATUS=0
        BEGIN 
          exec GetRefNoString 'TELCO', @NewNumber output
          insert into Ord_OrderToSupplier_Card_D(OrderSupplierNumber, CardTypeID, CardGradeID, OrderQty, FirstCardNumber, 
            EndCardNumber, BatchCardCode, PackageQty, OrderRoundUpQty, OrderAmount, OrderPoint)       
          select @NewNumber, CardTypeID, CardGradeID, OrderQty, FirstCardNumber, 
               EndCardNumber, BatchCardCode, PackageQty, OrderRoundUpQty, ceiling(OrderAmount), OrderPoint
            from @OrderToSupplierDetail where KeyID = @KeyID

          insert into Ord_OrderToSupplier_Card_H(OrderSupplierNumber, SupplierID, OrderSupplierDesc, ReferenceNo, SendMethod, 
              SendAddress,SupplierAddress, SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreID, 
              StoreContactName, StorePhone, StoreEmail, StoreMobile, ApproveStatus, Remark, IsProvideNumber, OrderType,  
              CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CompanyID, PackageQty, PurchaseType)   
          select @NewNumber, SupplierID, SupplierDesc1, @ReferenceNo, 1, 
              @StoreFullDetail, Supplieraddress, Contact, ContactPhone, ContactEmail, ContactMobile, @StoreID,
              @StoreContact, @StoreContactPhone, @StoreEmail, '', 'P', 'Telco Auto Top Up(HQ)', 0, 1,          
		          @BusDate, getdate(), @UserID, getdate(), @UserID, @CompanyID, 0, 2
            from Supplier where SupplierID = @OrderTargetID  

          FETCH FROM CUR_earchdetail INTO @KeyID   
        END
        CLOSE CUR_earchdetail 
        DEALLOCATE CUR_earchdetail                    
      end 
      else if @StoreTypeID = 2
      begin
       
        exec GetRefNoString 'TELCO', @NewNumber output
 
        delete from @OrdCardOrderFormD           
        INSERT INTO @OrdCardOrderFormD(CardOrderFormNumber, CardTypeID, CardGradeID, CardQty, OrderAmount, OrderPoint)
        select @NewNumber, H.CardTypeID, H.CardGradeID, null,
            case D.ReplenishType when 2 then max(isnull(D.RunningAmtBalance, 0)) - max(isnull(CC.TotalAmount, 0)-isnull(Y.StoreOrderPickAmount,0)-ISNULL(Z.StoreOrderAmount,0)) else 0 end,
            case D.ReplenishType when 3 then max(isnull(D.RunningPointBalance, 0)) - max(isnull(CC.TotalPoints, 0)-isnull(Y.StoreOrderPickPoint,0)+ISNULL(Z.StoreOrderPoint,0)) else 0 end          
         from InventoryReplenishRule_D D left join InventoryReplenishRule_H H on H.InventoryReplenishCode = D.InventoryReplenishCode
          left join Store S on D.StoreID = S.StoreID 
          left join Brand B on S.BrandID = B.StoreBrandID
          left join Card CC on D.StoreID = CC.IssueStoreID and H.CardTypeID = CC.CardTypeID and H.CardGradeID = CC.CardGradeID and CC.Status = 2   
          left join (select D.CardTypeID, D.CardGradeID, sum(D.OrderAmount) as StoreOrderPickAmount, sum(D.OrderPoint) as StoreOrderPickPoint, H.StoreID from Ord_CardPicking_D D left join Ord_CardPicking_H H on D.CardPickingNumber = H.CardPickingNumber where H.approvestatus in ('P') and H.PurchaseType = 2 group by D.CardTypeID, D.CardGradeID, H.StoreID) Y on H.CardTypeID = Y.CardTypeID and H.CardGradeID = Y.CardGradeID and D.StoreID = Y.StoreID 
          left join (select D.CardTypeID, D.CardGradeID, sum(D.OrderAmount) as StoreOrderAmount, sum(D.OrderPoint) as StoreOrderPoint, H.StoreID from Ord_CardOrderForm_D D left join Ord_CardOrderForm_H H on D.CardOrderFormNumber = H.CardOrderFormNumber where H.approvestatus in ('P') and H.PurchaseType = 2 group by D.CardTypeID, D.CardGradeID, H.StoreID) Z on H.CardTypeID = Z.CardTypeID and H.CardGradeID = Z.CardGradeID and D.StoreID = Z.StoreID                     
          where D.StoreID = @StoreID and D.OrderTargetID = @OrderTargetID
             and (@InventoryReplenishCode = '' or CHARINDEX(H.InventoryReplenishCode, @InventoryReplenishCode) > 0)
             and ( (isnull(CC.TotalAmount, 0)+isnull(Y.StoreOrderPickAmount,0)+ISNULL(Z.StoreOrderAmount,0) <= isnull(D.MinAmtBalance, 0) and isnull(D.MinAmtBalance, 0) > 0)
                    or (isnull(CC.TotalPoints, 0)+isnull(Y.StoreOrderPickPoint,0)+ISNULL(Z.StoreOrderPoint,0) <= isnull(D.MinPointBalance, 0) and isnull(D.MinPointBalance, 0) > 0)
                 )
             and H.AutoCreateOrder = 1
             and H.StartDate <= GetDate() and H.EndDate >= (Getdate()-1) and H.Status = 1
             and ( (@IsSchedule = 0) or 
                   (
                          ( (isnull(H.DayFlagID, 0) = 0) or ( (isnull(H.DayFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.DayFlagID, 1, getdate(), 1) = 1) ) )
                      and ( (isnull(H.WeekFlagID, 0) = 0) or ( (isnull(H.WeekFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.WeekFlagID, 2, getdate(), 1) = 1) ) )
                      and ( (isnull(H.MonthFlagID, 0) = 0) or ( (isnull(H.MonthFlagID, 0) <> 0) and (dbo.CheckDateFlag(H.MonthFlagID, 3, getdate(), 1) = 1) ) )   
		               )        
                 )
             and H.PurchaseType = 2 and H.MediaType = 2           
        group by H.CardTypeID, H.CardGradeID, D.ReplenishType        

        insert into Ord_CardOrderForm_D(CardOrderFormNumber, CardTypeID, CardGradeID, CardQty, OrderAmount, OrderPoint)
        select CardOrderFormNumber, CardTypeID, CardGradeID, CardQty, ceiling(OrderAmount), OrderPoint
          from @OrdCardOrderFormD where isnull(CardQty, 0) <> 0 or isnull(OrderAmount, 0) <> 0 or isnull(OrderPoint, 0) <> 0 
        if @@ROWCOUNT <> 0
        begin    
          INSERT INTO Ord_CardOrderForm_H(CardOrderFormNumber, PurchaseType, BrandID, FromStoreID, StoreID, CustomerType, CustomerID, SendMethod, SendAddress, FromAddress, 
                FromContactName, FromContactNumber, FromEmail, FromMobile, StoreContactName, StoreContactPhone,
                StoreContactEmail, StoreMobile, ApproveStatus, Remark,   
                CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, OrderType, ApprovalCode, ApproveOn)
          values(@NewNumber, 2, 0, @OrderTargetID, @StoreID, 2, null, 1, @StoreFullDetail, @TStoreFullDetail, 
               @TStoreContact, @TStoreContactPhone, @TStoreEmail, @TStoreContactPhone, @StoreContact, @StoreContactPhone, 
               @StoreEmail, @StoreContactPhone, 'A', 'Telco Auto Top Up(ST)', 
               @BusDate, GETDATE(), @UserID, GETDATE(), @UserID, 1, '', null)         
        end
      end                 
   
    FETCH FROM CUR_TelcoCardAutoTopUp INTO @StoreID, @StoreTypeID, @OrderTargetID, @StoreName, @StoreEmail, @StoreFullDetail, 
        @StoreContact, @StoreContactPhone, @ReplenishType, @TStoreName, @TStoreEmail, @TStoreFullDetail, 
        @TStoreContact, @TStoreContactPhone
  END
  CLOSE CUR_TelcoCardAutoTopUp 
  DEALLOCATE CUR_TelcoCardAutoTopUp      
end

GO
