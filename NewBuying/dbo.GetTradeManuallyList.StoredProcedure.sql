USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetTradeManuallyList]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[GetTradeManuallyList]
  @MemberID int,                   -- 会员ID
  @CardNumber varchar(64),         -- 会员卡号，有卡号时以卡号为准。只有MemberID时，选第一个卡
  @ConditionStr			varchar(1000),  -- 条件字符串
  @OrderbyStr   varchar(400),         -- 排序字符串
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1    
AS
/****************************************************************************
**  Name : GetTradeManuallyList  
**  Version: 1.0.0.0
**  Description : 手动上传交易 (Kiosk使用的接口，一个Head 下只有一个 detail 记录)
**  Parameter :

  declare @a int, @PageCount int, @RecordCount int
  exec @a = GetTradeManuallyList null, '00030183', '', '', 1, 0, @PageCount output, @RecordCount output,''
  print @a
  
 select * from Ord_TradeManually_H
**  Created by: Gavin @2014-12-03
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
  set @MemberID = ISNULL(@MemberID, 0) 
  set @CardNumber = ISNULL(@CardNumber, '')
  if @MemberID = 0  and @CardNumber = ''
    return -8          -- 没有输入MemberID
  if @CardNumber = ''
    select top 1 @CardNumber = CardNumber from Card where MemberID = @MemberID

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
      
  set @SQLStr = 'select D.TradeManuallyCode, D.StoreID, D.TraderAmount, D.EarnPoint, D.ReferenceNo, D.Receipt, H.CardNumber, H.BrandID, '
       + ' H.Busdate, H.TenderID, H.Remark, H.ApprovalCode, H.ApproveStatus, H.CreatedOn, H.UpdatedOn '
       + ' , case ' + cast(@Language as varchar) + ' when 2 then S.StoreName2 when 3 then S.StoreName3 else StoreName1 end as StoreName '
       + ' from Ord_TradeManually_D D '
       + ' left join Ord_TradeManually_H H on H.TradeManuallyCode = D.TradeManuallyCode '
       + ' left join Store S on D.StoreID = S.StoreID '
       + ' where H.CardNumber = ''' + @CardNumber + ''''
      
  exec SelectDataInBatchs @SQLStr, 'TradeManuallyCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderbyStr, @ConditionStr  
 
end

GO
