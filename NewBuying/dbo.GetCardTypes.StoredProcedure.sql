USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCardTypes]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetCardTypes]
  @MemberID				int,       -- 会员ID  
  @ConditionStr			nvarchar(1000),     -- 查询条件  
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=0 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。  
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1
AS
/****************************************************************************
**  Name : GetCardTypes  
**  Version: 1.0.0.1
**  Description : 获得所有CardType数据, 不加过滤限制
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = GetCardTypes null, 'cardtypeid=''2''', 1, 30, @count output, @recordcount output, 1
  print @a  
  print @count
  print @recordcount

**  Created by: Gavin @2012-02-20
**  Modified by: Gavin @2013-06-21 (Ver 1.0.0.1) 使用1.0.0.6版本的SelectDataInBatchs	,输入的@ConditionStr直接传给SelectDataInBatchs 
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int

  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  if @Language = 2
    set @SQLStr = ' select CardTypeName2 as CardTypeName, * from ViewCardTypes '
  else if @Language = 3
    set @SQLStr = ' select CardTypeName3 as CardTypeName, * from ViewCardTypes '
  else
    set @SQLStr = ' select CardTypeName1 as CardTypeName, * from ViewCardTypes '        

  exec SelectDataInBatchs @SQLStr, 'CardTypeID', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', @ConditionStr
  
  return 0
end

GO
