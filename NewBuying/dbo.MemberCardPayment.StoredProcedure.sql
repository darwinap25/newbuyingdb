USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardPayment]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[MemberCardPayment]
  @UserID		        varchar(512),			-- 登录用户
  @CardNumber			varchar(512),			-- 会员卡号
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),            -- 终端号码
  @ServerCode           varchar(512)='',			-- serverid 暂时用不到,预留
  @BrandCode			varchar(512),
  @TxnNoKeyID           varchar(512),			-- ReceiveTxn 表主键
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间
  @OprID		        int,					-- 操作类型
  @SecurityCode         varchar(512),			-- 签名
  @Additional           varchar(200),			-- 附加信息（如果是银行卡做支付，此处记录mask card number）
  @ReturnAmount         money output,			-- 操作完成后卡金额余额
  @ReturnPoint          int output,				-- 操作完成后卡积分余额
  @CardExpiryDate       datetime output,			-- 操作完成后卡的有效期（金额最高有效期）。
  @PaymentList			nvarchar(max)			-- 支付清单。XML格式。
AS
/****************************************************************************
**  Name : MemberCardPayment
**  Version: 1.0.0.0
**  Description : 会员支付（online shopping 支付时使用）
**
  exec MemberCardPayment
@AttachFilePathList 格式:  
<ROOT>
  <PAYMENT TYPE= 1  CARDNUMBER= 23423423  VALUE= 20.00 ></PAYMENT>
  <PAYMENT TYPE= 3  CARDNUMBER= 23423423  VALUE= CN0100001 ></PAYMENT>
  <PAYMENT TYPE= 3  CARDNUMBER= 23423423  VALUE= CN0100002 ></PAYMENT>
</ROOT>
TYPE:1:金额。2：积分。3：coupon

**  Created by: Gavin @2012-02-07
**MemberClause
****************************************************************************/
begin 
  declare @MessageInfoID int, @CardTypeID int
  DECLARE @idoc int 
  
  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()       
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120) + cast(@OprID as varchar) 
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
    return @CheckResult
  -- end
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
      
/*    
  --Create an internal representation of the XML document.
  EXEC sp_xml_preparedocument @idoc OUTPUT, @PaymentList
  -- Execute a SELECT statement that uses the OPENXML rowset provider.
  if exists(SELECT * FROM OPENXML (@idoc, '/ROOT/PAYMENT', 1) WITH ([TYPE] int, CARDNUMBER varchar(512), VALUE varchar(512)))
  begin
      DECLARE CUR_MemberBindCard CURSOR FOR
        select CardTypeID from CardType
      OPEN CUR_MemberBindCard
      FETCH FROM CUR_MemberBindCard INTO @CardTypeID
      WHILE @@FETCH_STATUS=0
      BEGIN
        select Top 1 @NewCardNumber = CardNumber from Card where CardTypeID = @CardTypeID and Status = 5
        Update Card set Status = 1, MemberID = @NewMemberID, UpdatedOn = getdate() 
          where CardTypeID = @CardTypeID and CardNumber = @NewCardNumber            
        FETCH FROM CUR_MemberBindCard INTO @CardTypeID
      END
      CLOSE CUR_MemberBindCard 
      DEALLOCATE CUR_MemberBindCard       
    SELECT [TYPE], CARDNUMBER, VALUE FROM OPENXML (@idoc, '/ROOT/PAYMENT', 1) WITH ([TYPE] int, CARDNUMBER varchar(512), VALUE varchar(512))
  end  
                  
  select (MessageInfoID, MessageType, MessageSubType, FromMemberID, ToMemberID, Receiver, Status, CreatedOn, UpdatedOn)
  from SendMessage
*/  
  return 0 
end

GO
