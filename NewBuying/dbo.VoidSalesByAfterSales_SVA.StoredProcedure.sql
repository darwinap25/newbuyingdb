USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[VoidSalesByAfterSales_SVA]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VoidSalesByAfterSales_SVA]  
  @AfterSalesTxnNo				varchar(64),    -- 售后单号
  @RefSalesTxnNo				varchar(64),    -- 售后单中相关的交易单号
  @CreatedBy                    int
AS
/****************************************************************************
**  Name : VoidSalesByAfterSales_SVA
**  Version: 1.0.0.1
**  Description : (bauhaus需求) (SVA DB) 根据aftersales单，void原单 
**  Parameter :
**
declare @a int, @TxnNo varchar(64)
exec @a = VoidSalesByAfterSales @TxnNo output
print @a
print @TxnNo
** Create By Gavin @2017-03-15
**  Modify by: Gavin @2017-04-07 (ver 1.0.0.1) 原单的status 改为 2 （void） (2 是 取消. sva sales 和 buying sales 统一)
** 
****************************************************************************/
begin
  declare @MemberID int, @TotalAmt money, @CardNumber varchar(64), @CardOpenAmt money, @CardOpenPoint int, 
          @BusDate datetime, @TxnDate datetime, @StoreID int, @StoreCode varchar(64), @RegCode varchar(64), 
		  @ServCode varchar(64), @CardStatus int, @CardExpiryDate datetime

  select @MemberID = memberid, @BusDate = BusDate, @TxnDate = TxnDate, @StoreCode = StoreCode, 
         @RegCode = RegisterCode, @ServCode = ServerCode
     from sales_H where TransNum = @RefSalesTxnNo
  select @TotalAmt = sum(isnull(LocalAmount,0)) 
    from sales_T where TransNum = @RefSalesTxnNo
  select @CardNumber = CardNumber, @CardOpenAmt = TotalAmount, @CardOpenPoint = TotalPoints, 
         @CardStatus = Status, @CardExpiryDate = CardExpiryDate
    from Card where MemberID = @MemberID
  select @StoreID = StoreID from Store where StoreCode = @StoreCode 

  update Sales_H set status = 2, InvalidateFlag = 1, UpdatedOn = GETDATE(), UpdatedBy = @CreatedBy
  where TransNum = @RefSalesTxnNo
  
  -- 交易金额加入到member 的card 中
  if @TotalAmt > 0 
  begin
        insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
           CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
        values
          (8, @CardNumber, '', 0, @RefSalesTxnNo, @CardOpenAmt, @TotalAmt, @CardOpenAmt + @TotalAmt, 0, @BusDate, @TxnDate, 
          null, null, '', '', @CreatedBy, @StoreID, @RegCode, @ServCode,
          @CardOpenPoint, @CardOpenPoint, @CardExpiryDate, @CardExpiryDate, @CardStatus, @CardStatus)      
  end

  -- 作废原单，还需要对库存，原单产生的订单等，做进一步操作。
  -- 这部分到BUYING DB中做

end

GO
