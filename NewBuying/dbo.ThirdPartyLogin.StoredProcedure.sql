USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ThirdPartyLogin]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ThirdPartyLogin]  
  @MessageServiceTypeID int,                    --第三方类型。
  @AccountNumber        varchar(64),            --第三方账号  
  @TokenUID             varchar(64),            --第三方网页的时间戳  
  @TokenStr             varchar(512),           --第三方网页的Token  
  @BrandCode            varchar(64),            --品牌code，如果为空, 取任意一个cardnumber 返回.(这种情况下,一个会员只能有一种卡)
  @MemberFamilyName     varchar(512),           -- 会员姓 (本地语言)
  @MemberGivenName      varchar(512),           -- 会员名 (本地语言)
  @NickName             varchar(512),           -- 会员昵称 (本地语言)
  @MemberID             int output,             -- 返回Member表主键
  @CardNumber           varchar(64) output,     -- 返回Card表主键
  @IsNew                int output              -- 1: 新会员,需要提示用户完善资料。 0：已存在的会员
AS
/****************************************************************************
**  Name : 第三方会员登录.
**  Version: 1.0.0.1
**  Description : 用户提供第三方账号（比如QQ），根据MemberMessageAccount记录来判断登录。如果没有，则自动新增此Member。
**  Example :
  declare @MemberID int, @A int, @CardNumber varchar(64),  @IsNew int 
  exec @A = ThirdPartyLogin '1', '353544353', '233', '23432424', '', @MemberID output, @CardNumber output, @IsNew output
  print @A
  print @MemberID
  print @CardNumber
  print @IsNew
  select * from card where memberid  >0
  select * from member
    select * from useraction_movement
**  Created by: Gavin @2014-05-14
**  Modified by: Gavin @2014-05-22 (ver 1.0.0.1) 增加输入参数.(会员姓名)
**
****************************************************************************/
begin
  declare @CardTypeID int, @CardExpiryDate datetime, @CardStatus int
  declare @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate date, @NewCouponExpiryDate date, @CouponNumber varchar(64)
  declare @CardGradeID int, @BindCouponCount int, @i int, @ApprovalCode varchar(6), @BindCouponTypeID int
     
  if isnull(@AccountNumber, '') = ''
    return  -86
  set @MessageServiceTypeID = ISNULL(@MessageServiceTypeID,0)
  set @MemberID = null
  set @BrandCode = ISNULL(@BrandCode, '')
  select @MemberID = MemberID from MemberMessageAccount where MessageServiceTypeID = @MessageServiceTypeID and AccountNumber = @AccountNumber

  if isnull(@MemberID, 0) > 0   -- 已存在此会员
  begin
    if @BrandCode <> ''
      select Top 1 @CardNumber = CardNumber from Card C 
         Left join cardtype T on C.CardTypeID = T.CardTypeID left join Brand B on T.BrandID = B.StoreBrandID
      where C.MemberID = @MemberID and B.StoreBrandCode = @BrandCode
    else   
      select Top 1 @CardNumber = CardNumber from Card where MemberID = @MemberID    
        
    set @IsNew = 0  
    update MemberMessageAccount set TokenUID = @TokenUID, TokenStr = @TokenStr, UpdatedOn = getdate()  
    where MessageServiceTypeID = @MessageServiceTypeID and AccountNumber = @AccountNumber
  end else          -- -- 新会员，需自动添加。
  begin 
    set @IsNew = 1
    
    insert into member(MemberRegisterMobile, MemberEngFamilyName,MemberEngGivenName, MemberChiFamilyName,MemberChiGivenName, NickName, MemberSex,   
        MemberMobilePhone, MemberDateOfBirth, MemberDayOfBirth, MemberMonthOfBirth, MemberYearOfBirth, MemberPassword, CountryCode, ReferCardNumber)      
    values('', @MemberFamilyName, @MemberGivenName, @MemberFamilyName, @MemberGivenName, @NickName, 0,   
          '', 0, 0, 0, 0, '', '', '')      
    set @MemberID = SCOPE_IDENTITY()  
    
    insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, IsPrefer, Status, Note, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, TokenUID, TokenStr)
    values (@MemberID, @MessageServiceTypeID, @AccountNumber, 0, 1, '', Getdate(), 1, Getdate(), 1, @TokenUID, @TokenStr)
    
    if @BrandCode <> ''
      select Top 1 @CardNumber = CardNumber, @CardExpiryDate = C.CardExpiryDate, @CardStatus = C.Status from Card C 
         Left join cardtype T on C.CardTypeID = T.CardTypeID left join Brand B on T.BrandID = B.StoreBrandID
      where B.StoreBrandCode = @BrandCode and C.Status in (0,1) and isnull(C.MemberID, 0) = 0
    else   
       select Top 1 @CardNumber = CardNumber from Card where Status in (0,1) and isnull(MemberID, 0) = 0
              
      insert into Card_Movement
        (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
         CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
	     OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
      values
        (26, @CardNumber, null, 0, '', 0, 0, 0, 0, GETDATE(), GETDATE(), 
         null, null, @MemberID, null, '', @MemberID, null, null, null,
          0, 0,  @CardExpiryDate, @CardExpiryDate, @CardStatus, 2)  
             
          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR  
            select CouponTypeID, HoldCount from CardGradeHoldCouponRule where CardGradeID = @CardGradeID and RuleType = 1
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN            
            set @CouponNumber = ''
            set @i = 1
            DECLARE CUR_MemberBindCouponCount CURSOR fast_forward local FOR  
              select CouponNumber, CouponExpiryDate from coupon where CouponTypeID = @BindCouponTypeID and Status = 1 
            OPEN CUR_MemberBindCouponCount  
            FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate  
            WHILE @@FETCH_STATUS=0  
            BEGIN
              set @CouponExpiryDate = isnull(@CouponExpiryDate, getdate())
              if @i <= @BindCouponCount
              begin              
                if isnull(@CouponNumber, '') <> ''
                begin
                  exec GenApprovalCode @ApprovalCode output
                  exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                  exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                  insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                  select  
                      32, @CardNumber, CouponNumber, CouponTypeID, '', 0, '', CouponAmount, 0, CouponAmount,
                      getdate(), getdate(), '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2
                    from Coupon where CouponNumber = @CouponNumber
                end
                set @i = @i + 1
              end else
                break
              FETCH FROM CUR_MemberBindCouponCount INTO @CouponNumber, @CouponExpiryDate  
            END  
            CLOSE CUR_MemberBindCouponCount   
            DEALLOCATE CUR_MemberBindCouponCount                 
            
            FETCH FROM CUR_MemberBindCoupon INTO @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
                 
  end
  return 0
end

GO
