USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_ImportCouponUID_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_ImportCouponUID_H](
	[ImportCouponNumber] [varchar](64) NOT NULL,
	[ImportCouponDesc1] [nvarchar](512) NULL,
	[ImportCouponDesc2] [nvarchar](512) NULL,
	[ImportCouponDesc3] [nvarchar](512) NULL,
	[NeedActive] [int] NULL,
	[NeedNewBatch] [int] NULL,
	[CouponCount] [int] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
 CONSTRAINT [PK_ORD_IMPORTCOUPONUID_H] PRIMARY KEY CLUSTERED 
(
	[ImportCouponNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_ImportCouponUID_H] ADD  DEFAULT ((0)) FOR [NeedActive]
GO
ALTER TABLE [dbo].[Ord_ImportCouponUID_H] ADD  DEFAULT ((1)) FOR [NeedNewBatch]
GO
ALTER TABLE [dbo].[Ord_ImportCouponUID_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_ImportCouponUID_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Update_Ord_ImportCouponUID_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_ImportCouponUID_H] ON [dbo].[Ord_ImportCouponUID_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_ImportCouponUID_H
* Version: 1.0.0.3
* Description : 导入Coupon 的批核触发器
*  
* Create By Gavin @2012-06-04
* Modify By Gavin @2012-07-25 （ver 1.0.0.1）读入的CouponUID，删除最后一位后，作为CouponNumber
* Modify By Gavin @2012-08-16 （ver 1.0.0.2）写入Coupon_movement时，补上OrgStatus,NewStatus, 分别是NUll和0
* Modify By Gavin @2012-08-31 （ver 1.0.0.3）具体实现移到ImportCouponUID中, 触发器负责调用
*/
/*==============================================================*/
BEGIN  
  declare @ImportCouponNumber varchar(512), @NeedActive int, @NeedNewBatch int, @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1)
  
  DECLARE CUR_Ord_ImportCouponUID_H CURSOR fast_forward local FOR
    SELECT ImportCouponNumber, NeedActive, NeedNewBatch, ApproveStatus, ApproveBy FROM INSERTED
  OPEN CUR_Ord_ImportCouponUID_H
  FETCH FROM CUR_Ord_ImportCouponUID_H INTO @ImportCouponNumber, @NeedActive, @NeedNewBatch, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where ImportCouponNumber = @ImportCouponNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin    
      exec ImportCouponUID @ImportCouponNumber, @NeedActive, @NeedNewBatch, @ApproveStatus, @CreatedBy 
    end
    FETCH FROM CUR_Ord_ImportCouponUID_H INTO @ImportCouponNumber, @NeedActive, @NeedNewBatch, @ApproveStatus, @CreatedBy    
  END
  CLOSE CUR_Ord_ImportCouponUID_H 
  DEALLOCATE CUR_Ord_ImportCouponUID_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键。导入单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'ImportCouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'ImportCouponDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'ImportCouponDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'ImportCouponDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绑定时激活Coupon。0：不激活。1：激活' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'NeedActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否先创建新批次的coupon，使用这些新的coupon来绑定。
0：不创建，使用已有的coupon，没有足够coupon则报错。
1：先创建此数量的Coupon，再绑定这些coupon。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'NeedNewBatch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon的数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'CouponCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入Coupon的UID。（销售实体coupon时，导入实体coupon的号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_ImportCouponUID_H'
GO
