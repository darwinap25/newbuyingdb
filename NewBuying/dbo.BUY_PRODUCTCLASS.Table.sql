USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PRODUCTCLASS]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PRODUCTCLASS](
	[ProdClassID] [int] IDENTITY(1,1) NOT NULL,
	[ProdClassCode] [varchar](64) NULL,
	[ParentID] [int] NULL,
	[ProductSizeType] [varchar](64) NOT NULL,
	[ProdClassDesc1] [varchar](100) NULL,
	[ProdClassDesc2] [varchar](100) NULL,
	[ProdClassDesc3] [varchar](100) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_PRODUCTCLASS] PRIMARY KEY CLUSTERED 
(
	[ProdClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_PRODUCTCLASS] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_PRODUCTCLASS] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大类主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS', @level2type=N'COLUMN',@level2name=N'ProdClassID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大类Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS', @level2type=N'COLUMN',@level2name=N'ProdClassCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父ID。（树状结构）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS', @level2type=N'COLUMN',@level2name=N'ParentID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此大类货品使用的尺寸类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS', @level2type=N'COLUMN',@level2name=N'ProductSizeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大类描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS', @level2type=N'COLUMN',@level2name=N'ProdClassDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大类描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS', @level2type=N'COLUMN',@level2name=N'ProdClassDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大类描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS', @level2type=N'COLUMN',@level2name=N'ProdClassDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品大类' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTCLASS'
GO
