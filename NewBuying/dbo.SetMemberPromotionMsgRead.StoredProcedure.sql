USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetMemberPromotionMsgRead]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetMemberPromotionMsgRead]
  @MemberID          int,           -- 会员ID
  @PromotionMsgCode  varchar(64)    -- 促销消息code
AS
/****************************************************************************
**  Name : SetMemberPromotionMsgRead  
**  Version: 1.0.0.0
**  Description : 设置会员的促销消息为已读
**  Parameter :
      exec SetMemberPromotionMsgRead '000300001', ''
**  return: 
     
**  Created by: Gavin @2014-07-03
**
****************************************************************************/
begin 
  set @MemberID = ISNULL(@MemberID, 0)  
  set @PromotionMsgCode = ISNULL(@PromotionMsgCode, '')  
  if exists(select * from MemberPromotionMsgRead where PromotionMsgCode = @PromotionMsgCode and MemberID = @MemberID)
  begin
    update MemberPromotionMsgRead set IsRead = 1 where PromotionMsgCode = @PromotionMsgCode and MemberID = @MemberID
  end
  else
  begin
    insert MemberPromotionMsgRead
    values(@MemberID, @PromotionMsgCode, 1)
  end
  
  return 0
end

GO
