USE [NewBuying]
GO
/****** Object:  Table [dbo].[USEFULEXPRESSIONS]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USEFULEXPRESSIONS](
	[USEFULEXPRESSIONSID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](64) NULL,
	[PhraseTitle1] [nvarchar](512) NULL,
	[PhraseTitle2] [nvarchar](512) NULL,
	[PhraseTitle3] [nvarchar](512) NULL,
	[PhraseContent1] [nvarchar](512) NULL,
	[PhraseContent2] [nvarchar](512) NULL,
	[PhraseContent3] [nvarchar](512) NULL,
	[CampaignID] [int] NULL,
	[PhrasePicFile] [nvarchar](512) NULL,
 CONSTRAINT [PK_USEFULEXPRESSIONS] PRIMARY KEY CLUSTERED 
(
	[USEFULEXPRESSIONSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'USEFULEXPRESSIONSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短语编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'PhraseTitle1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'PhraseTitle2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'PhraseTitle3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'PhraseContent1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'PhraseContent2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'PhraseContent3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活动ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'CampaignID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短语图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS', @level2type=N'COLUMN',@level2name=N'PhrasePicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'常用语表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'USEFULEXPRESSIONS'
GO
