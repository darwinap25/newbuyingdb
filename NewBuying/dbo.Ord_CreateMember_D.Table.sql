USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CreateMember_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CreateMember_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CreateMemberNumber] [varchar](64) NOT NULL,
	[CountryCode] [varchar](64) NULL,
	[MobileNumber] [varchar](64) NULL,
	[EngFamilyName] [varchar](64) NULL,
	[EngGivenName] [varchar](64) NULL,
	[ChiFamilyName] [varchar](64) NULL,
	[ChiGivenName] [varchar](64) NULL,
	[Birthday] [datetime] NULL,
	[Gender] [int] NULL,
	[HomeAddress] [varchar](512) NULL,
	[Email] [varchar](64) NULL,
	[Facebook] [varchar](64) NULL,
	[QQ] [varchar](64) NULL,
	[MSN] [varchar](64) NULL,
	[Weibo] [varchar](64) NULL,
	[OtherContact] [varchar](64) NULL,
 CONSTRAINT [PK_ORD_CREATEMEMBER_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CreateMember_D] ADD  DEFAULT ((0)) FOR [Gender]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'CreateMemberNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'国家Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码（CountryCode+MobileNumber= 注册号）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'MobileNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员英文名（姓）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'EngFamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员英文名（名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'EngGivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员中文名（姓）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'ChiFamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员中文名（名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'ChiGivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（出生日期）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'Birthday'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员性别
null或0：保密。1：男性。 2：女性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'Gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'家庭地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'HomeAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FaceBook账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'Facebook'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QQ号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'QQ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MSN账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'MSN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'微博账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'Weibo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他联系方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D', @level2type=N'COLUMN',@level2name=N'OtherContact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建Member记录的子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CreateMember_D'
GO
