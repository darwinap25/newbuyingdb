USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_STOCKTYPE]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_STOCKTYPE](
	[StockTypeID] [int] IDENTITY(1,1) NOT NULL,
	[StockTypeCode] [varchar](64) NOT NULL,
	[StockTypeName1] [nvarchar](512) NULL,
	[StockTypeName2] [nvarchar](512) NULL,
	[StockTypeName3] [nvarchar](512) NULL,
	[NeedSerialno] [int] NULL,
	[SubInvSuffix] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_STOCKTYPE] PRIMARY KEY CLUSTERED 
(
	[StockTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_STOCKTYPE] ADD  DEFAULT ((0)) FOR [NeedSerialno]
GO
ALTER TABLE [dbo].[BUY_STOCKTYPE] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_STOCKTYPE] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE', @level2type=N'COLUMN',@level2name=N'StockTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型编码
G: 正常库存
F: 坏货库存
D: demo库存' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE', @level2type=N'COLUMN',@level2name=N'StockTypeName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型名称2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE', @level2type=N'COLUMN',@level2name=N'StockTypeName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型名称3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE', @level2type=N'COLUMN',@level2name=N'StockTypeName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要serialno.。0：不。 1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE', @level2type=N'COLUMN',@level2name=N'NeedSerialno'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'后缀' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE', @level2type=N'COLUMN',@level2name=N'SubInvSuffix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型表。
@2015-09-17  库存类别3种： G，F，D。 退货退回到 G  
@2016-10-18 库存类别 增加 R （reserve）。  销售时有reserve动作。  G -> R,  R -> ''''' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKTYPE'
GO
