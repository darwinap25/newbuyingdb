USE [NewBuying]
GO
/****** Object:  Table [dbo].[Member]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[MemberID] [int] IDENTITY(1,1) NOT NULL,
	[MemberPassword] [varchar](512) NULL,
	[MemberIdentityType] [int] NULL DEFAULT ((1)),
	[MemberIdentityRef] [nvarchar](512) NULL,
	[MemberRegisterMobile] [varchar](64) NOT NULL,
	[MemberMobilePhone] [varchar](64) NULL,
	[MemberEmail] [nvarchar](512) NULL,
	[NickName] [nvarchar](512) NULL,
	[MemberAppellation] [nvarchar](512) NULL,
	[MemberEngFamilyName] [nvarchar](512) NULL,
	[MemberEngGivenName] [nvarchar](512) NULL,
	[MemberChiFamilyName] [nvarchar](512) NULL,
	[MemberChiGivenName] [nvarchar](512) NULL,
	[MemberSex] [int] NULL DEFAULT ((0)),
	[MemberDateOfBirth] [datetime] NOT NULL,
	[MemberDayOfBirth] [int] NOT NULL,
	[MemberMonthOfBirth] [int] NOT NULL,
	[MemberYearOfBirth] [int] NOT NULL,
	[MemberMarital] [int] NULL DEFAULT ((0)),
	[HomeAddress] [nvarchar](512) NULL,
	[HomeTelNum] [nvarchar](512) NULL,
	[HomeFaxNum] [nvarchar](512) NULL,
	[OfficeAddress] [nvarchar](512) NULL,
	[OfficeTelNum] [nvarchar](512) NULL,
	[OfficeFaxNum] [nvarchar](512) NULL,
	[MemberPictureFile] [nvarchar](512) NULL,
	[EducationID] [int] NULL DEFAULT ((0)),
	[ProfessionID] [int] NULL,
	[MemberPosition] [nvarchar](512) NULL,
	[NationID] [int] NULL,
	[CompanyDesc] [nvarchar](512) NULL,
	[SpRemark] [nvarchar](512) NULL,
	[OtherContact] [nvarchar](512) NULL,
	[Hobbies] [nvarchar](512) NULL,
	[Status] [int] NULL DEFAULT ((1)),
	[MemberDefLanguage] [int] NULL,
	[PasswordExpiryDate] [datetime] NULL,
	[PWDExpiryPromptDays] [int] NULL,
	[CountryCode] [varchar](64) NULL,
	[ReadReguFlag] [int] NULL DEFAULT ((0)),
	[ResetPWDCount] [int] NULL DEFAULT ((0)),
	[ReceiveAllAdvertising] [int] NULL DEFAULT ((0)),
	[TransPersonalInfo] [int] NULL DEFAULT ((0)),
	[ViewHistoryTranDays] [int] NULL,
	[ReferCardNumber] [varchar](64) NULL,
	[AcceptPhoneAdvertising] [int] NULL DEFAULT ((0)),
	[ResetPWDVerCode] [varchar](64) NULL,
	[ResetPWDVerCodeTime] [datetime] NULL,
	[PWDFailCount] [int] NULL DEFAULT ((0)),
	[NewMemberFlag] [int] NULL DEFAULT ((1)),
	[Score] [char](4) NULL DEFAULT ('0000'),
	[PromotionFlagUpdatedOn] [datetime] NULL,
	[AgeBracklet] [int] NULL DEFAULT ((0)),
	[MobileVerifyCode] [varchar](64) NULL,
	[MobileVerifyCodeStamp] [datetime] NULL,
	[VIPFlag] [int] NULL DEFAULT ((0)),
	[MemberDefCurrency] [varchar](64) NULL,
	[NoVerifyEMail] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[MemberPicture] [varchar](512) NULL,
 CONSTRAINT [Member_PK] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Delete_Member]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Delete_Member] ON [dbo].[Member]
FOR DELETE
AS
/*==============================================================*/
/*                
* Name: Delete_Member
* Version: 1.0.0.0
* Description :删除Member记录时，同时删除相关的记录
*  
** Create By Gavin @2014-08-13
*/
/*==============================================================*/
BEGIN  
  declare @MemberID int
  
  DECLARE CUR_MemberDelete CURSOR fast_forward FOR
    SELECT  MemberID FROM Deleted  
  OPEN CUR_MemberDelete
  FETCH FROM CUR_MemberDelete INTO @MemberID
  WHILE @@FETCH_STATUS=0
  BEGIN    
    delete from MemberMessageAccount where MemberID = @MemberID 
    update Card set Status = 5 where MemberID = @MemberID 
    FETCH FROM CUR_MemberDelete INTO @MemberID
  END
  CLOSE CUR_MemberDelete 
  DEALLOCATE CUR_MemberDelete       
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员密码。（MD5加密）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员证件类型
1：身份证号码。2：军官证。 3：回乡证' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberIdentityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员证件号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberIdentityRef'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员注册用手机号（加上国家代码的，例如0861392572219）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberRegisterMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员手机号，不加国家代码（例如，1394592381）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberMobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员昵称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'NickName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员称呼：
Mr.  Miss.  Mrs.     或者 先生   小姐   夫人。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberAppellation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员英文名（姓）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberEngFamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员英文名（名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberEngGivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员中文名（姓）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberChiFamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员中文名（名）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberChiGivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员性别
null或0：保密。1：男性。 2：女性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberSex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（出生日期）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberDateOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（天）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberDayOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（月）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberMonthOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员生日（年）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberYearOfBirth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'婚姻情况。默认 0 . 0: 保密  1：未婚. 2：已婚。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberMarital'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'家庭地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'HomeAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'家庭电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'HomeTelNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'家庭传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'HomeFaxNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'办公室地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'OfficeAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'办公室电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'OfficeTelNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'办公室传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'OfficeFaxNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员照片文件名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberPictureFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键 EducationID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'EducationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员专业，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ProfessionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员职位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberPosition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号码所属国籍,外键ID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'NationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'CompanyDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'SpRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他联系方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'OtherContact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'兴趣爱好' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'Hobbies'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。 0：无效。 1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外键，LanguageMap表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberDefLanguage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡密码失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'PasswordExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡密码失效提示预警天数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'PWDExpiryPromptDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号的国家码。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'阅读条例的标志。默认为0.  0:未读。1：已读
（扩展：
   根据二进制，右起第一位表示是否读过条例。 第二位表示是否玩过游戏。
二进制    十进制       描述
00                 0            未读过条例，未玩过游戏
01                 1            读过条例，未玩过游戏
10                 2            未读过条例，玩过游戏
11                 3            读过条例，玩过游戏
）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ReadReguFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重置密码次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ResetPWDCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'同意接收所有广告. 默认0.     0：不接收所有广告（所有messageservicetypeid）。 1：同意接收。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ReceiveAllAdvertising'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'同意传播个人信息。0：不同意。 1：同意      默认0.       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'TransPersonalInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许查看历史交易的天数范围。默认0，表示不设限制。  数字表示允许查看指定天数内的交易' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ViewHistoryTranDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'推荐人的CardNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ReferCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否接收电话广告。默认0，不接收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'AcceptPhoneAdvertising'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重置密码时产生的验证码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ResetPWDVerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重置密码验证码的产生时间。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'ResetPWDVerCodeTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MemberLogin时，密码错误计数。累计数超过CardGrade中的LoginFailureCount，则当日不允许登录。登录成功后清0， EOD时清0. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'PWDFailCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否新会员标志。 0：不是。 1：是的。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'NewMemberFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未作用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'Score'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录用户的 MemberMessageAccount表->MessageServiceTypeID=1or2 和Member table表->AcceptPhoneAdverting 的修改时间。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'PromotionFlagUpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员年龄段：0 表示不填年龄段。 分段从1 开始
25-29
30-34
35-39
40-44
45-49
50-54
55-59
60 or above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'AgeBracklet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机验证码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MobileVerifyCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机验证码写入的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MobileVerifyCodeStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VIP标志。 1： 此会员的卡不会降级。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'VIPFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'buy_Currency表的主键。会员默认支付类型。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'MemberDefCurrency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未验证邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'NoVerifyEMail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员表
@2015-10-08 MemberMobilePhone 类型改成varchar(64)
@2015-10-13 MemberMobilePhone 上增加唯一索引。
@2016-01-21 增加 VIPFlag,    VIPFlag=1 表示 此会员的Card 不会降低级别。
@2016-04-07 增加 MemberDefCurrency，会员的默认支付方式
@2016-09-13 按照Nick要求，增加未验证邮箱字段（NoVerifyEMail）。用于在GenVerifyMessageStr 时，记录要验证的邮箱。 VerifyMemberAccount 时检测这个邮箱。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member'
GO
