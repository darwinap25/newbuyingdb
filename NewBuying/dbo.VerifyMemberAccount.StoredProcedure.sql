USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[VerifyMemberAccount]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[VerifyMemberAccount]   
  @VerifyStr             varchar(4000)       -- 验证字符串
AS
/****************************************************************************
**  Name : VerifyMemberAccount    
**  Version: 1.0.0.5
**  Description : 校验验证用户提交的加密信息，并更新memberMessageAccount 的验证标识
  declare @a int
  --exec @a = VerifyMemberAccount '4D656D62657249443D312C4D656D6265724163636F756E74547970653D322C4D656D6265724163636F756E743D313135373632353838382C566572696679446174653D323031332D30382D3230'
  exec @a = VerifyMemberAccount '4D656D62657249443D3236352C4D656D6265724163636F756E74547970653D312C4D656D6265724163636F756E743D3736353933393730394071712E636F6D2C566572696679446174653D323031362D30392D3133'
  print @a
   exec  VerifyMemberAccount '955955551'
    
**  Created by: Gavin @2013-08-20
**  Modify by: Gavin @2013-08-29 (ver 1.0.0.1) 增加自动给推荐人奖励功能. TxnNo 使用 REF_ + CardNumber + '_' + MemberID 的规则。同一个卡推荐同一个人只能有一次奖励机会
**  Modify by: Gavin @2013-09-09 (ver 1.0.0.2) 取消DoSVAReward的调用.
**  Modify by: Gavin @2014-11-06 (ver 1.0.0.3) 如果此邮箱已经验证,则返回error. （和验证出错使用相同的errorcode）
**  Modify by: Gavin @2014-11-20 (ver 1.0.0.4) 新增邮箱已经验证过的错误code (-85), 和找不到账号的错误分开(-86)
**  Modify by: Gavin @2016-09-13 (ver 1.0.0.5) 按照Nick要求，增加未验证邮箱字段（NoVerifyEMail）。用于在GenVerifyMessageStr 时，记录要验证的邮箱。 VerifyMemberAccount 时检测这个邮箱。
**
****************************************************************************/
begin
  declare @ClearText varchar(4000), @TempStr varchar(4000), @POS int, @NodeStr varchar(500), 
          @FieldName varchar(200), @FieldValue varchar(100)
  declare @MemberID int, @MemberAccountType int, @MemberAccountNo varchar(100), @VerifyDate datetime ,      
		  @ReferCardNumber varchar(64)
  
  exec DecryptString @VerifyStr, @ClearText output
  set @TempStr = @ClearText
   
    while len(@TempStr) > 0
    begin
      set @POS = CharIndex(',', @TempStr)
      if @POS > 0 
      begin
        set @NodeStr = substring(@TempStr, 1, @POS - 1)
	    set @TempStr = substring(@TempStr, @POS + 1, len(@TempStr) - @POS)
      end  else
      begin
        set @NodeStr = @TempStr
	    set @TempStr = ''
      end  
      if len(@NodeStr) > 0
      begin
        set @POS = CharIndex('=', @NodeStr)
        set @FieldName = substring(@NodeStr, 1, @POS - 1)
	    set @FieldValue = substring(@NodeStr, @POS + 1, len(@NodeStr) - @POS)
	    if @FieldName = 'MemberID'
	      set @MemberID = cast(@FieldValue as int)
	    if @FieldName = 'MemberAccountType'
	      set @MemberAccountType = cast(@FieldValue as int)
	    if @FieldName = 'MemberAccount'
	      set @MemberAccountNo = @FieldValue
	    if @FieldName = 'VerifyDate'
	      set @VerifyDate = cast(@FieldValue as datetime)	      	      	      
      end 
    end
/*  
  select @ReferCardNumber = ReferCardNumber from Member where MemberID = @MemberID
  if isnull(@ReferCardNumber, '') <> ''
  begin
    declare @temp varchar(200)
    set @temp = 'REF_' + @ReferCardNumber + '_' + cast(@MemberID as varchar)
	exec DoSVAReward @ReferCardNumber,  @temp, 1
  end
*/

 -- ver 1.0.0.5 只是验证是此邮箱发出的，不做update Membermessageaccount， 按照新流程，之后会调用setmemberinfo。
  if not exists(select * from Member where MemberID = @MemberID and NoVerifyEMail = @MemberAccountNo)
  begin
    return -86
  end else
    return 0
  
/*
  if not exists(select * from Membermessageaccount  
                  where MemberID = @MemberID and AccountNumber = @MemberAccountNo and MessageServiceTypeID = @MemberAccountType )
  begin
    return -86
  end else
  begin      
    update Membermessageaccount set VerifyFlag = 1 
       where MemberID = @MemberID and AccountNumber = @MemberAccountNo and MessageServiceTypeID = @MemberAccountType
          and isnull(VerifyFlag, 0) = 0    -- ver 1.0.0.3 加上isnull(VerifyFlag, 0) = 0的条件。W 
    if @@ROWCOUNT = 0
      return -85
    else     
      return 0
  end 
*/   
end

GO
