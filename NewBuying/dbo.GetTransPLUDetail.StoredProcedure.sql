USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetTransPLUDetail]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTransPLUDetail]
  @TransNum              VARCHAR(64),              -- 交易号。 空表示不设条件
  @TransType             INT,                      -- 交易类型。 0 表示不设条件
  @TransStatus           INT,                      -- 交易状态。 0 表示不设条件
  @StoreCode             VARCHAR(64),              -- POS 的店铺编号
  @RegisterCode          VARCHAR(64),              -- POS 的注册编号
  @BusDate               DATE,                     -- 交易的Business date
  @CashierID             INT,                      -- 收银员
  @ConditionStr          NVARCHAR(1000)='',        -- 自定义查询条件
  @OrderCondition	       NVARCHAR(1000)='',        -- 自定义排序条件
  @PageCurrent           INT=1,                    -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize              INT=0,                    -- 每页记录数， 为0时，不分页，默认0
  @PageCount             INT=0 OUTPUT,	           -- 返回总页数。
  @RecordCount           INT=0 OUTPUT,	           -- 返回总记录数。
  @LanguageAbbr			     VARCHAR(20)=''            -- 语言: en_CA, zh_CN, zh_BigCN  
AS
/****************************************************************************
**  Name : GetTransPLUDetail
**  Version : 1.0.0.0
**  Description : 获得交易数据(Sales_D)
**
  declare  @a int, @PageCount int, @RecordCount int
  exec @a = GetTransPLUDetail '',0,0, '','',null,0,   '','', 1, 0, @PageCount output, @RecordCount output, 'zh_CN'
  print @a
  select * from Sales_D
**  Created by Gavin @2015-03-06
****************************************************************************/
BEGIN
  DECLARE @Language int, @SQLStr NVARCHAR(4000), @WhereStr NVARCHAR(1000)
  SELECT @Language = DescFieldNo FROM LanguageMap WHERE LanguageAbbr = @LanguageAbbr
  IF isnull(@Language, 0) = 0
    SET @Language = 1  
  
  SET @TransNum = ISNULL(@TransNum, '')
  SET @TransType = ISNULL(@TransType, 0)
  SET @TransStatus = ISNULL(@TransStatus, 0)
  SET @CashierID = ISNULL(@CashierID, 0)
  SET @StoreCode = ISNULL(@StoreCode, '')
  SET @RegisterCode = ISNULL(@RegisterCode, '')
  SET @BusDate = ISNULL(@BusDate, '1900-01-01')  
  
  SET @WhereStr = ' WHERE 1=1 '
  IF @TransNum <> ''
    SET @WhereStr = @WhereStr + ' AND A.TransNum=''' + @TransNum + ''''
  IF @TransType <> 0
    SET @WhereStr = @WhereStr + ' AND A.TransType=' + CAST(@TransType as VARCHAR)
  IF @TransStatus <> 0
    SET @WhereStr = @WhereStr + ' AND H.Status=' + CAST(@TransStatus as VARCHAR)   
  IF @CashierID <> 0
    SET @WhereStr = @WhereStr + ' AND A.CashierID=' + CAST(@CashierID as VARCHAR)       
  IF @StoreCode <> ''
    SET @WhereStr = @WhereStr + ' AND A.StoreCode=''' + @StoreCode + ''''
  IF @RegisterCode <> ''
    SET @WhereStr = @WhereStr + ' AND A.RegisterCode=''' + @RegisterCode + ''''
  IF DATEDIFF(dd, @BusDate, '1900-01-01') < 0
    SET @WhereStr = @WhereStr + ' AND A.BusDate=''' + CONVERT(VARCHAR(10), @BusDate,120) + ''''

  SET @SQLStr = 'SELECT SeqNo, A.TransNum, A.TransType, H.Status, A.StoreCode, A.RegisterCode, A.BusDate, A.TxnDate, A.ProdCode, A.ProdDesc, A.DepartCode, '
    + ' CASE ' + CAST(@Language AS VARCHAR) + ' WHEN 2 THEN C.DepartName2 WHEN 3 THEN C.DepartName3 ELSE C.DepartName1 END AS DepartName, '
    + ' Qty, OrgPrice, UnitPrice, NetPrice, OrgAmount, UnitAmount, NetAmount, TotalQty, Collected, SerialNoType, SerialNo,IMEI,'
    + ' DiscountPrice, DiscountAmount, POPrice, ExtraPrice, Additional1, Additional2, Additional3,IsBOM, IsCoupon, PickupDate, A.DeliveryDate '
    + ' FROM SALES_D A '
    + ' LEFT JOIN SALES_H H ON A.TransNum = H.TransNum '    
    + ' LEFT JOIN BUY_PRODUCT B ON A.ProdCode = B.ProdCode '
    + ' LEFT JOIN BUY_DEPARTMENT C ON A.DepartCode = C.DepartCode '
  SET @SQLStr = @SQLStr + @WhereStr
  
  EXEC SelectDataInBatchs @SQLStr, 'TransNum, SeqNo', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, @OrderCondition, @ConditionStr  

  RETURN 0
END

GO
