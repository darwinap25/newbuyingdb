USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetSubscriptionMailBox]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetSubscriptionMailBox]
  @EMail                varchar(64)   
AS
/****************************************************************************
**  Name : SetSubscriptionMailBox
**  Version: 1.0.0.0
**  Description : 扢离隆堐蚘眊 (還奀渠囥)
**
  declare @a int
  exec @a = SetSubscriptionMailBox 'sd@sdf.com'
  print @a  
  select * from SubscriptionMailBox
**  Created by: Gavin @2016-09-13
**
****************************************************************************/
begin
  
  if not exists(select * from SubscriptionMailBox where EMail = @EMail)
  begin 
    insert into SubscriptionMailBox(EMail)
    values(@EMail)
    return 0
  end else
    return -206
end

GO
