USE [NewBuying]
GO
/****** Object:  Table [dbo].[T_DailyMediaTxnDetail]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_DailyMediaTxnDetail](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[StoreID] [int] NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[OprID] [int] NOT NULL,
	[CardNumber] [varchar](64) NOT NULL,
	[CouponNumber] [varchar](64) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[RefKeyID] [int] NULL,
	[RefReceiveKeyID] [int] NULL,
	[RefTxnNo] [varchar](512) NULL,
	[OpenBal] [money] NULL,
	[Amount] [money] NULL,
	[CloseBal] [money] NULL,
	[OrgExpiryDate] [datetime] NULL,
	[NewExpiryDate] [datetime] NULL,
	[OrgStatus] [int] NULL,
	[NewStatus] [int] NULL,
	[BusDate] [datetime] NULL,
	[Txndate] [datetime] NULL,
	[TenderID] [int] NULL,
	[Additional] [varchar](512) NULL,
	[ApprovalCode] [varchar](64) NULL,
	[Remark] [varchar](512) NULL,
	[SecurityCode] [varchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [varchar](512) NULL,
 CONSTRAINT [PK_DailyMediaTxnDetail] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[T_DailyMediaTxnDetail] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺主键。receivetxn数据写入时，需要根据storecode+brandcode转换' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型ID：
101：会员获取Coupon（购买，消费赠送...）
102：会员获得转赠。
103：会员转赠他人。
104：过期作废。
105：会员使用coupon。
106：void操作。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'CouponNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有关联的movement记录的KeyID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'RefKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果有ReceiveTxn记录触发，则记录ReceiveTxn的KeyID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'RefReceiveKeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'如果有ReceiveTxn记录触发，则记录ReceiveTxn的TxnNo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'RefTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作之前的金额值。（现金卷时有效。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'OpenBal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作的金额值。（现金卷时有效。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作之后的金额值。（现金卷时有效。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'CloseBal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作前的有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'OrgExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改后Coupon的有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'NewExpiryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此操作前Coupon状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'OrgStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此操作后Coupon状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'NewStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'BusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'Txndate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息。从其他银行卡转账时，记录银行卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'Additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用银行卡支付后返回的号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'签名字段。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail', @level2type=N'COLUMN',@level2name=N'SecurityCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵操作流水记录表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_DailyMediaTxnDetail'
GO
