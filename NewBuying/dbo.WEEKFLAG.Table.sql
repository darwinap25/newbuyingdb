USE [NewBuying]
GO
/****** Object:  Table [dbo].[WEEKFLAG]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WEEKFLAG](
	[WeekFlagID] [int] IDENTITY(1,1) NOT NULL,
	[WeekFlagCode] [varchar](64) NULL,
	[Note] [nvarchar](512) NULL,
	[SundayFlag] [int] NULL DEFAULT ((0)),
	[MondayFlag] [int] NULL DEFAULT ((0)),
	[TuesdayFlag] [int] NULL DEFAULT ((0)),
	[WednesdayFlag] [int] NULL DEFAULT ((0)),
	[ThursdayFlag] [int] NULL DEFAULT ((0)),
	[FridayFlag] [int] NULL DEFAULT ((0)),
	[SaturdayFlag] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_WEEKFLAG] PRIMARY KEY CLUSTERED 
(
	[WeekFlagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'WeekFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'WeekFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'SundayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'MondayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'TuesdayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'WednesdayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'ThursdayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'FridayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG', @level2type=N'COLUMN',@level2name=N'SaturdayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'周标志表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WEEKFLAG'
GO
