USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponPicking_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponPicking_H](
	[CouponPickingNumber] [varchar](64) NOT NULL,
	[ReferenceNo] [varchar](64) NULL,
	[BrandID] [int] NULL,
	[FromStoreID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[CustomerType] [int] NULL DEFAULT ((2)),
	[CustomerID] [int] NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[FromAddress] [nvarchar](512) NULL,
	[StoreContactName] [nvarchar](512) NULL,
	[StoreContactPhone] [nvarchar](512) NULL,
	[StoreContactEmail] [nvarchar](512) NULL,
	[StoreMobile] [nvarchar](512) NULL,
	[FromContactName] [nvarchar](512) NULL,
	[FromContactNumber] [nvarchar](512) NULL,
	[FromEmail] [nvarchar](512) NULL,
	[FromMobile] [nvarchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[OrderType] [int] NULL DEFAULT ((0)),
	[Remark1] [varchar](512) NULL,
	[AutoGRN] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_ORD_COUPONPICKING_H] PRIMARY KEY CLUSTERED 
(
	[CouponPickingNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponPicking_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponPicking_H] ON [dbo].[Ord_CouponPicking_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponPicking_H
* Version: 1.1.0.0
* Description : Pickup表的批核触发器
*  select count(*) from Ord_CouponPicking_D
select * from Ord_CouponPicking_H
sp_helptext Update_Ord_CouponPicking_H
update Ord_CouponPicking_H set approvestatus = 'A' where CouponPickingNumber = 'COPO00000000012'
select * from ord_coupondelivery_h
* Create By Gavin @2012-09-21
* Modify by Gavin @2012-10-11 (ver 1.0.0.1) Void pickup单时，需要释放被pickup的coupon
* Modify by Gavin @2014-04-14 (ver 1.0.1.0) 实体Coupon订单表结构修改, 存储过程做相应修改。(增加StockStatus)
* Modify by Gavin @2014-04-14 (ver 1.1.0.0) Auto Approve if AutoGRN = 1
*/
/*==============================================================*/
BEGIN  
  declare @CouponPickingNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  DECLARE @FirstCoupon varchar(64), @EndCoupon varchar(64), @CouponTypeID int
  DECLARE @AutoGRN [bit]
  
  DECLARE CUR_Ord_CouponPicking_H CURSOR fast_forward FOR
    SELECT CouponPickingNumber, ApproveStatus, CreatedBy, AutoGRN FROM INSERTED
  OPEN CUR_Ord_CouponPicking_H
  FETCH FROM CUR_Ord_CouponPicking_H INTO @CouponPickingNumber, @ApproveStatus, @CreatedBy, @AutoGRN
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CouponPickingNumber = @CouponPickingNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      if not exists(select * from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber)
      begin
        RAISERROR ('No detail record.', 16, 1)
        ROLLBACK TRANSACTION
      end
      exec GenApprovalCode @ApprovalCode OUTPUT    
      
      IF @AutoGRN = 1
        BEGIN
            EXEC GenDeliveryOrder_Coupon @CreatedBy, @CouponPickingNumber, @ApprovalCode, 'A'      
        END
      ELSE
        BEGIN
            EXEC GenDeliveryOrder_Coupon @CreatedBy, @CouponPickingNumber, @ApprovalCode, 'P'      
        END
      
      update Ord_CouponPicking_H set ApprovalCode = @ApprovalCode where CouponPickingNumber = @CouponPickingNumber
      
      exec ChangeCouponStockStatus 5, @CouponPickingNumber, 1
    end
    
     if @ApproveStatus = 'A' 
		BEGIN
		  IF @AutoGRN = 1
			BEGIN
				EXEC GenDeliveryOrder_Coupon @CreatedBy, @CouponPickingNumber, @ApprovalCode, 'A'      
			END
		  ELSE
			BEGIN
				EXEC GenDeliveryOrder_Coupon @CreatedBy, @CouponPickingNumber, @ApprovalCode, 'P'      
			END
		END
		
    if @ApproveStatus = 'V' and Update(ApproveStatus)
    begin
      exec ChangeCouponStockStatus 5, @CouponPickingNumber, 2    
    end
        
    FETCH FROM CUR_Ord_CouponPicking_H INTO @CouponPickingNumber, @ApproveStatus, @CreatedBy , @AutoGRN   
  END
  CLOSE CUR_Ord_CouponPicking_H 
  DEALLOCATE CUR_Ord_CouponPicking_H  
END



GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拣货单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'CouponPickingNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。指coupon订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌限制。如果选择了品牌，那么fromstore 和 Store 都必须是这个品牌的，包括子表中的CouponType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源方店铺主键，一般可能是总部' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货方店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户类型（备用，由大客户引起订单时填写）。1：客戶訂貨。 2：店鋪訂貨
默认2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'CustomerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'customer表 主键， 外键.（有大客户时填写）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network，5：快递送货（实体Coupon）。
1到4 是虚拟Coupon。  5 是实体Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址（店铺地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'FromContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵拣货单主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPicking_H'
GO
