USE [NewBuying]
GO
/****** Object:  Table [dbo].[DAYFLAG]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DAYFLAG](
	[DayFlagID] [int] IDENTITY(1,1) NOT NULL,
	[DayFlagCode] [varchar](64) NULL,
	[Note] [nvarchar](512) NULL,
	[Day1Flag] [int] NULL DEFAULT ((0)),
	[Day2Flag] [int] NULL DEFAULT ((0)),
	[Day3Flag] [int] NULL DEFAULT ((0)),
	[Day4Flag] [int] NULL DEFAULT ((0)),
	[Day5Flag] [int] NULL DEFAULT ((0)),
	[Day6Flag] [int] NULL DEFAULT ((0)),
	[Day7Flag] [int] NULL DEFAULT ((0)),
	[Day8Flag] [int] NULL DEFAULT ((0)),
	[Day9Flag] [int] NULL DEFAULT ((0)),
	[Day10Flag] [int] NULL DEFAULT ((0)),
	[Day11Flag] [int] NULL DEFAULT ((0)),
	[Day12Flag] [int] NULL DEFAULT ((0)),
	[Day13Flag] [int] NULL DEFAULT ((0)),
	[Day14Flag] [int] NULL DEFAULT ((0)),
	[Day15Flag] [int] NULL DEFAULT ((0)),
	[Day16Flag] [int] NULL DEFAULT ((0)),
	[Day17Flag] [int] NULL DEFAULT ((0)),
	[Day18Flag] [int] NULL DEFAULT ((0)),
	[Day19Flag] [int] NULL DEFAULT ((0)),
	[Day20Flag] [int] NULL DEFAULT ((0)),
	[Day21Flag] [int] NULL DEFAULT ((0)),
	[Day22Flag] [int] NULL DEFAULT ((0)),
	[Day23Flag] [int] NULL DEFAULT ((0)),
	[Day24Flag] [int] NULL DEFAULT ((0)),
	[Day25Flag] [int] NULL DEFAULT ((0)),
	[Day26Flag] [int] NULL DEFAULT ((0)),
	[Day27Flag] [int] NULL DEFAULT ((0)),
	[Day28Flag] [int] NULL DEFAULT ((0)),
	[Day29Flag] [int] NULL DEFAULT ((0)),
	[Day30Flag] [int] NULL DEFAULT ((0)),
	[Day31Flag] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_DAYFLAG] PRIMARY KEY CLUSTERED 
(
	[DayFlagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'DayFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'DayFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day1Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day2Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day3Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day4Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day5Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day6Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day7Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day8Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day9Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day10Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day11Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day12Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day13Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day14Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day15Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day16Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day17Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day18Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day19Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day20Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day21Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day22Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day23Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day24Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day25Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day26Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day27Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day28Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day29Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day30Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG', @level2type=N'COLUMN',@level2name=N'Day31Flag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'天标志表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYFLAG'
GO
