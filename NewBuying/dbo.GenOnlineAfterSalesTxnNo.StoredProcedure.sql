USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenOnlineAfterSalesTxnNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenOnlineAfterSalesTxnNo]   
 @TxnNo				varchar(512) output 
AS
/****************************************************************************
**  Name : [GenOnlineAfterSalesTxnNo]
**  Version: 1.0.0.0
**  Description : 获得交易单的单号
**  Parameter :
**
declare @a int, @TxnNo varchar(64)
exec @a = [GenOnlineAfterSalesTxnNo] @TxnNo output
print @a
print @TxnNo

****************************************************************************/
begin
  exec GetRefNoString 'ASSNNO', @TxnNo output   
  return 0
end

GO
