USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenDeliveryOrder_Coupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[GenDeliveryOrder_Coupon]
  @UserID                   int,             --操作员ID
  @CouponPickingNumber      varchar(64),     --单号
  @PickupApprovalcode		varchar(6),       -- pickup的 approvalcode
  @ApproveStatus			char(1)
AS
/****************************************************************************
**  Name : GenDeliveryOrder_Coupon
**  Version: 1.2.0.0
**  Description : 根据邮件批核的提货单， 产生送货单
**  example :
  declare @a int  , @CouponOrderFormNumber varchar(64)
  set @CouponOrderFormNumber = 'COPO00000000025'
  exec @a = AddPickupDetail_Coupon 1, @CouponOrderFormNumber, 1, 2, 12, 2, '', ''   
  print @a  
  select * from coupontype
  delete from Ord_CouponPicking_D where keyid >= 277
  select * from Ord_CouponOrderForm_H
  select * from Ord_CouponOrderForm_D
  select * from Ord_CouponDelivery_H
  select * from Ord_CouponPicking_H
update Ord_CouponPicking_H set approvestatus = 'A' where CouponPickingNumber = 'COPO00000000011'  
  select * from coupon where couponnumber = 'CN0200006'
**  Created by: Gavin @2012-09-21
**  Modify by: Gavin @2012-09-26 (ver 1.0.0.1) 产生delivery单同时, 更改coupon状态到issued
**  Modify by: Gavin @2012-10-18 (ver 1.0.0.2) Ord_CouponDelivery_D 增加 BatchCouponCode
**  Modify by: Gavin @2014-04-14 (ver 1.0.1.0) 实体Coupon订单表结构修改, 存储过程做相应修改。
**  Modify by: Gavin @2014-04-14 (ver 1.0.1.1) (for RRG) RRG的情况：每条Detail记录只是一个Coupon，startNumber=EndNumber，并且数据量很大（1000K）。为加快速度，RRG版本直接取Detail写入movement，不用循环
**	Modify by: Darwin @2017-10-24 (ver 1.2.0.0) @AproveStatus
****************************************************************************/
begin
  declare @CouponOrderFormNumber varchar(64), @CouponDeliveryNumber varchar(64), @OrderQty int, @PickupQty int, @CouponNumber varchar(64), @BrandID int
  declare @i int, @EndCouponNumber varchar(64), @PrevCouponNumber varchar(64), @A int
  declare @FirstCoupon varchar(64), @EndCoupon varchar(64), @OrderStatus char(1)
  declare @BusDate datetime, @TxnDate datetime, @StoreID int, @CouponTypeID int
 
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()
  
  -- 获取pickup单的单号
  select @CouponOrderFormNumber = ReferenceNo, @OrderStatus = ApproveStatus from Ord_CouponPicking_H where CouponPickingNumber = @CouponPickingNumber
  if @OrderStatus <> 'A' 
    return -1
  exec GetRefNoString 'CODO', @CouponDeliveryNumber output   
  select @StoreID = StoreID from Ord_CouponOrderForm_H where CouponOrderFormNumber = @CouponOrderFormNumber
        
  insert into Ord_CouponDelivery_H(CouponDeliveryNumber, ReferenceNo, BrandID,  FromStoreID, StoreID, CustomerType, 
          CustomerID,SendMethod, SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, 
          StoreMobile, FromContactName, FromContactNumber, FromEmail, FromMobile,            
          NeedActive, Remark, CreatedBusdate, ApproveStatus, CreatedBy, UpdatedBy, Remark1)
  select @CouponDeliveryNumber, @CouponPickingNumber, BrandID,  FromStoreID, StoreID, CustomerType, 
          CustomerID,SendMethod, SendAddress, FromAddress, StoreContactName, StoreContactPhone, StoreContactEmail, 
          StoreMobile, FromContactName, FromContactNumber, FromEmail, FromMobile, 
          case when CustomerType = 1 then 1 else 0 end, Remark, CreatedBusdate, @ApproveStatus, @UserID, @UserID, Remark1
          --case when CustomerType = 1 then 1 else 0 end, Remark, CreatedBusdate, 'P', @UserID, @UserID, Remark1
     from Ord_CouponOrderForm_H where CouponOrderFormNumber = @CouponOrderFormNumber
  
  
  declare @OrderCount int, @TempCount int, @UnitCount int, @DoCount int, @SumCount int
  set @UnitCount = 10000
  set @SumCount = 0  
  select @OrderCount = COUNT(*) from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber
  set @TempCount = @OrderCount
  while @TempCount > 0
  begin
    if @TempCount > @UnitCount
    begin
      set @DoCount = @UnitCount 
      set @TempCount = @TempCount - @UnitCount
    end else
    begin
      set @DoCount = @TempCount 
      set @TempCount = 0
    end 
          
    insert into Ord_CouponDelivery_D (CouponDeliveryNumber, CouponTypeID, Description, OrderQty, PickQty, ActualQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode) 
    select Number, CouponTypeID, Description, OrderQty, PickQty, ActualQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode
    from (  
           select  ROW_NUMBER() OVER(order by FirstCouponNumber) as iid, @CouponDeliveryNumber as Number, CouponTypeID, Description, OrderQty, PickQty, ActualQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode
             from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber 
         ) A     
    where A.iid > @SumCount and A.iid <= @SumCount + @DoCount
    -- issue 发送的coupon 
    -- ver 1.0.1.1
   insert into Coupon_Movement
        (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
         BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
         OrgExpirydate, OrgStatus, NewStatus)
   select 40, '', CouponNumber, CouponTypeID, null, null, @CouponPickingNumber, CouponAmount, 0, CouponAmount,
          @BusDate, @TxnDate, '', '', @UserID, CouponExpiryDate, @PickupApprovalcode, @StoreID, '', '',
          CouponExpiryDate, 0, 1
   from (      
          select ROW_NUMBER() OVER(order by FirstCouponNumber) as iid, C.CouponNumber, C.CouponTypeID, C.CouponAmount, 
             C.CouponExpiryDate
          from Ord_CouponPicking_D D left join Coupon C on D.FirstCouponNumber = C.CouponNumber 
          where CouponPickingNumber = @CouponPickingNumber 
         ) A
    where A.iid > @SumCount and A.iid <= @SumCount + @DoCount           
   
    set @SumCount = @SumCount + @DoCount         
  end  
             
--  update Coupon set LocateStoreID = @StoreID, Status = 1, StockStatus = 5, CouponIssueDate = @BusDate
--  where CouponNumber in (select FirstCouponNumber from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber) 
        
/*    
  DECLARE CUR_PickupCoupon_Detail CURSOR fast_forward for
      select FirstCouponNumber, EndCouponNumber, CouponTypeID from Ord_CouponPicking_D where CouponPickingNumber = @CouponPickingNumber
	OPEN CUR_PickupCoupon_Detail
    FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
    WHILE @@FETCH_STATUS=0 
    BEGIN 
      insert into Coupon_Movement
        (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
         BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
         OrgExpirydate, OrgStatus, NewStatus)
      select 40, '', CouponNumber, CouponTypeID, null, null, @CouponPickingNumber, CouponAmount, 0, CouponAmount,
         @BusDate, @TxnDate, '', '', @UserID, CouponExpiryDate, @PickupApprovalcode, @StoreID, '', '',
         CouponExpiryDate, 0, 1
        from Coupon where CouponTypeID = @CouponTypeID and CouponNumber >= @FirstCoupon and CouponNumber <= @EndCoupon
	  FETCH FROM CUR_PickupCoupon_Detail INTO @FirstCoupon, @EndCoupon, @CouponTypeID
	END
  CLOSE CUR_PickupCoupon_Detail 
  DEALLOCATE CUR_PickupCoupon_Detail 
*/
  
  return 0
end

GO
