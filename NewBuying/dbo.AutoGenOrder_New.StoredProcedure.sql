USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[AutoGenOrder_New]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[AutoGenOrder_New]
  @UserID                 int,                     -- 操作用户ID
  @InventoryReplenishCode varchar(1000) = '',      -- 自动补货规则表Code, 可能会给多个，用 ,  隔开
  @ReferenceNo            varchar(64) = '',        -- 相关单号。（如果有的话）
  @BusDate                datetime = null,         -- 交易日
  @TxnDate                datetime = null,         -- 当前交易时间
  @IsSchedule             int=0                    -- 是否 日程安排 自动执行. 0:不是. 1: 是的
AS
/****************************************************************************
**  Name : AutoGenOrder_New
**  Version: 1.0.0.2
**  Description : 自动产生产生Coupon和Card的订单（包括给供应商和总部的）.
  （从规则表中InventoryReplenishRule_H读取设置。将取代AutoGenCouponOrder） 
**
**  Parameter :	 select * from card_movement
  declare @a int  
  exec @a = AutoGenOrder_New 1,  '54545,433443'
  print @a  
delete from ord_ordertosupplier_H
delete from ord_ordertosupplier_D 
delete from ord_ordertosupplier_card_H
delete from ord_ordertosupplier_card_D  
  
exec AutoGenOrder_New @UserID=1,@CouponReplenishCode='ddddd,ccccc,bbbbb,',@ReferenceNo=NULL,@BusDate=NULL,@TxnDate=NULL

**  Created by: Gavin @2014-09-28
**  Modify by Gavin @2014-11-20 (ver 1.0.0.1) 增加调用 DoDailyInventoryCheck
**  Modify by Gavin @2014-11-24 (ver 1.0.0.2) 移除DoDailyInventoryCheck（店铺订单批核，未产生picking单时，同步数据有错）
**
****************************************************************************/
begin
  declare @SupplierID int, @NewPRNumber varchar(64), @OrderTargetID int, @OrderStoreTypeID int, @OrderStoreID int
  declare @StoreAddress varchar(512), @StoreContact varchar(512), @StoreContactPhone varchar(512),
          @StoreEmail varchar(512)
  declare @SourceAddress varchar(512), @SourceContact varchar(512), @SourceContactPhone varchar(512),
          @SourceEmail varchar(512)
  declare @BrandID int, @CouponTypeID int, @OrderQty int, @CompanyID int 
  declare @CardTypeID int, @CardGradeID int, @MediaType int
  declare @OnhandQty int, @MinStockQty int, @RecordCount int, @PackageQty int
  declare @ApprovalCode varchar(64), @OrderRoundUpQty int
  declare @StoreBrandID int
 
 
-- 同步Card和Coupon的onhand数量和record count  
  -- exec DoDailyInventoryCheck
    
  set @RecordCount = 0 
  select Top 1 @CompanyID = CompanyID from Company Order by IsDefault desc
  set @InventoryReplenishCode = isnull(@InventoryReplenishCode, '')          
  if isnull(@BusDate, 0) = 0
  Begin
    select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
    set @BusDate=isnull(@BusDate,getdate())
  end
  if isnull(@TxnDate, 0) = 0
    set @TxnDate=getdate()
 
    exec AutoGenCouponOrder @UserID, @InventoryReplenishCode, @ReferenceNo, @BusDate, @TxnDate, @IsSchedule
    exec AutoGenCardOrder @UserID, @InventoryReplenishCode, @ReferenceNo, @BusDate, @TxnDate, @IsSchedule
  
  return 0
end

GO
