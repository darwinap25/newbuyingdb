USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CouponPush_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CouponPush_H](
	[CouponPushNumber] [varchar](64) NOT NULL,
	[PushCardBrandID] [int] NOT NULL,
	[PushCardTypeID] [int] NULL,
	[PushCardGradeID] [int] NULL,
	[RepeatPush] [int] NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_COUPONPUSH_H] PRIMARY KEY CLUSTERED 
(
	[CouponPushNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CouponPush_H] ADD  DEFAULT ((0)) FOR [PushCardBrandID]
GO
ALTER TABLE [dbo].[Ord_CouponPush_H] ADD  DEFAULT ((0)) FOR [PushCardTypeID]
GO
ALTER TABLE [dbo].[Ord_CouponPush_H] ADD  DEFAULT ((0)) FOR [PushCardGradeID]
GO
ALTER TABLE [dbo].[Ord_CouponPush_H] ADD  DEFAULT ((0)) FOR [RepeatPush]
GO
ALTER TABLE [dbo].[Ord_CouponPush_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CouponPush_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Update_Ord_CouponPush_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CouponPush_H] ON [dbo].[Ord_CouponPush_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CouponPush_H
* Version: 1.0.0.1
* Description : Coupon推送订单表的批核触发器
*  
* Create By Gavin @2013-03-06
* Modify By Gavin @2013-03-13 (ver 1.0.0.1) 修正调用DoPushCoupon的参数次序。
*/
/*==============================================================*/
BEGIN  
  declare @CouponPushNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @PushCardBrandID int, @PushCardTypeID int, @PushCardGradeID int, @RepeatPush int
  
  DECLARE CUR_Ord_CouponPush_H CURSOR fast_forward FOR
    SELECT CouponPushNumber, ApproveStatus, CreatedBy, PushCardBrandID, PushCardTypeID,PushCardGradeID, RepeatPush FROM INSERTED
  OPEN CUR_Ord_CouponPush_H
  FETCH FROM CUR_Ord_CouponPush_H INTO @CouponPushNumber, @ApproveStatus, @CreatedBy, @PushCardBrandID, @PushCardTypeID, @PushCardGradeID, @RepeatPush
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where CouponPushNumber = @CouponPushNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec DoPushCoupon @CouponPushNumber,@PushCardBrandID, @PushCardTypeID, @PushCardGradeID, @RepeatPush, @CreatedBy, @ApprovalCode output
      update Ord_CouponPush_H set ApprovalCode = @ApprovalCode  where CouponPushNumber = @CouponPushNumber
    end
    FETCH FROM CUR_Ord_CouponPush_H INTO @CouponPushNumber, @ApproveStatus, @CreatedBy, @PushCardBrandID, @PushCardTypeID, @PushCardGradeID, @RepeatPush
  END
  CLOSE CUR_Ord_CouponPush_H 
  DEALLOCATE CUR_Ord_CouponPush_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵推送单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'CouponPushNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'推送范围：卡品牌ID。 0：默认全部。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'PushCardBrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'推送范围：卡类型ID。 0：默认全部。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'PushCardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'推送范围：卡级别ID。 0：默认全部。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'PushCardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否重复推送。 默认0。 0：如已绑定该列表中优惠券类型的卡，不需要再绑定列表中的优惠券类型，1：相反' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'RepeatPush'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：Pending。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'推送优惠劵（主表）
（注：根据提供的UI需求，推送会员范围在主表中设置，不允许范围多选）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CouponPush_H'
GO
