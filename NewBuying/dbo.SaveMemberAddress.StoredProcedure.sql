USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SaveMemberAddress]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SaveMemberAddress]
  @MemberID         int,      -- 
  @ActionType       int,            -- 1:新增。 2：修改。 3：删除。
  @AddressCountry		varchar(64),
  @AddressProvince	varchar(64),
  @AddressCity			varchar(64),
  @AddressDistrict	varchar(64),
  @AddressDetail		varchar(512),
  @AddressZipCode		varchar(64),
  @Contact				  nvarchar(512),
  @ContactPhone			nvarchar(512),
  @AddressID			  int output,
  @MemberFirstAddr	int = 0,
  @AddressType      int = 0,
  @AddressTypeDesc  varchar(512)='',
  @IsBillAddress    int = 0,
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1      
AS
/****************************************************************************
**  Name : SaveMemberAddress
**  Version: 1.0.0.6
**  Description : 保存用户提交的地址。
declare @a int, @AddressID int
set @AddressID = 63
exec @a = SaveMemberAddress 741, 3, '086','HK','HK', '山东路','山东路18号11弄1室','200123','nathan',
  '123123123123', @AddressID output, 1, ''
print @a
print @AddressID 
select * from memberaddress
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-02-07
**  Modify by: Gavin @2013-01-07 (ver 1.0.0.1) 增加参数输入AddressDistrict， 输入的内容为Code，保存到指定字段，还需要根据code，取出Name，合并填入FullDetail
                                               增加参数@LanguageAbbr，确定根据code，选取的Name的语言。
**  Modify by: Gavin @2013-01-23 (ver 1.0.0.2) 保存address时,同时保存到member表
**  Modify by: Gavin @2013-12-11 (ver 1.0.0.3) 增强功能：如果传入的不是code，在设置表中找不到对应的城市，地区等数据，
                                               则直接使用传入的数据。（即，允许直接保存地址名称）
**  Modify by: Gavin @2015-06-29 (ver 1.0.0.4) 增加actiontype参数， 允许增删改查   
**  Modify by: Gavin @2015-08-25 (ver 1.0.0.5) MemberAddress表增加字段, 此过程做相应修改     
**  Modify by: Gavin @2015-10-12 (ver 1.0.0.6) 国家,省等几项 拼起来时 中间加上 空格                                         
**
****************************************************************************/
begin    
  declare @AddressFullDetail nvarchar(512), @TempName nvarchar(512), @Language int
  set @AddressCountry = isnull(@AddressCountry, '')
  set @AddressProvince = isnull(@AddressProvince, '')
  set @AddressCity = isnull(@AddressCity, '')
  set @AddressDistrict = isnull(@AddressDistrict, '')
  set @AddressDetail = isnull(@AddressDetail, '')
 
  set @AddressFullDetail = ''
  set @TempName = ''
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1  
  ------------
  if isnull(@MemberID, 0) = 0
    return -1  
      
  select @TempName = case @Language when 2 then CountryName2 when 3 then CountryName3 else CountryName1 end from Country where CountryCode = @AddressCountry
  set @AddressFullDetail = @AddressFullDetail + isnull(@TempName, @AddressCountry)
  select @TempName = case @Language when 2 then ProvinceName2 when 3 then ProvinceName3 else ProvinceName1 end from Province where ProvinceCode = @AddressProvince
  set @AddressFullDetail = @AddressFullDetail + ' ' + isnull(@TempName, @AddressProvince)
  select @TempName = case @Language when 2 then CityName2 when 3 then CityName3 else CityName1 end from City where CityCode = @AddressCity
  set @AddressFullDetail = @AddressFullDetail + ' ' + isnull(@TempName, @AddressCity)
  select @TempName = case @Language when 2 then DistrictName2 when 3 then DistrictName3 else DistrictName1 end from District where DistrictCode = @AddressDistrict
  set @AddressFullDetail = @AddressFullDetail + ' ' + isnull(@TempName, @AddressDistrict)  
  set @AddressFullDetail = @AddressFullDetail + ' ' + @AddressDetail  
  
  if @ActionType = 1
  begin
    insert into MemberAddress
      (MemberID, MemberFirstAddr, AddressCountry, AddressProvince, AddressCity, AddressDistrict, AddressDetail, AddressFullDetail, AddressZipCode, Contact, ContactPhone, AddressType, AddressTypeDesc, IsBillAddress)
    values
      (@MemberID, @MemberFirstAddr, @AddressCountry, @AddressProvince, @AddressCity, @AddressDistrict, @AddressDetail, @AddressFullDetail, @AddressZipCode, @Contact, @ContactPhone, @AddressType, @AddressTypeDesc, @IsBillAddress)  

    set @AddressID = @@identity
    if @MemberFirstAddr = 1    
      update Member set HomeAddress = @AddressFullDetail where memberID = @MemberID  
  end else if @ActionType = 2
  begin    
    if not exists(select * from MemberAddress where AddressID = @AddressID and MemberID = @MemberID)
      return -2
    update MemberAddress set MemberFirstAddr = @MemberFirstAddr, AddressCountry =@AddressCountry, AddressProvince = @AddressProvince, 
	 	 AddressCity = @AddressCity, AddressDistrict = @AddressDistrict, AddressDetail = @AddressDetail, 
		 AddressZipCode = @AddressZipCode, Contact = @Contact, ContactPhone = @ContactPhone,
		 AddressFullDetail = @AddressFullDetail,
		 AddressType = @AddressType, AddressTypeDesc = @AddressTypeDesc, IsBillAddress = @AddressTypeDesc
    where AddressID = @AddressID and MemberID = @MemberID
    if @MemberFirstAddr = 1
      update Member set HomeAddress = @AddressFullDetail where memberID = @MemberID     
  end else if @ActionType = 3
  begin
    delete from MemberAddress where AddressID = @AddressID and MemberID = @MemberID
    select @AddressFullDetail = AddressFullDetail from MemberAddress 
      where memberID = @MemberID and MemberFirstAddr = 1
    if isnull(@AddressFullDetail, '') = ''
      select top 1 @AddressFullDetail = AddressFullDetail from MemberAddress 
        where memberID = @MemberID   
    update Member set HomeAddress = @AddressFullDetail from Member where memberID = @MemberID     
  end
  return 0
end

GO
