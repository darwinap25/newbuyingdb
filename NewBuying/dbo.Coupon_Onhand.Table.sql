USE [NewBuying]
GO
/****** Object:  Table [dbo].[Coupon_Onhand]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coupon_Onhand](
	[CouponTypeID] [int] NOT NULL,
	[StoreTypeID] [int] NOT NULL,
	[StoreID] [int] NOT NULL,
	[CouponStatus] [int] NOT NULL DEFAULT ((0)),
	[CouponStockStatus] [int] NOT NULL DEFAULT ((0)),
	[OnhandQty] [int] NOT NULL DEFAULT ((0)),
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_COUPON_ONHAND] PRIMARY KEY CLUSTERED 
(
	[CouponTypeID] ASC,
	[StoreID] ASC,
	[CouponStatus] ASC,
	[CouponStockStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Onhand', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存类型。（即店铺类型）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Onhand', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存所属店铺（总部）ID。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Onhand', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵状态，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Onhand', @level2type=N'COLUMN',@level2name=N'CouponStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵库存状态，默认0。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Onhand', @level2type=N'COLUMN',@level2name=N'CouponStockStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存数量，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Onhand', @level2type=N'COLUMN',@level2name=N'OnhandQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵的库存数量。 （只有 CouponType中标志为IsPhysicalCoupon=1的Coupon才会有库存数量。）
（优惠劵数量的汇总表，表中数据应该和coupon表的汇总数据相等）
（目前不使用流水记录表，直接修改onhand 数量）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Coupon_Onhand'
GO
