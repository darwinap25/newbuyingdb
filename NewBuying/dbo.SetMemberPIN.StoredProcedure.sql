USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SetMemberPIN]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[SetMemberPIN]
  @MemberID             int,               -- 会员主键 
  @CardNumber			varchar(64),      -- 如填写CardNumber，忽略MemberID
  @MemberPINID          int,               -- MemberPIN 主键
  @PINID                int,               -- PIN主键 
  @Qty                  int,               -- PIN数量
  @Remark               nvarchar(512)       -- 备注
AS
/****************************************************************************
**  Name : SetMemberPIN  
**  Version: 1.0.0.0
**  Description : 设置会员拥有的PIN.(Disney使用)
**  Parameter :
  declare @a int  
  exec @a = SetMemberPIN 17, '', 0, 1, 1, 'test pin'
  print @a  
  select * from memberpin
**  Created by: Gavin @2013-02-20
**
****************************************************************************/
begin  
  set @PINID = isnull(@PINID, 0)
  set @MemberPINID = isnull(@MemberPINID, 0)
  if isnull(@MemberID, 0) = 0 
    select @MemberID = MemberID from Card where CardNumber = @CardNumber
  if (isnull(@MemberID, 0) = 0) and (isnull(@CardNumber, '') = '')  
    return -7  
    
  if exists(select * from MemberPIN where MemberPINID = @MemberPINID)
  begin
	update MemberPIN set PINID = @PINID, Qty = @Qty, Remark = @Remark 
	  where MemberPINID = @MemberPINID
  end else
  begin
	insert into MemberPIN(MemberID, PINID, Qty, Remark)   
	values(@MemberID, @PINID, @Qty, @Remark)
  end

  return 0
end

GO
