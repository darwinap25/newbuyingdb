USE [NewBuying]
GO
/****** Object:  Table [dbo].[coupontypestorecondition_List_bak]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[coupontypestorecondition_List_bak](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeID] [int] NOT NULL,
	[StoreConditionType] [int] NULL,
	[ConditionType] [int] NULL,
	[ConditionID] [int] NULL,
	[StoreID] [int] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL
) ON [PRIMARY]

GO
