USE [NewBuying]
GO
/****** Object:  Table [dbo].[SalesReserverDetail]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesReserverDetail](
	[TransNum] [varchar](64) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[SeqNo] [varchar](64) NOT NULL,
	[StoreCode] [varchar](64) NOT NULL,
	[StockTypeCode] [varchar](64) NULL DEFAULT ('G'),
	[Qty] [dbo].[Buy_Qty] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_SALESRESERVERDETAIL] PRIMARY KEY CLUSTERED 
(
	[TransNum] ASC,
	[ProdCode] ASC,
	[SeqNo] ASC,
	[StoreCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalesReserverDetail', @level2type=N'COLUMN',@level2name=N'TransNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalesReserverDetail', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单中货品序号。使用字符串是为了 预留 BOM功能时使用。 如果不使用BOM功能， 可以直接用 自然数。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalesReserverDetail', @level2type=N'COLUMN',@level2name=N'SeqNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扣库存的店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalesReserverDetail', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扣库存的库存类型。（和扣库有关）默认是G' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalesReserverDetail', @level2type=N'COLUMN',@level2name=N'StockTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalesReserverDetail', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售单预扣表。@2016-10-18
@2016-11-29 .  联合主键需要增加字段StoreCode，因为同一单的同一个货品可能拆分到不同的store（当前store不够数量时，剩余数量需要扣WH的库存。）来扣库存。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalesReserverDetail'
GO
