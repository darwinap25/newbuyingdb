USE [NewBuying]
GO
/****** Object:  Table [dbo].[ImportGM]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImportGM](
	[SKU] [varchar](64) NOT NULL,
	[UPC] [varchar](64) NULL,
	[SKUDesc] [varchar](64) NULL,
	[SKUUnitAmount] [money] NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[BU] [varchar](64) NULL,
	[GMValue] [decimal](12, 4) NOT NULL,
	[CardTypeCode] [varchar](64) NOT NULL,
	[CardGradeCode] [varchar](64) NOT NULL,
	[NumberMask] [varchar](64) NULL,
	[NumberPrefix] [varchar](64) NULL,
	[Export] [varchar](64) NULL,
	[MinAmount] [money] NULL,
	[MaxAmount] [money] NULL,
	[TransactionType] [varchar](64) NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_IMPORTGM] PRIMARY KEY CLUSTERED 
(
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ImportGM] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ImportGM] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SKU Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'SKU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UPC Code--- for reports' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'UPC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SKU Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'SKUDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SKU Unit Amount; --> need to multiply the Gross Margin to this amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'SKUUnitAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telco Product Code based on Telco API; --> parameter to be sent to Telco API call' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Different BU may have different GM on the same SKU' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'BU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gross Margin。 百分比值。 例如： 10  =  九折。   即： OFF 10%' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'GMValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Type Code  Telco Card Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'CardTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Grade Code。  (like Globe, Smart, Sun, etc.) --> as each store will have 3 card numbers depending on Telco card grade, need to identify which card number will ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'CardGradeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number Mask Rule; and' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'NumberMask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'to check if the mobile number prefix can be used with the SKU' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'NumberPrefix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PR export ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'Export'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'min amuont allow to be top up' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'MinAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'max amuont allow to be top up' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM', @level2type=N'COLUMN',@level2name=N'MaxAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入的GrossMargin表。  字段根据导入数据建立。
@2015-07-06 修改：
1.	Make SKU as the unique key
2.	During top up transactions, if ImportGM.BU = null, the SKU can be used in any BrandCode. If ImportGM.BU NOT null, then SKU can only be transacted in the specific Brand
@2015-07-07 修改：
每个SKU需要设置 money 的范围
@2015-08-04 增加字段： TransactionType varchar（64）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImportGM'
GO
