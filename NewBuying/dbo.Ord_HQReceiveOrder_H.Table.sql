USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_HQReceiveOrder_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_HQReceiveOrder_H](
	[HQReceiveOrderNumber] [varchar](64) NOT NULL,
	[OrderType] [int] NULL DEFAULT ((0)),
	[ReceiveType] [int] NULL,
	[ReferenceNo] [varchar](64) NULL,
	[FromStoreID] [int] NOT NULL,
	[FromContactName] [varchar](100) NULL,
	[FromContactPhone] [varchar](100) NULL,
	[FromMobile] [varchar](100) NULL,
	[FromEmail] [varchar](100) NULL,
	[FromAddress] [varchar](512) NULL,
	[StoreID] [int] NOT NULL,
	[StoreContactName] [varchar](100) NULL,
	[StoreContactPhone] [varchar](100) NULL,
	[StoreContactEmail] [varchar](100) NULL,
	[StoreMobile] [varchar](100) NULL,
	[StoreAddress] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_HQRECEIVEORDER_H] PRIMARY KEY CLUSTERED 
(
	[HQReceiveOrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_HQReceiveOrder_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_HQReceiveOrder_H] ON [dbo].[Ord_HQReceiveOrder_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_HQReceiveOrder_H
* Version: 1.0.1.0
* Description :
     select * from Ord_HQReceiveOrder_H
     update Ord_ReceiveOrder_H set approvestatus = 'A' where StoreOrderNumber = 'COPO00000000012'
* Create By Gavin @2015-03-10
**  Modify by Gavin @2016-01-07 (1.0.0.1) 获取busdate时增加StoreCode条件
**  Modify by Gavin @2016-04-15 (1.0.1.0) 订单库存变动流程更改,增加中转仓库
*/
/*==============================================================*/
BEGIN  
  DECLARE @HQReceiveOrderNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  DECLARE @Busdate DATE, @TxnDate DATETIME, @ReceiveType INT, @OprID INT, @StoreID int, @StoreCode varchar(64)
  DECLARE @TransferStoreID INT
  SELECT @TransferStoreID = StoreID FROM BUY_STORE WHERE StoreCode = '999'
  SET @TxnDate = GETDATE()
  SET @OprID = 0
          
  DECLARE CUR_Ord_HQReceiveOrder_H CURSOR fast_forward FOR
    SELECT HQReceiveOrderNumber, ApproveStatus, CreatedBy, ReceiveType, StoreID FROM INSERTED
  OPEN CUR_Ord_HQReceiveOrder_H
  FETCH FROM CUR_Ord_HQReceiveOrder_H INTO @HQReceiveOrderNumber, @ApproveStatus, @CreatedBy, @ReceiveType, @StoreID
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where HQReceiveOrderNumber = @HQReceiveOrderNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
	  select @StoreCode = StoreCode from BUY_STORE WHERE StoreID = @StoreID
	  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 AND StoreCode = @StoreCode ORDER BY BusDate DESC
      SET @Busdate = ISNULL(@Busdate, GETDATE())

      exec GenApprovalCode @ApprovalCode output
      
      IF @ReceiveType = 1 
        SET @OprID = 10
      IF @ReceiveType = 2
        SET @OprID = 12
                
      -- 插入movement表               -- 总部加上库存
      INSERT INTO STK_StockMovement
        (OprID, StoreID, StockTypeCode, ProdCode, ReferenceNo, ReferenceNo_Other, BusDate, TxnDate, OpenQty, ActQty, CloseQty, 
          SerialNoType, SerialNo, ApprovalCode, CreatedOn, CreatedBy)
      SELECT @OprID, H.StoreID, D.StockTypeCode, D.ProdCode, D.HQReceiveOrderNumber, '', @Busdate, @TxnDate, isnull(O.OnhandQty,0), D.ReceiveQty, ISNULL(O.OnhandQty,0) + ISNULL(D.ReceiveQty,0),
          0, '', @ApprovalCode, GETDATE(), H.CreatedBy
      FROM Ord_HQReceiveOrder_D D LEFT JOIN Ord_HQReceiveOrder_H H ON D.HQReceiveOrderNumber = H.HQReceiveOrderNumber
        LEFT JOIN STK_StockOnhand O ON D.ProdCode = O.ProdCode AND D.StockTypeCode = O.StockTypeCode AND H.StoreID = O.StoreID
      WHERE D.HQReceiveOrderNumber = @HQReceiveOrderNumber 
      
      IF @OprID = 12  -- 退货入库时同步扣除店铺的库存。
      BEGIN
        INSERT INTO STK_StockMovement
          (OprID, StoreID, StockTypeCode, ProdCode, ReferenceNo, ReferenceNo_Other, BusDate, TxnDate, OpenQty, ActQty, CloseQty, 
          SerialNoType, SerialNo, ApprovalCode, CreatedOn, CreatedBy)
        SELECT 22, @TransferStoreID, D.StockTypeCode, D.ProdCode, D.HQReceiveOrderNumber, '', @Busdate, @TxnDate, isnull(O.OnhandQty,0), -D.ReceiveQty, ISNULL(O.OnhandQty,0) - ISNULL(D.ReceiveQty,0),
           0, '', @ApprovalCode, GETDATE(), H.CreatedBy
        FROM Ord_HQReceiveOrder_D D LEFT JOIN Ord_HQReceiveOrder_H H ON D.HQReceiveOrderNumber = H.HQReceiveOrderNumber
          LEFT JOIN (SELECT * FROM STK_StockOnhand WHERE StoreID = @TransferStoreID) O ON D.ProdCode = O.ProdCode AND D.StockTypeCode = O.StockTypeCode 
        WHERE D.HQReceiveOrderNumber = @HQReceiveOrderNumber         
      END
      ------------------
            
      UPDATE Ord_HQReceiveOrder_H set ApprovalCode = @ApprovalCode, ApproveOn = GETDATE(), ApproveBusDate = @Busdate --, ApproveBy = @CreatedBy
        where HQReceiveOrderNumber = @HQReceiveOrderNumber
    end

    FETCH FROM CUR_Ord_HQReceiveOrder_H INTO @HQReceiveOrderNumber, @ApproveStatus, @CreatedBy, @ReceiveType, @StoreID
  END
  CLOSE CUR_Ord_HQReceiveOrder_H 
  DEALLOCATE CUR_Ord_HQReceiveOrder_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'HQReceiveOrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货来源。
1：总部给供应商的订单的收货。 此时，FromStoreID 的内容是 供应商ID
2：店铺退货单的收货
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'ReceiveType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货源方店铺主键，供应商或者退货的店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'FromStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货供应商联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'FromContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货供应商联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'FromContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货供应商联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'FromMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货供应商邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'FromEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货地址（供应商地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'FromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货方店铺主键 （总部）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'StoreContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'StoreContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'StoreAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部收货单 @2015-04-13

buying 订单取消 brandid 字段 @2015-03-31

@2015-04-13
总部和店铺的 receive 单 分开。 收供应商货，以及 收店铺的退货。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_HQReceiveOrder_H'
GO
