USE [NewBuying]
GO
/****** Object:  Table [dbo].[UserMessageSetting_D]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMessageSetting_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[UserMessageCode] [varchar](64) NOT NULL,
	[ReceiveUserID] [int] NULL,
	[MessageServiceTypeID] [int] NOT NULL,
	[AccountNumber] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_USERMESSAGESETTING_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_D', @level2type=N'COLUMN',@level2name=N'UserMessageCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收人员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_D', @level2type=N'COLUMN',@level2name=N'ReceiveUserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息发送方式 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_D', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_D', @level2type=N'COLUMN',@level2name=N'AccountNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统用户消息通知设定。子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserMessageSetting_D'
GO
