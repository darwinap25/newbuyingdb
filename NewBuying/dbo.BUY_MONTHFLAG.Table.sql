USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_MONTHFLAG]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_MONTHFLAG](
	[MonthFlagID] [int] IDENTITY(1,1) NOT NULL,
	[MonthFlagCode] [varchar](64) NULL,
	[Note] [nvarchar](512) NULL,
	[JanuaryFlag] [int] NULL,
	[FebruaryFlag] [int] NULL,
	[MarchFlag] [int] NULL,
	[AprilFlag] [int] NULL,
	[MayFlag] [int] NULL,
	[JuneFlag] [int] NULL,
	[JulyFlag] [int] NULL,
	[AugustFlag] [int] NULL,
	[SeptemberFlag] [int] NULL,
	[DecemberFlag] [int] NULL,
	[OctoberFlag] [int] NULL,
	[NovemberFlag] [int] NULL,
 CONSTRAINT [PK_BUY_MONTHFLAG] PRIMARY KEY CLUSTERED 
(
	[MonthFlagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [JanuaryFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [FebruaryFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [MarchFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [AprilFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [MayFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [JuneFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [JulyFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [AugustFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [SeptemberFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [DecemberFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [OctoberFlag]
GO
ALTER TABLE [dbo].[BUY_MONTHFLAG] ADD  DEFAULT ((0)) FOR [NovemberFlag]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'MonthFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'MonthFlagCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'JanuaryFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'FebruaryFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'MarchFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'AprilFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'MayFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'JuneFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'JulyFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'AugustFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'SeptemberFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'DecemberFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'OctoberFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：生效。 0：无效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG', @level2type=N'COLUMN',@level2name=N'NovemberFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'月标志表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_MONTHFLAG'
GO
