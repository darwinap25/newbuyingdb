USE [NewBuying]
GO
/****** Object:  Table [dbo].[MemberFavorite]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberFavorite](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [bigint] NOT NULL,
	[FavoriteType] [int] NOT NULL DEFAULT ((0)),
	[ProdCodeStyle] [varchar](64) NULL,
	[ProdStyleTitle] [varchar](512) NULL,
	[ProdStyleDesc] [varchar](512) NULL,
	[ProdStylePIC] [varchar](512) NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[Memo] [varchar](max) NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_MEMBERFAVORITE] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收藏类型。默认0。  
0; 所有
1: 货品收藏' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'FavoriteType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员收藏货品编码 （可能是 product style）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'ProdCodeStyle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'title显示内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'ProdStyleTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品描述。（会员在浏览器中看到的货品描述）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'ProdStyleDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'ProdStylePIC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员收藏货品编码。可以为空。 非空时，此条记录可以直接添加到购物车' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'Memo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员收藏夹。（会员在浏览器中收藏的货品）
@2015-08-19 增加字段 product。 用户可以直接把表中的记录，添加到购物车
@2016-10-17  因为Nick需要使用自己的ID作为MemberID，所以MemberID需从Int类型扩展到 BigInt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberFavorite'
GO
