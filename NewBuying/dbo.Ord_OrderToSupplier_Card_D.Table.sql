USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_OrderToSupplier_Card_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_OrderToSupplier_Card_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[OrderSupplierNumber] [varchar](64) NOT NULL,
	[CardTypeID] [int] NULL,
	[CardGradeID] [int] NULL,
	[OrderQty] [int] NULL,
	[FirstCardNumber] [varchar](64) NULL,
	[EndCardNumber] [varchar](64) NULL,
	[BatchCardCode] [varchar](64) NULL,
	[PackageQty] [int] NULL,
	[OrderRoundUpQty] [int] NULL,
	[OrderAmount] [money] NULL,
	[OrderPoint] [int] NULL,
 CONSTRAINT [PK_ORD_ORDERTOSUPPLIER_CARD_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_D] ADD  DEFAULT ((1)) FOR [OrderQty]
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_D] ADD  DEFAULT ((1)) FOR [PackageQty]
GO
ALTER TABLE [dbo].[Ord_OrderToSupplier_Card_D] ADD  DEFAULT ((1)) FOR [OrderRoundUpQty]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提交供应商订单单号，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'OrderSupplierNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'CardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'CardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'OrderQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提供给供应商的首card号。（如果IsProvideNumber=1，那么需要提供Card号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'FirstCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提供给供应商的末card号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'EndCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FirstcardNumber的批次号编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'BatchCardCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装数量。默认为1. 不得为0 或者 null
@2014-08-28：由每一包中的Coupon数量， 改为 包裹的数量。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'PackageQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每一包中的Card数量，来源于规则表中的 OrderRoundUpQty。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'OrderRoundUpQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'购买金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'OrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'购买积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D', @level2type=N'COLUMN',@level2name=N'OrderPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'面向供应商的订货单，明细表
如果要求向供应商提供CardNumber。  并且要求每个Number都记录下来。 那么OrderQty=1， FirstCardNumber=EndCardNumber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_Card_D'
GO
