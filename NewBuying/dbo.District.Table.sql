USE [NewBuying]
GO
/****** Object:  Table [dbo].[District]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[District](
	[DistrictCode] [varchar](64) NOT NULL,
	[CityCode] [varchar](64) NOT NULL,
	[DistrictName1] [nvarchar](512) NULL,
	[DistrictName2] [nvarchar](512) NULL,
	[DistrictName3] [nvarchar](512) NULL,
 CONSTRAINT [PK_DISTRICT] PRIMARY KEY CLUSTERED 
(
	[DistrictCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区县编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'District', @level2type=N'COLUMN',@level2name=N'DistrictCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上级市编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'District', @level2type=N'COLUMN',@level2name=N'CityCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区县名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'District', @level2type=N'COLUMN',@level2name=N'DistrictName1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区县名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'District', @level2type=N'COLUMN',@level2name=N'DistrictName2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区县名称1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'District', @level2type=N'COLUMN',@level2name=N'DistrictName3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'区县资料表。（city下一级。中国第三级行政区，用于填写地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'District'
GO
