USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[StaffLogin_OLD]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StaffLogin_OLD]
  @StaffCode          VARCHAR(64),     -- 员工编码
  @StaffPassword      VARCHAR(64),     -- 密码
  @StoreCode          VARCHAR(64),     -- 登录的StoreCode
  @RegisterCode       VARCHAR(64),     -- 登录的RegisterCode
  @StaffLevel         INT OUTPUT,      -- 会员权限。（数字转换为3 位的 二进制数。 第一位管理员权限。 第二位促销员权限。第三位收银员权限）
  @GraceLoginCount    INT OUTPUT,      -- 允许连续Login失败次数。（密码不对导致login失败）
  @StaffID            INT OUTPUT,      -- 员工ID
  @StaffName          VARCHAR(64) OUTPUT,  -- 员工名字
  @LastLoginTime      DATETIME OUTPUT,     -- 员工最后登录时间
  @IsCheck            INT=0            -- 0: 正常登录，需要更新LastLoginTime。 1：仅仅Check用户。不更新LastLoginTime。 默认0
AS
/****************************************************************************
**  Name : StaffLogin
**  Version : 1.0.0.0
**  Description : 员工登录
**
declare @a int, @StaffLevel int, @GraceLoginCount int, @StaffID int, @StaffName VARCHAR(64), @LastLoginTime DATETIME
exec @a=StaffLogin '130', 'c4ca4238a0b923820dcc509a6f75849b', '屈臣氏--132', 'R01', @StaffLevel output, @GraceLoginCount output, @StaffID output, @StaffName output, @LastLoginTime output,0
print @a
print @StaffLevel
print @GraceLoginCount
print @StaffID print @StaffName print @LastLoginTime
select * from posstaff
select * from buy_store
select * from STAFFSTOREMAP where storeid= 51

**  Created by Gavin @2015-03-06
****************************************************************************/
BEGIN
  DECLARE @Status int, @Last_Reset_PWD datetime, @PWD_Valid_Days int, @StaffPWD varchar(64)
  SET @IsCheck = ISNULL(@IsCheck, 0)
  SELECT @Status = Status, @Last_Reset_PWD = Last_Reset_PWD, @PWD_Valid_Days = PWD_Valid_Days, 
    @GraceLoginCount = Grace_Login_Count, @StaffPWD = StaffPWD, @StaffLevel = StaffLevel,
    @StaffID = StaffID, @StaffName = StaffName, @LastLoginTime = LastLoginTime
  FROM POSStaff WHERE StaffCode = @StaffCode 
  
  IF @@ROWCOUNT <> 0
  BEGIN
    IF @Status = 1
    BEGIN
      IF (@PWD_Valid_Days = 0) or (DATEDIFF(dd, @Last_Reset_PWD, GETDATE()) < @PWD_Valid_Days)
      BEGIN
        IF @StaffPassword = @StaffPWD
        BEGIN
          IF EXISTS(SELECT * FROM STAFFSTOREMAP WHERE StaffID = @StaffID 
                       AND StoreID IN (SELECT StoreID FROM BUY_STORE WHERE StoreCode = @StoreCode))
          BEGIN
            IF @IsCheck = 0
            BEGIN          
              UPDATE POSStaff SET LastLoginTime = GETDATE(), LastLoginStore = @StoreCode, LastLoginRegister = @RegisterCode 
                WHERE StaffCode = @StaffCode
            END                        
            RETURN 0   
          END 
          ELSE
            RETURN -505
        END 
        ELSE
          RETURN -504      
      END 
      ELSE
        RETURN -503
    END 
    ELSE
      RETURN -502
  END
  ELSE
    RETURN -501

END

GO
