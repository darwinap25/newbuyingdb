USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSCouponRedeemRecall]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[POSCouponRedeemRecall]
  @TxnNoSN              varchar(512),           -- 交易唯一序号(每次正常调用,都提供不同的序号,确保唯一)
  @CouponExpiryDate     datetime output,        -- coupon有效期
  @CouponStatus         int output,             -- coupon状态  
  @CouponAmount         money output,           -- coupon的面额  
  @ApprovalCode         varchar(512) output,    -- 操作批核号码。（只有当@NeedCallConfirm=0时，返回ApprovalCode）
  @DeductAmount			money output,			-- 实际扣款金额
  @ForfeitAmount		money output,			-- Forfeit金额
  @CloseBalance			money output			-- 扣款后余额
AS
/****************************************************************************
**  Name : POSCouponRedeemRecall
**  Version: 1.0.0.0   (for RRG)
**  Description : POS提交的POSCouponRedeem 记录， 如果出现返回0，但获取其他参数失败的情况重新获取。
**
**  Parameter :
declare @A int, @UserID varchar(512), @TxnNo varchar(512), @ApprovalCode varchar(512)
set @UserID = 
set TxnNo =
exec @A = POSTxnConfirmRecall @UserID, @TxnNo, @ApprovalCode output
print @A
**  Created by: Robin @2012-09-27
**
****************************************************************************/
begin
  declare @CouponNumber varchar(512)

if isnull(@TxnNoSN,'')<>''
  select top 1 @CouponNumber=isnull(CouponNumber,''),@DeductAmount=Amount,@ApprovalCode=isnull(ApprovalCode,'') from ReceiveTxn where TxnNoSN=@TxnNoSN

select @CouponStatus=[Status], @CouponExpiryDate=CouponExpiryDate, @CouponAmount=CouponAmount from Coupon where CouponNumber=@CouponNumber
set @ForfeitAmount=@CouponAmount-@DeductAmount
set @CloseBalance=0
  
if @CouponNumber='' return -1
else return 0

end


GO
