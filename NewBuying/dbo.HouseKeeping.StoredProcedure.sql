USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[HouseKeeping]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[HouseKeeping]
  @SQLServer         nvarchar(64),    -- SQL Server 服务器名字
  @SQLDBName         nvarchar(64),    -- 数据库名字
  @SQLUser           nvarchar(64),     -- 登录数据库的用户名
  @SQLPassword       nvarchar(64),     -- 登录 密码
  @OutputPath        nvarchar(200),    -- 导出的备份文件的 路径
  @HoldTime          int = 3           -- HoldTime 保留时间, 单位:月
AS
/****************************************************************************
**  Name : HouseKeeping
**  Version: 1.0.0.0
**  Description : SVA DB houseKeeping (注意删除coupon_movement后，report会有影响)
  exec HouseKeeping '172.16.64.24\SQLEXPRESS', 'SVAWeb_Kiosk', 'sa', '1234qweR', 'F:\', 1 
** 
**  Created by:  Gavin @2015-01-19
****************************************************************************/
begin
  declare @SQLStr nvarchar(1000), @DelDate datetime, @MaxKeyID int, @NowDateStr varchar(10) 
  declare @OuputConponMovement nvarchar(200), @OuputCardMovement nvarchar(200), @OuputCoupon nvarchar(200)
          , @OuputReceiveTxn nvarchar(200), @OuputSalesH nvarchar(200),  @OuputSalesD nvarchar(200),  @OuputSalesT nvarchar(200)
  select @NowDateStr = CONVERT(varchar(10), Getdate(), 120) 
  set @DelDate = DATEADD(mm, -@HoldTime, getdate()) 
  
  set @OuputConponMovement = @OutputPath + 'CouponMovement_Backup_' + @NowDateStr + '.dat' 
  set @OuputCardMovement = @OutputPath + 'CardMovement_Backup_' + @NowDateStr + '.dat'
  set @OuputCoupon = @OutputPath + 'Coupon_Backup_' + @NowDateStr + '.dat'
  set @OuputReceiveTxn = @OutputPath + 'ReceiveTxn_Backup_' + @NowDateStr + '.dat'
  set @OuputSalesH = @OutputPath + 'SalesH_Backup_' + @NowDateStr + '.dat'
  set @OuputSalesD = @OutputPath + 'SalesD_Backup_' + @NowDateStr + '.dat'
  set @OuputSalesT = @OutputPath + 'SalesT_Backup_' + @NowDateStr + '.dat'
  
  -- Coupon_Movement
  select @MaxKeyID = max(KeyID) from Coupon_Movement where CreatedOn < @DelDate 
  set @SQLStr = 'bcp "select StoreID,ServerCode,RegisterCode,OprID,CardNumber,CouponNumber,CouponTypeID,RefKeyID,RefReceiveKeyID,RefTxnNo, OpenBal,Amount,CloseBal,OrgExpiryDate,NewExpiryDate,OrgStatus,NewStatus,BusDate,Txndate,TenderID,Additional,ApprovalCode,Remark,SecurityCode,CreatedOn,CreatedBy from ' + @SQLDBName + '.dbo.Coupon_movement where KeyID <= ' + CAST(@MaxKeyID as varchar) + '" queryout ' + @OuputConponMovement + ' -S' + @SQLServer + ' -U' + @SQLUser + ' -P' + @SQLPassword + ' -c '
  exec master..xp_cmdshell @SQLStr
  delete from Coupon_Movement where KeyID <= @MaxKeyID

  -- Card_Movement
  select @MaxKeyID = max(KeyID) from Card_Movement where CreatedOn < @DelDate 
  set @SQLStr = 'bcp "select StoreID,ServerCode,RegisterCode,OprID,CardNumber,RefKeyID,RefReceiveKeyID,RefTxnNo,OpenBal,Amount,CloseBal,OpenPoint,Points,ClosePoint,BusDate,Txndate,OrgExpiryDate,NewExpiryDate,OrgStatus,NewStatus,CardCashDetailID,CardPointDetailID,TenderID,Additional,Remark,ApprovalCode,SecurityCode,CreatedOn,CreatedBy from ' + @SQLDBName + '.dbo.card_movement where KeyID <= ' + CAST(@MaxKeyID as varchar) + '" queryout ' + @OuputCardMovement + ' -S' + @SQLServer + ' -U' + @SQLUser + ' -P' + @SQLPassword + ' -c '
  exec master..xp_cmdshell @SQLStr
  delete from Card_Movement where KeyID <= @MaxKeyID
  
  -- Coupon
  --select @MaxKeyID = max(KeyID) from Coupon where CreatedOn < @DelDate 
  set @SQLStr = 'bcp "select * from ' + @SQLDBName + '.dbo.Coupon where Status >= 4 and UpdatedOn <= ''' + CONVERT(varchar(10), @DelDate, 120) + '''" queryout ' + @OuputCoupon + ' -S' + @SQLServer + ' -U' + @SQLUser + ' -P' + @SQLPassword + ' -c '
  exec master..xp_cmdshell @SQLStr
  delete from Coupon where Status >= 4 and UpdatedOn <= @DelDate
    
  -- ReceiveTxn
  select @MaxKeyID = max(KeyID) from ReceiveTxn where CreatedDate < @DelDate 
  set @SQLStr = 'bcp "select * from ' + @SQLDBName + '.dbo.ReceiveTxn where KeyID <= ' + CAST(@MaxKeyID as varchar) + '" queryout ' + @OuputReceiveTxn + ' -S' + @SQLServer + ' -U' + @SQLUser + ' -P' + @SQLPassword + ' -c '
  exec master..xp_cmdshell @SQLStr
  delete from ReceiveTxn where KeyID <= @MaxKeyID
 
  -- SalesD
  set @SQLStr = 'bcp "select D.* from ' + @SQLDBName + '.dbo.Sales_D D left join ' + @SQLDBName + '.dbo.Sales_H H on D.TxnNo = H.TxnNo where H.UpdatedOn <= ''' + CONVERT(varchar(10), @DelDate, 120) + '''" queryout ' + @OuputSalesD + ' -S' + @SQLServer + ' -U' + @SQLUser + ' -P' + @SQLPassword + ' -c '
  exec master..xp_cmdshell @SQLStr
  delete from Sales_D from Sales_D D left join Sales_H H on D.TransNum = H.TransNum where H.UpdatedOn <= @DelDate  
  
  -- SalesT   
  set @SQLStr = 'bcp "select T.* from ' + @SQLDBName + '.dbo.Sales_T T left join ' + @SQLDBName + '.dbo.Sales_H H on T.TxnNo = H.TxnNo where H.UpdatedOn <= ''' + CONVERT(varchar(10), @DelDate, 120) + '''" queryout ' + @OuputSalesT + ' -S' + @SQLServer + ' -U' + @SQLUser + ' -P' + @SQLPassword + ' -c '
  exec master..xp_cmdshell @SQLStr
  delete from Sales_T from Sales_T T left join Sales_H H on T.TransNum = H.TransNum where H.UpdatedOn <= @DelDate
        
  -- SalesH
  set @SQLStr = 'bcp "select * from ' + @SQLDBName + '.dbo.Sales_H where UpdatedOn <= ''' + CONVERT(varchar(10), @DelDate, 120) + '''" queryout ' + @OuputSalesH + ' -S' + @SQLServer + ' -U' + @SQLUser + ' -P' + @SQLPassword + ' -c '
  exec master..xp_cmdshell @SQLStr
  delete from Sales_H where UpdatedOn <= @DelDate

  -- MessageReceiveList
  select @MaxKeyID = max(KeyID) from MessageReceiveList where UpdatedOn < @DelDate 
  delete from MessageReceiveList where KeyID < @MaxKeyID
  
  -- MessageObject 
  select @MaxKeyID = max(MessageID) from MessageObject where CreatedOn < @DelDate 
  delete from MessageObject where MessageID < @MaxKeyID

  -- CardCashDetail 
  select @MaxKeyID = max(KeyID) from CardCashDetail where UpdatedOn < @DelDate and BalanceAmount = 0
  delete from CardCashDetail where UpdatedOn < @DelDate and KeyID < @MaxKeyID
  
  -- CardPointDetail 
  select @MaxKeyID = max(KeyID) from CardPointDetail where UpdatedOn < @DelDate and BalancePoint = 0
  delete from CardPointDetail where UpdatedOn < @DelDate and KeyID < @MaxKeyID
end

GO
