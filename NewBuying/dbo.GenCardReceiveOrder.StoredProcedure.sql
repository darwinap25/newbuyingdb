USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenCardReceiveOrder]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GenCardReceiveOrder]
  @UserID                   int,             --操作员ID
  @OrderSupplierNumber      varchar(64)     --单号
AS
/****************************************************************************
**  Name : GenCardReceiveOrder
**  Version: 1.0.0.2
**  Description : 根据提交给供应商的订单来产生收货单 （供应商订单 批核时调用）
**  example :
  declare @a int  , @CouponOrderFormNumber varchar(64)
  set @CouponOrderFormNumber = 'BCC0000000502'
  exec @a = GenCardReceiveOrder 1, @CouponOrderFormNumber
  print @a  
  select * from Ord_OrderToSupplier_D
  select * from Ord_CouponReceive_H where ReferenceNo = 'BCC0000000502'
  select * from Ord_CouponReceive_D where CouponReceiveNumber = 'CREC00000000476'
delete from  Ord_CouponReceive_H where CouponReceiveNumber >= 'CREC00000000473' 
**  Created by: Gavin @2014-10-08 
**  Modify by: Gavin @2014-10-13 (ver 1.0.0.1) 取消拆分功能。
**  Modify by: Gavin @2014-12-03 (ver 1.0.0.2) 增加PurchaseType的处理。 (这里只处理PurchaseType=1的单子) 
****************************************************************************/
begin
  declare @BusDate datetime, @TxnDate datetime, @CardReceiveNumber varchar(64)
  declare @FirstNumber varchar(64), @EndNumber varchar(64), @CardTypeID int, @CardGradeID int, @CardCount int
  declare @IssuedDate datetime, @InitAmount money, @InitPoints int, @RandomPWD int, @InitPassword varchar(512), 
          @ExpiryDate datetime, @ApprovalCode varchar(512), @ReturnBatchID varchar(30), 
          @ReturnStartNumber varchar(64), @ReturnEndNumber varchar(64), @InitStatus int, @StoreID int
  declare @PackageQty int, @OrderRoundUpQty int            
   
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  set @TxnDate = getdate()
  
  -- 批核供应商订单批核时产生CouponNumber，并扩展订单明细
  DECLARE CUR_GenCouponReceiveOrder CURSOR fast_forward for
    select CardTypeID, CardGradeID, OrderQty, D.PackageQty, OrderRoundUpQty
      from Ord_OrderToSupplier_Card_D D 
        left join Ord_OrderToSupplier_Card_H H on H.OrderSupplierNumber = D.OrderSupplierNumber
      where D.OrderSupplierNumber = @OrderSupplierNumber and isnull(FirstCardNumber, '') = '' and H.PurchaseType = 1
  OPEN CUR_GenCouponReceiveOrder
  FETCH FROM CUR_GenCouponReceiveOrder INTO @CardTypeID, @CardGradeID, @CardCount, @PackageQty, @OrderRoundUpQty
  WHILE @@FETCH_STATUS=0 
  BEGIN         
    set @IssuedDate = GETDATE()
    select @InitAmount = CardTypeInitAmount, @InitPoints = CardTypeInitPoints from CardGrade where CardGradeID = @CardGradeID
    set @RandomPWD = 0
    set @InitPassword = '' 
    set @ExpiryDate = null
    set @InitStatus = 0
    set @ApprovalCode = ''
    exec BatchGenerateNumber @UserID, 0, @CardTypeID, @CardGradeID, null, @CardCount, @IssuedDate, @InitAmount, @InitPoints, @RandomPWD, @InitPassword, @ExpiryDate, @OrderSupplierNumber, @ApprovalCode, @UserID, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, '', @InitStatus     
 
    -- @ReturnBatchID是自增长KeyID，认为当前过程批量产生时，大于@ReturnBatchID的都是属于这个订单的。
    insert into Ord_OrderToSupplier_Card_D (OrderSupplierNumber, CardTypeID, CardGradeID, OrderQty, FirstCardNumber, EndCardNumber, BatchCardCode, PackageQty, OrderRoundUpQty)
    select @OrderSupplierNumber, @CardTypeID, @CardGradeID, 1, CardNumber, CardNumber, BatchCardCode, @PackageQty, @OrderRoundUpQty
    from
    (
      select ROW_NUMBER() OVER(order by CardNumber) as iid, CardNumber, B.BatchCardCode
       from Card C left join BatchCard B on C.BatchCardID = B.BatchCardID
       where C.BatchCardID >= @ReturnBatchID and C.CardTypeID = @CardTypeID and C.CardGradeID = @CardGradeID
    ) A 
    where iid <= @CardCount   
           
  FETCH FROM CUR_GenCouponReceiveOrder INTO @CardTypeID, @CardGradeID, @CardCount, @PackageQty, @OrderRoundUpQty
  END
  CLOSE CUR_GenCouponReceiveOrder 
  DEALLOCATE CUR_GenCouponReceiveOrder 
  
  delete from Ord_OrderToSupplier_Card_D where OrderSupplierNumber = @OrderSupplierNumber and isnull(FirstCardNumber, '') = ''  
  -- 
   
  -- 产生收货单。
    exec GetRefNoString 'CRECCA', @CardReceiveNumber output     
    insert into Ord_CardReceive_H(CardReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ReceiveType, ordertype)
    select @CardReceiveNumber, OrderSupplierNumber, StoreID, SupplierID, SendAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, 1, ordertype
     from Ord_OrderToSupplier_Card_H where OrderSupplierNumber = @OrderSupplierNumber and PurchaseType = 1 

    insert into Ord_CardReceive_D (CardReceiveNumber, CardTypeID, CardGradeID, Description, OrderQty, ActualQty, 
          FirstCardNumber, EndCardNumber, BatchCardCode, CardStockStatus, ReceiveDateTime)  
    select @CardReceiveNumber, CardTypeID, CardGradeID, '',  OrderQty, OrderQty,  
         FirstCardNumber, EndCardNumber, BatchCardCode, 1, GETDATE()
        from Ord_OrderToSupplier_Card_D D
         left join Ord_OrderToSupplier_Card_H H on H.OrderSupplierNumber = D.OrderSupplierNumber
      where D.OrderSupplierNumber = @OrderSupplierNumber and H.PurchaseType = 1
      
  -- 库存数量变动
  exec ChangeCardStockStatus 2, @OrderSupplierNumber, 1   
        
  return 0
end

GO
