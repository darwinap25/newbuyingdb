USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[LostMemberPassword]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[LostMemberPassword]
  @UserID            int, --  操作员ID
  @MemberRegistID    nvarchar(512), --  会员注册ID （注册手机号） 
  @MemberEmail       nvarchar(512),  --  会员注册的邮箱  
  @NewPassword       varchar(512) output, --  会员新密码
  @ReturnMessageChannel int output,  -- 发送消息的serviceid. 0:不发, 1:邮件, 2:短信
  @ReturnXMLStr      varchar(max) output, -- 发消息需要的XML字符串
  @AssignedServiceID int=0,   --指定发送消息的ServiceID，默认0.  0表示不使用. ID 参照messageserviceID内容，一旦设置后表示跳过之前的逻辑检查，直接reset。
  @ReturnMessageAccount varchar(512) output    -- 发送消息的AccountNumber
AS
/****************************************************************************
**  Name : LostMemberPassword  会员密码遗忘
**  Version: 1.0.0.15
**  Description : 会员密码遗失，校验输入的号码，邮箱，重新设置密码，并发出消息。
**  Parameter :
**
 declare @a int, @NewPassword			varchar(512) , @ReturnMessageChannel int,
 @ReturnXMLStr      varchar(max)
-- set @NewPassword = '123'
  exec @a=LostMemberPassword  1, '1157625888', '', @NewPassword output, @ReturnMessageChannel output, @ReturnXMLStr output
  print @a
  print @NewPassword
select cast(MessageBody as nvarchar(max)) as MessageBodyChar, * from messageobject   order by createdon
select * from member where memberregistermobile = '1157625888' 
update member set memberemail = 'Nathan.WU@tap-group.com.cn'  where  memberregistermobile = '08613801605014'   
  select * from member where MemberRegisterMobile = '086444' 
  select * from membermessageaccount where memberid = 6156   
  select * from messageobject
**  Created by: Gavin @2012-12-08
**  Modify by: Gavin @2012-12-14 (ver 1.0.0.1) 修改资料时，同时把@MemberRegistID更新到MemberMessageAccount,作为手机号
**  Modify by: Gavin @2013-02-26 (ver 1.0.0.2) 取消@MemberEmail的校验，此账号用来发送消息。如果输入了Mail，那么使用这个账号，只发email，如果没有填，则按照原来的设置。
**  Modify by: Gavin @2013-02-28 (ver 1.0.0.3) @MemberEmail需要校验（和member表的MemberEmail），两者必须相同，而且不能为空。 校验通过后，新密码发到这个邮箱，其他短信之类的消息不再发送。增加新的返回值。
**  Modify by: Gavin @2013-04-03 (ver 1.0.0.4) 根据不同的情况，需要增加返回不同的错误代码。增加返回 -88
**  Modify by: Gavin @2013-04-28 (ver 1.0.0.5) 不判断@MemberEmail.(@MemberEmail输入都是空), 如果有邮箱，则只发邮件。如果用户没邮箱，则判断Member.ResetPWDCount, 如果0，则发短信，并且set ResetPWDCount=1， 否则返回错误
**  Modify by: Robin @2013-04-29 (ver 1.0.0.6) fixed wrong variable '@MemberEmail', it should use '@Email'
**  Modify by: Gavin @2013-05-06 (ver 1.0.0.7) 增加参数，允许跳过检查，直接指定发送消息的方法，来发送新密码消息。（如果指定的方法没有设置账号，则出错 -87）
**  Modify by: Gavin @2013-05-28 (ver 1.0.0.8) 增加返回参数，返回发送消息的messageservicetypeid。
**  Modify by: Gavin @2013-08-19 (ver 1.0.0.9) 不再直接发消息数据，改为返回含有消息替换内容的XML字符串
**  Modify by: Gavin @2013-10-14 (ver 1.0.0.10) 记录操作到UserAction_Movement表
**  Modify by: Gavin @2013-12-06 (ver 1.0.0.11) 按照passwordrule的设置，设置密码的长度。 ]
**  Modify by: Gavin @2014-01-02 (ver 1.0.0.12) 增加返回参数@ReturnMessageAccount,返回发送消息的AccountNumber
**  Modify by: Gavin @2014-02-24 (ver 1.0.0.13) 必须是已经注册了邮箱的用户，才可以使用忘记密码。所以@Email不可为空。@AssignedServiceID强制为0，表示只用email发送忘记密码信息。
**  Modify by: Gavin @2014-07-07 (ver 1.0.0.14) 增加参数@ReturnVerCode,@VerCode, 使用校验码来完成重置密码.
**  Modify by: Gavin @2015-03-24 (ver 1.0.0.15) 因为密码规则的要求, 返回的密码改为, 6位随机数+ hardcode的字符: Aa
**
****************************************************************************/
begin
 
  declare @MemberPassword varchar(512), @PWDValidDays int, @PWDValidDaysUnit int, @ResetPWDDays int, @NewExpiryDate datetime,
         @Email nvarchar(512), @MemberID int, @PWD varchar(512)
  declare @MemberIDStr varchar(64), @MessageID int, @ResetPWDCount int, @AccountNumber nvarchar(512),
          @CountryCode	varchar(64), @MemberMobilePhone nvarchar(512), @PWDMinLength  int, @RandNum decimal(20, 0) 
          
  set @MemberEmail = isnull(@MemberEmail, '')
  set @AssignedServiceID = isnull(@AssignedServiceID, 0)
  set @ReturnMessageChannel = 0
  set @AssignedServiceID = 0  -- ver 1.0.0.13
  
  select @MemberID = memberID, @MemberPassword = MemberPassword, @Email = isnull(MemberEmail, ''), 
    @ResetPWDCount = isnull(ResetPWDCount, 0), @CountryCode = isnull(CountryCode, ''),
    @MemberMobilePhone  = isnull(MemberMobilePhone, '')
    from member where (MemberRegisterMobile = @MemberRegistID or ISNULL(@MemberRegistID,'') = '')    
  if @@ROWCOUNT = 0 
    return -1 
  if isnull(@Email, '') = ''		 -- ver 1.0.0.13
    return -88
--  if @MemberEmail <> isnull(@Email, '')
--    return -89  

  if @AssignedServiceID	= 0
  begin
    -- if not exit '@' in @Email, then the @Email is bad  
    if (isnull(@Email, '') = '' or charindex('@', @Email) = 0) and @ResetPWDCount > 0
  	  return -88
  end else
  begin
	select @AccountNumber = AccountNumber from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = @AssignedServiceID
	if isnull(@AccountNumber, '') = ''
	  return -87
  end
  
     select Top 1 @PWDValidDays = PWDValidDays, @PWDValidDaysUnit = PWDValidDaysUnit, @ResetPWDDays = ResetPWDDays, @PWDMinLength = PWDMinLength 
       from PasswordRule where MemberPWDRule = 1
     set @PWDMinLength = isnull(@PWDMinLength, 6)
     if @PWDMinLength = 0 
       set @PWDMinLength = 6    
     set @NewExpiryDate = convert(varchar(10), getdate(), 120)     
     if @PWDValidDaysUnit = 0
       set @NewExpiryDate = '2100-01-01'
     if @PWDValidDaysUnit = 1
       set @NewExpiryDate = DATEADD(yy, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 2
       set @NewExpiryDate = DATEADD(mm, @PWDValidDays, @NewExpiryDate)   
     if @PWDValidDaysUnit = 3
       set @NewExpiryDate = DATEADD(ww, @PWDValidDays, @NewExpiryDate) 
     if @PWDValidDaysUnit = 4
       set @NewExpiryDate = DATEADD(dd, @PWDValidDays, @NewExpiryDate)  
--print '@PWDMinLength'
--print @PWDMinLength
    select @RandNum = round(rand() * 100000000000000000, 0)
    set @NewPassword = right('00000000000000000' + convert(varchar(20), @RandNum), @PWDMinLength) 
    -- ver 1.0.0.15
    set @NewPassword = @NewPassword + 'Aa'
--print @NewPassword     
    set @PWD = @NewPassword  
    if isnull(@PWD, '') <> ''
       set @PWD = dbo.EncryptMD5(@PWD)    
    Update Member set MemberPassword = @PWD,  PasswordExpiryDate = @NewExpiryDate, PWDExpiryPromptDays = @ResetPWDDays, 
           UpdatedBy = @UserID, UpdatedOn = Getdate(), ResetPWDCount = @ResetPWDCount + 1
        where memberID = @MemberID       

  if exists(select * from MemberMessageAccount where MemberID = @MemberID and MessageServiceTypeID = 2)
  begin
    update MemberMessageAccount set AccountNumber = @MemberRegistID where MemberID = @MemberID and MessageServiceTypeID = 2      
  end else
  begin
    insert into MemberMessageAccount (MemberID, MessageServiceTypeID, AccountNumber, Status, Note, IsPrefer, 
       CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
    values(@MemberID, 2, @MemberRegistID, 1, '', 1, getdate(), @UserID, getdate(), @UserID)
  end

  -- 产生消息记录 
  declare @Tempstr  varchar(max)
  set @Tempstr = 'MPASSWORD=' + @NewPassword + ';MEMBERID=' + cast(@MemberID as varchar)  
      
  if @AssignedServiceID	= 0
  begin
    if @Email <> ''
    begin
      set @Tempstr = @Tempstr + ';MSGACCOUNTTYPE=1' + ';MSGACCOUNT=' +  @Email  
      set @ReturnMessageChannel = 1
      set @ReturnMessageAccount = @Email
    end else
    begin
      set @Tempstr = @Tempstr + ';MSGACCOUNTTYPE=2' + ';MSGACCOUNT=' +  @MemberRegistID 
      set @ReturnMessageChannel = 2
      set @ReturnMessageAccount = @MemberRegistID  
    end
  end else
  begin
    set @Tempstr = @Tempstr + ';MSGACCOUNTTYPE=' + cast(@AssignedServiceID as varchar) + ';MSGACCOUNT=' +  @AccountNumber 
    set @ReturnMessageChannel = @AssignedServiceID
    set @ReturnMessageAccount = @AccountNumber   
  end  
  
  exec GenMessageJSONStr 'LostMemberPassword', @Tempstr, @ReturnXMLstr output      

  --end 
  exec RecordUserAction @MemberID, 3, '', '', 'Lost member password', '', '', '', @UserID
  
  return 0
end

GO
