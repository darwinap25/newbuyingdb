USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PRODUCT_PIC_Pending]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PRODUCT_PIC_Pending](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ProdCode] [varchar](64) NOT NULL,
	[ProductThumbnailsFile] [nvarchar](512) NULL,
	[ProductFullPicFile] [nvarchar](512) NULL,
	[ProductPicNote1] [nvarchar](512) NULL,
	[ProductPicNote2] [nvarchar](512) NULL,
	[ProductPicNote3] [nvarchar](512) NULL,
	[IsVideo] [int] NULL,
	[Is360Pic] [int] NULL,
	[IsSizeCategory] [int] NULL,
 CONSTRAINT [PK_BUY_PRODUCT_PIC_PENDING] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_PRODUCT_PIC_Pending] ADD  DEFAULT ((0)) FOR [IsVideo]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_PIC_Pending] ADD  DEFAULT ((0)) FOR [Is360Pic]
GO
ALTER TABLE [dbo].[BUY_PRODUCT_PIC_Pending] ADD  DEFAULT ((0)) FOR [IsSizeCategory]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品缩略图' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'ProductThumbnailsFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品完整图' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'ProductFullPicFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'ProductPicNote1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片备注2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'ProductPicNote2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片备注3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'ProductPicNote3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: 是video。 0：不是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'IsVideo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：是360图。0：不是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'Is360Pic'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1：是尺寸示意图。0：不是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending', @level2type=N'COLUMN',@level2name=N'IsSizeCategory'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品图片
Pending表 @2016-08-08 (for bauhaus)
@2016-08-17 增加字段：isvideo，is360pic,IsSizeCategory' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_PIC_Pending'
GO
