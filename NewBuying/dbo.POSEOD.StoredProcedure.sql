USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSEOD]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[POSEOD]
  @StoreCode             VARCHAR(64),              -- POS 的店铺编号
  @RegisterCode          VARCHAR(64),              -- POS 的注册编号
  @BusDate               DATE OUTPUT,              -- 交易的Business date
  @CashierID             INT                       -- 收银员
AS
/****************************************************************************
**  Name : POSEOD
**  Version : 1.0.0.0
**  Description : POS EOD
**
  declare @a int, @BusDate date
  exec @a = POSEOD 'watsons214', '201', @BusDate output, 1
  print @a  print @BusDate
  select * from sodeod where storecode = 'watsons214'
  update sodeod set eod = 1 where storecode = 'watsons214'
  delete from sodeod  where storecode = 'watsons214'
**  Created by Gavin @2015-03-26
**  Modify by Gavin @2016-01-07 (1.0.0.1) 增加StoreCode使用
****************************************************************************/
BEGIN
  IF @BusDate IS NULL
    SELECT TOP 1 @BusDate = BusDate FROM SODEOD WHERE SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode
	  ORDER BY BusDate DESC
  
  IF @BusDate IS NOT NULL
  BEGIN
    -- 更新SODEOD
    UPDATE SODEOD SET EOD = 1 WHERE BusDate = @BusDate AND SOD = 1 AND EOD = 0 AND StoreCode = @StoreCode
    IF @@ROWCOUNT = 0
      RETURN -512 
    -- 更新Sales_H  
    UPDATE Sales_H SET CompleteDate = @BusDate,  UpdatedOn = GETDATE() 
      WHERE CompleteDate IS NULL
    -- 产生POS报表 (未定)
    
    -- 产生主档数据。
    exec Gen_BUY_RPRICE_M '', NULL     
  END
  ELSE
    RETURN -510
    
  RETURN 0
END

GO
