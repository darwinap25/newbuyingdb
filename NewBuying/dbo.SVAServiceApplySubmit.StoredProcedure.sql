USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SVAServiceApplySubmit]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SVAServiceApplySubmit]
  @TxnNo				 varchar(512) output, -- 交易号码
  @TxnType			     int,				  -- 交易类型
  @RefTransNum           varchar(64),         -- 原单号  
  @BusDate				 datetime output,     -- 交易busdate
  @TxnDate				 datetime,			  -- 交易系统时间
  @Reason                varchar(512),        -- 售后单原因
  @UserID				 int,		          -- 操作员ID
  @SalesDetail			nvarchar(max)		  -- 交易单货品明细(交易货品, XML格式字符串)
AS
/****************************************************************************
**  Name : [SVAServiceApplySubmit]
**  Version: 1.0.0.1   
**  Description : 新增退化货申请单。
**  Created by: Lisa @2016-01-05
**  Update  by: Lisa @2016-01-06
**  Update  by: Lisa @2016-01-07 新增的时候从store表中把区域、县、城市、地区默认读取出来
@SalesDetail格式范例:  
<ROOT>
  <PLU  SeqNo=""   RefSeqNo=""  ProdCode="0001" ProdDesc=""  QTY=""  Description=""  UploadPicFile1=""  UploadPicFile2=""  UploadPicFile3=""  UploadPicFile4="" UploadPicFile5=""  ></PLU>
  <PLU  SeqNo=""   RefSeqNo=""  ProdCode="0002" ProdDesc=""  QTY=""  Description=""  UploadPicFile1=""  UploadPicFile2=""  UploadPicFile3=""  UploadPicFile4="" UploadPicFile5=""  ></PLU>
</ROOT>
****************************************************************************/
BEGIN
	SET NOCOUNT ON;
	declare @MemberSalesFlag int ,@Collected int,@TotalAmount decimal,@Result int, @MemberID int,@KeyID int,
	@CardNumber varchar(64),@StoreCode varchar(64),@RegisterCode varchar(64),@SeqNo varchar(64)
	declare @SalesDData xml
	--详细    
	declare @AfterSales_D table (SeqNo varchar(64),TxnNo varchar(64),TxnType int,RefSeqNo varchar(64),
	ProdCode varchar(64), ProdDesc nvarchar(512), DepartCode varchar(64),Qty int ,Description varchar(1000),
	UploadPicFile1 varchar(512),UploadPicFile2 varchar(512),UploadPicFile3 varchar(512),UploadPicFile4 varchar(512),UploadPicFile5 varchar(512) )



	 --PLU detail数据写入临时表
    if isnull(@SalesDetail, '') <> ''
    begin
	set @SalesDData = @SalesDetail
	Insert into @AfterSales_D(SeqNo,TxnNo,TxnType,RefSeqNo, ProdCode, ProdDesc,DepartCode, Qty, 
	Description,UploadPicFile1,UploadPicFile2,UploadPicFile3,UploadPicFile4,UploadPicFile5)
	select
	    T.v.value('@SeqNo','varchar(64)') as SeqNo,
	    T.v.value('@TxnNo','varchar(64)') as TxnNo , 
	    T.v.value('@TxnType','int')as TxnType ,
		T.v.value('@RefSeqNo','varchar(64)')as RefSeqNo,
		T.v.value('@ProdCode','varchar(64)')as ProdCode,
		T.v.value('@ProdDesc','varchar(64)')as ProdDesc,
	    T.v.value('@DepartCode','varchar(64)')as DepartCode,
		T.v.value('@QTY','int') as QTY,
		T.v.value('@Description','varchar(1000)') as Description,
		T.v.value('@UploadPicFile1','varchar(512)') as UploadPicFile1,
		T.v.value('@UploadPicFile2','varchar(512)') as UploadPicFile2,
		T.v.value('@UploadPicFile3','varchar(512)') as UploadPicFile3,
		T.v.value('@UploadPicFile4','varchar(512)') as UploadPicFile4,
		T.v.value('@UploadPicFile5','varchar(512)') as UploadPicFile5
	  from @SalesDData.nodes('/ROOT/PLU') T(v)         
  end	

   begin tran -- 开始事务
	set @Result = 0

	--根据RefTransNum（原单号）查询Sales_H 数据
	select @TotalAmount=TotalAmount,@MemberID=MemberID,@CardNumber=CardNumber,@StoreCode=StoreCode,@RegisterCode=RegisterCode from Sales_H where TransNum=@RefTransNum
	--Sales_H 存在当前输入的交易号,可以提交退换号单
	if exists(select TransNum from Sales_H where TransNum=@RefTransNum)   
	begin
		--判断是否是会员
		if(@MemberID!=null  or @CardNumber!=null )
		set @MemberSalesFlag=1;
		else
		set @MemberSalesFlag=0;
		--写入AfterSales_H数据
	    insert into AfterSales_H(TxnNo,TxnType,RefTransNum,StoreCode,RegisterCode,BusDate,TxnDate,TotalAmount,Status,Reason,
		InvalidateFlag,MemberSalesFlag,MemberID,CardNumber,CreatedOn,CreatedBy)
		values(@TxnNo,@TxnType,@RefTransNum,@StoreCode,@RegisterCode,@BusDate,@TxnDate,@TotalAmount,0,@Reason,
		0,@MemberSalesFlag,@MemberID,@CardNumber,GetDate(),@UserID)
	end
	--不存在记录，回滚操作
	else
	  begin
	     rollback tran
		 return 
	  end

	
   --写入PLU detail数据
   if isnull(@SalesDetail, '') <> ''
   begin
		delete from AfterSales_D where  TxnNo=@TxnNo
		--查询交易编号
		Insert into AfterSales_D(SeqNo,TxnNo,TxnType,RefSeqNo,StoreCode,RegisterCode,BusDate,TxnDate, ProdCode, ProdDesc,DepartCode, Qty, 
		Description,UploadPicFile1,UploadPicFile2,UploadPicFile3,UploadPicFile4,UploadPicFile5,CreatedOn,CreatedBy)
		select SeqNo,@TxnNo,@TxnType,RefSeqNo,@StoreCode,@RegisterCode,@BusDate,@TxnDate,ProdCode,ProdDesc,DepartCode,Qty,Description,
		UploadPicFile1,UploadPicFile2,UploadPicFile3,UploadPicFile4,UploadPicFile5,GetDate(),@UserID from @AfterSales_D

   end  

   begin
		--修改Sales_D
		 --update Sales_D set Collected = case Collected when 0 then 80 when 1 then 81 when 2 then 82  when 4 then 84 end
	  --   where TxnNo =@TxnNo
	  --   and SeqNo in (select RefSeqNo from AfterSales_D where TxnNo =@TxnNo) 
	  	  update Sales_D set Collected = case Collected when 0 then 80 when 1 then 81 else 84 end
	      where TransNum = @RefTransNum 
	      and seqno in (select RefSeqNo from AfterSales_D where txnno = @TxnNo) 
   end

   IF @@ERROR <> 0
    begin
	set @Result=-1;
      rollback tran
	  return
    end
   else
     begin
	 set @Result=0;
	  commit tran
	 end

    SET NOCOUNT OFF
	return @Result
END

GO
