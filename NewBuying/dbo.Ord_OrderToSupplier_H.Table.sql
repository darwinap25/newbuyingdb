USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_OrderToSupplier_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_OrderToSupplier_H](
	[OrderSupplierNumber] [varchar](64) NOT NULL,
	[SupplierID] [int] NULL,
	[OrderSupplierDesc] [varchar](512) NULL,
	[OrderType] [int] NULL DEFAULT ((0)),
	[ReferenceNo] [varchar](64) NULL,
	[SendMethod] [int] NULL,
	[SendAddress] [nvarchar](512) NULL,
	[SupplierAddress] [nvarchar](512) NULL,
	[SuppliertContactName] [varchar](512) NULL,
	[SupplierPhone] [varchar](512) NULL,
	[SupplierEmail] [varchar](512) NULL,
	[SupplierMobile] [varchar](512) NULL,
	[StoreID] [int] NULL,
	[StoreContactName] [varchar](512) NULL,
	[StorePhone] [varchar](512) NULL,
	[StoreEmail] [varchar](512) NULL,
	[StoreMobile] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[IsProvideNumber] [int] NULL DEFAULT ((0)),
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[CouponTypeID] [int] NOT NULL,
	[CouponQty] [int] NOT NULL,
	[CompanyID] [int] NULL,
	[PackageQty] [int] NULL DEFAULT ((1)),
	[Subject] [varchar](512) NULL,
	[CouponNatureID] [int] NULL,
 CONSTRAINT [PK_ORD_ORDERTOSUPPLIER_H] PRIMARY KEY CLUSTERED 
(
	[OrderSupplierNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_OrderToSupplier_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_OrderToSupplier_H] ON [dbo].[Ord_OrderToSupplier_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_OrderToSupplier_H
* Version: 1.0.0.0
* Description : 给供应商的Coupon订单表的批核触发器
*  
** Create By Gavin @2014-06-04
*/
/*==============================================================*/
BEGIN  
  declare @OrderSupplierNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  
  DECLARE CUR_OrderToSupplier_H CURSOR fast_forward FOR
    SELECT OrderSupplierNumber, ApproveStatus, CreatedBy FROM INSERTED
  OPEN CUR_OrderToSupplier_H
  FETCH FROM CUR_OrderToSupplier_H INTO @OrderSupplierNumber, @ApproveStatus, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    select @OldApproveStatus = ApproveStatus from Deleted where OrderSupplierNumber = @OrderSupplierNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin    
      exec GenApprovalCode @ApprovalCode output    
      exec GenCouponReceiveOrder @CreatedBy, @OrderSupplierNumber
      update Ord_OrderToSupplier_H set ApprovalCode = @ApprovalCode where OrderSupplierNumber = @OrderSupplierNumber   
      -- 产生提示消息.
      exec GenUserMessage @CreatedBy, 9, 1, @OrderSupplierNumber 
    end
    
    FETCH FROM CUR_OrderToSupplier_H INTO @OrderSupplierNumber, @ApproveStatus, @CreatedBy    
  END
  CLOSE CUR_OrderToSupplier_H 
  DEALLOCATE CUR_OrderToSupplier_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'提交供应商订单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'OrderSupplierNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商主键，外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SupplierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单供应商描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'OrderSupplierDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。如果是由其他单据产生的，就填此单据号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货方式。1：直接交付（打印），2：SMS，3：Email，4：Social Network，5：快递送货（实体Coupon）。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SendMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址。（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SendAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SupplierAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SuppliertContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SupplierPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SupplierEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'SupplierMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺ID （总部）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'StorePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'StoreEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要向供应商提供号码。（供应商根据提供的号码打印）。 默认0。  0：不提供。 1：提供' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'IsProvideNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'CouponQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装数量。默认为1. 不得为0 或者 null' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'PackageQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任意内容。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H', @level2type=N'COLUMN',@level2name=N'Subject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'面向供应商的订货单（订货实体Coupon）
注：工作流程：总部向供应商下订单时，提供需要的Coupon号码（首号码 和 数量）。供应商根据提供的号码 提供Coupon。
修改内容： @2014-06-04：
CouponTypeID和QTY 移到 H 表，一个订单只能下一个CouponType。多个CouponType需要另外开单。 Detail表中的CouponType作为冗余字段。
修改内容： @2014-06-05：
OrderQty 改成 CouponQty， CouponTypeID和CouponQty 放在最后' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_OrderToSupplier_H'
GO
