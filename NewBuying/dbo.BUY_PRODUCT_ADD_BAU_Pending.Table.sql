USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PRODUCT_ADD_BAU_Pending]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PRODUCT_ADD_BAU_Pending](
	[ProdCode] [varchar](64) NOT NULL,
	[Standard_Cost] [money] NULL,
	[Export_Cost] [money] NULL,
	[AVG_Cost] [money] NULL,
	[MODEL] [varchar](64) NULL,
	[SKU] [varchar](64) NULL,
	[YEAR] [int] NULL,
	[REORDER_LEVEL] [int] NULL,
	[HS_CODE] [varchar](64) NULL,
	[COO] [varchar](64) NULL,
	[SIZE_RANGE] [varchar](64) NULL,
	[DESIGNER] [varchar](64) NULL,
	[BUYER] [varchar](64) NULL,
	[MERCHANDISER] [varchar](64) NULL,
	[RETIRE_DATE] [datetime] NULL,
	[CompanyCode] [varchar](64) NULL,
	[Material] [varchar](64) NULL,
	[overview] [varchar](64) NULL,
	[style] [varchar](64) NULL,
	[fabric_care] [varchar](64) NULL,
	[SizeM1] [varchar](64) NULL,
	[SizeM2] [varchar](64) NULL,
	[SizeM3] [varchar](64) NULL,
	[SizeM4] [varchar](64) NULL,
	[SizeM5] [varchar](64) NULL,
	[SizeM6] [varchar](64) NULL,
	[SizeM7] [varchar](64) NULL,
	[SizeM8] [varchar](64) NULL,
 CONSTRAINT [PK_BUY_PRODUCT_ADD_BAU_PENDING] PRIMARY KEY CLUSTERED 
(
	[ProdCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_ADD_BAU_Pending', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标准成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_ADD_BAU_Pending', @level2type=N'COLUMN',@level2name=N'Standard_Cost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'出口成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_ADD_BAU_Pending', @level2type=N'COLUMN',@level2name=N'Export_Cost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'平均成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_ADD_BAU_Pending', @level2type=N'COLUMN',@level2name=N'AVG_Cost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Model' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_ADD_BAU_Pending', @level2type=N'COLUMN',@level2name=N'MODEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bauhaus用的货品sku。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_ADD_BAU_Pending', @level2type=N'COLUMN',@level2name=N'SKU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bauhaus 货品的 附加属性内容。
Pending表 @2016-08-08 (for bauhaus)
@2016-08-17： 增加字段：sizeM1 - sizeM8
@2016-10-11： sizeM1 - sizeM8 字段类型由decimal改成varchar(64)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCT_ADD_BAU_Pending'
GO
