USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCardGrade]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[UpdateCardGrade]
  @CardNumber		nvarchar(30),
  @CardTypeID		int=0,
  @TotalAmount      money = 0 
as
/****************************************************************************
**  Name : UpdateCardGrade (for Bauhaus)  
**  Version: 1.0.0.4
**  Description :  根据卡积分升级。
**  Parameter :   
**  return:   select * from sodeod
exec UpdateCardGrade '', 1
**  Created by: Gavin @2012-04-19
**  Modify by: Gavin @2016-01-29 (ver 1.0.0.1) 增加赠送Coupon
**  Modify by: Gavin @2016-02-01 (ver 1.0.0.2) 从 grade的升序序来判断是否这个grade能到达. (升序可以实现逐级升级,并且每级的Coupon奖励都可以获得) 
**  Modify by: Gavin @2016-02-05 (ver 1.0.0.3) @Busdate必须赋值,否则couponissuedate 会为空.
                                               修正发Coupon的问题
**  Modify by: Gavin @2016-02-17 (ver 1.0.0.4) 增加支持CardGradeUpdMethod=3的设置（單筆消費滿多少錢就可以升級）。（但是不考虑降级）
**
****************************************************************************/
begin
  Declare @PreCardGradeID int, @CardGradeID int, @CumulativeEarnPoints int, @CumulativeConsumptionAmt money, @PreCardGradeRank int
  declare @CardGradeUpdMethod int, @CardGradeUpdThreshold int, @CardGradeRank int
  declare @CardValidityDuration int, @CardValidityUnit int
  declare @CouponNumber varchar(64), @BindCouponCount int, @i int, @BindCouponTypeID int, @CouponAmount money, 
          @CouponExpiryDate datetime, @ApprovalCode varchar(6), @NewCouponStatus int, @CouponStatus int,
		  @NewCouponExpiryDate datetime, @NewCardExpiryDate datetime, @BusDate datetime, @TxnDate datetime, @MemberID int
  declare @NewCardGradeID int
  
  select @CardTypeID = CardTypeID, @PreCardGradeID = CardGradeID, @CumulativeEarnPoints = CumulativeEarnPoints, @CumulativeConsumptionAmt = CumulativeConsumptionAmt
    from card where CardNumber = @CardNumber ---and CardTypeID = @CardTypeID

  if @@ROWCOUNT = 0
    return -2
  set @PreCardGradeID = isnull(@PreCardGradeID, 0)
  select @PreCardGradeRank = CardGradeRank from cardGrade where CardGradeID = @PreCardGradeID  
  set @PreCardGradeRank = isnull(@PreCardGradeRank, 0)
  set @NewCardGradeID =  @PreCardGradeID  
  select Top 1 @BusDate = Busdate from SODEOD where SOD = 1 and EOD = 0 order by Busdate desc 

      DECLARE CUR_UpdateGrade_SCHE_CARDGRADE CURSOR fast_forward local FOR
        select CardGradeID, CardGradeUpdMethod, CardGradeUpdThreshold, CardGradeRank,
             CardValidityDuration, CardValidityUnit 
          from cardGrade where CardTypeID = @CardTypeID and CardGradeRank > @PreCardGradeRank
         order by CardGradeRank
      OPEN CUR_UpdateGrade_SCHE_CARDGRADE
      FETCH FROM CUR_UpdateGrade_SCHE_CARDGRADE INTO @CardGradeID, @CardGradeUpdMethod, @CardGradeUpdThreshold, @CardGradeRank,
         @CardValidityDuration, @CardValidityUnit 
      WHILE @@FETCH_STATUS=0
      BEGIN 
	    if ((isnull(@CardGradeUpdMethod, 0) = 1 and @CumulativeConsumptionAmt >= @CardGradeUpdThreshold)
            or (isnull(@CardGradeUpdMethod, 0) = 2 and @CumulativeEarnPoints >= @CardGradeUpdThreshold)  
			or (isnull(@CardGradeUpdMethod, 0) = 3 and @TotalAmount >= @CardGradeUpdThreshold and @CardGradeUpdThreshold > 0)) 
		begin
		  set @NewCardGradeID = @CardGradeID
      -- ver 1.0.0.2
          /*赠送coupon=====================================================*/          
          DECLARE CUR_MemberBindCoupon CURSOR fast_forward local FOR
            select A.CardNumber, B.CouponTypeID, B.HoldCount from
            (
              select @CardGradeID as NextGradeID, * from Card                        
                where CardNumber = @CardNumber) A 
			left join (select * from CardGradeHoldCouponRule where RuleType = 1) B on A.NextGradeID = B.CardGradeID         
          OPEN CUR_MemberBindCoupon  
          FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          WHILE @@FETCH_STATUS=0  
          BEGIN
            set @CouponNumber = ''
            set @i = 1
            while @i <= @BindCouponCount
            begin
              exec GetCouponNumber @BindCouponTypeID, 1, @CouponNumber output            
              if isnull(@CouponNumber, '') <> ''
              begin
                select @CouponExpiryDate = CouponExpiryDate, @CouponAmount = CouponAmount from Coupon where CouponNumber = @CouponNumber
                
                exec GenApprovalCode @ApprovalCode output
                exec CalcCouponNewStatus @CouponNumber, @BindCouponTypeID, 32, @CouponStatus, @NewCouponStatus output
                exec CalcCouponNewExpiryDate @BindCouponTypeID, 32, @CouponExpiryDate, @NewCouponExpiryDate output     
                insert into Coupon_Movement
                    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
                     BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
                     OrgExpirydate, OrgStatus, NewStatus)    
                values(  
                      32, @CardNumber, @CouponNumber, @BindCouponTypeID, '', 0, '', @CouponAmount, 0, @CouponAmount,
                      @BusDate, @TxnDate, '','', @MemberID, @NewCouponExpiryDate, @ApprovalCode, null, '', '',
                      @CouponExpiryDate, 1, 2)
              end
              set @i = @i + 1    
            end                         
            FETCH FROM CUR_MemberBindCoupon INTO @CardNumber, @BindCouponTypeID, @BindCouponCount  
          END  
          CLOSE CUR_MemberBindCoupon   
          DEALLOCATE CUR_MemberBindCoupon 
          /*=====================================================*/  
        -------------------
		end 

        FETCH FROM CUR_UpdateGrade_SCHE_CARDGRADE INTO @CardGradeID, @CardGradeUpdMethod, @CardGradeUpdThreshold, @CardGradeRank,
           @CardValidityDuration, @CardValidityUnit 
      END
      CLOSE CUR_UpdateGrade_SCHE_CARDGRADE 
      DEALLOCATE CUR_UpdateGrade_SCHE_CARDGRADE  


  if @NewCardGradeID <> @PreCardGradeID
  begin
        -- 插入Card_Movement
        insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
           CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
        select 1001, CardNumber, '', 0, Convert(varchar(10), Getdate(), 120), TotalAmount, 0, TotalAmount, 0, @BusDate, @TxnDate, 
          null, null, '', '', 1, null, null, null,
          TotalPoints, TotalPoints, CardExpiryDate, CardExpiryDate, Status, Status
        from Card
          where CardNumber = @CardNumber
                   
        -- ver 1.0.0.3 先update member 再update card
        Update Member set UpdatedOn = GetDate() 
          where MemberID in (select MemberID from Card where CardNumber = @CardNumber)                   
                       
		  -- 更新Card
		--设置有效期 
		if @CardValidityUnit = 1
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 2
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(mm, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 3
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(ww, isnull(@CardValidityDuration, 0), Getdate()))))
		else if @CardValidityUnit = 4
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(dd, isnull(@CardValidityDuration, 0), Getdate()))))  
		else if @CardValidityUnit = 5    -- 不变更。
		  set @NewCardExpiryDate = 0
		else if isnull(@CardValidityUnit, 0) = 0  
		  set @NewCardExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, 100, Getdate()))))  
                             
        update Card set CardGradeID = @NewCardGradeID, CardExpiryDate = case when @NewCardExpiryDate = 0 then CardExpiryDate else @NewCardExpiryDate end
          where CardNumber = @CardNumber    
  end
end

GO
