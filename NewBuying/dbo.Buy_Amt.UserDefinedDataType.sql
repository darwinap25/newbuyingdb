USE [NewBuying]
GO
/****** Object:  UserDefinedDataType [dbo].[Buy_Amt]    Script Date: 12/13/2017 4:02:32 PM ******/
CREATE TYPE [dbo].[Buy_Amt] FROM [decimal](12, 4) NOT NULL
GO
