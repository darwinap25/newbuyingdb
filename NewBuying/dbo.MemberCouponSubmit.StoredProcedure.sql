USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCouponSubmit]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MemberCouponSubmit]
  @UserID		        varchar(10),			-- 登录用户
  @CardNumber			varchar(512),			-- 会员卡号
  @CouponNumber			varchar(512),			-- 使用的Coupon号码。
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),           -- 终端号码
  @ServerCode           varchar(512)='',		-- serverid 暂时用不到,预留
  @BrandCode			varchar(512),			-- 品牌编码
  @TxnNoSN		        varchar(512),			-- 交易单号(提交唯一序号)
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间
  @OprID		        int,					-- 操作类型：33、激活指定coupon。34、使用指定coupon。35、设置coupon无效（void）
  @Amount		        money,					-- 操作金额
  @VoidTxnNo            varchar(512),			-- 原单交易单号
  @SecurityCode         varchar(512),			-- 签名
  @TenderCode			varchar(512),			-- 使用的Tender code。（如果是银行卡做支付，）
  @Additional           varchar(512),			-- 附加信息
  @ApprovedCode         varchar(512),			-- 如果是银行卡做支付，此处记录批核号
  @ReturnAmount         money output,			-- 操作完成后Coupon金额余额
  @ReturnStatus         int output,				-- 操作完成后Coupon状态 
  @ReturnMessageStr		varchar(max) output,	-- 返回信息。（json格式）
  @NeedPassword         int=0,		            -- 是否验证密码。0：不需要。1：需要
  @Password             varchar(512)='',		-- Coupon密码
  @NeedCallConfirm      int=0                   -- 是否需要调用POSTxnConfirm。0：不需要。 1：需要。  默认0
AS
/****************************************************************************
**  Name : MemberCouponSubmit  
**  Version: 1.0.0.2
**  Description : 对于Coupon操作请求入口（主要kiosk调用）（33、激活指定coupon。34、使用指定coupon。35、设置coupon无效（void））
       
**  Parameter :
  declare @ReturnAmount money, @ReturnStatus int, @CardExpiryDate datetime, @Result int,	 @ReturnMessageStr varchar(max)    
  --exec @Result = MemberCouponSubmit '1', '1111002', '0001', 'store1', '001', '01', '201203260010002', '20120326002', '2012-03-26', '2012-08-22 12:12:12', 2, 100, 0, '', '', '', '', '', @ReturnAmount output, @ReturnPoint output, @CardExpiryDate output, @ReturnMessageStr output
  exec @Result = MemberCouponSubmit '1', '', '01000050', '', '', '', '', '', '', null, null, 35, 0, '', '', '', '', '',  @ReturnAmount output, @ReturnStatus output, @ReturnMessageStr output  
  print @Result    
  print @ReturnAmount
  print @ReturnStatus
  print @ReturnMessageStr
  
  select * from coupon
  select top 10 * from receivetxn order by keyid desc 
  select * from coupon_movement where OprID = 35
**  Created by: Gavin @2014-06-11
**  Modify by: Gavin @2015-03-04 (ver 1.0.0.1) 修正Bug，查询Coupon表时 缺少CouponNumber条件
**  Modify by: Darwin @2017-10-25 (ver 1.0.0.2) Added @prID = 58
****************************************************************************/
begin
  declare @CouponTypeID int, @CouponAmount money, @CouponCardNumber varchar(64), @CouponStatus int,
          @CouponExpirydate datetime, @CouponBrandID int, @CouponPassword varchar(512) 
  declare @MD5PWD varchar(512), @TenderID int, @Status int, @MemberID int
    		            
  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120) + cast(@OprID as varchar) + cast(cast(floor(@Amount) as int) as varchar)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate
  if @CheckResult <> 0
    return @CheckResult
  -- end
  
  set @Status = 0
  set @CardNumber = ISNULL(@CardNumber, '')
  set @CouponNumber = ISNULL(@CouponNumber, '') 
  
  -- 取busdate
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from SODEOD where EOD=0 and SOD=1 
  if isnull(@TxnDate, 0) = 0
    set @TxnDate = getdate()
  
  -- 取Coupon数据  
  select @CouponTypeID = C.CouponTypeID, @CouponAmount = CouponAmount, @CouponCardNumber = CardNumber, 
      @CouponStatus = C.Status, @CouponExpirydate = CouponExpirydate, @CouponBrandID = BrandID,
      @CouponPassword = CouponPassword
    from Coupon C left join CouponType T on C.CouponTypeID = T.CouponTypeID
    where C.CouponNumber = @CouponNumber  
  if @@ROWCOUNT = 0
    return -2
 
  if @CardNumber <> ''
    select @MemberID = Memberid from Card where CardNumber = @CardNumber
    
  if isnull(@TenderCode, '') <> ''
  begin
    select @TenderID = TenderID from tender where TenderCode = @TenderCode    
  end    

  if isnull(@NeedPassword, 0) = 1
  begin
    set @MD5PWD = dbo.EncryptMD5(@Password)
    if @MD5PWD <> @CouponPassword
      return -4
  end  
  

  -- 激活Coupon（销售Coupon）
  if @OprID = 33  
  begin
    if @CouponStatus not in (0, 1)
      set @Status = -20        
  end

  -- 使用Coupon
  if @OprID = 34   
  begin
    if @CouponStatus not in (2)
      set @Status = -20        
  end
  
  -- 作废 (删除)
  if @OprID = 35   
  begin
    if @CouponStatus >= 5
      set @Status = -20  
  end
  
  if @OprID = 58   
  BEGIN
    if @CouponStatus = 0 
      set @Status = -20  
  END
  
  if @Status <> 0
    return @Status
    

  insert into ReceiveTxn
      (BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber, OprID, Amount, Points, TenderID,
      [Status], CouponNumber, VoidKeyID, VoidTxnNo, Additional, ApproveStatus, Remark, SecurityCode, CreatedBy, UpdatedBy,Approvedby, ApprovalCode)
  values
      (@BrandCode, @StoreCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber, @OprID, @Amount, 0, @TenderID,
       @Status, @CouponNumber, null, @VoidTxnNo, @Additional, case isnull(@NeedCallConfirm,0) when 0 then 'A' else 'P' end, '', @SecurityCode, @UserID, @UserID, @UserID, @ApprovedCode) 

--如果有会员，则发消息。
if isnull(@MemberID, 0) > 0
begin
  declare @InputString varchar(max), @MessageServiceTypeID int, @AccountNumber nvarchar(512)
  set @InputString = ''
  set @ReturnMessageStr = ''
  
  DECLARE CUR_MemberCardSubmit CURSOR fast_forward FOR
    select MessageServiceTypeID, AccountNumber from MemberMessageAccount where MemberID = @MemberID and IsPrefer = 1  
  OPEN CUR_MemberCardSubmit
  FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber
  WHILE @@FETCH_STATUS=0
  BEGIN
	if isnull(@AccountNumber, '') <> '' 
	begin
	  if @InputString <> ''
	    set @InputString = @InputString + '|' 
 
      set @InputString = @InputString + 'OPRID=' + cast(@OprID as varchar) + ';AMOUNT=' + cast(@Amount as varchar)	
     	   + ';COUPONNUMBER=' + cast(@CouponNumber as varchar)	+ ';EXPDATE=' + convert(varchar(10), @CouponExpiryDate, 120)
     	   + ';TXNNO=' + @TxnNo + ';CARDNUMBER=' + cast(@CardNumber as varchar)	
     	   + ';MSGACCOUNT=' + @AccountNumber + ';MSGACCOUNTTYPE=' + cast(@MessageServiceTypeID as varchar)	   
	end
    FETCH FROM CUR_MemberCardSubmit INTO @MessageServiceTypeID, @AccountNumber   
  END
  CLOSE CUR_MemberCardSubmit 
  DEALLOCATE CUR_MemberCardSubmit        	
	  
  exec GenMessageJSONStr 'CouponSubmit', @InputString, @ReturnMessageStr  output
end
  
  return @Status
end


GO
