USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[Gen_BUY_RPRICE_M]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Gen_BUY_RPRICE_M] 
  @RPriceCode VARCHAR(64),
  @BusDate    DATETIME
AS
/****************************************************************************
**  Name : Gen_BUY_RPRICE_M  
**  Version: 1.0.0.0
**  Description : 产生货品价格主档数据	  (此过程中不做校验，调用前检查记录是否允许执行)
**  Parameter :
    
    exec Gen_BUY_RPRICE_M '', '2015-03-26'
    select * from buy_rprice_m
**  Created by: Gavin @2015-03-27
**
****************************************************************************/
begin
  DECLARE @Code VARCHAR(64), @NowDate DATETIME, @StoreCode VARCHAR(64), @StoreGroupCode VARCHAR(64)
  DECLARE @StartDate DATETIME, @EndDate DATETIME, @ApproveBy INT
  
  IF @BusDate IS NULL
    SET @NowDate = GETDATE()
  ELSE   
    SET @NowDate = @BusDate
  
  SET @RPriceCode = ISNULL(@RPriceCode, '')
  DECLARE CUR_GenBUYRPRICEM CURSOR fast_forward FOR
    SELECT RPriceCode, ISNULL(StoreCode,''), ISNULL(StoreGroupCode,''), StartDate, EndDate, ApproveBy  from buy_rprice_h 
      WHERE ApproveStatus = 'A' AND RPriceCode = @RPriceCode or (@RPriceCode = '') 
        AND DATEDIFF(DD, StartDate, @NowDate) >= 0 AND DATEDIFF(DD, @NowDate, EndDate) >= 0
      ORDER BY STARTDATE, CREATEDON
  OPEN CUR_GenBUYRPRICEM
  FETCH FROM CUR_GenBUYRPRICEM INTO @Code, @StoreCode, @StoreGroupCode, @StartDate, @EndDate, @ApproveBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    DELETE FROM buy_rprice_m WHERE ISNULL(StoreCode,'') = @StoreCode AND ISNULL(StoreGroupCode,'') = @StoreGroupCode 
    
    insert into buy_rprice_m (RPriceCode, ProdCode, RPriceTypeCode, RefPrice, Price, StoreCode, StoreGroupCode, StartDate, EndDate,
       Status, ApproveOn, ApproveBy, UpdatedOn, UpdatedBy, PromotionPrice, MemberPrice)
    select @Code, D.ProdCode, D.RPriceTypeCode, D.RefPrice, D.Price, @StoreCode, @StoreGroupCode, @StartDate, @EndDate,
       1, Getdate(), @ApproveBy, Getdate(), @ApproveBy, D.PromotionPrice, D.MemberPrice 
      from buy_rprice_d D     
     where D.RPriceCode = @Code 
    FETCH FROM CUR_GenBUYRPRICEM INTO @Code, @StoreCode, @StoreGroupCode, @StartDate, @EndDate, @ApproveBy
  END
  CLOSE CUR_GenBUYRPRICEM 
  DEALLOCATE CUR_GenBUYRPRICEM  
   
  return 0   
end

GO
