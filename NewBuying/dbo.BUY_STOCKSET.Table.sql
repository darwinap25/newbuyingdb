USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_STOCKSET]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_STOCKSET](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[StoreCode] [varchar](64) NOT NULL,
	[ReOrder] [dbo].[Buy_Qty] NULL,
	[MaxReOrder] [dbo].[Buy_Qty] NULL,
	[StockHolding] [dbo].[Buy_Qty] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_STOCKSET] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_STOCKSET] ADD  DEFAULT ((0)) FOR [ReOrder]
GO
ALTER TABLE [dbo].[BUY_STOCKSET] ADD  DEFAULT ((0)) FOR [MaxReOrder]
GO
ALTER TABLE [dbo].[BUY_STOCKSET] ADD  DEFAULT ((0)) FOR [StockHolding]
GO
ALTER TABLE [dbo].[BUY_STOCKSET] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_STOCKSET] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKSET', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKSET', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKSET', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重订货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKSET', @level2type=N'COLUMN',@level2name=N'ReOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大重订货数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKSET', @level2type=N'COLUMN',@level2name=N'MaxReOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'库存持有量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKSET', @level2type=N'COLUMN',@level2name=N'StockHolding'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品库存设置表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_STOCKSET'
GO
