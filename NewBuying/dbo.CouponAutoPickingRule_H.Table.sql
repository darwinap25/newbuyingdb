USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponAutoPickingRule_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponAutoPickingRule_H](
	[CouponAutoPickingRuleCode] [varchar](64) NOT NULL,
	[Description] [varchar](512) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL,
	[DayFlagID] [int] NULL,
	[MonthFlagID] [int] NULL,
	[WeekFlagID] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_COUPONAUTOPICKINGRULE_H] PRIMARY KEY CLUSTERED 
(
	[CouponAutoPickingRuleCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CouponAutoPickingRule_H] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[CouponAutoPickingRule_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CouponAutoPickingRule_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵订单自动提货规则编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'CouponAutoPickingRuleCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否生效。 0：无效，1：生效。 默认生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效天的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'DayFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效月的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'MonthFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效周的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H', @level2type=N'COLUMN',@level2name=N'WeekFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon订单（店铺给总部的）自动产生pickup单的规则设置表。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponAutoPickingRule_H'
GO
