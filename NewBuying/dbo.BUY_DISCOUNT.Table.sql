USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_DISCOUNT]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_DISCOUNT](
	[DiscountID] [int] IDENTITY(1,1) NOT NULL,
	[DiscountCode] [varchar](64) NOT NULL,
	[ReasonCode] [varchar](64) NOT NULL,
	[Description1] [nvarchar](512) NULL,
	[Description2] [nvarchar](512) NULL,
	[Description3] [nvarchar](512) NULL,
	[DiscType] [int] NULL,
	[SalesDiscLevel] [int] NULL,
	[DiscPrice] [dbo].[Buy_Amt] NULL,
	[AuthLevel] [int] NULL,
	[AllowDiscOnDisc] [int] NULL,
	[AllowChgDisc] [int] NULL,
	[TransDiscControl] [int] NULL,
	[Status] [int] NULL,
	[Note] [nvarchar](512) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_DISCOUNT] PRIMARY KEY CLUSTERED 
(
	[DiscountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT ((0)) FOR [DiscType]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT ((0)) FOR [SalesDiscLevel]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT ((0)) FOR [AuthLevel]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT ((0)) FOR [AllowDiscOnDisc]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT ((0)) FOR [AllowChgDisc]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT ((0)) FOR [TransDiscControl]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BUY_DISCOUNT] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'DiscountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'DiscountCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原因编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'ReasonCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'Description1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'Description2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'Description3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣类型。决定DiscPrice的含义。
0： item $ off;    
1： item % off
2： trans $ off;   
3： trans % off
4： price markdown （减价）

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'DiscType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣优先级。默认0.  0最低， 数字越大，优先级越高' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'SalesDiscLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣值。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'DiscPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'权限设置。0：没有权限要求。1：管理员权限。 2：经理权限。 默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'AuthLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许折上折。 0：不允许。1：允许。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'AllowDiscOnDisc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否允许变动折扣值。0：不允许。1：允许（此时DiscPrice为最大限制值）。默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'AllowChgDisc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'整单折扣控制.
0：不允许用于整单折扣，只允许单个货品折扣。
1：允许用于整单折扣，在每个货品的Netprice基础上计算。（货品的netprice可以包括单个折扣）
2：允许用于整单折扣，如果货品上已经有折扣，则跳过此货品记录分摊。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'TransDiscControl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态。0：无效。1：有效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣设置表. （手动折扣）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_DISCOUNT'
GO
