USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[Gen_BUY_PROMO_M]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Gen_BUY_PROMO_M] 
  @PromotionCode varchar(64)
AS
/****************************************************************************
**  Name : Gen_BUY_PROMO_M  
**  Version: 1.0.0.0
**  Description : 产生促销价格主档数据  (此过程中不做校验，调用前检查记录是否允许执行)
**  Parameter :

**  Created by: Gavin @2014-01-02
**
****************************************************************************/
begin
  insert into BUY_PROMO_M (PromotionCode, Description1, Description2, Description3, StoreCode, StoreGroupCode, StartDate, EndDate,
       StartTime, EndTime, EntityNum, EntityType, HitAmount, HitOP, DiscPrice, DiscType, DayFlagCode, WeekFlagCode, MonthFlagCode,
       LoyaltyFlag, LoyaltyCardTypeCode, LoyaltyCardGradeCode, BirthdayFlag, Status, Approveon, ApproveBy, UpdatedOn, UpdatedBy)
  select H.PromotionCode, H.Description1, H.Description2, H.Description3, H.StoreCode, H.StoreGroupCode, H.StartDate, H.EndDate,
       H.StartTime, H.EndTime, D.EntityNum, D.EntityType, D.HitAmount, D.HitOP, D.DiscPrice, D.DiscType, H.DayFlagCode, H.WeekFlagCode, H.MonthFlagCode,
       H.MemberPromotionFlag, H.CardTypeCode, H.CardGradeCode, H.BirthdayFlag, 1, Getdate(), H.ApproveBy, Getdate(), H.UpdatedBy
    from BUY_PROMO_D D left join BUY_PROMO_H H on H.PromotionCode = D.PromotionCode
   where H.PromotionCode = @PromotionCode
  return 0   
end

GO
