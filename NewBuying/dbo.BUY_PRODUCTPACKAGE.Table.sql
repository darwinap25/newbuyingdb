USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_PRODUCTPACKAGE]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_PRODUCTPACKAGE](
	[PackageSizeID] [int] IDENTITY(1,1) NOT NULL,
	[PackageSizeCode] [varchar](64) NOT NULL,
	[PackageSizeDesc1] [varchar](512) NULL,
	[PackageSizeDesc2] [varchar](512) NULL,
	[PackageSizeDesc3] [varchar](512) NULL,
	[PackageSizeType] [int] NULL,
	[PackageSizeUnit] [varchar](64) NULL,
	[PackageSizeQty] [int] NULL,
 CONSTRAINT [PK_BUY_PRODUCTPACKAGE] PRIMARY KEY CLUSTERED 
(
	[PackageSizeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装描述1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeDesc1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装描述2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeDesc2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装描述3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeDesc3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装货品类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装单位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeUnit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装单位数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE', @level2type=N'COLUMN',@level2name=N'PackageSizeQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货品包装设置。（销售单位）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_PRODUCTPACKAGE'
GO
