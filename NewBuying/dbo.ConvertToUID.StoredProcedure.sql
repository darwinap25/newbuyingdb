USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ConvertToUID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConvertToUID]
  @InputID varchar(20),
  @ResultUID varchar(20) output
AS
/****************************************************************************
**  Name : ConvertToUID
**  Version: 1.0.0.0
**  Description : 输入的可能是实体卡的十六进制UID，也可能是实体卡上印刷的加密ID。把这个ID或者UID，转换成UID(十六进制数的字符串形式)
**  Parameter :
**         @InputID varchar(20)		-- 十六进制UID, 或者实体卡上印刷的ID
**         return:  UID
**
**  Created by: Gavin @2011-03-18
**
****************************************************************************/
BEGIN
  declare @ss bigint, @TempUID varchar(20)
  set @ss = 72057594037927935   -- hardcode 的转换参数.
  set @TempUID = RTrim(LTrim(@InputID))
  set @ResultUID = ''
  
  -- UID换算（判断是UID（十六进制数），还是转换过的UID（实体卡印刷数））
  -- 判断是UID的条件为： 14位长度，其中包含字母。
  if len(@TempUID) > 14                                      -- 输入的是印刷的ID
  begin
    declare @TempID bigint
    set @TempID = convert(bigint, @TempUID)  
    select @ResultUID = dbo.ConvertHexToString(@ss - @TempID)   
    -- UID 是14位的，只截取右边14位
    if len(@ResultUID) > 14
      select @ResultUID = Right(@ResultUID, 14)
  end
  else
  begin                                                      -- 输入的是UID
    set @ResultUID = @InputID
  end  
  Return 0
END

GO
