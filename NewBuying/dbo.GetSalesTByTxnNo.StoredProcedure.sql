USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetSalesTByTxnNo]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[GetSalesTByTxnNo]
  @TxnNo              varchar(512)           -- 交易号
AS
/****************************************************************************
**  Name : GetSalesTByTxnNo
**  Version: 1.0.0.3
**  Description : 根据交易号获得此交易的sales_T 数据。
**
**  Parameter :
  exec GetSalesTByTxnNo 'KTXNNO000000317'
  select * from sales_T
  select * from tender
**  Created by: Gavin @2012-10-11
**  Modified by: Gavin @2012-10-22 (ver 1.0.0.1) 增加返回additional 字段
**  Modified by: Gavin @2017-01-04 (ver 1.0.0.2) 增加返回TenderType
**  Modified by: Gavin @2017-08-24 (ver 1.0.0.3) 为了kiosk使用，把TransNum 返回字段名字改为TxnNo
**
****************************************************************************/
begin
  select TransNum as TxnNo, SeqNo, T.TenderID, T.TenderCode, T.TenderDesc, T.TenderAmount, T.LocalAmount, T.ExchangeRate, T.Additional, 
    T.CreatedOn, T.CreatedBy, T.UpdatedOn, T.UpdatedBy, C.TenderType
  from sales_T T 
  left join Tender C on T.TenderCode = C.TenderCode
  where TransNum = @TxnNo 
  order by SeqNo  

  return 0
end  

GO
