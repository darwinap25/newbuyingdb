USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CheckStaffTokenString]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckStaffTokenString]
  @TokenStr             varchar(20),     -- 需要验证的Token
  @VerifyType           int,            -- 验证类型: 1：邮箱验证:  2：手机验证
  @VerifyTypeDesc       Varchar(100),    -- 验证类型描述
  @MemberID             int,             -- 验证的会员ID
  @StoreCode            varchar(64)     -- 店铺code
AS
/****************************************************************************
**  Name : CheckStaffTokenString    (for bauhaus)
**  Version: 1.0.0.3
**  Description : 验证token， 验证成功，则更新token状态为 2. 首先检查token是否已经过期。 
**  Example :
    declare @a int
    exec @a = CheckStaffTokenString 1, '3145492685'
	print @a
	select * from stafftoken
**  Created by: Gavin @2016-11-03
**  Modify by: Gavin @2016-11-08 (ver 1.0.0.1) 增加字段VerifyType, VerifyTypeDesc, UpdatedOn
**  Modify by: Gavin @2016-11-15 (ver 1.0.0.2) 增加字段MemberID,需要输入
**  Modify by: Gavin @2017-01-10 (ver 1.0.0.3) 增加输入@StoreCode， 取消 @StaffID， 不再需要校验staffid 
**
****************************************************************************/
begin
  declare @status int 
  
  update StaffToken set status = -1 
     where status = 1 and datediff(dd, ExpiryOn, getdate()) < 0 

  select @status = status from StaffToken where TokenStr = @TokenStr
  if @status is null       -- 没有找到token
    return -1
  else if @status = 2      -- token已经被使用了
    return -2
  else if @status = -1     -- token 已经生效（过期）
    return -3
  else if @status = 1      -- token有效。
  begin
    update StaffToken set status = 2, VerifyType = @VerifyType, VerifyTypeDesc = @VerifyTypeDesc, 
	    MemberID = @MemberID, UpdatedOn = Getdate(), StoreCode = @StoreCode
	  where status = 1  and TokenStr = @TokenStr
    return 0
  end  
end

GO
