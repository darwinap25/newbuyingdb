USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetDistricts]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetDistricts]
  @CityCode             varchar(64),       -- Code. 不填为全部
  @DistrictCode         varchar(64),       -- Code. 不填为全部
  @PageCurrent			int=1,			   -- 取第几页记录，默认1，小于等于0时也认为1
  @PageSize				int=0,			   -- 每页记录数， 为0时，不分页，默认0
  @PageCount			int=1 output,	   -- 返回总页数。
  @RecordCount			int=0 output,	   -- 返回总记录数。
  @LanguageAbbr			varchar(20)=''			   -- 1:取CardTypeName1,2：取CardTypeName2. 3：取CardTypeName3.  其他：取CardTypeName1
AS
/****************************************************************************
**  Name : GetDistricts  
**  Version: 1.0.0.0
**  Description : 获得City数据
**  Parameter :
  declare @count int, @recordcount int, @a int  
  exec @a = GetDistricts 'anqing', 'anqingxian', 1, 30, @count output, @recordcount output, 'zh_CN'
  print @a  
  print @count
  print @recordcount  
**  Created by: Gavin @2013-01-22
**
****************************************************************************/
begin
  declare @SQLStr nvarchar(4000), @Language int
      
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1

  set @SQLStr = 'select case ' + cast(@Language as varchar) + ' when 2 then DistrictName2 when 3 then DistrictName3 else DistrictName1 end as DistrictName, * from District where 1 = 1 '
  if isnull(@CityCode, '') <> ''
    set @SQLStr = @SQLStr + ' and CityCode = ''' + @CityCode + ''''       
  if isnull(@DistrictCode, '') <> ''
    set @SQLStr = @SQLStr + ' and DistrictCode = ''' + @DistrictCode + ''''
   
  exec SelectDataInBatchs @SQLStr, 'DistrictCode', @PageCurrent, @PageSize, @PageCount output, @RecordCount output, '', ''
  
  return 0
end

GO
