USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_BOXSALE]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_BOXSALE](
	[BOMID] [int] IDENTITY(1,1) NOT NULL,
	[BOMCode] [dbo].[Buy_PLU] NOT NULL,
	[ProdCode] [dbo].[Buy_PLU] NOT NULL,
	[Qty] [dbo].[Buy_Qty] NOT NULL,
	[MinQty] [int] NOT NULL DEFAULT ((0)),
	[MaxQty] [int] NOT NULL DEFAULT ((0)),
	[DefaultQty] [int] NOT NULL DEFAULT ((0)),
	[Price] [dbo].[Buy_Amt] NOT NULL DEFAULT ((0)),
	[PartValue] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[ValueType] [int] NULL DEFAULT ((0)),
	[ActPrice] [dbo].[Buy_Amt] NULL DEFAULT ((0)),
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[MutexTag] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_BUY_BOXSALE] PRIMARY KEY CLUSTERED 
(
	[BOMID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'BOMID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主货品编码（一般也是ProdCode）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'BOMCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子货品编码.（也可以是BOMCode，但不可循环嵌套）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'ProdCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子货品数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'Qty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子货品最小数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'MinQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子货品最大数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'MaxQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子货品默认数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'DefaultQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'附加价格。（子货品在BOM中一般价格都是0，货品的价格都集中在主货品上。有些子货品是可以更换的，可能会要增加加额外价格）。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'Price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'此子货品占BOM总价格的百分比或者价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'PartValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'决定ValueType内容是金额还是百分比。0：金额。1：百分比' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'ValueType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计算字段。根据BOM总价，PartValue，ValueType计算' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'ActPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'（取消此字段。暂不删除。） 互斥功能 通过创建BOM来实现。 将需要互斥的货品归入一个BOM中，此BOM货品设置为互斥属性 ）
互斥标签。  默认 0 。  0： 无效。此货品没有互斥性。  其他数字，表示有效。
同一个BOM中，相同 MutexTag值的 货品之间互斥，只能保留一种。
例如：  
一个BOM中 设置4 种货品， A，B，C，D，  A和B互斥，C和D互斥。 MutexTag设置为
A： 1
B： 1
C： 2
D： 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'MutexTag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'套餐销售设置表
@2015-10-09 主键 BOMID 改成 KeyID。
@2015-10-14 主键 KeyID改成 BOMID 。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_BOXSALE'
GO
