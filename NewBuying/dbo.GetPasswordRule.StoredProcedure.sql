USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetPasswordRule]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetPasswordRule]
  @PasswordRuleID        int,
  @LanguageAbbr          varchar(20)=''          
AS
/****************************************************************************
**  Name : GetPasswordRule
**  Version: 1.0.0.1
**  Description : 返回密码规则。
**
**  Parameter :
  exec GetPasswordRule 
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-08-30
**  Modify by: Gavin @2013-12-16 (ver 1.0.0.1) 如果输入的@PasswordRuleID= -1，则表示要求获取Member的 密码规则。只取一条记录。
**
****************************************************************************/
begin 
  declare @Language int
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
    
  if @PasswordRuleID = -1
  begin
    select top 1 PasswordRuleID, PasswordRuleCode, 
        case when @Language = 2 then Name2 when @Language = 3 then Name3 else Name1 end as PasswordRuleName,
        PWDMinLength, PWDMaxLength, PWDSecurityLevel, PWDStructure,
        PWDValidDays, PWDValidDaysUnit, ResetPWDDays, MemberPWDRule
     from PasswordRule where MemberPWDRule = 1
     order by PWDSecurityLevel desc 
  end else
  begin
    select PasswordRuleID, PasswordRuleCode, 
        case when @Language = 2 then Name2 when @Language = 3 then Name3 else Name1 end as PasswordRuleName,
        PWDMinLength, PWDMaxLength, PWDSecurityLevel, PWDStructure,
        PWDValidDays, PWDValidDaysUnit, ResetPWDDays, MemberPWDRule
     from PasswordRule where PasswordRuleID = @PasswordRuleID  --  or isnull(@PasswordRuleID, 0) = 0
  end
  return 0 
end

GO
