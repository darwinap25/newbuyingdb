USE [NewBuying]
GO
/****** Object:  View [dbo].[ViewColor_IsOnlineSKU]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewColor_IsOnlineSKU]
AS
/*
*/
	select ColorID, RGB AS ColorCode, ColorName1, ColorName2, ColorName3, ColorPicFile from Color 
	where ColorCode in	(select distinct ColorCode from product where IsOnlineSKU = 1)

GO
