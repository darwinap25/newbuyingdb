USE [NewBuying]
GO
/****** Object:  Table [dbo].[BUY_Promotion_Gift]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BUY_Promotion_Gift](
	[PromotionCode] [varchar](64) NOT NULL,
	[GiftSeq] [int] NOT NULL,
	[GiftLogicalOpr] [int] NULL DEFAULT ((0)),
	[PromotionGiftType] [int] NULL DEFAULT ((1)),
	[PromotionDiscountOn] [int] NULL DEFAULT ((0)),
	[PromotionDiscountType] [int] NULL DEFAULT ((0)),
	[PromotionValue] [dbo].[Buy_Amt] NULL CONSTRAINT [DF_BUY_MNM_H_Amount]  DEFAULT ((0)),
	[PromotionGiftQty] [int] NULL DEFAULT ((0)),
	[PromotionGiftItem] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_BUY_PROMOTION_GIFT] PRIMARY KEY CLUSTERED 
(
	[PromotionCode] ASC,
	[GiftSeq] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'GiftSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销结果(Gift)之间的逻辑关系。默认0
 0：and   1：or' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'GiftLogicalOpr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销实现方式.. 默认1
1：现金返还（可以设置现金或者折扣百分比）（需要进下一步的设置）
2：积分返还 
3：实物赠送。（即Hit命中后，添加某些货品免费。需要相应的货品设置表）
（注：选择1，设置百分之百折扣，或者现金为0，表示指定货品免费，只是此货品要求手动输入
选择3，此货品自动添加到销售单中，并且价格为0）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionGiftType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣金额实现的方式。默认0
0：在命中货品上实现折扣或现金抵扣 （此选项不需要设置gift货品）
1：在Gift货品上实现折扣或现金抵扣   
2：全单实现折扣或现金抵扣  （此选项不需要设置gift货品）
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionDiscountOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折扣金额设定类型。默认0
0：Sales at （金额）
1：Sales at （折扣）  
2：Sales off（金额）
3：Sales off（折扣）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionDiscountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销内容值。（金额或百分比）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销货品数量。默认0。 0表示没有Gift货品。  数字表示Gift货品不能超过。
如果有多个促销品，则需要多条记录，使用GiftLogicalOpr来表示之间的关系
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionGiftQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销货品条件：默认0
0：没有具体的货品条件，任何货品
1：Promotion_Gift_PLU中的任意货品合计。
2：Promotion_Gift_PLU中任意一个单独货品合计。
5:   Promotion_Gift_PLU中 货品都不包含在内。 （即除了这些货品，都可以。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift', @level2type=N'COLUMN',@level2name=N'PromotionGiftItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'促销礼品表。
@2017-03-07 ： PromotionGiftItem多增加一种情况
5:   Promotion_Gift_PLU中 货品都不包含在内。 （即除了这些货品，都可以。）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BUY_Promotion_Gift'
GO
