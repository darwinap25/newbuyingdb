USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[ImportCouponUID]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[ImportCouponUID]
  @ImportCouponNumber varchar(64),   -- coupon导入单的单号
  @NeedActive int,                   -- 是否需要立即激活（0：不需要，1：需要）
  @NeedNewBatch int,                 -- 是否需要建立新批次。（0）
  @ApproveStatus char(1), 
  @CreatedBy int
AS
/****************************************************************************
**  Name : ImportCouponUID
**  Version: 1.0.0.5
**  Description : ImportCouponUID触发器中调用，用于执行写入UID和 coupon表
**
**  Parameter :
  declare @MemberID varchar(36), @count int, @recordcount int, @a int  
  exec @a = ImportCouponUID 0, ''
  print @a  
**  Created by: Gavin @2012-08-17
**  Modify by: Gavin @2012-08-22 (ver 1.0.0.1) Import UID时,产生的coupon 只能是 Dormant状态, 不做自动激活,自动issuer操作
**  Modify by: Gavin @2012-08-22 (ver 1.0.0.2) 按照coupontypeid 和 batchid 来 group, 用batchid 填写到 batchcouponcode中.
											(注:导入的数据中应保证导入的batchid唯一)
**  Modify by: Gavin @2012-08-28 (ver 1.0.0.3) 修正导入的数据中有多个batchid的bug	
											因UIDToCouponNumber, CouponNumberToUID选项修改，需要修改相应存储过程。Card 部分同样修改。
**  Modify by: Gavin @2012-11-13 (ver 1.0.0.4) 修改CalcCheckDigit_EAN13 为CalcCheckDigit。
**  Modify by: Gavin @2014-10-16 (ver 1.0.0.5) 创建时stockstatus 设置为2 																					
**
****************************************************************************/
begin
  declare @CouponTypeID int, @CouponCount int, @OprID int, @BatchCouponID int
  declare @KeyID int, @CouponUID varchar(512), @ExpiryDate datetime, @CouponExpiryDate datetime, @CardExpiryDate datetime
  declare @OldApproveStatus char(1), @CardNumber varchar(512), @CouponNumber varchar(512), @CouponValidityDuration int, 
		@CouponValidityUnit int, @CouponSpecifyExpiryDate datetime, @IsConsecutiveUID int, @UIDToCouponNumber int, 
		@CouponTypeAmount money, @UIDCheckDigit int
  declare @A int, @ReturnBatchID varchar(30), @ReturnStartNumber nvarchar(30), @ReturnEndNumber nvarchar(30), @IssuedDate datetime
  declare @ApprovalCode char(6), @CouponBatchID varchar(512), @BatchID varchar(512), @Denomination money, @CouponAmount money
  declare @IsImportCouponNumber int, @NewCouponNumber varchar(512), @i int, @TranCount int, @NewCouponStatus int
  declare @BusDate date --Business Date
  declare @TxnDate datetime --Transaction Date
  declare @Status int --Coupon Status
  
  --设置Busdate
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  if @BusDate is null
    set @BusDate=convert(varchar(10), getdate(), 120)
  set @TxnDate=getdate()
  set @IssuedDate =convert(varchar(10), getdate(), 120)    
  
  --设置是否激活  ((ver 1.0.0.1) Import UID时,产生的coupon 只能是 Dormant状态, 不做自动激活,自动issuer操作)
  set @Status = 0
--  if isnull(@NeedActive, 0) = 1 
--    set @Status=2   -- active
--  else
--    set @Status=1   -- issued

  --获得Approvalcode  
  exec GenApprovalCode @ApprovalCode output 
  
  DECLARE CUR_Ord_ImportCouponUID_D_Group CURSOR fast_forward local FOR
    SELECT CouponTypeID, count(KeyID) as CouponCount, BatchID FROM Ord_ImportCouponUID_D where ImportCouponNumber = @ImportCouponNumber
       Group By CouponTypeID, BatchID 
     order by CouponTypeID, BatchID 
  OPEN CUR_Ord_ImportCouponUID_D_Group
  FETCH FROM CUR_Ord_ImportCouponUID_D_Group INTO @CouponTypeID, @CouponCount, @BatchID
  WHILE @@FETCH_STATUS=0
  BEGIN
    set @BatchCouponID = 0

    select @CouponValidityDuration = CouponValidityDuration, @CouponValidityUnit = CouponValidityUnit, @IsImportCouponNumber = IsImportCouponNumber,
		@UIDToCouponNumber = UIDToCouponNumber, @CouponSpecifyExpiryDate = CouponSpecifyExpiryDate, @CouponTypeAmount = CouponTypeAmount,
		@UIDCheckDigit = UIDCheckDigit
      from CouponType where CouponTypeID = @CouponTypeID    
    --设置有效期 ( 5:取coupon自己的expirydate,不做改变。7：需要另外存储过程实现，暂时不做)
    if @CouponValidityUnit = 1
      set @CouponExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, isnull(@CouponValidityDuration, 0), @IssuedDate))))
    else if @CouponValidityUnit = 2
      set @CouponExpiryDate = convert(datetime, floor(convert(float, DateAdd(mm, isnull(@CouponValidityDuration, 0), @IssuedDate))))
    else if @CouponValidityUnit = 3
      set @CouponExpiryDate = convert(datetime, floor(convert(float, DateAdd(ww, isnull(@CouponValidityDuration, 0), @IssuedDate))))
    else if @CouponValidityUnit = 4
      set @CouponExpiryDate = convert(datetime, floor(convert(float, DateAdd(dd, isnull(@CouponValidityDuration, 0), @IssuedDate))))
    else if @CouponValidityUnit = 6
      set @CouponExpiryDate = @CouponSpecifyExpiryDate     
    else if isnull(@CouponValidityUnit, 0) = 0  
      set @CouponExpiryDate = convert(datetime, floor(convert(float, DateAdd(yy, 100, @IssuedDate))))  
    
    --产生Coupon记录
    --if (isnull(@IsImportCouponNumber, 0) = 0) or (@UIDToCouponNumber = 0)     -- 仅根据SVA的设置来产生Coupon，CouponNumber和CouponUID在Map表中逐个关联
    if (@UIDToCouponNumber = 0)
    begin
      if isnull(@NeedNewBatch, 0) = 1        -- 要求新建一批Coupon来绑定
      begin
        exec @A = BatchGenerateNumber @CreatedBy, 1, @CouponTypeID, 0,0, @CouponCount, @IssuedDate, @CouponTypeAmount, 0, 0, '', 
		   @CouponExpiryDate, @ImportCouponNumber, @ApprovalCode, @CreatedBy, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, @BatchID, 0, 2
        if @A = 0
          set @BatchCouponID = @ReturnBatchID               
      end    
      -- 绑定Coupon
      insert into CouponUIDMap(CouponUID, ImportCouponNumber, BatchCouponID, CouponTypeID, CouponNumber, Status, CreatedOn, CreatedBy)          
      select U.CouponUID, U.ImportCouponNumber, C.BatchCouponID, C.CouponTypeID, C.CouponNumber, 1, @TxnDate, @CreatedBy
        from (SELECT ROW_NUMBER() over (order by KeyID) as SeqNo, ImportCouponNumber, KeyID, CouponUID, ExpiryDate, BatchID, Denomination FROM Ord_ImportCouponUID_D 
                where ImportCouponNumber = @ImportCouponNumber and CouponTypeID = @CouponTypeID and BatchID = @BatchID) U 
            left join (select ROW_NUMBER() over (order by CouponNumber) as SeqNo, BatchCouponID, CouponTypeID,CouponNumber from coupon where CouponTypeID = @CouponTypeID and Status = 0 and (BatchCouponID = isnull(@BatchCouponID,0) or @NeedNewBatch = 0)) C 
            on U.SeqNo = C.SeqNo
       where U.KeyID is not null and C.CouponNumber is not null
      --如果有调整面值，有效期，coupon状态的，则插入coupon_movement               
      insert into Coupon_Movement
			(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
			BusDate, Txndate, Remark, SecurityCode, CreatedBy, ApprovalCode,OrgExpiryDate,NewExpiryDate, OrgStatus, NewStatus)			
      select 39, '', M.CouponNumber, D.CouponTypeID,'',0, D.ImportCouponNumber, 0, isnull(D.Denomination, C.CouponAmount), isnull(D.Denomination, C.CouponAmount),
	     @BusDate, @TxnDate, '','', @CreatedBy,@ApprovalCode, C.CouponExpiryDate,isnull(D.ExpiryDate,@CouponExpiryDate), 0, @Status
             from Ord_ImportCouponUID_D D left join CouponUIDMap M on D.CouponUID = M.CouponUID
               left join Coupon C on M.CouponNumber = C.CouponNumber
          where D.ImportCouponNumber = @ImportCouponNumber and D.CouponTypeID = @CouponTypeID and BatchID = @BatchID
    end else            -- 根据导入的UID来产生CouponNumber
    begin  
       if isnull(@BatchID, '') = ''
          exec GetRefNoString 'BTHCOU', @CouponBatchID output   
       else set @CouponBatchID = @BatchID 
             
          insert into BatchCoupon
             (BatchCouponCode, SeqFrom, SeqTo, Qty, CouponTypeID, CreatedOn, UpdatedOn, CreatedBy, UpdatedBy)
          values (@CouponBatchID, 0, 0, 0, @CouponTypeID, @TxnDate, @TxnDate, @CreatedBy, @CreatedBy)             
          --set @NewBatchID = @@IDENTITY    
          set @BatchCouponID = SCOPE_IDENTITY()

          insert into Coupon
             (CouponNumber, CouponTypeID, CouponIssueDate, CouponExpiryDate, StoreID, Status, BatchCouponID, 
               CouponPassword, CardNumber, CouponAmount, Createdby, Updatedby, StockStatus)      
          select
              case when @UIDToCouponNumber = 1 then CouponUID
                when @UIDToCouponNumber = 2 then  substring(CouponUID, 1, len(RTrim(LTrim(CouponUID))) - 1)
                when @UIDToCouponNumber = 3 then  dbo.CalcCheckDigit(CouponUID, isnull(M.CheckDigitModeCode, 'EAN13'))
              end,  
               D.CouponTypeID, @IssuedDate, isnull(ExpiryDate,@CouponExpiryDate), null, @Status, @BatchCouponID, 
               '', '', isnull(Denomination,T.CouponTypeAmount),@Createdby,@Createdby, 2
             from Ord_ImportCouponUID_D D left join CouponType T on D.CouponTypeID = T.CouponTypeID
             left join CheckDigitMode M on T.CheckDigitModeID = M.CheckDigitModeID
             where ImportCouponNumber = @ImportCouponNumber and D.CouponTypeID = @CouponTypeID and BatchID = @BatchID
             
        --插入Coupon_Movement,OprID=0新增 Robin 2012-06-28
        insert into Coupon_Movement
			(CouponNumber, OprID, CardNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
			BusDate, Txndate, Remark, SecurityCode, CreatedBy, ApprovalCode,OrgExpiryDate,NewExpiryDate, OrgStatus, NewStatus)			
        select
              case when @UIDToCouponNumber = 1 then CouponUID
                when @UIDToCouponNumber = 2 then  substring(CouponUID, 1, len(RTrim(LTrim(CouponUID))) - 1)
                when @UIDToCouponNumber = 3 then  dbo.CalcCheckDigit(CouponUID, isnull(M.CheckDigitModeCode, 'EAN13'))
              end,        
              0, '', D.CouponTypeID,'',0,@ImportCouponNumber,0, isnull(Denomination,T.CouponTypeAmount),isnull(Denomination,T.CouponTypeAmount),
	          @BusDate, @TxnDate, '','', @CreatedBy,@ApprovalCode, null,isnull(ExpiryDate,@CouponExpiryDate), null, @status
          from Ord_ImportCouponUID_D D left join CouponType T on D.CouponTypeID = T.CouponTypeID
           left join CheckDigitMode M on T.CheckDigitModeID = M.CheckDigitModeID
        where ImportCouponNumber = @ImportCouponNumber and D.CouponTypeID = @CouponTypeID and BatchID = @BatchID		  
             
        insert into CouponUIDMap(CouponNumber, CouponUID, ImportCouponNumber, BatchCouponID, CouponTypeID, Status, CreatedOn, CreatedBy)
        select 
              case when @UIDToCouponNumber = 1 then CouponUID
                when @UIDToCouponNumber = 2 then  substring(CouponUID, 1, len(RTrim(LTrim(CouponUID))) - 1)
                when @UIDToCouponNumber = 3 then  dbo.CalcCheckDigit(CouponUID, isnull(M.CheckDigitModeCode, 'EAN13'))
              end,        
             CouponUID,@ImportCouponNumber, @BatchCouponID,D.CouponTypeID, 1, @TxnDate,@CreatedBy
             from Ord_ImportCouponUID_D D left join CouponType T on D.CouponTypeID = T.CouponTypeID
               left join CheckDigitMode M on T.CheckDigitModeID = M.CheckDigitModeID
             where ImportCouponNumber = @ImportCouponNumber and D.CouponTypeID = @CouponTypeID and BatchID = @BatchID             
    end
    FETCH FROM CUR_Ord_ImportCouponUID_D_Group INTO @CouponTypeID, @CouponCount, @BatchID
  END
  CLOSE CUR_Ord_ImportCouponUID_D_Group 
  DEALLOCATE CUR_Ord_ImportCouponUID_D_Group              
 
  update Ord_ImportCouponUID_H set ApprovalCode = @ApprovalCode where ImportCouponNumber = @ImportCouponNumber 
end

GO
