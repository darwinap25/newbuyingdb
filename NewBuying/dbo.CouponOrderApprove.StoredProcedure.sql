USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CouponOrderApprove]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[CouponOrderApprove]
  @UserID              int,            -- 批核人员
  @OrderType           int,            --订单类型。 1： Ord_CouponReceive_H（供应商订单的收货） 2:Ord_CouponReturn_H (店铺订单和退单的收货)
  @OrderNumber         varchar(64)     --单号
AS
/****************************************************************************
**  Name : CouponOrderApprove
**  Version: 1.0.0.4
**  Description : 批核实体Coupon的订单。目前不通过Coupon_movement，直接修改Coupon
**  example :
select * from Ord_CouponReceive_H 
  update ord_couponreceive_H set ApproveStatus = 'A' where CouponReceiveNumber = 'CREC00000000003' 
**  Created by: Gavin @2014-05-20
**  Modified by: Gavin @2014-06-09 （ver 1.0.0.1) Ord_CouponReceive_H增加Receivetype。
**  Modified by: Gavin @2014-06-21 （ver 1.0.0.2) 供应商订单的收货单批核时，如果有坏货，自动把这些坏货再产生一个收货单，等待批核 
**  Modified by: Gavin @2014-07-28  (ver 1.0.0.3) 使用Detail中的CouponTypeID,一个订单允许多个CouponType
**  Modified by: Gavin @2014-09-16  (ver 1.0.0.4) 如果批核时一次没收完,剩下的coupon产生新的订单,则把原单中未收到的货品直接删除.
**
****************************************************************************/
begin
  declare  @FirstNumber varchar(64), @EndNumber varchar(64), @Qty int, @CouponTypeID int, @StoreID int
  declare @ReceiveType int, @StockStatus int, @CouponReceiveNumber varchar(64), @ReferenceNo varchar(64)
  declare @NewReceiveNumber varchar(64), @CouponStockStatus int, @CouponNoPickQty int, @CouponGoodQty int, @CouponBadQty int
  
  if @OrderType = 1      -- 收货单批核 
  begin
    select @ReceiveType = ReceiveType, @ReferenceNo = ReferenceNo, @StoreID = StoreID 
      from Ord_CouponReceive_H where CouponReceiveNumber = @OrderNumber
    
    if @ReceiveType = 1   -- 来自供应商的订单收货
    begin                   
      -- 如果有未定货(CouponStockStatus = 0)，则重新并且是供应商订单，则重新产生收货单。
      if exists(select * from Ord_CouponReceive_D where CouponReceiveNumber = @OrderNumber and CouponStockStatus = 1)
      begin
        if exists(select * from Ord_OrderToSupplier_H where OrderSupplierNumber = @ReferenceNo)
        begin
          exec GetRefNoString 'CREC', @NewReceiveNumber output
          insert into Ord_CouponReceive_H(CouponReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
               SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
               StoreEmail, StoreMobile, ApproveStatus, Remark,   
               CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponTypeID, CouponQty, ReceiveType, ordertype)
          select @NewReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
              SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
              StoreEmail, StoreMobile, 'P', Remark,   
              CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, @CouponNoPickQty, CouponQty, 1, ordertype
            from Ord_CouponReceive_H where CouponReceiveNumber = @OrderNumber
  
          insert into Ord_CouponReceive_D (CouponReceiveNumber, CouponTypeID, Description, OrderQty, ActualQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode, CouponStockStatus, ReceiveDateTime)   
          select @NewReceiveNumber, CouponTypeID, '', OrderQty, OrderQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode, 1, GETDATE()
          from Ord_CouponReceive_D where  CouponReceiveNumber = @OrderNumber and CouponStockStatus = 1
          
          -- ver 1.0.0.4
          delete from Ord_CouponReceive_D where  CouponReceiveNumber = @OrderNumber and CouponStockStatus = 1
          
        --  exec UpdateCouponOnhand_CouponOrder 70, null, @StoreID, @CouponTypeID, @CouponNoPickQty
        end
      end
      ----------------------------------                    
    end         
  end else if @OrderType = 2
  begin
    exec GetRefNoString 'CREC', @CouponReceiveNumber output
    select @StoreID = FromStoreID from Ord_CouponReturn_H where CouponReturnNumber = @OrderNumber
    select @CouponGoodQty = count(*) from Ord_CouponReturn_D where CouponReturnNumber = @OrderNumber
     
    select top 1 @CouponTypeID = CouponTypeID From Ord_CouponReturn_D  where CouponReturnNumber = @OrderNumber 
    insert into Ord_CouponReceive_H(CouponReceiveNumber, ReferenceNo, StoreID, SupplierID, StorerAddress, SupplierAddress, 
          SuppliertContactName, SupplierPhone, SupplierEmail, SupplierMobile, StoreContactName, StorePhone,
          StoreEmail, StoreMobile, ApproveStatus, Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponTypeID, CouponQty, ReceiveType, ordertype)
      select @CouponReceiveNumber, CouponReturnNumber, StoreID, FromStoreID, SendAddress, FromAddress, 
          FromContactName, FromContactNumber, FromEmail, FromMobile, StoreContactName, StoreContactPhone,
          StoreContactEmail, StoreMobile, 'P', Remark,   
          CreatedBusDate, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, @CouponTypeID, 0, 2, 0
     from Ord_CouponReturn_H where CouponReturnNumber = @OrderNumber
  
    insert into Ord_CouponReceive_D (CouponReceiveNumber, CouponTypeID, Description, OrderQty, ActualQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode, CouponStockStatus, ReceiveDateTime)   
    select @CouponReceiveNumber, CouponTypeID, '', OrderQty, OrderQty, FirstCouponNumber, EndCouponNumber, BatchCouponCode, 2, GETDATE()
     from Ord_CouponReturn_D where CouponReturnNumber = @OrderNumber
           
    -- 更改Onhand 表 (库存)
    -- exec UpdateCouponOnhand_CouponOrder 75, @StoreID, null, @CouponTypeID, @CouponGoodQty               
    ------------------------
  end    
  
end

GO
