USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSTxnConfirmBySN]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[POSTxnConfirmBySN]      
  @UserID             varchar(512),   --用户ＩＤ      
  @TxnNoSN            varchar(512),   --交易号（序号）      
  @CouponList         varchar(max),     --Coupon清单 (不使用)   
  @ApprovalCode    varchar(512) output --返回的approvalCode      
as      
/****************************************************************************      
**  Name : POSTxnConfirmBySN      
**  Version: 1.0.0.3     
**  Description : POS根据提交的TXNSN来confirm指定的receivetxn记录。（逐条提交，单个Coupon, 无整单校验。RRG目前采用）      
**      
**  Parameter :  
  
declare @A int, @UserID varchar(512), @TxnNo varchar(512), @ApprovalCode varchar(512)      
set @UserID = '1'      
set @TxnNo = '001201503131249330002003'      
exec @A = POSTxnConfirm @UserID, @TxnNo,'', @ApprovalCode output      
print @A      
print @ApprovalCode      
  select * from coupon where status = 1  
  select * from receivetxn  
**  Created by: Gavin @2014-05-26      
**  Modify by: Gavin @2014-06-05 （ver 1.0.0.1）状态不正确时,返回细分的errorcode    
**  Modify By: Gavin @2014-07-17 (ver 1.1.0.2) 加上OprID=56的处理  
**  Modify By: Gavin @2014-09-01 (ver 1.1.0.3)  允许批量Coupon的confirm  
****************************************************************************/      
begin      
  declare @result int, @denomination money      
  declare @CouponNumber varchar(64), @RefTxnNo varchar(512), @BusDate datetime,       
          @TxnDate datetime, @StoreID int, @StoreCode varchar(64), @BrandCode varchar(64),      
          @ServerCode varchar(512), @RegisterCode varchar(512), @CouponStatus int, @OprID int,  
          @CouponNumPatternLen int, @ApprovalcodeCount int  
  declare @KeyID varchar(30), @ShopCode varchar(10), @TxnNo varchar(64), @CardNumber nvarchar(30), @CouponCardNumber nvarchar(30),       
          @CardTypeID int, @Amount money, @Status int, @VoidKeyID varchar(30), @VoidTxnNo varchar(30), @Additional varchar(200), @Points int,      
          @Remark varchar(200), @SecurityCode varchar(300), @ApproveStatus char(1), @ApprovedBy varchar(10), @TenderID int      
  declare @TotalAmount money, @OpenBal money, @CloseBal money, @BatchID varchar(60),       
    @UID varchar(60), @RefKeyID int, @VoidMovementKeyID int      
  declare @CouponCount int, @NewCouponStatus int      
  declare @CouponNumberEnd Varchar(512),@CouponLength int, @ActAmount money      
  declare @CouponExpiryDate datetime, @CouponTypeID int, @NewCouponExpiryDate datetime      
  declare @BrandID int      
  declare @CouponOpenBal money, @CouponCloseBal money    
        
  declare @T Table(OprID int, [Status] int, CreatedBy varchar(512), CouponNumber varchar(512), TxnNo varchar(512), Busdate DateTime, TxnDate DateTime,       
         ApprovalCode varchar(512), StoreCode varchar(512), BrandCode varchar(512), ServerCode varchar(512), RegisterCode varchar(512), CouponCount int)      
  DECLARE @idoc int       
  Declare @T_CouponList Table(COUPONNUMBER varchar(64),Qty int, OprID int)   
  declare @ActiveReceiveRecordCount int   
 
  select Top 1 @ApprovalCode = ApprovalCode, @CardNumber=CardNumber, @CouponNumber = CouponNumber,   
       @OprID = OprID, @Amount = Amount, @CouponCount = CouponCount,  
       @KeyID = KeyID, @BrandCode = BrandCode, @ShopCode = StoreCode, @ServerCode = ServerCode,   
       @RegisterCode = RegisterCode, @TxnNo = TxnNo, @BusDate = BusDate, @TxnDate = TxnDate   
     from ReceiveTxn where TxnNoSN = @TxnNoSN 
    
  if isnull(@ApprovalCode, '') <> ''    
  begin    
    return 0    
  end  
    
  set @result=-1   
  set @ActiveReceiveRecordCount = 0  
  select @BrandID=StoreBrandID from Brand where StoreBrandCode=@BrandCode      
  select @StoreID=StoreID from Store where BrandID=@BrandID and StoreCode=@ShopCode       
    
  if @OprID < 31   -- Card 的操作  
  begin  
    exec GenApprovalCode @ApprovalCode output      
    Update ReceiveTxn set ApproveStatus = 'A', ApprovedBy = @UserID, ApprovedDate = Getdate(),ApprovalCode=@ApprovalCode       
      where TxnNoSN = @TxnNoSN    
    if @@ROWCOUNT > 0        
      set @result = 0          
    else        
      set @result = -1     
          
  end    
  else     -- Coupon 的操作  
  begin  
    
  --再次检查Coupon状态-----------------------      
  select @CouponStatus = Status, @CardNumber = CardNumber, @CouponOpenBal = isNull(CouponAmount,0),   
       @CouponExpiryDate = CouponExpiryDate, @CouponTypeID = CouponTypeID  
     from Coupon where CouponNumber = @CouponNumber  
  if (@OprID = 53) and (@CouponStatus not in (0,1))  
  begin  
    set @result = -20  
    exec ConvertReturnStatus @result, @CouponStatus, @result output  
    return @result  
  end      
  if (@OprID = 54) and (@CouponStatus <> 2)  
  begin  
    set @result = -20  
    exec ConvertReturnStatus @result, @CouponStatus, @result output  
    return @result  
  end    
   
      
  Begin Tran      
          
    exec GenApprovalCode @ApprovalCode output      
    Update ReceiveTxn set ApproveStatus = 'A', ApprovedBy = @UserID, ApprovedDate = Getdate(),ApprovalCode=@ApprovalCode       
      where TxnNoSN = @TxnNoSN    
    if @@ROWCOUNT > 0        
      set @result = 0          
    else        
      set @result = -1      
  
  
   if @OprID in (53)       -- 激活coupon， 使用输入的金额，作为coupon最终的实际金额      
   begin      
     set @CouponCloseBal=@Amount      
     set @ActAmount=@Amount-@CouponOpenBal      
   end      
   else if @OprID in (54)              
   begin      
     set @ActAmount = @Amount      
     set @CouponCloseBal = @CouponOpenBal + @Amount         
   end      
   else if @OprID in (56)              
   begin      
     set @ActAmount = @Amount      
     set @CouponCloseBal = @CouponOpenBal - @Amount          
   end     
  /********执行插入Coupon_Movement**************/        
       
  declare @OldSTATUS char(1), @CardTotalAmt money , @Card_CardType varchar(40), @CardStatus int, @CardTotalPoints int      
  DECLARE CUR_RECTXN CURSOR fast_forward local for    
    select max(KeyID), max(OprID), max(BrandCode), max(StoreCode), max(ServerCode), max(RegisterCode), TxnNoSN, max(TxnNo), max(BusDate), max(TxnDate), max(CardNumber),    
         CouponNumber, sum(Amount), sum(CouponCount),sum(Points),      
         max(VoidKeyID), max(VoidTxnNo), max(Additional), max(Remark), max(SecurityCode), max(ApproveStatus), max(ApprovedBy), max(ApprovalCode), max(TenderID)    
    from (      
      SELECT KeyID, OprID, BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber,CouponNumber, isnull(Amount,0) as Amount, isnull(CouponCount,0) as CouponCount,isnull(Points,0) as Points,      
         VoidKeyID, VoidTxnNo, Additional, Remark, SecurityCode, ApproveStatus, ApprovedBy, ApprovalCode, TenderID      
       FROM receivetxn where TxnNoSN = @TxnNoSN           
     ) A group by TxnNoSN, CouponNumber     
  OPEN CUR_RECTXN      
  FETCH FROM CUR_RECTXN INTO @KeyID, @OprID, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber,@CouponNumber, @Amount,       
  @CouponCount, @Points, @VoidKeyID, @VoidTxnNo, @Additional, @Remark, @SecurityCode, @ApproveStatus, @ApprovedBy,@ApprovalCode, @TenderID      
  WHILE @@FETCH_STATUS=0      
  BEGIN       
   -- ver 1.1.0.6 去除couponlist校验  
   if (@CouponCount > 0) --and (exists(select M.CouponNumber from @T_CouponList T left join CouponUIDMap M on T.CouponNumber = M.CouponUID where M.CouponNumber=@CouponNumber and T.QTY=@CouponCount and T.OprID=@OprID))    
   begin  
     set @ActiveReceiveRecordCount = @ActiveReceiveRecordCount + 1  
    select @BrandID=StoreBrandID from Brand where StoreBrandCode=@BrandCode      
    select @StoreID=StoreID from Store where BrandID=@BrandID and StoreCode=@ShopCode          
    select @CardTotalAmt = TotalAmount, @CardTotalPoints = isnull(TotalPoints,0) from Card where CardNumber = @CardNumber      
    set @OpenBal = isnull(@CardTotalAmt, 0)      
    set @Amount = isnull(@Amount, 0)      
    set @CloseBal = @OpenBal + @Amount      
      
      if @OprID in (53,54,56)      
      begin       
        
  -- Todo 异常处理 没有再次校验       
  select @CouponTypeID=CouponTypeID from Coupon where CouponNumber=@CouponNumber      
--  select @CouponNumPatternLen=Len(isNull(CouponNumPattern,'')) from CouponType where CouponTypeID=@CouponTypeID      
--  set @CouponLength=len(cast(@CouponCount-1 as varchar(512)))      
  --set @CouponNumberEnd=Substring(@CouponNumber,1,len(@CouponNumber)-@CouponLength-3)+right('0'+Cast(Cast(Substring(@CouponNumber,len(@CouponNumber)-@CouponLength-2,@CouponLength+3) as int)+@CouponCount-1 as Varchar(512)),@CouponLength+3)      
--  set @CouponNumberEnd=Substring(@CouponNumber,1,@CouponNumPatternLen)+right('0000000000000000'+Cast(Cast(right(@CouponNumber,len(@CouponNumber)-@CouponNumPatternLen) as bigint)+@CouponCount-1 as Varchar(512)),len(@CouponNumber)-@CouponNumPatternLen)   
   
  exec CalcLastNumber 1, @CouponCount, @CouponNumber, @CouponNumberEnd output  
      
        DECLARE CUR_ReceiveTxnCoupon CURSOR fast_forward local FOR      
           SELECT CouponNumber,isNull(CouponAmount,0), CouponExpiryDate, CardNumber, CouponTypeID, Status      
              from Coupon where CouponNumber between @CouponNumber and @CouponNumberEnd and CouponTypeID = @CouponTypeID      
              --from Coupon where CouponNumber = @CouponNumber  
        OPEN CUR_ReceiveTxnCoupon      
        FETCH FROM CUR_ReceiveTxnCoupon INTO @CouponNumber,@CouponOpenBal, @CouponExpiryDate, @CouponCardNumber, @CouponTypeID, @CouponStatus      
        WHILE @@FETCH_STATUS=0      
        BEGIN      
   if @OprID in (53)       -- 激活coupon， 使用输入的金额，作为coupon最终的实际金额      
   begin      
     set @CouponCloseBal=@Amount      
     set @ActAmount=@Amount-@CouponOpenBal      
   end      
   else if @OprID in (54)              
   begin      
     set @ActAmount = @Amount      
     set @CouponCloseBal = @CouponOpenBal + @Amount      
     set @CardNumber = @CouponCardNumber      
   end      
   else if @OprID in (56)              
   begin      
     set @ActAmount = @Amount      
     set @CouponCloseBal = @CouponOpenBal - @Amount          
   end  
  
   exec CalcCouponNewStatus @CouponNumber, @CouponTypeID, @OprID, @CouponStatus, @NewCouponStatus output      
   exec CalcCouponNewExpiryDate @CouponTypeID, @OprID, @CouponExpiryDate, @NewCouponExpiryDate output       
   insert into Coupon_Movement      
    (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal,       
    BusDate, Txndate, Remark, SecurityCode, CreatedBy, OrgExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,      
    NewExpiryDate, OrgStatus, NewStatus)      
   values      
    (@OprID, @CardNumber, @CouponNumber, @CouponTypeID, '', @KeyID, @TxnNo, @CouponOpenBal, @ActAmount, @CouponCloseBal,      
    @BusDate, @TxnDate, @Remark, '', @ApprovedBy, @CouponExpiryDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode,      
    @NewCouponExpiryDate, @CouponStatus, @NewCouponStatus)      
          
            -- 当消费Coupon时(OprID = 54), 执行Forfeit的过程。      
--            if @OprID = 54      
--              exec DoCouponForfeit @ApprovedBy, @CouponNumber, @TxnNo, @BusDate, @TxnDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode, 0       
          
   FETCH FROM CUR_ReceiveTxnCoupon INTO @CouponNumber,@CouponOpenBal, @CouponExpiryDate, @CouponCardNumber, @CouponTypeID, @CouponStatus      
  END      
  CLOSE CUR_ReceiveTxnCoupon      
  DEALLOCATE CUR_ReceiveTxnCoupon               
      end      
  end    /*end: if @CouponCount > 0  */    
    FETCH FROM CUR_RECTXN INTO @KeyID, @OprID, @BrandCode, @ShopCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber,@CouponNumber, @Amount,       
    @CouponCount, @Points, @VoidKeyID, @VoidTxnNo, @Additional, @Remark, @SecurityCode, @ApproveStatus, @ApprovedBy,@ApprovalCode, @TenderID               
  END      
  CLOSE CUR_RECTXN       
  DEALLOCATE CUR_RECTXN            
  /********执行插入Coupon_Movement**************/        
    
          
  if @ActiveReceiveRecordCount = 0   
    set @result = -1  
      
              
  Commit Tran        
  
  end  
    
    
  return @result      
end     
GO
