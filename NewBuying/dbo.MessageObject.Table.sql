USE [NewBuying]
GO
/****** Object:  Table [dbo].[MessageObject]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessageObject](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageServiceTypeID] [int] NOT NULL,
	[MessageType] [int] NOT NULL,
	[MessagePriority] [int] NULL,
	[MessageCoding] [int] NULL,
	[MessageTitle] [nvarchar](512) NULL,
	[MessageBody] [varbinary](max) NULL,
	[FromMemberID] [int] NULL,
	[IsInternal] [int] NULL,
	[Status] [int] NOT NULL DEFAULT ((0)),
	[ResponseCode] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_MESSAGEOBJECT] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键，自增长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'MessageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息发送方式 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'MessageServiceTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息类型：
GenBusinessMessage  产生的消息， MessageTypeID = 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'MessageType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息优先级。默认0。  0：最低。 依次增高' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'MessagePriority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息编码方式。默认0。  0：UTF-8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'MessageCoding'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'MessageTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息正文' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'MessageBody'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送者MemberID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'FromMemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否内部消息。默认0。 0：非内部消息。1：内部消息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'IsInternal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'-1：未准备好，需要处理 ， 0,待发送，1开始发送中，2全部发送完成（不考虑发送实际发送成功还是失败）。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0,发送成功，其他值均为发送失败时返回的code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject', @level2type=N'COLUMN',@level2name=N'ResponseCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息平台， 消息主体表。
业务逻辑：
1、 一个消息可同时有多个接收方， 接收方式（或者发送方式），统一设置。
2、发送方和接收方必须 是Member成员。（需要从Member 的系列表中获得接收方邮箱，手机号，以及其他账号）
3、一个消息只能选择一个发送方式。（MessageServiceTypeID）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MessageObject'
GO
