USE [NewBuying]
GO
/****** Object:  Table [dbo].[CouponReplenishRule_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponReplenishRule_H](
	[CouponReplenishCode] [varchar](64) NOT NULL,
	[Description] [varchar](512) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL,
	[BrandID] [int] NULL,
	[CouponTypeID] [int] NOT NULL,
	[StoreTypeID] [int] NOT NULL,
	[AutoCreateOrder] [int] NULL,
	[AutoApproveOrder] [int] NULL,
	[DayFlagID] [int] NULL,
	[MonthFlagID] [int] NULL,
	[WeekFlagID] [int] NULL,
	[ActiveTime] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_COUPONREPLENISHRULE_H] PRIMARY KEY CLUSTERED 
(
	[CouponReplenishCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CouponReplenishRule_H] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[CouponReplenishRule_H] ADD  DEFAULT ((1)) FOR [AutoCreateOrder]
GO
ALTER TABLE [dbo].[CouponReplenishRule_H] ADD  DEFAULT ((0)) FOR [AutoApproveOrder]
GO
ALTER TABLE [dbo].[CouponReplenishRule_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CouponReplenishRule_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠劵自动补货规则编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'CouponReplenishCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否生效。 0：无效，1：生效。 默认生效' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon的品牌ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'BrandID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根据brandid选择 CouponType ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'CouponTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺类型ID。1：总部。 2：店铺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'StoreTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自动创建订单' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'AutoCreateOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自动创建的订单，是否直接批核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'AutoApproveOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效天的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'DayFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效月的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'MonthFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效周的设置ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'WeekFlagID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H', @level2type=N'COLUMN',@level2name=N'ActiveTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon自动补货规则设置表. 
根据设置，自动创建order单。 （可能直接批核，可能不是。）
需要设定storetypeid， 根据storetypeid， 子表中的Store 必须根据此storetype 来过滤。
注：自动补货规则包含两种，根据发起的店铺类型不同，产生不同的order。   店铺的只能给后台， 后台的只能给供应商。 两种不同的订单。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CouponReplenishRule_H'
GO
