USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GenRandomPassword]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GenRandomPassword]
   @PWDMask varchar(512),  -- 默认为空。如果不为空，格式类似为： AAA9999。  A表示固定值。9表示随机值（@PWDPattern）。
   @PWDPattern varchar(512),  -- 默认为空。 按照@PWDMask的设置。
   @PWDLength int,            -- 密码长度。 如果@PWDMask不为空，则按照@PWDMask的长度。
   @PWDStyle int,          -- 0: 只有数字。 1：数字+字母。2：数字+字母+符号。 默认为0
   @PWD varchar(512) output,    -- 返回密码
   @PWD_Encrypt varchar(512) output  -- 返回加密的密码。（目前只有MD5）
AS
/****************************************************************************
**  Name : GenRandomPassword
**  Version: 1.0.0.0
**  Description : 根据Password的规则要求,产生随机密码.
  declare @A int, @PWD varchar(512), @PWD_Encrypt varchar(512), @PWDMask varchar(512), @PWDPattern varchar(512), @PWDLength int, @PWDStyle int
  set @PWDMask = 'AAAA999999999'
  set @PWDPattern = 'PPDQ' 
  set @PWDLength = 20
  set @PWDStyle = 0
  exec @A = GenRandomPassword @PWDMask, @PWDPattern, @PWDLength, @PWDStyle, @PWD output, @PWD_Encrypt output
  print @A
  print @PWD
  print @PWD_Encrypt 
**  Created by: Gavin @2012-08-21
**
****************************************************************************/
begin
  declare @PasswordLen int, @PasswordRandomLen int, @PatternLen int
  declare @i int, @seed int, @PWDCharAscii int  

  if isnull(@PWDMask, '') <> ''
  begin
    set @PatternLen = 0
    while @PatternLen < Len(@PWDMask)
    begin      
      if substring(@PWDMask, @PatternLen + 1, 1) = 'A'
        set @PatternLen = @PatternLen + 1
      else  
        break  
    end   
    set @PWDPattern = isnull(@PWDPattern, '')
    set @PasswordLen = Len(RTrim(@PWDMask)) 
	set @PasswordRandomLen = Len(RTrim(@PWDMask)) - @PatternLen	
  end else
  begin
    set @PasswordRandomLen = @PWDLength
    set @PasswordLen = @PWDLength
  end
  
  set @i = 0
  set @PWD = ''
  while @i < @PasswordRandomLen
  begin
--    set @seed = round(abs(cast(getdate() as float) * 10000000 - round(cast(getdate() as float) * 10000000, 0)) * 10000000, 0)
    if @PWDStyle = 0  -- 0~9, ascii: 48~57
    begin      
      set @PWDCharAscii = floor(rand() * 10) + 48
      set @PWD = @PWD + char(@PWDCharAscii)
    end
    if @PWDStyle = 1  -- 0~9, a~z, A~Z ascii: 48~57, 97~122, 65~90
    begin      
      set @PWDCharAscii = floor(rand() * 74) + 48
      if @PWDCharAscii > 57 
        set @PWDCharAscii = @PWDCharAscii + 7
      if @PWDCharAscii > 90
        set @PWDCharAscii = @PWDCharAscii + 6
      if @PWDCharAscii > 122
        set @PWDCharAscii = 48          
      set @PWD = @PWD + char(@PWDCharAscii)
    end
    if @PWDStyle = 2  -- 所有可见字符 ascii: 33~128
    begin
      set @PWDCharAscii = floor(rand() * 95) + 33
      if @PWDCharAscii > 128
        set @PWDCharAscii = 33          
      set @PWD = @PWD + char(@PWDCharAscii)
    end 
    set @i = @i + 1       
  end
  
  if isnull(@PWDMask, '') <> ''
    set @PWD = @PWDPattern + @PWD
  set @PWD_Encrypt = dbo.MD5(@PWD, 32)
end

GO
