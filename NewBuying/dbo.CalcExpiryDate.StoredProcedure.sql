USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[CalcExpiryDate]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[CalcExpiryDate]
   @OprID				int,
   @CardNumber			varchar(512),
   @AddValue			money,
   @NewCardExpiryDate	datetime output,
   @NewAmountExpiryDate datetime output,
   @NewPointExpiryDate	datetime output,
   @IsMultiExpiry		int output
AS
/****************************************************************************
**  Name : CalcExpiryDate
**  Version: 1.0.0.10
**  Description : 计算增值时，有效期的延长。（根据OprID判断，需要返回卡有效期，金额有效期，积分有效期。）
  declare @NewCardExpiryDate datetime, @NewAmountExpiryDate datetime, @NewPointExpiryDate datetime, @IsMultiExpiry int
  exec CalcExpiryDate 22, '11012578360', 0, @NewCardExpiryDate output, @NewAmountExpiryDate output, @NewPointExpiryDate output, @IsMultiExpiry output
  print @NewCardExpiryDate 
  print @NewAmountExpiryDate 
  print @NewPointExpiryDate 
  print @IsMultiExpiry 
 
**  Created by: Gavin @2012-03-19
**  Modify by: Gavin @2012-07-17 (Ver 1.0.0.1)
**  Modify by: Gavin @2012-12-28 (Ver 1.0.0.2) Oprid=26 时，需要重算有效期。
**  Modify by: Gavin @2014-09-02 (Ver 1.0.0.3) 修正当没有设置CardExtensionRule表时,重置时间不断累加问题. 增加默认 @ExtendType = 0
**  Modify by: Gavin @2014-09-15 (Ver 1.0.0.4) 增加指定有效期的选项, 增加有效期延长的极限值。
**  Modify by: Gavin @2014-09-16 (Ver 1.0.0.5) 对于没有在CardExtensionRule中设置的情况，卡的有效期将不做变动，除非是已经过期，或者要求重置
**  Modify by: Gavin @2014-09-23 (Ver 1.0.0.6) 对于ExtensionUnit=0的情况(永久有效), 直接设置一个hardcode的 大日期值. 2100-01-01
**  Modify by: Gavin @2014-12-31 (Ver 1.0.0.7) 统一startdate和enddate的比较规则：如果填写的日期不带时间（时间为0），则和 getdate（）比较时，startdate可以直接判断 < getdate， enddate 需要 >= (getdate() - 1)    
**  Modify by: Gavin @2015-12-30 (Ver 1.0.0.9) 修正：当卡有效期延长没有设置，积分有效期规则有设置时， 同同时更改卡有效期的问题     
**  Modify by: Gavin @2017-01-17 (Ver 1.0.0.10) 增加oprid = 22 的情况。                                    
**
****************************************************************************/
begin 
   set @IsMultiExpiry = 1
   
   declare @CardTypedaysnum int,  @CardTypedaysunit int, @Extensiondaysnum int,  @Extensiondaysunit int, @CardGradeID int,
           @CardExpiryDate datetime, @CardTypeID int   
   declare @RuleType int, @MaxLimit int, @RuleAmount money, @ExtendType int, @daysnum int, @daysunit int, @ActiveResetExpiryDate int
   declare @SpecifyExpiryDate datetime, @LimitExpiryDate datetime
   
   select @CardGradeID = isnull(CardGradeID,0), @CardExpiryDate = CardExpiryDate, @CardTypeID = CardTypeID
     from Card where CardNumber = @CardNumber
   select @CardTypedaysnum = isnull(CardValidityDuration, 0), @CardTypedaysunit = CardValidityUnit, @ActiveResetExpiryDate = ActiveResetExpiryDate 
     from CardGrade where CardGradeID = @CardGradeID     
     
   set @ExtendType = 0  
   set @daysnum = @CardTypedaysnum
   set @daysunit = @CardTypedaysunit
   set @NewCardExpiryDate = @CardExpiryDate
   set @NewAmountExpiryDate = @NewCardExpiryDate
   set @NewPointExpiryDate = @NewCardExpiryDate  
   
   -- 增值加卡有效期，金额有效期。(增加金额的操作)
   if @OprID in (2, 7, 11, 12, 17, 21, 22, 26, 27)
   begin
     -- 如果已经过期，或者设置为非延期，而是重置时，都是从当天的基础上添加延期        
     if (@OprID in (11, 21, 26) and @ActiveResetExpiryDate = 1)
       set @NewCardExpiryDate = Convert(date, GETDATE(), 120)
          
     select top 1 @Extensiondaysnum = isnull(Extension, 0),  @Extensiondaysunit = isnull(ExtensionUnit, 1), 
          @RuleType = RuleType, @MaxLimit = MaxLimit, @RuleAmount = RuleAmount, @ExtendType = ExtendType,
          @SpecifyExpiryDate = SpecifyExpiryDate, @LimitExpiryDate = LimitExpiryDate
        from CardExtensionRule 
       where CardTypeID = @CardTypeID and isnull(CardGradeID,0) = @CardGradeID 
         and StartDate <= Getdate() and EndDate >= (GetDate() - 1)
         and @AddValue >= RuleAmount and Status = 1 and RuleType = 0
       order by RuleAmount, ExtensionRuleSeqNo
       
--     if @@ROWCOUNT <> 0           
--     begin
--       set @daysnum = @Extensiondaysnum
--       set @daysunit = @Extensiondaysunit     
--     end     
     if @@ROWCOUNT <> 0   
     begin  
       set @daysnum = @Extensiondaysnum
       set @daysunit = @Extensiondaysunit       
       if @daysunit = 0
         set @NewCardExpiryDate = '2100-01-01'  
       if @daysunit = 1
         set @NewCardExpiryDate = DATEADD(yy, @daysnum, @NewCardExpiryDate) 
       if @daysunit = 2
         set @NewCardExpiryDate = DATEADD(mm, @daysnum, @NewCardExpiryDate)   
       if @daysunit = 3
         set @NewCardExpiryDate = DATEADD(ww, @daysnum, @NewCardExpiryDate) 
       if @daysunit = 4
         set @NewCardExpiryDate = DATEADD(dd, @daysnum, @NewCardExpiryDate) 
       if @daysunit = 6
         set @NewCardExpiryDate = @SpecifyExpiryDate
--     if @NewCardExpiryDate > @LimitExpiryDate
--       set @NewCardExpiryDate = @LimitExpiryDate  
     end
     
     -- 延长后还是过期，则强制改到当前日期。（看用户具体情况，是否需要这个功能）
     if (@NewCardExpiryDate <= GETDATE())
       set @NewCardExpiryDate = Convert(date, GETDATE(), 120)     
              
     -- 计算金额有效期  
     select top 1 @Extensiondaysnum = isnull(Extension, 0),  @Extensiondaysunit = isnull(ExtensionUnit, 1), 
          @RuleType = RuleType, @MaxLimit = MaxLimit, @RuleAmount = RuleAmount, @ExtendType = ExtendType,
          @SpecifyExpiryDate = SpecifyExpiryDate, @LimitExpiryDate = LimitExpiryDate
        from CardExtensionRule 
       where CardTypeID = @CardTypeID and isnull(CardGradeID,0) = @CardGradeID 
	     and StartDate <= Getdate() and EndDate >= (GetDate() - 1)
         and @AddValue >= RuleAmount and Status = 1 and RuleType = 1
       order by RuleAmount, ExtensionRuleSeqNo
     if @@ROWCOUNT <> 0           
     begin
       set @daysnum = @Extensiondaysnum
       set @daysunit = @Extensiondaysunit  
       -- 新增金额都是从当天开始计算
       set @NewAmountExpiryDate = Convert(date, GETDATE(), 120)   
       if @daysunit = 0
         set @NewAmountExpiryDate = '2100-01-01'                                     
       if @daysunit = 1
         set @NewAmountExpiryDate = DATEADD(yy, @daysnum, @NewAmountExpiryDate) 
       if @daysunit = 2
         set @NewAmountExpiryDate = DATEADD(mm, @daysnum, @NewAmountExpiryDate)   
       if @daysunit = 3
         set @NewAmountExpiryDate = DATEADD(ww, @daysnum, @NewAmountExpiryDate) 
       if @daysunit = 4
         set @NewAmountExpiryDate = DATEADD(dd, @daysnum, @NewAmountExpiryDate)   
       if @daysunit = 6
         set @NewAmountExpiryDate = @SpecifyExpiryDate                
     end else
       -- 没有设置的话，直接使用card的有效期                   
       set @NewAmountExpiryDate = @NewCardExpiryDate
       
--     if @NewAmountExpiryDate > @LimitExpiryDate
--       set @NewAmountExpiryDate = @LimitExpiryDate         
   end
   
   -- 消费加积分，购买积分
   -- 增值加卡有效期，金额有效期。
   if @OprID in (3, 14, 15, 27, 23, 47)
   begin
     select top 1 @Extensiondaysnum = isnull(Extension, 0),  @Extensiondaysunit = isnull(ExtensionUnit, 1), 
          @RuleType = RuleType, @MaxLimit = MaxLimit, @RuleAmount = RuleAmount, @ExtendType = ExtendType,
          @SpecifyExpiryDate = SpecifyExpiryDate, @LimitExpiryDate = LimitExpiryDate
        from CardExtensionRule 
       where CardTypeID = @CardTypeID and isnull(CardGradeID,'') = @CardGradeID 
	     and StartDate <= Getdate() and EndDate >= (GetDate() - 1)
         and @AddValue >= RuleAmount and Status = 1 and RuleType = 2
       order by RuleAmount, ExtensionRuleSeqNo       
     if @@ROWCOUNT <> 0           
     begin
       set @daysnum = @Extensiondaysnum
       set @daysunit = @Extensiondaysunit          
       -- 新增积分都是从当天开始计算
       set @NewPointExpiryDate = GETDATE() 
       if @daysunit = 0
         set @NewPointExpiryDate = '2100-01-01'                                          
       if @daysunit = 1
         set @NewPointExpiryDate = DATEADD(yy, @daysnum, @NewPointExpiryDate) 
       if @daysunit = 2
         set @NewPointExpiryDate = DATEADD(mm, @daysnum, @NewPointExpiryDate)   
       if @daysunit = 3
         set @NewPointExpiryDate = DATEADD(ww, @daysnum, @NewPointExpiryDate) 
       if @daysunit = 4
         set @NewPointExpiryDate = DATEADD(dd, @daysnum, @NewPointExpiryDate)  
       if @daysunit = 6
         set @NewPointExpiryDate = @SpecifyExpiryDate                                                     
     end else
       set @NewPointExpiryDate = @NewCardExpiryDate
       
 --    if @NewPointExpiryDate > @LimitExpiryDate
 --      set @NewPointExpiryDate = @LimitExpiryDate         
   end    
end

GO
