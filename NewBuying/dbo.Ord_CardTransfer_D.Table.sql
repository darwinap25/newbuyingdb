USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardTransfer_D]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardTransfer_D](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[CardTransferNumber] [varchar](64) NOT NULL,
	[SourceCardTypeID] [int] NULL,
	[SourceCardGradeID] [int] NULL,
	[SourceCardNumber] [varchar](64) NOT NULL,
	[DestCardTypeID] [int] NULL,
	[DestCardGradeID] [int] NULL,
	[DestCardNumber] [varchar](64) NOT NULL,
	[OriginalTxnNo] [varchar](512) NULL,
	[TxnDate] [datetime] NULL,
	[StoreCode] [varchar](64) NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[BrandCode] [varchar](64) NULL,
	[ReasonID] [int] NULL,
	[Note] [nvarchar](512) NULL,
	[OrderAmount] [money] NULL,
	[ActAmount] [money] NULL,
	[OrderPoint] [int] NULL,
	[ActPoints] [int] NULL,
	[ActCouponNumbers] [varchar](max) NULL,
	[CopyCardFlag] [int] NULL,
 CONSTRAINT [PK_ORD_CARDTRANSFER_D] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增长主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'KeyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'CardTransferNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转出的卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'SourceCardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转出的卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'SourceCardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转出的卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'SourceCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转入的卡类型ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'DestCardTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转入的卡级别ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'DestCardGradeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转入的卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'DestCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联的原始单据号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'OriginalTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'BrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转移原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'ReasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'OrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际操作转移金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'ActAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'OrderPoint'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际操作转移积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'ActPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'coupon列表。 例如： ‘0010001’，‘0020003’' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'ActCouponNumbers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否复制旧卡到新卡。null/0：只是从一个卡中转移指定值到另外一个卡。 1：旧卡换新卡。（所有数据复制到新卡），旧卡注销。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D', @level2type=N'COLUMN',@level2name=N'CopyCardFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'转赠单据表. 子表
@2014-10-30  表名从Ord_CardTransfer 改为 Ord_CardTransfer_D， 加上Ord_CardTransfer_H表。组成主从表。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardTransfer_D'
GO
