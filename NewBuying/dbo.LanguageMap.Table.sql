USE [NewBuying]
GO
/****** Object:  Table [dbo].[LanguageMap]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LanguageMap](
	[KeyID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageAbbr] [varchar](512) NOT NULL,
	[DescFieldNo] [int] NOT NULL,
	[LanguageDesc] [nvarchar](512) NULL,
 CONSTRAINT [PK_LANGUAGEMAP] PRIMARY KEY CLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'语言缩写，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageMap', @level2type=N'COLUMN',@level2name=N'LanguageAbbr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'获取CardTypeSN，CouponTypeSN的字段选择。
1：获取CardTypeSN1，CouponTypeSN1
2：获取CardTypeSN2，CouponTypeSN2
3：获取CardTypeSN3，CouponTypeSN3
else： 获取CardTypeSN1，CouponTypeSN1
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageMap', @level2type=N'COLUMN',@level2name=N'DescFieldNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'kiosk调用，传入的LanguageCode 绑定名称字段的设置表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageMap'
GO
