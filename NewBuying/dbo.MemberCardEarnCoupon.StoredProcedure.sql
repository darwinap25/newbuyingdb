USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[MemberCardEarnCoupon]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[MemberCardEarnCoupon]
  @UserID		        varchar(512),
  @OprID				int,					-- 
  @CardNumber			varchar(512),			-- 会员卡号 
  @EarnCouponTypeCode	varchar(512),			-- 期望获得的coupontype
  @ExchangeType         int,                    -- 换购方法。 null或0， 默认方法
  @StoreCode            varchar(512),		    -- 店铺编号
  @RegisterCode         varchar(512),            -- 终端号码
  @ServerCode           varchar(512)='',			-- serverid 暂时用不到,预留  
  @BrandCode            varchar(512),		    -- 品牌编号 
  @TxnNoKeyID           varchar(512),			-- ReceiveTxn 表主键
  @TxnNo		        varchar(512),			-- 交易单号
  @BusDate              datetime,				-- 交易busdate
  @TxnDate              datetime,				-- 交易发生日期时间  
  @SecurityCode         varchar(512),			-- 签名
  @Additional           varchar(512),			-- 附加信息  
  @ReturnCouponNumber   varchar(512) output,    -- 返回获取的coupon号码
  @ReturnAmount         money output,			-- 操作完成后卡金额余额
  @ReturnPoint          int output,				-- 操作完成后卡积分余额
  @ReturnExpiryDate     datetime output,	    -- 获得的Coupon的有效期
  @ReturnCouponName     varchar(512) output,	-- 获得的Coupon的名称
  @LanguageAbbr			varchar(20)=''	
as
/****************************************************************************
**  Name : MemberCardEarnCoupon  
**  Version: 1.0.0.10
**  Description : 获取coupon（使用卡金额，卡积分，coupon 来获取指定的coupon）
**  Parameter :
  declare @ReturnAmount money, @ReturnPoint int, @ReturnCouponNumber nvarchar(30),	 @Result int	    
   exec @Result = MemberCardEarnCoupon '1095', 0, '01159012', 'VOC100', 0, '8888', '', '', '', '', '', null, null, '', '', @ReturnCouponNumber output, @ReturnAmount output, @ReturnPoint output
  print @Result   
  print @ReturnCouponNumber   
  print @ReturnAmount
  print @ReturnPoint

**  Created by: Gavin @2012-03-06
**  Modify by: Gavin @2012-07-19 (ver 1.0.0.1) coupon_movement 新增approvalcode，storeID等字段。 传入的storecode，需要配合新增brandcode
**  Modify by: Gavin @2012-07-19 (ver 1.0.0.2) coupon_movement 中填充OpenBal， amount， closebal的值。
**  Modify by: Gavin @2012-10-23 (ver 1.0.0.3) 派发给member的 coupon 必须是 issued 状态的。
**  Modify by: Gavin @2013-01-15 (ver 1.0.0.4) 实现CardGradeHoldCouponRule的控制。
**  Modify by: Gavin @2013-04-02 (ver 1.0.0.5) 当CardGradeHoldCouponRule中没有对应记录时,则跳过这个检查
**  Modify by: Gavin @2013-09-22 (ver 1.0.0.6) 实现coupon换coupon， 免费获取 （ExchangeType=4,7）。（消费金额，充值金额应该不在此实现）
**  Modify by: Gavin @2014-12-02 (ver 1.0.0.7) 如果传入的@TxnNo为空,则自动产生一个TxnNo, 格式 YYYYMMDDHHNNSS 
**  Modify by: Gavin @2015-01-16 (ver 1.0.0.8) 改成使用GetCouponNumber来获取CouponNumber
**  Modify by: Gavin @2015-07-10 (ver 1.0.0.9) （for 711） 增加显式事务确保事务完整。增加try catch, 捕捉到错误就rollback
**  Modify by: Gavin @2016-07-18 (ver 1.0.0.10) （for Bauhaus）增加返回CouponType的名称. 输入参数增加语言条件， 以判断返回的CouponType的名称的语言
** 未定: 获得Coupon后发送消息(OprID=516).(此功能是否要做? ) 
**
****************************************************************************/
begin
  declare @CardExpiryDate datetime, @MemberID varchar(36), @CardGradeID nvarchar(10), @CardTypeID int, @EarnCouponTypeID int,
          @BatchID varchar(30), @TotalPoints int, @TotalAmount money, @CardStatus int, @Status int,
          @NewAmt money, @MaxAmt money, @OpenBal money, @CloseBal money, @NewPoints int, @OpenPoints int, @ClosePoints int,
          @NewCardExpiryDate datetime, @NewAmountExpiryDate datetime, @NewPointExpiryDate datetime, @IsMultiExpiry int,
          @CalcAddType int,  @CalcAddValue money, @VoidKeyID varchar(30), @MaxAmtReturn money
  declare @ExchangeAmount money, @ExchangePoint int, @ExchangeCouponTypeID nvarchar(20), @ExchangeCouponCount int
  declare @ApprovalCode varchar(6), @StoreID int, @BrandID int, @CouponAmount money, @ExchangeCouponNumber  varchar(64)
  declare @CouponStatus int, @NewCouponStatus int, @CouponExpiryDate date, @NewCouponExpiryDate date  
  declare @MemberHoldCouponCount int, @RuleHoldCouponCount int
  declare @CouponCount int, @ExchangeCouponAmount money, @ExchangeCouponExpiryDate datetime, @MsgCardBrandID int,@Language INT
  
  if isnull(@BusDate, 0) = 0
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc
  if isnull(@TxnDate, 0) = 0 
    set @TxnDate = getdate()
  set @TxnNo = isnull(@TxnNo, '')
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1 
  
  --校验Security               
  declare @ClearText varchar(512), @CheckResult int, @CheckSysDate datetime
  set @CheckSysDate = getdate()         
  set  @ClearText = LTrim(RTrim(@TxnNo)) + LTrim(RTrim(@CardNumber)) + convert(varchar(19), @TxnDate, 120)
  exec @CheckResult = CheckSecurityCode @SecurityCode, @ClearText, @CheckSysDate   
  if @CheckResult <> 0
    return @CheckResult    
  -- end
  
  if @TxnNo = ''
  begin
    set @TxnNo = datepart(yy, GetDate())*10000000000 + datepart(mm, GetDate())*100000000  + datepart(dd, GetDate())*1000000
               + datepart(hh, GetDate())*10000 + datepart(mi, GetDate())*100 + datepart(ss, GetDate())   
  end   
  
  select @BrandID = StoreBrandID from brand where StoreBrandCode = @BrandCode
  select @StoreID = StoreID from Store where StoreCode = @StoreCode and BrandID = @BrandID

  -- 默认操作方式， 使用卡金额购买coupon
  if isnull(@OprID, 0) = 0
    set @OprID = 31
                         
  select top 1 @CardNumber = CardNumber, @CardTypeID = C.CardTypeID,  @CardStatus = C.Status, @CardExpiryDate = CardExpiryDate, 
       @CardGradeID = CardGradeID, @TotalAmount = isnull(TotalAmount, 0), @TotalPoints = isnull(TotalPoints, 0),
       @MemberID = MemberID, @MsgCardBrandID = T.BrandID
    from Card C left join CardType T on C.CardTypeID = T.CardTypeID
    where CardNumber = @CardNumber       
  if @@ROWCOUNT = 0
    return -2
	
  select @EarnCouponTypeID = CouponTypeID, 
      @ReturnCouponName = case @Language when 2 then CouponTypeName2 when 3 then CouponTypeName3 else CouponTypeName1 end  
    from CouponType where CouponTypeCode = @EarnCouponTypeCode  
  if @@ROWCOUNT = 0
    return -18
    
  -- CardGradeHoldCouponRule数量校验
  select @RuleHoldCouponCount = HoldCount from CardGradeHoldCouponRule where CardGradeID = @CardGradeID and CouponTypeID = @EarnCouponTypeID and RuleType = 0
  select @MemberHoldCouponCount = count(CouponNumber) from coupon where CardNumber = @CardNumber and CouponTypeID = @EarnCouponTypeID and Status = 2
  if (isnull(@MemberHoldCouponCount, 0) >= @RuleHoldCouponCount) and (@RuleHoldCouponCount is not null)
    return -68
  select @RuleHoldCouponCount = HoldCount from CardGradeHoldCouponRule where CardGradeID = @CardGradeID and CouponTypeID = @EarnCouponTypeID and RuleType = 2
  select @MemberHoldCouponCount = count(CouponNumber) from coupon where InitCardNumber = @CardNumber and CouponTypeID = @EarnCouponTypeID
  if (isnull(@MemberHoldCouponCount, 0) >= @RuleHoldCouponCount) and (@RuleHoldCouponCount is not null)
    return -68
  
  if @OprID = 31      -- 不检查用户是否允许获得此coupon。 只是如果用卡金额或积分换取（OprID = 31），则需要检查金额等是否足够。
  begin
    if isnull(@ExchangeType, 0) = 0
      select top 1 @ExchangeType = ExchangeType, @ExchangeAmount = isnull(ExchangeAmount,0), @ExchangePoint = isnull(ExchangePoint,0), 
            @ExchangeCouponTypeID = ExchangeCouponTypeID, @ExchangeCouponCount = ExchangeCouponCount 
       from EarnCouponRule where CouponTypeID = @EarnCouponTypeID order by ExchangeRank
    else
      select @ExchangeAmount = isnull(ExchangeAmount,0), @ExchangePoint = isnull(ExchangePoint,0), 
            @ExchangeCouponTypeID = ExchangeCouponTypeID, @ExchangeCouponCount = ExchangeCouponCount 
       from EarnCouponRule where CouponTypeID = @EarnCouponTypeID and ExchangeType = @ExchangeType       
    if @@ROWCOUNT = 0
      return -16    
    if @ExchangeType in (1,3) and @TotalAmount < @ExchangeAmount    
      return -37
    if @ExchangeType in (2,3) and @TotalPoints < @ExchangePoint
      return -38    
    
    if @ExchangeType = 4  
    begin
      select @CouponCount = count(*) from coupon where CouponTypeID = @ExchangeCouponTypeID and CardNumber = @CardNumber and status = 2 
      if isnull(@CouponCount, 0) < @ExchangeCouponCount
        return -17
    end
    set @ReturnAmount = @TotalAmount - @ExchangeAmount
    set @ReturnPoint = @TotalPoints - @ExchangePoint
  end  
      
  -- 执行  
  -- ver 1.0.0.8   
--  select top 1 @ReturnCouponNumber = CouponNumber, @CouponAmount = isnull(CouponAmount,0),
--      @CouponStatus = status, @CouponExpiryDate = CouponExpiryDate 
--    from coupon where CouponTypeID = @EarnCouponTypeID and Status = 1

  exec GetCouponNumber @EarnCouponTypeID, 1, @ReturnCouponNumber output
  select @CouponAmount = isnull(CouponAmount,0), @CouponStatus = status, @CouponExpiryDate = CouponExpiryDate 
    from coupon where CouponNumber = @ReturnCouponNumber
        
  if isnull(@ReturnCouponNumber, '') <> ''
  begin
    BEGIN TRY
      BEGIN TRANSACTION  
      if @OprID = 31 and @ExchangeType in (1,2,3) --用卡金额购买，还需要需要同步扣除卡金额
      begin
        insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, 
           CardCashDetailID, CardPointDetailID, Remark, SecurityCode, CreatedBy, StoreID, RegisterCode, ServerCode,
           OpenPoint, ClosePoint, OrgExpiryDate, NewExpiryDate, OrgStatus, NewStatus)
        values
          (@OprID, @CardNumber, '', 0, @TxnNo, @TotalAmount, -@ExchangeAmount, @TotalAmount - @ExchangeAmount, -@ExchangePoint, @BusDate, @TxnDate, 
          null, null, '', @SecurityCode, @UserID, @StoreID, @RegisterCode, @ServerCode,
          @TotalPoints, @TotalPoints, @CardExpiryDate, @CardExpiryDate, @CardStatus, @CardStatus)    
      end
      if @ExchangeType = 4
      begin
        declare @i int
        set @i = 0
        DECLARE CUR_EarnCoupon CURSOR fast_forward local FOR  
            select CouponNumber, CouponAmount, CouponExpiryDate from coupon 
              where CouponTypeID = @ExchangeCouponTypeID and CardNumber = @CardNumber	and status = 2
            order by CouponNumber 
        OPEN CUR_EarnCoupon  
        FETCH FROM CUR_EarnCoupon INTO @ExchangeCouponNumber, @ExchangeCouponAmount, @ExchangeCouponExpiryDate
        WHILE @@FETCH_STATUS=0  
        BEGIN
          if @i >= @ExchangeCouponCount 
            break
          else 
          begin          
            insert into Coupon_Movement
              (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
               BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
               OrgExpirydate, OrgStatus, NewStatus)    
            values
              (34, @CardNumber, @ExchangeCouponNumber, @ExchangeCouponTypeID, '', 0, @TxnNo, @ExchangeCouponAmount, -@ExchangeCouponAmount, 0,
               @BusDate, @TxnDate, '',@SecurityCode, @UserID, @ExchangeCouponExpiryDate, '', @StoreID, @ServerCode, @RegisterCode,
               @ExchangeCouponExpiryDate, 2, 3)  
            set @i = @i + 1
          end        
          FETCH FROM CUR_EarnCoupon INTO @ExchangeCouponNumber, @ExchangeCouponAmount, @ExchangeCouponExpiryDate
        END
        CLOSE CUR_EarnCoupon
        DEALLOCATE CUR_EarnCoupon 
      end
    
      exec GenApprovalCode @ApprovalCode output
  	  exec CalcCouponNewStatus @ReturnCouponNumber, @EarnCouponTypeID, @OprID, @CouponStatus, @NewCouponStatus output
	  exec CalcCouponNewExpiryDate @EarnCouponTypeID, @OprID, @CouponExpiryDate, @NewCouponExpiryDate output     
	  set @ReturnExpiryDate = @NewCouponExpiryDate
      insert into Coupon_Movement
        (OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
         BusDate, Txndate, Remark, SecurityCode, CreatedBy, NewExpiryDate, ApprovalCode, StoreID, ServerCode, RegisterCode,
         OrgExpirydate, OrgStatus, NewStatus)    
      values
        (@OprID, @CardNumber, @ReturnCouponNumber, @EarnCouponTypeID, '', 0, @TxnNo, @CouponAmount, 0, @CouponAmount,
         @BusDate, @TxnDate, '',@SecurityCode, @UserID, @NewCouponExpiryDate, @ApprovalCode, @StoreID, @ServerCode, @RegisterCode,
         @CouponExpiryDate, @CouponStatus, @NewCouponStatus)
       
      -- 增加消息 ?
      -- exec GenBusinessMessage 516, @MemberID, @CardNumber, @ReturnCouponNumber, @CardTypeID, @CardGradeID, @MsgCardBrandID,  @EarnCouponTypeID, ''   
      ---  ---------  
      COMMIT TRANSACTION  
      return 0
    END TRY
    BEGIN CATCH      
      ROLLBACK TRANSACTION
      return -999
    END CATCH  
  end else
    return -18   
end

GO
