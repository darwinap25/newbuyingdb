USE [NewBuying]
GO
/****** Object:  Table [dbo].[S_Tree]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[S_Tree](
	[NodeID] [int] NOT NULL,
	[Text] [varchar](100) NOT NULL,
	[ParentID] [int] NOT NULL,
	[ParentPath] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[OrderID] [int] NULL,
	[comment] [varchar](50) NULL,
	[Url] [varchar](100) NULL,
	[PermissionID] [int] NULL,
	[ImageUrl] [varchar](100) NULL,
	[ModuleID] [int] NULL,
	[KeShiDM] [int] NULL,
	[KeshiPublic] [varchar](10) NULL,
	[SSCode] [char](3) NOT NULL,
 CONSTRAINT [PK_S_Tree] PRIMARY KEY NONCLUSTERED 
(
	[NodeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'S_Tree', @level2type=N'COLUMN',@level2name=N'SSCode'
GO
