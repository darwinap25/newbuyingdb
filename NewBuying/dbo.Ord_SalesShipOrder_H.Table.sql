USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_SalesShipOrder_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_SalesShipOrder_H](
	[SalesShipOrderNumber] [varchar](64) NOT NULL,
	[OrderType] [int] NULL DEFAULT ((0)),
	[MemberID] [varchar](64) NULL,
	[CardNumber] [varchar](64) NULL,
	[ReferenceNo] [varchar](64) NULL,
	[TxnNo] [varchar](64) NOT NULL,
	[DeliveryFlag] [int] NULL DEFAULT ((0)),
	[DeliveryCountry] [varchar](64) NULL,
	[DeliveryProvince] [varchar](64) NULL,
	[DeliveryCity] [varchar](64) NULL,
	[DeliveryDistrict] [varchar](64) NULL,
	[DeliveryAddressDetail] [nvarchar](512) NULL,
	[DeliveryFullAddress] [nvarchar](512) NULL,
	[DeliveryNumber] [varchar](64) NULL,
	[DeliveryPostCode] [varchar](64) NULL,
	[LogisticsProviderID] [int] NULL,
	[Contact] [nvarchar](512) NULL,
	[ContactPhone] [nvarchar](512) NULL,
	[RequestDeliveryDate] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryBy] [int] NULL,
	[Remark] [varchar](512) NULL,
	[Status] [int] NULL DEFAULT ((0)),
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_SALESSHIPORDER_H] PRIMARY KEY CLUSTERED 
(
	[SalesShipOrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Update_Ord_SalesShipOrder_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_SalesShipOrder_H] ON [dbo].[Ord_SalesShipOrder_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_SalesShipOrder_H
* Version: 1.0.0.3
* Description :
     select * from Ord_SalesShipOrder_H
     update Update_Ord_SalesShipOrder_H set approvestatus = 'A' where SalesPickOrderNumber = 'SPUO00000000001'
** Create By Gavin @2016-07-01
** Modify By Gavin @2016-10-24 (ver 1.0.0.1) 根据vinnce要求，货品全部顾客签收后，直接update sales -> status = 5. 不需要status=14 的状态。 
** Modify By Gavin @2016-11-17 (ver 1.0.0.2) 把update Sales_H语句，放到if 判断中，不会每次都去update.  另外加上同步更新SVA上的sales_H (LINKSVA)。
** Modify By Gavin @2016-11-18 (ver 1.0.0.3) 取消linkserver的调用。 （在trigger中更新linkserver的表，会出错误，单独写SP由程序来调用）
*/
/*==============================================================*/
BEGIN  
  DECLARE @SalesShipOrderNumber varchar(64), @Status INT, @CreatedBy INT, @OldStatus INT, @ApprovalCode char(6)
  DECLARE @Busdate DATE, @StoreID INT, @StoreCode varchar(64)
  DECLARE @SalesPickOrderNumber VARCHAR(64), @TxnNO VARCHAR(64), @SalesStatus INT
  
  SELECT TOP 1 @BusDate = BusDate FROM sodeod WHERE SOD = 1 and EOD = 0 ORDER BY BusDate DESC

  DECLARE CUR_Ord_SalesShipOrder_H CURSOR fast_forward FOR
    SELECT SalesShipOrderNumber, Status, CreatedBy FROM INSERTED
  OPEN CUR_Ord_SalesShipOrder_H
  FETCH FROM CUR_Ord_SalesShipOrder_H INTO @SalesShipOrderNumber, @Status, @CreatedBy
  WHILE @@FETCH_STATUS=0
  BEGIN
    SELECT @OldStatus = Status, @SalesPickOrderNumber = ReferenceNo from Deleted where SalesShipOrderNumber = @SalesShipOrderNumber
	SELECT @TxnNO = H.ReferenceNo, @StoreCode = H.PickupLocation, @StoreID = S.StoreID FROM Ord_SalesPickOrder_H H 
	  LEFT JOIN BUY_STORE S ON H.PickupLocation = S.StoreCode
	WHERE SalesPickOrderNumber = @SalesPickOrderNumber

	IF NOT EXISTS(SELECT * FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNO AND ApproveStatus <> 'A')
	BEGIN
	  SET @SalesStatus = 11
	  IF EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE ReferenceNo IN (SELECT SalesPickOrderNumber FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNO) AND Status = 1)
	    SET @SalesStatus = 12
	  IF EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE ReferenceNo IN (SELECT SalesPickOrderNumber FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNO) AND Status = 3)
	    SET @SalesStatus = 13
	  IF NOT EXISTS(SELECT * FROM Ord_SalesShipOrder_H WHERE ReferenceNo IN (SELECT SalesPickOrderNumber FROM Ord_SalesPickOrder_H WHERE ReferenceNo = @TxnNO) AND Status <> 3)
	  BEGIN
	    -- SET @SalesStatus = 14
		SET @SalesStatus = 5 
		--UPDATE A SET Status = @SalesStatus FROM OPENQUERY([LINKSVA], 'SELECT * FROM SVA_621.dbo.Sales_h') A  WHERE TxnNo = @TxnNO AND Status <> @SalesStatus 
		--UPDATE LINKSVA.SVA_621.dbo.Sales_h SET Status = @SalesStatus WHERE TxnNo = @TxnNO AND Status <> @SalesStatus  -- sva 只需要complete就行。
	  END
	  UPDATE Sales_H SET Status = @SalesStatus WHERE TransNum = @TxnNO AND Status <> @SalesStatus  
	END	

	IF @OldStatus < 1 AND @Status = 1 
	BEGIN
	  EXEC DeductStockBySales @CreatedBy, @SalesShipOrderNumber, @TxnNO, @StoreID, 1 
	END

    FETCH FROM CUR_Ord_SalesShipOrder_H INTO @SalesShipOrderNumber, @Status, @CreatedBy
  END
  CLOSE CUR_Ord_SalesShipOrder_H 
  DEALLOCATE CUR_Ord_SalesShipOrder_H  
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'SalesShipOrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。  1：需要第三方中转（比如送到门店，顾客门店自提） 2：直接产生送货单，交付快递就算完成。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员号码。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡号码。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'CardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。（SalesPickOrderNumber 号码）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'TxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货标志。0： 自提，不送货。 1：送货' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在国家（存放Country表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryCountry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在省 （存放Province表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryProvince'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货所在城市 （存放City表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryCity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址所在区县 （存放District表Code）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryDistrict'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货详细地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryAddressDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货完整地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryFullAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货单号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮编' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryPostCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物流供应商ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'LogisticsProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要求送货日期  （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'RequestDeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'货物送达日期   （system date）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'DeliveryBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态： 0：创建。 1：发货。 2：中转签收（门店）3：顾客签收（完成）4：未发送作废。5：退回 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发货单（面向订单的）。 @2016-05-20
（Ord_SalesPickOrder_H 批核后，产生发货单）
@2016-09-06 增加字段： DeliveryPostCode 。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_SalesShipOrder_H'
GO
