USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberCardPointDetail]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMemberCardPointDetail]
  @CardNumber			varchar(512)			-- 会员卡号  
AS
/****************************************************************************
**  Name : GetMemberCardPointDetail  
**  Version: 1.0.0.1
**  Description : 获得会员卡的积分明细(可能按照有效期group)
**  Parameter :
         exec GetMemberCardPointDetail '000300001'
**  return:
   数据集: CardNumber， CardTypeID， AddPoint（int）， BalancePoint（int）， ExpiryDate（datetime）， Statusd（int）
**  Created by: Gavin @2012-03-06
**  Modify by: Gavin @2015-12-23 (ver 1.0.0.1) 有效期可能带有时间, group时需要去掉时间
**
****************************************************************************/
begin
  declare @CardTypeID int
  select @CardTypeID = C.CardTypeID from Card C Left join CardType T on C.CardTypeID = T.CardTypeID 
       where C.CardNumber = @CardNumber 
  if isnull(@CardTypeID, 0) = 0
    return -2

  select CardNumber,CardTypeID, RechargePoint,BalancePoint,ForfeitPoint, cast(ExpiryDateStr as datetime) as ExpiryDate,[status] from
  (select max(CardNumber) as CardNumber, @CardTypeID as CardTypeID, 
		sum(isnull(AddPoint,0)) as RechargePoint, sum(isnull(BalancePoint,0)) as BalancePoint, 
		sum(isnull(ForfeitPoint,0)) as ForfeitPoint, ExpiryDateStr, max([status]) as [status]
  from (select Convert(varchar(10), ExpiryDate, 120) as ExpiryDateStr, * from CardPointDetail where CardNumber = @CardNumber and [status] = 1) A  
  group by ExpiryDateStr) temp 
  where BalancePoint>0
  order by ExpiryDate

  return 0
end

GO
