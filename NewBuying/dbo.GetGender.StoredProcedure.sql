USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetGender]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetGender]
  @KeyID           int,
  @KeyFilter       varchar(20),		  -- 条件格式： 1,2,3    
  @LanguageAbbr    varchar(20)='' 
AS
/****************************************************************************
**  Name : GetGender
**  Version: 1.0.0.3
**  Description : 获得性别表
**
**  Parameter :
  declare @a int, @MessageID int
  exec @a = GetGender 1,'2,3',''
  print @a  
**  Created by: Gavin @2014-01-07
**  Modify by: Gavin @2014-01-22 (ver 1.0.0.1) hardcode增加一条记录：  全部 
**  Modify by: Gavin @2014-01-23 (ver 1.0.0.2) 增加自定义条件参数@KeyFilter。条件格式： 1,2,3  
**  Modify by: Gavin @2016-04-07 (ver 1.0.0.3) 主键名字由KeyID 改为 GenderID 
**
****************************************************************************/
begin
  declare @Language int, @SQLStr nvarchar(1000)
  select @Language = DescFieldNo from LanguageMap where LanguageAbbr = @LanguageAbbr
  if isnull(@Language, 0) = 0
    set @Language = 1
  set @KeyID = isnull(@KeyID, 0)  
  set @KeyFilter = isnull(@KeyFilter, '')
																				
  set @SQLStr = 'select null as KeyID, ''A'' as GenderCode, ''All'' as GenderDesc1, ''全部'' as GenderDesc2, ''全部'' as GenderDesc3,  '
    + 'case ' + cast(@Language as varchar) + ' when 2 then ''全部'' when 3 then ''全部'' else ''All'' end as GenderDesc '
    + ' union all '
    + ' select GenderID as KeyID, GenderCode, GenderDesc1, GenderDesc2, GenderDesc3, '
    + ' case ' + cast(@Language as varchar) + ' when 2 then GenderDesc2 when 3 then GenderDesc3 else GenderDesc1 end as GenderDesc '
    + ' from Gender where (GenderID = ' + cast(@KeyID as varchar) + ' or ' + cast(@KeyID as varchar) + ' = 0) '
  if @KeyFilter <> '' 
    set @SQLStr = @SQLStr + ' and GenderID in (' + @KeyFilter + ')'

  exec sp_executesql @SQLStr
   
  return 0  
end

GO
