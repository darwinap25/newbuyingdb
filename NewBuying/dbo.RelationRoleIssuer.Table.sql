USE [NewBuying]
GO
/****** Object:  Table [dbo].[RelationRoleIssuer]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelationRoleIssuer](
	[RoleID] [int] NOT NULL,
	[CardIssuerID] [int] NOT NULL,
 CONSTRAINT [PK_RELATIONROLEISSUER] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC,
	[CardIssuerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
