USE [NewBuying]
GO
/****** Object:  Table [dbo].[AppParam]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppParam](
	[AppCode] [varchar](64) NOT NULL,
	[AppName] [varchar](512) NULL,
	[DefaultLang] [varchar](64) NULL,
	[SSCode] [char](3) NOT NULL,
 CONSTRAINT [PK_APPPARAM] PRIMARY KEY CLUSTERED 
(
	[AppCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键。标示哪个客户，比如： RRG' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AppParam', @level2type=N'COLUMN',@level2name=N'AppCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AppParam', @level2type=N'COLUMN',@level2name=N'AppName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LanguageMap的LanguageAbbr' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AppParam', @level2type=N'COLUMN',@level2name=N'DefaultLang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AppParam', @level2type=N'COLUMN',@level2name=N'SSCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统设置表。 只一条记录。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AppParam'
GO
