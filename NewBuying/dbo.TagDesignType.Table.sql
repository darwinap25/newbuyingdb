USE [NewBuying]
GO
/****** Object:  Table [dbo].[TagDesignType]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TagDesignType](
	[TagDesignTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TagDesignTypeCode] [varchar](64) NULL,
	[TagDesignTypeName1] [nvarchar](512) NULL,
	[TagDesignTypeName2] [nvarchar](512) NULL,
	[TagDesignTypeName3] [nvarchar](512) NULL,
	[BrandCode] [varchar](64) NULL,
	[TagDesignTypeDesc1] [nvarchar](512) NULL,
	[TagDesignTypeDesc2] [nvarchar](512) NULL,
	[TagDesignTypeDesc3] [nvarchar](512) NULL,
	[TagDesignTypeImageURL] [varchar](500) NULL,
	[TagDesignTypeThumbURL] [varchar](500) NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[TagDesignTypeIsActive] [int] NULL,
 CONSTRAINT [PK_TagDesignType] PRIMARY KEY CLUSTERED 
(
	[TagDesignTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
