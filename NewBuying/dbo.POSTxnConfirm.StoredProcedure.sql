USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSTxnConfirm]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[POSTxnConfirm]    
  @UserID             varchar(512),        --用户ＩＤ    
  @TxnNo              varchar(512),        --交易号    
  @TxnSNList          varchar(max),        --TxnSN List 清单， 号码之间用',' 隔开  
  @ApprovalCode       varchar(512) output  --返回的approvalCode    
as    
/****************************************************************************    
**  Name : POSTxnConfirm    
**  Version: 1.0.1.0  (POS调用入口)     
**  Description : POS提交的receivetxn 记录， 提交完成。    
**    
select * from languagemap
declare @ApprovalCode    varchar(512) , @a int
exec @a = POSTxnConfirm 1, '201403260030002', '', @ApprovalCode output
print @a
print @ApprovalCode
**  Created by: Gavin @2014-05-26   
**  Modify by: Gavin @2015-05-14 (ver 1.0.1.0) 把 @CouponList 改为 TxnSNList, TxnSN数据之间用 ',' 隔开，只confirm列表中的receive记录。 如果TxnSNList为空，则整单confirm
                                               根据TxnSNList列表来完成confirm. 只调用POSTxnConfirmNormal.  
****************************************************************************/    
begin  
  -- （ver 1.0.1.0 修改未完成）
  declare  @Result int
  exec @Result = POSTxnConfirmNormal @UserID,@TxnNo, @TxnSNList, @ApprovalCode output
  
/*  
  declare @Model varchar(64), @Result int
  exec GetAppParam @Model output  
  if @Model = 'RRG'
  begin
    exec @Result = POSTxnConfirmBySN @UserID,@TxnNo, @CouponList, @ApprovalCode output
  end else
    exec @Result = POSTxnConfirmNormal @UserID,@TxnNo, @CouponList, @ApprovalCode output
*/  
  return @Result
end

GO
