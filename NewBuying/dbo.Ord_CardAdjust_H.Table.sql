USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardAdjust_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardAdjust_H](
	[CardAdjustNumber] [varchar](64) NOT NULL,
	[OprID] [int] NOT NULL,
	[OriginalTxnNo] [varchar](512) NULL,
	[TxnDate] [datetime] NULL,
	[StoreCode] [varchar](64) NULL,
	[StoreID] [int] NULL,
	[ServerCode] [varchar](64) NULL,
	[RegisterCode] [varchar](64) NULL,
	[BrandCode] [varchar](64) NULL,
	[ReasonID] [int] NULL,
	[Note] [varchar](512) NULL,
	[ActStatus] [int] NULL,
	[ActAmount] [money] NULL,
	[ActPoints] [int] NULL,
	[ActExpireDate] [datetime] NULL,
	[CardCount] [int] NULL,
	[EffectiveDate] [datetime] NULL DEFAULT (getdate()),
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[Flag1] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_Ord_CardAdjust_H] PRIMARY KEY CLUSTERED 
(
	[CardAdjustNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [dbo].[Insert_Ord_CardAdjust_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Insert_Ord_CardAdjust_H] ON [dbo].[Ord_CardAdjust_H]
FOR INSERT
AS
/*==============================================================*/
/*                
* Name: Insert_ord_cardajdust_h
* Version: 1.0.0.0
* Description : 插入记录时触发 消息发送
*  
** Create By Gavin @2015-09-25
*/
/*==============================================================*/
BEGIN  
  declare @CreatedBy int, @OprID int, @ordernumber varchar(64)
  SELECT  @CreatedBy = CreatedBy, @OprID = OprID, @ordernumber = CardAdjustNumber FROM INSERTED  
  if @OprID in (50, 51, 52, 60)
    exec GenUserMessage @CreatedBy, 24, 0, @ordernumber
  else exec GenUserMessage @CreatedBy, 5, 0, @ordernumber  
END

GO
/****** Object:  Trigger [dbo].[Update_Ord_CardAdjust_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CardAdjust_H] ON [dbo].[Ord_CardAdjust_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CardAdjust_H
* Version: 1.0.0.16
* Description : 卡调整单据
*    select * from card 
*	 select * from Ord_CardAdjust_h, select * from Ord_CardAdjust_d
* Create By Gavin @2012-05-18
* Modify By Gavin @2014-06-05 (ver 1.0.0.1) 增加issue （OprID=28）
* Modify By Gavin @2014-07-04 (ver 1.0.0.2) 增加任意状态调整 （OprID=29）
* Modify By Gavin @2014-08-20 (ver 1.0.0.3) 增加Card redeem操作 （OprID=3）
* Modify By Gavin @2014-09-18 (ver 1.0.0.4) 如果没有填TxnDate的话,给个默认值getdate()
* Modify By Gavin @2014-10-21 (ver 1.0.0.5) Card_Movement 中写入 storeid
* Modify by Gavin @2015-02-11 (ver 1.0.0.6) 增加PurchaseType的区分。 PurchaseType=1 是原有的card库存订单。 PurchaseType=2是成card购买充值订单
* Modify by Gavin @2015-04-08 (ver 1.0.0.7) 增加调用 ChangeCardStockStatus, update card 的stockstatus
* Modify by Gavin @2015-04-13 (ver 1.0.0.8) 增加 (OprID=60),和 50 一样的处理
* Modify by Gavin @2015-04-15 (ver 1.0.0.9) Telco充值(OprID = 50,60), 因为有discount, 所以OrderAmount是充值订单金额. actamount是计算折扣后的实际扣款金额. 写入cardmovement时使用 actamount
* Modify by Gavin @2015-04-16 (ver 1.0.0.10) 50,60, 使用orderamount, 52 和 22 相同，而不是 3. 
* Modify by Gavin @2015-05-08 (ver 1.0.0.11) void 卡时,检查是否在WalletRule_D 有关联记录,如果有的话,则删除WalletRule_D记录.
* Modify by Gavin @2015-05-08 (ver 1.0.0.12) 50,60, 必须使用actualamount, 因为有GMValue. 用户填写orderamount, 通过GMValue折扣计算, 实际值写入actualamount
* Modify by Gavin @2015-08-17 (ver 1.0.0.13) (for RRG) Ord_CardAdjust_H->Note字段存放 Telco 返回的 approvalcode （TransactionRRN）
* Modify by Gavin @2015-08-27 (ver 1.0.0.14) (for RRG) 防止出现card_movement 的 Additional 中只有一个 “,” 的情况 
* Modify by Gavin @2017-02-10 (ver 1.0.0.15) (for SWD) Ord_CardAdjust_H增加字段EffectiveDate, 批核后的订单，只有到了EffectiveDate日期， 才做执行。approvestatus 需要增加一种状态： C：Complete 已经执行完成。
                                             改成调用BatchExecuteCardAdjust
* Modify by Gavin @2017-02-13 (ver 1.0.0.16) (for SWD) BatchExecuteCardAdjust 过程修改，增加参数。 此处调用，新增参数值为 1
*/
/*==============================================================*/
BEGIN  
  declare @CardAdjustNumber varchar(512), @OprID int, @RefTxnNo varchar(512), @TxnDate datetime, @ServerCode varchar(512), @RegisterCode varchar(512), @ReasonID int, @ActAmountH money, @ActPointsH int, @ActExpireDate datetime, @ApproveStatus char(1), @CreatedBy int
  declare @KeyID int, @CardNumber varchar(512), @OrderAmount money, @ActAmount money, @OrderPoints int, @ActPoints int 
  declare @OldApproveStatus char(1), @OpenBal money, @CloseBal money, @Additional nvarchar(512), @Remark nvarchar(512), @CardExpiryDate datetime
  declare @ApprovalCode char(6), @ActStatus int, @OrgStatus int, @NewStatus int
  declare @BusDate date, @StoreID int --Business Date
  declare @IssueStoreID int, @CardTypeID int, @CardGradeID int, @Note varchar(512)
  declare @NewAdditional nvarchar(512)

  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())  
  
  DECLARE CUR_Ord_CardAdjust_H CURSOR fast_forward local FOR
    SELECT CardAdjustNumber, OprID, OriginalTxnNo, isnull(TxnDate,getdate()), ServerCode, RegisterCode, 
        ReasonID, ActAmount, ActPoints, ActExpireDate, ApproveStatus, ApproveBy, ActStatus, StoreID, isnull(Note,'')
     FROM INSERTED
  OPEN CUR_Ord_CardAdjust_H
  FETCH FROM CUR_Ord_CardAdjust_H INTO @CardAdjustNumber, @OprID, @RefTxnNo, @TxnDate, @ServerCode, @RegisterCode, 
      @ReasonID, @ActAmountH, @ActPointsH, @ActExpireDate, @ApproveStatus, @CreatedBy, @ActStatus, @StoreID, @Note
  WHILE @@FETCH_STATUS=0
  BEGIN 
    SELECT @OldApproveStatus = ApproveStatus from Deleted where CardAdjustNumber = @CardAdjustNumber
    IF (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    BEGIN
      EXEC BatchExecuteCardAdjust @CardAdjustNumber, 1
    END
    FETCH FROM CUR_Ord_CardAdjust_H INTO @CardAdjustNumber, @OprID, @RefTxnNo, @TxnDate, @ServerCode, @RegisterCode, 
      @ReasonID, @ActAmountH, @ActPointsH, @ActExpireDate, @ApproveStatus, @CreatedBy, @ActStatus, @StoreID, @Note
  END
  CLOSE CUR_Ord_CardAdjust_H 
  DEALLOCATE CUR_Ord_CardAdjust_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号码，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'CardAdjustNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作ID：
5、 卡作废
21、卡激活
22、卡金额调整
23、卡积分调整
28、卡发布
29、卡状态调整。
3、 卡消费金额     （增加@2014-08-10）
50、TelCo 卡充值(店铺扣除)（实际操作：扣除店铺的TelCo卡中金额，成功后，调用程序调用外部接口执行操作（比如调用电信接口给SIM卡充值）。 SVA 端动作类似OprID：3）
51、TelCo 卡充值(总部充值钱或者积分)
52、TelCo卡金额调整。（增加@2015-02-27 ）
60、TelCo 卡充值(店铺扣除) (Offline)  （与50 的区别在于， 不需要调用telco 的接口）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'OprID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联的原始单据号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'OriginalTxnNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易单交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'TxnDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺编号（为SVAWeb程序兼容，继续保留）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'StoreCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服务器编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ServerCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'终端编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'RegisterCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品牌编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'BrandCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调整原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ReasonID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡状态调整到此设置状态。 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ActStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ActAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ActPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调整有效期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ActExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作卡数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'CardCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生效日期，默认为 当日。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'EffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： P：prepare。  A:Approve 。 V：Void.  C: 执行完成' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留标志位。 目前用于 RRG TelCo 的 Offline标志。 0：Online； 1：Offline。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H', @level2type=N'COLUMN',@level2name=N'Flag1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡调整单据主表
Modify By Gavin @20170113  add field :EffectiveDate  (for PNS SWD) .  Approvestatus 增加  C：执行完成 的状态。
订单批核后，不马上执行， 除非生效日期就是系统日期。
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardAdjust_H'
GO
