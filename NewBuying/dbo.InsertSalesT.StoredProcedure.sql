USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[InsertSalesT]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[InsertSalesT]
  @TxnNo				varchar(64),-- 交易号码
  @TenderCode           varchar(64),
  @TenderName           varchar(64),
  @TenderAmount         money
as
/****************************************************************************
**  NAME : InsertSalesT  （SVA DB，buying DB）
**  VERSION: 1.0.0.0
**  DESCRIPTION :  在Sales_T中插入记录(只插入一条，反复执行时做update处理)。(bauhaus,仅供Nick使用)
**  PARAMETER :
  declare @a int 
  exec @a=GetOrderInfo '12345'
  print @a  
**  CREATED BY: GAVIN @2017-07-26
**
****************************************************************************/
begin
    declare @a int
    IF EXISTS (SELECT name FROM   sysobjects WHERE  name = N'SVAInsertSalesT' AND TYPE = 'P')
    BEGIN
	  exec @a = SVAInsertSalesT @TxnNo, @TenderCode, @TenderName, @TenderAmount
	END  

    IF EXISTS (SELECT name FROM   sysobjects WHERE  name = N'POSInsertSalesT' AND TYPE = 'P')
    BEGIN
	  exec @a = POSInsertSalesT @TxnNo, @TenderCode, @TenderName, @TenderAmount
	END 

	return @a
end

GO
