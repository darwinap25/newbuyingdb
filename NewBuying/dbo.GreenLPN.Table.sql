USE [NewBuying]
GO
/****** Object:  Table [dbo].[GreenLPN]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GreenLPN](
	[LPNActual] [varchar](64) NOT NULL,
	[LPNType] [int] NULL,
	[GreenCardNumber] [varchar](64) NULL,
	[GreenCardStatus] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_GREENLPN] PRIMARY KEY CLUSTERED 
(
	[LPNActual] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[GreenLPN] ADD  DEFAULT ((0)) FOR [LPNType]
GO
ALTER TABLE [dbo].[GreenLPN] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[GreenLPN] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taxi LPN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'LPNActual'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What type of taxi is the incoming LPN?
1: Urban
2: New Territories
3: Lantau Island
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'LPNType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绿卡号码。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'GreenCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绿卡状态。 0：inactive。 1：active。 2：idle。 3：（为配合LPNstatus，3状态空着）。4： expiry。 5： void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'GreenCardStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'CreatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'UpdatedOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有GreenCard的 LPN。 单独保存一个表， 定时清除。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GreenLPN'
GO
