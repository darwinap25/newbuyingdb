USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[BatchGenerateNumber_Base]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchGenerateNumber_Base]
  @UserID               int,
  @Type                 int,				-- 0:卡。 1:Coupon 默认0
  @NumberTypeID		    int,	    -- CardTypeID或者CouponTypeID. CardTypeID时，可以不填
  @CardGradeID	        int,       -- 创建卡的等级， coupon 没有 cardgrade，此处置空
  @StartingNumber		bigint,             -- 批量创建Coupon的初始号码。（当CouponType的 IsImportCouponNumber=1时，@StartingNumber有效，如果此时不输入@StartingNumber，则使用hardcode号码。输入的@StartingNumber必须是整数。）
  @GenQty				int,                -- 创建数量。
  @IssueDate			datetime,           -- 此批次的发行日期
  @InitAmount			money,              -- 此批次的初始金额。 默认0
  @InitPoints           int,                -- 此批次的初始积分   默认0
  @RandomPWD			int,				-- 1:随机密码。 0: 不是。
  @InitPWD				varchar(512),        -- 初始密码
  @ExpiryDate			datetime,           -- 此批次的有效日期   默认0. (如果是0或者Null，则按照CardType或者CouponType的设置)   
  @RefTxnNo varchar(512),			    -- 参考编号，一般填入原始单号
  @ApprovalCode varchar(512), 			    -- Approval号
  @Createdby int, 				    -- 创建人ID
  @ReturnBatchID        varchar(30) output, -- 返回的批次号
  @ReturnStartNumber    nvarchar(30) output,-- 返回的起始号码
  @ReturnEndNumber      nvarchar(30) output, -- 返回的结束号码
  @ImportBatchCode		varchar(60) = '',     -- 传入的batchcardcode或者batchcouponcode
  @InitStatus           int = 0,			    -- 此批次的初始状态。
  @InitStockStatus      int = 0,          -- 此批次的初始库存状态
  @InitStoreID          int = 0          -- 创建时的初始storeid, 只是创建时,写入 card_movement表        
  --@CouponNatureID       int     --coupon nature
AS
/****************************************************************************
**  Name : BatchGenerateNumber_Base 
**  Version: 1.0.1.4
**  Description : 批量产生卡号, coupon号码
**  Parameter :
**
  declare @A int,
  @ReturnBatchID        varchar(30),
  @ReturnStartNumber    nvarchar(30),
  @ReturnEndNumber      nvarchar(30), @issuedate datetime
  set @issuedate = getdate()
  --exec @A = BatchGenerateNumber 1, 0, 8, 3, null, 50000, '2014-08-21', 100, 0, 0, '', 0, '','', 1, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output
  exec @A = BatchGenerateNumber_Base 1, 1, 8, null, null, 1000, @issuedate, 500, 0, 0, '', 0, '','', 1, @ReturnBatchID output, @ReturnStartNumber output, @ReturnEndNumber output, '', 2,6
  print @A
  print @ReturnBatchID
  print @ReturnStartNumber
  print  @ReturnEndNumber 
select * from coupontype  
select * from coupon where coupontypeid =12
select * from cardUIDMap where cardnumber = '282800000000002'
select * from cardgrade where cardtypeid = 60
select top 100 * from card where status = 1
select * from card where cardtypeid = 60 and cardgradeid = 3
  update coupontype set IsImportCouponNumber = 1
  select * from coupontype
  select * from cardtype  
  select * from cardgrade
   select count(*) from coupon where coupontypeid = 3
delete from couponuidmap where coupontypeid = 3
  select dbo.CalcCheckDigit('989800000001', 'MOD10')
  select * from coupon where couponnumber like '1234500%'
  select * from card_movement where oprid = 0 and cardnumber like '22222%'
  select * from card where cardtypeid = 8 and cardgradeid = 3
  select count(*) from Coupon where batchcouponid = 11
  delete from coupon where coupontypeid = 36
  update coupon set status = 1, cardnumber = '000100001' where coupontypeid = 1 and couponnumber >= 'CN0100071'
    delete from batchcard
    select * from batchcoupon
    select * from member
update card set status = 5 where     
**  Created by: Gavin @2012-04-10
**  Modify by: Gavin @2012-07-17 (ver 1.0.0.1) cardgrade中增加了CardNumMask，CardNumPattern，产生卡号时要优先考虑cardgrade中设置，没有时再考虑cardtype
**  Modify by: Gavin @2012-07-18 (ver 1.0.0.2) 如果CardCheckdigit=1,则产生的号码需要加上校验位.（按照EAN13的规则，如果其中有非数字字符，则作为0处理）
**  Modify by: Gavin @2012-07-20 (ver 1.0.0.3) 创建卡时，插入card_movement
**  Modify by: Gavin @2012-07-27 (ver 1.0.0.4) 因为按照 cardgrade中增加了CardNumMask，所以取最大cardnumber时，必须要加上CardGrade条件
**  Modify by: Gavin @2012-07-27 (ver 1.0.0.5) 取最大Seqno时,按照@CardNumPattern, 和@CardNumMask的长度的条件来取最大seqno
**  Modify by: Gavin @2012-08-20 (ver 1.0.0.6) 创建coupon时,是否同时产生UID,将根据字段CardNumberToUID,CouponNumberToUID来判断
**  Modify by: Gavin @2012-08-23 (ver 1.0.0.7) 增加默认参数，@ImportBatchCode，默认为空，如果有值时，就使用这个code，空时就自动产生
**  Modify by: Gavin @2012-08-24 (ver 1.0.0.8) MD5加密耗时，创建时初始密码不需要加密。
**  Modify by: Gavin @2012-08-28 (ver 1.0.0.9) 因UIDToCouponNumber, CouponNumberToUID选项修改，需要修改相应存储过程。Card 部分同样修改。
											MD5加密问题：1：随机密码：暂时取消。因为必须要做MD5加密，无法破译。2：输入密码，做MD5加密。 3：输入密码为空，则密码填空，不加密
**  Modify by: Gavin @2012-09-10 (ver 1.0.0.10)修正创建Card时，carduid 设置为 checkdigit 的情况下，取CardUID不正确问题。
**  Modify by: Gavin @2012-09-12 (ver 1.0.0.11) 输入的@Expirydate为0或null时,根据cardgrade或coupontype的设置取有效期. 
**  Modify by: Gavin @2012-11-13 (ver 1.0.0.12) 修改CalcCheckDigit_EAN13 为CalcCheckDigit。
**  Modify by: Gavin @2012-11-29 (ver 1.0.0.13) Cardtype，CardGrade的卡号产生规则变动： 如果填写了CardType中的卡号规则，下属CardGrade中的卡号规则将是相同的，不得变动。如果没有填写CardType中的卡号规则，下属CardGrade中的卡号规则必须填写，而且不能是相同的。
**  Modify by: Gavin @2013-08-30 (ver 1.0.0.14) 增加参数@InitStatus，默认0. 作为产生的号码的初始状态
**  Modify by: Gavin @2013-09-11 (ver 1.0.0.15)	如果创建coupon时,@InitStatus=1,则增加插入 40 的movement记录
**  Modify by: Gavin @2014-09-30 (ver 1.0.0.16)	如果有初始金额或者初始积分，同步写入cardcashdetail和cardpointdetail
**  Modify by: Gavin @2014-10-16 (ver 1.0.0.17)	增加输入@InitStockStatus
**  Modify by: Gavin @2014-12-30 (ver 1.0.0.18)	@InitStatus 大于 0 时, 取消插入 40 的movement记录, insert coupon时直接改状态
**  Modify by: Gavin @2014-12-31 (ver 1.0.0.19) 先把数据插入到内存表, 然后再一次性插入到正式表.
**  Modify by: Gavin @2015-01-05 (ver 1.0.1.0) 加上 set nocount on ，不返回每次影响的行数，加快速度。
                                               取消事务和分批执行（分批在BatchGenerateNumber中实现，这个过程只做插入动作）
**  Modify by: Gavin @2015-05-07 (ver 1.0.1.1) 如果初始状态 >0, 那么需要多加一条movement记录。（大批量创建时，对速度影响较大）   
**  Modify by: Gavin @2015-05-28 (ver 1.0.1.2) 增加输入参数 @InitStoreID                                            
**  Modify by: Darwin @2017-09-09 (ver 1.0.1.3) @CouponNatureID for insert into coupon
                                                @QRCodePrefix = CouponType.QRCodePrefix for concatenation as prefix to CouponNumber 
**  Modify by: Darwin @2017-09-09 (ver 1.0.1.4) @StartingNumber1, custom tag id range
****************************************************************************/
begin
  declare @NewBatchID int, @NewBatchCode varchar(30), @MaxID bigint, @TempSeqNo bigint, @i int
  declare @NumberHead varchar(100), @NumberSeqNo varchar(100), @NumberSeqNoLength int
  declare @CardNumMask nvarchar(30), @CardNumPattern nvarchar(30), @PatternLen int, @MaxSeqNo bigint
  declare @CouponNumMask nvarchar(30), @CouponNumPattern nvarchar(30), @CouponPatternLen int, @MaxCouponSeqNo bigint       
  declare @TranCount int, @MaxCouponNumber varchar(64), @IsImportCouponNumber int, @Checkdigit int
  declare @BusDate date --Business Date
  declare @TxnDate datetime --Transaction Date
  declare @NewNumber varchar(64)
  declare @IsPhysicalCard int, @CouponUID varchar(512), @NumberToUID int, @CardUID varchar(512)
  declare @ExpirydateValidityDuration int, @ExpirydateValidityUnit int, @SpecifyExpiryDate date
  declare @ModeID int, @ModeCode varchar(64)
  declare @IsCardTypeNumMask int
  DECLARE @BatchQty int    ---  一批数量 
  declare @CardTemp table(CardNumber varchar(64), CardTypeID int, CardIssueDate datetime, CardExpiryDate datetime, MemberID int, 
                          Status int, UsedCount int, CardGradeID int, BatchCardID int, TotalPoints int, TotalAmount money, 
                          CardPassword varchar(512), StockStatus int)
  declare @CouponTemp table(CouponNumber varchar(64), CouponTypeID int, CouponIssueDate datetime, CouponExpiryDate datetime, StoreID int, 
                       Status int, BatchCouponID int, CouponPassword varchar(512), CardNumber varchar(64), CouponAmount money, StockStatus int)
  DECLARE @QRCodePrefix [varchar](64)
  DECLARE @StartingNumber1		bigint
  
  set @InitStatus = isnull(@InitStatus, 0)
  select top 1 @BusDate=BusDate from SODEOD where EOD=0 and SOD=1
  set @BusDate=isNull(@BusDate,getdate())
  set @TxnDate=getdate()
  
  set nocount on
  
  -- 忽略参数@RandomPWD, 目前不允许随机密码。 如果输入了初始密码，则MD5加密
  if isnull(@InitPWD, '') <> ''
    set @InitPWD = dbo.EncryptMD5(@InitPWD)
    
  if @Type = 0
  begin         
    --alter   table   Card_movement   disable   trigger  Insert_Card_Movement 
    -- Ver 1.0.0.1 CardNumMask先取CardGrade，如果没有，则取CardType
    select @IsPhysicalCard = T.IsPhysicalCard, @NumberTypeID = T.CardTypeID from CardType T left join CardGrade G on T.CardTypeID = G.CardTypeID
      where CardGradeID = @CardGradeID
    select @CardNumMask = CardNumMask, @CardNumPattern = CardNumPattern, @NumberTypeID = CardTypeID, @Checkdigit = CardCheckdigit, 
         @NumberToUID = CardNumberToUID, @ModeID = isnull(CheckDigitModeID, 0) from CardType where CardTypeID = @NumberTypeID    
    if isnull(@CardNumMask, '') = ''      
    begin
      set @IsCardTypeNumMask = 0
      select @CardGradeID = CardGradeID, @NumberTypeID = CardTypeID, @CardNumMask = CardNumMask, @CardNumPattern = CardNumPattern, @Checkdigit = CardCheckdigit, @NumberToUID = CardNumberToUID,
         @ModeID = isnull(CheckDigitModeID, 0), @ExpirydateValidityDuration = CardValidityDuration, @ExpirydateValidityUnit = CardValidityUnit         
       from CardGrade where CardGradeID = @CardGradeID     
    end else
    begin
      set @IsCardTypeNumMask = 1
      select @CardGradeID = CardGradeID, @NumberTypeID = CardTypeID, @ExpirydateValidityDuration = CardValidityDuration, @ExpirydateValidityUnit = CardValidityUnit         
       from CardGrade where CardGradeID = @CardGradeID     
    end       
    select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
    set @ModeCode = isnull(@ModeCode, 'EAN13')
    -- 设置有效期
    if isnull(@ExpiryDate, 0) = 0
    begin
       if @ExpirydateValidityUnit = 1
         set @ExpiryDate = DATEADD(yy, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120)) 
       else if @ExpirydateValidityUnit = 2
         set @ExpiryDate = DATEADD(mm, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120))   
       else if @ExpirydateValidityUnit = 3
         set @ExpiryDate = DATEADD(ww, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120)) 
       else if @ExpirydateValidityUnit = 4
         set @ExpiryDate = DATEADD(dd, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120))  
       else set @ExpiryDate = DATEADD(yy, 100, Convert(date, GETDATE(), 120))             
    end    
        
    if isnull(@ImportBatchCode, '') = ''
      exec GetRefNoString 'BTHCAD', @NewBatchCode output
    else set @NewBatchCode = @ImportBatchCode  
    
    set @PatternLen = 0
    while @PatternLen < Len(@CardNumMask)
    begin      
      if substring(@CardNumMask, @PatternLen + 1, 1) = 'A'
        set @PatternLen = @PatternLen + 1
      else  
        break  
    end 
    
    if @Checkdigit = 1
      select @MaxSeqNo=Convert(bigint, substring(max(CardNumber), @PatternLen + 1,  Len(@CardNumMask) - @PatternLen - 1)) from Card 
        where len(CardNumber) = len(@CardNumMask) and (CardNumber like (@CardNumPattern + '%') or isnull(@CardNumPattern,'') = '' )
    else  
      select @MaxSeqNo=Convert(bigint, substring(max(CardNumber), @PatternLen + 1,  Len(@CardNumMask) - @PatternLen)) from Card 
        where len(CardNumber) = len(@CardNumMask) and (CardNumber like (@CardNumPattern + '%') or isnull(@CardNumPattern,'') = '' )
    select @MaxSeqNo = isnull(@MaxSeqNo, 0) + 1
    set @TempSeqNo = @MaxSeqNo

    if @Checkdigit = 1
    begin
	  select @ReturnStartNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo), Len(@CardNumMask) - @PatternLen - 1)
      select @ReturnEndNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo + @GenQty - 1), Len(@CardNumMask) - @PatternLen - 1)        
      select @ReturnStartNumber = dbo.CalcCheckDigit(@ReturnStartNumber, @ModeCode)
      select @ReturnEndNumber = dbo.CalcCheckDigit(@ReturnStartNumber, @ModeCode)
    end else
    begin
	  select @ReturnStartNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo), Len(@CardNumMask) - @PatternLen)
      select @ReturnEndNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxSeqNo + @GenQty - 1), Len(@CardNumMask) - @PatternLen)    
    end
    insert into BatchCard
      (BatchCardCode, SeqFrom, SeqTo, Qty, CardGradeID, CreatedOn, UpdatedOn, CreatedBy, UpdatedBy)
    values (@NewBatchCode, @MaxSeqNo, @MaxSeqNo + @GenQty - 1, @GenQty, @CardGradeID, getdate(), getdate(), @UserID, @UserID)  
    --set @NewBatchID = @@IDENTITY
    set @NewBatchID = SCOPE_IDENTITY()    
    set @i = 1    
    set @TranCount = 0
    
    while @i <= @GenQty
    begin    

      if @Checkdigit = 1
      begin
        set @NewNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @TempSeqNo), Len(@CardNumMask) - @PatternLen - 1)    
        select @NewNumber = dbo.CalcCheckDigit(@NewNumber, @ModeCode)    
      end else
        set @NewNumber = substring(@CardNumPattern, 1, @PatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @TempSeqNo), Len(@CardNumMask) - @PatternLen)    
		
      insert into @CardTemp
        (CardNumber, CardTypeID, CardIssueDate, CardExpiryDate, MemberID, Status, UsedCount, CardGradeID,
         BatchCardID, TotalPoints, TotalAmount, CardPassword, StockStatus)
      values
        (@NewNumber, @NumberTypeID, @IssueDate, @ExpiryDate, null, @InitStatus, 0, @CardGradeID, @NewBatchID, @InitPoints, @InitAmount, @InitPWD, @InitStockStatus)
      
      set @TempSeqNo = @TempSeqNo + 1 
      set @i = @i + 1  
    end
 --print  @NewNumber  
    insert into Card
        (CardNumber, CardTypeID, CardIssueDate, CardExpiryDate, MemberID, Status, UsedCount, CardGradeID,
         BatchCardID, TotalPoints, TotalAmount, CardPassword, StockStatus)
      select CardNumber, CardTypeID, CardIssueDate, CardExpiryDate, MemberID, Status, UsedCount, CardGradeID,
         BatchCardID, TotalPoints, TotalAmount, CardPassword, StockStatus
       from @CardTemp 
      insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
           CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, ApprovalCode, OrgStatus, NewStatus,
           OpenPoint, ClosePoint, StoreID)
      select 
          0, CardNumber, null, null, @RefTxnNo, 0, @InitAmount, @InitAmount, @InitPoints, @BusDate, @TxnDate, null,
           null, null, '', '', '', @UserID, @ApprovalCode, 0, @InitStatus,
           0, @InitPoints, @InitStoreID
         from @CardTemp   
         
     -- ver 1.0.1.1
     if ISNULL(@InitStatus, 0) > 0 
     begin
       insert into Card_Movement
          (OprID, CardNumber, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, Points, BusDate, Txndate, TenderID,
           CardCashDetailID, CardPointDetailID, Additional, Remark, SecurityCode, CreatedBy, ApprovalCode, OrgStatus, NewStatus,
           OpenPoint, ClosePoint, StoreID)
       select 
           401, CardNumber, null, null, @RefTxnNo, @InitAmount, 0, @InitAmount, 0, @BusDate, @TxnDate, null,
           null, null, '', '', '', @UserID, @ApprovalCode, @InitStatus, @InitStatus,
           0, @InitPoints, @InitStoreID
         from @CardTemp  
     end          
     --------------
                   
     -- ver 1.0.0.16
     if isnull(@InitAmount, 0) > 0 
       insert into CardCashDetail (CardNumber, TenderCode , TenderRate, AddAmount, BalanceAmount, ForfeitAmount, ExpiryDate, Status, CreatedBy, UpdatedBy)
       select CardNumber, '', 1, @InitAmount, @InitAmount, 0, @ExpiryDate, 1, @CreatedBy, @CreatedBy
       from @CardTemp  
     if isnull(@InitPoints, 0) > 0 
       insert into CardPointDetail (CardNumber, AddPoint, BalancePoint, ForfeitPoint, ExpiryDate, Status, CreatedBy, UpdatedBy)
       select CardNumber, @InitPoints, @InitPoints, 0, @ExpiryDate, 1, @CreatedBy, @CreatedBy
       from @CardTemp         
     -------------------
 	   if @NumberToUID in (1, 2, 3) 
	   begin	      
	     insert into CardUIDMap(CardUID, ImportCardNumber, BatchCardID, CardGradeID, CardNumber, Status, CreatedOn, CreatedBy)	     
	     select case @NumberToUID when 1 then CardNumber when 2 then substring(CardNumber, 1, len(CardNumber) - 1) when 3 then dbo.CalcCheckDigit(CardNumber, @ModeCode) end, 
	       null, @NewBatchID, @CardGradeID, CardNumber, 1, getdate(), @CreatedBy
	     from @CardTemp
	   end  
	   delete from @CardTemp

  end
  if @Type = 1
  begin         
    select @CouponNumMask = CouponNumMask, @CouponNumPattern = CouponNumPattern, @NumberTypeID = CouponTypeID, @IsImportCouponNumber = isnull(IsImportCouponNumber, 0),
        @Checkdigit = isnull(CouponCheckdigit, 0), @NumberToUID = isnull(CouponNumberToUID, 0),
         @ExpirydateValidityDuration = CouponValidityDuration, @ExpirydateValidityUnit = CouponValidityUnit,
         @SpecifyExpiryDate = CouponSpecifyExpiryDate, @ModeID = CheckDigitModeID, @QRCodePrefix = QRCodePrefix
      from CouponType where CouponTypeID = @NumberTypeID
    select @ModeCode = CheckDigitModeCode from CheckDigitMode where CheckDigitModeID = @ModeID
    set @ModeCode = isnull(@ModeCode, 'EAN13')
    -- 设置有效期
    if isnull(@ExpiryDate, 0) = 0
    begin                           
       if @ExpirydateValidityUnit = 1
         set @ExpiryDate = DATEADD(yy, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120)) 
       else if @ExpirydateValidityUnit = 2
         set @ExpiryDate = DATEADD(mm, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120))   
       else if @ExpirydateValidityUnit = 3
         set @ExpiryDate = DATEADD(ww, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120)) 
       else if @ExpirydateValidityUnit = 4
         set @ExpiryDate = DATEADD(dd, @ExpirydateValidityDuration, Convert(date, GETDATE(), 120))  
       else if @ExpirydateValidityUnit = 6  
         set @ExpiryDate = @SpecifyExpiryDate         
       else set @ExpiryDate = DATEADD(yy, 100, Convert(date, GETDATE(), 120))      
    end   
        
    if isnull(@ImportBatchCode, '') = ''  
      exec GetRefNoString 'BTHCOU', @NewBatchCode output
    else set @NewBatchCode = @ImportBatchCode

    set @CouponPatternLen = 0
    while @CouponPatternLen < Len(@CouponNumMask)
    begin      
        if substring(@CouponNumMask, @CouponPatternLen + 1, 1) = 'A'
          set @CouponPatternLen = @CouponPatternLen + 1
        else  
          break  
    end
    -- 考虑到导入的CouponNumber问题, 自增长的当前最大号码需要根据CouponNumPattern获得    
    if @Checkdigit = 1
              select @MaxCouponSeqNo = Convert(bigint, substring(max(CouponNumber), @CouponPatternLen + 1,  Len(@CouponNumMask) - @CouponPatternLen - 1)) from Coupon where len(CouponNumber) = len(@CouponNumMask) and CouponTypeID = @NumberTypeID and (CouponNumber like (@CouponNumPattern + '%') or isnull(@CouponNumPattern,'') = '') 
    else  
      select @MaxCouponSeqNo = Convert(bigint, substring(max(CouponNumber), @CouponPatternLen + 1,  Len(@CouponNumMask) - @CouponPatternLen)) from Coupon where  len(CouponNumber) = len(@CouponNumMask) and CouponTypeID = @NumberTypeID and (CouponNumber like (@CouponNumPattern + '%') or isnull(@CouponNumPattern,'') = '')
    select @MaxCouponSeqNo = isnull(@MaxCouponSeqNo, 0) + 1
    set @TempSeqNo = @StartingNumber --@MaxCouponSeqNo
 
    if @Checkdigit = 1
    begin
      select @ReturnStartNumber = substring(@CouponNumPattern, 1, @CouponPatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxCouponSeqNo), Len(@CouponNumMask) - @CouponPatternLen - 1)
      select @ReturnEndNumber = substring(@CouponNumPattern, 1, @CouponPatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxCouponSeqNo + @GenQty - 1), Len(@CouponNumMask) - @CouponPatternLen - 1)    
      select @ReturnStartNumber = dbo.CalcCheckDigit(@ReturnStartNumber, @ModeCode)
      select @ReturnEndNumber = dbo.CalcCheckDigit(@ReturnEndNumber, @ModeCode)
    end else
    begin
      select @ReturnStartNumber = substring(@CouponNumPattern, 1, @CouponPatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxCouponSeqNo), Len(@CouponNumMask) - @CouponPatternLen)
      select @ReturnEndNumber = substring(@CouponNumPattern, 1, @CouponPatternLen) + Right('000000000000000000000000000000' + Convert(varchar(30), @MaxCouponSeqNo + @GenQty - 1), Len(@CouponNumMask) - @CouponPatternLen)        
    end
    if isnull(@IsImportCouponNumber, 0) = 1 and isnull(@StartingNumber, 0) > 0  -- 根据输入的起始号码参数来产生couponnumber
    begin
      set @MaxCouponSeqNo = 1
      set @TempSeqNo = @StartingNumber 
      set @ReturnStartNumber = @StartingNumber
    END
    
    IF @StartingNumber = 1
        BEGIN
            SELECT @StartingNumber1 = 1
        END
    ELSE
        BEGIN
            SELECT @StartingNumber1 = SUBSTRING(CAST(@StartingNumber AS char), @CouponPatternLen + 1, LEN(CAST(@StartingNumber AS char)) -@CouponPatternLen) 
        END
                
    insert INTO BatchCoupon
      (BatchCouponCode, SeqFrom, SeqTo, Qty, CouponTypeID, CreatedOn, UpdatedOn, CreatedBy, UpdatedBy)
    --values (@NewBatchCode, @MaxCouponSeqNo, @MaxCouponSeqNo + @GenQty - 1, @GenQty, @NumberTypeID, getdate(), getdate(), @UserID, @UserID)             
    values (@NewBatchCode, @StartingNumber1, @StartingNumber1 + @GenQty - 1, @GenQty, @NumberTypeID, getdate(), getdate(), @UserID, @UserID)             
    --set @NewBatchID = @@IDENTITY    
    set @NewBatchID = SCOPE_IDENTITY()

    set @NumberHead = substring(@CouponNumPattern, 1, @CouponPatternLen)
    set @NumberSeqNoLength = Len(@CouponNumMask) - @CouponPatternLen  

    set @i = 1    
    set @TranCount = 0
    
    while @i <= @GenQty
    begin
--      if @RandomPWD = 1      -- 随机密码: 随机6位数字
--        set @InitPWD = LTRIM(str(round(rand() * 1000000, 0)))
      if isnull(@IsImportCouponNumber, 0) = 1 and isnull(@StartingNumber, 0) > 0  -- 根据输入的起始号码参数来产生couponnumber
      begin
        set @NumberSeqNo = @TempSeqNo
        set @ReturnEndNumber = @NumberSeqNo
      end else
      begin
        if @Checkdigit = 1											 
        begin
		      set @NumberSeqNo = @NumberHead + Right('000000000000000000000000000000' + Convert(varchar(30), @TempSeqNo), @NumberSeqNoLength - 1)
		      select @NumberSeqNo = dbo.CalcCheckDigit(@NumberSeqNo, @ModeCode)      
        end 
        else
          set @NumberSeqNo = @NumberHead + Right('000000000000000000000000000000' + Convert(varchar(30), @TempSeqNo), @NumberSeqNoLength)
      end        
        
      insert into @CouponTemp
          (CouponNumber, CouponTypeID, CouponIssueDate, CouponExpiryDate, StoreID, Status, BatchCouponID, 
           CouponPassword, CardNumber, CouponAmount, StockStatus)      
      values          
          (@NumberSeqNo, @NumberTypeID, @IssueDate, @ExpiryDate, null, @InitStatus, @NewBatchID, @InitPWD, '', @InitAmount, @InitStockStatus)
    
      set @TempSeqNo = @TempSeqNo + 1  
      set @i = @i + 1  
    END      
--print @NumberSeqNo        
    
    insert INTO Coupon
          (CouponNumber, CouponTypeID,  CouponIssueDate, CouponExpiryDate, StoreID, Status, BatchCouponID, 
           CouponPassword, CardNumber, CouponAmount, StockStatus)      
    SELECT CouponNumber, @NumberTypeID,@IssueDate, @ExpiryDate, null, @InitStatus, @NewBatchID, @InitPWD, 
       '', @InitAmount, @InitStockStatus from @CouponTemp

	  if @NumberToUID in (1, 2, 3) 
	  begin
	    if @NumberToUID = 1	      
	      set @CouponUID = @NumberSeqNo
	    else if @NumberToUID = 2		  
	      set @CouponUID = substring(@NumberSeqNo, 1, len(@NumberSeqNo) - 1)        
      else if @NumberToUID = 3        
	      set @CouponUID = dbo.CalcCheckDigit(@NumberSeqNo, @ModeCode)
	      
	    insert into CouponUIDMap(CouponUID, ImportCouponNumber, BatchCouponID, CouponTypeID, CouponNumber, Status, CreatedOn, CreatedBy)
	    select case @NumberToUID when 1 then CouponNumber when 2 then substring(CouponNumber, 1, len(CouponNumber) - 1) when 3 then dbo.CalcCheckDigit(CouponNumber, @ModeCode) end, 
	      null, @NewBatchID, @NumberTypeID, CouponNumber, 1, getdate(), @CreatedBy from @CouponTemp
	  end
	  
      --Todo 增加参数，送入参考编号,Approval Code, Createdby
    insert INTO Coupon_Movement
			(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
			BusDate, Txndate, Remark, SecurityCode, CreatedBy, ApprovalCode,OrgExpiryDate,NewExpiryDate, OrgStatus, NewStatus) 
	  select 0, '', CouponNumber, @NumberTypeID,'',0,@RefTxnNo,0, @InitAmount,@InitAmount,
	     @BusDate, @TxnDate, '','', @CreatedBy, @ApprovalCode,null,@ExpiryDate, 0, @InitStatus
	    from @CouponTemp 

    delete from @CouponTemp
    
/*
      if @InitStatus = 1
      begin
        insert into Coupon_Movement
			(OprID, CardNumber, CouponNumber, CouponTypeID, RefKeyID, RefReceiveKeyID, RefTxnNo, OpenBal, Amount, CloseBal, 
			 BusDate, Txndate, Remark, SecurityCode, CreatedBy, ApprovalCode,OrgExpiryDate,NewExpiryDate, OrgStatus, NewStatus) 
	    values
            (40, '', @NumberSeqNo, @NumberTypeID,'',0,@RefTxnNo,@InitAmount, 0 ,@InitAmount,
	         @BusDate, @TxnDate, '','', @CreatedBy, @ApprovalCode,null,@ExpiryDate, 0, @InitStatus)	         
	  end
*/	     
        
  end
  set @ReturnBatchID = @NewBatchID   

  return 0
end






GO
