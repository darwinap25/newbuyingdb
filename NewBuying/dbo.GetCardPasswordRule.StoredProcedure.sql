USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[GetCardPasswordRule]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[GetCardPasswordRule]
  @CardNumber			varchar(64),
  @LanguageAbbr			varchar(20)=''			   -- 1:
AS
/****************************************************************************
**  Name : GetCardPasswordRule
**  Version: 1.0.0.1
**  Description : 返回指定卡的密码规则。
**
**  Parameter :
  declare @a int
  exec @a = GetCardPasswordRule '000100086', ''
  print @a
**  Return:  0: 成功。 -1：
**  Created by: Gavin @2012-08-30
**  Modify by: Gavin @2013-12-16 (ver 1.0.0.1) 如果输入的@CardNumber为空，则表示要求获取Member的 密码规则(@PasswordRuleID设置为-1)。
**
****************************************************************************/
begin 
  declare @PasswordRuleID int, @result int
  set @CardNumber = isnull(@CardNumber, '')
  if @CardNumber <> ''
  begin
    select @PasswordRuleID = PasswordRuleID from Card C left join CardGrade G on C.CardGradeID = G.CardGradeID where CardNumber = @CardNumber
    if @@rowcount = 0
      return -2
  end else
    set @PasswordRuleID = -1      
  exec @result = GetPasswordRule @PasswordRuleID, @LanguageAbbr  
  return @result
end

GO
