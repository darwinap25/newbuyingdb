USE [NewBuying]
GO
/****** Object:  Table [dbo].[Ord_CardReceive_H]    Script Date: 12/13/2017 4:02:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ord_CardReceive_H](
	[CardReceiveNumber] [varchar](64) NOT NULL,
	[PurchaseType] [int] NULL,
	[ReferenceNo] [varchar](64) NULL,
	[StoreID] [int] NULL,
	[SupplierID] [int] NULL,
	[StorerAddress] [nvarchar](512) NULL,
	[SupplierAddress] [nvarchar](512) NULL,
	[SuppliertContactName] [varchar](512) NULL,
	[SupplierPhone] [varchar](512) NULL,
	[SupplierEmail] [varchar](512) NULL,
	[SupplierMobile] [varchar](512) NULL,
	[StoreContactName] [varchar](512) NULL,
	[StorePhone] [varchar](512) NULL,
	[StoreEmail] [varchar](512) NULL,
	[StoreMobile] [varchar](512) NULL,
	[Remark] [varchar](512) NULL,
	[OrderType] [int] NULL,
	[ReceiveType] [int] NULL,
	[Remark1] [varchar](512) NULL,
	[Remark2] [varchar](512) NULL,
	[CompanyID] [int] NULL,
	[CreatedBusDate] [datetime] NULL,
	[ApproveBusDate] [datetime] NULL,
	[ApprovalCode] [varchar](64) NULL,
	[ApproveStatus] [char](1) NULL,
	[ApproveOn] [datetime] NULL,
	[ApproveBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_ORD_CARDRECEIVE_H] PRIMARY KEY CLUSTERED 
(
	[CardReceiveNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Ord_CardReceive_H] ADD  DEFAULT ((1)) FOR [PurchaseType]
GO
ALTER TABLE [dbo].[Ord_CardReceive_H] ADD  DEFAULT ((0)) FOR [OrderType]
GO
ALTER TABLE [dbo].[Ord_CardReceive_H] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Ord_CardReceive_H] ADD  DEFAULT (getdate()) FOR [UpdatedOn]
GO
/****** Object:  Trigger [dbo].[Update_Ord_CardReceive_H]    Script Date: 12/13/2017 4:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Update_Ord_CardReceive_H] ON [dbo].[Ord_CardReceive_H]
FOR INSERT, UPDATE
AS
/*==============================================================*/
/*                
* Name: Update_Ord_CardReceive_H
* Version: 1.0.0.4
* Description : 供应商的Card收货单表的批核触发器

** Create By Gavin @2014-10-09
** Modify by Gavin @2015-02-11 (ver 1.0.0.1) 增加PurchaseType的区分。 PurchaseType=1 是原有的card库存订单。 PurchaseType=2是成card购买充值订单
** Modify by Gavin @2015-05-08 (ver 1.0.0.2) add call  AutoAllocationAfterHQReceive
** Modify by Gavin @2015-06-04 (ver 1.0.0.3) 增加校验,如果收货单中的卡不存在,则返回错误. trigger将会raise error
** Modify by Gavin @2015-06-08 (ver 1.0.0.4)  ROLLBACK 不能中止后面的操作, 必须加上return
*/
/*==============================================================*/
BEGIN  
  declare @CardReceiveNumber varchar(64), @ApproveStatus char(1), @CreatedBy int, @OldApproveStatus char(1), @ApprovalCode char(6)
  declare @FirstCard varchar(64), @EndCard varchar(64), @CardTypeID int, @ReceiveType int, @VoidStockStatus int
  declare @CardQty int, @StoreID int, @CardGradeID int, @CouponQty int, @PurchaseType int, @a int
  
  DECLARE CUR_CardReceive_H CURSOR fast_forward FOR
    SELECT CardReceiveNumber, ApproveStatus, CreatedBy, ReceiveType, StoreID, PurchaseType FROM INSERTED
  OPEN CUR_CardReceive_H
  FETCH FROM CUR_CardReceive_H INTO @CardReceiveNumber, @ApproveStatus, @CreatedBy, @ReceiveType, @StoreID, @PurchaseType
  WHILE @@FETCH_STATUS=0
  BEGIN  
    if @ReceiveType = 1
      set @VoidStockStatus = 0
    if @ReceiveType = 2
      set @VoidStockStatus = 6
            
    select @OldApproveStatus = ApproveStatus from Deleted where CardReceiveNumber = @CardReceiveNumber
    if (@OldApproveStatus = 'P' or isnull(@OldApproveStatus, '') = '') and @ApproveStatus = 'A' and Update(ApproveStatus)
    begin
      exec GenApprovalCode @ApprovalCode output   
      if isnull(@PurchaseType, 1) = 1
      begin 
        exec CardOrderApprove 1, 1, @CardReceiveNumber
        update Ord_CardReceive_H set ApprovalCode = @ApprovalCode where CardReceiveNumber = @CardReceiveNumber      
        -- 库存数量变动
        exec ChangeCardStockStatus 4, @CardReceiveNumber, 1   
        -- 产生提示消息.
        exec GenUserMessage @CreatedBy, 10, 1, @CardReceiveNumber     
      end else if @PurchaseType = 2
      begin
        if @ReceiveType = 1 
        begin 
          exec @a = DoCardRecharge_Purchase_HQ @CreatedBy, @CardReceiveNumber
          if @a <> 0
          begin
            RAISERROR ('Not find HQ Card.', 16, 1)
            if (@@Trancount > 0)
              ROLLBACK TRANSACTION 
            return  
          end else           
            exec AutoAllocationAfterHQReceive @CardReceiveNumber
            
        end else if @ReceiveType = 2
        begin
          exec @a = DoCardRecharge_Purchase_Store @CreatedBy, @CardReceiveNumber
          if @a <> 0
          begin
            RAISERROR ('Not find HQ Card.', 16, 1)
            if (@@Trancount > 0)
              ROLLBACK TRANSACTION 
            return  
          end           
        end
           
        update Ord_CardReceive_H set ApprovalCode = @ApprovalCode where CardReceiveNumber = @CardReceiveNumber        
      end   
    end  

    if @ApproveStatus = 'V' and Update(ApproveStatus)
    begin   
      if isnull(@PurchaseType, 1) = 1
      begin                      
        DECLARE CUR_CardReceive_Detail CURSOR fast_forward for
          select FirstCardNumber, EndCardNumber, CardTypeID, CardGradeID from Ord_CardReceive_D where CardReceiveNumber = @CardReceiveNumber
	      OPEN CUR_CardReceive_Detail
        FETCH FROM CUR_CardReceive_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
        WHILE @@FETCH_STATUS=0 
        BEGIN         
          update Card set StockStatus = @VoidStockStatus
            where CardTypeID = @CardTypeID and CardNumber >= @FirstCard and CardNumber <= @EndCard                                 
	        FETCH FROM CUR_CardReceive_Detail INTO @FirstCard, @EndCard, @CardTypeID, @CardGradeID
    	  END
        CLOSE CUR_CardReceive_Detail 
        DEALLOCATE CUR_CardReceive_Detail 
        
        -- 库存数量变动
        exec ChangeCardStockStatus 4, @CardReceiveNumber, 2   
      end 
      else if isnull(@PurchaseType, 1) = 2
      begin
        if @OldApproveStatus = 'A' 
        begin
          if @ReceiveType = 1 
          begin
            exec @a = DoCardRecharge_Purchase_HQ @CreatedBy, @CardReceiveNumber, 1
            if @a <> 0
            begin
              RAISERROR ('Not find HQ Card.', 16, 1)
              if (@@Trancount > 0)
                ROLLBACK TRANSACTION 
              return  
            end            
          end else if @ReceiveType = 2
          begin
            exec @a = DoCardRecharge_Purchase_Store @CreatedBy, @CardReceiveNumber, 1 
            if @a <> 0
            begin
              RAISERROR ('Not find HQ Card.', 16, 1)
              if (@@Trancount > 0)
                ROLLBACK TRANSACTION 
              return  
            end               
          end             
        end
      end                 
    end
       
    FETCH FROM CUR_CardReceive_H INTO @CardReceiveNumber, @ApproveStatus, @CreatedBy, @ReceiveType, @StoreID, @PurchaseType
  END
  CLOSE CUR_CardReceive_H 
  DEALLOCATE CUR_CardReceive_H   
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货确认单单号，主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'CardReceiveNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订货种类。1：实体卡订货。 2： 指定卡购买金额或者积分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'PurchaseType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参考编号。指coupon订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'ReferenceNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收到货的店铺（后台）主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'StoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发出或的供应商ID， 外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送货地址。（总部地址）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'StorerAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'SuppliertContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'SupplierMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'StoreContactName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'StorePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'StoreEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总部联系手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'StoreMobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单类型。 0：手动。1：自动。 默认：1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'OrderType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货来源。 1：正常的订单收货。2：退货单的收货（只有店铺的退货单）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'ReceiveType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'Remark1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'Remark2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据创建时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'CreatedBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据批核时的busdate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'ApproveBusDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批核时产生授权号，并通知前台' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'ApprovalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据状态。状态： R：prepare。 P: Picked.  A:Approve 。 V：Void' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H', @level2type=N'COLUMN',@level2name=N'ApproveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收货确认单。主表 （后台对供应商收货确认）（copy来源Ord_CouponReceive_H @2014-09-17）
@2015-02-11 统一增加PurchaseType 字段, 用于区分 金额/积分类的 订单 或者是 实体卡的 订单' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ord_CardReceive_H'
GO
