USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[POSCouponRedeemNormal]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[POSCouponRedeemNormal]  
  @UserID               varchar(512),           -- POS用户  
  @CardNumber           varchar(512),           -- 会员卡号 (可不填. 填写则绑定到此卡)  
  @CouponUID            varchar(512),           -- 需要激活的Coupon号码.如果多coupon则填写首号码  
  @CouponCount          int,                    -- 默认1, 大于1则表示从 @CouponNumber 开始的连续Coupon, 如果中间有不同状态的coupon,则返回错误.  
  @Brandcode            varchar(512),  -- 品牌编号   
  @StoreCode            varchar(512),  -- 店铺编号  
  @RegisterCode         varchar(512),           -- 终端号码  
  @ServerCode           varchar(512)='',        -- serverid   
  @TxnNoSN              varchar(512),           -- 交易唯一序号(每次正常调用,都提供不同的序号,确保唯一)  
  @TxnNo                varchar(512),           -- 交易单号  
  @BusDate              datetime,               -- 交易busdate  
  @TxnDate              datetime,    -- 交易发生日期时间  
  @Amount               money ,           -- 支付金额（如果coupon金额大于支付金额，则redeem coupon，并且返回支付金额。如果coupon金额小于支付金额则redeem coupon，并且返回coupon金额）  
  @VoidTxnNo            varchar(64),            -- 原单交易单号 
  @VoidTxnNoSN          varchar(64),            -- 原单交易单号 序号
  @SecurityCode         varchar(512),           -- 签名    
  @NeedCallConfirm      int=0,                    -- 是否需要调用POSTxnConfirm。0：不需要。 1：需要  
  @Additional           varchar(512),           -- 附加信息  
  @ItemList             varchar(max),   -- 销售清单  
  @CouponExpiryDate     datetime output,        -- coupon有效期  
  @CouponStatus         int output,             -- coupon状态    
  @CouponAmount         money output,           -- coupon的面额    
  @ApprovalCode         varchar(512) output,    -- 操作批核号码。（只有当@NeedCallConfirm=0时，返回ApprovalCode）  
  @DeductAmount   money output,   -- 实际扣款金额  
  @ForfeitAmount  money output,   -- Forfeit金额  
  @CloseBalance   money output,   -- 扣款后余额  
  @NeedPassword         int=0,       -- 是否验证密码。0：不需要。1：需要
  @CouponPassword       varchar(512)='',   -- Coupon密码  
  @TenderCode           varchar(64)=''    -- redeem coupon时，使用的POS的 TenderID。用于检查这个Coupon和Tender是否匹配    
                                          -- 为能兼容之前版本，参数放在最后，并加上默认值。为空时不做校验
AS  
/****************************************************************************  
**  Name : POSCouponRedeemNormal  
**  Version: 2.0.0.32  (for 711)  
**  Description : 在POS端使用coupon.  
**  
**  Parameter :  
select * from member
select * from Card
select * from Coupon

declare @A int ,@CouponExpiryDate     datetime, @CouponStatus         int, @CouponAmount         money, @ApprovalCode         varchar(512)
declare @DeductAmount   money, @ForfeitAmount  money, @CloseBalance   money
exec @A = POSCouponRedeem '1', '', '0018000020', 1, '01', 'D01', '', '', '201212040001001', '201212040001', '2012-12-04',
  '2012-12-04 05:12:12', 100, '', '', 1, '', N'<ROOT><ITEM PRODCODE="4894090007812" DEPTCODE="" QTY="1" NETPRICE="10.00" /></ROOT>',
  @CouponExpiryDate output, @CouponStatus output, @CouponAmount output, @ApprovalCode output, 
  @DeductAmount output, @ForfeitAmount output, @CloseBalance output, ''
print @A

**  Created by: Gavin @2012-05-23  
**  Modified By: Robin @2012-08-23  
**  Modified By: Robin @2012-08-28  
**  Modified By: Gavin @2012-08-31 （ver 2.0.0.2） 插入receiveTxn时， @amount使用负数  
**  Modified By: Robin @2012-09-05 （ver 2.0.0.3） 使用完XML后，释放XML对象  
**  Modified By: Gavin @2012-09-07 (ver 2.0.0.4) 校验出错时(@status<>0)，不再记录到receivetxn。receivetxn只记录成功交易  
**  Modified By: Robin @2012-09-11 (ver 2.0.0.5) 修正item coupon的计算错误  
**  Modified by: Robin @2012-09-26 (Ver 2.0.0.6) 不用#TempList表，而是用变量表@T  
**  Modified by: Gavin @2012-10-18 (Ver 2.0.0.7) 恢复返回 -28 的 error 校验  
**  Modify by: Gavin @2012-10-22 （ver 2.0.0.8）不传入Busdate，由存储过程自己获取  
**  Modify by: Gavin @2012-10-26 （ver 2.0.0.9）当不传入TxnNo时，就认为只是为Sell做校验，不实际插入Receivetxn。（for EdgePOS）  
**  Modify by: Gavin @2012-11-09 （ver 2.0.0.10）IsCheck时，也要检查sku  
**  Modify by: Robin @2012-11-15 (ver 2.0.0.11) 检查CouponNumber是否在同一单里面有重复  
**  Modify by: Gavin @2012-11-29 （ver 2.0.0.12）检查CouponNumber作为起始号是否在同一单里面有重复后，Return值由 -27 改为 -67
**  Modify by: Gavin @2012-12-10 （ver 2.0.0.13）当不传入TxnNo时,需要获得@RT_CouponTypeID值，否则校验失败 （for EdgePOS）
**  Modify by: Gavin @2012-12-25 （ver 2.0.0.14）2.0.0.12 版本的单号重复检查,需要过滤void情况
**  Modify By: Gavin @2012-12-31 (ver 2.0.0.15) 游标加上Local， 加快执行速度
**  Modify by: Gavin @2013-01-06 （ver 2.0.0.16）针对FS调用超时，DB执行成功的情况。 FS将执行recall， 此时判断TxnNoSN是否存在，如果已经存在则直接返回True
**  Modify by: Gavin @2013-02-20 （ver 2.0.0.17）FS在retry操作时，如果检查到receivetxn已经有记录，也需要填写返回参数。
**  Modify by: Gavin @2013-02-25 （ver 2.0.0.18）因为FS retry,是用不同的进程, 会有并发插入receivetxn的情况. 所以判断是否重复需要用脏读的方式读取receivetxn
**  Modify by: Gavin @2013-04-27 （ver 2.0.0.19）增加password校验,增加参数NeedPassword, CouponPassword.
**  Modify by: Gavin @2014-04-29 （ver 2.0.0.20）用户输入的Itemlist结构变更,改为:'<Root><Tenders><Tender TenderId="1234" Amount="1000"/><Tender TenderId="7876" Amount="2000" /></Tenders><Items><ITEM PRODCODE="123" DEPTCODE="" QTY="2" NETPRICE="33.00" /><ITEM PRODCODE="4894090007899" DEPTCODE="" QTY="2" NETPRICE="33.00" /></Items></Root>'
**                                               解析XML部分做相应修改. 
**  Modify by: Gavin @2014-05-29 （ver 2.0.0.21）金额部分是 * 100 的，所以需要做修改
**  Modify by: Gavin @2014-06-05 （ver 2.0.0.22）状态不正确时,返回细分的errorcode  
**  Modify by: Gavin @2014-07-16 （ver 2.0.0.23）兼容ItemList中 XML格式为 <ROOT><ITEM ...... /></ROOT> 的情况
**  Modify by: Gavin @2014-07-17 （ver 2.0.0.24）如果输入的AMount为负数, 则表示,此次提交为 Void 操作, 还原Coupon 的状态和金额,不做任何判断
**  Modify by: Gavin @2014-07-22 （ver 2.0.0.25）校验CardNumber和 CouponNumber关系.
**  Modify by: Gavin @2014-07-22 （ver 2.0.0.26）Void操作时, 和调用POSCouponRedeemVoid方法一样, 写入负数的金额和数量, 相同的OprID
**  Modify by: Gavin @2014-08-12 （ver 2.0.0.27）（for 711) 修改2.0.0.16的检查。不能覆盖@CouponNumber变量
**  Modify by: Gavin @2014-09-04 （ver 2.0.0.28）(for 711) 首先检查是否UnlimitedUsage的Coupon，如果是的话，直接返回0，退出，不写Receivetxn
**  Modify by: Gavin @2014-09-05 （ver 2.0.0.29）(for 711) 增加输入参数@VoidTxnNoSN，用于void时检查原记录的依据
**  Modify by: Gavin @2014-09-11 （ver 2.0.0.30）(for 711) UnlimitedUsage的Coupon, 需要加上检查状态，必须是 status=2 的
**  Modify by: Gavin @2014-09-19 （ver 2.0.0.31）(for 711) 调用MemberCardExchangeCouponCheck 来检查.
**  Modify by: Gavin @2017-08-24 （ver 2.0.0.32）(for Bauhaus) 合并SVA buying DB。 brand 区分store和product
**  
****************************************************************************/  
begin  
  declare @CouponNumber varchar(512), @Status int, @CouponTypeID int, @StoreCount int, @BrandID int, @StoreID int  
  DECLARE @idoc int   
  Declare @ProdCode varchar(512),@DepartCode varchar(12)   
  Declare @ItemAmount money  
  Declare @SEQ int, @NetPrice money, @IsCheck int  
  Declare @RT_Amount money, @RT_CouponTypeID int, @RT_CouponNumber varchar(512)  
  Declare @T Table(SEQ int identity, PRODCODE varchar(512),DEPTCODE varchar(512),QTY int ,NETPRICE money)  
  Declare @DuplicateCouponCount int --检查CouponNumber是否在同一单里面有重复 
  declare @RT_CouponCount int 
  declare @MD5CouponPWD varchar(512), @CouponPWD varchar(512)
  declare @ItemListTempStr varchar(64), @ItemsIndex int, @CouponCard varchar(64)
  declare @UnlimitedUsage int, @UnlimitedCouponStatus int, @CouponDeductAmount money
  
  set @CardNumber = ISNULL(@CardNumber, '')
  set @status=0  
  select @CouponNumber=CouponNumber from CouponUIDMap where CouponUID=@CouponUID  
  if isnull(@CouponNumber,'')=''   
    --set @status=-19  
    return -19  
  
  -- ver 2.0.0.28
    -- ver 2.0.0.30
  select @UnlimitedUsage = T.UnlimitedUsage, @UnlimitedCouponStatus = C.Status, @CouponCard = isnull(CardNumber, '') 
     from Coupon C Left join CouponType T on C.CouponTypeID = T.CouponTypeID
    where C.CouponNumber = @CouponNumber     
  if @UnlimitedUsage = 1 
  begin
     -- ver 2.0.0.25 同样要检查这个
    if @CardNumber <> '' and @CouponNumber <> '' and @CouponCard <> ''
    begin
      if @CardNumber <> @CouponCard 
        return -108
    end
    
    if @UnlimitedCouponStatus <> 2 
    begin
      set @status=-27 
      exec ConvertReturnStatus @status, @UnlimitedCouponStatus, @status output    
      return @status    
    end else 
      return 0     
  end
    
  -- ver 2.0.0.24
  if @Amount < 0
  begin   
    -- ver 2.0.0.29 
    if exists(select * from ReceiveTxn where TxnNoSN = @VoidTxnNoSN) 
    begin
      insert into ReceiveTxn  
        (BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber, OprID, Amount, Points, TenderID,  
        [Status], VoidKeyID, VoidTxnNo, Additional, ApproveStatus, Remark, SecurityCode, CreatedBy, UpdatedBy,Approvedby, ApprovalCode,  
        CouponNumber,CouponCount)  
      select @BrandCode, @StoreCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber, 54, C.CouponAmount - T.CouponTypeAmount, 0, Null,  
         0, Null, @VoidTxnNo, @Additional, case isnull(@NeedCallConfirm,0) when 0 then 'A' else 'P' end, '', @SecurityCode, @UserID, @UserID, @UserID, @ApprovalCode,  
        @CouponNumber, -abs(@CouponCount)
       from Coupon C left join CouponType T on C.CouponTypeID = T.CouponTypeID
       where C.CouponNumber = @CouponNumber
    end
    return 0 
  end
  
  -- ver 2.0.0.23 
  if isnull(@ItemList, '') <> ''
  begin
    select @ItemsIndex = charindex('<Items>', @ItemList)    
    if @ItemsIndex > 0     
      set @ItemListTempStr = '/Root/Items/ITEM'    
    begin    
       select @ItemsIndex = charindex('<PLU', @ItemList)    
       if @ItemsIndex > 0     
         set @ItemListTempStr = '/ROOT/PLU'     
       else          
         set @ItemListTempStr = '/ROOT/ITEM'     
    end  
  end
  
  -- ver 2.0.0.31 校验
  exec @status = MemberCardExchangeCouponCheck @CardNumber, @CouponUID, @StoreID, @Amount, @ItemList, @CouponDeductAmount output 
  if @status <> 0
    return @status
     
 
  -- ver 2.0.0.16
  declare @ApproveStatus char(1)
  select @ApproveStatus = ApproveStatus, @DeductAmount = abs(Amount)  --, @CouponNumber = CouponNumber 
    from receivetxn with (nolock) where TxnNo = @TxnNo and TxnNoSN = @TxnNoSN 
  set @DeductAmount = isnull(@DeductAmount, 0)
  -- ver 2.0.0.17
  if isnull(@ApproveStatus, '') = 'P'
  begin
    set @ApprovalCode = ''        
    select @CouponAmount = CouponAmount, @CouponStatus = Status, @CouponExpiryDate = CouponExpiryDate,
       @ForfeitAmount = CouponAmount - @DeductAmount, @CloseBalance = 0
      from Coupon where CouponNumber = @CouponNumber    
    return 0
  end
  ------------------    
  if isnull(@TxnNo, '') = '' or Right(@TxnNo, 6) = '000000'     -- FS 会组合TxnNo，如果前台传入空，这里会变成000000  
    set @IsCheck = 1      
  else set @IsCheck = 0  
    
  set @CouponCount=isnull(@CouponCount,1)  
  if @CouponCount=0 set @CouponCount=1  
  if isnull(@BusDate, 0) = 0  
    select top 1 @BusDate = BusDate from sodeod where SOD = 1 and EOD = 0 order by BusDate desc  
  if isnull(@TxnDate, 0) = 0   
    set @TxnDate = getdate()     
	 
  if @Status=0  
  begin  
    select @BrandID=StoreBrandID from Brand where StoreBrandCode=@BrandCode  
    select @CouponExpiryDate=CouponExpiryDate,@CouponStatus=[Status],@CouponAmount=CouponAmount, @CouponTypeID=CouponTypeID,
        @CouponPWD = CouponPassword, @CouponCard = isnull(CardNumber, '')
      from Coupon where CouponNumber=@CouponNumber  
    if @CouponStatus<>2  
    begin 
      set @status=-27 
      exec ConvertReturnStatus @status, @CouponStatus, @status output    
      return @status
    end  
         
    --修改请求金额，当大于Coupon金额时候，直接改为Coupon金额  
    if @CouponAmount < @Amount  
      set @Amount=@CouponAmount  
 --------------------------------------------------------      
  end  
 
   -- ver 2.0.0.25 
  if @CardNumber <> '' and @CouponNumber <> '' and @CouponCard <> ''
  begin
    if @CardNumber <> @CouponCard 
      return -108
  end
 
  -- ver 2.0.0.19
  if isnull(@NeedPassword, 0) = 1
  begin
    set @MD5CouponPWD = dbo.EncryptMD5(@CouponPassword)
    if @MD5CouponPWD <> @CouponPWD
      return -4 
  end
    
  if @Status=0  
  begin  
    -- 2.0.0.14
    --select @DuplicateCouponCount=Count(CouponNumber) from ReceiveTxn where TxnNo=@TxnNo and CouponNumber=@CouponNumber  
    select @DuplicateCouponCount=sum(CouponCount) from ReceiveTxn where TxnNo=@TxnNo and CouponNumber=@CouponNumber  
    if @DuplicateCouponCount>0   
    begin  
      set @status=-67 --交易中Coupon重复，返回状态不正确的错误  
      return @status  
    end  
  end    
    
  if @Status=0  
  begin  
    select @StoreID=StoreID from Store where BrandID=@BrandID and StoreCode=@StoreCode    
    select @StoreCount=count(StoreID) from CouponTypeStoreCondition_List  
    where CouponTypeID=@CouponTypeID and StoreID=@StoreID and StoreConditionType=2  
    if @StoreCount=0   
    begin  
      set @status=-28  
      return @status  
    end  
  end    
  
  -- Tender的校验
  if (@Status = 0) and (isnull(@TenderCode, '') <> '')
  begin
   if not exists(select keyid from CouponTypeExchangeBinding     
              where CouponTypeID = @CouponTypeID and BindingType = 5 and TenderCode = @TenderCode)
     return -69    -- POS使用Coupon时,使用的TenderID不正确.          
  end
  -- Tender的校验 end
     
  --Create an internal representation of the XML document.  
  EXEC sp_xml_preparedocument @idoc OUTPUT, @ItemList  
  -- Execute a SELECT statement that uses the OPENXML rowset provider.  
  if exists(SELECT PRODCODE,DEPTCODE,QTY,NETPRICE FROM OPENXML (@idoc, @ItemListTempStr, 1) WITH (PRODCODE varchar(512),DEPTCODE varchar(512),QTY int, NETPRICE money))  
  begin  
    insert into @T(prodcode,deptcode,qty,netprice) SELECT PRODCODE,DEPTCODE,QTY,NETPRICE FROM OPENXML (@idoc, @ItemListTempStr, 1) WITH (PRODCODE varchar(512),DEPTCODE varchar(512),QTY int, NETPRICE money)  
--    update @T set netprice = netprice * 0.01
  end  
      
  if @Status=0 and  @IsCheck = 1  
  begin
    set @RT_CouponTypeID = @CouponTypeID  
    if not exists(select B.ProdCode, DepartCode  from CouponTypeExchangeBinding B inner join @T I on B.ProdCode=I.ProdCode  
                    where CouponTypeID=@RT_CouponTypeID and BindingType=2 and BrandID=@BrandID)   
    begin   
      if @idoc>0  
        EXECUTE sp_xml_removedocument @idoc      
      set @status=-29  
      return @status   
    end     
  end  
   
  if @Status=0 and  @IsCheck = 0  
  begin  
    if exists(select ProdCode, DepartCode  from CouponTypeExchangeBinding  
                 where CouponTypeID=@CouponTypeID and BindingType=2 and BrandID=@BrandID)  
    begin  
      -- 删除已经用过Coupon的货品----------------------------------------  
      declare CUR_ReceiveTXN cursor fast_forward local for  
        --select abs(Amount), CouponTypeID, ReceiveTxn.CouponNumber from ReceiveTxn inner join Coupon on ReceiveTxn.CouponNumber=Coupon.CouponNumber where TxnNo=@TxnNo  
        select abs(sum(Amount)), max(CouponTypeID), max(CouponNumber), sum(CouponCount)
        from (
               select R.Amount, C.CouponTypeID, R.CouponNumber, R.CouponCount
                 from ReceiveTxn R inner join Coupon C on R.CouponNumber=C.CouponNumber   
                where TxnNo=@TxnNo  
            ) A group by CouponTypeID, CouponNumber
        having sum(CouponCount) > 0
      Open CUR_ReceiveTxn  
      Fetch From CUR_ReceiveTxn INTO @RT_Amount, @RT_CouponTypeID, @RT_CouponNumber, @RT_CouponCount 
      WHILE @@FETCH_STATUS=0      
      BEGIN 
        if not exists(select B.ProdCode, DepartCode  from CouponTypeExchangeBinding B inner join @T I on B.ProdCode=I.ProdCode  
                        where CouponTypeID=@RT_CouponTypeID and BindingType=2 and BrandID=@BrandID)  
        begin  
          declare CUR_DEPTLIST cursor fast_forward local for   
            select I.SEQ, I.NetPrice from CouponTypeExchangeBinding B inner join @T I on B.DepartCode=I.DeptCode  
              where CouponTypeID=@RT_CouponTypeID and BindingType=2 and BrandID=@BrandID  
          Open CUR_DEPTLIST  
          Fetch from CUR_DEPTLIST into @SEQ, @NetPrice  
          While @@FETCH_STATUS=0  
          Begin  
            if @NetPrice<=@RT_Amount  
            begin  
              set @RT_Amount=@RT_Amount-@NetPrice  
              DELETE from @T where Seq=@SEQ  
            end    
            else  
            begin  
              UPDATE @T set NetPrice=@NetPrice-@RT_Amount where Seq=@SEQ  
              BREAK   
            end  
            Fetch from CUR_DEPTLIST into @SEQ, @NetPrice  
          End  
          Close CUR_DEPTLIST  
          Deallocate CUR_DEPTLIST  
        end  
        else  
        begin  
          declare CUR_PRODLIST cursor fast_forward local for   
            select I.SEQ, I.NetPrice from CouponTypeExchangeBinding B inner join @T I on B.ProdCode=I.ProdCode   
              where CouponTypeID=@RT_CouponTypeID and BindingType=2 and BrandID=@BrandID  
          Open CUR_PRODLIST  
          Fetch from CUR_PRODLIST into @SEQ, @NetPrice  
          While @@FETCH_STATUS=0  
          Begin  
            if @NetPrice<=@RT_Amount  
            begin  
              set @RT_Amount=@RT_Amount-@NetPrice  
              DELETE from @T where Seq=@SEQ  
            end    
            else  
            begin  
              UPDATE @T set NetPrice=@NetPrice-@RT_Amount where Seq=@SEQ  
              BREAK   
            end  
            Fetch from CUR_PRODLIST into @SEQ, @NetPrice  
          End  
          Close CUR_PRODLIST  
          Deallocate CUR_PRODLIST       
        end  
        FETCH FROM CUR_ReceiveTxn INTO @RT_Amount, @RT_CouponTypeID, @RT_CouponNumber, @RT_CouponCount
      END  
      CLOSE CUR_ReceiveTxn  
      Deallocate CUR_ReceiveTxn  
        
      -------------------------------------------------------------------  
        
      --判断能否使用当前的Coupon  
      if not exists(select B.ProdCode, DepartCode  from CouponTypeExchangeBinding B inner join @T I  on B.ProdCode=I.ProdCode where CouponTypeID=@CouponTypeID and BindingType=2 and BrandID=@BrandID)  
      begin  
        select @ItemAmount=sum(NetPrice) from CouponTypeExchangeBinding B inner join @T I on B.DepartCode=I.DeptCode  
          where CouponTypeID=@CouponTypeID and BindingType=2 and BrandID=@BrandID  
        if isnull(@ItemAmount,0)=0  
        begin  
          if @idoc>0  
            EXECUTE sp_xml_removedocument @idoc       
          return -29  
        end  
        if @ItemAmount<@Amount  
        begin  
          set @Amount=@ItemAmount  
        end  
      end  
      else  
      begin  
        select @ItemAmount=sum(NetPrice) from CouponTypeExchangeBinding B inner join @T I on B.ProdCode=I.ProdCode where CouponTypeID=@CouponTypeID and BindingType=2 and BrandID=@BrandID  
        if @ItemAmount<@Amount  
        begin  
          set @Amount=@ItemAmount  
        end  
      end  
      ---------------------------------------------------------------------     
    end  
    else  
    begin  
      if @CouponAmount < @Amount  
        set @Amount=@CouponAmount  
    end  
  end    
    
  set @DeductAmount=@Amount  
  set @ForfeitAmount=@CouponAmount-@DeductAmount --Robin: Todo 调用Forfeit方法计算金额  
  set @CloseBalance=@CouponAmount-@DeductAmount-@ForfeitAmount  
  if @Status = 0 and @IsCheck = 0  
  begin  
    insert into ReceiveTxn  
      (BrandCode, StoreCode, ServerCode, RegisterCode, TxnNoSN, TxnNo, BusDate, TxnDate, CardNumber, OprID, Amount, Points, TenderID,  
      [Status], VoidKeyID, VoidTxnNo, Additional, ApproveStatus, Remark, SecurityCode, CreatedBy, UpdatedBy,Approvedby, ApprovalCode,  
      CouponNumber,CouponCount)  
    values  
      (@BrandCode, @StoreCode, @ServerCode, @RegisterCode, @TxnNoSN, @TxnNo, @BusDate, @TxnDate, @CardNumber, 54, -(@Amount), 0, Null,  
       @Status, Null, @VoidTxnNo, @Additional, case isnull(@NeedCallConfirm,0) when 0 then 'A' else 'P' end, '', @SecurityCode, @UserID, @UserID, @UserID, @ApprovalCode,  
      @CouponNumber,@CouponCount)   
  end  
  if @idoc>0  
   EXECUTE sp_xml_removedocument @idoc  
  return @Status    
end    

GO
