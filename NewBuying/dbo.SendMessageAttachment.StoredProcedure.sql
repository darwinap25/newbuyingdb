USE [NewBuying]
GO
/****** Object:  StoredProcedure [dbo].[SendMessageAttachment]    Script Date: 12/13/2017 4:02:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  PROCEDURE [dbo].[SendMessageAttachment]
  @KeyID                int,          --附件表ID
  @MemberID				int,          --接收方的会员ID
  @MessageID            int,          --消息ID
  @FileName             varchar(64),  --附件文件名（检索使用）
  @FilePath             nvarchar(512),--附件文件路径
  @FileBinaryContent    varbinary(max)--附件内容。二进制存放
AS
/****************************************************************************
**  Name : SendMessageAttachment
**  Version: 1.0.0.0
**  Description : 发送会员附件 （必须有会员ID）
**
**  Parameter :
  declare @a int, @MessageID int
  exec @a = UpdateMemberMessageObject 2, 2
  print @a  
**  Created by: Gavin @2012-05-23
**
****************************************************************************/
begin

  if isnull(@KeyID, 0) = 0      -- 修改附件内容。
  begin
    update MessageAttachment set AttachFileName = @FileName, AttachFilePath = @FilePath, AttachFileBinaryContent = @FileBinaryContent
      where KeyID = @KeyID
  end else
  begin
    insert into MessageAttachment
      (MessageID, MemberID, AttachFileName, AttachFilePath, AttachFileBinaryContent)
    values(@MessageID, @MemberID, @FileName, @FilePath, @FileBinaryContent)  
  end

  return 0  
end

GO
